.class public Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JSP;


# instance fields
.field private a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private b:Landroid/widget/ImageButton;

.field public c:LX/JSs;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

.field private g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

.field private h:Landroid/graphics/drawable/TransitionDrawable;

.field public i:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2695270
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2695271
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h()V

    .line 2695272
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2695267
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2695268
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h()V

    .line 2695269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2695264
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2695265
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h()V

    .line 2695266
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2695251
    const v0, 0x7f030ba5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2695252
    const v0, 0x7f0d1ce0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b:Landroid/widget/ImageButton;

    .line 2695253
    const v0, 0x7f0d1cdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2695254
    const v0, 0x7f0d1cdd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2695255
    const v0, 0x7f0d1ce1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->f:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    .line 2695256
    const v0, 0x7f0d1cde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    .line 2695257
    new-instance v0, LX/JSs;

    invoke-direct {v0, p0}, LX/JSs;-><init>(LX/JSP;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    .line 2695258
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0214b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0213f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v4

    .line 2695259
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    .line 2695260
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 2695261
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2695262
    const v0, 0x7f0d1cdc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2695263
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2695248
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2695249
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2695250
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2695246
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2695247
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1Ad;LX/33B;)V
    .locals 2

    .prologue
    .line 2695238
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 2695239
    iput-object p4, v0, LX/1bX;->j:LX/33B;

    .line 2695240
    move-object v0, v0

    .line 2695241
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2695242
    invoke-virtual {p3, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2695243
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2695244
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    .line 2695245
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2695234
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2695235
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2695236
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2695237
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2695232
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2695233
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2695273
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2695274
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 2695275
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 2695223
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-nez v0, :cond_1

    .line 2695224
    :cond_0
    :goto_0
    return-void

    .line 2695225
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    .line 2695226
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    .line 2695227
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->d:Landroid/graphics/drawable/Drawable;

    .line 2695228
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2695229
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2695230
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 2695231
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCoverArt()Landroid/view/View;
    .locals 1

    .prologue
    .line 2695222
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getCoverArtDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2695221
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getPlayButtonDrawable()Landroid/graphics/drawable/TransitionDrawable;
    .locals 1

    .prologue
    .line 2695220
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->h:Landroid/graphics/drawable/TransitionDrawable;

    return-object v0
.end method

.method public getPlaybackAnimation()LX/JSs;
    .locals 1

    .prologue
    .line 2695219
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    return-object v0
.end method

.method public getProgressView()Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;
    .locals 1

    .prologue
    .line 2695218
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->f:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    return-object v0
.end method

.method public getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;
    .locals 1

    .prologue
    .line 2695206
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    return-object v0
.end method

.method public setBlurredCoverDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2695216
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->i:Landroid/graphics/drawable/Drawable;

    .line 2695217
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2695209
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695210
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695211
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695212
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->g:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695213
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695214
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->f:Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695215
    return-void
.end method

.method public setPlayButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2695207
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2695208
    return-void
.end method
