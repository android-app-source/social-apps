.class public Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "LX/JSZ;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JSn;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2696026
    new-instance v0, LX/JSm;

    invoke-direct {v0}, LX/JSm;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696027
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2696028
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->b:Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    .line 2696029
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;
    .locals 4

    .prologue
    .line 2696030
    const-class v1, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    monitor-enter v1

    .line 2696031
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2696032
    sput-object v2, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2696033
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2696034
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2696035
    new-instance p0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;-><init>(Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;)V

    .line 2696036
    move-object v0, p0

    .line 2696037
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2696038
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2696039
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2696040
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 2696041
    sget-object v0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2696042
    check-cast p2, LX/JSn;

    .line 2696043
    iget-object v6, p0, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->b:Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    new-instance v0, LX/JSg;

    iget-object v1, p2, LX/JSn;->b:LX/JSe;

    iget-object v2, p2, LX/JSn;->a:LX/JTY;

    iget-object v3, p2, LX/JSn;->c:LX/JSl;

    iget v4, p2, LX/JSn;->d:I

    iget-object v5, p2, LX/JSn;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v0 .. v5}, LX/JSg;-><init>(LX/JSe;LX/JTY;LX/JSl;ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2696044
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2696045
    const/4 v0, 0x1

    return v0
.end method
