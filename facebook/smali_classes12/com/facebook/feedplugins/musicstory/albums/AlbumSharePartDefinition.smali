.class public Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        "E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

.field public final c:Landroid/content/Context;

.field public final d:LX/1V8;

.field public final e:LX/1DR;

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/2dq;

.field public final h:LX/JTZ;

.field public final i:Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

.field public final j:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/1V8;LX/1DR;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;LX/JTZ;Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696005
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2696006
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->c:Landroid/content/Context;

    .line 2696007
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->j:Ljava/util/concurrent/ExecutorService;

    .line 2696008
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->d:LX/1V8;

    .line 2696009
    iput-object p4, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->e:LX/1DR;

    .line 2696010
    iput-object p5, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2696011
    iput-object p6, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->g:LX/2dq;

    .line 2696012
    iput-object p10, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2696013
    iput-object p9, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    .line 2696014
    iput-object p7, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->h:LX/JTZ;

    .line 2696015
    iput-object p8, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->i:Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    .line 2696016
    return-void
.end method

.method private a(LX/1Pq;LX/JSy;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSl;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pq;",
            "LX/JSy;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JSl;",
            ")",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 2696004
    new-instance v0, LX/JSj;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/JSj;-><init>(Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSy;LX/JSl;LX/1Pq;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;
    .locals 14

    .prologue
    .line 2695993
    const-class v1, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    monitor-enter v1

    .line 2695994
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695995
    sput-object v2, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695996
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695997
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695998
    new-instance v3, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V8;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v9

    check-cast v9, LX/2dq;

    const-class v10, LX/JTZ;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/JTZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/1V8;LX/1DR;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;LX/JTZ;Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2695999
    move-object v0, v3

    .line 2696000
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2696001
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2696002
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2696003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2695989
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2695990
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2695991
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2695992
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x78df566b

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 2695988
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2695892
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 2695893
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->c:Landroid/content/Context;

    .line 2695894
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->d:LX/1V8;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-virtual {v1, v2}, LX/1V8;->a(LX/1Ua;)LX/1Ub;

    move-result-object v1

    .line 2695895
    iget-object v2, v1, LX/1Ub;->d:LX/1Uc;

    move-object v1, v2

    .line 2695896
    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Uc;->a(I)F

    move-result v1

    .line 2695897
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->e:LX/1DR;

    iget-object v3, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/1DR;->a(Landroid/content/Context;F)I

    move-result v1

    move v1, v1

    .line 2695898
    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 2695899
    int-to-float v0, v0

    const/high16 v1, 0x41000000    # 8.0f

    add-float/2addr v1, v0

    .line 2695900
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2695901
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v2

    .line 2695902
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2695903
    new-instance v2, LX/1X6;

    sget-object v3, LX/2eF;->a:LX/1Ua;

    invoke-direct {v2, v0, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2695904
    new-instance v3, LX/JTT;

    .line 2695905
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2695906
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v3, v0}, LX/JTT;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2695907
    new-instance v0, LX/JSx;

    .line 2695908
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2695909
    iget-object v4, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2695910
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v4, 0x0

    move v6, v4

    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2695911
    new-instance v10, LX/JTU;

    invoke-virtual {p2, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-direct {v10, v4}, LX/JTU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2695912
    new-instance v4, LX/JSd;

    invoke-virtual {v10}, LX/JTU;->a()Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v4, v11}, LX/JSd;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v10}, LX/JTU;->c()Ljava/lang/String;

    move-result-object v11

    .line 2695913
    iput-object v11, v4, LX/JSd;->c:Ljava/lang/String;

    .line 2695914
    move-object v4, v4

    .line 2695915
    invoke-virtual {v10}, LX/JTU;->b()Ljava/lang/String;

    move-result-object v11

    .line 2695916
    iput-object v11, v4, LX/JSd;->f:Ljava/lang/String;

    .line 2695917
    move-object v4, v4

    .line 2695918
    invoke-virtual {v10}, LX/JTU;->d()Landroid/net/Uri;

    move-result-object v11

    .line 2695919
    iput-object v11, v4, LX/JSd;->g:Landroid/net/Uri;

    .line 2695920
    move-object v4, v4

    .line 2695921
    invoke-virtual {v10}, LX/JTU;->e()Landroid/net/Uri;

    move-result-object v11

    .line 2695922
    iput-object v11, v4, LX/JSd;->k:Landroid/net/Uri;

    .line 2695923
    move-object v4, v4

    .line 2695924
    invoke-virtual {v10}, LX/JTU;->f()Ljava/lang/String;

    move-result-object v11

    .line 2695925
    iput-object v11, v4, LX/JSd;->e:Ljava/lang/String;

    .line 2695926
    move-object v4, v4

    .line 2695927
    invoke-virtual {v10}, LX/JTU;->g()Ljava/lang/String;

    move-result-object v11

    .line 2695928
    iput-object v11, v4, LX/JSd;->d:Ljava/lang/String;

    .line 2695929
    move-object v4, v4

    .line 2695930
    invoke-virtual {v10}, LX/JTU;->h()Landroid/net/Uri;

    move-result-object v11

    .line 2695931
    iput-object v11, v4, LX/JSd;->h:Landroid/net/Uri;

    .line 2695932
    move-object v4, v4

    .line 2695933
    invoke-virtual {v10}, LX/JTU;->i()Landroid/net/Uri;

    move-result-object v11

    .line 2695934
    iput-object v11, v4, LX/JSd;->i:Landroid/net/Uri;

    .line 2695935
    move-object v4, v4

    .line 2695936
    invoke-virtual {v10}, LX/JTU;->j()Ljava/lang/String;

    move-result-object v10

    .line 2695937
    iput-object v10, v4, LX/JSd;->j:Ljava/lang/String;

    .line 2695938
    move-object v4, v4

    .line 2695939
    invoke-virtual {v4}, LX/JSd;->a()LX/JSe;

    move-result-object v4

    .line 2695940
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2695941
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2695942
    :cond_0
    move-object v4, v7

    .line 2695943
    invoke-direct {v0, v4}, LX/JSx;-><init>(Ljava/util/List;)V

    .line 2695944
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object v4, v4

    .line 2695945
    iput-object v4, v0, LX/JSx;->c:Ljava/lang/String;

    .line 2695946
    move-object v0, v0

    .line 2695947
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->bD()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2695948
    const/4 v4, 0x0

    .line 2695949
    :goto_2
    move-object v4, v4

    .line 2695950
    iput-object v4, v0, LX/JSx;->d:Landroid/net/Uri;

    .line 2695951
    move-object v0, v0

    .line 2695952
    iget-object v4, v3, LX/JTT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, v3, LX/JTT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-nez v4, :cond_8

    .line 2695953
    :cond_1
    const/4 v4, 0x0

    .line 2695954
    :goto_3
    move-object v4, v4

    .line 2695955
    iput-object v4, v0, LX/JSx;->h:Landroid/net/Uri;

    .line 2695956
    move-object v0, v0

    .line 2695957
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->aa()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->aa()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2695958
    :cond_2
    const/4 v4, 0x0

    .line 2695959
    :goto_4
    move-object v4, v4

    .line 2695960
    iput-object v4, v0, LX/JSx;->b:Ljava/lang/String;

    .line 2695961
    move-object v0, v0

    .line 2695962
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fF()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2695963
    iput-object v4, v0, LX/JSx;->a:Ljava/lang/String;

    .line 2695964
    move-object v0, v0

    .line 2695965
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2695966
    :cond_3
    const/4 v4, 0x0

    .line 2695967
    :goto_5
    move-object v4, v4

    .line 2695968
    iput-object v4, v0, LX/JSx;->e:Landroid/net/Uri;

    .line 2695969
    move-object v0, v0

    .line 2695970
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2695971
    :cond_4
    const/4 v4, 0x0

    .line 2695972
    :goto_6
    move-object v4, v4

    .line 2695973
    iput-object v4, v0, LX/JSx;->f:Landroid/net/Uri;

    .line 2695974
    move-object v0, v0

    .line 2695975
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    .line 2695976
    :cond_5
    const/4 v4, 0x0

    .line 2695977
    :goto_7
    move-object v3, v4

    .line 2695978
    iput-object v3, v0, LX/JSx;->g:Ljava/lang/String;

    .line 2695979
    move-object v0, v0

    .line 2695980
    new-instance v3, LX/JSy;

    invoke-direct {v3, v0}, LX/JSy;-><init>(LX/JSx;)V

    move-object v0, v3

    .line 2695981
    move-object v4, v0

    .line 2695982
    new-instance v3, LX/JSk;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, LX/JSk;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2695983
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, LX/JSl;

    .line 2695984
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695985
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    new-instance v2, LX/2dx;

    invoke-direct {v2}, LX/2dx;-><init>()V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695986
    iget-object v6, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->g:LX/2dq;

    sget-object v7, LX/2eF;->a:LX/1Ua;

    const/4 v8, 0x1

    invoke-virtual {v2, v1, v7, v8}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    const/4 v2, 0x0

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p3, v4, p2, v3}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a(LX/1Pq;LX/JSy;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSl;)LX/2eJ;

    move-result-object v3

    invoke-static {v5}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695987
    const/4 v0, 0x0

    return-object v0

    :cond_6
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->W()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_7
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->bD()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_2

    :cond_8
    iget-object v4, v3, LX/JTT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_3

    :cond_9
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->aa()LX/0Px;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto/16 :goto_4

    :cond_a
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_5

    :cond_b
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_6

    :cond_c
    iget-object v4, v3, LX/JTT;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2695891
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
