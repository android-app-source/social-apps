.class public Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:LX/JSJ;

.field public b:LX/JSe;

.field public c:LX/JSI;

.field public d:I

.field public e:LX/JSl;

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JSJ;)V
    .locals 1

    .prologue
    .line 2695395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695396
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSJ;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    .line 2695397
    return-void
.end method

.method public static b(Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V
    .locals 2

    .prologue
    .line 2695393
    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, p0, v0, v1}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2695394
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2695370
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    invoke-virtual {v0, v1}, LX/JSJ;->b(LX/JSe;)Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    move-result-object v0

    .line 2695371
    if-nez v0, :cond_1

    .line 2695372
    :cond_0
    :goto_0
    return-void

    .line 2695373
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    .line 2695374
    iget-object v3, v1, LX/JSJ;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->c:LX/JSR;

    invoke-virtual {v3, v2}, LX/JSR;->c(LX/JSe;)Z

    move-result v3

    move v1, v3

    .line 2695375
    if-eqz v1, :cond_0

    .line 2695376
    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2695377
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->f:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_2
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2695378
    if-nez v1, :cond_6

    :cond_3
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2695379
    if-eqz v1, :cond_5

    .line 2695380
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    iget-object v3, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->c:LX/JSI;

    .line 2695381
    iget-object p0, v1, LX/JSJ;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    iget-object p0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->c:LX/JSR;

    invoke-virtual {p0, v2, v3}, LX/JSR;->c(LX/JSe;LX/JSI;)V

    .line 2695382
    iget-object p0, v1, LX/JSJ;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    iget-object p0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->d:Ljava/util/HashMap;

    invoke-virtual {p0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2695383
    iget-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object v1, v1

    .line 2695384
    if-eqz v1, :cond_4

    .line 2695385
    iget-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object v1, v1

    .line 2695386
    invoke-virtual {v1}, LX/JSs;->b()V

    .line 2695387
    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b()V

    .line 2695388
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setPlayButtonVisibility(I)V

    .line 2695389
    :cond_4
    goto :goto_0

    .line 2695390
    :cond_5
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->e:LX/JSl;

    if-eqz v1, :cond_8

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->d:I

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->e:LX/JSl;

    iget v2, v2, LX/JSl;->a:I

    if-eq v1, v2, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2695391
    if-nez v1, :cond_0

    .line 2695392
    invoke-static {p0, v0}, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b(Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method
