.class public Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JSg;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JSZ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

.field private final f:Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

.field private final g:LX/JTD;

.field private final h:Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2695795
    const-class v0, LX/JSZ;

    const-string v1, "music_story_inline_card"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;LX/JSC;LX/JTD;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695830
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2695831
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->b:Landroid/content/Context;

    .line 2695832
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->e:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    .line 2695833
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->f:Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    .line 2695834
    iput-object p5, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->g:LX/JTD;

    .line 2695835
    iput-object p6, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2695836
    iput-object p7, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 2695837
    sget-object v0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2695838
    new-instance p3, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;

    invoke-static {p4}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p1

    check-cast p1, LX/1Ad;

    const-class p2, LX/JSA;

    invoke-interface {p4, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/JSA;

    invoke-direct {p3, v0, p1, p2}, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;-><init>(Lcom/facebook/common/callercontext/CallerContext;LX/1Ad;LX/JSA;)V

    .line 2695839
    move-object v0, p3

    .line 2695840
    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->h:Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;

    .line 2695841
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;
    .locals 11

    .prologue
    .line 2695819
    const-class v1, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    monitor-enter v1

    .line 2695820
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695821
    sput-object v2, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695822
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695823
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695824
    new-instance v3, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    const-class v7, LX/JSC;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JSC;

    invoke-static {v0}, LX/JTD;->b(LX/0QB;)LX/JTD;

    move-result-object v8

    check-cast v8, LX/JTD;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;LX/JSC;LX/JTD;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V

    .line 2695825
    move-object v0, v3

    .line 2695826
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695827
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695828
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2695796
    check-cast p2, LX/JSg;

    check-cast p3, LX/1Pq;

    .line 2695797
    iget-object v6, p2, LX/JSg;->a:LX/JSe;

    .line 2695798
    const v0, 0x7f0d2d28

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2695799
    iget-object v2, v6, LX/JSe;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2695800
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695801
    const v0, 0x7f0d2d29

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v6}, LX/JSi;->a(LX/JSe;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695802
    const v0, 0x7f0d2d2a

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->b:Landroid/content/Context;

    invoke-static {v2, v6}, LX/JSi;->a(Landroid/content/Context;LX/JSe;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695803
    const v0, 0x7f0d1cdd

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v2, LX/2f8;

    invoke-direct {v2}, LX/2f8;-><init>()V

    .line 2695804
    iget-object v3, v6, LX/JSe;->f:Landroid/net/Uri;

    move-object v3, v3

    .line 2695805
    invoke-virtual {v2, v3}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2695806
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2695807
    move-object v2, v2

    .line 2695808
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695809
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->h:Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;

    .line 2695810
    iget-object v1, v6, LX/JSe;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 2695811
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695812
    const v7, 0x7f0d2c75

    iget-object v8, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->e:Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    new-instance v0, LX/JSL;

    iget-object v1, p2, LX/JSg;->a:LX/JSe;

    iget-object v2, p2, LX/JSg;->b:LX/JTY;

    iget-object v3, p2, LX/JSg;->c:LX/JSl;

    iget v4, p2, LX/JSg;->d:I

    iget-object v5, p2, LX/JSg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v0 .. v5}, LX/JSL;-><init>(LX/JSe;LX/JTY;LX/JSl;ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v7, v8, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695813
    new-instance v1, LX/JSh;

    iget-object v0, p2, LX/JSg;->a:LX/JSe;

    invoke-virtual {v0}, LX/JSe;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/JSh;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2695814
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v1, v6}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSf;

    .line 2695815
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->g:LX/JTD;

    iget-object v2, p2, LX/JSg;->b:LX/JTY;

    invoke-virtual {v1, v6, v2, p3, v0}, LX/JTD;->a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;

    move-result-object v0

    .line 2695816
    if-eqz v0, :cond_0

    .line 2695817
    const v1, 0x7f0d2d2b

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/SingleSongPartDefinition;->f:Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695818
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
