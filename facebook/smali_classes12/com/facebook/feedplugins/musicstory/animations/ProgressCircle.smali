.class public Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/RectF;

.field private c:F

.field private d:F

.field public e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2696138
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2696139
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->b()V

    .line 2696140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2696162
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2696163
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->b()V

    .line 2696164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2696159
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2696160
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->b()V

    .line 2696161
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 2696150
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b25b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    .line 2696151
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b25b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    mul-float/2addr v1, v6

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->d:F

    .line 2696152
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    .line 2696153
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2696154
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2696155
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2696156
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2696157
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    div-float/2addr v1, v6

    iget v2, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    div-float/2addr v2, v6

    iget v3, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->d:F

    iget v4, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    mul-float/2addr v4, v7

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->d:F

    iget v5, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->c:F

    mul-float/2addr v5, v7

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->b:Landroid/graphics/RectF;

    .line 2696158
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2696148
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    .line 2696149
    return-void
.end method

.method public getAngle()F
    .locals 1

    .prologue
    .line 2696147
    iget v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2696143
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2696144
    iget v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    const/high16 v1, 0x43b40000    # 360.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2696145
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->b:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    iget v3, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 2696146
    :cond_0
    return-void
.end method

.method public setAngle(F)V
    .locals 1

    .prologue
    .line 2696141
    const/high16 v0, 0x43b40000    # 360.0f

    rem-float v0, p1, v0

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/ProgressCircle;->e:F

    .line 2696142
    return-void
.end method
