.class public Lcom/facebook/feedplugins/musicstory/animations/VinylView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/Path;

.field private final b:Landroid/graphics/Path;

.field private final c:Landroid/graphics/RectF;

.field public d:Landroid/graphics/drawable/Drawable;

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2696221
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2696222
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    .line 2696223
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    .line 2696224
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    .line 2696225
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b()V

    .line 2696226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2696260
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2696261
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    .line 2696262
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    .line 2696263
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    .line 2696264
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b()V

    .line 2696265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2696254
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2696255
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    .line 2696256
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    .line 2696257
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    .line 2696258
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b()V

    .line 2696259
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2696251
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    .line 2696252
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2696253
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2696248
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->e:F

    .line 2696249
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->clearAnimation()V

    .line 2696250
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2696239
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2696240
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    .line 2696241
    :cond_0
    :goto_0
    return-void

    .line 2696242
    :cond_1
    iget v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 2696243
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 2696244
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2696245
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2696246
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    iget v2, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->e:F

    sub-float v2, v4, v2

    iget v3, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->e:F

    sub-float v3, v4, v3

    iget v4, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    mul-float/2addr v3, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 2696247
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->a:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 2696233
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 2696234
    if-nez p1, :cond_0

    .line 2696235
    :goto_0
    return-void

    .line 2696236
    :cond_0
    sub-int v0, p4, p2

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    .line 2696237
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->b:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    div-float/2addr v1, v4

    iget v2, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    div-float/2addr v2, v4

    iget v3, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    div-float/2addr v3, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 2696238
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->c:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    iget v2, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->f:F

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2696231
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->d:Landroid/graphics/drawable/Drawable;

    .line 2696232
    return-void
.end method

.method public setRectangularity(F)V
    .locals 1

    .prologue
    .line 2696227
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2696228
    iput p1, p0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->e:F

    .line 2696229
    return-void

    .line 2696230
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
