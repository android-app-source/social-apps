.class public Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JSL;",
        "LX/JSM;",
        "TE;",
        "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/JSR;

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:LX/0SG;

.field public final j:LX/JSG;

.field private final k:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2695142
    const-class v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    const-string v1, "music_story_inline_card"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;Ljava/util/concurrent/ExecutorService;LX/0SG;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/JSR;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/JSG;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695143
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2695144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->d:Ljava/util/HashMap;

    .line 2695145
    new-instance v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    new-instance v1, LX/JSJ;

    invoke-direct {v1, p0}, LX/JSJ;-><init>(Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;)V

    invoke-direct {v0, v1}, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;-><init>(LX/JSJ;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->k:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    .line 2695146
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->b:Landroid/content/Context;

    .line 2695147
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->f:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 2695148
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->h:Ljava/util/concurrent/ExecutorService;

    .line 2695149
    iput-object p5, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2695150
    iput-object p6, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->c:LX/JSR;

    .line 2695151
    iput-object p4, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->i:LX/0SG;

    .line 2695152
    iput-object p7, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 2695153
    iput-object p8, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->j:LX/JSG;

    .line 2695154
    return-void
.end method

.method private a(LX/1aD;LX/JSL;LX/1Pq;)LX/JSM;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/JSL;",
            "TE;)",
            "LX/JSM;"
        }
    .end annotation

    .prologue
    .line 2695155
    move-object/from16 v0, p2

    iget-object v4, v0, LX/JSL;->a:LX/JSe;

    move-object/from16 v1, p3

    .line 2695156
    check-cast v1, LX/1Pr;

    move-object/from16 v0, p2

    iget-object v2, v0, LX/JSL;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v4, v2}, LX/JSG;->a(LX/1Pr;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSO;

    move-result-object v2

    .line 2695157
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->c:LX/JSR;

    invoke-virtual {v1, v4}, LX/JSR;->a(LX/JSe;)I

    move-result v1

    iput v1, v2, LX/JSO;->b:I

    .line 2695158
    const v1, 0x7f0d1cdd

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v5, LX/2f8;

    invoke-direct {v5}, LX/2f8;-><init>()V

    invoke-virtual {v4}, LX/JSe;->e()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/2f8;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/2f8;

    move-result-object v5

    invoke-virtual {v5}, LX/2f8;->a()LX/2f9;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695159
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->j:LX/JSG;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/JSL;->b:LX/JTY;

    invoke-virtual {v1, v4, v3, v2}, LX/JSG;->a(LX/JSe;LX/JTY;LX/JSO;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 2695160
    const v3, 0x7f0d1ce0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695161
    const v3, 0x7f0d1cde

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695162
    const v3, 0x7f0d1cdf

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2695163
    new-instance v6, LX/JSp;

    invoke-direct {v6}, LX/JSp;-><init>()V

    .line 2695164
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->f:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v1, LX/82M;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v5, LX/JSq;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, LX/JSe;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, LX/JSq;

    invoke-direct {v5, v6}, LX/JSq;-><init>(LX/JSp;)V

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v7, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695165
    new-instance v8, LX/JSN;

    invoke-direct {v8, v4}, LX/JSN;-><init>(LX/JSe;)V

    .line 2695166
    new-instance v1, LX/JSM;

    new-instance v5, LX/JSI;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/JSL;->c:LX/JSl;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->b:Landroid/content/Context;

    move-object/from16 v9, p3

    check-cast v9, LX/1Pr;

    move-object/from16 v0, p2

    iget-object v10, v0, LX/JSL;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget v11, v0, LX/JSL;->d:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->k:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->i:LX/0SG;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->h:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p2

    iget-object v0, v0, LX/JSL;->b:LX/JTY;

    move-object/from16 v16, v0

    move-object v13, v4

    invoke-direct/range {v5 .. v16}, LX/JSI;-><init>(LX/JSl;Landroid/content/Context;LX/1KL;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;ILcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;LX/JSe;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/JTY;)V

    invoke-direct {v1, v5}, LX/JSM;-><init>(LX/JSI;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;
    .locals 12

    .prologue
    .line 2695167
    const-class v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    monitor-enter v1

    .line 2695168
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695169
    sput-object v2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695170
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695171
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695172
    new-instance v3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/JSR;->a(LX/0QB;)LX/JSR;

    move-result-object v9

    check-cast v9, LX/JSR;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/JSG;->a(LX/0QB;)LX/JSG;

    move-result-object v11

    check-cast v11, LX/JSG;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;Ljava/util/concurrent/ExecutorService;LX/0SG;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/JSR;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/JSG;)V

    .line 2695173
    move-object v0, v3

    .line 2695174
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695175
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695176
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695177
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2695178
    check-cast p2, LX/JSL;

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->a(LX/1aD;LX/JSL;LX/1Pq;)LX/JSM;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2dfe7c4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2695179
    check-cast p1, LX/JSL;

    check-cast p2, LX/JSM;

    check-cast p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    .line 2695180
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->j:LX/JSG;

    iget-object v2, p1, LX/JSL;->a:LX/JSe;

    iget-object p3, p2, LX/JSM;->a:LX/JSI;

    invoke-virtual {v1, v2, p3}, LX/JSG;->a(LX/JSe;LX/JSI;)V

    .line 2695181
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->d:Ljava/util/HashMap;

    iget-object v2, p1, LX/JSL;->a:LX/JSe;

    .line 2695182
    iget-object p3, v2, LX/JSe;->h:Landroid/net/Uri;

    move-object v2, p3

    .line 2695183
    invoke-virtual {v1, v2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2695184
    const/16 v1, 0x1f

    const v2, 0x3186652b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2695185
    check-cast p1, LX/JSL;

    check-cast p2, LX/JSM;

    check-cast p3, LX/1Pq;

    check-cast p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    .line 2695186
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->j:LX/JSG;

    move-object v1, p3

    check-cast v1, LX/1Pr;

    iget-object v2, p1, LX/JSL;->a:LX/JSe;

    iget-object v3, p2, LX/JSM;->a:LX/JSI;

    iget-object v4, p1, LX/JSL;->b:LX/JTY;

    iget-object v5, p1, LX/JSL;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p1, LX/JSL;->c:LX/JSl;

    iget v7, p1, LX/JSL;->d:I

    move-object v8, p4

    invoke-virtual/range {v0 .. v8}, LX/JSG;->a(LX/1Pr;LX/JSe;LX/JSI;LX/JTY;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSl;ILcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    .line 2695187
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackPartDefinition;->d:Ljava/util/HashMap;

    iget-object v1, p1, LX/JSL;->a:LX/JSe;

    .line 2695188
    iget-object v2, v1, LX/JSe;->h:Landroid/net/Uri;

    move-object v1, v2

    .line 2695189
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2695190
    return-void
.end method
