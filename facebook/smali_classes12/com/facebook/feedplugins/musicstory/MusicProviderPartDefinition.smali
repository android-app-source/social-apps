.class public Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JT0;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695346
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2695347
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2695348
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->b:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    .line 2695349
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2695350
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;
    .locals 6

    .prologue
    .line 2695351
    const-class v1, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    monitor-enter v1

    .line 2695352
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695353
    sput-object v2, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695354
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695355
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695356
    new-instance p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2695357
    move-object v0, p0

    .line 2695358
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695359
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695360
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2695362
    check-cast p2, LX/JT0;

    const/4 v3, 0x0

    .line 2695363
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p2}, LX/JT0;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695364
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p2}, LX/JT0;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695365
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicProviderPartDefinition;->b:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    new-instance v1, LX/2eV;

    invoke-interface {p2}, LX/JT0;->c()I

    move-result v2

    invoke-direct {v1, v2, v3}, LX/2eV;-><init>(ILjava/lang/Integer;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2695366
    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4cec4b97    # 1.23886776E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2695367
    check-cast p1, LX/JT0;

    check-cast p4, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2695368
    invoke-interface {p1}, LX/JT0;->d()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setBackgroundResource(I)V

    .line 2695369
    const/16 v1, 0x1f

    const v2, -0x28bda6a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
