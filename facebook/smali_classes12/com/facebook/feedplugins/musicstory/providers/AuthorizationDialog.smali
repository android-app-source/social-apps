.class public Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public n:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/JTE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/webkit/WebView;

.field public q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2696368
    const-class v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2696389
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2696390
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-static {p0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object p0

    check-cast p0, LX/48V;

    iput-object p0, p1, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->n:LX/48V;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16a6bda0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2696385
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2696386
    const-class v1, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-static {v1, p0}, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 2696387
    const/4 v1, 0x0

    const v2, 0x7f0e0c33

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2696388
    const/16 v1, 0x2b

    const v2, 0x31588e84

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v0, 0x2a

    const v1, -0x6d6d1708

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2696369
    const v0, 0x7f030147

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2696370
    const v0, 0x7f0d0614

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2696371
    const v0, 0x7f0d0615

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->p:Landroid/webkit/WebView;

    .line 2696372
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2696373
    const-string v0, "https://accounts.spotify.com/authorize"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2696374
    const-string v3, "client_id"

    const-string v4, "9cc4aaeb43f24b098cff096385f00233"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696375
    const-string v3, "response_type"

    const-string v4, "code"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696376
    const-string v3, "show_dialog"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696377
    const-string v3, "redirect_uri"

    const-string v4, "https://m.facebook.com/spotify_callback"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696378
    iget-object v3, p0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->p:Landroid/webkit/WebView;

    .line 2696379
    new-instance v4, LX/JT4;

    invoke-direct {v4, p0}, LX/JT4;-><init>(Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;)V

    move-object v4, v4

    .line 2696380
    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2696381
    new-instance v3, LX/48U;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, LX/48U;-><init>(Ljava/lang/String;)V

    .line 2696382
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->p:Landroid/webkit/WebView;

    .line 2696383
    invoke-virtual {v3}, LX/48U;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 2696384
    const/16 v0, 0x2b

    const v3, -0x2cd9fa7b

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
