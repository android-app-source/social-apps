.class public final Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2696700
    const-class v0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;

    new-instance v1, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2696701
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2696702
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2696691
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2696692
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2696693
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2696694
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2696695
    if-eqz p0, :cond_0

    .line 2696696
    const-string p2, "save_successful"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2696697
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2696698
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2696699
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2696690
    check-cast p1, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;->a(Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
