.class public final Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x69f90a5f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2696704
    const-class v0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2696703
    const-class v0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2696723
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2696724
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2696718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2696719
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2696720
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2696721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2696722
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2696715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2696716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2696717
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2696712
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2696713
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;->e:Z

    .line 2696714
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2696710
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2696711
    iget-boolean v0, p0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2696707
    new-instance v0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;-><init>()V

    .line 2696708
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2696709
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2696706
    const v0, 0x472fffbe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2696705
    const v0, 0x7db13f81

    return v0
.end method
