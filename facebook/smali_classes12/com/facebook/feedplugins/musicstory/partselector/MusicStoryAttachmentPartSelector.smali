.class public Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

.field private final b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

.field private final c:Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition",
            "<*",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696288
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2696289
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->c:Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    .line 2696290
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->d:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    .line 2696291
    iput-object p4, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    .line 2696292
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    .line 2696293
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;
    .locals 7

    .prologue
    .line 2696298
    const-class v1, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;

    monitor-enter v1

    .line 2696299
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2696300
    sput-object v2, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2696301
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2696302
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2696303
    new-instance p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;-><init>(Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;)V

    .line 2696304
    move-object v0, p0

    .line 2696305
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2696306
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2696307
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2696308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2696309
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2696310
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->c:Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->d:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2696311
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2696294
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2696295
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2696296
    const/4 v0, 0x1

    move v0, v0

    .line 2696297
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
