.class public Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Landroid/net/Uri;",
        "LX/JSB;",
        "LX/1PW;",
        "LX/JSZ;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1Ad;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;

.field private final c:LX/JS9;


# direct methods
.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;LX/1Ad;LX/JSA;)V
    .locals 1
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694791
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2694792
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->a:LX/1Ad;

    .line 2694793
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2694794
    const/16 v0, 0x14

    invoke-static {v0}, LX/JSA;->a(I)LX/JS9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->c:LX/JS9;

    .line 2694795
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2694796
    check-cast p2, Landroid/net/Uri;

    .line 2694797
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->c:LX/JS9;

    .line 2694798
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 2694799
    move-object v0, v0

    .line 2694800
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2694801
    new-instance v1, LX/JSB;

    invoke-direct {v1, v0}, LX/JSB;-><init>(LX/1bf;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x303ab7e6    # -6.6196736E9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694802
    check-cast p1, Landroid/net/Uri;

    check-cast p2, LX/JSB;

    check-cast p4, LX/JSZ;

    .line 2694803
    iget-object v1, p4, LX/JSZ;->c:Landroid/net/Uri;

    move-object v1, v1

    .line 2694804
    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2694805
    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->a:LX/1Ad;

    iget-object v2, p2, LX/JSB;->a:LX/1bf;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    .line 2694806
    iget-object v2, p4, LX/JSZ;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    move-object v2, v2

    .line 2694807
    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/BlurredImagePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2694808
    iget-object v2, p4, LX/JSZ;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2694809
    iput-object p1, p4, LX/JSZ;->c:Landroid/net/Uri;

    .line 2694810
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x19fb5902

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
