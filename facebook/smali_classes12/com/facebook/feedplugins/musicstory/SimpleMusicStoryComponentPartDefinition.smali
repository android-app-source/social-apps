.class public Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/JSX;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JSX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695514
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2695515
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->d:LX/JSX;

    .line 2695516
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2695498
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->d:LX/JSX;

    const/4 v1, 0x0

    .line 2695499
    new-instance v2, LX/JSW;

    invoke-direct {v2, v0}, LX/JSW;-><init>(LX/JSX;)V

    .line 2695500
    iget-object p0, v0, LX/JSX;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JSV;

    .line 2695501
    if-nez p0, :cond_0

    .line 2695502
    new-instance p0, LX/JSV;

    invoke-direct {p0, v0}, LX/JSV;-><init>(LX/JSX;)V

    .line 2695503
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JSV;->a$redex0(LX/JSV;LX/1De;IILX/JSW;)V

    .line 2695504
    move-object v2, p0

    .line 2695505
    move-object v1, v2

    .line 2695506
    move-object v0, v1

    .line 2695507
    iget-object v1, v0, LX/JSV;->a:LX/JSW;

    iput-object p2, v1, LX/JSW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695508
    iget-object v1, v0, LX/JSV;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695509
    move-object v0, v0

    .line 2695510
    iget-object v1, v0, LX/JSV;->a:LX/JSW;

    iput-object p3, v1, LX/JSW;->b:LX/1Pn;

    .line 2695511
    iget-object v1, v0, LX/JSV;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695512
    move-object v0, v0

    .line 2695513
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;
    .locals 5

    .prologue
    .line 2695487
    const-class v1, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    monitor-enter v1

    .line 2695488
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695489
    sput-object v2, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695490
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695491
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695492
    new-instance p0, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JSX;->a(LX/0QB;)LX/JSX;

    move-result-object v4

    check-cast v4, LX/JSX;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;-><init>(Landroid/content/Context;LX/JSX;)V

    .line 2695493
    move-object v0, p0

    .line 2695494
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695495
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695496
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695497
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2695484
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2695485
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2695486
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x48d0bc60

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ah()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2695517
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2695483
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2695482
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/musicstory/SimpleMusicStoryComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2695479
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695480
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2695481
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
