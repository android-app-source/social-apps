.class public Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/1vg;

.field public final d:LX/JTD;

.field private final e:LX/JSE;

.field private final f:LX/JS9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2695755
    const-class v0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;LX/JTD;LX/JSE;LX/JSA;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2695745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695746
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->b:LX/1nu;

    .line 2695747
    iput-object p2, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->c:LX/1vg;

    .line 2695748
    iput-object p3, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->d:LX/JTD;

    .line 2695749
    iput-object p4, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->e:LX/JSE;

    .line 2695750
    const/16 v0, 0x14

    invoke-static {v0}, LX/JSA;->a(I)LX/JS9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->f:LX/JS9;

    .line 2695751
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/1Pn;LX/JSe;)LX/1Dg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/JSe;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2695752
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->b:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    .line 2695753
    iget-object v1, p3, LX/JSe;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 2695754
    invoke-virtual {v0, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->f:LX/JS9;

    invoke-virtual {v0, v1}, LX/1nw;->a(LX/33B;)LX/1nw;

    move-result-object v0

    check-cast p2, LX/1Pp;

    invoke-virtual {v0, p2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/1Di;->k(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pn;)LX/1Dg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/JSe;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JTY;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2695687
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const/16 v2, 0xc

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-static/range {p0 .. p5}, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->b(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pn;)LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    const/4 p3, 0x0

    const/4 v4, 0x1

    .line 2695688
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0e012a

    invoke-static {p1, p3, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    .line 2695689
    iget-object v3, p2, LX/JSe;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2695690
    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0e0121

    invoke-static {p1, p3, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    invoke-static {p2}, LX/JSi;->a(LX/JSe;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0e012c

    invoke-static {p1, p3, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p2}, LX/JSi;->a(Landroid/content/Context;LX/JSe;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const/high16 v3, 0x41100000    # 9.0f

    invoke-virtual {v2, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 2695691
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->d:LX/JTD;

    check-cast p5, LX/1Pq;

    new-instance v3, LX/JSf;

    invoke-direct {v3}, LX/JSf;-><init>()V

    invoke-virtual {v2, p2, p4, p5, v3}, LX/JTD;->a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;

    move-result-object v2

    .line 2695692
    if-eqz v2, :cond_1

    .line 2695693
    iget-object v3, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->c:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    invoke-interface {v2}, LX/JT0;->d()I

    move-result v4

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2695694
    invoke-static {p1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object v4

    invoke-interface {v2}, LX/JT0;->a()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v4, p3}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object v4

    const p3, 0x7f0e012c

    invoke-virtual {v4, p3}, LX/8yk;->i(I)LX/8yk;

    move-result-object v4

    .line 2695695
    iget-object p3, v4, LX/8yk;->a:LX/8yl;

    iput-object v3, p3, LX/8yl;->d:LX/1dc;

    .line 2695696
    move-object v3, v4

    .line 2695697
    invoke-interface {v2}, LX/JT0;->c()I

    move-result v4

    if-eqz v4, :cond_0

    .line 2695698
    iget-object v4, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->c:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    invoke-interface {v2}, LX/JT0;->c()I

    move-result v2

    invoke-virtual {v4, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const v4, 0x7f0a0097

    invoke-virtual {v2, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    .line 2695699
    iget-object v4, v3, LX/8yk;->a:LX/8yl;

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object p3

    iput-object p3, v4, LX/8yl;->c:LX/1dc;

    .line 2695700
    :cond_0
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2695701
    const v3, -0x58d52a2

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2695702
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    const v4, 0x7f0b25ba

    invoke-interface {v2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b25b9

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b25b8

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 2695703
    :goto_0
    move-object v2, v2

    .line 2695704
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2695705
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 2695706
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;
    .locals 9

    .prologue
    .line 2695707
    const-class v1, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;

    monitor-enter v1

    .line 2695708
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695709
    sput-object v2, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695710
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695711
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695712
    new-instance v3, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/JTD;->b(LX/0QB;)LX/JTD;

    move-result-object v6

    check-cast v6, LX/JTD;

    invoke-static {v0}, LX/JSE;->a(LX/0QB;)LX/JSE;

    move-result-object v7

    check-cast v7, LX/JSE;

    const-class v8, LX/JSA;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/JSA;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;-><init>(LX/1nu;LX/1vg;LX/JTD;LX/JSE;LX/JSA;)V

    .line 2695713
    move-object v0, v3

    .line 2695714
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695715
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695716
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pn;)LX/1Dg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/JSe;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JTY;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2695718
    iget-object v0, p0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->e:LX/JSE;

    const/4 v1, 0x0

    .line 2695719
    new-instance v2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;-><init>(LX/JSE;)V

    .line 2695720
    iget-object p0, v0, LX/JSE;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JSD;

    .line 2695721
    if-nez p0, :cond_0

    .line 2695722
    new-instance p0, LX/JSD;

    invoke-direct {p0, v0}, LX/JSD;-><init>(LX/JSE;)V

    .line 2695723
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JSD;->a$redex0(LX/JSD;LX/1De;IILcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;)V

    .line 2695724
    move-object v2, p0

    .line 2695725
    move-object v1, v2

    .line 2695726
    move-object v0, v1

    .line 2695727
    iget-object v1, v0, LX/JSD;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    iput-object p2, v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    .line 2695728
    iget-object v1, v0, LX/JSD;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695729
    move-object v0, v0

    .line 2695730
    iget-object v1, v0, LX/JSD;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    iput-object p3, v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695731
    iget-object v1, v0, LX/JSD;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695732
    move-object v0, v0

    .line 2695733
    iget-object v1, v0, LX/JSD;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    iput-object p4, v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    .line 2695734
    iget-object v1, v0, LX/JSD;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695735
    move-object v0, v0

    .line 2695736
    check-cast p5, LX/1Pq;

    .line 2695737
    iget-object v1, v0, LX/JSD;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    iput-object p5, v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    .line 2695738
    iget-object v1, v0, LX/JSD;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695739
    move-object v0, v0

    .line 2695740
    sget-object v1, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2695741
    iget-object v2, v0, LX/JSD;->a:Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    .line 2695742
    iget-object v2, v0, LX/JSD;->e:Ljava/util/BitSet;

    const/4 p0, 0x4

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2695743
    move-object v0, v0

    .line 2695744
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b25b0

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b25b0

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
