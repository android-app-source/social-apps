.class public final Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JSE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JSe;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/JTY;

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:LX/JSI;

.field public f:LX/82M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/82M",
            "<",
            "LX/JSO;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/82L;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/82L",
            "<",
            "LX/JSO;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/view/View$OnClickListener;

.field public i:LX/JSO;

.field public j:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic k:LX/JSE;


# direct methods
.method public constructor <init>(LX/JSE;)V
    .locals 1

    .prologue
    .line 2694835
    iput-object p1, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->k:LX/JSE;

    .line 2694836
    move-object v0, p1

    .line 2694837
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2694838
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2694839
    const-string v0, "MusicPlaybackComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/JSE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2694840
    check-cast p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694841
    iget-object v0, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    .line 2694842
    iget-object v0, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->f:LX/82M;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->f:LX/82M;

    .line 2694843
    iget-object v0, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    .line 2694844
    iget-object v0, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->h:Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->h:Landroid/view/View$OnClickListener;

    .line 2694845
    iget-object v0, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->i:LX/JSO;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->i:LX/JSO;

    .line 2694846
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2694847
    if-ne p0, p1, :cond_1

    .line 2694848
    :cond_0
    :goto_0
    return v0

    .line 2694849
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2694850
    goto :goto_0

    .line 2694851
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694852
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2694853
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2694854
    if-eq v2, v3, :cond_0

    .line 2694855
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    iget-object v3, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2694856
    goto :goto_0

    .line 2694857
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    if-nez v2, :cond_4

    .line 2694858
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2694859
    goto :goto_0

    .line 2694860
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2694861
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    iget-object v3, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2694862
    goto :goto_0

    .line 2694863
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    if-nez v2, :cond_a

    .line 2694864
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    iget-object v3, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2694865
    goto :goto_0

    .line 2694866
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    if-nez v2, :cond_d

    .line 2694867
    :cond_f
    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2694868
    goto :goto_0

    .line 2694869
    :cond_10
    iget-object v2, p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2694870
    const/4 v1, 0x0

    .line 2694871
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694872
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    .line 2694873
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->f:LX/82M;

    .line 2694874
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    .line 2694875
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->h:Landroid/view/View$OnClickListener;

    .line 2694876
    iput-object v1, v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->i:LX/JSO;

    .line 2694877
    return-object v0
.end method
