.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687473
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2687474
    iput-object p1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->b:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    .line 2687475
    iput-object p2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->a:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    .line 2687476
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;
    .locals 5

    .prologue
    .line 2687477
    const-class v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;

    monitor-enter v1

    .line 2687478
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687479
    sput-object v2, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687480
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687481
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687482
    new-instance p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;-><init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;)V

    .line 2687483
    move-object v0, p0

    .line 2687484
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687485
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687486
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2687488
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687489
    iget-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->b:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2687490
    iget-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoPartDefinition;->a:Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2687491
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2687492
    const/4 v0, 0x1

    return v0
.end method
