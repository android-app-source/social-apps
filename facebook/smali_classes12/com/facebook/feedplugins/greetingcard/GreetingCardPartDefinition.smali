.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JOK;",
        "LX/JOL;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/greetingcard/GreetingCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/JOE;

.field public final c:LX/03V;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2687272
    const-class v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    sput-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/util/concurrent/Executor;LX/JOE;LX/03V;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687273
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2687274
    iput-object p3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->b:LX/JOE;

    .line 2687275
    iput-object p4, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->c:LX/03V;

    .line 2687276
    iput-object p1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->e:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    .line 2687277
    iput-object p2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->d:Ljava/util/concurrent/Executor;

    .line 2687278
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;
    .locals 7

    .prologue
    .line 2687279
    const-class v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    monitor-enter v1

    .line 2687280
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687281
    sput-object v2, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687282
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687283
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687284
    new-instance p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a(LX/0QB;)Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    move-result-object v3

    check-cast v3, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/JOE;->a(LX/0QB;)LX/JOE;

    move-result-object v5

    check-cast v5, LX/JOE;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;-><init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/util/concurrent/Executor;LX/JOE;LX/03V;)V

    .line 2687285
    move-object v0, p0

    .line 2687286
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687287
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687288
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;Landroid/content/Context;Lcom/facebook/greetingcards/model/GreetingCard;Ljava/lang/String;)Lcom/facebook/greetingcards/render/RenderCardFragment;
    .locals 3

    .prologue
    .line 2687290
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2687291
    new-instance v1, LX/GjV;

    invoke-direct {v1}, LX/GjV;-><init>()V

    .line 2687292
    const v2, 0x7f0835f1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const p0, 0x7f0835f2

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/GjU;->a(LX/0Px;)LX/GjU;

    .line 2687293
    if-eqz p2, :cond_0

    .line 2687294
    invoke-virtual {v1, p2}, LX/GjU;->a(Lcom/facebook/greetingcards/model/GreetingCard;)LX/GjU;

    .line 2687295
    :cond_0
    iget-object v0, v1, LX/GjU;->b:Landroid/os/Bundle;

    const-string v2, "args_source"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2687296
    invoke-virtual {v1}, LX/GjU;->b()Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v0

    move-object v0, v0

    .line 2687297
    return-object v0
.end method

.method public static a(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/JOL;Landroid/content/Context;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 2687298
    invoke-static {p2}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 2687299
    iget-object v1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->e:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    .line 2687300
    sget-object v9, LX/0zS;->b:LX/0zS;

    move-object v4, v1

    move-object v5, p3

    move v6, v2

    move v7, v0

    move-object v8, v3

    invoke-static/range {v4 .. v9}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a$redex0(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/lang/String;IILX/0wC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2687301
    move-object v0, v4

    .line 2687302
    new-instance v1, LX/JOI;

    invoke-direct {v1, p0, p1}, LX/JOI;-><init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/JOL;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2687303
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2687304
    check-cast p2, LX/JOK;

    const/4 v1, 0x0

    .line 2687305
    iget-object v2, p2, LX/JOK;->a:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-static {v2}, LX/JOE;->a(Lcom/facebook/graphql/model/GraphQLGreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v3

    .line 2687306
    iget-object v0, p2, LX/JOK;->a:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->n()Ljava/lang/String;

    move-result-object v4

    .line 2687307
    iget-object v0, p2, LX/JOK;->a:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->j()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    .line 2687308
    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplateTheme;

    .line 2687309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplateTheme;->j()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2687310
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "#"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardTemplateTheme;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2687311
    :goto_1
    new-instance v1, LX/JOL;

    invoke-direct {v1, v0}, LX/JOL;-><init>(I)V

    .line 2687312
    new-instance v0, LX/JOH;

    invoke-direct {v0, p0, p2, v3, v1}, LX/JOH;-><init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/JOK;Lcom/facebook/greetingcards/model/GreetingCard;LX/JOL;)V

    .line 2687313
    iput-object v0, v1, LX/JOL;->c:Landroid/view/View$OnClickListener;

    .line 2687314
    return-object v1

    .line 2687315
    :catch_0
    move-exception v0

    .line 2687316
    iget-object v2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->c:LX/03V;

    const-string v4, "feedplugins.greetingcard.binder"

    const-string v5, "Failed to parse GreetingCard attachment style title text color"

    invoke-virtual {v2, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 2687317
    goto :goto_1

    .line 2687318
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x53ee37db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2687319
    check-cast p1, LX/JOK;

    check-cast p2, LX/JOL;

    check-cast p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;

    const/4 v2, 0x0

    .line 2687320
    iget-object v4, p1, LX/JOK;->a:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-static {v4}, LX/JOE;->a(Lcom/facebook/graphql/model/GraphQLGreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v4

    .line 2687321
    iget-object v1, p1, LX/JOK;->a:Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGreetingCard;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 2687322
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 2687323
    :goto_0
    iget-object p3, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p1, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2687324
    iget-object v1, v4, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    .line 2687325
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 2687326
    iget-object p3, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 p1, 0x4

    invoke-virtual {p3, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2687327
    :goto_1
    iget v1, p2, LX/JOL;->b:I

    .line 2687328
    if-eqz v1, :cond_0

    .line 2687329
    iget-object p3, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2687330
    :cond_0
    iget-object v1, v4, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v1, :cond_1

    iget-object v1, v4, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, v4, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2687331
    iget-object v1, v4, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    iget-object v1, v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/model/CardPhoto;

    move-object v2, v1

    .line 2687332
    :cond_1
    if-nez v2, :cond_5

    .line 2687333
    iget-object v1, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p3, 0x4

    invoke-virtual {v1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2687334
    :goto_2
    iget-object v1, p2, LX/JOL;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2687335
    iget-object v1, p2, LX/JOL;->a:LX/Gk2;

    if-nez v1, :cond_2

    .line 2687336
    invoke-virtual {p4}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v4, Lcom/facebook/greetingcards/model/GreetingCard;->d:Ljava/lang/String;

    invoke-static {p0, p2, v1, v2}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/JOL;Landroid/content/Context;Ljava/lang/String;)V

    .line 2687337
    :cond_2
    const/16 v1, 0x1f

    const v2, -0x7a4c07ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    move-object v1, v2

    .line 2687338
    goto :goto_0

    .line 2687339
    :cond_4
    iget-object p3, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 p1, 0x0

    invoke-virtual {p3, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2687340
    iget-object p3, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2687341
    :cond_5
    iget-object v1, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p3, 0x0

    invoke-virtual {v1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2687342
    iget-object v1, v2, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_6

    .line 2687343
    iget-object v1, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    iget-object p3, v2, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    invoke-virtual {v1, p3}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2687344
    :cond_6
    iget-object v1, p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p3, v2, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    sget-object p1, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p3, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2687345
    check-cast p4, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;

    .line 2687346
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2687347
    return-void
.end method
