.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JOO;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JOO;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687420
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2687421
    iput-object p2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->d:LX/JOO;

    .line 2687422
    iput-object p3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2687423
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2687438
    iget-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->d:LX/JOO;

    const/4 v1, 0x0

    .line 2687439
    new-instance v2, LX/JON;

    invoke-direct {v2, v0}, LX/JON;-><init>(LX/JOO;)V

    .line 2687440
    sget-object v3, LX/JOO;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOM;

    .line 2687441
    if-nez v3, :cond_0

    .line 2687442
    new-instance v3, LX/JOM;

    invoke-direct {v3}, LX/JOM;-><init>()V

    .line 2687443
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JOM;->a$redex0(LX/JOM;LX/1De;IILX/JON;)V

    .line 2687444
    move-object v2, v3

    .line 2687445
    move-object v1, v2

    .line 2687446
    move-object v0, v1

    .line 2687447
    iget-object v1, v0, LX/JOM;->a:LX/JON;

    iput-object p2, v1, LX/JON;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687448
    iget-object v1, v0, LX/JOM;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2687449
    move-object v1, v0

    .line 2687450
    move-object v0, p3

    check-cast v0, LX/1Pk;

    invoke-interface {v0}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    .line 2687451
    iget-object v2, v1, LX/JOM;->a:LX/JON;

    iput-object v0, v2, LX/JON;->b:LX/1SX;

    .line 2687452
    iget-object v2, v1, LX/JOM;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2687453
    move-object v0, v1

    .line 2687454
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2687455
    iget-object v1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2687427
    const-class v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2687428
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687429
    sput-object v2, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687430
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687431
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687432
    new-instance p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JOO;->a(LX/0QB;)LX/JOO;

    move-result-object v4

    check-cast v4, LX/JOO;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JOO;LX/1V0;)V

    .line 2687433
    move-object v0, p0

    .line 2687434
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687435
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687436
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687437
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2687456
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2687426
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2687425
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2687424
    const/4 v0, 0x0

    return-object v0
.end method
