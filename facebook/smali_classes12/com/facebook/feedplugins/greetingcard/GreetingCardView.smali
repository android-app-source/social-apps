.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2687512
    const-class v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;

    const-string v1, "greeting_cards"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2687493
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2687494
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    .line 2687495
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 p1, -0x2

    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 2687496
    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2687497
    const v0, 0x7f0307f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2687498
    const v0, 0x7f0d14e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2687499
    const v0, 0x7f0d14eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2687500
    const v0, 0x7f0d14ec    # 1.8752978E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2687501
    const v0, 0x7f0d14e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020c2b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2687502
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 2687503
    iget-object v0, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->d:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, "sans-serif-light"

    const/4 p1, 0x0

    invoke-static {v1, p1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2687504
    :cond_0
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 2687505
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2687506
    invoke-virtual {p0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->getMeasuredWidth()I

    move-result v0

    .line 2687507
    invoke-virtual {p0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardView;->getMeasuredHeight()I

    move-result v1

    .line 2687508
    int-to-float v2, v0

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2687509
    if-le v2, v1, :cond_0

    .line 2687510
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2687511
    :cond_0
    return-void
.end method
