.class public final Lcom/facebook/dialtone/cachemanager/DialtoneCacheManagerStateChangedListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1Ye;


# direct methods
.method public constructor <init>(LX/1Ye;)V
    .locals 0

    .prologue
    .line 2521445
    iput-object p1, p0, Lcom/facebook/dialtone/cachemanager/DialtoneCacheManagerStateChangedListener$1;->a:LX/1Ye;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 2521446
    iget-object v0, p0, Lcom/facebook/dialtone/cachemanager/DialtoneCacheManagerStateChangedListener$1;->a:LX/1Ye;

    iget-object v0, v0, LX/1Ye;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hwp;

    .line 2521447
    iget-object v1, v0, LX/Hwp;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->w:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2521448
    iget-object v4, v0, LX/Hwp;->f:LX/0si;

    invoke-interface {v4}, LX/0c5;->clearUserData()V

    .line 2521449
    const/4 v10, 0x1

    .line 2521450
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2521451
    const-string v4, "clearCacheResetFeedLoader"

    invoke-virtual {v6, v4, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2521452
    iget-object v4, v0, LX/Hwp;->a:LX/0aG;

    const-string v5, "feed_clear_cache"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x2eba8f7c    # -5.2999766E10f

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 2521453
    iget-object v4, v0, LX/Hwp;->e:LX/Fqw;

    invoke-virtual {v4}, LX/Fqw;->b()V

    .line 2521454
    iget-object v4, v0, LX/Hwp;->c:LX/2AD;

    invoke-virtual {v4}, LX/2AG;->d()V

    .line 2521455
    iget-object v4, v0, LX/Hwp;->d:LX/2AD;

    invoke-virtual {v4}, LX/2AG;->d()V

    .line 2521456
    iget-object v4, v0, LX/Hwp;->g:LX/3Ek;

    invoke-virtual {v4}, LX/0Tr;->f()V

    .line 2521457
    iget-object v4, v0, LX/Hwp;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bl3;

    invoke-virtual {v4}, LX/0Tr;->f()V

    .line 2521458
    iget-object v4, v0, LX/Hwp;->h:LX/FS8;

    invoke-virtual {v4}, LX/0Tr;->f()V

    .line 2521459
    iget-object v1, v0, LX/Hwp;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dQ;->w:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2521460
    :cond_0
    return-void
.end method
