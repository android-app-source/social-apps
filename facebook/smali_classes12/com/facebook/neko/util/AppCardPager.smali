.class public Lcom/facebook/neko/util/AppCardPager;
.super Landroid/support/v4/view/ViewPager;
.source ""


# instance fields
.field private a:F

.field private b:F

.field public c:LX/Iv7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2627433
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 2627434
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2627435
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2627436
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, 0x41a00000    # 20.0f

    .line 2627437
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 2627438
    if-nez v1, :cond_2

    .line 2627439
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/neko/util/AppCardPager;->a:F

    .line 2627440
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/neko/util/AppCardPager;->b:F

    .line 2627441
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    .line 2627442
    :cond_2
    if-ne v1, v0, :cond_0

    .line 2627443
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 2627444
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 2627445
    iget v3, p0, Lcom/facebook/neko/util/AppCardPager;->a:F

    sub-float v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2627446
    iget v3, p0, Lcom/facebook/neko/util/AppCardPager;->b:F

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float v3, v1, v4

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2627447
    iget-object v0, p0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    sget-object v1, LX/Iv7;->Apps:LX/Iv7;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppCardPager;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2627448
    invoke-direct {p0, p1}, Lcom/facebook/neko/util/AppCardPager;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2627449
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2627450
    move v0, v1

    move v2, v1

    .line 2627451
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppCardPager;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2627452
    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppCardPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2627453
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, p1, v4}, Landroid/view/View;->measure(II)V

    .line 2627454
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 2627455
    if-le v3, v2, :cond_0

    move v2, v3

    .line 2627456
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2627457
    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2627458
    invoke-super {p0, p1, v0}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    .line 2627459
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x69eb313b

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2627460
    iget-object v0, p0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    sget-object v2, LX/Iv7;->Apps:LX/Iv7;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppCardPager;->getChildCount()I

    move-result v0

    if-le v0, v4, :cond_0

    .line 2627461
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x68448cd2

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2627462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const v2, 0x7040b632

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAppUnitState(LX/Iv7;)V
    .locals 0

    .prologue
    .line 2627463
    iput-object p1, p0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    .line 2627464
    return-void
.end method
