.class public Lcom/facebook/neko/util/AppShimmerFrameLayout;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final c:Landroid/graphics/PorterDuffXfermode;


# instance fields
.field public a:Landroid/animation/ValueAnimator;

.field public b:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:LX/Iv1;

.field public g:LX/Iv4;

.field private h:Landroid/graphics/Bitmap;

.field private i:Landroid/graphics/Bitmap;

.field public j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field public q:Z

.field private r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2627651
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    sput-object v0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->c:Landroid/graphics/PorterDuffXfermode;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2627652
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2627653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2627654
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2627655
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2627656
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2627657
    invoke-virtual {p0, v2}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setWillNotDraw(Z)V

    .line 2627658
    new-instance v0, LX/Iv1;

    invoke-direct {v0}, LX/Iv1;-><init>()V

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    .line 2627659
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->d:Landroid/graphics/Paint;

    .line 2627660
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    .line 2627661
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2627662
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 2627663
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2627664
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    sget-object v1, Lcom/facebook/neko/util/AppShimmerFrameLayout;->c:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2627665
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->c()V

    .line 2627666
    if-eqz p2, :cond_f

    .line 2627667
    sget-object v0, LX/03r;->NekoShimmerFrameLayout:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2627668
    :try_start_0
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627669
    const/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setAutoStart(Z)V

    .line 2627670
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2627671
    const/16 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setBaseAlpha(F)V

    .line 2627672
    :cond_1
    const/16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2627673
    const/16 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setDuration(I)V

    .line 2627674
    :cond_2
    const/16 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2627675
    const/16 v0, 0x3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatCount(I)V

    .line 2627676
    :cond_3
    const/16 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2627677
    const/16 v0, 0x4

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatDelay(I)V

    .line 2627678
    :cond_4
    const/16 v0, 0x5

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2627679
    const/16 v0, 0x5

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatMode(I)V

    .line 2627680
    :cond_5
    const/16 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2627681
    const/16 v0, 0x6

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2627682
    sparse-switch v0, :sswitch_data_0

    .line 2627683
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv2;->CW_0:LX/Iv2;

    iput-object v2, v0, LX/Iv1;->a:LX/Iv2;

    .line 2627684
    :cond_6
    :goto_0
    const/16 v0, 0xd

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2627685
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2627686
    packed-switch v0, :pswitch_data_0

    .line 2627687
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv3;->LINEAR:LX/Iv3;

    iput-object v2, v0, LX/Iv1;->i:LX/Iv3;

    .line 2627688
    :cond_7
    :goto_1
    const/16 v0, 0x7

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2627689
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/Iv1;->c:F

    .line 2627690
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2627691
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, v0, LX/Iv1;->d:I

    .line 2627692
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2627693
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, v0, LX/Iv1;->e:I

    .line 2627694
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2627695
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/Iv1;->f:F

    .line 2627696
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2627697
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/Iv1;->g:F

    .line 2627698
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2627699
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/Iv1;->h:F

    .line 2627700
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2627701
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/Iv1;->b:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2627702
    :cond_e
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2627703
    :cond_f
    return-void

    .line 2627704
    :sswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv2;->CW_90:LX/Iv2;

    iput-object v2, v0, LX/Iv1;->a:LX/Iv2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 2627705
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 2627706
    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv2;->CW_180:LX/Iv2;

    iput-object v2, v0, LX/Iv1;->a:LX/Iv2;

    goto/16 :goto_0

    .line 2627707
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv2;->CW_270:LX/Iv2;

    iput-object v2, v0, LX/Iv1;->a:LX/Iv2;

    goto/16 :goto_0

    .line 2627708
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v2, LX/Iv3;->RADIAL:LX/Iv3;

    iput-object v2, v0, LX/Iv1;->i:LX/Iv3;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 2627709
    invoke-static {p0, p2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private static a(II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 2627710
    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b(II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2627711
    :goto_0
    return-object v0

    .line 2627712
    :catch_0
    const-string v0, "ShimmerFrameLayout failed to create working bitmap"

    .line 2627713
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2627714
    const-string v0, " (width = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627715
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2627716
    const-string v0, ", height = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627717
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2627718
    const-string v0, ")\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627719
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2627720
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627721
    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627722
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2627723
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2627724
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2627725
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2627726
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 2627727
    :cond_0
    const/4 v0, 0x0

    .line 2627728
    :goto_0
    return v0

    .line 2627729
    :cond_1
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2627730
    invoke-super {p0, v2}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2627731
    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2627732
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->c(Landroid/graphics/Canvas;)V

    .line 2627733
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v3, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2627734
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2627735
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2627736
    :goto_0
    return-object v0

    .line 2627737
    :catch_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2627738
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 2627739
    invoke-virtual {p0, v2}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setAutoStart(Z)V

    .line 2627740
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setDuration(I)V

    .line 2627741
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatCount(I)V

    .line 2627742
    invoke-virtual {p0, v2}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatDelay(I)V

    .line 2627743
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setRepeatMode(I)V

    .line 2627744
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v1, LX/Iv2;->CW_0:LX/Iv2;

    iput-object v1, v0, LX/Iv1;->a:LX/Iv2;

    .line 2627745
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    sget-object v1, LX/Iv3;->LINEAR:LX/Iv3;

    iput-object v1, v0, LX/Iv1;->i:LX/Iv3;

    .line 2627746
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v0, LX/Iv1;->c:F

    .line 2627747
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput v2, v0, LX/Iv1;->d:I

    .line 2627748
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput v2, v0, LX/Iv1;->e:I

    .line 2627749
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/4 v1, 0x0

    iput v1, v0, LX/Iv1;->f:F

    .line 2627750
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput v3, v0, LX/Iv1;->g:F

    .line 2627751
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput v3, v0, LX/Iv1;->h:F

    .line 2627752
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    const/high16 v1, 0x41a00000    # 20.0f

    iput v1, v0, LX/Iv1;->b:F

    .line 2627753
    new-instance v0, LX/Iv4;

    invoke-direct {v0}, LX/Iv4;-><init>()V

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    .line 2627754
    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p0, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setBaseAlpha(F)V

    .line 2627755
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627756
    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2627757
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getMaskBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2627758
    if-nez v0, :cond_0

    .line 2627759
    :goto_0
    return-void

    .line 2627760
    :cond_0
    iget v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->o:I

    iget v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->p:I

    iget v3, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->o:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->p:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 2627761
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2627762
    iget v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->o:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->p:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private d()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2627763
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2627764
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    .line 2627765
    :cond_0
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2627766
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2627767
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    .line 2627768
    :cond_0
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private f()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 2627769
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getWidth()I

    move-result v0

    .line 2627770
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getHeight()I

    move-result v1

    .line 2627771
    invoke-static {v0, v1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V
    .locals 0

    .prologue
    .line 2627772
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b()V

    .line 2627773
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h()V

    .line 2627774
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i()V

    .line 2627775
    return-void
.end method

.method private getLayoutListener()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 2627607
    new-instance v0, LX/Iuy;

    invoke-direct {v0, p0}, LX/Iuy;-><init>(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    return-object v0
.end method

.method private getMaskBitmap()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v0, 0x0

    .line 2627776
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2627777
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 2627778
    :goto_0
    return-object v0

    .line 2627779
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->s:Z

    if-eqz v1, :cond_1

    .line 2627780
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 2627781
    :cond_1
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, LX/Iv1;->a(I)I

    move-result v9

    .line 2627782
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, LX/Iv1;->b(I)I

    move-result v8

    .line 2627783
    invoke-static {v9, v8}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a(II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 2627784
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 2627785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->s:Z

    .line 2627786
    const/4 v0, 0x0

    goto :goto_0

    .line 2627787
    :cond_2
    new-instance v10, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    invoke-direct {v10, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2627788
    sget-object v1, LX/Iv0;->a:[I

    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v2, v2, LX/Iv1;->i:LX/Iv3;

    invoke-virtual {v2}, LX/Iv3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2627789
    sget-object v1, LX/Iv0;->b:[I

    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v2, v2, LX/Iv1;->a:LX/Iv2;

    invoke-virtual {v2}, LX/Iv2;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    move v4, v0

    move v3, v9

    move v2, v0

    move v1, v0

    .line 2627790
    :goto_1
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v1, v1

    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v4, v4

    iget-object v5, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {v5}, LX/Iv1;->a()[I

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {v6}, LX/Iv1;->b()[F

    move-result-object v6

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 2627791
    :goto_2
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v1, v1, LX/Iv1;->b:F

    div-int/lit8 v2, v9, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v8, 0x2

    int-to-float v3, v3

    invoke-virtual {v10, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2627792
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2627793
    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2627794
    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 2627795
    neg-int v1, v0

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    add-int v3, v9, v0

    int-to-float v3, v3

    add-int/2addr v0, v8

    int-to-float v4, v0

    move-object v0, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2627796
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :pswitch_0
    move v4, v8

    move v3, v0

    move v2, v0

    move v1, v0

    .line 2627797
    goto :goto_1

    :pswitch_1
    move v4, v0

    move v3, v0

    move v2, v0

    move v1, v9

    .line 2627798
    goto :goto_1

    :pswitch_2
    move v4, v0

    move v3, v0

    move v2, v8

    move v1, v0

    .line 2627799
    goto :goto_1

    .line 2627800
    :pswitch_3
    div-int/lit8 v1, v9, 0x2

    .line 2627801
    div-int/lit8 v2, v8, 0x2

    .line 2627802
    new-instance v0, Landroid/graphics/RadialGradient;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-double v4, v3

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-float v3, v4

    iget-object v4, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {v4}, LX/Iv1;->a()[I

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    invoke-virtual {v5}, LX/Iv1;->b()[F

    move-result-object v5

    sget-object v6, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getShimmerAnimation()Landroid/animation/Animator;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2627803
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 2627804
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 2627805
    :goto_0
    return-object v0

    .line 2627806
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getWidth()I

    move-result v0

    .line 2627807
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getHeight()I

    move-result v1

    .line 2627808
    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v2, v2, LX/Iv1;->i:LX/Iv3;

    invoke-virtual {v2}, LX/Iv3;->ordinal()I

    .line 2627809
    sget-object v2, LX/Iv0;->b:[I

    iget-object v3, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v3, v3, LX/Iv1;->a:LX/Iv2;

    invoke-virtual {v3}, LX/Iv2;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2627810
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    neg-int v2, v0

    invoke-virtual {v1, v2, v4, v0, v4}, LX/Iv4;->a(IIII)V

    .line 2627811
    :goto_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v1, v0, v4

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->m:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->k:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 2627812
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->k:I

    iget v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->m:I

    add-int/2addr v1, v2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2627813
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->l:I

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 2627814
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->n:I

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 2627815
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Iuz;

    invoke-direct {v1, p0}, LX/Iuz;-><init>(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2627816
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    goto :goto_0

    .line 2627817
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    neg-int v2, v1

    invoke-virtual {v0, v4, v2, v4, v1}, LX/Iv4;->a(IIII)V

    goto :goto_1

    .line 2627818
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    neg-int v2, v0

    invoke-virtual {v1, v0, v4, v2, v4}, LX/Iv4;->a(IIII)V

    goto :goto_1

    .line 2627819
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    neg-int v2, v1

    invoke-virtual {v0, v4, v1, v4, v2}, LX/Iv4;->a(IIII)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2627820
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2627821
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2627822
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b:Landroid/graphics/Bitmap;

    .line 2627823
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2627824
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2627825
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2627826
    iput-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->i:Landroid/graphics/Bitmap;

    .line 2627827
    :cond_0
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 2627828
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2627829
    iput-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->h:Landroid/graphics/Bitmap;

    .line 2627830
    :cond_1
    return-void
.end method

.method public static setMaskOffsetX(Lcom/facebook/neko/util/AppShimmerFrameLayout;I)V
    .locals 1

    .prologue
    .line 2627831
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->o:I

    if-ne v0, p1, :cond_0

    .line 2627832
    :goto_0
    return-void

    .line 2627833
    :cond_0
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->o:I

    .line 2627834
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->invalidate()V

    goto :goto_0
.end method

.method public static setMaskOffsetY(Lcom/facebook/neko/util/AppShimmerFrameLayout;I)V
    .locals 1

    .prologue
    .line 2627835
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->p:I

    if-ne v0, p1, :cond_0

    .line 2627836
    :goto_0
    return-void

    .line 2627837
    :cond_0
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->p:I

    .line 2627838
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2627839
    iget-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->s:Z

    if-eqz v0, :cond_1

    .line 2627840
    :cond_0
    :goto_0
    return-void

    .line 2627841
    :cond_1
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getShimmerAnimation()Landroid/animation/Animator;

    move-result-object v0

    .line 2627842
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 2627843
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->q:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2627844
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 2627845
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2627846
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 2627847
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2627848
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a:Landroid/animation/ValueAnimator;

    .line 2627849
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->q:Z

    .line 2627850
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2627851
    iget-boolean v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2627852
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2627853
    :goto_0
    return-void

    .line 2627854
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a(Landroid/graphics/Canvas;)Z

    goto :goto_0
.end method

.method public getAngle()LX/Iv2;
    .locals 1

    .prologue
    .line 2627855
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v0, v0, LX/Iv1;->a:LX/Iv2;

    return-object v0
.end method

.method public getBaseAlpha()F
    .locals 2

    .prologue
    .line 2627856
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getDropoff()F
    .locals 1

    .prologue
    .line 2627650
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->c:F

    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 2627857
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->k:I

    return v0
.end method

.method public getFixedHeight()I
    .locals 1

    .prologue
    .line 2627583
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->e:I

    return v0
.end method

.method public getFixedWidth()I
    .locals 1

    .prologue
    .line 2627606
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->d:I

    return v0
.end method

.method public getIntensity()F
    .locals 1

    .prologue
    .line 2627605
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->f:F

    return v0
.end method

.method public getMaskShape()LX/Iv3;
    .locals 1

    .prologue
    .line 2627604
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget-object v0, v0, LX/Iv1;->i:LX/Iv3;

    return-object v0
.end method

.method public getRelativeHeight()F
    .locals 1

    .prologue
    .line 2627603
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->h:F

    return v0
.end method

.method public getRelativeWidth()F
    .locals 1

    .prologue
    .line 2627602
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->g:F

    return v0
.end method

.method public getRepeatCount()I
    .locals 1

    .prologue
    .line 2627601
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->l:I

    return v0
.end method

.method public getRepeatDelay()I
    .locals 1

    .prologue
    .line 2627600
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->m:I

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    .prologue
    .line 2627599
    iget v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->n:I

    return v0
.end method

.method public getTilt()F
    .locals 1

    .prologue
    .line 2627598
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iget v0, v0, LX/Iv1;->b:F

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x708723e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2627593
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 2627594
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v1, :cond_0

    .line 2627595
    invoke-direct {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getLayoutListener()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2627596
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2627597
    const/16 v1, 0x2d

    const v2, 0x287edc79

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x41f5cd1a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2627587
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b()V

    .line 2627588
    iget-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 2627589
    invoke-virtual {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2627590
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->r:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2627591
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 2627592
    const/16 v1, 0x2d

    const v2, 0xc669890

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAngle(LX/Iv2;)V
    .locals 1

    .prologue
    .line 2627584
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput-object p1, v0, LX/Iv1;->a:LX/Iv2;

    .line 2627585
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627586
    return-void
.end method

.method public setAutoStart(Z)V
    .locals 0

    .prologue
    .line 2627608
    iput-boolean p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->j:Z

    .line 2627609
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627610
    return-void
.end method

.method public setBaseAlpha(F)V
    .locals 3

    .prologue
    .line 2627611
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->d:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2, p1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a(FFF)F

    move-result v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2627612
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627613
    return-void
.end method

.method public setDropoff(F)V
    .locals 1

    .prologue
    .line 2627614
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput p1, v0, LX/Iv1;->c:F

    .line 2627615
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627616
    return-void
.end method

.method public setDuration(I)V
    .locals 0

    .prologue
    .line 2627617
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->k:I

    .line 2627618
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627619
    return-void
.end method

.method public setFixedHeight(I)V
    .locals 1

    .prologue
    .line 2627620
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput p1, v0, LX/Iv1;->e:I

    .line 2627621
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627622
    return-void
.end method

.method public setFixedWidth(I)V
    .locals 1

    .prologue
    .line 2627623
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput p1, v0, LX/Iv1;->d:I

    .line 2627624
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627625
    return-void
.end method

.method public setIntensity(F)V
    .locals 1

    .prologue
    .line 2627626
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput p1, v0, LX/Iv1;->f:F

    .line 2627627
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627628
    return-void
.end method

.method public setMaskShape(LX/Iv3;)V
    .locals 1

    .prologue
    .line 2627629
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput-object p1, v0, LX/Iv1;->i:LX/Iv3;

    .line 2627630
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627631
    return-void
.end method

.method public setRelativeHeight(I)V
    .locals 2

    .prologue
    .line 2627632
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    int-to-float v1, p1

    iput v1, v0, LX/Iv1;->h:F

    .line 2627633
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627634
    return-void
.end method

.method public setRelativeWidth(I)V
    .locals 2

    .prologue
    .line 2627635
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    int-to-float v1, p1

    iput v1, v0, LX/Iv1;->g:F

    .line 2627636
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627637
    return-void
.end method

.method public setRepeatCount(I)V
    .locals 0

    .prologue
    .line 2627638
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->l:I

    .line 2627639
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627640
    return-void
.end method

.method public setRepeatDelay(I)V
    .locals 0

    .prologue
    .line 2627641
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->m:I

    .line 2627642
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627643
    return-void
.end method

.method public setRepeatMode(I)V
    .locals 0

    .prologue
    .line 2627644
    iput p1, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->n:I

    .line 2627645
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627646
    return-void
.end method

.method public setTilt(F)V
    .locals 1

    .prologue
    .line 2627647
    iget-object v0, p0, Lcom/facebook/neko/util/AppShimmerFrameLayout;->f:LX/Iv1;

    iput p1, v0, LX/Iv1;->b:F

    .line 2627648
    invoke-static {p0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V

    .line 2627649
    return-void
.end method
