.class public Lcom/facebook/neko/util/MainActivityFragment;
.super Landroid/support/v4/app/Fragment;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0hc;

.field private final c:Landroid/view/View$OnClickListener;

.field private d:LX/HjM;

.field public e:Lcom/facebook/neko/util/AppCardPager;

.field public f:LX/Iux;

.field public g:LX/Iup;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2628111
    const-class v0, Lcom/facebook/neko/util/MainActivityFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/neko/util/MainActivityFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2628107
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 2628108
    new-instance v0, LX/IvC;

    invoke-direct {v0, p0}, LX/IvC;-><init>(Lcom/facebook/neko/util/MainActivityFragment;)V

    iput-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->b:LX/0hc;

    .line 2628109
    new-instance v0, LX/IvD;

    invoke-direct {v0, p0}, LX/IvD;-><init>(Lcom/facebook/neko/util/MainActivityFragment;)V

    iput-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->c:Landroid/view/View$OnClickListener;

    .line 2628110
    return-void
.end method

.method public static b(Lcom/facebook/neko/util/MainActivityFragment;)V
    .locals 13

    .prologue
    const/16 v3, 0xa

    .line 2628102
    new-instance v0, LX/HjM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "184069175262368_184069815262304"

    invoke-direct {v0, v1, v2, v3}, LX/HjM;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    .line 2628103
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    iput-object p0, v0, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    .line 2628104
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    sget-object v4, LX/HjA;->NONE:LX/HjA;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    sget-object v8, LX/Hk2;->j:LX/Hk2;

    iget v11, v0, LX/HjM;->d:I

    new-instance v5, LX/HkE;

    iget-object v6, v0, LX/HjM;->b:Landroid/content/Context;

    iget-object v7, v0, LX/HjM;->c:Ljava/lang/String;

    const/4 v9, 0x0

    sget-object v10, LX/HjM;->a:LX/Hjs;

    move-object v12, v4

    invoke-direct/range {v5 .. v12}, LX/HkE;-><init>(Landroid/content/Context;Ljava/lang/String;LX/Hk2;LX/Hj3;LX/Hjs;ILjava/util/EnumSet;)V

    iput-object v5, v0, LX/HjM;->h:LX/HkE;

    iget-boolean v5, v0, LX/HjM;->i:Z

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/HjM;->h:LX/HkE;

    const/4 v6, 0x0

    iput-boolean v6, v5, LX/HkE;->h:Z

    iget-object v6, v5, LX/HkE;->i:Landroid/os/Handler;

    iget-object v7, v5, LX/HkE;->j:Ljava/lang/Runnable;

    invoke-static {v6, v7}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    :cond_0
    iget-object v5, v0, LX/HjM;->h:LX/HkE;

    new-instance v6, LX/HjL;

    invoke-direct {v6, v0, v4}, LX/HjL;-><init>(LX/HjM;Ljava/util/EnumSet;)V

    iput-object v6, v5, LX/HkE;->k:LX/HjL;

    iget-object v5, v0, LX/HjM;->h:LX/HkE;

    invoke-virtual {v5}, LX/HkE;->a()V

    .line 2628105
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "184069175262368_184069815262304"

    aput-object v2, v0, v1

    .line 2628106
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2628089
    new-instance v0, LX/Iux;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Iux;-><init>(LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    .line 2628090
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    invoke-virtual {v0}, LX/Iux;->d()V

    .line 2628091
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->c:Landroid/view/View$OnClickListener;

    .line 2628092
    iput-object v1, v0, LX/Iux;->h:Landroid/view/View$OnClickListener;

    .line 2628093
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2628094
    const v1, 0x7f0d1203

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/neko/util/AppCardPager;

    iput-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    .line 2628095
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->b:LX/0hc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2628096
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2628097
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2436

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2628098
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2628099
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    sget-object v1, LX/Iv7;->Loading:LX/Iv7;

    .line 2628100
    iput-object v1, v0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    .line 2628101
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 2628068
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    invoke-virtual {v0}, LX/HjM;->b()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2628069
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    invoke-virtual {v2}, LX/HjM;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2628070
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    .line 2628071
    sget-object v2, LX/Iv7;->Apps:LX/Iv7;

    iput-object v2, v0, LX/Iux;->b:LX/Iv7;

    .line 2628072
    iget-object v2, v0, LX/Iux;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2628073
    invoke-virtual {v1}, LX/HjM;->b()I

    move-result v2

    iput v2, v0, LX/Iux;->f:I

    .line 2628074
    const/4 v2, 0x0

    :goto_0
    iget v3, v0, LX/Iux;->f:I

    if-ge v2, v3, :cond_1

    .line 2628075
    iget-object v3, v0, LX/Iux;->e:Ljava/util/List;

    iget-object v4, v1, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x0

    :cond_0
    :goto_1
    move-object v4, v4

    .line 2628076
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2628077
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2628078
    :cond_1
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2628079
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    sget-object v1, LX/Iv7;->Apps:LX/Iv7;

    .line 2628080
    iput-object v1, v0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    .line 2628081
    :goto_2
    return-void

    .line 2628082
    :cond_2
    sget-object v0, Lcom/facebook/neko/util/MainActivityFragment;->a:Ljava/lang/String;

    const-string v1, "Successful ad request with 0 fill"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2628083
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    if-eqz v0, :cond_3

    .line 2628084
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    invoke-interface {v0}, LX/Iup;->b()V

    .line 2628085
    :cond_3
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    invoke-virtual {v0, v2}, LX/Iux;->a(Z)V

    .line 2628086
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    .line 2628087
    iput-object v1, v0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    .line 2628088
    goto :goto_2

    :cond_4
    iget v5, v1, LX/HjM;->f:I

    add-int/lit8 v4, v5, 0x1

    iput v4, v1, LX/HjM;->f:I

    iget-object v4, v1, LX/HjM;->e:Ljava/util/List;

    iget-object v6, v1, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    rem-int v6, v5, v6

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HjF;

    iget-object v6, v1, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v5, v6, :cond_0

    new-instance v5, LX/HjF;

    invoke-direct {v5, v4}, LX/HjF;-><init>(LX/HjF;)V

    move-object v4, v5

    goto :goto_1
.end method

.method public final a(LX/Hj0;)V
    .locals 3

    .prologue
    .line 2628057
    sget-object v0, Lcom/facebook/neko/util/MainActivityFragment;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encountered ad loading error \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/Hj0;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2628058
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2628059
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    if-eqz v0, :cond_0

    .line 2628060
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    invoke-interface {v0}, LX/Iup;->a()V

    .line 2628061
    :cond_0
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    sget-object v1, LX/Iv7;->Error:LX/Iv7;

    .line 2628062
    iput-object v1, v0, Lcom/facebook/neko/util/AppCardPager;->c:LX/Iv7;

    .line 2628063
    iget v0, p1, LX/Hj0;->g:I

    move v0, v0

    .line 2628064
    const/16 v1, 0x3ed

    if-ne v0, v1, :cond_1

    .line 2628065
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Iux;->a(Z)V

    .line 2628066
    :goto_0
    return-void

    .line 2628067
    :cond_1
    iget-object v0, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Iux;->a(Z)V

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2be15432

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2628053
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 2628054
    instance-of v1, p1, LX/Iup;

    if-eqz v1, :cond_0

    .line 2628055
    check-cast p1, LX/Iup;

    iput-object p1, p0, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    .line 2628056
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x362a6fd0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5cbe344c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2628052
    const v1, 0x7f030693

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x29537684

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x5487ec6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2628041
    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    if-eqz v1, :cond_0

    .line 2628042
    iget-object v1, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    iput-object v2, v1, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    .line 2628043
    :cond_0
    iput-object v2, p0, Lcom/facebook/neko/util/MainActivityFragment;->d:LX/HjM;

    .line 2628044
    iput-object v2, p0, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    .line 2628045
    iput-object v2, p0, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    .line 2628046
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 2628047
    const/16 v1, 0x2b

    const v2, 0x2dd7b6cc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2628048
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2628049
    invoke-direct {p0}, Lcom/facebook/neko/util/MainActivityFragment;->c()V

    .line 2628050
    invoke-static {p0}, Lcom/facebook/neko/util/MainActivityFragment;->b(Lcom/facebook/neko/util/MainActivityFragment;)V

    .line 2628051
    return-void
.end method
