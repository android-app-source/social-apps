.class public Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Iup;


# instance fields
.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2627382
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v0, p0, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->p:LX/0Zb;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2627380
    iget-object v0, p0, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->p:LX/0Zb;

    invoke-interface {v0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2627381
    return-void
.end method

.method public static b(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2627377
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2627378
    iget-object v1, p0, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->p:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2627379
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2627383
    const-string v0, "getgames_no_network_error"

    invoke-static {p0, v0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->b(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Ljava/lang/String;)V

    .line 2627384
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2627375
    const-string v0, "getgames_scrolled_event"

    invoke-direct {p0, v0, p1}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2627376
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2627373
    const-string v0, "getgames_no_fill_error"

    invoke-static {p0, v0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->b(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Ljava/lang/String;)V

    .line 2627374
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2627361
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2627362
    invoke-static {p0, p0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2627363
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->setContentView(I)V

    .line 2627364
    const v0, 0x7f0d1204

    invoke-virtual {p0, v0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2627365
    new-instance v1, LX/Iun;

    invoke-direct {v1, p0, p0}, LX/Iun;-><init>(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2627366
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2627367
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2627368
    const v1, 0x7f0838c6

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2627369
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasFbLogo(Z)V

    .line 2627370
    new-instance v1, LX/Iuo;

    invoke-direct {v1, p0}, LX/Iuo;-><init>(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2627371
    const-string v0, "getgames_activity_start"

    invoke-static {p0, v0}, Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;->b(Lcom/facebook/neko/getgamesneko/GetGamesNekoActivity;Ljava/lang/String;)V

    .line 2627372
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x72f78298

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2627359
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2627360
    const/16 v1, 0x23

    const v2, -0x7c6ee89e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6af1894

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2627357
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2627358
    const/16 v1, 0x23

    const v2, 0x5b2e7de0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
