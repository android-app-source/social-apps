.class public Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;
.super LX/5pb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AdInterfacesModule"
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/98q;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/1HI;

.field private e:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2666714
    const-class v0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/5pY;Ljava/util/concurrent/ExecutorService;LX/98q;LX/1HI;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2666715
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2666716
    iput-object p2, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->c:Ljava/util/concurrent/ExecutorService;

    .line 2666717
    iput-object p3, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    .line 2666718
    iput-object p4, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->d:LX/1HI;

    .line 2666719
    return-void
.end method

.method private static a(LX/5pC;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pC;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2666720
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2666721
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2666722
    invoke-interface {p0, v0}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2666723
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2666724
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;Landroid/graphics/Bitmap;IIIIILjava/lang/String;)V
    .locals 8

    .prologue
    .line 2666725
    const/4 v7, 0x0

    .line 2666726
    :try_start_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 2666727
    int-to-float v0, p6

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 2666728
    const/4 v6, 0x0

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2666729
    :try_start_1
    const-string v1, "upload"

    const-string v2, ".jpeg"

    invoke-static {v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 2666730
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2666731
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x55

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2666732
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 2666733
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2666734
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2666735
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2666736
    :cond_0
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 2666737
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2666738
    const-string v2, "file_path"

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2666739
    const-string v1, "photo_category"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2666740
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2666741
    :cond_1
    iget-object v0, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    .line 2666742
    :cond_2
    :goto_0
    return-void

    .line 2666743
    :catch_0
    move-object v0, v7

    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v1}, LX/98q;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2666744
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2666745
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2666746
    :catch_1
    move-object v0, v7

    :goto_2
    :try_start_3
    iget-object v1, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v1}, LX/98q;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2666747
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2666748
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2666749
    :catch_2
    move-object v0, v7

    :goto_3
    :try_start_4
    iget-object v1, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v1}, LX/98q;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2666750
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2666751
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 2666752
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2666753
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v0

    .line 2666754
    :catchall_1
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_4

    .line 2666755
    :catch_3
    goto :goto_3

    .line 2666756
    :catch_4
    goto :goto_2

    .line 2666757
    :catch_5
    goto :goto_1
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2666758
    const-string v0, "AdInterfacesModule"

    return-object v0
.end method

.method public onDetailedTargetingSelected(LX/5pC;)V
    .locals 7
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666759
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2666760
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2666761
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2666762
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2666763
    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v3

    .line 2666764
    if-eqz v3, :cond_0

    .line 2666765
    const-string v4, "path"

    invoke-interface {v3, v4}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->a(LX/5pC;)LX/0Px;

    move-result-object v4

    .line 2666766
    new-instance v5, LX/A9C;

    invoke-direct {v5}, LX/A9C;-><init>()V

    .line 2666767
    const-string v6, "id"

    invoke-interface {v3, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/A9C;->c:Ljava/lang/String;

    .line 2666768
    const-string v6, "name"

    invoke-interface {v3, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/A9C;->d:Ljava/lang/String;

    .line 2666769
    const-string v6, "description"

    invoke-interface {v3, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/A9C;->b:Ljava/lang/String;

    .line 2666770
    const-string v6, "target_type"

    invoke-interface {v3, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/A9C;->f:Ljava/lang/String;

    .line 2666771
    const-string v6, "audience_size"

    invoke-interface {v3, v6}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v5, LX/A9C;->a:I

    .line 2666772
    iput-object v4, v5, LX/A9C;->e:LX/0Px;

    .line 2666773
    invoke-virtual {v5}, LX/A9C;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2666774
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2666775
    :cond_1
    const-string v0, "detailed_targeting"

    invoke-static {v2, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 2666776
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2666777
    :cond_2
    iget-object v0, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    .line 2666778
    return-void
.end method

.method public onImageSelected(LX/5pG;)V
    .locals 8
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666779
    if-nez p1, :cond_0

    .line 2666780
    iget-object v0, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    .line 2666781
    :goto_0
    return-void

    .line 2666782
    :cond_0
    const-string v0, "imageSourceCategory"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2666783
    const-string v0, "imageSource"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2666784
    const-string v1, "uri"

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2666785
    if-nez v0, :cond_1

    .line 2666786
    iget-object v0, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    goto :goto_0

    .line 2666787
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 2666788
    const-string v1, "transformData"

    invoke-interface {p1, v1}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v1

    .line 2666789
    const-string v2, "offset"

    invoke-interface {v1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v3

    .line 2666790
    const-string v2, "size"

    invoke-interface {v1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v2

    const-string v4, "width"

    invoke-interface {v2, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2666791
    const-string v2, "size"

    invoke-interface {v1, v2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v1

    const-string v2, "height"

    invoke-interface {v1, v2}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 2666792
    const-string v1, "x"

    invoke-interface {v3, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2666793
    const-string v1, "y"

    invoke-interface {v3, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2666794
    iget-object v1, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->d:LX/1HI;

    sget-object v7, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v7}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->e:LX/1ca;

    .line 2666795
    iget-object v7, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->e:LX/1ca;

    new-instance v0, LX/JG3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/JG3;-><init>(Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;IIIILjava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v7, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
