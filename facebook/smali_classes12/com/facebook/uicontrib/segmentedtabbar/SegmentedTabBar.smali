.class public Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:LX/Hed;

.field public e:LX/GKz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2490683
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2490684
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490685
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2490680
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2490681
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490682
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2490637
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490638
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490639
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2490662
    const v0, 0x7f0312e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2490663
    const v0, 0x7f0d2c0b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->a:Landroid/view/View;

    .line 2490664
    const v0, 0x7f0d2c0c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    .line 2490665
    const v0, 0x7f0d2c0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    .line 2490666
    sget-object v0, LX/03r;->SegmentedTabBar:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2490667
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2490668
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 2490669
    if-lez v1, :cond_0

    .line 2490670
    iget-object v3, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490671
    :goto_0
    if-lez v2, :cond_1

    .line 2490672
    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490673
    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2490674
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    new-instance v1, LX/Hee;

    sget-object v2, LX/Hed;->LEFT:LX/Hed;

    invoke-direct {v1, p0, v2}, LX/Hee;-><init>(Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;LX/Hed;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2490675
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    new-instance v1, LX/Hee;

    sget-object v2, LX/Hed;->RIGHT:LX/Hed;

    invoke-direct {v1, p0, v2}, LX/Hee;-><init>(Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;LX/Hed;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2490676
    sget-object v0, LX/Hed;->LEFT:LX/Hed;

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->setSelectedTab(LX/Hed;)V

    .line 2490677
    return-void

    .line 2490678
    :cond_0
    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    const/16 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2490679
    :cond_1
    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    const/16 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getSelectedTab()LX/Hed;
    .locals 1

    .prologue
    .line 2490661
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->d:LX/Hed;

    return-object v0
.end method

.method public setLeftTabName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2490659
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490660
    return-void
.end method

.method public setListener(LX/GKz;)V
    .locals 0

    .prologue
    .line 2490657
    iput-object p1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->e:LX/GKz;

    .line 2490658
    return-void
.end method

.method public setRightTabName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2490655
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490656
    return-void
.end method

.method public setSelectedTab(LX/Hed;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2490640
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->d:LX/Hed;

    if-eq v0, p1, :cond_0

    .line 2490641
    iput-object p1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->d:LX/Hed;

    .line 2490642
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->d:LX/Hed;

    sget-object v3, LX/Hed;->LEFT:LX/Hed;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 2490643
    :goto_0
    iget-object v3, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2490644
    iget-object v4, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->b:Landroid/widget/TextView;

    if-nez v0, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 2490645
    iget-object v3, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2490646
    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 2490647
    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->a:Landroid/view/View;

    if-eqz v0, :cond_4

    const v0, 0x7f0217f0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2490648
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->e:LX/GKz;

    if-eqz v0, :cond_0

    .line 2490649
    iget-object v0, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->e:LX/GKz;

    iget-object v1, p0, Lcom/facebook/uicontrib/segmentedtabbar/SegmentedTabBar;->d:LX/Hed;

    invoke-interface {v0, v1}, LX/GKz;->a(LX/Hed;)V

    .line 2490650
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2490651
    goto :goto_0

    :cond_2
    move v3, v2

    .line 2490652
    goto :goto_1

    :cond_3
    move v1, v2

    .line 2490653
    goto :goto_2

    .line 2490654
    :cond_4
    const v0, 0x7f0217f3

    goto :goto_3
.end method
