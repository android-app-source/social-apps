.class public Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;
.super Lcom/facebook/fbreact/fragment/ReactActivity;
.source ""


# instance fields
.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2574215
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/ReactActivity;-><init>()V

    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2574224
    iget v0, p0, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->q:I

    if-lez v0, :cond_0

    .line 2574225
    :goto_0
    return-void

    .line 2574226
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2574227
    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->q:I

    goto :goto_0

    :cond_1
    const/16 v0, 0x19

    goto :goto_1
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2574221
    invoke-direct {p0}, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->l()V

    .line 2574222
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/ReactActivity;->b(Landroid/os/Bundle;)V

    .line 2574223
    return-void
.end method

.method public final d(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2574216
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2574217
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2574218
    const-string v1, "group"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574219
    const-string v0, "statusBarHeight"

    iget v1, p0, Lcom/facebook/groups/fb4a/react/activity/GroupsFeedReactActivity;->q:I

    int-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2574220
    return-object p1
.end method
