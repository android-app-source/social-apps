.class public Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public final c:Landroid/content/res/Resources;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/17W;

.field public final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2572976
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    const-string v1, "add_member"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/listview/BetterListView;LX/0gc;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/0ad;)V
    .locals 1
    .param p1    # Lcom/facebook/widget/listview/BetterListView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2572977
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2572978
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2572979
    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a:LX/0Px;

    .line 2572980
    iput-object p3, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->c:Landroid/content/res/Resources;

    .line 2572981
    iput-object p4, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2572982
    iput-object p5, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->e:LX/17W;

    .line 2572983
    iput-object p6, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->f:LX/0ad;

    .line 2572984
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2572985
    sget-object v0, LX/IOr;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2572986
    check-cast p2, LX/DMB;

    .line 2572987
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2572988
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;LX/IOb;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;",
            ">;",
            "LX/IOb;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2572989
    iput-object p1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->b:Ljava/lang/String;

    .line 2572990
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2572991
    new-instance v0, LX/IOo;

    sget-object v1, LX/IOr;->d:LX/DML;

    invoke-direct {v0, p0, v1}, LX/IOo;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;LX/DML;)V

    move-object v0, v0

    .line 2572992
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2572993
    if-eqz p4, :cond_0

    .line 2572994
    new-instance v0, LX/IOk;

    sget-object v1, LX/IOr;->b:LX/DML;

    invoke-direct {v0, p0, v1}, LX/IOk;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;LX/DML;)V

    move-object v0, v0

    .line 2572995
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2572996
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2572997
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2572998
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    .line 2572999
    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2573000
    new-instance v3, LX/IOm;

    sget-object p1, LX/IOr;->c:LX/DML;

    invoke-direct {v3, p0, p1, v0, p3}, LX/IOm;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;LX/DML;Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;LX/IOb;)V

    move-object v0, v3

    .line 2573001
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2573002
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2573003
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a:LX/0Px;

    .line 2573004
    const v0, 0x17741149

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2573005
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2573006
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2573007
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2573008
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2573009
    sget-object v1, LX/IOr;->e:LX/0Px;

    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2573010
    sget-object v0, LX/IOr;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
