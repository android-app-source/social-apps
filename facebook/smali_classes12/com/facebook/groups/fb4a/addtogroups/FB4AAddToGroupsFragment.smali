.class public Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IOb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DKo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DWD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Lcom/facebook/widget/listview/BetterListView;

.field public o:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

.field public p:Z

.field public q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2572835
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2572836
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2572837
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->p:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2572838
    invoke-virtual {p2}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2572839
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 2572840
    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2572841
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->p:Z

    .line 2572842
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2572843
    if-eqz v0, :cond_1

    .line 2572844
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2572845
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082826

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2572846
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->q:Z

    .line 2572847
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2572848
    :cond_1
    :goto_0
    return-void

    .line 2572849
    :cond_2
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 2572850
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082827

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 2572851
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082828

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2572852
    iget-boolean v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->q:Z

    if-eqz v1, :cond_1

    .line 2572853
    :cond_0
    :goto_0
    return v0

    .line 2572854
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    if-eqz v1, :cond_2

    .line 2572855
    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    invoke-virtual {v1}, LX/IOb;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    .line 2572856
    if-lez v1, :cond_0

    .line 2572857
    const/4 v4, 0x1

    .line 2572858
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2572859
    invoke-virtual {v0, v4}, LX/0ju;->a(Z)LX/0ju;

    .line 2572860
    const v2, 0x7f08282c

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2572861
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f011b

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->m:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2572862
    const v2, 0x7f08282d

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IOc;

    invoke-direct {v3, p0}, LX/IOc;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2572863
    const v2, 0x7f08282e

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IOd;

    invoke-direct {v3, p0}, LX/IOd;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2572864
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2572865
    const/4 v0, 0x1

    goto :goto_0

    .line 2572866
    :cond_2
    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->g:LX/03V;

    sget-object v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->k:Ljava/lang/String;

    const-string v3, "onBackPressed is called before onFragmentCreate"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2572867
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2572868
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    new-instance v4, LX/IOb;

    invoke-direct {v4}, LX/IOb;-><init>()V

    move-object v4, v4

    move-object v4, v4

    check-cast v4, LX/IOb;

    const-class v5, LX/IOp;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IOp;

    invoke-static {v0}, LX/DKo;->b(LX/0QB;)LX/DKo;

    move-result-object v6

    check-cast v6, LX/DKo;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/DWD;->b(LX/0QB;)LX/DWD;

    move-result-object v8

    check-cast v8, LX/DWD;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const/16 p1, 0x15e7

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, LX/0TD;

    iput-object v3, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    iput-object v4, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    iput-object v5, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->c:LX/IOp;

    iput-object v6, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->d:LX/DKo;

    iput-object v7, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->e:LX/0tX;

    iput-object v8, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->f:LX/DWD;

    iput-object v9, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->g:LX/03V;

    iput-object p1, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->h:LX/0Or;

    iput-object v0, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->i:LX/0TD;

    .line 2572869
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2572870
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->l:Ljava/lang/String;

    .line 2572871
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->m:Ljava/lang/String;

    .line 2572872
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x60baa13a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2572873
    const v1, 0x7f0300a6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x14e9edb5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b9c4fc3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2572874
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2572875
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2572876
    if-eqz v1, :cond_0

    .line 2572877
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2572878
    const v2, 0x7f082821

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2572879
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08282b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2572880
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2572881
    move-object v2, v2

    .line 2572882
    const/4 v4, -0x2

    .line 2572883
    iput v4, v2, LX/108;->h:I

    .line 2572884
    move-object v2, v2

    .line 2572885
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2572886
    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2572887
    new-instance v2, LX/IOf;

    invoke-direct {v2, p0}, LX/IOf;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2572888
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x574f523

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2572889
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2572890
    iget-object p1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->d:LX/DKo;

    new-instance p2, LX/IOh;

    invoke-direct {p2, p0}, LX/IOh;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V

    invoke-virtual {p1, p2}, LX/DKo;->a(LX/DKm;)V

    .line 2572891
    iget-object p1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->d:LX/DKo;

    invoke-virtual {p1}, LX/DKo;->a()V

    .line 2572892
    const v0, 0x7f0d04af

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    .line 2572893
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->c:LX/IOp;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    .line 2572894
    new-instance v5, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v10

    check-cast v10, LX/17W;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    move-object v6, v1

    move-object v7, v2

    invoke-direct/range {v5 .. v11}, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;-><init>(Lcom/facebook/widget/listview/BetterListView;LX/0gc;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/0ad;)V

    .line 2572895
    move-object v0, v5

    .line 2572896
    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->o:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    .line 2572897
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->o:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->l:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a(Ljava/lang/String;LX/0Px;LX/IOb;Z)V

    .line 2572898
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->o:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2572899
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IOg;

    invoke-direct {v1, p0}, LX/IOg;-><init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2572900
    return-void
.end method
