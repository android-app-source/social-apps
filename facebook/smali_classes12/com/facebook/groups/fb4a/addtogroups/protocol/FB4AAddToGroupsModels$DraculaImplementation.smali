.class public final Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2573106
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2573107
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2573104
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2573105
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2573078
    if-nez p1, :cond_0

    move v0, v1

    .line 2573079
    :goto_0
    return v0

    .line 2573080
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2573081
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2573082
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2573083
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2573084
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2573085
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2573086
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2573087
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2573088
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2573089
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2573090
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2573091
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2573092
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2573093
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2573094
    :sswitch_1
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    .line 2573095
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2573096
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2573097
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2573098
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2573099
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2573100
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2573101
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2573102
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2573103
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2ba22c0a -> :sswitch_0
        0x610ed0ca -> :sswitch_1
        0x6217781d -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2573077
    new-instance v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2573056
    if-eqz p0, :cond_0

    .line 2573057
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2573058
    if-eq v0, p0, :cond_0

    .line 2573059
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2573060
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2573072
    sparse-switch p2, :sswitch_data_0

    .line 2573073
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2573074
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    .line 2573075
    invoke-static {v0, p3}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2573076
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2ba22c0a -> :sswitch_1
        0x610ed0ca -> :sswitch_0
        0x6217781d -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2573071
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2573069
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2573070
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2573108
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2573109
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2573110
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a:LX/15i;

    .line 2573111
    iput p2, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->b:I

    .line 2573112
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2573068
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2573067
    new-instance v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2573064
    iget v0, p0, LX/1vt;->c:I

    .line 2573065
    move v0, v0

    .line 2573066
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2573061
    iget v0, p0, LX/1vt;->c:I

    .line 2573062
    move v0, v0

    .line 2573063
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2573053
    iget v0, p0, LX/1vt;->b:I

    .line 2573054
    move v0, v0

    .line 2573055
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2573050
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2573051
    move-object v0, v0

    .line 2573052
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2573041
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2573042
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2573043
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2573044
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2573045
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2573046
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2573047
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2573048
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2573049
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2573038
    iget v0, p0, LX/1vt;->c:I

    .line 2573039
    move v0, v0

    .line 2573040
    return v0
.end method
