.class public final Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2573251
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;

    new-instance v1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2573252
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2573254
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2573255
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2573256
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2573257
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2573258
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2573259
    if-eqz v2, :cond_0

    .line 2573260
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2573261
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2573262
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2573263
    if-eqz v2, :cond_1

    .line 2573264
    const-string p0, "groups"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2573265
    invoke-static {v1, v2, p1, p2}, LX/IOy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2573266
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2573267
    if-eqz v2, :cond_2

    .line 2573268
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2573269
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2573270
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2573271
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2573253
    check-cast p1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$Serializer;->a(Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;LX/0nX;LX/0my;)V

    return-void
.end method
