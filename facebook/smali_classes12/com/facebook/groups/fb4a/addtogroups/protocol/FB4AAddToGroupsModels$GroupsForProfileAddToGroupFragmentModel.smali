.class public final Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x61876de6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2573442
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2573441
    const-class v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2573439
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2573440
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2573433
    iput-object p1, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->g:Ljava/lang/String;

    .line 2573434
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2573435
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2573436
    if-eqz v0, :cond_0

    .line 2573437
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2573438
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2573423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2573424
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x610ed0ca

    invoke-static {v1, v0, v2}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2573425
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2573426
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2573427
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2573428
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2573429
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2573430
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2573431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2573432
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2573413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2573414
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2573415
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x610ed0ca

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2573416
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2573417
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    .line 2573418
    iput v3, v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->e:I

    .line 2573419
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2573420
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2573421
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2573422
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2573412
    new-instance v0, LX/IOv;

    invoke-direct {v0, p1}, LX/IOv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2573411
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2573408
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2573409
    const/4 v0, 0x0

    const v1, 0x610ed0ca

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->e:I

    .line 2573410
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2573388
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2573389
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2573390
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2573391
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 2573392
    :goto_0
    return-void

    .line 2573393
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2573405
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2573406
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->a(Ljava/lang/String;)V

    .line 2573407
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2573402
    new-instance v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;-><init>()V

    .line 2573403
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2573404
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2573401
    const v0, -0x180850ab

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2573400
    const v0, 0x41e065f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2573398
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2573399
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2573396
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->f:Ljava/lang/String;

    .line 2573397
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2573394
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->g:Ljava/lang/String;

    .line 2573395
    iget-object v0, p0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method
