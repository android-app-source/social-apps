.class public Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/text/NumberFormat;

.field public k:Lcom/facebook/widget/tiles/ThreadTileView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Lcom/facebook/widget/text/BetterTextView;

.field public o:Lcom/facebook/resources/ui/FbButton;

.field public p:Lcom/facebook/resources/ui/FbButton;

.field public q:Landroid/view/View;

.field public r:Z

.field public s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

.field public t:LX/IPB;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2573737
    const-class v0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    const-string v1, "discovery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2573738
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2573739
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v4

    check-cast v4, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    const/16 v7, 0xc

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    iput-object v3, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->a:LX/0W9;

    iput-object v4, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->b:LX/3mF;

    iput-object v5, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->c:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->d:LX/0kL;

    iput-object v7, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->e:LX/0Or;

    iput-object v8, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->g:Ljava/lang/String;

    iput-object v0, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->h:LX/0tX;

    .line 2573740
    const v0, 0x7f030602

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2573741
    const v0, 0x7f0d1081

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->q:Landroid/view/View;

    .line 2573742
    const v0, 0x7f0d1082

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->k:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2573743
    const v0, 0x7f0d1084

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2573744
    const v0, 0x7f0d1083

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2573745
    const v0, 0x7f0d1085

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 2573746
    const v0, 0x7f0d1086

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->o:Lcom/facebook/resources/ui/FbButton;

    .line 2573747
    const v0, 0x7f0d1087

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->p:Lcom/facebook/resources/ui/FbButton;

    .line 2573748
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->j:Ljava/text/NumberFormat;

    .line 2573749
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 2573750
    if-eqz p2, :cond_0

    .line 2573751
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2573752
    :goto_0
    return-void

    .line 2573753
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2573754
    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->clearAnimation()V

    .line 2573755
    :cond_1
    iput-object p1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2573756
    invoke-virtual {p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    .line 2573757
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2573758
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2573759
    invoke-virtual {p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v3, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2573760
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->k:Lcom/facebook/widget/tiles/ThreadTileView;

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2573761
    if-nez v0, :cond_2

    move-object v2, v6

    .line 2573762
    :goto_1
    move-object v2, v2

    .line 2573763
    invoke-virtual {v1, v2}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2573764
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    const/4 v3, 0x0

    .line 2573765
    sget-object v1, LX/IP7;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2573766
    :goto_2
    new-instance v0, LX/IP3;

    invoke-direct {v0, p0}, LX/IP3;-><init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2573767
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->o:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/IP4;

    invoke-direct {v1, p0}, LX/IP4;-><init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2573768
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->p:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/IP5;

    invoke-direct {v1, p0}, LX/IP5;-><init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2573769
    goto :goto_0

    .line 2573770
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v8

    .line 2573771
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 2573772
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_b

    .line 2573773
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2573774
    const-class v10, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v9, v2, v7, v10}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_a

    move v2, v3

    :goto_3
    if-eqz v2, :cond_d

    .line 2573775
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v10, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v9, v2, v7, v10}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2573776
    if-eqz v2, :cond_c

    move v2, v3

    :goto_4
    if-eqz v2, :cond_f

    .line 2573777
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v10, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v9, v2, v7, v10}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2573778
    invoke-virtual {v9, v2, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    :goto_5
    move v2, v3

    .line 2573779
    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->l()I

    move-result v2

    if-nez v2, :cond_3

    .line 2573780
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v2, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v4, v2, LX/1vs;->b:I

    .line 2573781
    new-instance v2, LX/IPN;

    invoke-virtual {v3, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2573782
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2573783
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2573784
    invoke-direct {v2, v8, v3, v4, v5}, LX/IPN;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/0Px;)V

    goto/16 :goto_1

    .line 2573785
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->k()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v9

    .line 2573786
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2573787
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    move v7, v5

    .line 2573788
    :goto_6
    if-ge v7, v9, :cond_9

    const/4 v2, 0x3

    if-ge v7, v2, :cond_9

    .line 2573789
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->k()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;

    .line 2573790
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2573791
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;->k()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2573792
    if-eqz v3, :cond_5

    move v3, v4

    :goto_7
    if-eqz v3, :cond_8

    .line 2573793
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;->k()LX/1vs;

    move-result-object v3

    iget-object p1, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2573794
    invoke-virtual {p1, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    move v3, v4

    :goto_8
    if-eqz v3, :cond_4

    .line 2573795
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object p1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;->j()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p1, p2}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2573796
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$GroupMembersModel$EdgesModel$EdgesNodeModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2573797
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2573798
    :cond_4
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_6

    :cond_5
    move v3, v5

    .line 2573799
    goto :goto_7

    :cond_6
    move v3, v5

    goto :goto_7

    :cond_7
    move v3, v5

    goto :goto_8

    :cond_8
    move v3, v5

    goto :goto_8

    .line 2573800
    :cond_9
    new-instance v2, LX/IPN;

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {v2, v8, v6, v3, v4}, LX/IPN;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/0Px;)V

    goto/16 :goto_1

    :cond_a
    move v2, v7

    goto/16 :goto_3

    :cond_b
    move v2, v7

    goto/16 :goto_3

    :cond_c
    move v2, v7

    goto/16 :goto_4

    :cond_d
    move v2, v7

    goto/16 :goto_4

    :cond_e
    move v3, v7

    goto/16 :goto_5

    :cond_f
    move v3, v7

    goto/16 :goto_5

    .line 2573801
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->r:Z

    .line 2573802
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->o:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08388d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2573803
    :pswitch_1
    iput-boolean v3, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->r:Z

    .line 2573804
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->o:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08388e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2573805
    :pswitch_2
    iput-boolean v3, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->r:Z

    .line 2573806
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->o:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08388f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
