.class public Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/IPM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IPA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Xp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/IPG;

.field private f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private final i:LX/IPB;

.field public final j:LX/1DI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2573888
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2573889
    new-instance v0, LX/IPB;

    invoke-direct {v0, p0}, LX/IPB;-><init>(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->i:LX/IPB;

    .line 2573890
    new-instance v0, LX/IPC;

    invoke-direct {v0, p0}, LX/IPC;-><init>(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->j:LX/1DI;

    .line 2573891
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    new-instance v4, LX/IPM;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v3}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v9

    check-cast v9, LX/1My;

    invoke-direct/range {v4 .. v9}, LX/IPM;-><init>(LX/1Ck;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0kb;LX/1My;)V

    move-object v0, v4

    check-cast v0, LX/IPM;

    new-instance v1, LX/IPA;

    invoke-direct {v1}, LX/IPA;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, LX/IPA;

    invoke-static {v3}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-static {v3}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v3

    check-cast v3, LX/0Xp;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    iput-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    iput-object v2, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->c:LX/0kb;

    iput-object v3, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->d:LX/0Xp;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V
    .locals 2

    .prologue
    .line 2573935
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/IPA;->a(Z)V

    .line 2573936
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    invoke-virtual {v0}, LX/IPM;->d()V

    .line 2573937
    return-void
.end method

.method public static d(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V
    .locals 5

    .prologue
    .line 2573927
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_0

    .line 2573928
    const v0, 0x7f0d107e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2573929
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, "%s\n\n%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x7f08388b

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const v4, 0x7f08388c

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2573930
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2573931
    const v0, 0x7f0d1080

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2573932
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2573933
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2573934
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2573926
    const-string v0, "discovery"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2573923
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2573924
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2573925
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x48005301

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573922
    const v1, 0x7f030601

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1ef13dbd

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5da5e7a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573916
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2573917
    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    .line 2573918
    const/4 v2, 0x0

    iput-object v2, v1, LX/IPM;->j:LX/IPE;

    .line 2573919
    iget-object v2, v1, LX/IPM;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2573920
    iget-object v2, v1, LX/IPM;->f:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2573921
    const/16 v1, 0x2b

    const v2, 0x32949f4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x133d9906

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573909
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2573910
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2573911
    if-eqz v1, :cond_0

    .line 2573912
    const v2, 0x7f083888

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2573913
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2573914
    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2573915
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x52786292

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2573892
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2573893
    new-instance v0, LX/IPG;

    invoke-direct {v0, p0}, LX/IPG;-><init>(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->e:LX/IPG;

    .line 2573894
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->d:LX/0Xp;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->e:LX/IPG;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2573895
    const v0, 0x7f0d107f    # 1.875068E38f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2573896
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2573897
    const v0, 0x7f0d1080

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2573898
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2573899
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    iget-object v1, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->i:LX/IPB;

    .line 2573900
    iput-object v1, v0, LX/IPA;->f:LX/IPB;

    .line 2573901
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IPD;

    invoke-direct {v1, p0}, LX/IPD;-><init>(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2573902
    iget-object v0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, LX/IPE;

    invoke-direct {v2, p0}, LX/IPE;-><init>(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    .line 2573903
    iput-object v2, v0, LX/IPM;->j:LX/IPE;

    .line 2573904
    iput-object v1, v0, LX/IPM;->p:Landroid/content/res/Resources;

    .line 2573905
    iget-object v3, v0, LX/IPM;->p:Landroid/content/res/Resources;

    const p1, 0x7f0b23e7

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, v0, LX/IPM;->q:F

    .line 2573906
    new-instance v3, LX/IPH;

    invoke-direct {v3, v0}, LX/IPH;-><init>(LX/IPM;)V

    iput-object v3, v0, LX/IPM;->i:LX/0TF;

    .line 2573907
    invoke-static {p0}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b$redex0(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    .line 2573908
    return-void
.end method
