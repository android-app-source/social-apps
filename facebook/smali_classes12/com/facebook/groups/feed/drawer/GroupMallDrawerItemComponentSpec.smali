.class public Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DOL;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2574671
    const-class v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DOL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574653
    iput-object p1, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->b:LX/0Or;

    .line 2574654
    iput-object p2, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->c:LX/0Ot;

    .line 2574655
    iput-object p3, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->d:LX/0Ot;

    .line 2574656
    iput-object p4, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->e:Landroid/content/Context;

    .line 2574657
    iput-object p5, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->f:LX/0Ot;

    .line 2574658
    iput-object p6, p0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->g:LX/0Ot;

    .line 2574659
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;
    .locals 10

    .prologue
    .line 2574660
    const-class v1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;

    monitor-enter v1

    .line 2574661
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2574662
    sput-object v2, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2574663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2574665
    new-instance v3, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x240a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    const/16 v8, 0xac0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xaa5

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;-><init>(LX/0Or;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/0Ot;LX/0Ot;)V

    .line 2574666
    move-object v0, v3

    .line 2574667
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2574668
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574669
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2574670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
