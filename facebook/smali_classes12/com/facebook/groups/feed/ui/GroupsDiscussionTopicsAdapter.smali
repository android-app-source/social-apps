.class public Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IQt;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/IRO;

.field private d:Landroid/content/Context;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2575867
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2575868
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2575869
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->d:Landroid/content/Context;

    .line 2575870
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->e:Landroid/view/LayoutInflater;

    .line 2575871
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2575872
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f030846

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2575873
    new-instance v2, LX/IQt;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->c:LX/IRO;

    invoke-direct {v2, v0, v1}, LX/IQt;-><init>(Landroid/widget/FrameLayout;LX/IRO;)V

    .line 2575874
    const v1, 0x7f0d1591

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, v2, LX/IQt;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2575875
    const v1, 0x7f0d1590

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, v2, LX/IQt;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2575876
    return-object v2
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2575877
    check-cast p1, LX/IQt;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2575878
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    .line 2575879
    iget-object v3, p1, LX/IQt;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2575880
    iget-object v4, p1, LX/IQt;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2575881
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2575882
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575883
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->k()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_2

    .line 2575884
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2575885
    :goto_1
    return-void

    .line 2575886
    :cond_1
    const-string v5, ""

    invoke-virtual {v3, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2575887
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2575888
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->b:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
