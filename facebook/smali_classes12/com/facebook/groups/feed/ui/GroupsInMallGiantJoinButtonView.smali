.class public Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/ITK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/graphics/Paint;

.field public d:LX/ITJ;

.field public e:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2578016
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2578017
    invoke-direct {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Landroid/content/Context;)V

    .line 2578018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2578019
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2578020
    invoke-direct {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Landroid/content/Context;)V

    .line 2578021
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2578022
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2578023
    invoke-direct {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Landroid/content/Context;)V

    .line 2578024
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 2578025
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2578026
    const v0, 0x7f030852

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2578027
    invoke-virtual {p0, v1}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->setOrientation(I)V

    .line 2578028
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->c:Landroid/graphics/Paint;

    .line 2578029
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2578030
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2578031
    const v0, 0x7f0d15b4

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2578032
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2578033
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2578034
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2578035
    :goto_0
    return-void

    .line 2578036
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;LX/ITK;LX/3my;)V
    .locals 0

    .prologue
    .line 2578037
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a:LX/ITK;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->b:LX/3my;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    const-class v0, LX/ITK;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/ITK;

    invoke-static {v1}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v1

    check-cast v1, LX/3my;

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;LX/ITK;LX/3my;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V
    .locals 3

    .prologue
    .line 2578038
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2578039
    :cond_0
    :goto_0
    return-void

    .line 2578040
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->b:LX/3my;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    .line 2578041
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->b:LX/3my;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v1

    .line 2578042
    if-eqz v0, :cond_2

    const v0, 0x7f081c1d

    .line 2578043
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2578044
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081c1e    # 1.80921E38f

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2578045
    :goto_2
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/ISW;

    invoke-direct {v2, p0, p2, p1, v0}, LX/ISW;-><init>(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;LX/IRb;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;I)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2578046
    :cond_2
    if-eqz v1, :cond_3

    const v0, 0x7f081c1c

    goto :goto_1

    :cond_3
    const v0, 0x7f081c1b

    goto :goto_1

    .line 2578047
    :cond_4
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_2
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2578048
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2578049
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2578050
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2578051
    return-void
.end method
