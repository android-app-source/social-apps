.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

.field private final i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

.field private final j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583103
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2583104
    iput-object p10, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 2583105
    iput-object p8, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 2583106
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2583107
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    .line 2583108
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->e:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 2583109
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2583110
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 2583111
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    .line 2583112
    iput-object p9, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2583113
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    .line 2583114
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;
    .locals 14

    .prologue
    .line 2583092
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    monitor-enter v1

    .line 2583093
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583094
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583095
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583096
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583097
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;)V

    .line 2583098
    move-object v0, v3

    .line 2583099
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583100
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583101
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583102
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2583089
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2583090
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2583091
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2583078
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583079
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583080
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583081
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583082
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583083
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->e:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583084
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583085
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583086
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583087
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583088
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2583076
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583077
    invoke-static {p1}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
