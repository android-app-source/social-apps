.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1VD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VD",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1VE;

.field private final f:LX/1V0;

.field private final g:LX/1VH;

.field private final h:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1VD;LX/1V0;LX/1VH;LX/1VE;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582868
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2582869
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->d:LX/1VD;

    .line 2582870
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->f:LX/1V0;

    .line 2582871
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->g:LX/1VH;

    .line 2582872
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->e:LX/1VE;

    .line 2582873
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->h:LX/0Uh;

    .line 2582874
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2582875
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->d:LX/1VD;

    invoke-virtual {v0, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v1

    .line 2582876
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582877
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-virtual {v1, v0}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2582878
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->e:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 2582879
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2582880
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->g:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->GROUP:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;
    .locals 10

    .prologue
    .line 2582881
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2582882
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582883
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582884
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582885
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582886
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v5

    check-cast v5, LX/1VD;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v7

    check-cast v7, LX/1VH;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v8

    check-cast v8, LX/1VE;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VD;LX/1V0;LX/1VH;LX/1VE;LX/0Uh;)V

    .line 2582887
    move-object v0, v3

    .line 2582888
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582889
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582890
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2582892
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2582893
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2582894
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2582895
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582896
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582897
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->h:LX/0Uh;

    const/16 v2, 0x97

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2582898
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582899
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
