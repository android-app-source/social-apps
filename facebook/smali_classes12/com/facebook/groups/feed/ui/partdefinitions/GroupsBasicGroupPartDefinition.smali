.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0Ot;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;LX/0Ot;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            ">;",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            ">;",
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;",
            ">;",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;",
            ">;",
            "Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582711
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582712
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 2582713
    iput-object p10, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 2582714
    iput-object p9, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2582715
    iput-object p8, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    .line 2582716
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->e:LX/0Ot;

    .line 2582717
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2582718
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 2582719
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->h:LX/0Ot;

    .line 2582720
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->i:LX/0Ot;

    .line 2582721
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;

    .line 2582722
    iput-object p11, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->k:LX/0Ot;

    .line 2582723
    iput-object p12, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->l:LX/0Ot;

    .line 2582724
    iput-object p13, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->m:LX/0Ot;

    .line 2582725
    iput-object p14, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->n:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    .line 2582726
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;
    .locals 3

    .prologue
    .line 2582727
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    monitor-enter v1

    .line 2582728
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582729
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582732
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582733
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;
    .locals 15

    .prologue
    .line 2582735
    new-instance v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    const/16 v2, 0x704

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2582736
    new-instance v4, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    const/16 v5, 0x243a

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;LX/0Ot;)V

    .line 2582737
    move-object v3, v4

    .line 2582738
    check-cast v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;

    const/16 v4, 0x926

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    const/16 v7, 0x6ef

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    const/16 v11, 0x929

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xa0d

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xb35

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0Ot;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;LX/0Ot;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;LX/0Ot;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;)V

    .line 2582739
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2582740
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582741
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582742
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582743
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1VF;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->i:LX/0Ot;

    invoke-virtual {p1, v0, v1, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 2582744
    invoke-static {p2}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2582745
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582746
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->j:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582747
    invoke-static {p2}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2582748
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582749
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582750
    invoke-static {p2}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2582751
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582752
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582753
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->n:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582754
    invoke-static {p2}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2582755
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582756
    :cond_4
    invoke-static {p2}, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2582757
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582758
    :cond_5
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582759
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582760
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582761
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582762
    const/4 v0, 0x0

    return-object v0

    .line 2582763
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582764
    const/4 v0, 0x1

    return v0
.end method
