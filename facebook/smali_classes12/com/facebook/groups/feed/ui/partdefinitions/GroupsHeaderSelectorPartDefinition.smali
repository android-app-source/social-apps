.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582942
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582943
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    .line 2582944
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;->b:LX/0Ot;

    .line 2582945
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2582935
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v2, 0x0

    .line 2582936
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->LEARNING_UNIT:LX/1Qt;

    invoke-virtual {v0, v1}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582937
    :goto_0
    return-object v2

    .line 2582938
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderSelectorPartDefinition;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582939
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582940
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582941
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
