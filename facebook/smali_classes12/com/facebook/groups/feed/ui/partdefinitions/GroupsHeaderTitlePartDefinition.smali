.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1xc;

.field private final b:LX/BsK;

.field public final c:LX/Bs7;

.field public final d:LX/1DR;

.field public final e:LX/1xv;

.field private final f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1xc;LX/BsK;LX/Bs7;LX/1DR;LX/1xv;Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582978
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2582979
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->a:LX/1xc;

    .line 2582980
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->b:LX/BsK;

    .line 2582981
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->c:LX/Bs7;

    .line 2582982
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->d:LX/1DR;

    .line 2582983
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->e:LX/1xv;

    .line 2582984
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    .line 2582985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;
    .locals 10

    .prologue
    .line 2582986
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    monitor-enter v1

    .line 2582987
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582988
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582989
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582990
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582991
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    invoke-static {v0}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v4

    check-cast v4, LX/1xc;

    invoke-static {v0}, LX/BsK;->a(LX/0QB;)LX/BsK;

    move-result-object v5

    check-cast v5, LX/BsK;

    invoke-static {v0}, LX/Bs7;->a(LX/0QB;)LX/Bs7;

    move-result-object v6

    check-cast v6, LX/Bs7;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v8

    check-cast v8, LX/1xv;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;-><init>(LX/1xc;LX/BsK;LX/Bs7;LX/1DR;LX/1xv;Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;)V

    .line 2582992
    move-object v0, v3

    .line 2582993
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582994
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582995
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2582997
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 2582998
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->f:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    new-instance v1, LX/IVt;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->b:LX/BsK;

    .line 2582999
    iget-object v3, v2, LX/BsK;->a:LX/1nq;

    move-object v2, v3

    .line 2583000
    check-cast p3, LX/1Pk;

    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v3

    invoke-direct {v1, p0, v2, p2, v3}, LX/IVt;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2583001
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5db26116

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583002
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 2583003
    const v1, 0x7f0d0081

    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTag(ILjava/lang/Object;)V

    .line 2583004
    const/16 v1, 0x1f

    const v2, 0x50d9a3dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
