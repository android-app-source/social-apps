.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582811
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582812
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 2582813
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->b:LX/0Ot;

    .line 2582814
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->c:LX/0Uh;

    .line 2582815
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;
    .locals 6

    .prologue
    .line 2582816
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    monitor-enter v1

    .line 2582817
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582818
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582819
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582820
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582821
    new-instance v5, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    const/16 v4, 0x706

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;LX/0Ot;LX/0Uh;)V

    .line 2582822
    move-object v0, v5

    .line 2582823
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582824
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582825
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582826
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2582827
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582828
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x98

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582829
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582830
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582831
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582832
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582833
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
