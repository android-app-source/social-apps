.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static q:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

.field private final d:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

.field private final e:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field public final k:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

.field private final l:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

.field public final m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field public final n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

.field private final p:LX/0qn;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0qn;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583037
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2583038
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    .line 2583039
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    .line 2583040
    iput-object p8, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->h:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    .line 2583041
    iput-object p9, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->i:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 2583042
    iput-object p10, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->j:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2583043
    iput-object p11, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->k:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    .line 2583044
    iput-object p13, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 2583045
    iput-object p14, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->n:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    .line 2583046
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->o:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    .line 2583047
    iput-object p12, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->l:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    .line 2583048
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->a:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    .line 2583049
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    .line 2583050
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->c:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    .line 2583051
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->d:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    .line 2583052
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->e:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    .line 2583053
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->p:LX/0qn;

    .line 2583054
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;
    .locals 3

    .prologue
    .line 2583029
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    monitor-enter v1

    .line 2583030
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583031
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583032
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583033
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0qn;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2583025
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2583026
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2583027
    invoke-virtual {p1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 2583028
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;
    .locals 17

    .prologue
    .line 2583055
    new-instance v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v16

    check-cast v16, LX/0qn;

    invoke-direct/range {v0 .. v16}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;LX/0qn;)V

    .line 2583056
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2583016
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583017
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->a:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583018
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->c:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583019
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->e:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->d:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583020
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    new-instance v1, LX/Bsx;

    .line 2583021
    new-instance v2, LX/IVv;

    invoke-direct {v2, p0}, LX/IVv;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;)V

    move-object v2, v2

    .line 2583022
    invoke-direct {v1, p2, v2}, LX/Bsx;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/Bsv;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583023
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->l:Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583024
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2583014
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583015
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->p:LX/0qn;

    invoke-static {p1, v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    return v0
.end method
