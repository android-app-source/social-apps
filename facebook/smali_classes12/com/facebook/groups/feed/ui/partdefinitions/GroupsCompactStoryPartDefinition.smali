.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582807
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582808
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->a:LX/0Ot;

    .line 2582809
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->b:LX/0Ot;

    .line 2582810
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;
    .locals 5

    .prologue
    .line 2582788
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;

    monitor-enter v1

    .line 2582789
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582790
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582793
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;

    const/16 v4, 0x1eb5

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x8f5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2582794
    move-object v0, v3

    .line 2582795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2582799
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 2582800
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v2, LX/C33;

    sget-object v3, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    .line 2582801
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2582802
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v4, v3, p2, v1}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582803
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582804
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582805
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582806
    invoke-static {p1}, LX/14w;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
