.class public Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

.field private final b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

.field private final c:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

.field private final d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582647
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582648
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    .line 2582649
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    .line 2582650
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    .line 2582651
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    .line 2582652
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;
    .locals 7

    .prologue
    .line 2582653
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 2582654
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582655
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582656
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582657
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582658
    new-instance p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;)V

    .line 2582659
    move-object v0, p0

    .line 2582660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2582664
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582665
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->c:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/CommunityGraphQLStorySelectorPartDefinition;->d:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2582666
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582667
    const/4 v0, 0x1

    return v0
.end method
