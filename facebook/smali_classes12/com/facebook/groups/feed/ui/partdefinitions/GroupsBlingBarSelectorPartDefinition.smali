.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

.field private final b:Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

.field private final c:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582782
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582783
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

    .line 2582784
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    .line 2582785
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->c:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    .line 2582786
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->d:LX/0Uh;

    .line 2582787
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;
    .locals 7

    .prologue
    .line 2582765
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    monitor-enter v1

    .line 2582766
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582767
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582768
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582769
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582770
    new-instance p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;LX/0Uh;)V

    .line 2582771
    move-object v0, p0

    .line 2582772
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582773
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582774
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2582777
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582778
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->d:LX/0Uh;

    const/16 v1, 0xe0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582779
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->c:Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2582780
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2582781
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBlingBarSelectorPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummarySelectorPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582776
    const/4 v0, 0x1

    return v0
.end method
