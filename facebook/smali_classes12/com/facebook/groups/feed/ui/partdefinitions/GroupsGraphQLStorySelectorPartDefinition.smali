.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0qn;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0qn;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsCompactStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsEdgeStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;",
            ">;",
            "LX/0qn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582859
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2582860
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->a:LX/0Ot;

    .line 2582861
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->b:LX/0Ot;

    .line 2582862
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->c:LX/0Ot;

    .line 2582863
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->d:LX/0Ot;

    .line 2582864
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->e:LX/0Ot;

    .line 2582865
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->f:LX/0Ot;

    .line 2582866
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->g:LX/0qn;

    .line 2582867
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;
    .locals 11

    .prologue
    .line 2582848
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 2582849
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582850
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582851
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582852
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582853
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;

    const/16 v4, 0x2434

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x243f

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x243d

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2436

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2437

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2440

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v10

    check-cast v10, LX/0qn;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0qn;)V

    .line 2582854
    move-object v0, v3

    .line 2582855
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582856
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582857
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2582834
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2582835
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->g:LX/0qn;

    invoke-static {p2, v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsOfflineGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0qn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582836
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2582837
    :goto_0
    return-object v1

    .line 2582838
    :cond_0
    invoke-static {p2}, LX/14w;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2582839
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2582840
    :cond_1
    invoke-static {p2}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2582841
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2582842
    :cond_2
    invoke-static {p2}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSharedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2582843
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2582844
    :cond_3
    invoke-static {p2}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2582845
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2582846
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2582847
    const/4 v0, 0x1

    return v0
.end method
