.class public Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

.field private final g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583203
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2583204
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->a:Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    .line 2583205
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    .line 2583206
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 2583207
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2583208
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 2583209
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->f:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    .line 2583210
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    .line 2583211
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;
    .locals 11

    .prologue
    .line 2583192
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;

    monitor-enter v1

    .line 2583193
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583194
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583195
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583196
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583197
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;-><init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;)V

    .line 2583198
    move-object v0, v3

    .line 2583199
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583200
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583201
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583202
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2583213
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583214
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->g:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->f:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583215
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583216
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583217
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583218
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583219
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;->a:Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2583220
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2583212
    const/4 v0, 0x1

    return v0
.end method
