.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

.field private final b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583057
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2583058
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    .line 2583059
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    .line 2583060
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;
    .locals 5

    .prologue
    .line 2583061
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;

    monitor-enter v1

    .line 2583062
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583063
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583064
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583065
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583066
    new-instance p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;)V

    .line 2583067
    move-object v0, p0

    .line 2583068
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583069
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583070
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2583072
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583073
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    new-instance v1, LX/DDe;

    invoke-direct {v1, p2}, LX/DDe;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2583074
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2583075
    const/4 v0, 0x1

    return v0
.end method
