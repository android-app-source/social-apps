.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/IVO;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/IVO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583115
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2583116
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->d:LX/1V0;

    .line 2583117
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->e:LX/IVO;

    .line 2583118
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2583119
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->e:LX/IVO;

    const/4 v1, 0x0

    .line 2583120
    new-instance v2, LX/IVN;

    invoke-direct {v2, v0}, LX/IVN;-><init>(LX/IVO;)V

    .line 2583121
    iget-object v3, v0, LX/IVO;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IVM;

    .line 2583122
    if-nez v3, :cond_0

    .line 2583123
    new-instance v3, LX/IVM;

    invoke-direct {v3, v0}, LX/IVM;-><init>(LX/IVO;)V

    .line 2583124
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/IVM;->a$redex0(LX/IVM;LX/1De;IILX/IVN;)V

    .line 2583125
    move-object v2, v3

    .line 2583126
    move-object v1, v2

    .line 2583127
    move-object v0, v1

    .line 2583128
    iget-object v1, v0, LX/IVM;->a:LX/IVN;

    iput-object p3, v1, LX/IVN;->a:LX/1Pn;

    .line 2583129
    iget-object v1, v0, LX/IVM;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2583130
    move-object v0, v0

    .line 2583131
    iget-object v1, v0, LX/IVM;->a:LX/IVN;

    iput-object p2, v1, LX/IVN;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583132
    iget-object v1, v0, LX/IVM;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2583133
    move-object v0, v0

    .line 2583134
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2583135
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2583136
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;
    .locals 6

    .prologue
    .line 2583137
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;

    monitor-enter v1

    .line 2583138
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583139
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583140
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583141
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583142
    new-instance p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/IVO;->a(LX/0QB;)LX/IVO;

    move-result-object v5

    check-cast v5, LX/IVO;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/IVO;)V

    .line 2583143
    move-object v0, p0

    .line 2583144
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583145
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583146
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2583148
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2583149
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2583150
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2583151
    :goto_0
    return v0

    .line 2583152
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 2583153
    goto :goto_0

    .line 2583154
    :cond_3
    invoke-static {p0}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2583155
    goto :goto_0

    .line 2583156
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2583157
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2583158
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2583159
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583160
    invoke-static {p1}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsSubStoriesHScrollComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583161
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2583162
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 2583163
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object p0

    .line 2583164
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    move v3, v2

    :goto_0
    if-ge v3, p1, :cond_2

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2583165
    invoke-static {v1}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2583166
    const/4 v1, 0x1

    .line 2583167
    :goto_1
    move v0, v1

    .line 2583168
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 2583169
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 2583170
    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2583171
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2583172
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
