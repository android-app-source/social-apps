.class public Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pi;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3V4;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition",
            "<TE;",
            "LX/3V4;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1VE;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;LX/1VE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582925
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2582926
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2582927
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->c:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 2582928
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    .line 2582929
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    .line 2582930
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    .line 2582931
    iput-object p6, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    .line 2582932
    iput-object p7, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->g:LX/1VE;

    .line 2582933
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;
    .locals 11

    .prologue
    .line 2582914
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    monitor-enter v1

    .line 2582915
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582916
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582917
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582918
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582919
    new-instance v3, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v10

    check-cast v10, LX/1VE;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;LX/1VE;)V

    .line 2582920
    move-object v0, v3

    .line 2582921
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582922
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582923
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2582934
    sget-object v0, LX/3V4;->m:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2582900
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582901
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->c:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2582902
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->g:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2582903
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2582904
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->g:LX/1VE;

    invoke-virtual {v0, p2}, LX/1VE;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    .line 2582905
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->g:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    .line 2582906
    const v2, 0x7f0d160d

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    new-instance v4, LX/BsB;

    invoke-direct {v4, p2, v1, v0}, LX/BsB;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2582907
    const v0, 0x7f0d1616

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->b:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2582908
    const v0, 0x7f0d1615

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    new-instance v2, LX/Bry;

    const/4 v3, -0x1

    invoke-direct {v2, p2, v3}, LX/Bry;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2582909
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2582910
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582911
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582912
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582913
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
