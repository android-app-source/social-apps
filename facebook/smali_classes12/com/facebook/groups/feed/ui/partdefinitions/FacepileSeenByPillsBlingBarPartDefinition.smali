.class public Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

.field private final b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582686
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2582687
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    .line 2582688
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    .line 2582689
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->c:LX/0ad;

    .line 2582690
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;
    .locals 6

    .prologue
    .line 2582691
    const-class v1, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 2582692
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582693
    sput-object v2, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582694
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582695
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582696
    new-instance p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;LX/0ad;)V

    .line 2582697
    move-object v0, p0

    .line 2582698
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582699
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582700
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2582702
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2582703
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582704
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582705
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2582706
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2582707
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2582708
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2582709
    const/4 v0, 0x0

    .line 2582710
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->c:LX/0ad;

    sget-short v2, LX/9Lj;->d:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/partdefinitions/FacepileSeenByPillsBlingBarPartDefinition;->c:LX/0ad;

    sget-short v2, LX/88j;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
