.class public Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/DNB;


# instance fields
.field public a:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Sz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DNh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DOK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HideApprovedStoryPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/9Mz;

.field private t:Lcom/facebook/api/feedtype/FeedType;

.field public u:LX/1Qq;

.field public v:LX/DNp;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575455
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;LX/DNT;LX/9Sz;LX/DNq;LX/DNh;LX/DNJ;LX/DNR;LX/DOK;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/0Ot;LX/IRJ;LX/1g8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;",
            "LX/DNT;",
            "LX/9Sz;",
            "LX/DNq;",
            "LX/DNh;",
            "LX/DNJ;",
            "LX/DNR;",
            "LX/DOK;",
            "LX/DOp;",
            "LX/IVs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HideApprovedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/ModerationGroupPartDefinition;",
            ">;",
            "LX/IRJ;",
            "LX/1g8;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2575456
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->a:LX/DNT;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->b:LX/9Sz;

    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->c:LX/DNq;

    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->d:LX/DNh;

    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    iput-object p6, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    iput-object p7, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->g:LX/DOK;

    iput-object p8, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->h:LX/DOp;

    iput-object p9, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->i:LX/IVs;

    iput-object p10, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->m:LX/IRJ;

    iput-object p14, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->n:LX/1g8;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;

    invoke-static {v14}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v1

    check-cast v1, LX/DNT;

    const-class v2, LX/9Sz;

    invoke-interface {v14, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9Sz;

    const-class v3, LX/DNq;

    invoke-interface {v14, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DNq;

    invoke-static {v14}, LX/DNh;->b(LX/0QB;)LX/DNh;

    move-result-object v4

    check-cast v4, LX/DNh;

    invoke-static {v14}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v5

    check-cast v5, LX/DNJ;

    invoke-static {v14}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v6

    check-cast v6, LX/DNR;

    invoke-static {v14}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v7

    check-cast v7, LX/DOK;

    invoke-static {v14}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v8

    check-cast v8, LX/DOp;

    invoke-static {v14}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v9

    check-cast v9, LX/IVs;

    const/16 v10, 0x2438

    invoke-static {v14, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2412

    invoke-static {v14, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2442

    invoke-static {v14, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const-class v13, LX/IRJ;

    invoke-interface {v14, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/IRJ;

    invoke-static {v14}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v14

    check-cast v14, LX/1g8;

    invoke-static/range {v0 .. v14}, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->a(Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;LX/DNT;LX/9Sz;LX/DNq;LX/DNh;LX/DNJ;LX/DNR;LX/DOK;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/0Ot;LX/IRJ;LX/1g8;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2575457
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->m:LX/IRJ;

    sget-object v1, LX/IRH;->PENDING:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2575458
    sget-object v3, LX/DOu;->a:LX/DOu;

    move-object v3, v3

    .line 2575459
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment$3;-><init>(Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2575483
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->w:Z

    if-eqz v0, :cond_0

    .line 2575484
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->h:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->i:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->l:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->k:LX/0Ot;

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->u:LX/1Qq;

    .line 2575485
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->u:LX/1Qq;

    return-object v0

    .line 2575486
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->h:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->i:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->j:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->k:LX/0Ot;

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->u:LX/1Qq;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2575445
    const-string v0, "pending_posts_admin"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2575460
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2575461
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2575462
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->s:LX/9Mz;

    .line 2575463
    const/4 v0, 0x1

    .line 2575464
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2575465
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v2

    invoke-interface {v2}, LX/9Mz;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    if-eq v1, v2, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v2

    invoke-interface {v2}, LX/9Mz;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 2575466
    :cond_0
    :goto_0
    move v0, v0

    .line 2575467
    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->w:Z

    .line 2575468
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->g:LX/DOK;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->s:LX/9Mz;

    invoke-virtual {v0, v1, v2}, LX/DOK;->a(Ljava/lang/String;LX/9Mz;)V

    .line 2575469
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2575470
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2575471
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2575472
    move-object v1, v0

    .line 2575473
    sget-object v2, LX/B1T;->PendingPosts:LX/B1T;

    .line 2575474
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2575475
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->r:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->t:Lcom/facebook/api/feedtype/FeedType;

    .line 2575476
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->a:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->t:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2575477
    return-void

    .line 2575478
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2575479
    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2575480
    const-string v2, "group_is_viewer_admin"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2575481
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2575482
    const-string v1, "group_is_viewer_admin"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2575488
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2575489
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2575490
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081bc4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575491
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2575487
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2575453
    return-void
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 1

    .prologue
    .line 2575454
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2575452
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2575451
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2575449
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2575450
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2575448
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2575446
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2575447
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2575443
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2575444
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2575440
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2575441
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2575442
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x3da2477f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2575413
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2575414
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->a:LX/DNT;

    new-instance v3, LX/IQb;

    invoke-direct {v3, p0}, LX/IQb;-><init>(Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2575415
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    .line 2575416
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2575417
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->e:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2575418
    new-instance v0, LX/IQc;

    invoke-direct {v0, p0}, LX/IQc;-><init>(Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;)V

    .line 2575419
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->d:LX/DNh;

    .line 2575420
    iget-object v2, v3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 2575421
    const/4 v4, 0x1

    .line 2575422
    iget-object v5, v1, LX/DNh;->a:LX/0bH;

    iget-object v6, v1, LX/DNh;->b:LX/DNg;

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b2;)Z

    .line 2575423
    iput-object v2, v1, LX/DNh;->d:LX/0qq;

    .line 2575424
    iput-object v0, v1, LX/DNh;->e:LX/IQc;

    .line 2575425
    iput-boolean v4, v1, LX/DNh;->f:Z

    .line 2575426
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->c:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->t:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->b:LX/9Sz;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->t:Lcom/facebook/api/feedtype/FeedType;

    .line 2575427
    new-instance v7, LX/9Sy;

    invoke-static {v4}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v6

    check-cast v6, LX/0pn;

    invoke-direct {v7, v5, v6}, LX/9Sy;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2575428
    move-object v4, v7

    .line 2575429
    invoke-virtual {v1, v2, v4, v3, v0}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->v:LX/DNp;

    .line 2575430
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->n:LX/1g8;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->s:LX/9Mz;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2575431
    :goto_0
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "admin_panel_pending_posts_view"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "pending_posts_admin"

    .line 2575432
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2575433
    move-object v3, v3

    .line 2575434
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2575435
    if-eqz v0, :cond_0

    .line 2575436
    const-string v4, "viewer_admin_type"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2575437
    :cond_0
    iget-object v4, v1, LX/1g8;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2575438
    const v0, -0x5734a043

    invoke-static {v0, v11}, LX/02F;->f(II)V

    return-object v10

    .line 2575439
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->s:LX/9Mz;

    invoke-interface {v0}, LX/9Mz;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3f5495fa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575406
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2575407
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->d:LX/DNh;

    .line 2575408
    iget-object v2, v1, LX/DNh;->a:LX/0bH;

    iget-object v4, v1, LX/DNh;->b:LX/DNg;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2575409
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->v:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2575410
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->f:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2575411
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;->u:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2575412
    const/16 v1, 0x2b

    const v2, 0x60e114c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6028fde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2575401
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2575402
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2575403
    if-eqz v0, :cond_0

    .line 2575404
    const v2, 0x7f081bb0

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2575405
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x4d9d14e2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
