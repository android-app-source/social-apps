.class public abstract Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0o2;


# instance fields
.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private b:Ljava/lang/String;

.field public o:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568110
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2568111
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2568112
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568113
    if-eqz v0, :cond_0

    .line 2568114
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568115
    const-string v1, "group_feed_model"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2568116
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568117
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->b:Ljava/lang/String;

    .line 2568118
    :cond_0
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;

    invoke-static {v0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v2

    check-cast v2, LX/8ht;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object p1

    check-cast p1, LX/3my;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->o:LX/8ht;

    iput-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->p:LX/0Uh;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->q:LX/3my;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->r:LX/0ad;

    .line 2568119
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    .line 2568120
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->o:LX/8ht;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->p:LX/0Uh;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->r:LX/0ad;

    invoke-static/range {v0 .. v5}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2568121
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()LX/9Mz;
    .locals 1

    .prologue
    .line 2568122
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
