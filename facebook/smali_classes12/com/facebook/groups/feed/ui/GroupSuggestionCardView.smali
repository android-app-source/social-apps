.class public Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/F2N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/IK1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/ISp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Landroid/widget/Button;

.field public o:Lcom/facebook/fbui/glyph/GlyphView;

.field public p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public r:Z

.field public s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2575557
    const-class v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2575558
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2575559
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575560
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2575561
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575562
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2575563
    const v0, 0x7f03086b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2575564
    const v0, 0x7f0d0a0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->k:Landroid/widget/TextView;

    .line 2575565
    const v0, 0x7f0d15e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->l:Landroid/widget/TextView;

    .line 2575566
    const v0, 0x7f0d15e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->m:Landroid/widget/TextView;

    .line 2575567
    const v0, 0x7f0d15ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->n:Landroid/widget/Button;

    .line 2575568
    const v0, 0x7f0d15e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->o:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2575569
    const v0, 0x7f0d15e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2575570
    const v0, 0x7f0d15e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2575571
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p2

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-static {p2}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {p2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {p2}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v6

    check-cast v6, LX/1Kf;

    invoke-static {p2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p2}, LX/Jaq;->b(LX/0QB;)LX/Jaq;

    move-result-object v8

    check-cast v8, LX/DVF;

    invoke-static {p2}, LX/F2N;->b(LX/0QB;)LX/F2N;

    move-result-object v9

    check-cast v9, LX/F2N;

    new-instance v1, LX/IK1;

    invoke-direct {v1}, LX/IK1;-><init>()V

    invoke-static {p2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-static {p2}, LX/88n;->b(LX/0QB;)LX/88n;

    move-result-object p3

    check-cast p3, LX/88n;

    invoke-static {p2}, LX/IK8;->b(LX/0QB;)LX/IK8;

    move-result-object v0

    check-cast v0, LX/IK8;

    invoke-static {p2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p0

    check-cast p0, LX/0kL;

    iput-object p1, v1, LX/IK1;->b:LX/0tX;

    iput-object p3, v1, LX/IK1;->c:LX/88n;

    iput-object v0, v1, LX/IK1;->d:LX/IK8;

    iput-object p0, v1, LX/IK1;->e:LX/0kL;

    move-object p1, v1

    check-cast p1, LX/IK1;

    invoke-static {p2}, LX/ISp;->b(LX/0QB;)LX/ISp;

    move-result-object p2

    check-cast p2, LX/ISp;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->a:LX/17W;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->c:LX/0W9;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->d:LX/1Kf;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->f:LX/DVF;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->g:LX/F2N;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->h:LX/IK1;

    iput-object p2, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->i:LX/ISp;

    .line 2575572
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
        .annotation build Lcom/facebook/graphql/calls/AYMTTipEventType;
        .end annotation
    .end param

    .prologue
    .line 2575573
    new-instance v0, LX/B1H;

    invoke-direct {v0}, LX/B1H;-><init>()V

    move-object v1, v0

    .line 2575574
    new-instance v0, LX/4D5;

    invoke-direct {v0}, LX/4D5;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4D5;->a(Ljava/lang/String;)LX/4D5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4D5;->b(Ljava/lang/String;)LX/4D5;

    move-result-object v0

    const-string v2, "groups_mall_mobile"

    .line 2575575
    const-string v3, "surface"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2575576
    move-object v2, v0

    .line 2575577
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->b()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->hR_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4D5;->c(Ljava/lang/String;)LX/4D5;

    move-result-object v0

    .line 2575578
    const-string v2, "CLICK"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2575579
    const-string v2, "primary"

    invoke-virtual {v0, v2}, LX/4D5;->d(Ljava/lang/String;)LX/4D5;

    .line 2575580
    :cond_0
    :goto_0
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2575581
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2575582
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2575583
    new-instance v1, LX/IQg;

    invoke-direct {v1, p0}, LX/IQg;-><init>(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2575584
    return-void

    .line 2575585
    :cond_1
    const-string v2, "IMPRESSION"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2575586
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->r:Z

    goto :goto_0
.end method
