.class public Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.source ""


# instance fields
.field public a:LX/9Sv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IRx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2578396
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;

    const-class v1, LX/9Sv;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9Sv;

    const-class p0, LX/IRx;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/IRx;

    iput-object v1, p1, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->a:LX/9Sv;

    iput-object v2, p1, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->b:LX/IRx;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
    .locals 2

    .prologue
    .line 2578397
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->a:LX/9Sv;

    .line 2578398
    new-instance p0, LX/9Su;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v1

    check-cast v1, LX/0pn;

    invoke-direct {p0, p1, v1}, LX/9Su;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2578399
    move-object v0, p0

    .line 2578400
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2578401
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2578402
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2578403
    const-string v1, "group_learning_unit_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->c:Ljava/lang/String;

    .line 2578404
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2578405
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2578406
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2578407
    sget-object v1, LX/B1T;->GroupLearningUnitPosts:LX/B1T;

    .line 2578408
    iput-object v1, v0, LX/B1S;->b:LX/B1T;

    .line 2578409
    move-object v1, v0

    .line 2578410
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->c:Ljava/lang/String;

    .line 2578411
    iput-object v2, v1, LX/B1S;->g:Ljava/lang/String;

    .line 2578412
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->t:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final d()LX/DNC;
    .locals 9

    .prologue
    .line 2578413
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsLearningUnitFragment;->b:LX/IRx;

    .line 2578414
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    move-object v1, v1

    .line 2578415
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v2, v2

    .line 2578416
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081baf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2578417
    new-instance v4, LX/ISq;

    .line 2578418
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2578419
    const-string v6, "group_feed_title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2578420
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v6

    .line 2578421
    const-string v7, "feed_description"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2578422
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2578423
    const-string v8, "learning_unit_progress"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, LX/ISq;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v4, v4

    .line 2578424
    sget-object v5, LX/DOs;->a:LX/DOs;

    move-object v5, v5

    .line 2578425
    invoke-virtual/range {v0 .. v5}, LX/IRx;->a(LX/DNR;LX/DNJ;Ljava/lang/String;LX/1Cv;LX/1PT;)LX/IRw;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2578426
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081b7d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
