.class public Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IQn;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0hy;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/03V;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/IQj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/03V;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2575690
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2575691
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->a:LX/0tX;

    .line 2575692
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->b:Ljava/util/concurrent/Executor;

    .line 2575693
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->c:LX/0hy;

    .line 2575694
    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2575695
    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->e:LX/03V;

    .line 2575696
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    .line 2575697
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2575698
    const/4 v3, 0x0

    .line 2575699
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030144

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2575700
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter$1;-><init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2575701
    new-instance v1, LX/IQn;

    invoke-direct {v1, v0}, LX/IQn;-><init>(Landroid/view/View;)V

    .line 2575702
    new-instance v0, LX/IQk;

    invoke-direct {v0, p0, v1}, LX/IQk;-><init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;LX/IQn;)V

    .line 2575703
    iget-object v2, v1, LX/IQn;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2575704
    iget-object v2, v1, LX/IQn;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2575705
    iget-object v0, v1, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/IQm;

    invoke-direct {v2, p0, v1}, LX/IQm;-><init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;LX/IQn;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2575706
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2575707
    check-cast p1, LX/IQn;

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2575708
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IQj;

    .line 2575709
    iput-object v0, p1, LX/IQn;->p:LX/IQj;

    .line 2575710
    iget-object v1, p1, LX/IQn;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/IQj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575711
    iget v1, v0, LX/IQj;->f:I

    if-le v1, v7, :cond_0

    .line 2575712
    iget-object v1, p1, LX/IQn;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p1, LX/IQn;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00d0

    iget v4, v0, LX/IQj;->f:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, LX/IQj;->d:Ljava/lang/String;

    aput-object v6, v5, v8

    iget v6, v0, LX/IQj;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575713
    :goto_0
    iget-object v2, p1, LX/IQn;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, v0, LX/IQj;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/IQj;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_1
    const-class v3, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2575714
    iget-boolean v0, v0, LX/IQj;->g:Z

    if-eqz v0, :cond_2

    .line 2575715
    iget-object v0, p1, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f081c39

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2575716
    iget-object v0, p1, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v8}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2575717
    :goto_2
    return-void

    .line 2575718
    :cond_0
    iget-object v1, p1, LX/IQn;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/IQj;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2575719
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2575720
    :cond_2
    iget-object v0, p1, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f081c38

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2575721
    iget-object v0, p1, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v7}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2575722
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
