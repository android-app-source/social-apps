.class public Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/DNB;


# instance fields
.field public a:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Rc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9Rg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9Re;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DOK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsYourPostsGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Lcom/facebook/api/feedtype/FeedType;

.field public n:LX/1Qq;

.field public s:LX/DNp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2580912
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2580938
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580939
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 2580940
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->m:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 2580935
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580936
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 2580937
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->n:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2580932
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->l:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2580933
    sget-object v3, LX/DOr;->a:LX/DOr;

    move-object v3, v3

    .line 2580934
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment$3;-><init>(Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2580930
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->i:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->j:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->k:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->n:LX/1Qq;

    .line 2580931
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->n:LX/1Qq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2580929
    const-string v0, "owner_authored_posts"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2580922
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2580923
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;

    invoke-static {p1}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v3

    check-cast v3, LX/DNT;

    const-class v4, LX/9Rc;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9Rc;

    const-class v5, LX/9Rg;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9Rg;

    const-class v6, LX/9Re;

    invoke-interface {p1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/9Re;

    const-class v7, LX/DNq;

    invoke-interface {p1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DNq;

    invoke-static {p1}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v8

    check-cast v8, LX/DNJ;

    invoke-static {p1}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v9

    check-cast v9, LX/DNR;

    invoke-static {p1}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v10

    check-cast v10, LX/DOK;

    invoke-static {p1}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v11

    check-cast v11, LX/DOp;

    invoke-static {p1}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v12

    check-cast v12, LX/IVs;

    const/16 v13, 0x2441

    invoke-static {p1, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v0, LX/IRJ;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IRJ;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->a:LX/DNT;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->b:LX/9Rc;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->c:LX/9Rg;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->d:LX/9Re;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->e:LX/DNq;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    iput-object v10, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->h:LX/DOK;

    iput-object v11, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->i:LX/DOp;

    iput-object v12, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->j:LX/IVs;

    iput-object v13, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->k:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->l:LX/IRJ;

    .line 2580924
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->h:LX/DOK;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DOK;->a(Ljava/lang/String;LX/9Mz;)V

    .line 2580925
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2580926
    const-string v1, "feed_type_arguments_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedtype/FeedType;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580927
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->a:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2580928
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2580913
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2580914
    const v0, 0x7f081bd9

    move v1, v0

    .line 2580915
    :goto_0
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2580916
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2580917
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2580918
    return-void

    .line 2580919
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2580920
    const v0, 0x7f081bda

    move v1, v0

    goto :goto_0

    .line 2580921
    :cond_1
    const v0, 0x7f081bdb

    move v1, v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2580868
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2580911
    return-void
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 1

    .prologue
    .line 2580941
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2580910
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2580909
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2580906
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2580907
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2580908
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2580905
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2580903
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2580904
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2580901
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2580902
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x55dc7e63

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2580880
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2580881
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->a:LX/DNT;

    new-instance v3, LX/IUe;

    invoke-direct {v3, p0}, LX/IUe;-><init>(Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2580882
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    .line 2580883
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2580884
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2580885
    new-instance v1, LX/IUf;

    invoke-direct {v1, p0}, LX/IUf;-><init>(Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;)V

    .line 2580886
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2580887
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->b:LX/9Rc;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580888
    new-instance v5, LX/9Rb;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v4

    check-cast v4, LX/0pn;

    invoke-direct {v5, v2, v4}, LX/9Rb;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2580889
    move-object v0, v5

    .line 2580890
    :goto_0
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->e:LX/DNq;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2, v4, v0, v3, v1}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->s:LX/DNp;

    .line 2580891
    const v0, 0x31d8df2

    invoke-static {v0, v11}, LX/02F;->f(II)V

    return-object v10

    .line 2580892
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2580893
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->c:LX/9Rg;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580894
    new-instance v5, LX/9Rf;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v4

    check-cast v4, LX/0pn;

    invoke-direct {v5, v2, v4}, LX/9Rf;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2580895
    move-object v0, v5

    .line 2580896
    goto :goto_0

    .line 2580897
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->d:LX/9Re;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2580898
    new-instance v5, LX/9Rd;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v4

    check-cast v4, LX/0pn;

    invoke-direct {v5, v2, v4}, LX/9Rd;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2580899
    move-object v0, v5

    .line 2580900
    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x110da286

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580874
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2580875
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->s:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2580876
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->g:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2580877
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->n:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2580878
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->n:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2580879
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x6d5de98f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56484e0c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580869
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2580870
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2580871
    if-nez v1, :cond_0

    .line 2580872
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x4efc23cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2580873
    :cond_0
    const v2, 0x7f081b78

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method
