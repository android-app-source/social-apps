.class public Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/25T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field private g:LX/IQq;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2575783
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2575784
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->h:Z

    .line 2575785
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->b()V

    .line 2575786
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575787
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2575788
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->h:Z

    .line 2575789
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->b()V

    .line 2575790
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575791
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2575792
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->h:Z

    .line 2575793
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->b()V

    .line 2575794
    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;LX/0tX;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;LX/25T;)V
    .locals 0

    .prologue
    .line 2575795
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a:LX/0tX;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->c:LX/03V;

    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->d:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->e:LX/25T;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v5}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    new-instance v6, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v5}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v5}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v9

    check-cast v9, LX/0hy;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-direct/range {v6 .. v11}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;-><init>(LX/0tX;Ljava/util/concurrent/Executor;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/03V;)V

    move-object v4, v6

    check-cast v4, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-static {v5}, LX/25T;->a(LX/0QB;)LX/25T;

    move-result-object v5

    check-cast v5, LX/25T;

    invoke-static/range {v0 .. v5}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;LX/0tX;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;LX/25T;)V

    return-void
.end method

.method private b()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2575796
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2575797
    const v0, 0x7f030145

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2575798
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->setOrientation(I)V

    .line 2575799
    const v0, 0x7f0d0612

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2575800
    new-instance v1, LX/IQq;

    invoke-direct {v1, p0}, LX/IQq;-><init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->g:LX/IQq;

    .line 2575801
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->g:LX/IQq;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2575802
    const v0, 0x7f0d0613

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->f:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2575803
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->e:LX/25T;

    invoke-virtual {v0, v2}, LX/1P1;->b(I)V

    .line 2575804
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->f:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->e:LX/25T;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2575805
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->f:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/62a;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b010f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, LX/62a;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2575806
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->f:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->d:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2575807
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/IQj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2575808
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->g:LX/IQq;

    .line 2575809
    iput-object p1, v0, LX/IQq;->b:Ljava/lang/String;

    .line 2575810
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->d:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    const/4 v3, 0x0

    .line 2575811
    iput-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->f:Ljava/lang/String;

    .line 2575812
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2575813
    :cond_0
    :goto_0
    return-void

    .line 2575814
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 2575815
    const/4 v4, 0x1

    move v2, v3

    .line 2575816
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 2575817
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IQj;

    iget-object p0, v1, LX/IQj;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IQj;

    iget-object v1, v1, LX/IQj;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2575818
    :goto_2
    if-nez v3, :cond_0

    .line 2575819
    :cond_2
    iput-object p2, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->g:Ljava/util/List;

    .line 2575820
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 2575821
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2
.end method
