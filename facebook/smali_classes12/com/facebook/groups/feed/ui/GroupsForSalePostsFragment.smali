.class public Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/DNB;


# instance fields
.field public a:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Ri;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DOK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsProductFeedGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/api/feedtype/FeedType;

.field public l:LX/1Qq;

.field public m:LX/DNp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2577876
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2577877
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->j:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2577878
    sget-object v3, LX/DOr;->a:LX/DOr;

    move-object v3, v3

    .line 2577879
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment$3;-><init>(Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2577880
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->g:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->h:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->i:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->l:LX/1Qq;

    .line 2577881
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->l:LX/1Qq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2577903
    const-string v0, "group_for_sale_posts"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2577882
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2577883
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;

    invoke-static {p1}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v3

    check-cast v3, LX/DNT;

    const-class v4, LX/9Ri;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9Ri;

    const-class v5, LX/DNq;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DNq;

    invoke-static {p1}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v6

    check-cast v6, LX/DNJ;

    invoke-static {p1}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v7

    check-cast v7, LX/DNR;

    invoke-static {p1}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v8

    check-cast v8, LX/DOK;

    invoke-static {p1}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v9

    check-cast v9, LX/DOp;

    invoke-static {p1}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v10

    check-cast v10, LX/IVs;

    const/16 v11, 0x243e

    invoke-static {p1, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const-class v0, LX/IRJ;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IRJ;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->a:LX/DNT;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->b:LX/9Ri;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->c:LX/DNq;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->f:LX/DOK;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->g:LX/DOp;

    iput-object v10, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->h:LX/IVs;

    iput-object v11, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->i:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->j:LX/IRJ;

    .line 2577884
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->f:LX/DOK;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DOK;->a(Ljava/lang/String;LX/9Mz;)V

    .line 2577885
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->q:LX/3my;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    move v0, v0

    .line 2577886
    if-eqz v0, :cond_0

    .line 2577887
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2577888
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2577889
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2577890
    move-object v1, v0

    .line 2577891
    sget-object v2, LX/B1T;->CommununityForSalePosts:LX/B1T;

    .line 2577892
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2577893
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->x:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->k:Lcom/facebook/api/feedtype/FeedType;

    .line 2577894
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->a:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->k:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2577895
    return-void

    .line 2577896
    :cond_0
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2577897
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2577898
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2577899
    move-object v1, v0

    .line 2577900
    sget-object v2, LX/B1T;->AvailableForSalePosts:LX/B1T;

    .line 2577901
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2577902
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->p:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->k:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2577905
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2577906
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2577907
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081bdb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2577908
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2577904
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2577874
    return-void
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 1

    .prologue
    .line 2577875
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2577873
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2577872
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2577869
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2577870
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2577871
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2577868
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2577866
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2577867
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2577864
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2577865
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2577861
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2577862
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2577863
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7e65cab8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2577850
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2577851
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->a:LX/DNT;

    new-instance v3, LX/ISM;

    invoke-direct {v3, p0}, LX/ISM;-><init>(Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2577852
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    .line 2577853
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2577854
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->d:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2577855
    new-instance v0, LX/ISN;

    invoke-direct {v0, p0}, LX/ISN;-><init>(Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;)V

    .line 2577856
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->c:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->k:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->b:LX/9Ri;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->k:Lcom/facebook/api/feedtype/FeedType;

    .line 2577857
    new-instance v7, LX/9Rh;

    invoke-static {v4}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v6

    check-cast v6, LX/0pn;

    invoke-direct {v7, v5, v6}, LX/9Rh;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2577858
    move-object v4, v7

    .line 2577859
    invoke-virtual {v1, v2, v4, v3, v0}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->m:LX/DNp;

    .line 2577860
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, -0x18943530

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v10
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x43f00486

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2577844
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2577845
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->m:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2577846
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->e:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2577847
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->l:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2577848
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsForSalePostsFragment;->l:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2577849
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2db19dde

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x216839b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2577839
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2577840
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2577841
    if-nez v1, :cond_0

    .line 2577842
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x6eb5db1f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2577843
    :cond_0
    const v2, 0x7f081b7a

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method
