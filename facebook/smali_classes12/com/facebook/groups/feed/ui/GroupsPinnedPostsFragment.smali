.class public Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/DNB;


# instance fields
.field public a:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9T1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DOF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DOK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HideUnpinnedStoryPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Lcom/facebook/api/feedtype/FeedType;

.field public n:LX/1Qq;

.field public s:LX/DNp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2578650
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2578713
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->l:LX/IRJ;

    sget-object v1, LX/IRH;->PINNED:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2578714
    sget-object v3, LX/DOv;->a:LX/DOv;

    move-object v3, v3

    .line 2578715
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment$3;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment$3;-><init>(Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2578711
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->h:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->i:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->j:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->k:LX/0Ot;

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->n:LX/1Qq;

    .line 2578712
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->n:LX/1Qq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2578710
    const-string v0, "pinned_posts"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2578698
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2578699
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;

    invoke-static {p1}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v3

    check-cast v3, LX/DNT;

    const-class v4, LX/9T1;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9T1;

    const-class v5, LX/DNq;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DNq;

    invoke-static {p1}, LX/DOF;->b(LX/0QB;)LX/DOF;

    move-result-object v6

    check-cast v6, LX/DOF;

    invoke-static {p1}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v7

    check-cast v7, LX/DNJ;

    invoke-static {p1}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v8

    check-cast v8, LX/DNR;

    invoke-static {p1}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v9

    check-cast v9, LX/DOK;

    invoke-static {p1}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v10

    check-cast v10, LX/DOp;

    invoke-static {p1}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v11

    check-cast v11, LX/IVs;

    const/16 v12, 0x2438

    invoke-static {p1, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2415

    invoke-static {p1, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v0, LX/IRJ;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IRJ;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->a:LX/DNT;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->b:LX/9T1;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->c:LX/DNq;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->d:LX/DOF;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->g:LX/DOK;

    iput-object v10, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->h:LX/DOp;

    iput-object v11, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->i:LX/IVs;

    iput-object v12, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->j:LX/0Ot;

    iput-object v13, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->k:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->l:LX/IRJ;

    .line 2578700
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->g:LX/DOK;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->n()LX/9Mz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DOK;->a(Ljava/lang/String;LX/9Mz;)V

    .line 2578701
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2578702
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2578703
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2578704
    move-object v1, v0

    .line 2578705
    sget-object v2, LX/B1T;->PinnedPosts:LX/B1T;

    .line 2578706
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2578707
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->s:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2578708
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->a:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2578709
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2578694
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2578695
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2578696
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081bc3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2578697
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2578693
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2578692
    return-void
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 1

    .prologue
    .line 2578691
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2578690
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2578689
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2578686
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2578687
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2578688
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2578685
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2578683
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2578684
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2578681
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2578682
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2578678
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2578679
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2578680
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1d4ec2ca

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2578664
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2578665
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->a:LX/DNT;

    new-instance v3, LX/ISy;

    invoke-direct {v3, p0}, LX/ISy;-><init>(Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2578666
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    .line 2578667
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2578668
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->e:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2578669
    new-instance v0, LX/ISz;

    invoke-direct {v0, p0}, LX/ISz;-><init>(Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;)V

    .line 2578670
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->c:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->b:LX/9T1;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->m:Lcom/facebook/api/feedtype/FeedType;

    .line 2578671
    new-instance v7, LX/9T0;

    invoke-static {v4}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v6

    check-cast v6, LX/0pn;

    invoke-direct {v7, v5, v6}, LX/9T0;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2578672
    move-object v4, v7

    .line 2578673
    invoke-virtual {v1, v2, v4, v3, v0}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->s:LX/DNp;

    .line 2578674
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->d:LX/DOF;

    .line 2578675
    iget-object v2, v3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 2578676
    sget-object v3, LX/DOJ;->Unpin:LX/DOJ;

    invoke-virtual {v1, v2, v0, v3}, LX/DOF;->a(LX/0qq;LX/DOD;LX/DOJ;)V

    .line 2578677
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, -0x4b0bcac9

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v10
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f50fa84

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2578657
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2578658
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->s:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2578659
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->f:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2578660
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->d:LX/DOF;

    invoke-virtual {v1}, LX/DOF;->a()V

    .line 2578661
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->n:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2578662
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPinnedPostsFragment;->n:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2578663
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7f26ccf3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x21865ada

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2578651
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2578652
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2578653
    if-eqz v1, :cond_0

    .line 2578654
    const v2, 0x7f081b8b

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2578655
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2578656
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x58b554d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
