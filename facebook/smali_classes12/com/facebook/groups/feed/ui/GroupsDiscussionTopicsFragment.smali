.class public Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Xp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/9Rk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IRx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:LX/1ZF;

.field public x:LX/IRw;

.field public y:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2576259
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;-><init>()V

    return-void
.end method

.method public static y(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V
    .locals 2

    .prologue
    .line 2576256
    new-instance v0, Landroid/content/Intent;

    const-string v1, "group_discussion_topics_mutation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2576257
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->f:LX/0Xp;

    invoke-virtual {v1, v0}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2576258
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
    .locals 2

    .prologue
    .line 2576252
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->h:LX/9Rk;

    .line 2576253
    new-instance p0, LX/9Rj;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v1

    check-cast v1, LX/0pn;

    invoke-direct {p0, p1, v1}, LX/9Rj;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2576254
    move-object v0, p0

    .line 2576255
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2576251
    const-string v0, "DISCUSSION_TOPICS_POSTS"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2576260
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    const/16 v3, 0x15e7

    invoke-static {v11, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v11}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v11}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0Tf;

    const/16 v6, 0x1399

    invoke-static {v11, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v11}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v11}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v8

    check-cast v8, LX/0Xp;

    const/16 v9, 0x12c4

    invoke-static {v11, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const-class v10, LX/9Rk;

    invoke-interface {v11, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/9Rk;

    const-class v0, LX/IRx;

    invoke-interface {v11, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/IRx;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->c:LX/0Tf;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->e:LX/0ad;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->f:LX/0Xp;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->g:LX/0Or;

    iput-object v10, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->h:LX/9Rk;

    iput-object v11, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->i:LX/IRx;

    .line 2576261
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576262
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->n:Ljava/lang/String;

    .line 2576263
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576264
    const-string v1, "groups_discussion_topics_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->s:Ljava/lang/String;

    .line 2576265
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576266
    const-string v1, "group_discussion_topics_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    .line 2576267
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->u:Ljava/lang/String;

    .line 2576268
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576269
    const-string v1, "group_discussion_topics_subscribe_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->v:Ljava/lang/String;

    .line 2576270
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576271
    const-string v1, "group_discussion_topics_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    .line 2576272
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576273
    const-string v1, "group_can_viewer_change_post_topics"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->y:Z

    .line 2576274
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2576275
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2576241
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2576242
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->n:Ljava/lang/String;

    .line 2576243
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2576244
    move-object v1, v0

    .line 2576245
    sget-object v2, LX/B1T;->GroupDiscussionTopicsPosts:LX/B1T;

    .line 2576246
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2576247
    move-object v1, v1

    .line 2576248
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->s:Ljava/lang/String;

    .line 2576249
    iput-object v2, v1, LX/B1S;->e:Ljava/lang/String;

    .line 2576250
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->v:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final d()LX/DNC;
    .locals 9

    .prologue
    .line 2576227
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->i:LX/IRx;

    .line 2576228
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    move-object v1, v1

    .line 2576229
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v2, v2

    .line 2576230
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081bb1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2576231
    new-instance v4, LX/IRE;

    .line 2576232
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2576233
    const-string v6, "group_discussion_topics_title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2576234
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v6

    .line 2576235
    const-string v7, "group_discussion_topics_cover_photo_uri"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2576236
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2576237
    const-string v8, "group_discussion_topics_stories_number"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, LX/IRE;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v4

    .line 2576238
    sget-object v5, LX/DOr;->a:LX/DOr;

    move-object v5, v5

    .line 2576239
    invoke-virtual/range {v0 .. v5}, LX/IRx;->a(LX/DNR;LX/DNJ;Ljava/lang/String;LX/1Cv;LX/1PT;)LX/IRw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->x:LX/IRw;

    .line 2576240
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->x:LX/IRw;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2576226
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final k()V
    .locals 4

    .prologue
    .line 2576212
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    .line 2576213
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    if-nez v0, :cond_1

    .line 2576214
    :cond_0
    :goto_0
    return-void

    .line 2576215
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2576216
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->e:LX/0ad;

    sget-short v1, LX/88j;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2576217
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    const v1, 0x7f020722

    const/4 v2, 0x1

    .line 2576218
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    .line 2576219
    iput v1, v3, LX/108;->i:I

    .line 2576220
    move-object v3, v3

    .line 2576221
    iput-boolean v2, v3, LX/108;->d:Z

    .line 2576222
    move-object v3, v3

    .line 2576223
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    move-object v1, v3

    .line 2576224
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2576225
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    new-instance v1, LX/IR6;

    invoke-direct {v1, p0}, LX/IR6;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2576208
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2576209
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v0, v0

    .line 2576210
    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2576211
    return-void
.end method
