.class public Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/DP5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Lcom/facebook/resources/ui/FbTextView;

.field public k:Lcom/facebook/fbui/glyph/GlyphView;

.field public l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2581268
    const-class v0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2581269
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2581270
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2581271
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->c:LX/0Ot;

    .line 2581272
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2581273
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->d:LX/0Ot;

    .line 2581274
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2581275
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->e:LX/0Ot;

    .line 2581276
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2581277
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->f:LX/0Ot;

    .line 2581278
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-static {v0}, LX/DP5;->b(LX/0QB;)LX/DP5;

    move-result-object v3

    check-cast v3, LX/DP5;

    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb39

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1430

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p1, 0x12c4

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v0

    check-cast v0, LX/1g8;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->b:LX/DP5;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->c:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->d:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->e:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->f:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->g:LX/1g8;

    .line 2581279
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->setOrientation(I)V

    .line 2581280
    const v0, 0x7f030831

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2581281
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2581282
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 2581283
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020c50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2581284
    :goto_0
    const v0, 0x7f0d1567

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2581285
    const v0, 0x7f0d0a22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2581286
    const v0, 0x7f0d1568

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2581287
    const v0, 0x7f0d1569

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2581288
    return-void

    .line 2581289
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020c50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2581290
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    .line 2581291
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->g:LX/1g8;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "suggestion_chaining"

    .line 2581292
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "group_discover_impression"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "group_feed"

    .line 2581293
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2581294
    move-object v4, v4

    .line 2581295
    const-string v5, "group"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2581296
    iget-object v5, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2581297
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2581298
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    if-eqz v0, :cond_4

    .line 2581299
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2581300
    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    .line 2581301
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2581302
    invoke-virtual {v5, v0, v3}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    if-eqz v0, :cond_8

    .line 2581303
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2581304
    invoke-virtual {v5, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v5, v0, v3}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_7

    :goto_2
    if-eqz v2, :cond_a

    .line 2581305
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2581306
    :goto_3
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2581307
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v2, 0x7f020c6c

    invoke-virtual {v0, v2}, LX/1af;->b(I)V

    .line 2581308
    :goto_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/IV2;

    invoke-direct {v2, p0}, LX/IV2;-><init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2581309
    new-instance v0, LX/IV3;

    invoke-direct {v0, p0}, LX/IV3;-><init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;)V

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2581310
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->i:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2581311
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_0

    .line 2581312
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2581313
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2581314
    iget-object v5, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v3, v1}, LX/15i;->j(II)I

    move-result v1

    invoke-static {v0, v1}, LX/DP5;->a(Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2581315
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    .line 2581316
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2581317
    sget-object v1, LX/IV5;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2581318
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2581319
    :goto_6
    return-void

    :cond_1
    move v0, v1

    .line 2581320
    goto :goto_5

    :cond_2
    move v0, v1

    goto :goto_5

    :cond_3
    move v0, v3

    .line 2581321
    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto/16 :goto_1

    :cond_6
    move v0, v3

    goto/16 :goto_1

    :cond_7
    move v2, v3

    goto/16 :goto_2

    :cond_8
    move v2, v3

    goto/16 :goto_2

    .line 2581322
    :cond_9
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v4}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2581323
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_4

    :cond_a
    move-object v2, v4

    goto/16 :goto_3

    .line 2581324
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f020971

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2581325
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f0a00a6

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2581326
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081b99

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 2581327
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f0207d9

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2581328
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f0a008f

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2581329
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->k:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081b9a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
