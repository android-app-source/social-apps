.class public Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fi;
.implements LX/0fj;
.implements LX/0yL;
.implements LX/DNB;
.implements LX/0gr;
.implements LX/0o2;
.implements LX/5vN;
.implements LX/0fw;
.implements LX/63S;
.implements Ljava/util/Observer;


# annotations
.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation


# static fields
.field public static final N:Ljava/lang/String;

.field private static final O:Z


# instance fields
.field public A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/IW5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/IW6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/ISV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/IQU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/IPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/CGV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/ISH;

.field public Q:Lcom/facebook/api/feedtype/FeedType;

.field public R:LX/DNp;

.field private S:Landroid/view/View;

.field public T:LX/IUv;

.field private U:Landroid/view/ViewStub;

.field public V:Landroid/view/ViewStub;

.field private W:Z

.field public X:LX/DOD;

.field public Y:Landroid/view/ViewGroup;

.field private Z:LX/1gl;

.field public a:LX/2lS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:LX/1L6;

.field private ab:LX/1L6;

.field public ac:LX/IPY;

.field public ad:LX/0fx;

.field public ae:LX/DRc;

.field public af:LX/IPj;

.field private ag:LX/DJ6;

.field public ah:LX/ISU;

.field public ai:LX/IQT;

.field public aj:LX/0Yb;

.field public ak:LX/1Qq;

.field public al:Z

.field public am:Z

.field private an:LX/DRf;

.field public b:LX/1CF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1lu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DOF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/9T3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/DNX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0jU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/DKH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/IPZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/IUw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DRd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2580310
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->N:Ljava/lang/String;

    .line 2580311
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->O:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2580300
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2580301
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->W:Z

    .line 2580302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->al:Z

    .line 2580303
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->am:Z

    .line 2580304
    return-void
.end method

.method public static A(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 2

    .prologue
    .line 2580305
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ac:LX/IPY;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    if-nez v0, :cond_0

    .line 2580306
    :goto_0
    return-void

    .line 2580307
    :cond_0
    new-instance v0, LX/IUR;

    invoke-direct {v0, p0}, LX/IUR;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ad:LX/0fx;

    .line 2580308
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ad:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_0
.end method

.method public static C(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z
    .locals 3

    .prologue
    .line 2580309
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->v:LX/0ad;

    sget-short v1, LX/100;->ab:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static E(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z
    .locals 2

    .prologue
    .line 2580279
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580280
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580281
    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    .line 2580282
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EV;

    invoke-virtual {v0}, LX/1EV;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static H(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 3

    .prologue
    .line 2580312
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580313
    invoke-virtual {v0}, LX/DNA;->i()V

    .line 2580314
    iget-object v1, v0, LX/DNA;->y:LX/DNR;

    .line 2580315
    iget-object v2, v1, LX/DNR;->c:LX/0fz;

    move-object v1, v2

    .line 2580316
    iget-object v2, v0, LX/DNA;->p:LX/1CY;

    .line 2580317
    iget-object p0, v1, LX/0fz;->d:LX/0qm;

    move-object p0, p0

    .line 2580318
    invoke-virtual {p0}, LX/0qm;->a()Ljava/util/List;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 2580319
    iget-object v2, v0, LX/DNA;->p:LX/1CY;

    invoke-virtual {v2, v1}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 2580320
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    .line 2580321
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2580322
    :goto_0
    return-void

    .line 2580323
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->an:LX/DRf;

    if-nez v0, :cond_1

    .line 2580324
    new-instance v0, LX/DRf;

    invoke-direct {v0}, LX/DRf;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->an:LX/DRf;

    .line 2580325
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->an:LX/DRf;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580326
    iget-object p0, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, p0

    .line 2580327
    invoke-virtual {v0, v1}, LX/DRf;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    goto :goto_0
.end method

.method public static L(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2580328
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580329
    iget-object v2, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v2

    .line 2580330
    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580331
    iget-boolean v2, v0, LX/DNR;->v:Z

    move v0, v2

    .line 2580332
    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    .line 2580333
    :goto_0
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580334
    iget-object v3, v2, LX/ISU;->i:LX/IW3;

    .line 2580335
    iget-boolean v2, v3, LX/IW3;->n:Z

    move v3, v2

    .line 2580336
    move v2, v3

    .line 2580337
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 2580338
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, v1}, LX/ISU;->c(Z)V

    .line 2580339
    :cond_1
    return-void

    .line 2580340
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2580341
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580342
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580343
    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v1, v0}, LX/DJw;->b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2580344
    const-string v0, "extra_is_composer_intercept_status"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2580345
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2580346
    :goto_0
    return-void

    .line 2580347
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    .line 2580348
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, LX/DJ4;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2580349
    new-instance v3, LX/5Rj;

    invoke-direct {v3}, LX/5Rj;-><init>()V

    .line 2580350
    iput-object v2, v3, LX/5Rj;->d:Ljava/lang/String;

    .line 2580351
    move-object v2, v3

    .line 2580352
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    .line 2580353
    iput-object v3, v2, LX/5Rj;->f:Ljava/lang/String;

    .line 2580354
    move-object v2, v2

    .line 2580355
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 2580356
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2580357
    iput-object v0, v2, LX/5Rj;->e:Ljava/lang/Long;

    .line 2580358
    :cond_1
    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "extra_is_composer_intercept_attachments"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v2}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2580359
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v3, 0x0

    const/16 v4, 0x6dc

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v3, v2, v4, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2580360
    if-nez p1, :cond_0

    .line 2580361
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->N:Ljava/lang/String;

    const-string v3, "Update group header with null value. Looks wrong."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580362
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580363
    iget-object v2, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v2

    .line 2580364
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580365
    iget-object v2, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v2

    .line 2580366
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_c

    :cond_1
    move v0, v1

    .line 2580367
    :goto_0
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    invoke-virtual {v2, p1}, LX/IW5;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2580368
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->G()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->G()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2580369
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580370
    new-instance v2, LX/0ju;

    iget-object v3, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2580371
    iget-object v3, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const v5, 0x7f081bac

    invoke-virtual {v3, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2580372
    iget-object v3, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const v5, 0x7f081bad

    invoke-virtual {v3, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2580373
    iget-object v3, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const v5, 0x7f081bae

    invoke-virtual {v3, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, LX/ISQ;

    invoke-direct {v5, v0}, LX/ISQ;-><init>(LX/ISU;)V

    invoke-virtual {v2, v3, v5}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2580374
    iget-object v3, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const v5, 0x7f081ba8

    invoke-virtual {v3, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, LX/ISR;

    invoke-direct {v5, v0}, LX/ISR;-><init>(LX/ISU;)V

    invoke-virtual {v2, v3, v5}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2580375
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 2580376
    :cond_2
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2580377
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    invoke-virtual {v0, v1}, LX/DNX;->e(Z)V

    .line 2580378
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580379
    iget-object v2, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v2

    .line 2580380
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v0, v2, :cond_e

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2580381
    if-eqz v0, :cond_4

    .line 2580382
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580383
    iget-object v2, v0, LX/DNA;->d:LX/0g8;

    iget-object v3, v0, LX/DNX;->m:LX/1Cw;

    invoke-interface {v2, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2580384
    :cond_4
    const/4 v2, 0x0

    .line 2580385
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580386
    iget-object v3, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v3

    .line 2580387
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    if-nez v3, :cond_f

    :cond_5
    move v0, v2

    .line 2580388
    :goto_2
    move v0, v0

    .line 2580389
    if-eqz v0, :cond_6

    .line 2580390
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580391
    iget-object v2, v0, LX/ISU;->e:LX/0zG;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 2580392
    if-nez v2, :cond_13

    .line 2580393
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v2}, LX/IQT;->c()Z

    move-result v2

    if-nez v2, :cond_d

    move v2, v1

    :goto_4
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580394
    iget-object v3, v1, LX/DNA;->y:LX/DNR;

    invoke-virtual {v3}, LX/DNR;->l()Z

    move-result v3

    move v3, v3

    .line 2580395
    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2580396
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->J()V

    .line 2580397
    if-eqz p1, :cond_8

    .line 2580398
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580399
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580400
    invoke-static {v0}, LX/IQV;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2580401
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v1, 0x7f0d159f

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    .line 2580402
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580403
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2580404
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V

    .line 2580405
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    if-nez v1, :cond_8

    .line 2580406
    invoke-static {v0}, LX/IUw;->a(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;)LX/IUv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    .line 2580407
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ad:LX/0fx;

    if-nez v0, :cond_7

    .line 2580408
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->A(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580409
    :cond_7
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    invoke-virtual {v0}, LX/IUv;->b()V

    .line 2580410
    :cond_8
    :goto_5
    if-eqz p1, :cond_9

    .line 2580411
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0}, LX/ISU;->c()V

    .line 2580412
    :cond_9
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    .line 2580413
    iget-object v1, v0, LX/IQT;->b:LX/IW5;

    .line 2580414
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v2, v2

    .line 2580415
    if-eqz v2, :cond_15

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_15

    iget-object v1, v0, LX/IQT;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_15

    iget-boolean v1, v0, LX/IQT;->n:Z

    if-nez v1, :cond_15

    invoke-static {v2}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v1

    invoke-static {v1}, LX/DJw;->a(LX/DZC;)Z

    move-result v1

    if-nez v1, :cond_15

    invoke-static {v2}, LX/DJw;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v1

    if-eqz v1, :cond_15

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    if-ne v1, v2, :cond_15

    const/4 v1, 0x1

    :goto_6
    move v1, v1

    .line 2580416
    if-eqz v1, :cond_a

    .line 2580417
    iget-object v1, v0, LX/IQT;->b:LX/IW5;

    .line 2580418
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2580419
    new-instance v2, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;

    invoke-direct {v2}, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;-><init>()V

    .line 2580420
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2580421
    iput-object v1, v2, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->n:Ljava/lang/String;

    .line 2580422
    new-instance v1, LX/IQS;

    invoke-direct {v1, v0}, LX/IQS;-><init>(LX/IQT;)V

    .line 2580423
    iput-object v1, v2, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->m:LX/IQR;

    .line 2580424
    iget-object v1, v0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const-string v3, "SUGGEST_PURPOSE_DIALOG"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2580425
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/IQT;->n:Z

    .line 2580426
    :cond_a
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    .line 2580427
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580428
    iget-object v3, v2, LX/DNR;->c:LX/0fz;

    move-object v2, v3

    .line 2580429
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-static {v1, v2, v3}, LX/IPu;->a(ILX/0fz;LX/1Qq;)I

    move-result v1

    .line 2580430
    iput v1, v0, LX/IPj;->v:I

    .line 2580431
    :cond_b
    return-void

    :cond_c
    move v0, v4

    .line 2580432
    goto/16 :goto_0

    :cond_d
    move v2, v4

    .line 2580433
    goto/16 :goto_4

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2580434
    :cond_f
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v3, v5}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v0, v2

    .line 2580435
    goto/16 :goto_2

    .line 2580436
    :cond_10
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    .line 2580437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3my;

    invoke-virtual {v0, v3}, LX/3my;->a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_11
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_12
    move v0, v2

    goto/16 :goto_2

    .line 2580438
    :cond_13
    iget-object v3, v2, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v2, v3

    .line 2580439
    if-eqz v2, :cond_6

    .line 2580440
    invoke-virtual {v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d()V

    .line 2580441
    new-instance v3, LX/ISS;

    invoke-direct {v3, v0}, LX/ISS;-><init>(LX/ISU;)V

    invoke-virtual {v2, v3}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 2580442
    :cond_14
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    if-eqz v0, :cond_8

    .line 2580443
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->T:LX/IUv;

    invoke-virtual {v0}, LX/IUv;->a()V

    goto/16 :goto_5

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_6
.end method

.method public static a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;I)V
    .locals 4

    .prologue
    .line 2580444
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580445
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580446
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2580447
    :cond_0
    :goto_0
    return-void

    .line 2580448
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v1

    .line 2580449
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->b()I

    move-result v2

    .line 2580450
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v3

    invoke-static {v3}, LX/9Oi;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;)LX/9Oi;

    move-result-object v3

    .line 2580451
    add-int/2addr v1, p1

    .line 2580452
    iput v1, v3, LX/9Oi;->a:I

    .line 2580453
    const/4 v1, -0x1

    if-eq v2, v1, :cond_2

    .line 2580454
    add-int v1, v2, p1

    .line 2580455
    iput v1, v3, LX/9Oi;->b:I

    .line 2580456
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2580457
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2580458
    invoke-virtual {v3}, LX/9Oi;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    .line 2580459
    invoke-static {p0, v2, v1, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V

    goto :goto_0

    .line 2580460
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;LX/2lS;LX/1CF;LX/0Ot;LX/1lu;LX/DNT;LX/0Ot;LX/DOF;LX/9T3;LX/DNq;LX/DNX;LX/DNR;LX/0jU;LX/0Ot;LX/0Ot;LX/0bH;LX/0zG;LX/0Ot;LX/1My;LX/DKH;LX/0yc;LX/0Ot;LX/0ad;LX/IPZ;LX/IUw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/1g8;LX/0Ot;LX/0Or;LX/0Ot;LX/IW5;LX/IW6;LX/ISV;LX/IQU;LX/IPk;LX/CGV;LX/0Xl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;",
            "LX/2lS;",
            "LX/1CF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1lu;",
            "LX/DNT;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/DOF;",
            "LX/9T3;",
            "LX/DNq;",
            "LX/DNX;",
            "LX/DNR;",
            "LX/0jU;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/0bH;",
            "LX/0zG;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "LX/1My;",
            "LX/DKH;",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;",
            "LX/0ad;",
            "LX/IPZ;",
            "LX/IUw;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DRd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1g8;",
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;",
            "LX/IW5;",
            "LX/IW6;",
            "LX/ISV;",
            "LX/IQU;",
            "LX/IPk;",
            "LX/CGV;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2580461
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a:LX/2lS;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->b:LX/1CF;

    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d:LX/1lu;

    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->e:LX/DNT;

    iput-object p6, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->g:LX/DOF;

    iput-object p8, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->h:LX/9T3;

    iput-object p9, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->i:LX/DNq;

    iput-object p10, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    iput-object p11, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    iput-object p12, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l:LX/0jU;

    iput-object p13, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->p:LX/0zG;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->r:LX/1My;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->s:LX/DKH;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t:LX/0yc;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->u:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->v:LX/0ad;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->w:LX/IPZ;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->x:LX/IUw;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->y:LX/0Ot;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->z:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->A:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->B:LX/0Uh;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->C:LX/1g8;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->D:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->E:LX/0Or;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->F:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H:LX/IW6;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->I:LX/ISV;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->J:LX/IQU;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->K:LX/IPk;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->L:LX/CGV;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->M:LX/0Xl;

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V
    .locals 3
    .param p0    # Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2580462
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580463
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580464
    new-instance v1, LX/9OZ;

    invoke-direct {v1}, LX/9OZ;-><init>()V

    .line 2580465
    iput-object p1, v1, LX/9OZ;->r:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    .line 2580466
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580467
    iget-object p1, v2, LX/IW5;->c:Ljava/lang/String;

    move-object v2, p1

    .line 2580468
    iput-object v2, v1, LX/9OZ;->A:Ljava/lang/String;

    .line 2580469
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580470
    iget-object p1, v2, LX/IW5;->d:Ljava/lang/String;

    move-object v2, p1

    .line 2580471
    iput-object v2, v1, LX/9OZ;->C:Ljava/lang/String;

    .line 2580472
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object p2, v1, LX/9OZ;->g:LX/15i;

    iput p3, v1, LX/9OZ;->h:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2580473
    invoke-static {v0}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v0

    invoke-virtual {v1}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    .line 2580474
    iput-object v1, v0, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2580475
    move-object v0, v0

    .line 2580476
    invoke-virtual {v0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    .line 2580477
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2580478
    return-void

    .line 2580479
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 42

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v41

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-static/range {v41 .. v41}, LX/2lS;->a(LX/0QB;)LX/2lS;

    move-result-object v3

    check-cast v3, LX/2lS;

    invoke-static/range {v41 .. v41}, LX/1CF;->a(LX/0QB;)LX/1CF;

    move-result-object v4

    check-cast v4, LX/1CF;

    const/16 v5, 0x259

    move-object/from16 v0, v41

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v41 .. v41}, LX/1lu;->a(LX/0QB;)LX/1lu;

    move-result-object v6

    check-cast v6, LX/1lu;

    invoke-static/range {v41 .. v41}, LX/DNT;->a(LX/0QB;)LX/DNT;

    move-result-object v7

    check-cast v7, LX/DNT;

    const/16 v8, 0x19c6

    move-object/from16 v0, v41

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v41 .. v41}, LX/DOF;->a(LX/0QB;)LX/DOF;

    move-result-object v9

    check-cast v9, LX/DOF;

    const-class v10, LX/9T3;

    move-object/from16 v0, v41

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/9T3;

    const-class v11, LX/DNq;

    move-object/from16 v0, v41

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/DNq;

    invoke-static/range {v41 .. v41}, LX/DNX;->a(LX/0QB;)LX/DNX;

    move-result-object v12

    check-cast v12, LX/DNX;

    invoke-static/range {v41 .. v41}, LX/DNR;->a(LX/0QB;)LX/DNR;

    move-result-object v13

    check-cast v13, LX/DNR;

    invoke-static/range {v41 .. v41}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v14

    check-cast v14, LX/0jU;

    const/16 v15, 0x3be

    move-object/from16 v0, v41

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x1061

    move-object/from16 v0, v41

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {v41 .. v41}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v17

    check-cast v17, LX/0bH;

    invoke-static/range {v41 .. v41}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v18

    check-cast v18, LX/0zG;

    const/16 v19, 0x2be

    move-object/from16 v0, v41

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {v41 .. v41}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v20

    check-cast v20, LX/1My;

    invoke-static/range {v41 .. v41}, LX/DKH;->a(LX/0QB;)LX/DKH;

    move-result-object v21

    check-cast v21, LX/DKH;

    invoke-static/range {v41 .. v41}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v22

    check-cast v22, LX/0yc;

    const/16 v23, 0x110

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-static/range {v41 .. v41}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    const-class v25, LX/IPZ;

    move-object/from16 v0, v41

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/IPZ;

    const-class v26, LX/IUw;

    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/IUw;

    const/16 v27, 0x34e8

    move-object/from16 v0, v41

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x2465

    move-object/from16 v0, v41

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x12be

    move-object/from16 v0, v41

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    invoke-static/range {v41 .. v41}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v30

    check-cast v30, LX/0Uh;

    invoke-static/range {v41 .. v41}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v31

    check-cast v31, LX/1g8;

    const/16 v32, 0xb2a

    move-object/from16 v0, v41

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x2367

    move-object/from16 v0, v41

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v33

    const/16 v34, 0xb2c

    move-object/from16 v0, v41

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v41 .. v41}, LX/IW5;->a(LX/0QB;)LX/IW5;

    move-result-object v35

    check-cast v35, LX/IW5;

    invoke-static/range {v41 .. v41}, LX/IW6;->a(LX/0QB;)LX/IW6;

    move-result-object v36

    check-cast v36, LX/IW6;

    const-class v37, LX/ISV;

    move-object/from16 v0, v41

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/ISV;

    const-class v38, LX/IQU;

    move-object/from16 v0, v41

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/IQU;

    const-class v39, LX/IPk;

    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v39

    check-cast v39, LX/IPk;

    invoke-static/range {v41 .. v41}, LX/CGV;->a(LX/0QB;)LX/CGV;

    move-result-object v40

    check-cast v40, LX/CGV;

    invoke-static/range {v41 .. v41}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v41

    check-cast v41, LX/0Xl;

    invoke-static/range {v2 .. v41}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;LX/2lS;LX/1CF;LX/0Ot;LX/1lu;LX/DNT;LX/0Ot;LX/DOF;LX/9T3;LX/DNq;LX/DNX;LX/DNR;LX/0jU;LX/0Ot;LX/0Ot;LX/0bH;LX/0zG;LX/0Ot;LX/1My;LX/DKH;LX/0yc;LX/0Ot;LX/0ad;LX/IPZ;LX/IUw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/1g8;LX/0Ot;LX/0Or;LX/0Ot;LX/IW5;LX/IW6;LX/ISV;LX/IQU;LX/IPk;LX/CGV;LX/0Xl;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;LX/1ZX;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2580618
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580619
    iget-object v2, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v2

    .line 2580620
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2580621
    :goto_0
    return-object v0

    .line 2580622
    :cond_0
    invoke-virtual {p1}, LX/1ZX;->a()Ljava/lang/String;

    move-result-object v2

    .line 2580623
    invoke-virtual {p1}, LX/1ZX;->b()Ljava/lang/String;

    move-result-object v3

    .line 2580624
    if-nez v2, :cond_1

    if-eqz v3, :cond_1

    .line 2580625
    invoke-virtual {v0, v3}, LX/0fz;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 2580626
    :cond_1
    if-eqz v2, :cond_3

    .line 2580627
    invoke-virtual {v0, v2}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2580628
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2580629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2580630
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_2

    .line 2580631
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2580632
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 8

    .prologue
    .line 2580480
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580481
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580482
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2580483
    :cond_0
    :goto_0
    return-void

    .line 2580484
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2580485
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2580486
    const/4 v4, 0x0

    .line 2580487
    if-eqz p1, :cond_2

    if-eqz v1, :cond_2

    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v1, v4, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    if-eqz v3, :cond_2

    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v1, v4, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    :cond_2
    move v3, v4

    .line 2580488
    :goto_1
    move v1, v3

    .line 2580489
    if-eqz v1, :cond_0

    .line 2580490
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 2580491
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V

    goto :goto_0

    .line 2580492
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2580493
    :cond_3
    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v1, v4, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 2580494
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2580495
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2580496
    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    move v3, v4

    .line 2580497
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    .line 2580615
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580616
    iget-object v1, v0, LX/IW5;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2580617
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p3}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2580614
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    invoke-virtual {v0}, LX/DNX;->u()Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2580594
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->e:LX/DNT;

    new-instance v3, LX/IUO;

    invoke-direct {v3, p0}, LX/IUO;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a:LX/2lS;

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580595
    iget-object v8, v0, LX/IW5;->f:Ljava/util/ArrayList;

    move-object v8, v8

    .line 2580596
    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2580597
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580598
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2580599
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580600
    iget-object v1, v0, LX/IW5;->f:Ljava/util/ArrayList;

    move-object v4, v1

    .line 2580601
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    new-instance v7, LX/IUP;

    invoke-direct {v7, p0}, LX/IUP;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v8, 0x1

    :goto_0
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v9

    move-object v4, p0

    move-object v5, p0

    move v6, v10

    invoke-virtual/range {v0 .. v9}, LX/DNX;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2580602
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->g:LX/DOF;

    .line 2580603
    iget-object v1, v3, LX/0fz;->a:LX/0qq;

    move-object v1, v1

    .line 2580604
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->X:LX/DOD;

    sget-object v4, LX/DOJ;->Pin:LX/DOJ;

    invoke-virtual {v0, v1, v2, v4}, LX/DOF;->a(LX/0qq;LX/DOD;LX/DOJ;)V

    .line 2580605
    new-instance v0, LX/IUJ;

    invoke-direct {v0, p0}, LX/IUJ;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580606
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->b:LX/1CF;

    .line 2580607
    iget-object v2, v3, LX/0fz;->d:LX/0qm;

    move-object v2, v2

    .line 2580608
    invoke-virtual {v1, v0, v2}, LX/1CF;->a(LX/1CH;LX/0qm;)V

    .line 2580609
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d:LX/1lu;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Z:LX/1gl;

    .line 2580610
    iget-object v2, v3, LX/0fz;->d:LX/0qm;

    move-object v2, v2

    .line 2580611
    invoke-virtual {v0, v1, v2}, LX/1lu;->a(LX/1gl;LX/0qm;)V

    .line 2580612
    return-void

    :cond_0
    move v8, v10

    .line 2580613
    goto :goto_0
.end method

.method public static t(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2580584
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580585
    iget-object v2, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v2

    .line 2580586
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2580587
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    .line 2580588
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DRd;

    invoke-virtual {v0, v2}, LX/DRd;->a(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2580589
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->al:Z

    move v0, v0

    .line 2580590
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2580591
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2580592
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2580593
    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2580579
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2580580
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2580581
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2580582
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2580583
    const/4 v0, 0x0

    return v0
.end method

.method public final a(LX/0g8;)LX/1Pf;
    .locals 7

    .prologue
    .line 2580574
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H:LX/IW6;

    .line 2580575
    iget-object v1, v0, LX/IW6;->c:LX/IRJ;

    sget-object v2, LX/IRH;->NORMAL:LX/IRH;

    iget-object v3, v0, LX/IW6;->a:Landroid/content/Context;

    .line 2580576
    sget-object v4, LX/DOr;->a:LX/DOr;

    move-object v4, v4

    .line 2580577
    new-instance v5, Lcom/facebook/groups/feed/ui/standalone/StandAloneGroupsFeedControllerResponderDelegate$1;

    invoke-direct {v5, v0}, Lcom/facebook/groups/feed/ui/standalone/StandAloneGroupsFeedControllerResponderDelegate$1;-><init>(LX/IW6;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v1

    move-object v0, v1

    .line 2580578
    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2580569
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H:LX/IW6;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580570
    iget-object v2, v0, LX/IW6;->d:LX/DOp;

    iget-object v3, v0, LX/IW6;->e:LX/IVs;

    iget-object v4, v0, LX/IW6;->f:LX/0Ot;

    iget-object v5, v0, LX/IW6;->g:LX/0Ot;

    invoke-virtual {v3, v4, v5}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v2, p1, v3, p2, v1}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v2

    iput-object v2, v0, LX/IW6;->k:LX/1Qq;

    .line 2580571
    iget-object v2, v0, LX/IW6;->k:LX/1Qq;

    move-object v0, v2

    .line 2580572
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    .line 2580573
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2580568
    const-string v0, "group_feed"

    return-object v0
.end method

.method public final a(LX/69s;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2580567
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2580507
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2580508
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2580509
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580510
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2580511
    const-string v4, "is_tabbed_mall"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v2, LX/IW5;->b:Z

    .line 2580512
    const-string v4, "group_feed_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/IW5;->c:Ljava/lang/String;

    .line 2580513
    const-string v4, "group_feed_title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/IW5;->d:Ljava/lang/String;

    .line 2580514
    const-string v4, "group_tip_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/IW5;->e:Ljava/lang/String;

    .line 2580515
    const-string v4, "group_feed_hoisted_story_ids"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v2, LX/IW5;->f:Ljava/util/ArrayList;

    .line 2580516
    const-string v4, "group_feed_hoisted_comment_ids"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v2, LX/IW5;->g:Ljava/util/ArrayList;

    .line 2580517
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    invoke-virtual {v2, p0}, LX/IW5;->addObserver(Ljava/util/Observer;)V

    .line 2580518
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a:LX/2lS;

    invoke-virtual {v2, v0, v1}, LX/2lS;->a(J)V

    .line 2580519
    if-nez p1, :cond_0

    .line 2580520
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/16 v1, 0x6dc

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Kf;->a(ILandroid/app/Activity;)V

    .line 2580521
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2580522
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2580523
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580524
    iget-object v2, v1, LX/IW5;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2580525
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2580526
    move-object v1, v0

    .line 2580527
    sget-object v2, LX/B1T;->GroupsFeed:LX/B1T;

    .line 2580528
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2580529
    move-object v1, v1

    .line 2580530
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580531
    iget-object v3, v2, LX/IW5;->f:Ljava/util/ArrayList;

    move-object v2, v3

    .line 2580532
    iput-object v2, v1, LX/B1S;->c:Ljava/util/List;

    .line 2580533
    move-object v1, v1

    .line 2580534
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580535
    iget-object v3, v2, LX/IW5;->g:Ljava/util/ArrayList;

    move-object v2, v3

    .line 2580536
    iput-object v2, v1, LX/B1S;->d:Ljava/util/List;

    .line 2580537
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->k:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Q:Lcom/facebook/api/feedtype/FeedType;

    .line 2580538
    new-instance v0, LX/IUM;

    invoke-direct {v0, p0}, LX/IUM;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->X:LX/DOD;

    .line 2580539
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l:LX/0jU;

    invoke-virtual {v0, p0}, LX/0jU;->a(LX/0fi;)V

    .line 2580540
    new-instance v0, LX/IUN;

    invoke-direct {v0, p0}, LX/IUN;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Z:LX/1gl;

    .line 2580541
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->e:LX/DNT;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Q:Lcom/facebook/api/feedtype/FeedType;

    const/16 v3, 0xa

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->v:LX/0ad;

    sget-short v4, LX/88j;->g:S

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2580542
    new-instance v0, LX/IUW;

    invoke-direct {v0, p0}, LX/IUW;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aa:LX/1L6;

    .line 2580543
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aa:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2580544
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d()LX/1Cv;

    .line 2580545
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->I:LX/ISV;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    invoke-virtual {v0, v1, p0}, LX/ISV;->a(LX/ISH;Lcom/facebook/base/fragment/FbFragment;)LX/ISU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580546
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    const/4 v1, 0x0

    .line 2580547
    new-instance v2, LX/IST;

    invoke-direct {v2, v0}, LX/IST;-><init>(LX/ISU;)V

    iput-object v2, v0, LX/ISU;->t:LX/IST;

    .line 2580548
    iget-object v2, v0, LX/ISU;->m:LX/DK3;

    iget-object v3, v0, LX/ISU;->t:LX/IST;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2580549
    iget-object v2, v0, LX/ISU;->m:LX/DK3;

    iget-object v3, v0, LX/ISU;->b:LX/DK4;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2580550
    new-instance v2, LX/ISP;

    invoke-direct {v2, v0}, LX/ISP;-><init>(LX/ISU;)V

    iput-object v2, v0, LX/ISU;->s:LX/DKM;

    .line 2580551
    iget-object v2, v0, LX/ISU;->o:LX/DKL;

    iget-object v3, v0, LX/ISU;->s:LX/DKM;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2580552
    iget-object v2, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    .line 2580553
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2580554
    if-eqz v2, :cond_1

    iget-object v1, v0, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    .line 2580555
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v2

    .line 2580556
    const-string v2, "parent_control_title_bar"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    iput-boolean v1, v0, LX/ISU;->v:Z

    .line 2580557
    new-instance v0, LX/IUV;

    invoke-direct {v0, p0}, LX/IUV;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ab:LX/1L6;

    .line 2580558
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ab:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2580559
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2580560
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->C:LX/1g8;

    .line 2580561
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2580562
    const-string v2, "group_view_referrer"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580563
    iget-object p0, v2, LX/IW5;->c:Ljava/lang/String;

    move-object v2, p0

    .line 2580564
    invoke-virtual {v0, v1, v2}, LX/1g8;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580565
    return-void

    .line 2580566
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2580506
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 2580498
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580499
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v1

    .line 2580500
    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2580501
    iget-object v1, v0, LX/0fz;->a:LX/0qq;

    move-object v1, v1

    .line 2580502
    invoke-virtual {v1, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2580503
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2580504
    :cond_0
    :goto_0
    return-void

    .line 2580505
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2580283
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, p1}, LX/ISU;->c(Z)V

    .line 2580284
    return-void
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2580285
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2580286
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    .line 2580287
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_4

    .line 2580288
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 2580289
    :cond_0
    :goto_0
    move v0, v1

    .line 2580290
    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    move v4, v0

    .line 2580291
    :goto_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580292
    iget-object v3, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v3

    .line 2580293
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v3}, LX/IQT;->c()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_3
    move v3, p1

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2580294
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->J()V

    .line 2580295
    return-void

    :cond_1
    move v0, v5

    .line 2580296
    goto :goto_1

    :cond_2
    move v2, v5

    .line 2580297
    goto :goto_3

    :cond_3
    move v4, p2

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    .line 2580298
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    move v1, v1

    .line 2580299
    goto :goto_0
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2580104
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    invoke-virtual {v0}, LX/DNX;->u()Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2580105
    invoke-virtual {p1}, LX/0kb;->d()Z

    move-result v0

    .line 2580106
    if-eqz v0, :cond_1

    .line 2580107
    invoke-virtual {p2}, LX/4nk;->a()V

    .line 2580108
    :cond_0
    :goto_0
    return v1

    .line 2580109
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580110
    iget-object v2, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v2

    .line 2580111
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v2, v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 2580112
    :goto_1
    if-eqz v0, :cond_4

    sget-object v0, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    :goto_2
    invoke-virtual {p2, v0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0

    .line 2580113
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2580114
    :cond_4
    sget-object v0, LX/DBa;->NO_CONNECTION:LX/DBa;

    goto :goto_2
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2580096
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580097
    iget-object v1, v0, LX/IW5;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2580098
    if-nez v0, :cond_0

    .line 2580099
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2580100
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2580101
    :cond_0
    if-nez v0, :cond_1

    .line 2580102
    const/4 v0, 0x0

    .line 2580103
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "group_id"

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2580092
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2580093
    if-eqz v0, :cond_0

    .line 2580094
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/ISU;->c(Z)V

    .line 2580095
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2580089
    if-eqz p1, :cond_0

    .line 2580090
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v0}, LX/IQT;->b()V

    .line 2580091
    :cond_0
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2580062
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    if-eqz v0, :cond_0

    .line 2580063
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    .line 2580064
    :goto_0
    return-object v0

    .line 2580065
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->B:LX/0Uh;

    const/16 v1, 0x470

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2580066
    if-eqz v0, :cond_1

    .line 2580067
    sget-boolean v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    .line 2580068
    :goto_1
    new-instance v1, LX/DRc;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->p:LX/0zG;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->U:Landroid/view/ViewStub;

    invoke-direct {v1, v2, v3, v4, v0}, LX/DRc;-><init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;I)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    .line 2580069
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    new-instance v1, LX/IUU;

    invoke-direct {v1, p0}, LX/IUU;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580070
    iput-object v1, v0, LX/DRc;->h:LX/DRb;

    .line 2580071
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H:LX/IW6;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    .line 2580072
    iget-object v2, v0, LX/IW6;->i:LX/ISJ;

    iget-object v3, v0, LX/IW6;->b:LX/IW5;

    invoke-virtual {v2, v3, v1}, LX/ISJ;->a(LX/IRb;LX/DRc;)LX/ISI;

    move-result-object v2

    .line 2580073
    iget-object v3, v0, LX/IW6;->b:LX/IW5;

    .line 2580074
    iget-boolean v4, v3, LX/IW5;->b:Z

    move v3, v4

    .line 2580075
    if-nez v3, :cond_4

    .line 2580076
    iget-object v3, v0, LX/IW6;->h:LX/ITk;

    iget-object v4, v0, LX/IW6;->b:LX/IW5;

    invoke-virtual {v3, v4, v1}, LX/ITk;->a(LX/IRb;LX/DRc;)LX/ITj;

    move-result-object v3

    .line 2580077
    new-instance v4, LX/IVw;

    invoke-direct {v4, v3, v2}, LX/IVw;-><init>(LX/ITj;LX/ISI;)V

    iput-object v4, v0, LX/IW6;->j:LX/ISH;

    .line 2580078
    :goto_2
    iget-object v2, v0, LX/IW6;->b:LX/IW5;

    .line 2580079
    iget-object v3, v2, LX/IW5;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2580080
    if-eqz v2, :cond_2

    .line 2580081
    iget-object v2, v0, LX/IW6;->j:LX/ISH;

    iget-object v3, v0, LX/IW6;->b:LX/IW5;

    .line 2580082
    iget-object v4, v3, LX/IW5;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2580083
    invoke-virtual {v2, v3}, LX/ISH;->a(Ljava/lang/String;)V

    .line 2580084
    :cond_2
    iget-object v2, v0, LX/IW6;->j:LX/ISH;

    move-object v0, v2

    .line 2580085
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    .line 2580086
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    goto :goto_0

    .line 2580087
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2580088
    :cond_4
    iput-object v2, v0, LX/IW6;->j:LX/ISH;

    goto :goto_2
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2580054
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2580055
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580056
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2580057
    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/DNA;->g()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2580058
    new-instance v1, LX/5Mj;

    iget-object v2, v0, LX/DNA;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/5Mk;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    .line 2580059
    iget-object v2, v1, LX/5Mj;->f:LX/5Mk;

    move-object v2, v2

    .line 2580060
    invoke-virtual {v0}, LX/DNA;->g()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 2580061
    :cond_0
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2580052
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->J()V

    .line 2580053
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->an:LX/DRf;

    return-object v0
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 2580008
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2580009
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2580010
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d(I)Landroid/view/View;

    move-result-object v1

    .line 2580011
    if-eqz v1, :cond_0

    .line 2580012
    instance-of v0, v1, LX/63R;

    .line 2580013
    :cond_0
    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2580005
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d(I)Landroid/view/View;

    move-result-object v1

    .line 2580006
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2580007
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2580017
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->W:Z

    .line 2580018
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2580019
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 2

    .prologue
    .line 2580020
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/ISU;->b(Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2580014
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580015
    iget-object p0, v0, LX/DNA;->d:LX/0g8;

    move-object v0, p0

    .line 2580016
    return-object v0
.end method

.method public final kR_()LX/0cQ;
    .locals 1

    .prologue
    .line 2580115
    sget-object v0, LX/0cQ;->GROUP_ALBUM_FRAGMENT:LX/0cQ;

    return-object v0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2580116
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->H(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580117
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2580118
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    invoke-virtual {v0}, LX/DNA;->k()V

    .line 2580119
    return-void
.end method

.method public final mK_()LX/63R;
    .locals 1

    .prologue
    .line 2580120
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 2580121
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2580122
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    .line 2580123
    const/4 v1, -0x1

    if-eq p2, v1, :cond_6

    .line 2580124
    :goto_0
    if-nez p2, :cond_1

    const/16 v0, 0x6de

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->am:Z

    if-eqz v0, :cond_1

    .line 2580125
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v0}, LX/IQT;->e()V

    .line 2580126
    iput-boolean v6, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->am:Z

    .line 2580127
    :cond_0
    :goto_1
    return-void

    .line 2580128
    :cond_1
    if-ne p2, v5, :cond_0

    .line 2580129
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2580130
    sparse-switch p1, :sswitch_data_0

    goto :goto_1

    .line 2580131
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, v7}, LX/ISU;->c(Z)V

    goto :goto_1

    .line 2580132
    :sswitch_1
    const-string v0, "extra_is_composer_intercept_sell"

    invoke-virtual {p3, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2580133
    invoke-direct {p0, p3}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 2580134
    :cond_2
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2580135
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2580136
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2580137
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2580138
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2iz;->a(Ljava/lang/String;)V

    .line 2580139
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->E(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2580140
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ag:LX/DJ6;

    if-nez v1, :cond_4

    .line 2580141
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->E:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DJ6;

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ag:LX/DJ6;

    .line 2580142
    :cond_4
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580143
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2580144
    invoke-static {v1}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v2

    .line 2580145
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ag:LX/DJ6;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {v2, v1}, LX/DJw;->a(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v1

    .line 2580146
    iput-object v1, v3, LX/DJ6;->f:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 2580147
    move-object v1, v3

    .line 2580148
    new-instance v2, LX/IUS;

    invoke-direct {v2, p0}, LX/IUS;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580149
    iput-object v2, v1, LX/DJ6;->i:LX/DJr;

    .line 2580150
    move-object v1, v1

    .line 2580151
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/21D;->GROUP_FEED:LX/21D;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/DJ6;->a(Ljava/lang/String;LX/21D;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2580152
    :sswitch_2
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2580153
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 2580154
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->s:LX/DKH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580155
    iget-object v3, v2, LX/IW5;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2580156
    new-instance v3, LX/IUT;

    invoke-direct {v3, p0, v0}, LX/IUT;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;Landroid/net/Uri;)V

    invoke-virtual {v1, v2, v0, v3}, LX/DKH;->a(Ljava/lang/String;Landroid/net/Uri;LX/0TF;)V

    .line 2580157
    goto/16 :goto_1

    .line 2580158
    :sswitch_3
    const-string v0, "extra_album"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2580159
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580160
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2580161
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580162
    iget-object v3, v2, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v2, v3

    .line 2580163
    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/ISn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2580164
    :sswitch_4
    if-eqz v0, :cond_5

    const-string v1, "submitted_email"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2580165
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2580166
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f083013

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "submitted_email"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a00d5

    const v3, 0x7f0a05f1

    invoke-static {v1, v0, v5, v2, v3}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v0

    invoke-virtual {v0}, LX/4nm;->a()V

    .line 2580167
    :cond_5
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, v7}, LX/ISU;->c(Z)V

    goto/16 :goto_1

    .line 2580168
    :sswitch_5
    if-eqz v0, :cond_0

    .line 2580169
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2580170
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082514

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a00d5

    const v3, 0x7f0a05f1

    invoke-static {v1, v0, v5, v2, v3}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v0

    invoke-virtual {v0}, LX/4nm;->a()V

    goto/16 :goto_1

    .line 2580171
    :cond_6
    sparse-switch p1, :sswitch_data_1

    .line 2580172
    sget-object v1, LX/DNA;->f:Ljava/lang/Class;

    const-string v2, "Unexpected request code!"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2580173
    :sswitch_6
    iget-object v1, v0, LX/DNA;->i:LX/DCE;

    invoke-virtual {v1, p3}, LX/DCE;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2580174
    :sswitch_7
    iget-object v1, v0, LX/DNA;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2cm;

    invoke-virtual {v1, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_0
        0x12d -> :sswitch_2
        0x3e7 -> :sswitch_5
        0x6dc -> :sswitch_1
        0x7ad -> :sswitch_4
        0x7ae -> :sswitch_0
        0x7c7 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x6de -> :sswitch_6
        0x138a -> :sswitch_7
    .end sparse-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2580175
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2580176
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2580177
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2580178
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x6a748733

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2580179
    if-eqz p3, :cond_0

    .line 2580180
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    const-string v2, "update_cover_photo_key"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 2580181
    iput-boolean v2, v0, LX/ISU;->u:Z

    .line 2580182
    :cond_0
    const v0, 0x7f0313b0

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    .line 2580183
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v2, 0x7f0d159e

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->S:Landroid/view/View;

    .line 2580184
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v2, 0x7f0d15a0

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->U:Landroid/view/ViewStub;

    .line 2580185
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v2, 0x7f0d15a2

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->V:Landroid/view/ViewStub;

    .line 2580186
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->J:LX/IQU;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->S:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, v2, v3, p0, v4}, LX/IQU;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/base/fragment/FbFragment;LX/ISU;)LX/IQT;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    .line 2580187
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->s()V

    .line 2580188
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->p:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->W:Z

    if-nez v0, :cond_1

    .line 2580189
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k()LX/0g8;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, LX/ISU;->a(LX/63S;LX/0g9;)V

    .line 2580190
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->i:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Q:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->h:LX/9T3;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Q:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3, v4}, LX/9T3;->a(Lcom/facebook/api/feedtype/FeedType;)LX/9T2;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    .line 2580191
    iget-object p1, v4, LX/DNR;->c:LX/0fz;

    move-object v4, p1

    .line 2580192
    iget-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->X:LX/DOD;

    invoke-virtual {v0, v2, v3, v4, p1}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->R:LX/DNp;

    .line 2580193
    const/4 v0, 0x0

    .line 2580194
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->v:LX/0ad;

    sget-short v3, LX/9Lj;->a:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2580195
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->K:LX/IPk;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->V:Landroid/view/ViewStub;

    .line 2580196
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2580197
    if-nez v4, :cond_3

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/IPk;->a(Landroid/view/ViewStub;I)LX/IPj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    .line 2580198
    :cond_2
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->C(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2580199
    :goto_1
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->A(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580200
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->M:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v3, LX/IUK;

    invoke-direct {v3, p0}, LX/IUK;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    invoke-interface {v0, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aj:LX/0Yb;

    .line 2580201
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aj:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2580202
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    invoke-virtual {v0}, LX/IW5;->i()V

    .line 2580203
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v0, v5}, LX/ISU;->c(Z)V

    .line 2580204
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, -0x620b9241

    invoke-static {v6, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    .line 2580205
    :cond_3
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2580206
    const-string p1, "drawer_position"

    invoke-virtual {v4, p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 2580207
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->v:LX/0ad;

    sget-short v2, LX/100;->ac:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v2, LX/0zw;

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v3, 0x7f0d2b5e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v2

    .line 2580208
    :goto_2
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->w:LX/IPZ;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance p1, LX/IUQ;

    invoke-direct {p1, p0}, LX/IUQ;-><init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    invoke-virtual {v2, v0, v3, v4, p1}, LX/IPZ;->a(LX/0zw;LX/DN9;Landroid/content/Context;LX/IPW;)LX/IPY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ac:LX/IPY;

    goto :goto_1

    .line 2580209
    :cond_5
    new-instance v2, LX/0zw;

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    const v3, 0x7f0d2b5f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v2

    goto :goto_2
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d682b55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580210
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2580211
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aa:LX/1L6;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2580212
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ab:LX/1L6;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2580213
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580214
    iget-object v2, v1, LX/ISU;->m:LX/DK3;

    iget-object v4, v1, LX/ISU;->t:LX/IST;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2580215
    iget-object v2, v1, LX/ISU;->m:LX/DK3;

    iget-object v4, v1, LX/ISU;->b:LX/DK4;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2580216
    iget-object v2, v1, LX/ISU;->o:LX/DKL;

    iget-object v4, v1, LX/ISU;->s:LX/DKM;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2580217
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2580218
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2580219
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    invoke-virtual {v1}, LX/IW5;->deleteObservers()V

    .line 2580220
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l:LX/0jU;

    invoke-virtual {v1, p0}, LX/0jU;->b(LX/0fi;)V

    .line 2580221
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ad:LX/0fx;

    if-eqz v1, :cond_0

    .line 2580222
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ad:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2580223
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x602e68ce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x4aae65b3    # 5714649.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580224
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2580225
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->g:LX/DOF;

    invoke-virtual {v1}, LX/DOF;->a()V

    .line 2580226
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->b:LX/1CF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2580227
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2580228
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->R:LX/DNp;

    if-eqz v1, :cond_0

    .line 2580229
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->R:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2580230
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580231
    iget-object v4, v1, LX/ISU;->l:LX/88n;

    invoke-virtual {v4}, LX/88n;->a()V

    .line 2580232
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    if-eqz v1, :cond_1

    .line 2580233
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    invoke-virtual {v1}, LX/ISH;->a()V

    .line 2580234
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->b()V

    .line 2580235
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->k()V

    .line 2580236
    iput-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->S:Landroid/view/View;

    .line 2580237
    iput-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->Y:Landroid/view/ViewGroup;

    .line 2580238
    iput-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->U:Landroid/view/ViewStub;

    .line 2580239
    iput-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->V:Landroid/view/ViewStub;

    .line 2580240
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    if-eqz v1, :cond_2

    .line 2580241
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->af:LX/IPj;

    invoke-virtual {v1}, LX/IPj;->a()V

    .line 2580242
    :cond_2
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aj:LX/0Yb;

    if-eqz v1, :cond_3

    .line 2580243
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->aj:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2580244
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x72bfebf2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x38e4dcd7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580245
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2580246
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ag:LX/DJ6;

    if-eqz v1, :cond_0

    .line 2580247
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ag:LX/DJ6;

    invoke-virtual {v1}, LX/DJ6;->a()V

    .line 2580248
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580249
    iget-object v2, v1, LX/ISU;->i:LX/IW3;

    .line 2580250
    const/4 v1, 0x0

    iput-boolean v1, v2, LX/IW3;->l:Z

    .line 2580251
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a:LX/2lS;

    invoke-virtual {v1}, LX/2lS;->a()V

    .line 2580252
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->d()V

    .line 2580253
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->i()V

    .line 2580254
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->C(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2580255
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ac:LX/IPY;

    invoke-virtual {v1}, LX/IPY;->g()V

    .line 2580256
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x4ca3ac28

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4b1c578d    # 1.0246029E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580257
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ak:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2580258
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v1}, LX/IQT;->a()V

    .line 2580259
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->r:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 2580260
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v1}, LX/ISU;->k()V

    .line 2580261
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->j()V

    .line 2580262
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 2580263
    const/16 v1, 0x2b

    const v2, -0x2d745dd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2580264
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2580265
    const-string v0, "update_cover_photo_key"

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580266
    iget-boolean p0, v1, LX/ISU;->u:Z

    move v1, p0

    .line 2580267
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2580268
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1abef8ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580269
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2580270
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    invoke-virtual {v1}, LX/ISU;->c()V

    .line 2580271
    const/16 v1, 0x2b

    const v2, 0x3ccb0ab8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd6d51e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580272
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStop()V

    .line 2580273
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    .line 2580274
    iget-boolean v2, v1, LX/ISU;->v:Z

    if-eqz v2, :cond_1

    .line 2580275
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x4c3127c0    # 4.6440192E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2580276
    :cond_1
    iget-object v2, v1, LX/ISU;->p:Lcom/facebook/base/fragment/FbFragment;

    const-class p0, LX/1ZF;

    invoke-virtual {v2, p0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2580277
    if-eqz v2, :cond_0

    .line 2580278
    const/4 p0, 0x0

    invoke-interface {v2, p0}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2580021
    sget-object v0, LX/IUL;->a:[I

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580022
    iget-object p1, v1, LX/IW5;->i:LX/IW2;

    move-object v1, p1

    .line 2580023
    invoke-virtual {v1}, LX/IW2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2580024
    :goto_0
    :pswitch_0
    return-void

    .line 2580025
    :pswitch_1
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->L(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V

    .line 2580026
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580027
    iget-object v1, v0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v0, v1

    .line 2580028
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2580029
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2580030
    iget-object p0, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, p0

    .line 2580031
    const/4 v2, 0x0

    .line 2580032
    iget-object p0, v0, LX/IQT;->i:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/IQT;->k:LX/0Uh;

    const/16 p1, 0x575

    invoke-virtual {p0, p1, v2}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v1}, LX/IQV;->e(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v2, v2

    .line 2580033
    if-nez v2, :cond_2

    .line 2580034
    :cond_1
    :goto_1
    goto :goto_0

    .line 2580035
    :cond_2
    iget-object v2, v0, LX/IQT;->a:LX/0iA;

    const-string p0, "4327"

    const-class p1, LX/8vQ;

    invoke-virtual {v2, p0, p1}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/8vQ;

    .line 2580036
    if-eqz v2, :cond_1

    .line 2580037
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p0

    .line 2580038
    iput-object p0, v2, LX/8vQ;->d:Ljava/lang/String;

    .line 2580039
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object p0

    .line 2580040
    iput-object p0, v2, LX/8vQ;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2580041
    iget-object p0, v0, LX/IQT;->e:Landroid/view/ViewGroup;

    .line 2580042
    iput-object p0, v2, LX/8vQ;->b:Landroid/view/View;

    .line 2580043
    invoke-static {v1}, LX/IQV;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/0Px;

    move-result-object p0

    .line 2580044
    iput-object p0, v2, LX/8vQ;->c:LX/0Px;

    .line 2580045
    invoke-static {v1}, LX/IQV;->c(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)I

    move-result p0

    .line 2580046
    iput p0, v2, LX/8vQ;->f:I

    .line 2580047
    invoke-static {v1}, LX/IQV;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result p0

    .line 2580048
    iput-boolean p0, v2, LX/8vQ;->e:Z

    .line 2580049
    iget-object v2, v0, LX/IQT;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15W;

    iget-object p0, v0, LX/IQT;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    new-instance p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {p1, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2580050
    const-class p2, LX/0i1;

    const/4 v0, 0x0

    invoke-virtual {v2, p0, p1, p2, v0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2580051
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
