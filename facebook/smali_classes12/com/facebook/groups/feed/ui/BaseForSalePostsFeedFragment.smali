.class public abstract Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""

# interfaces
.implements LX/DNB;


# instance fields
.field public a:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Qq;

.field private g:Lcom/facebook/api/feedtype/FeedType;

.field public h:LX/DNp;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575104
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2575101
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->e:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2575102
    sget-object v3, LX/DOr;->a:LX/DOr;

    move-object v3, v3

    .line 2575103
    new-instance v4, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment$1;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment$1;-><init>(Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/DNq;LX/0fz;LX/DNe;)LX/DNp;
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2575096
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2575097
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;

    const-class v3, LX/DNq;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DNq;

    invoke-static {p1}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v4

    check-cast v4, LX/DNR;

    invoke-static {p1}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v5

    check-cast v5, LX/DNT;

    invoke-static {p1}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v6

    check-cast v6, LX/DNJ;

    const-class v0, LX/IRJ;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IRJ;

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->a:LX/DNq;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->c:LX/DNT;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    iput-object p1, v2, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->e:LX/IRJ;

    .line 2575098
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 2575099
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->c:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->g:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2575100
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2575092
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2575093
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2575094
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081bdb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575095
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2575091
    return-void
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 2575090
    return-void
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 1

    .prologue
    .line 2575089
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b()Lcom/facebook/api/feedtype/FeedType;
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2575105
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2575088
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2575087
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract k()I
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2575085
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2575086
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2575083
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2575084
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2575080
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2575081
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2575082
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x657caf08

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2575070
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2575071
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->c:LX/DNT;

    new-instance v3, LX/IQE;

    invoke-direct {v3, p0}, LX/IQE;-><init>(Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2575072
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    .line 2575073
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2575074
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2575075
    new-instance v0, LX/IQF;

    invoke-direct {v0, p0}, LX/IQF;-><init>(Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;)V

    .line 2575076
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->a:LX/DNq;

    invoke-virtual {p0, v1, v3, v0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->a(LX/DNq;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->h:LX/DNp;

    .line 2575077
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->h:LX/DNp;

    if-eqz v0, :cond_0

    .line 2575078
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->i:Z

    .line 2575079
    :cond_0
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, 0x2139c66a

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v10
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x217c565f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575063
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2575064
    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->i:Z

    if-eqz v1, :cond_0

    .line 2575065
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->h:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2575066
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2575067
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->f:LX/1Qq;

    if-eqz v1, :cond_1

    .line 2575068
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->f:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2575069
    :cond_1
    const/16 v1, 0x2b

    const v2, -0xdd9775e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x38777514

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575058
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2575059
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2575060
    if-nez v1, :cond_0

    .line 2575061
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x4267e0ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2575062
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->k()I

    move-result v2

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method
