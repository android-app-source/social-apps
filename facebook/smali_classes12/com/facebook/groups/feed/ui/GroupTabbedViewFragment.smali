.class public Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575596
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x6d3cd772

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2575587
    const v0, 0x7f030834

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;->a:Landroid/view/ViewGroup;

    .line 2575588
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d1571

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2575589
    new-instance v1, LX/IUa;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    .line 2575590
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2575591
    invoke-direct {v1, v3, v4}, LX/IUa;-><init>(LX/0gc;Landroid/os/Bundle;)V

    .line 2575592
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2575593
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;->a:Landroid/view/ViewGroup;

    const v3, 0x7f0d1570

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/texttabbar/FigTextTabBar;

    .line 2575594
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2575595
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupTabbedViewFragment;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x2b

    const v3, -0x46fd22db

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
