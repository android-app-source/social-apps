.class public Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;

.field private static final n:[Ljava/lang/String;


# instance fields
.field public o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/content/SecureContextHelper;

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DOL;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/DP5;

.field public t:Z

.field public u:[Ljava/lang/String;

.field public v:LX/3my;

.field public w:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2578917
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    const-string v1, "group_feed"

    const-string v2, "plutonium_cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->m:Lcom/facebook/common/callercontext/CallerContext;

    .line 2578918
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->n:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2578899
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2578900
    new-instance v0, LX/IT0;

    invoke-direct {v0, p0}, LX/IT0;-><init>(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->x:Landroid/view/View$OnClickListener;

    .line 2578901
    const/4 p1, 0x1

    .line 2578902
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2578903
    invoke-virtual {p0}, LX/Ban;->e()V

    .line 2578904
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->t:Z

    if-eqz v0, :cond_0

    .line 2578905
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2578906
    :cond_0
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setFocusable(Z)V

    .line 2578907
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v1, 0x7f0e0781

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2578908
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v1, 0x7f0e0782

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleTextAppearance(I)V

    .line 2578909
    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->d:LX/Bam;

    .line 2578910
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2578911
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->h(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    .line 2578912
    invoke-virtual {p0}, LX/Ban;->getProfileEditIconViewStub()Landroid/view/ViewStub;

    move-result-object v1

    .line 2578913
    const v0, 0x7f0d2d72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2578914
    invoke-static {p0, v1, v0, p1}, LX/Bah;->a(LX/Ban;Landroid/view/ViewStub;Landroid/view/ViewStub;Z)V

    .line 2578915
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    .line 2578916
    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v2, 0x0

    .line 2578884
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b123f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2578885
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b123d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2578886
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b123e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 2578887
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2578888
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0e0781

    invoke-direct {v0, v1, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2578889
    invoke-static {p1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v8, v0, v2, v1, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2578890
    const-string v0, "\u2060"

    invoke-virtual {v8, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2578891
    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 2578892
    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v8, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2578893
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2578894
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    neg-int v3, v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 2578895
    add-int v1, v7, v6

    invoke-virtual {v0, v6, v2, v1, v7}, Landroid/graphics/drawable/InsetDrawable;->setBounds(IIII)V

    .line 2578896
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v8, v1, v9, v0, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2578897
    return-object v8

    .line 2578898
    :cond_0
    const-string p2, "[badge]"

    goto :goto_0
.end method

.method public static a(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2578879
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->GROUP:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->LAUNCH_COVER_PIC_CROPPER:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2578880
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2578881
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2578882
    const/16 v2, 0x12d

    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {p0, v1, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2578883
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2578818
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2578819
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2578820
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2578821
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578822
    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2578823
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2578824
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2578825
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578826
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    :goto_3
    if-eqz v1, :cond_9

    .line 2578827
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2578828
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->u:[Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 2578829
    :cond_0
    :goto_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578830
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->setCoverPhoto(Ljava/lang/String;)V

    .line 2578831
    :goto_5
    return-void

    :cond_1
    move v0, v2

    .line 2578832
    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3

    :cond_8
    move v1, v2

    goto :goto_3

    .line 2578833
    :cond_9
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_b

    .line 2578834
    :cond_a
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->h(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    goto :goto_5

    .line 2578835
    :cond_b
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;->b()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_13

    .line 2578836
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2578837
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object v5

    .line 2578838
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2578839
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_6
    if-ge v4, v7, :cond_11

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;

    .line 2578840
    if-eqz v0, :cond_e

    .line 2578841
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2578842
    if-eqz v1, :cond_d

    move v1, v2

    :goto_7
    if-eqz v1, :cond_10

    .line 2578843
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v1

    iget-object p1, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2578844
    invoke-virtual {p1, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    move v1, v2

    :goto_8
    if-eqz v1, :cond_c

    .line 2578845
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel$NodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578846
    new-instance p1, LX/6UY;

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p1, v0, v2, v2}, LX/6UY;-><init>(Landroid/net/Uri;II)V

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2578847
    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    :cond_d
    move v1, v3

    .line 2578848
    goto :goto_7

    :cond_e
    move v1, v3

    goto :goto_7

    :cond_f
    move v1, v3

    goto :goto_8

    :cond_10
    move v1, v3

    goto :goto_8

    .line 2578849
    :cond_11
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_12

    .line 2578850
    sget-object v0, LX/Bap;->FACEPILE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2578851
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->x:Landroid/view/View$OnClickListener;

    .line 2578852
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2578853
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iget v3, p0, LX/Ban;->c:I

    invoke-direct {v2, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2578854
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2578855
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x8

    if-lt v1, v2, :cond_1b

    .line 2578856
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setNumRows(I)V

    .line 2578857
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    iget-object v2, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/facepile/FacepileGridView;

    .line 2578858
    iget v3, v2, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    move v2, v3

    .line 2578859
    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setNumCols(I)V

    .line 2578860
    :goto_9
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v1, v6}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 2578861
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578862
    :cond_12
    goto/16 :goto_5

    .line 2578863
    :cond_13
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->b()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2578864
    invoke-direct {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->setDefaultCoverPhoto(Z)V

    goto/16 :goto_5

    .line 2578865
    :cond_14
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->h(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V

    goto/16 :goto_5

    .line 2578866
    :cond_15
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_17

    .line 2578867
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2578868
    const-class v6, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v1, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    if-eqz v1, :cond_16

    move v1, v3

    :goto_a
    if-eqz v1, :cond_19

    .line 2578869
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2578870
    const-class v6, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v1, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_18

    move v1, v3

    :goto_b
    if-eqz v1, :cond_1a

    .line 2578871
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2578872
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v4

    const-class p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v1, v4, p1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v3

    iput-object v6, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->u:[Ljava/lang/String;

    goto/16 :goto_4

    :cond_16
    move v1, v4

    .line 2578873
    goto :goto_a

    :cond_17
    move v1, v4

    goto :goto_a

    :cond_18
    move v1, v4

    goto :goto_b

    :cond_19
    move v1, v4

    goto :goto_b

    .line 2578874
    :cond_1a
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->u:[Ljava/lang/String;

    goto/16 :goto_4

    .line 2578875
    :cond_1b
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setNumRows(I)V

    .line 2578876
    iget-object v1, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/facepile/FacepileGridView;

    iget-object v2, p0, LX/Ban;->j:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/facepile/FacepileGridView;

    .line 2578877
    iget v3, v2, Lcom/facebook/fbui/facepile/FacepileGridView;->h:I

    move v2, v3

    .line 2578878
    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setNumCols(I)V

    goto/16 :goto_9
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xbc6

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x240a

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/DP5;->b(LX/0QB;)LX/DP5;

    move-result-object v5

    check-cast v5, LX/DP5;

    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v6

    check-cast v6, LX/3my;

    invoke-static {p0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    iput-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->q:LX/0Or;

    iput-object v4, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->r:LX/0Ot;

    iput-object v5, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->s:LX/DP5;

    iput-object v6, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->v:LX/3my;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, v1, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->t:Z

    return-void
.end method

.method public static g(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2578785
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2578786
    :cond_0
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2578787
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2578788
    :goto_0
    return-void

    .line 2578789
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    .line 2578790
    const/4 v0, 0x0

    .line 2578791
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v1, v4, :cond_4

    .line 2578792
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->y()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2578793
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->y()Ljava/lang/String;

    move-result-object v0

    .line 2578794
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 2578795
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->v:LX/3my;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2578796
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081b8d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f020ec3

    invoke-static {p0, v0, v4, v5}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->a(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2578797
    :cond_3
    :goto_2
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2578798
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578799
    invoke-virtual {v1, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2578800
    :goto_3
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_7

    .line 2578801
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->o()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2578802
    invoke-virtual {v4, v1, v2}, LX/15i;->j(II)I

    move-result v1

    .line 2578803
    :goto_4
    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v4}, LX/DKD;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2578804
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->t()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x145860f9

    invoke-static {v4, v3, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_5
    invoke-virtual {v3, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2578805
    iget-object v5, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v4, v3, v7}, LX/15i;->j(II)I

    move-result v7

    invoke-virtual {v4, v3, v2}, LX/15i;->j(II)I

    move-result v2

    .line 2578806
    invoke-static {v0, v7, v2}, LX/DP5;->a(Ljava/lang/String;II)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 2578807
    invoke-static {v3, v1}, LX/DP5;->a(Landroid/text/SpannableStringBuilder;I)V

    .line 2578808
    move-object v0, v3

    .line 2578809
    invoke-virtual {v5, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2578810
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2578811
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2578812
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2578813
    :cond_5
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2578814
    :cond_6
    const-string v0, ""

    goto :goto_3

    :cond_7
    move v1, v2

    .line 2578815
    goto :goto_4

    .line 2578816
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_5

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2578817
    :cond_9
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-static {v0, v1}, LX/DP5;->a(Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public static h(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V
    .locals 15

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 2578782
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2578783
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    sget-object v9, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->n:[Ljava/lang/String;

    sget-object v10, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->m:Lcom/facebook/common/callercontext/CallerContext;

    move-object v5, v4

    move-object v6, v4

    move v8, v7

    move-object v11, v4

    move-object v12, v4

    move v13, v3

    move v14, v7

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2578784
    return-void
.end method

.method public static j(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2578768
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2578769
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578770
    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2578771
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2578772
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2578773
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2578774
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->q:LX/0Or;

    if-eqz v0, :cond_0

    .line 2578775
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 2578776
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2578777
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-class v6, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v3, v2, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/74S;->GROUPS_COVER_PHOTO:LX/74S;

    invoke-interface {v0, v6, v7, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;

    move-result-object v0

    .line 2578778
    if-eqz v0, :cond_0

    .line 2578779
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578780
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2578781
    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method private setCoverPhoto(Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 2578764
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2578765
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2578766
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getCoverPhotoDescription()[Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->m:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v11, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->x:Landroid/view/View$OnClickListener;

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2578767
    return-void
.end method

.method private setDefaultCoverPhoto(Z)V
    .locals 15

    .prologue
    .line 2578745
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    sget-object v9, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->n:[Ljava/lang/String;

    sget-object v10, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->m:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v11, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->x:Landroid/view/View$OnClickListener;

    const/4 v12, 0x0

    const/4 v13, 0x1

    if-nez p1, :cond_0

    const/4 v14, 0x1

    :goto_0
    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2578746
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2578747
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Ban;->a(Landroid/view/View;)V

    .line 2578748
    return-void

    .line 2578749
    :cond_0
    const/4 v14, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCoverPhotoDescription()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 2578763
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->u:[Ljava/lang/String;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2578756
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2578757
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 2578758
    iget v1, p0, LX/Ban;->b:I

    if-eq v1, v0, :cond_0

    .line 2578759
    iput v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->b:I

    .line 2578760
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2578761
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->a(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;Z)V

    .line 2578762
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2578750
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2578751
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 2578752
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2578753
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->w:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2578754
    :cond_0
    invoke-super {p0, p1, p2}, LX/Ban;->onMeasure(II)V

    .line 2578755
    return-void
.end method
