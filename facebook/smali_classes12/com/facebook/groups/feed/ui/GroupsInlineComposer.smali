.class public Lcom/facebook/groups/feed/ui/GroupsInlineComposer;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/DaM",
        "<",
        "LX/B1U;",
        ">;"
    }
.end annotation


# static fields
.field private static final t:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/fbui/glyph/GlyphView;

.field public B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public D:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public E:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public F:LX/34b;

.field public G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public H:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public I:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public L:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;",
            ">;"
        }
    .end annotation
.end field

.field public M:Z

.field private N:Z

.field public O:Z

.field private P:Z

.field private Q:Z

.field private final R:F

.field private final S:LX/ISe;

.field public final T:Landroid/view/View$OnClickListener;

.field public final U:Landroid/view/View$OnClickListener;

.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/groups/feed/ui/IsSellActionInInlineComposerEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/ISn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsMultiCompanyChatEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AP9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1b2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1E8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/JEW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/88k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/ISp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/user/model/User;

.field public v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public w:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;

.field public x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

.field public y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public z:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2578332
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    const-string v1, "group_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->t:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2578330
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2578331
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2578301
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2578302
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->R:F

    .line 2578303
    new-instance v0, LX/ISe;

    invoke-direct {v0, p0}, LX/ISe;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->S:LX/ISe;

    .line 2578304
    new-instance v0, LX/ISf;

    invoke-direct {v0, p0}, LX/ISf;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->T:Landroid/view/View$OnClickListener;

    .line 2578305
    new-instance v0, LX/ISg;

    invoke-direct {v0, p0}, LX/ISg;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->U:Landroid/view/View$OnClickListener;

    .line 2578306
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2578307
    const v0, 0x7f03084c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2578308
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->setOrientation(I)V

    .line 2578309
    const v0, 0x7f0d15a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2578310
    const v0, 0x7f0d15a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2578311
    const v0, 0x7f0d15a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->z:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2578312
    const v0, 0x7f0d15a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->A:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2578313
    const v0, 0x7f0d15a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2578314
    new-instance p1, LX/0zw;

    const v0, 0x7f0d15ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->H:LX/0zw;

    .line 2578315
    new-instance p1, LX/0zw;

    const v0, 0x7f0d15ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->I:LX/0zw;

    .line 2578316
    new-instance p1, LX/0zw;

    const v0, 0x7f0d15a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->L:LX/0zw;

    .line 2578317
    new-instance p1, LX/0zw;

    const v0, 0x7f0d15aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->J:LX/0zw;

    .line 2578318
    const/4 p2, 0x0

    .line 2578319
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getResources()Landroid/content/res/Resources;

    .line 2578320
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->setGravity(I)V

    .line 2578321
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b00d7

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2578322
    const p1, 0x7f020c6f

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->setBackgroundResource(I)V

    .line 2578323
    invoke-virtual {p0, p2, v0, p2, p2}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->setPadding(IIII)V

    .line 2578324
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2578325
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0a060f

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2578326
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2578327
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2578328
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b010f

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 2578329
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2578285
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->i:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SEEDS_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/3kj;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kj;

    .line 2578286
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 2578287
    if-nez p1, :cond_2

    .line 2578288
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->i:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3kj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2578289
    :cond_0
    :goto_1
    return-void

    .line 2578290
    :cond_1
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2578291
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->i:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_SEEDS_COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/IVq;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/IVq;

    .line 2578292
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2578293
    if-nez p1, :cond_3

    .line 2578294
    :goto_2
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->i:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/IVq;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2578295
    :cond_2
    new-instance v1, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2578296
    const v2, 0x7f081c04

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 2578297
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0

    .line 2578298
    :cond_3
    new-instance v1, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2578299
    const v2, 0x7f081c05

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 2578300
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;Landroid/content/Context;LX/1Kf;LX/0Or;LX/0WJ;LX/0Ot;LX/0Uh;LX/0Or;LX/ISn;LX/0iA;LX/0W9;Ljava/lang/Boolean;LX/0ad;LX/0Ot;LX/1b2;LX/0Ot;LX/1E8;LX/JEW;LX/88k;LX/ISp;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/ui/GroupsInlineComposer;",
            "Landroid/content/Context;",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/ISn;",
            "LX/0iA;",
            "LX/0W9;",
            "Ljava/lang/Boolean;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/AP9;",
            ">;",
            "LX/1b2;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;",
            "LX/1E8;",
            "LX/JEW;",
            "LX/88k;",
            "LX/ISp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2578284
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b:LX/1Kf;

    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->d:LX/0WJ;

    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->f:LX/0Uh;

    iput-object p7, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->h:LX/ISn;

    iput-object p9, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->i:LX/0iA;

    iput-object p10, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->j:LX/0W9;

    iput-object p11, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->k:Ljava/lang/Boolean;

    iput-object p12, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->l:LX/0ad;

    iput-object p13, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->n:LX/1b2;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->p:LX/1E8;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->q:LX/JEW;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->r:LX/88k;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->s:LX/ISp;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v21

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {v21 .. v21}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v4

    check-cast v4, LX/1Kf;

    const/16 v5, 0xbc6

    move-object/from16 v0, v21

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v21 .. v21}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v6

    check-cast v6, LX/0WJ;

    const/16 v7, 0x455

    move-object/from16 v0, v21

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v21 .. v21}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    const/16 v9, 0x31e

    move-object/from16 v0, v21

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {v21 .. v21}, LX/ISn;->a(LX/0QB;)LX/ISn;

    move-result-object v10

    check-cast v10, LX/ISn;

    invoke-static/range {v21 .. v21}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v11

    check-cast v11, LX/0iA;

    invoke-static/range {v21 .. v21}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v12

    check-cast v12, LX/0W9;

    invoke-static/range {v21 .. v21}, LX/8vR;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {v21 .. v21}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    const/16 v15, 0x1965

    move-object/from16 v0, v21

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v21 .. v21}, LX/1b2;->a(LX/0QB;)LX/1b2;

    move-result-object v16

    check-cast v16, LX/1b2;

    const/16 v17, 0x3572

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v21 .. v21}, LX/1E8;->a(LX/0QB;)LX/1E8;

    move-result-object v18

    check-cast v18, LX/1E8;

    invoke-static/range {v21 .. v21}, LX/JEW;->a(LX/0QB;)LX/JEW;

    move-result-object v19

    check-cast v19, LX/JEW;

    invoke-static/range {v21 .. v21}, LX/88k;->a(LX/0QB;)LX/88k;

    move-result-object v20

    check-cast v20, LX/88k;

    invoke-static/range {v21 .. v21}, LX/ISp;->a(LX/0QB;)LX/ISp;

    move-result-object v21

    check-cast v21, LX/ISp;

    invoke-static/range {v2 .. v21}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;Landroid/content/Context;LX/1Kf;LX/0Or;LX/0WJ;LX/0Ot;LX/0Uh;LX/0Or;LX/ISn;LX/0iA;LX/0W9;Ljava/lang/Boolean;LX/0ad;LX/0Ot;LX/1b2;LX/0Ot;LX/1E8;LX/JEW;LX/88k;LX/ISp;)V

    return-void
.end method

.method public static b(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)Z
    .locals 1

    .prologue
    .line 2578283
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->a(LX/DZC;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 2578333
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->N:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBottomSheetAdapterForMorebutton(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)LX/34b;
    .locals 11

    .prologue
    .line 2578247
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    if-nez v0, :cond_4

    .line 2578248
    new-instance v0, LX/34b;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    .line 2578249
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const/4 v1, 0x1

    .line 2578250
    iput-boolean v1, v0, LX/34b;->d:Z

    .line 2578251
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->O:Z

    if-nez v0, :cond_0

    .line 2578252
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AP9;

    sget-object v3, LX/2rw;->GROUP:LX/2rw;

    const/4 v6, 0x0

    .line 2578253
    sget-object v9, LX/5Rn;->NORMAL:LX/5Rn;

    const/4 v10, 0x0

    move-object v4, v2

    move-object v5, v3

    move v7, v6

    move v8, v6

    invoke-virtual/range {v4 .. v10}, LX/AP9;->a(LX/2rw;ZZZLX/5Rn;LX/ARN;)Z

    move-result v4

    move v2, v4

    .line 2578254
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2578255
    if-eqz v0, :cond_0

    .line 2578256
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c06

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578257
    const v1, 0x7f02090e

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578258
    new-instance v1, LX/ISX;

    invoke-direct {v1, p0}, LX/ISX;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578259
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c07

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578260
    const v1, 0x7f020903

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578261
    new-instance v1, LX/ISY;

    invoke-direct {v1, p0}, LX/ISY;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578262
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2578263
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c0c

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578264
    const v1, 0x7f0209f0

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578265
    new-instance v1, LX/ISZ;

    invoke-direct {v1, p0}, LX/ISZ;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578266
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c0b

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578267
    const v1, 0x7f02095b

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578268
    new-instance v1, LX/ISa;

    invoke-direct {v1, p0}, LX/ISa;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578269
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2578270
    if-eqz v0, :cond_2

    .line 2578271
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c08

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578272
    const v1, 0x7f020855

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578273
    new-instance v1, LX/ISb;

    invoke-direct {v1, p0}, LX/ISb;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578274
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->E()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2578275
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->r:LX/88k;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/88k;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2578276
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c09

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578277
    invoke-static {}, LX/10A;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578278
    new-instance v1, LX/ISc;

    invoke-direct {v1, p0}, LX/ISc;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578279
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    const v1, 0x7f081c0a

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 2578280
    const v1, 0x7f020945

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2578281
    new-instance v1, LX/ISd;

    invoke-direct {v1, p0}, LX/ISd;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2578282
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    return-object v0

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static h$redex0(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 3

    .prologue
    .line 2578241
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2578242
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageProfilePicUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->t:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2578243
    :cond_0
    :goto_0
    return-void

    .line 2578244
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->u:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2578245
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->u:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2578246
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->t:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2578236
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->q:LX/JEW;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->q:LX/JEW;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2578237
    invoke-static {v0}, LX/JEW;->a(LX/JEW;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2578238
    const/4 p0, 0x0

    .line 2578239
    :goto_0
    move v0, p0

    .line 2578240
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-static {v0}, LX/JEW;->b(LX/JEW;)Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 4

    .prologue
    .line 2578226
    new-instance v0, LX/89I;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 2578227
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2578228
    iput-object v1, v0, LX/89I;->c:Ljava/lang/String;

    .line 2578229
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->F()Ljava/lang/String;

    move-result-object v1

    .line 2578230
    iput-object v1, v0, LX/89I;->d:Ljava/lang/String;

    .line 2578231
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->n:LX/1b2;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1b2;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 2578232
    sget-object v2, LX/21D;->GROUP_FEED:LX/21D;

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/facecast/FacecastActivity;->a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/os/Bundle;

    move-result-object v0

    .line 2578233
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2578234
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578235
    return-void
.end method

.method public static s(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 6

    .prologue
    .line 2578224
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->j:LX/0W9;

    invoke-static {v0, v3}, LX/DJw;->b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 v4, 0x6dc

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v3, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2578225
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2578150
    check-cast p1, LX/B1U;

    .line 2578151
    iget-object v0, p1, LX/B1U;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2578152
    iget-boolean v0, p1, LX/B1U;->b:Z

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->Q:Z

    .line 2578153
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->d:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->u:Lcom/facebook/user/model/User;

    .line 2578154
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->s:LX/ISp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->S:LX/ISe;

    invoke-virtual {v0, v1, v2}, LX/ISp;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/ISe;)V

    .line 2578155
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->h$redex0(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    .line 2578156
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2578157
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const/4 v2, 0x0

    .line 2578158
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    if-nez v1, :cond_9

    move-object v1, v2

    .line 2578159
    :goto_0
    move-object v1, v1

    .line 2578160
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->w:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->L:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->L:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    .line 2578161
    iget-boolean v2, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->h:Z

    move v0, v2

    .line 2578162
    if-nez v0, :cond_1

    .line 2578163
    :cond_0
    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->w:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;

    .line 2578164
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/IQo;->a(Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2578165
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2578166
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->L:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2578167
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->P:Z

    if-eqz v0, :cond_2

    .line 2578168
    :goto_2
    return-void

    .line 2578169
    :cond_2
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2578170
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const v1, 0x7f081c03

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(I)V

    .line 2578171
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->H:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    .line 2578172
    const v0, 0x7f0d1588

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2578173
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScale(F)V

    .line 2578174
    :cond_3
    :goto_3
    new-instance v0, LX/ISi;

    invoke-direct {v0, p0}, LX/ISi;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    .line 2578175
    new-instance v1, LX/ISj;

    invoke-direct {v1, p0}, LX/ISj;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    .line 2578176
    new-instance v2, LX/ISk;

    invoke-direct {v2, p0}, LX/ISk;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    .line 2578177
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->b(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2578178
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578179
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->z:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/ISl;

    invoke-direct {v1, p0}, LX/ISl;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578180
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578181
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578182
    :goto_4
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->U:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578183
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->M:Z

    if-eqz v0, :cond_7

    .line 2578184
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->E:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a(Landroid/view/View;)V

    .line 2578185
    :cond_4
    :goto_5
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2578186
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 2578187
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2578188
    :goto_6
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->J:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    .line 2578189
    new-instance v2, LX/ISh;

    invoke-direct {v2, p0, v0}, LX/ISh;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 2578190
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578191
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578192
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2578193
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMaxLines(I)V

    .line 2578194
    :try_start_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->y:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->P()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 2578195
    :goto_7
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->q:LX/JEW;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2578196
    invoke-static {v0}, LX/JEW;->a(LX/JEW;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 2578197
    :cond_5
    :goto_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->P:Z

    goto/16 :goto_2

    .line 2578198
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->Q:Z

    if-eqz v0, :cond_3

    .line 2578199
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->A:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2578200
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->Q:Z

    if-nez v0, :cond_4

    .line 2578201
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->a(Landroid/view/View;)V

    goto :goto_5

    .line 2578202
    :cond_8
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->L:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto/16 :goto_1

    .line 2578203
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_9
    if-ge v3, v5, :cond_b

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;

    .line 2578204
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;->j()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object p1

    if-eqz p1, :cond_a

    .line 2578205
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;->j()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->r()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$CrossPostSuggestionsModel;

    move-result-object v1

    goto/16 :goto_0

    .line 2578206
    :cond_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    :cond_b
    move-object v1, v2

    .line 2578207
    goto/16 :goto_0

    .line 2578208
    :cond_c
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->K:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578209
    iget-boolean v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->M:Z

    if-eqz v3, :cond_f

    .line 2578210
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->s()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2578211
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578212
    :goto_a
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->D:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578213
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->E:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 2578214
    :cond_d
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->O:Z

    if-eqz v0, :cond_e

    .line 2578215
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    new-instance v3, LX/ISm;

    invoke-direct {v3, p0}, LX/ISm;-><init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_a

    .line 2578216
    :cond_e
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_a

    .line 2578217
    :cond_f
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->z:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578218
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->A:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 2578219
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 2578220
    :catch_0
    goto/16 :goto_7

    .line 2578221
    :cond_11
    invoke-static {v0}, LX/JEW;->b(LX/JEW;)Ljava/util/Set;

    move-result-object v2

    .line 2578222
    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2578223
    iget-object v3, v0, LX/JEW;->c:LX/2cv;

    invoke-virtual {v3}, LX/2cv;->a()LX/3C9;

    move-result-object v3

    sget-object v4, LX/JEW;->b:LX/2d5;

    invoke-interface {v3, v4, v2}, LX/3C9;->a(LX/2d5;Ljava/lang/Object;)LX/3C9;

    move-result-object v2

    invoke-interface {v2}, LX/3C9;->a()V

    goto/16 :goto_8
.end method
