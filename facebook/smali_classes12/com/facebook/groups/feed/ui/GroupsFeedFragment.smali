.class public Lcom/facebook/groups/feed/ui/GroupsFeedFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fi;
.implements LX/0fj;
.implements LX/0yL;
.implements LX/DNB;
.implements LX/0gr;
.implements LX/0o2;
.implements LX/5vN;
.implements LX/0fw;
.implements LX/63S;


# annotations
.annotation runtime Lcom/facebook/search/interfaces/GraphSearchTitleSupport;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final af:Ljava/lang/String;

.field private static final ag:Z


# instance fields
.field public A:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HidePinnedStoryPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/IRJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/DKH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Or;
    .annotation runtime Lcom/facebook/groups/feed/ui/IsGroupCommercePurposeModalEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/DKL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/IPZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/IUw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DRd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ILp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public a:LX/2lS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aA:LX/1L6;

.field private aB:LX/1L6;

.field private aC:LX/IRr;

.field private final aD:LX/DK4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DK4",
            "<",
            "LX/DK7;",
            ">;"
        }
    .end annotation
.end field

.field private aE:Landroid/content/Context;

.field public aF:Z

.field public aG:Z

.field public aH:Z

.field private aI:LX/DKM;

.field public aJ:LX/IPY;

.field public aK:LX/0fx;

.field public aL:LX/DRc;

.field public aM:LX/IRb;

.field private aN:LX/DJ6;

.field public aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public aP:LX/1Qq;

.field private aQ:Z

.field public aR:Z

.field public aS:Z

.field public aT:Z

.field public aU:Z

.field private aV:LX/DRf;

.field public aW:LX/0Yb;

.field public final aX:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public aa:LX/IPk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ab:LX/Hgt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ac:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/CGV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ae:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:Ljava/lang/String;

.field public ai:Ljava/lang/String;

.field private aj:Ljava/lang/String;

.field public ak:I

.field private al:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public am:LX/ISH;

.field public an:Lcom/facebook/api/feedtype/FeedType;

.field public ao:LX/DNp;

.field public ap:Landroid/view/View;

.field public aq:LX/IUv;

.field private ar:Landroid/view/ViewStub;

.field public as:Landroid/view/ViewStub;

.field public at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

.field private au:Z

.field public av:LX/DOD;

.field public aw:Landroid/view/ViewGroup;

.field public ax:LX/IPj;

.field private ay:LX/1gl;

.field private az:LX/63Q;

.field public b:LX/DO3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1CF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1lu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DOF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/9T3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0jU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/88n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/ITk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/ISJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/DK3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2576892
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->af:Ljava/lang/String;

    .line 2576893
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ag:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2577175
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2577176
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->au:Z

    .line 2577177
    new-instance v0, LX/IRf;

    invoke-direct {v0, p0}, LX/IRf;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aD:LX/DK4;

    .line 2577178
    iput-boolean v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    .line 2577179
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aR:Z

    .line 2577180
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aS:Z

    .line 2577181
    iput-boolean v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aT:Z

    .line 2577182
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aU:Z

    .line 2577183
    new-instance v0, LX/IRj;

    invoke-direct {v0, p0}, LX/IRj;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aX:LX/0TF;

    .line 2577184
    return-void
.end method

.method private A()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2577157
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->f:LX/DNT;

    new-instance v3, LX/IRq;

    invoke-direct {v3, p0}, LX/IRq;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    iget-object v8, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->al:Ljava/util/ArrayList;

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2577158
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2577159
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2577160
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    new-instance v7, LX/IRV;

    invoke-direct {v7, p0}, LX/IRV;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->al:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v8, 0x1

    :goto_0
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v9

    move-object v4, p0

    move-object v5, p0

    move v6, v10

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2577161
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->au:Z

    if-nez v0, :cond_0

    .line 2577162
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aa(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577163
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->h:LX/DOF;

    .line 2577164
    iget-object v1, v3, LX/0fz;->a:LX/0qq;

    move-object v1, v1

    .line 2577165
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->av:LX/DOD;

    sget-object v4, LX/DOJ;->Pin:LX/DOJ;

    invoke-virtual {v0, v1, v2, v4}, LX/DOF;->a(LX/0qq;LX/DOD;LX/DOJ;)V

    .line 2577166
    new-instance v0, LX/IRd;

    invoke-direct {v0, p0}, LX/IRd;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577167
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->c:LX/1CF;

    .line 2577168
    iget-object v2, v3, LX/0fz;->d:LX/0qm;

    move-object v2, v2

    .line 2577169
    invoke-virtual {v1, v0, v2}, LX/1CF;->a(LX/1CH;LX/0qm;)V

    .line 2577170
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->e:LX/1lu;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ay:LX/1gl;

    .line 2577171
    iget-object v2, v3, LX/0fz;->d:LX/0qm;

    move-object v2, v2

    .line 2577172
    invoke-virtual {v0, v1, v2}, LX/1lu;->a(LX/1gl;LX/0qm;)V

    .line 2577173
    return-void

    :cond_1
    move v8, v10

    .line 2577174
    goto :goto_0
.end method

.method public static B(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2577149
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2577150
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    .line 2577151
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DRd;

    invoke-virtual {v0, v2}, LX/DRd;->a(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DRd;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2577152
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aT:Z

    move v0, v0

    .line 2577153
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2577154
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2577155
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2577156
    goto :goto_0
.end method

.method public static H(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 2

    .prologue
    .line 2577145
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aJ:LX/IPY;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    if-nez v0, :cond_0

    .line 2577146
    :goto_0
    return-void

    .line 2577147
    :cond_0
    new-instance v0, LX/IRX;

    invoke-direct {v0, p0}, LX/IRX;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aK:LX/0fx;

    .line 2577148
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aK:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_0
.end method

.method public static J(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z
    .locals 3

    .prologue
    .line 2577144
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    sget-short v1, LX/100;->ab:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static M(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z
    .locals 1

    .prologue
    .line 2577142
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    .line 2577143
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->W:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EV;

    invoke-virtual {v0}, LX/1EV;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static P(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 1

    .prologue
    .line 2577140
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2577141
    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    .line 2577063
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2577064
    :goto_0
    return-void

    .line 2577065
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aV:LX/DRf;

    if-nez v0, :cond_1

    .line 2577066
    new-instance v0, LX/DRf;

    invoke-direct {v0}, LX/DRf;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aV:LX/DRf;

    .line 2577067
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aV:LX/DRf;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0, v1}, LX/DRf;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    goto :goto_0
.end method

.method private V()V
    .locals 4

    .prologue
    .line 2577130
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2577131
    :cond_0
    :goto_0
    return-void

    .line 2577132
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->G:LX/8ht;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->j()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ht;->f(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2577133
    const v0, 0x7f0820b7

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2577134
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63T;

    invoke-interface {v0, v1}, LX/63T;->setTitleHint(Ljava/lang/String;)V

    goto :goto_0

    .line 2577135
    :cond_2
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2577136
    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aH:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 2577137
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2577138
    invoke-interface {v0}, LX/1ZF;->lH_()V

    goto :goto_0
.end method

.method public static X(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2577120
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2577121
    iget-object v3, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v3

    .line 2577122
    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2577123
    iget-boolean v3, v0, LX/DNR;->v:Z

    move v0, v3

    .line 2577124
    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 2577125
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aS:Z

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 2577126
    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aS:Z

    .line 2577127
    invoke-static {p0, v2}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2577128
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2577129
    goto :goto_0
.end method

.method public static Y(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z
    .locals 2

    .prologue
    .line 2577119
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Z(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 3

    .prologue
    .line 2577112
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Y(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2577113
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2577114
    const v1, 0x7f081baa

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    .line 2577115
    const v1, 0x7f081ba9

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    .line 2577116
    const v1, 0x7f081ba8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2577117
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2577118
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2577096
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v1, v0}, LX/DJw;->b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2577097
    const-string v0, "extra_is_composer_intercept_status"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2577098
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    .line 2577099
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, LX/DJ4;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2577100
    new-instance v3, LX/5Rj;

    invoke-direct {v3}, LX/5Rj;-><init>()V

    .line 2577101
    iput-object v2, v3, LX/5Rj;->d:Ljava/lang/String;

    .line 2577102
    move-object v2, v3

    .line 2577103
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    .line 2577104
    iput-object v3, v2, LX/5Rj;->f:Ljava/lang/String;

    .line 2577105
    move-object v2, v2

    .line 2577106
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2577107
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2577108
    iput-object v0, v2, LX/5Rj;->e:Ljava/lang/Long;

    .line 2577109
    :cond_0
    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "extra_is_composer_intercept_attachments"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v2}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2577110
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v3, 0x0

    const/16 v4, 0x6dc

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v3, v2, v4, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2577111
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;I)V
    .locals 4

    .prologue
    .line 2577081
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2577082
    :cond_0
    :goto_0
    return-void

    .line 2577083
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->a()I

    move-result v0

    .line 2577084
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->b()I

    move-result v1

    .line 2577085
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    invoke-static {v2}, LX/9Oi;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;)LX/9Oi;

    move-result-object v2

    .line 2577086
    add-int/2addr v0, p1

    .line 2577087
    iput v0, v2, LX/9Oi;->a:I

    .line 2577088
    const/4 v0, -0x1

    if-eq v1, v0, :cond_2

    .line 2577089
    add-int v0, v1, p1

    .line 2577090
    iput v0, v2, LX/9Oi;->b:I

    .line 2577091
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2577092
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2577093
    invoke-virtual {v2}, LX/9Oi;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v2

    .line 2577094
    invoke-static {p0, v2, v1, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V

    goto :goto_0

    .line 2577095
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;LX/2lS;LX/DO3;LX/1CF;LX/0Ot;LX/1lu;LX/DNT;LX/0Ot;LX/DOF;LX/9T3;LX/DNq;LX/DNJ;LX/DNR;LX/0jU;LX/0Ot;LX/88n;LX/0Ot;LX/0Ot;LX/0iA;LX/0Ot;LX/ITk;LX/ISJ;LX/0bH;LX/DK3;LX/0zG;LX/0Ot;LX/1My;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/IRJ;LX/DKH;LX/8ht;LX/0Ot;LX/0Or;LX/0Ot;LX/0yc;LX/DKL;LX/0Ot;LX/0ad;LX/IPZ;LX/IUw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/1g8;Ljava/lang/Boolean;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/IPk;LX/Hgt;LX/0Or;LX/CGV;LX/0Xl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/ui/GroupsFeedFragment;",
            "LX/2lS;",
            "LX/DO3;",
            "LX/1CF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1lu;",
            "LX/DNT;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/DOF;",
            "LX/9T3;",
            "LX/DNq;",
            "LX/DNJ;",
            "LX/DNR;",
            "LX/0jU;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/88n;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;",
            "LX/0iA;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/ITk;",
            "LX/ISJ;",
            "LX/0bH;",
            "LX/DK3;",
            "LX/0zG;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "LX/1My;",
            "LX/DOp;",
            "LX/IVs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HidePinnedStoryPartDefinition;",
            ">;",
            "LX/IRJ;",
            "LX/DKH;",
            "LX/8ht;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0yc;",
            "LX/DKL;",
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;",
            "LX/0ad;",
            "LX/IPZ;",
            "LX/IUw;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DRd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hI;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1g8;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DJ6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ILp;",
            ">;",
            "LX/IPk;",
            "LX/Hgt;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/CGV;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2577080
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    iput-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b:LX/DO3;

    iput-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->c:LX/1CF;

    iput-object p4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->e:LX/1lu;

    iput-object p6, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->f:LX/DNT;

    iput-object p7, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->h:LX/DOF;

    iput-object p9, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->i:LX/9T3;

    iput-object p10, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->j:LX/DNq;

    iput-object p11, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    iput-object p12, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    iput-object p13, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->m:LX/0jU;

    iput-object p14, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->o:LX/88n;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->r:LX/0iA;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->t:LX/ITk;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->u:LX/ISJ;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->w:LX/DK3;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->y:LX/0Ot;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->z:LX/1My;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->A:LX/DOp;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B:LX/IVs;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->C:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->D:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->E:LX/IRJ;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->F:LX/DKH;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->G:LX/8ht;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->H:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->I:LX/0Or;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->J:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->K:LX/0yc;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->L:LX/DKL;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->M:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->O:LX/IPZ;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->P:LX/IUw;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Q:LX/0Ot;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->R:LX/0Ot;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->S:LX/0Ot;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->T:LX/0Uh;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->U:LX/1g8;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->V:Ljava/lang/Boolean;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->W:LX/0Ot;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->X:LX/0Or;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Y:LX/0Ot;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Z:LX/0Ot;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aa:LX/IPk;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ab:LX/Hgt;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ac:LX/0Or;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ad:LX/CGV;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ae:LX/0Xl;

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V
    .locals 2
    .param p0    # Lcom/facebook/groups/feed/ui/GroupsFeedFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2577068
    new-instance v0, LX/9OZ;

    invoke-direct {v0}, LX/9OZ;-><init>()V

    .line 2577069
    iput-object p1, v0, LX/9OZ;->r:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    .line 2577070
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    iput-object v1, v0, LX/9OZ;->A:Ljava/lang/String;

    .line 2577071
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ai:Ljava/lang/String;

    iput-object v1, v0, LX/9OZ;->C:Ljava/lang/String;

    .line 2577072
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p2, v0, LX/9OZ;->g:LX/15i;

    iput p3, v0, LX/9OZ;->h:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2577073
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v1

    invoke-virtual {v0}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    .line 2577074
    iput-object v0, v1, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2577075
    move-object v0, v1

    .line 2577076
    invoke-virtual {v0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    .line 2577077
    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2577078
    return-void

    .line 2577079
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 60

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v59

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static/range {v59 .. v59}, LX/2lS;->a(LX/0QB;)LX/2lS;

    move-result-object v3

    check-cast v3, LX/2lS;

    invoke-static/range {v59 .. v59}, LX/DO3;->a(LX/0QB;)LX/DO3;

    move-result-object v4

    check-cast v4, LX/DO3;

    invoke-static/range {v59 .. v59}, LX/1CF;->a(LX/0QB;)LX/1CF;

    move-result-object v5

    check-cast v5, LX/1CF;

    const/16 v6, 0x259

    move-object/from16 v0, v59

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v59 .. v59}, LX/1lu;->a(LX/0QB;)LX/1lu;

    move-result-object v7

    check-cast v7, LX/1lu;

    invoke-static/range {v59 .. v59}, LX/DNT;->a(LX/0QB;)LX/DNT;

    move-result-object v8

    check-cast v8, LX/DNT;

    const/16 v9, 0x19c6

    move-object/from16 v0, v59

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v59 .. v59}, LX/DOF;->a(LX/0QB;)LX/DOF;

    move-result-object v10

    check-cast v10, LX/DOF;

    const-class v11, LX/9T3;

    move-object/from16 v0, v59

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/9T3;

    const-class v12, LX/DNq;

    move-object/from16 v0, v59

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/DNq;

    invoke-static/range {v59 .. v59}, LX/DNJ;->a(LX/0QB;)LX/DNJ;

    move-result-object v13

    check-cast v13, LX/DNJ;

    invoke-static/range {v59 .. v59}, LX/DNR;->a(LX/0QB;)LX/DNR;

    move-result-object v14

    check-cast v14, LX/DNR;

    invoke-static/range {v59 .. v59}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v15

    check-cast v15, LX/0jU;

    const/16 v16, 0x3be

    move-object/from16 v0, v59

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {v59 .. v59}, LX/88n;->a(LX/0QB;)LX/88n;

    move-result-object v17

    check-cast v17, LX/88n;

    const/16 v18, 0xbca

    move-object/from16 v0, v59

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x1052

    move-object/from16 v0, v59

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {v59 .. v59}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v20

    check-cast v20, LX/0iA;

    const/16 v21, 0x1061

    move-object/from16 v0, v59

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const-class v22, LX/ITk;

    move-object/from16 v0, v59

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/ITk;

    const-class v23, LX/ISJ;

    move-object/from16 v0, v59

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/ISJ;

    invoke-static/range {v59 .. v59}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v24

    check-cast v24, LX/0bH;

    invoke-static/range {v59 .. v59}, LX/DK3;->a(LX/0QB;)LX/DK3;

    move-result-object v25

    check-cast v25, LX/DK3;

    invoke-static/range {v59 .. v59}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v26

    check-cast v26, LX/0zG;

    const/16 v27, 0x2be

    move-object/from16 v0, v59

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {v59 .. v59}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v28

    check-cast v28, LX/1My;

    invoke-static/range {v59 .. v59}, LX/DOp;->a(LX/0QB;)LX/DOp;

    move-result-object v29

    check-cast v29, LX/DOp;

    invoke-static/range {v59 .. v59}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v30

    check-cast v30, LX/IVs;

    const/16 v31, 0x2438

    move-object/from16 v0, v59

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x2414

    move-object/from16 v0, v59

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const-class v33, LX/IRJ;

    move-object/from16 v0, v59

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v33

    check-cast v33, LX/IRJ;

    invoke-static/range {v59 .. v59}, LX/DKH;->a(LX/0QB;)LX/DKH;

    move-result-object v34

    check-cast v34, LX/DKH;

    invoke-static/range {v59 .. v59}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v35

    check-cast v35, LX/8ht;

    const/16 v36, 0x455

    move-object/from16 v0, v59

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x14be

    move-object/from16 v0, v59

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v37

    const/16 v38, 0x2eb

    move-object/from16 v0, v59

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v38

    invoke-static/range {v59 .. v59}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v39

    check-cast v39, LX/0yc;

    invoke-static/range {v59 .. v59}, LX/DKL;->a(LX/0QB;)LX/DKL;

    move-result-object v40

    check-cast v40, LX/DKL;

    const/16 v41, 0x110

    move-object/from16 v0, v59

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v41

    invoke-static/range {v59 .. v59}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v42

    check-cast v42, LX/0ad;

    const-class v43, LX/IPZ;

    move-object/from16 v0, v59

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v43

    check-cast v43, LX/IPZ;

    const-class v44, LX/IUw;

    move-object/from16 v0, v59

    move-object/from16 v1, v44

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v44

    check-cast v44, LX/IUw;

    const/16 v45, 0x34e8

    move-object/from16 v0, v59

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v45

    const/16 v46, 0x2465

    move-object/from16 v0, v59

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v47, 0x12be

    move-object/from16 v0, v59

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v47

    invoke-static/range {v59 .. v59}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v48

    check-cast v48, LX/0Uh;

    invoke-static/range {v59 .. v59}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v49

    check-cast v49, LX/1g8;

    invoke-static/range {v59 .. v59}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v50

    check-cast v50, Ljava/lang/Boolean;

    const/16 v51, 0xb2a

    move-object/from16 v0, v59

    move/from16 v1, v51

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v51

    const/16 v52, 0x2367

    move-object/from16 v0, v59

    move/from16 v1, v52

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v52

    const/16 v53, 0xb2c

    move-object/from16 v0, v59

    move/from16 v1, v53

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v53

    const/16 v54, 0x2382

    move-object/from16 v0, v59

    move/from16 v1, v54

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v54

    const-class v55, LX/IPk;

    move-object/from16 v0, v59

    move-object/from16 v1, v55

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v55

    check-cast v55, LX/IPk;

    invoke-static/range {v59 .. v59}, LX/Hgt;->a(LX/0QB;)LX/Hgt;

    move-result-object v56

    check-cast v56, LX/Hgt;

    const/16 v57, 0x15e7

    move-object/from16 v0, v59

    move/from16 v1, v57

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v57

    invoke-static/range {v59 .. v59}, LX/CGV;->a(LX/0QB;)LX/CGV;

    move-result-object v58

    check-cast v58, LX/CGV;

    invoke-static/range {v59 .. v59}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v59

    check-cast v59, LX/0Xl;

    invoke-static/range {v2 .. v59}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;LX/2lS;LX/DO3;LX/1CF;LX/0Ot;LX/1lu;LX/DNT;LX/0Ot;LX/DOF;LX/9T3;LX/DNq;LX/DNJ;LX/DNR;LX/0jU;LX/0Ot;LX/88n;LX/0Ot;LX/0Ot;LX/0iA;LX/0Ot;LX/ITk;LX/ISJ;LX/0bH;LX/DK3;LX/0zG;LX/0Ot;LX/1My;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/IRJ;LX/DKH;LX/8ht;LX/0Ot;LX/0Or;LX/0Ot;LX/0yc;LX/DKL;LX/0Ot;LX/0ad;LX/IPZ;LX/IUw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/1g8;Ljava/lang/Boolean;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/IPk;LX/Hgt;LX/0Or;LX/CGV;LX/0Xl;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;LX/1ZX;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2577312
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2577313
    iget-object v2, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v2

    .line 2577314
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2577315
    :goto_0
    return-object v0

    .line 2577316
    :cond_0
    invoke-virtual {p1}, LX/1ZX;->a()Ljava/lang/String;

    move-result-object v2

    .line 2577317
    invoke-virtual {p1}, LX/1ZX;->b()Ljava/lang/String;

    move-result-object v3

    .line 2577318
    if-nez v2, :cond_1

    if-eqz v3, :cond_1

    .line 2577319
    invoke-virtual {v0, v3}, LX/0fz;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 2577320
    :cond_1
    if-eqz v2, :cond_3

    .line 2577321
    invoke-virtual {v0, v2}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2577322
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2577323
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2577324
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_2

    .line 2577325
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2577326
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 2577296
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2577297
    :cond_0
    :goto_0
    return-void

    .line 2577298
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2577299
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2577300
    const/4 v3, 0x0

    .line 2577301
    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_2

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move v2, v3

    .line 2577302
    :goto_1
    move v0, v2

    .line 2577303
    if-eqz v0, :cond_0

    .line 2577304
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    .line 2577305
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;LX/15i;I)V

    goto :goto_0

    .line 2577306
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2577307
    :cond_3
    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2577308
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2577309
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2577310
    const/4 v2, 0x1

    goto :goto_1

    :cond_5
    move v2, v3

    .line 2577311
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2577231
    if-nez p1, :cond_0

    .line 2577232
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->af:Ljava/lang/String;

    const-string v3, "Update group header with null value. Looks wrong."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2577233
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_d

    :cond_1
    move v0, v1

    .line 2577234
    :goto_0
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577235
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->G()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->G()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2577236
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2577237
    const v2, 0x7f081bac

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2577238
    const v2, 0x7f081bad

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2577239
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081bae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IRn;

    invoke-direct {v3, p0}, LX/IRn;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2577240
    const v2, 0x7f081ba8

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IRo;

    invoke-direct {v3, p0}, LX/IRo;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2577241
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2577242
    :cond_2
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2577243
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0, v1}, LX/DNJ;->c(Z)V

    .line 2577244
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v0, v2, :cond_f

    :cond_4
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2577245
    if-eqz v0, :cond_5

    .line 2577246
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    .line 2577247
    iget-object v2, v0, LX/DNJ;->B:LX/2iI;

    iget-object v3, v0, LX/DNJ;->t:LX/1Cw;

    invoke-virtual {v2, v3}, LX/2iI;->a(Landroid/widget/ListAdapter;)V

    .line 2577248
    :cond_5
    const/4 v2, 0x0

    .line 2577249
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_10

    :cond_6
    move v0, v2

    .line 2577250
    :goto_2
    move v0, v0

    .line 2577251
    if-eqz v0, :cond_7

    .line 2577252
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 2577253
    if-nez v0, :cond_14

    .line 2577254
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ab(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v2

    if-nez v2, :cond_e

    move v2, v1

    :goto_4
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    .line 2577255
    iget-boolean v3, v1, LX/DNJ;->I:Z

    move v3, v3

    .line 2577256
    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2577257
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->S()V

    .line 2577258
    if-eqz p1, :cond_9

    .line 2577259
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/IQV;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2577260
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const v1, 0x7f0d159f

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;

    .line 2577261
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aM:LX/IRb;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V

    .line 2577262
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    if-nez v1, :cond_9

    .line 2577263
    invoke-static {v0}, LX/IUw;->a(Lcom/facebook/groups/feed/ui/GroupsInMallGiantJoinButtonView;)LX/IUv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    .line 2577264
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aK:LX/0fx;

    if-nez v0, :cond_8

    .line 2577265
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->H(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577266
    :cond_8
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    invoke-virtual {v0}, LX/IUv;->b()V

    .line 2577267
    :cond_9
    :goto_5
    if-eqz p1, :cond_a

    .line 2577268
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->V()V

    .line 2577269
    :cond_a
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aF:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->a(LX/DZC;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DJw;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    if-ne v0, v1, :cond_16

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 2577270
    if-eqz v0, :cond_b

    .line 2577271
    new-instance v0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;

    invoke-direct {v0}, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;-><init>()V

    .line 2577272
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2577273
    iput-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->n:Ljava/lang/String;

    .line 2577274
    new-instance v1, LX/IRp;

    invoke-direct {v1, p0}, LX/IRp;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577275
    iput-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->m:LX/IQR;

    .line 2577276
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "SUGGEST_PURPOSE_DIALOG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2577277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aF:Z

    .line 2577278
    :cond_b
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_c

    .line 2577279
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2577280
    iget-object v3, v2, LX/DNR;->c:LX/0fz;

    move-object v2, v3

    .line 2577281
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-static {v1, v2, v3}, LX/IPu;->a(ILX/0fz;LX/1Qq;)I

    move-result v1

    .line 2577282
    iput v1, v0, LX/IPj;->v:I

    .line 2577283
    :cond_c
    return-void

    :cond_d
    move v0, v4

    .line 2577284
    goto/16 :goto_0

    :cond_e
    move v2, v4

    .line 2577285
    goto/16 :goto_4

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2577286
    :cond_10
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v2

    .line 2577287
    goto/16 :goto_2

    .line 2577288
    :cond_11
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    .line 2577289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3my;

    invoke-virtual {v0, v3}, LX/3my;->a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_13
    move v0, v2

    goto/16 :goto_2

    .line 2577290
    :cond_14
    iget-object v2, v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2577291
    if-eqz v0, :cond_7

    .line 2577292
    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d()V

    .line 2577293
    new-instance v2, LX/IRe;

    invoke-direct {v2, p0}, LX/IRe;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 2577294
    :cond_15
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    if-eqz v0, :cond_9

    .line 2577295
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aq:LX/IUv;

    invoke-virtual {v0}, LX/IUv;->a()V

    goto/16 :goto_5

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6
.end method

.method public static a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    .line 2577230
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, LX/9A3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p3}, LX/9A3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static aa(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2577224
    const-class v0, LX/63U;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/63U;

    .line 2577225
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->az:LX/63Q;

    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 2577226
    new-instance v0, LX/63Q;

    invoke-direct {v0}, LX/63Q;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->az:LX/63Q;

    .line 2577227
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->az:LX/63Q;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/63T;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k()LX/0g8;

    move-result-object v3

    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aH:Z

    if-nez v1, :cond_1

    move v6, v5

    :goto_0
    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2577228
    :cond_0
    return-void

    .line 2577229
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static ab(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z
    .locals 1

    .prologue
    .line 2577223
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ap:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ap:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 2577219
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->V:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2577220
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ab:LX/Hgt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2577221
    sget-object p0, LX/Hgs;->GROUP:LX/Hgs;

    invoke-static {v0, v1, p0}, LX/Hgt;->a(LX/Hgt;Landroid/content/Context;LX/Hgs;)Z

    .line 2577222
    :cond_0
    return-void
.end method

.method public static ad(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 1

    .prologue
    .line 2577215
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aG:Z

    if-eqz v0, :cond_0

    .line 2577216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aG:Z

    .line 2577217
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2577218
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V
    .locals 5

    .prologue
    .line 2577203
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2577204
    if-eqz p1, :cond_2

    sget-object v0, LX/88p;->COMMUNITY:LX/88p;

    move-object v1, v0

    .line 2577205
    :goto_0
    if-eqz p1, :cond_3

    sget-object v0, LX/DO3;->a:LX/0zS;

    .line 2577206
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    .line 2577207
    iget-boolean v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    if-eqz v3, :cond_0

    .line 2577208
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v3}, LX/2lS;->c()V

    .line 2577209
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v3}, LX/2lS;->b()V

    .line 2577210
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    .line 2577211
    :cond_0
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->o:LX/88n;

    new-instance v4, LX/IRg;

    invoke-direct {v4, p0, v0}, LX/IRg;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;LX/0zS;)V

    new-instance v0, LX/IRh;

    invoke-direct {v0, p0, v2}, LX/IRh;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    invoke-virtual {v3, v1, v4, v0}, LX/88n;->a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V

    .line 2577212
    :cond_1
    return-void

    .line 2577213
    :cond_2
    sget-object v0, LX/88p;->BASIC_INFO:LX/88p;

    move-object v1, v0

    goto :goto_0

    .line 2577214
    :cond_3
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_1
.end method

.method private d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2577185
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->f()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    .line 2577202
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->G:LX/8ht;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->T:LX/0Uh;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    move v2, p1

    invoke-static/range {v0 .. v5}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public static v(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 2

    .prologue
    .line 2577197
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v0, :cond_0

    .line 2577198
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2577199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2577200
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ap:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2577201
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2577192
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2577193
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2577194
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2577195
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2577196
    const/4 v0, 0x0

    return v0
.end method

.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2577189
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->E:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2577190
    sget-object v3, LX/DOr;->a:LX/DOr;

    move-object v3, v3

    .line 2577191
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsFeedFragment$15;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment$15;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2577187
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->A:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->C:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->D:LX/0Ot;

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    .line 2577188
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2577139
    const-string v0, "group_feed"

    return-object v0
.end method

.method public final a(LX/69s;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2577186
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2576761
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2576762
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2576763
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v2, v0, v1}, LX/2lS;->a(J)V

    .line 2576764
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    .line 2576765
    if-nez p1, :cond_0

    .line 2576766
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/16 v1, 0x6dc

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Kf;->a(ILandroid/app/Activity;)V

    .line 2576767
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->K:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2576768
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aE:Landroid/content/Context;

    .line 2576769
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576770
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    .line 2576771
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576772
    const-string v1, "group_feed_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ai:Ljava/lang/String;

    .line 2576773
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576774
    const-string v1, "group_tip_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aj:Ljava/lang/String;

    .line 2576775
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576776
    const-string v1, "drawer_position"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ak:I

    .line 2576777
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576778
    const-string v1, "group_feed_hoisted_story_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->al:Ljava/util/ArrayList;

    .line 2576779
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576780
    const-string v1, "parent_control_title_bar"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aH:Z

    .line 2576781
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576782
    const-string v1, "group_feed_hoisted_comment_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2576783
    new-instance v1, LX/B1S;

    invoke-direct {v1}, LX/B1S;-><init>()V

    .line 2576784
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    .line 2576785
    iput-object v2, v1, LX/B1S;->a:Ljava/lang/String;

    .line 2576786
    move-object v2, v1

    .line 2576787
    sget-object v3, LX/B1T;->GroupsFeed:LX/B1T;

    .line 2576788
    iput-object v3, v2, LX/B1S;->b:LX/B1T;

    .line 2576789
    move-object v2, v2

    .line 2576790
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->al:Ljava/util/ArrayList;

    .line 2576791
    iput-object v3, v2, LX/B1S;->c:Ljava/util/List;

    .line 2576792
    move-object v2, v2

    .line 2576793
    iput-object v0, v2, LX/B1S;->d:Ljava/util/List;

    .line 2576794
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->k:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->an:Lcom/facebook/api/feedtype/FeedType;

    .line 2576795
    new-instance v0, LX/IRk;

    invoke-direct {v0, p0}, LX/IRk;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->av:LX/DOD;

    .line 2576796
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->m:LX/0jU;

    invoke-virtual {v0, p0}, LX/0jU;->a(LX/0fi;)V

    .line 2576797
    new-instance v0, LX/IRl;

    invoke-direct {v0, p0}, LX/IRl;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ay:LX/1gl;

    .line 2576798
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->f:LX/DNT;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->an:Lcom/facebook/api/feedtype/FeedType;

    const/16 v3, 0xa

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    sget-short v4, LX/88j;->g:S

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2576799
    new-instance v0, LX/IRt;

    invoke-direct {v0, p0}, LX/IRt;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aA:LX/1L6;

    .line 2576800
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aA:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2576801
    new-instance v0, LX/IRr;

    invoke-direct {v0, p0}, LX/IRr;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aC:LX/IRr;

    .line 2576802
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->w:LX/DK3;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aC:LX/IRr;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2576803
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->w:LX/DK3;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aD:LX/DK4;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2576804
    new-instance v0, LX/IRs;

    invoke-direct {v0, p0}, LX/IRs;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aB:LX/1L6;

    .line 2576805
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aB:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2576806
    new-instance v0, LX/IRm;

    invoke-direct {v0, p0}, LX/IRm;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aI:LX/DKM;

    .line 2576807
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->L:LX/DKL;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aI:LX/DKM;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2576808
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2576809
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->U:LX/1g8;

    .line 2576810
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576811
    const-string v2, "group_view_referrer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2576812
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576813
    const-string v3, "group_feed_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/1g8;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576814
    return-void

    .line 2576815
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2576891
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 2576883
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2576884
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v1

    .line 2576885
    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2576886
    iget-object v1, v0, LX/0fz;->a:LX/0qq;

    move-object v1, v1

    .line 2576887
    invoke-virtual {v1, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2576888
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2576889
    :cond_0
    :goto_0
    return-void

    .line 2576890
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->P(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2576881
    invoke-static {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2576882
    return-void
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2576868
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aE:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 2576869
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aE:Landroid/content/Context;

    const/4 v1, 0x0

    .line 2576870
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v3, v4, :cond_4

    .line 2576871
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 2576872
    :cond_0
    :goto_0
    move v0, v1

    .line 2576873
    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    move v4, v0

    .line 2576874
    :goto_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ab(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_3
    move v3, p1

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2576875
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->S()V

    .line 2576876
    return-void

    :cond_1
    move v0, v5

    .line 2576877
    goto :goto_1

    :cond_2
    move v2, v5

    .line 2576878
    goto :goto_3

    :cond_3
    move v4, p2

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    .line 2576879
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    move v1, v1

    .line 2576880
    goto :goto_0
.end method

.method public final a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2576859
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->f()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2576860
    invoke-virtual {p1}, LX/0kb;->d()Z

    move-result v0

    .line 2576861
    if-eqz v0, :cond_1

    .line 2576862
    invoke-virtual {p2}, LX/4nk;->a()V

    .line 2576863
    :cond_0
    :goto_0
    return v1

    .line 2576864
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 2576865
    :goto_1
    if-eqz v0, :cond_4

    sget-object v0, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    :goto_2
    invoke-virtual {p2, v0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0

    .line 2576866
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2576867
    :cond_4
    sget-object v0, LX/DBa;->NO_CONNECTION:LX/DBa;

    goto :goto_2
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2576852
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    .line 2576853
    if-nez v0, :cond_0

    .line 2576854
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2576855
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2576856
    :cond_0
    if-nez v0, :cond_1

    .line 2576857
    const/4 v0, 0x0

    .line 2576858
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "group_id"

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2576848
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2576849
    if-eqz v0, :cond_0

    .line 2576850
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2576851
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 2576845
    if-eqz p1, :cond_0

    .line 2576846
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576847
    :cond_0
    return-void
.end method

.method public final d()LX/1Cv;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2576830
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->T:LX/0Uh;

    const/16 v1, 0x470

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2576831
    if-eqz v0, :cond_0

    .line 2576832
    sget-boolean v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ag:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->S:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v0

    .line 2576833
    :goto_0
    new-instance v1, LX/DRc;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->x:LX/0zG;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ar:Landroid/view/ViewStub;

    invoke-direct {v1, v2, v3, v4, v0}, LX/DRc;-><init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;I)V

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    .line 2576834
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    new-instance v1, LX/IRa;

    invoke-direct {v1, p0}, LX/IRa;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576835
    iput-object v1, v0, LX/DRc;->h:LX/DRb;

    .line 2576836
    :cond_0
    new-instance v0, LX/IRc;

    invoke-direct {v0, p0}, LX/IRc;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aM:LX/IRb;

    .line 2576837
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->t:LX/ITk;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aM:LX/IRb;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    invoke-virtual {v0, v1, v2}, LX/ITk;->a(LX/IRb;LX/DRc;)LX/ITj;

    move-result-object v0

    .line 2576838
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->u:LX/ISJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aM:LX/IRb;

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    invoke-virtual {v1, v2, v3}, LX/ISJ;->a(LX/IRb;LX/DRc;)LX/ISI;

    move-result-object v1

    .line 2576839
    new-instance v2, LX/IVw;

    invoke-direct {v2, v0, v1}, LX/IVw;-><init>(LX/ITj;LX/ISI;)V

    move-object v0, v2

    .line 2576840
    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    .line 2576841
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aj:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2576842
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/ISH;->a(Ljava/lang/String;)V

    .line 2576843
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    return-object v0

    .line 2576844
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2576827
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2576828
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2576829
    return-void
.end method

.method public final e()LX/DRf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2576825
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->S()V

    .line 2576826
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aV:LX/DRf;

    return-object v0
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 2576823
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2576824
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2576819
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d(I)Landroid/view/View;

    move-result-object v1

    .line 2576820
    if-eqz v1, :cond_0

    .line 2576821
    instance-of v0, v1, LX/63R;

    .line 2576822
    :cond_0
    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2576816
    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d(I)Landroid/view/View;

    move-result-object v1

    .line 2576817
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2576818
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2576759
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->au:Z

    .line 2576760
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2577011
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 2577062
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2577059
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    .line 2577060
    iget-object p0, v0, LX/DNJ;->B:LX/2iI;

    move-object v0, p0

    .line 2577061
    return-object v0
.end method

.method public final kR_()LX/0cQ;
    .locals 1

    .prologue
    .line 2577058
    sget-object v0, LX/0cQ;->GROUP_ALBUM_FRAGMENT:LX/0cQ;

    return-object v0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2577056
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->P(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577057
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2577054
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2577055
    return-void
.end method

.method public final mK_()LX/63R;
    .locals 1

    .prologue
    .line 2577053
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 2577012
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2577013
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2577014
    if-nez p2, :cond_1

    const/16 v0, 0x6de

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aU:Z

    if-eqz v0, :cond_1

    .line 2577015
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Z(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577016
    iput-boolean v6, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aU:Z

    .line 2577017
    :cond_0
    :goto_0
    return-void

    .line 2577018
    :cond_1
    if-ne p2, v5, :cond_0

    .line 2577019
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2577020
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2577021
    :sswitch_0
    invoke-static {p0, v7}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    goto :goto_0

    .line 2577022
    :sswitch_1
    const-string v0, "extra_is_composer_intercept_sell"

    invoke-virtual {p3, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2577023
    invoke-direct {p0, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 2577024
    :cond_2
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2577025
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2577026
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2577027
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2577028
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2iz;->a(Ljava/lang/String;)V

    .line 2577029
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->M(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2577030
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aN:LX/DJ6;

    if-nez v1, :cond_4

    .line 2577031
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->X:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DJ6;

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aN:LX/DJ6;

    .line 2577032
    :cond_4
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v2

    .line 2577033
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aN:LX/DJ6;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {v2, v1}, LX/DJw;->a(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v1

    .line 2577034
    iput-object v1, v3, LX/DJ6;->f:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 2577035
    move-object v1, v3

    .line 2577036
    new-instance v2, LX/IRY;

    invoke-direct {v2, p0}, LX/IRY;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2577037
    iput-object v2, v1, LX/DJ6;->i:LX/DJr;

    .line 2577038
    move-object v1, v1

    .line 2577039
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    sget-object v2, LX/21D;->GROUP_FEED:LX/21D;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/DJ6;->a(Ljava/lang/String;LX/21D;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2577040
    :sswitch_2
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2577041
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 2577042
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->F:LX/DKH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    new-instance v3, LX/IRZ;

    invoke-direct {v3, p0, v0}, LX/IRZ;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Landroid/net/Uri;)V

    invoke-virtual {v1, v2, v0, v3}, LX/DKH;->a(Ljava/lang/String;Landroid/net/Uri;LX/0TF;)V

    .line 2577043
    goto/16 :goto_0

    .line 2577044
    :sswitch_3
    const-string v0, "extra_album"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2577045
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/ISn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2577046
    :sswitch_4
    if-eqz v0, :cond_5

    if-eqz p3, :cond_5

    const-string v1, "submitted_email"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2577047
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2577048
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f083013

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "submitted_email"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a00d5

    const v3, 0x7f0a05f1

    invoke-static {v1, v0, v5, v2, v3}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v0

    invoke-virtual {v0}, LX/4nm;->a()V

    .line 2577049
    :cond_5
    invoke-static {p0, v7}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    goto/16 :goto_0

    .line 2577050
    :sswitch_5
    if-eqz v0, :cond_0

    .line 2577051
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2577052
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082514

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a00d5

    const v3, 0x7f0a05f1

    invoke-static {v1, v0, v5, v2, v3}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v0

    invoke-virtual {v0}, LX/4nm;->a()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_0
        0x12d -> :sswitch_2
        0x3e7 -> :sswitch_5
        0x6dc -> :sswitch_1
        0x7ad -> :sswitch_4
        0x7ae -> :sswitch_0
        0x7af -> :sswitch_0
        0x7c7 -> :sswitch_3
    .end sparse-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2576894
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2576895
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2576896
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2576897
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x3420962f    # -2.9283234E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2576980
    if-eqz p3, :cond_0

    .line 2576981
    const-string v0, "update_cover_photo_key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aG:Z

    .line 2576982
    :cond_0
    const v0, 0x7f03084b

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    .line 2576983
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const v2, 0x7f0d159e

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ap:Landroid/view/View;

    .line 2576984
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const v2, 0x7f0d15a0

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ar:Landroid/view/ViewStub;

    .line 2576985
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const v2, 0x7f0d15a2

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->as:Landroid/view/ViewStub;

    .line 2576986
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->A()V

    .line 2576987
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->j:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->an:Lcom/facebook/api/feedtype/FeedType;

    iget-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->i:LX/9T3;

    iget-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->an:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p1, p2}, LX/9T3;->a(Lcom/facebook/api/feedtype/FeedType;)LX/9T2;

    move-result-object p1

    iget-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2576988
    iget-object p3, p2, LX/DNR;->c:LX/0fz;

    move-object p2, p3

    .line 2576989
    iget-object p3, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->av:LX/DOD;

    invoke-virtual {v0, v2, p1, p2, p3}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ao:LX/DNp;

    .line 2576990
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    sget-short v2, LX/9Lj;->a:S

    const/4 p1, 0x0

    invoke-interface {v0, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2576991
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aa:LX/IPk;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->as:Landroid/view/ViewStub;

    iget p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ak:I

    invoke-virtual {v0, v2, p1}, LX/IPk;->a(Landroid/view/ViewStub;I)LX/IPj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    .line 2576992
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->J(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2576993
    :goto_0
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->H(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576994
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ae:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance p1, LX/IRi;

    invoke-direct {p1, p0}, LX/IRi;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-interface {v0, v2, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aW:LX/0Yb;

    .line 2576995
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aW:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2576996
    new-instance v0, LX/9OZ;

    invoke-direct {v0}, LX/9OZ;-><init>()V

    .line 2576997
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    iput-object v2, v0, LX/9OZ;->A:Ljava/lang/String;

    .line 2576998
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ai:Ljava/lang/String;

    iput-object v2, v0, LX/9OZ;->C:Ljava/lang/String;

    .line 2576999
    new-instance v2, LX/9OQ;

    invoke-direct {v2}, LX/9OQ;-><init>()V

    .line 2577000
    invoke-virtual {v0}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    .line 2577001
    iput-object v0, v2, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2577002
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    .line 2577003
    iput-object v0, v2, LX/9OQ;->n:Ljava/lang/String;

    .line 2577004
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2577005
    invoke-static {p0, v3}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2577006
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ac()V

    .line 2577007
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x178afbe8

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0

    .line 2577008
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->N:LX/0ad;

    sget-short v2, LX/100;->ac:S

    const/4 p1, 0x0

    invoke-interface {v0, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v2, LX/0zw;

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const p1, 0x7f0d2b5e

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v2

    .line 2577009
    :goto_1
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->O:LX/IPZ;

    iget-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    iget-object p2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance p3, LX/IRW;

    invoke-direct {p3, p0}, LX/IRW;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    invoke-virtual {v2, v0, p1, p2, p3}, LX/IPZ;->a(LX/0zw;LX/DN9;Landroid/content/Context;LX/IPW;)LX/IPY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aJ:LX/IPY;

    goto/16 :goto_0

    .line 2577010
    :cond_3
    new-instance v2, LX/0zw;

    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aw:Landroid/view/ViewGroup;

    const p1, 0x7f0d2b5f

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v2

    goto :goto_1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x27bb1ac2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576968
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2576969
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aA:LX/1L6;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2576970
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aB:LX/1L6;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2576971
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->w:LX/DK3;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aC:LX/IRr;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2576972
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->w:LX/DK3;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aD:LX/DK4;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2576973
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->L:LX/DKL;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aI:LX/DKM;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2576974
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->K:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2576975
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->z:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2576976
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->m:LX/0jU;

    invoke-virtual {v1, p0}, LX/0jU;->b(LX/0fi;)V

    .line 2576977
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aK:LX/0fx;

    if-eqz v1, :cond_0

    .line 2576978
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k()LX/0g8;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aK:LX/0fx;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2576979
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x16e22c51

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1e916b44

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576952
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2576953
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->h:LX/DOF;

    invoke-virtual {v1}, LX/DOF;->a()V

    .line 2576954
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->c:LX/1CF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2576955
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2576956
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ao:LX/DNp;

    if-eqz v1, :cond_0

    .line 2576957
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ao:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2576958
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->o:LX/88n;

    invoke-virtual {v1}, LX/88n;->a()V

    .line 2576959
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    if-eqz v1, :cond_1

    .line 2576960
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    invoke-virtual {v1}, LX/ISH;->a()V

    .line 2576961
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->b()V

    .line 2576962
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->k()V

    .line 2576963
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    if-eqz v1, :cond_2

    .line 2576964
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    invoke-virtual {v1}, LX/IPj;->a()V

    .line 2576965
    :cond_2
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aW:LX/0Yb;

    if-eqz v1, :cond_3

    .line 2576966
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aW:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2576967
    :cond_3
    const/16 v1, 0x2b

    const v2, -0x1a150d0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x169b2970

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576942
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2576943
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aN:LX/DJ6;

    if-eqz v1, :cond_0

    .line 2576944
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aN:LX/DJ6;

    invoke-virtual {v1}, LX/DJ6;->a()V

    .line 2576945
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aQ:Z

    .line 2576946
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v1}, LX/2lS;->a()V

    .line 2576947
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->z:LX/1My;

    invoke-virtual {v1}, LX/1My;->d()V

    .line 2576948
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->i()V

    .line 2576949
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->J(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2576950
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aJ:LX/IPY;

    invoke-virtual {v1}, LX/IPY;->g()V

    .line 2576951
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x42950c9a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x263fc448

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576911
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2576912
    const/4 v2, 0x0

    .line 2576913
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->r:LX/0iA;

    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v5, LX/1wS;

    invoke-virtual {v1, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/1wS;

    .line 2576914
    if-eqz v1, :cond_5

    .line 2576915
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    .line 2576916
    if-nez v4, :cond_4

    move v1, v2

    .line 2576917
    :goto_0
    move v1, v1

    .line 2576918
    if-eqz v1, :cond_0

    .line 2576919
    :goto_1
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->z:LX/1My;

    invoke-virtual {v1}, LX/1My;->e()V

    .line 2576920
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ad(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576921
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->j()V

    .line 2576922
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 2576923
    const/16 v1, 0x2b

    const v2, -0x477588de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2576924
    :cond_0
    const/4 v1, 0x0

    .line 2576925
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->r:LX/0iA;

    sget-object v4, LX/77s;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v5, LX/77s;

    invoke-virtual {v2, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/77s;

    .line 2576926
    if-nez v2, :cond_6

    .line 2576927
    const/4 v2, 0x0

    .line 2576928
    :goto_2
    move-object v2, v2

    .line 2576929
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v4, v2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2576930
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->r()V

    goto :goto_1

    .line 2576931
    :cond_1
    if-eqz v2, :cond_2

    .line 2576932
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2hf;

    invoke-virtual {v1, v2}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v1

    .line 2576933
    :cond_2
    instance-of v2, v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v2, :cond_3

    .line 2576934
    check-cast v1, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2576935
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d159e

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->at:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v1, v2, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2576936
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ap:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2576937
    :cond_3
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    goto :goto_1

    .line 2576938
    :cond_4
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->r:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-virtual {v1}, LX/1wS;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2576939
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->H:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2576940
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 2576941
    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2576908
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2576909
    const-string v0, "update_cover_photo_key"

    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aG:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2576910
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x516b09af

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576905
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2576906
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->V()V

    .line 2576907
    const/16 v1, 0x2b

    const v2, -0x951bb67

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5a7fabb8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576898
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStop()V

    .line 2576899
    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aH:Z

    if-nez v1, :cond_0

    .line 2576900
    iget-boolean v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aH:Z

    if-eqz v1, :cond_1

    .line 2576901
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x38dfc903

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2576902
    :cond_1
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2576903
    if-eqz v1, :cond_0

    .line 2576904
    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method
