.class public Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2580832
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    .line 2580833
    return-void
.end method


# virtual methods
.method public final mJ_()V
    .locals 0

    .prologue
    .line 2580834
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2580835
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2580836
    const v0, 0x7f0d15fc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2580837
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    check-cast v0, LX/IUd;

    .line 2580838
    iget-object p0, v0, LX/IUd;->b:Landroid/support/v4/app/Fragment;

    move-object v0, p0

    .line 2580839
    check-cast v0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;

    .line 2580840
    if-eqz v0, :cond_0

    .line 2580841
    iget-object p0, v0, Lcom/facebook/groups/feed/ui/GroupsYourPostsFragment;->f:LX/DNJ;

    invoke-virtual {p0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2580842
    :cond_0
    return-void
.end method

.method public final synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x42225a7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580843
    const v1, 0x7f03086e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v1, v1

    .line 2580844
    const/16 v2, 0x2b

    const v3, 0x58a5d6e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2580845
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2580846
    const v0, 0x7f0d15fc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2580847
    new-instance v1, LX/IUd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/IUd;-><init>(Lcom/facebook/groups/feed/ui/GroupsYourPostsFeedsFragment;LX/0gc;)V

    .line 2580848
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2580849
    const v1, 0x7f0d15fb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2580850
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2580851
    return-void
.end method
