.class public Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.source ""


# static fields
.field public static final t:Ljava/lang/String;


# instance fields
.field public a:LX/ISx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Sx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IRx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IW7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CGV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public s:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2578530
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2578529
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;-><init>()V

    return-void
.end method

.method public static v(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V
    .locals 4

    .prologue
    .line 2578527
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->e:LX/1Ck;

    const-string v1, "fetch_member_bio_header"

    new-instance v2, LX/ISs;

    invoke-direct {v2, p0}, LX/ISs;-><init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V

    new-instance v3, LX/ISt;

    invoke-direct {v3, p0}, LX/ISt;-><init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2578528
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
    .locals 2

    .prologue
    .line 2578523
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->b:LX/9Sx;

    .line 2578524
    new-instance p0, LX/9Sw;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v1

    check-cast v1, LX/0pn;

    invoke-direct {p0, p1, v1}, LX/9Sw;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2578525
    move-object v0, p0

    .line 2578526
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2578487
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-static {v9}, LX/ISx;->b(LX/0QB;)LX/ISx;

    move-result-object v3

    check-cast v3, LX/ISx;

    const-class v4, LX/9Sx;

    invoke-interface {v9, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9Sx;

    const-class v5, LX/IRx;

    invoke-interface {v9, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IRx;

    new-instance v7, LX/IW7;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-direct {v7, v6}, LX/IW7;-><init>(LX/0tX;)V

    move-object v6, v7

    check-cast v6, LX/IW7;

    invoke-static {v9}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    const/16 v8, 0x1ce

    invoke-static {v9, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v0, 0x238d

    invoke-static {v9, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    iput-object v3, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->a:LX/ISx;

    iput-object v4, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->b:LX/9Sx;

    iput-object v5, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->c:LX/IRx;

    iput-object v6, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->d:LX/IW7;

    iput-object v7, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->g:LX/0Ot;

    .line 2578488
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2578489
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->h:Ljava/lang/String;

    .line 2578490
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2578491
    const-string v1, "group_member_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->i:Ljava/lang/String;

    .line 2578492
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/ISu;

    invoke-direct {v2, p0}, LX/ISu;-><init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->s:LX/0Yb;

    .line 2578493
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2578494
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->v(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V

    .line 2578495
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2578496
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2578513
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2578514
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->h:Ljava/lang/String;

    .line 2578515
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2578516
    move-object v1, v0

    .line 2578517
    sget-object v2, LX/B1T;->GroupMemberPosts:LX/B1T;

    .line 2578518
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2578519
    move-object v1, v1

    .line 2578520
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->i:Ljava/lang/String;

    .line 2578521
    iput-object v2, v1, LX/B1S;->f:Ljava/lang/String;

    .line 2578522
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->q:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final d()LX/DNC;
    .locals 6

    .prologue
    .line 2578507
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->c:LX/IRx;

    .line 2578508
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    move-object v1, v1

    .line 2578509
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v2, v2

    .line 2578510
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081baf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->a:LX/ISx;

    .line 2578511
    sget-object v5, LX/DOt;->a:LX/DOt;

    move-object v5, v5

    .line 2578512
    invoke-virtual/range {v0 .. v5}, LX/IRx;->a(LX/DNR;LX/DNJ;Ljava/lang/String;LX/1Cv;LX/1PT;)LX/IRw;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2578506
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2578502
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2578503
    if-eqz v0, :cond_0

    .line 2578504
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2578505
    :cond_0
    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d29a990

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2578498
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->s:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2578499
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->s:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2578500
    :cond_0
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->onDestroyView()V

    .line 2578501
    const/16 v1, 0x2b

    const v2, 0x3823574e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 2578497
    const/4 v0, 0x0

    return v0
.end method
