.class public Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Xp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/IQQ;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel$ActionLinksModel$GroupStoryTopicsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:Landroid/widget/EditText;

.field private final r:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final s:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$FetchGroupPostAssignedTopicsModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2576018
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2576019
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    .line 2576020
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->p:Ljava/util/Set;

    .line 2576021
    new-instance v0, LX/IQx;

    invoke-direct {v0, p0}, LX/IQx;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->r:LX/0TF;

    .line 2576022
    new-instance v0, LX/IQy;

    invoke-direct {v0, p0}, LX/IQy;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->s:LX/0TF;

    return-void
.end method

.method public static b(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V
    .locals 3

    .prologue
    .line 2576023
    iget-boolean v0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->j:Z

    if-nez v0, :cond_1

    .line 2576024
    new-instance v0, LX/9ME;

    invoke-direct {v0}, LX/9ME;-><init>()V

    move-object v0, v0

    .line 2576025
    const-string v1, "count"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2576026
    const-string v1, "group_id"

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2576027
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    if-eqz v1, :cond_0

    .line 2576028
    const-string v1, "topics_after"

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2576029
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2576030
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->r:LX/0TF;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576031
    :cond_1
    return-void
.end method

.method public static l(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V
    .locals 2

    .prologue
    .line 2576032
    new-instance v0, Landroid/content/Intent;

    const-string v1, "group_discussion_topics_mutation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2576033
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->d:LX/0Xp;

    invoke-virtual {v1, v0}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2576034
    return-void
.end method


# virtual methods
.method public final synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7c9ea97

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576035
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, LX/0Tf;

    invoke-static {v1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v1}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v8

    check-cast v8, LX/0Xp;

    const/16 p3, 0x12c4

    invoke-static {v1, p3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v5, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->a:LX/0tX;

    iput-object v6, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b:LX/0Tf;

    iput-object v7, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->c:Ljava/lang/String;

    iput-object v8, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->d:LX/0Xp;

    iput-object p3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->e:LX/0Or;

    iput-object v1, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 2576036
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2576037
    const-string v2, "group_feed_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->m:Ljava/lang/String;

    .line 2576038
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2576039
    const-string v2, "groups_discussion_topics_story_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->n:Ljava/lang/String;

    .line 2576040
    const v1, 0x7f03083b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2576041
    const v2, 0x7f0d1580

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 2576042
    new-instance v3, LX/IQQ;

    invoke-direct {v3}, LX/IQQ;-><init>()V

    iput-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    .line 2576043
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    new-instance v5, LX/IQu;

    invoke-direct {v5, p0}, LX/IQu;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    .line 2576044
    iput-object v5, v3, LX/IQQ;->d:LX/IQu;

    .line 2576045
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    new-instance v5, LX/IQv;

    invoke-direct {v5, p0}, LX/IQv;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    .line 2576046
    iput-object v5, v3, LX/IQQ;->c:LX/IQv;

    .line 2576047
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2576048
    new-instance v3, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v3, v5}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2576049
    new-instance v3, LX/IQw;

    invoke-direct {v3, p0}, LX/IQw;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2576050
    invoke-static {p0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    .line 2576051
    new-instance v2, LX/9MD;

    invoke-direct {v2}, LX/9MD;-><init>()V

    move-object v2, v2

    .line 2576052
    const-string v3, "story_id"

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2576053
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2576054
    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->s:LX/0TF;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b:LX/0Tf;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576055
    move-object v1, v1

    .line 2576056
    const/16 v2, 0x2b

    const v3, 0x5a614ba4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f58e2a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576057
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2576058
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2576059
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2576060
    new-instance v1, LX/9TL;

    invoke-direct {v1}, LX/9TL;-><init>()V

    move-object v5, v1

    .line 2576061
    const-string v1, "%1$s %2$s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->n:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v1, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2576062
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->p:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2576063
    iget-object v8, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-interface {v8, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2576064
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2576065
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2576066
    iget-object v8, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->p:Ljava/util/Set;

    invoke-interface {v8, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2576067
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2576068
    :cond_3
    new-instance v1, LX/4Fs;

    invoke-direct {v1}, LX/4Fs;-><init>()V

    iget-object v7, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->c:Ljava/lang/String;

    .line 2576069
    const-string v8, "actor_id"

    invoke-virtual {v1, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576070
    move-object v1, v1

    .line 2576071
    iget-object v7, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->n:Ljava/lang/String;

    .line 2576072
    const-string v8, "story_id"

    invoke-virtual {v1, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576073
    move-object v1, v1

    .line 2576074
    const-string v7, "client_mutation_id"

    invoke-virtual {v1, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576075
    move-object v1, v1

    .line 2576076
    const-string v6, "topics_to_add"

    invoke-virtual {v1, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2576077
    move-object v1, v1

    .line 2576078
    const-string v2, "topics_to_remove"

    invoke-virtual {v1, v2, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2576079
    move-object v1, v1

    .line 2576080
    const-string v2, "story_input"

    invoke-virtual {v5, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2576081
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2576082
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2576083
    new-instance v2, LX/IQz;

    invoke-direct {v2, p0}, LX/IQz;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b:LX/0Tf;

    invoke-static {v1, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576084
    const/16 v1, 0x2b

    const v2, -0x7da79807

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x78af6881

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576085
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2576086
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2576087
    if-nez v1, :cond_0

    .line 2576088
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x244a1888

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2576089
    :cond_0
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2576090
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081b7f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0
.end method
