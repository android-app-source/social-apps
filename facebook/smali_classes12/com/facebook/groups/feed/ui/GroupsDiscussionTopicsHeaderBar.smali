.class public Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2576309
    const-class v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2576310
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2576311
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2576312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576313
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2576314
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576315
    const p1, 0x7f030847

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2576316
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->setOrientation(I)V

    .line 2576317
    const/16 p1, 0x11

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->setGravity(I)V

    .line 2576318
    const p1, 0x7f0d090e

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2576319
    const p1, 0x7f0d1594

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2576320
    const p1, 0x7f0d1595

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2576321
    const p1, 0x7f0d1592

    invoke-virtual {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2576322
    return-void
.end method
