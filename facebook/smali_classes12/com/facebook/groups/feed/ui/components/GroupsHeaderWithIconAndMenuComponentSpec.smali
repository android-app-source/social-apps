.class public Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pf;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/1vb;

.field public final d:LX/1VE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2581677
    const-class v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1vb;LX/1VE;LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581662
    iput-object p1, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->c:LX/1vb;

    .line 2581663
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->d:LX/1VE;

    .line 2581664
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->b:LX/1nu;

    .line 2581665
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;
    .locals 6

    .prologue
    .line 2581666
    const-class v1, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;

    monitor-enter v1

    .line 2581667
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581668
    sput-object v2, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581669
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581670
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581671
    new-instance p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v4

    check-cast v4, LX/1VE;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;-><init>(LX/1vb;LX/1VE;LX/1nu;)V

    .line 2581672
    move-object v0, p0

    .line 2581673
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581674
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581675
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
