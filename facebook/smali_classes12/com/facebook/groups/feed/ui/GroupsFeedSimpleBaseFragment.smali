.class public abstract Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;
.source ""


# instance fields
.field private a:Lcom/facebook/api/feedtype/FeedType;

.field public b:LX/DNp;

.field public c:LX/DNC;

.field public j:LX/DNT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/DNq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DNJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/DNR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568151
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;

    invoke-static {p0}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v1

    check-cast v1, LX/DNT;

    const-class v2, LX/DNq;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/DNq;

    invoke-static {p0}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v3

    check-cast v3, LX/DNJ;

    invoke-static {p0}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object p0

    check-cast p0, LX/DNR;

    iput-object v1, p1, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->j:LX/DNT;

    iput-object v2, p1, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->k:LX/DNq;

    iput-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    iput-object p0, p1, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2568152
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->a(Landroid/os/Bundle;)V

    .line 2568153
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2568154
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->d()LX/DNC;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->c:LX/DNC;

    .line 2568155
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->b()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 2568156
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->j:LX/DNT;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a:Lcom/facebook/api/feedtype/FeedType;

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, LX/DNT;->a(Lcom/facebook/api/feedtype/FeedType;II)V

    .line 2568157
    return-void
.end method

.method public abstract b()Lcom/facebook/api/feedtype/FeedType;
.end method

.method public abstract d()LX/DNC;
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2568148
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2568149
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2568150
    return-void
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public k()V
    .locals 2

    .prologue
    .line 2568133
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2568134
    if-eqz v0, :cond_0

    .line 2568135
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2568136
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2568137
    :cond_0
    return-void
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2568131
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->j()V

    .line 2568132
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4f5eec84

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2568123
    const v0, 0x7f03084b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/view/ViewGroup;

    .line 2568124
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->j:LX/DNT;

    new-instance v3, LX/IRu;

    invoke-direct {v3, p0}, LX/IRu;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;)V

    const-wide/16 v4, 0x3e8

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/DNR;->a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V

    .line 2568125
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    .line 2568126
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v3, v1

    .line 2568127
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    iget-object v5, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->c:LX/DNC;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->t()Z

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v4, p0

    invoke-virtual/range {v0 .. v9}, LX/DNJ;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V

    .line 2568128
    new-instance v0, LX/IRv;

    invoke-direct {v0, p0}, LX/IRv;-><init>(Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;)V

    .line 2568129
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->k:LX/DNq;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p0, v4}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;

    move-result-object v4

    invoke-virtual {v1, v2, v4, v3, v0}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->b:LX/DNp;

    .line 2568130
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, -0x74c9247

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v10
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x46b28e73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568138
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onDestroyView()V

    .line 2568139
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->b:LX/DNp;

    invoke-virtual {v1}, LX/DNp;->a()V

    .line 2568140
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->h()V

    .line 2568141
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->c:LX/DNC;

    invoke-virtual {v1}, LX/DNC;->a()LX/1Qq;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2568142
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->c:LX/DNC;

    invoke-virtual {v1}, LX/DNC;->a()LX/1Qq;

    move-result-object v1

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2568143
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x76a55675

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7607e2d2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568144
    invoke-super {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->onStart()V

    .line 2568145
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->k()V

    .line 2568146
    const/16 v1, 0x2b

    const v2, 0x78e22225

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 2568147
    const/4 v0, 0x1

    return v0
.end method
