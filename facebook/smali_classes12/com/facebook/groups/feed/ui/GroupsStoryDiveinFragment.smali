.class public Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/IRG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2580656
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;

    const-class p0, LX/IRG;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/IRG;

    iput-object v1, p1, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->a:LX/IRG;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
    .locals 1

    .prologue
    .line 2580655
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2580644
    const-string v0, "GROUP_DIVEIN_POSTS"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2580657
    const-class v0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2580658
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2580659
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->b:Ljava/lang/String;

    .line 2580660
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2580661
    const-string v1, "groups_divein_story_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->c:Ljava/lang/String;

    .line 2580662
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2580663
    const-string v1, "groups_divein_header_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->d:Ljava/lang/String;

    .line 2580664
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2580665
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2580645
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2580646
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->b:Ljava/lang/String;

    .line 2580647
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2580648
    move-object v1, v0

    .line 2580649
    sget-object v2, LX/B1T;->GroupStoryDiveinPosts:LX/B1T;

    .line 2580650
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2580651
    move-object v1, v1

    .line 2580652
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->c:Ljava/lang/String;

    .line 2580653
    iput-object v2, v1, LX/B1S;->h:Ljava/lang/String;

    .line 2580654
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->w:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final d()LX/DNC;
    .locals 11

    .prologue
    .line 2580638
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->a:LX/IRG;

    .line 2580639
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    move-object v1, v1

    .line 2580640
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v2, v2

    .line 2580641
    new-instance v3, LX/IRF;

    const-class v4, LX/IRJ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/IRJ;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v6

    check-cast v6, LX/DOp;

    invoke-static {v0}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v7

    check-cast v7, LX/IVs;

    const/16 v8, 0x2434

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    move-object v9, v1

    move-object v10, v2

    invoke-direct/range {v3 .. v10}, LX/IRF;-><init>(LX/IRJ;Landroid/content/Context;LX/DOp;LX/IVs;LX/0Ot;LX/DNR;LX/DNJ;)V

    .line 2580642
    move-object v0, v3

    .line 2580643
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2580637
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsStoryDiveinFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2580633
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2580634
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v0, v0

    .line 2580635
    invoke-virtual {v0, p1, p2, p3}, LX/DNJ;->a(IILandroid/content/Intent;)V

    .line 2580636
    return-void
.end method
