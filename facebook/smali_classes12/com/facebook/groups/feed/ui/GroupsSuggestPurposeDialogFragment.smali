.class public final Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/IQR;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/util/concurrent/ExecutorService;

.field public q:LX/0tX;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2580684
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2580685
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 2580686
    const v0, 0x7f081c19

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2580687
    const v0, 0x7f081c1a

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2580688
    new-instance v3, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2580689
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0310af

    .line 2580690
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2580691
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2580692
    invoke-virtual {v3, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 2580693
    new-instance v0, LX/IUX;

    invoke-direct {v0, p0}, LX/IUX;-><init>(Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;)V

    invoke-virtual {v3, v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2580694
    new-instance v0, LX/IUY;

    invoke-direct {v0, p0}, LX/IUY;-><init>(Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;)V

    invoke-virtual {v3, v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2580695
    new-instance v0, LX/4KP;

    invoke-direct {v0}, LX/4KP;-><init>()V

    .line 2580696
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->n:Ljava/lang/String;

    .line 2580697
    const-string v2, "group_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580698
    move-object v1, v0

    .line 2580699
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->o:Ljava/lang/String;

    .line 2580700
    const-string v4, "actor_id"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580701
    move-object v1, v1

    .line 2580702
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2580703
    const-string v4, "seen_group_purposes_modal"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2580704
    new-instance v1, LX/B23;

    invoke-direct {v1}, LX/B23;-><init>()V

    move-object v1, v1

    .line 2580705
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B23;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2580706
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->q:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2580707
    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1cfb43b9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2580708
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2580709
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->o:Ljava/lang/String;

    iput-object v4, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->p:Ljava/util/concurrent/ExecutorService;

    iput-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsSuggestPurposeDialogFragment;->q:LX/0tX;

    .line 2580710
    const/16 v1, 0x2b

    const v2, 0x79b1ae13

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
