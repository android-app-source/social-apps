.class public Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;
.super Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public g:LX/DOp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/IVs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/permalink/PermalinkGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/9Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575138
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2575156
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->g:LX/DOp;

    iget-object v1, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->h:LX/IVs;

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->i:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->f:LX/1Qq;

    .line 2575157
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->f:LX/1Qq;

    return-object v0
.end method

.method public final a(LX/DNq;LX/0fz;LX/DNe;)LX/DNp;
    .locals 3

    .prologue
    .line 2575151
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->b()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    .line 2575152
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->j:LX/9Lz;

    .line 2575153
    new-instance p0, LX/9Ly;

    invoke-static {v1}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v2

    check-cast v2, LX/0pn;

    invoke-direct {p0, v0, v2}, LX/9Ly;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    .line 2575154
    move-object v1, p0

    .line 2575155
    invoke-virtual {p1, v0, v1, p2, p3}, LX/DNq;->a(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;)LX/DNp;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2575150
    const-string v0, "cross_group_for_sale_posts"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2575158
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2575159
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;

    invoke-static {v4}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v2

    check-cast v2, LX/DOp;

    invoke-static {v4}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v3

    check-cast v3, LX/IVs;

    const/16 p1, 0x1cd2

    invoke-static {v4, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const-class v0, LX/9Lz;

    invoke-interface {v4, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9Lz;

    iput-object v2, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->g:LX/DOp;

    iput-object v3, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->h:LX/IVs;

    iput-object p1, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->i:LX/0Ot;

    iput-object v4, p0, Lcom/facebook/groups/feed/ui/CrossGroupForSalePostsFragment;->j:LX/9Lz;

    .line 2575160
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2575143
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2575144
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2575145
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2575146
    move-object v1, v0

    .line 2575147
    sget-object v2, LX/B1T;->CrossGroupForSalePosts:LX/B1T;

    .line 2575148
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2575149
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->l:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2575140
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2575141
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/BaseForSalePostsFeedFragment;->d:LX/DNJ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/DNJ;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2575142
    return-void
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2575139
    const v0, 0x7f081b79

    return v0
.end method
