.class public Lcom/facebook/groups/widget/actionbar/GroupsActionBar;
.super LX/AhO;
.source ""


# instance fields
.field private final a:I

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/AhN;

.field public d:LX/IT4;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2585379
    invoke-direct {p0, p1}, LX/AhO;-><init>(Landroid/content/Context;)V

    .line 2585380
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a:I

    .line 2585381
    new-instance v0, LX/IWy;

    invoke-direct {v0, p0}, LX/IWy;-><init>(Lcom/facebook/groups/widget/actionbar/GroupsActionBar;)V

    iput-object v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->c:LX/AhN;

    .line 2585382
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a()V

    .line 2585383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2585374
    invoke-direct {p0, p1, p2}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2585375
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a:I

    .line 2585376
    new-instance v0, LX/IWy;

    invoke-direct {v0, p0}, LX/IWy;-><init>(Lcom/facebook/groups/widget/actionbar/GroupsActionBar;)V

    iput-object v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->c:LX/AhN;

    .line 2585377
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a()V

    .line 2585378
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2585384
    invoke-direct {p0, p1, p2, p3}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2585385
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a:I

    .line 2585386
    new-instance v0, LX/IWy;

    invoke-direct {v0, p0}, LX/IWy;-><init>(Lcom/facebook/groups/widget/actionbar/GroupsActionBar;)V

    iput-object v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->c:LX/AhN;

    .line 2585387
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->a()V

    .line 2585388
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2585368
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 2585369
    iget-object v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->c:LX/AhN;

    .line 2585370
    iput-object v0, p0, LX/AhO;->b:LX/AhN;

    .line 2585371
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->b:Ljava/util/Map;

    .line 2585372
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1}, LX/AhO;->a(ZZI)V

    .line 2585373
    return-void
.end method


# virtual methods
.method public setItems(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IX1;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2585357
    invoke-virtual {p0}, LX/AhO;->b()V

    .line 2585358
    invoke-virtual {p0}, LX/AhO;->clear()V

    .line 2585359
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IX1;

    .line 2585360
    iget v4, v0, LX/IX1;->iconId:I

    .line 2585361
    iget-object v5, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->b:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0}, LX/IX1;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585362
    iget v5, v0, LX/IX1;->labelResId:I

    invoke-virtual {p0, v2, v4, v2, v5}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v4

    iget v5, v0, LX/IX1;->showAsAction:I

    invoke-interface {v4, v5}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v4

    iget v5, v0, LX/IX1;->iconMediumResId:I

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v5, v0, LX/IX1;->enabled:Z

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v5, v0, LX/IX1;->checkable:Z

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v0, v0, LX/IX1;->checked:Z

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2585363
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2585364
    :cond_0
    invoke-virtual {p0}, LX/AhO;->d()V

    .line 2585365
    return-void
.end method

.method public setListener(LX/IT4;)V
    .locals 0

    .prologue
    .line 2585366
    iput-object p1, p0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->d:LX/IT4;

    .line 2585367
    return-void
.end method
