.class public final Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2571759
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2571760
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2571811
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2571812
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2571772
    if-nez p1, :cond_0

    move v0, v1

    .line 2571773
    :goto_0
    return v0

    .line 2571774
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2571775
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2571776
    :sswitch_0
    const-class v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 2571777
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2571778
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2571779
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2571780
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2571781
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2571782
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2571783
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2571784
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2571785
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2571786
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2571787
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2571788
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2571789
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2571790
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2571791
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2571792
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2571793
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2571794
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2571795
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2571796
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2571797
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2571798
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2571799
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2571800
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v3

    .line 2571801
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v4

    .line 2571802
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2571803
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2571804
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2571805
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2571806
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2571807
    invoke-virtual {p3, v8, v3}, LX/186;->a(IZ)V

    .line 2571808
    invoke-virtual {p3, v9, v4}, LX/186;->a(IZ)V

    .line 2571809
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 2571810
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7a4f5207 -> :sswitch_3
        -0x5a447a5d -> :sswitch_1
        0xeb0d906 -> :sswitch_4
        0x1382909d -> :sswitch_2
        0x675e5316 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2571771
    new-instance v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2571766
    if-eqz p0, :cond_0

    .line 2571767
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2571768
    if-eq v0, p0, :cond_0

    .line 2571769
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2571770
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2571761
    sparse-switch p2, :sswitch_data_0

    .line 2571762
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2571763
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 2571764
    invoke-static {v0, p3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2571765
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7a4f5207 -> :sswitch_1
        -0x5a447a5d -> :sswitch_1
        0xeb0d906 -> :sswitch_1
        0x1382909d -> :sswitch_1
        0x675e5316 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2571756
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2571757
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2571758
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2571813
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2571814
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2571815
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a:LX/15i;

    .line 2571816
    iput p2, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->b:I

    .line 2571817
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2571730
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2571731
    new-instance v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2571732
    iget v0, p0, LX/1vt;->c:I

    .line 2571733
    move v0, v0

    .line 2571734
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2571735
    iget v0, p0, LX/1vt;->c:I

    .line 2571736
    move v0, v0

    .line 2571737
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2571738
    iget v0, p0, LX/1vt;->b:I

    .line 2571739
    move v0, v0

    .line 2571740
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2571741
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2571742
    move-object v0, v0

    .line 2571743
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2571744
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2571745
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2571746
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2571747
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2571748
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2571749
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2571750
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2571751
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2571752
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2571753
    iget v0, p0, LX/1vt;->c:I

    .line 2571754
    move v0, v0

    .line 2571755
    return v0
.end method
