.class public final Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55e1ed53
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2572362
    const-class v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2572363
    const-class v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2572364
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2572365
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2572366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2572367
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2572368
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2572369
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2572370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2572371
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2572372
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2572373
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2572374
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    .line 2572375
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2572376
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    .line 2572377
    iput-object v0, v1, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    .line 2572378
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2572379
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupsYouShouldJoin"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2572380
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    iput-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    .line 2572381
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2572382
    new-instance v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    invoke-direct {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;-><init>()V

    .line 2572383
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2572384
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2572385
    const v0, -0x45d2613a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2572386
    const v0, -0x6747e1ce

    return v0
.end method
