.class public final Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7dd006ba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2572214
    const-class v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2572259
    const-class v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2572257
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2572258
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2572254
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2572255
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2572256
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2572244
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2572245
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2572246
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x7a4f5207

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2572247
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2572248
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2572249
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2572250
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2572251
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2572252
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2572253
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2572229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2572230
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2572231
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    .line 2572232
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2572233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2572234
    iput-object v0, v1, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    .line 2572235
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2572236
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7a4f5207

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2572237
    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2572238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2572239
    iput v3, v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->f:I

    move-object v1, v0

    .line 2572240
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2572241
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2572242
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2572243
    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2572227
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    .line 2572228
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->e:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2572224
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2572225
    const/4 v0, 0x1

    const v1, -0x7a4f5207

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->f:I

    .line 2572226
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2572221
    new-instance v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;-><init>()V

    .line 2572222
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2572223
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2572220
    const v0, 0x528e219a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2572219
    const v0, 0x2ded4f45

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestionContext"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2572217
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2572218
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2572215
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->g:Ljava/lang/String;

    .line 2572216
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->g:Ljava/lang/String;

    return-object v0
.end method
