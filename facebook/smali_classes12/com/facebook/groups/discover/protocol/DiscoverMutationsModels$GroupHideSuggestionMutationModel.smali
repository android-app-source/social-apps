.class public final Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2571711
    const-class v0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2571710
    const-class v0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2571708
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2571709
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2571706
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;->e:Ljava/lang/String;

    .line 2571707
    iget-object v0, p0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2571700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2571701
    invoke-direct {p0}, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2571702
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2571703
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2571704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2571705
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2571697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2571698
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2571699
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2571694
    new-instance v0, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/discover/protocol/DiscoverMutationsModels$GroupHideSuggestionMutationModel;-><init>()V

    .line 2571695
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2571696
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2571693
    const v0, -0x546b5e5a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2571692
    const v0, -0x201d25d8

    return v0
.end method
