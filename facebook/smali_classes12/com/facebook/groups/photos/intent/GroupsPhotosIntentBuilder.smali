.class public Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2583789
    const-class v0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    const-string v1, "group_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2583786
    iput-object p1, p0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b:LX/0Or;

    .line 2583787
    iput-object p2, p0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->c:Landroid/content/res/Resources;

    .line 2583788
    return-void
.end method

.method private static a(Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2583758
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2583759
    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;
    .locals 3

    .prologue
    .line 2583783
    new-instance v1, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    const/16 v0, 0xc

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v2, v0}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;-><init>(LX/0Or;Landroid/content/res/Resources;)V

    .line 2583784
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2583766
    invoke-static {p0}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->a(Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;)Landroid/content/Intent;

    move-result-object v0

    .line 2583767
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_ALBUM_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2583768
    const-string v1, "ALBUM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2583769
    new-instance v1, LX/4Vp;

    invoke-direct {v1}, LX/4Vp;-><init>()V

    .line 2583770
    iput-object p1, v1, LX/4Vp;->m:Ljava/lang/String;

    .line 2583771
    move-object v1, v1

    .line 2583772
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2583773
    const-string v2, "extra_album_selected"

    invoke-static {v0, v2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2583774
    const-string v1, "extra_caller_context"

    sget-object v2, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2583775
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2583776
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->c:Landroid/content/res/Resources;

    const v2, 0x7f083894

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 2583777
    :cond_0
    new-instance v1, LX/89I;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2583778
    iput-object p3, v1, LX/89I;->c:Ljava/lang/String;

    .line 2583779
    move-object v1, v1

    .line 2583780
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 2583781
    const-string v2, "extra_composer_target_data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2583782
    :cond_1
    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2583760
    invoke-static {p0}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->a(Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;)Landroid/content/Intent;

    move-result-object v0

    .line 2583761
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_ALBUM_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2583762
    const-string v1, "extra_album_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2583763
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2583764
    const-string v1, "group_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2583765
    return-object v0
.end method
