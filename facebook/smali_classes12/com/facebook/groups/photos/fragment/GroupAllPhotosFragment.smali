.class public Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/IWT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/IWJ;

.field public q:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2583663
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    .line 2583664
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 4

    .prologue
    .line 2583665
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030860

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2583666
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->q:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f083898

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2583667
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->q:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2583668
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2583669
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;

    invoke-static {p1}, LX/IWT;->a(LX/0QB;)LX/IWT;

    move-result-object v2

    check-cast v2, LX/IWT;

    const/16 v0, 0x2ead

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->n:LX/IWT;

    iput-object p1, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->o:LX/0Ot;

    .line 2583670
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2583671
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2583672
    iget-object v3, v2, LX/Dvb;->i:LX/Dvc;

    move-object v2, v3

    .line 2583673
    invoke-virtual {v2}, LX/Dvc;->d()LX/0Px;

    move-result-object v4

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->n:LX/IWT;

    .line 2583674
    iget-object v3, v2, LX/IWT;->a:Ljava/lang/String;

    move-object v5, v3

    .line 2583675
    move-object v2, p1

    move-object v3, p2

    const/4 p1, 0x0

    .line 2583676
    new-instance p0, LX/9hF;

    invoke-direct {p0}, LX/9hF;-><init>()V

    invoke-static {v5}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object p0

    sget-object p2, LX/74S;->GROUPS_INFO_PAGE_PHOTO_ITEM:LX/74S;

    invoke-virtual {p0, p2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object p0

    const/4 p2, 0x0

    .line 2583677
    iput-boolean p2, p0, LX/9hD;->m:Z

    .line 2583678
    move-object p0, p0

    .line 2583679
    invoke-virtual {p0, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object p2

    if-eqz v3, :cond_0

    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object p0

    :goto_0
    invoke-virtual {p2, p0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object p0

    invoke-virtual {p0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object p2

    .line 2583680
    iget-object p0, v0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/23R;

    invoke-interface {p0, v1, p2, p1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2583681
    return-void

    :cond_0
    move-object p0, p1

    .line 2583682
    goto :goto_0
.end method

.method public final c()LX/Dcc;
    .locals 1

    .prologue
    .line 2583683
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->n:LX/IWT;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x54531ca1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583684
    new-instance v1, LX/IWJ;

    invoke-direct {v1, p0}, LX/IWJ;-><init>(Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->p:LX/IWJ;

    .line 2583685
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->n:LX/IWT;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->p:LX/IWJ;

    .line 2583686
    iput-object v2, v1, LX/IWT;->e:LX/IWJ;

    .line 2583687
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7ee639eb

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x729d74d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583688
    invoke-super {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onDestroyView()V

    .line 2583689
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->n:LX/IWT;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->p:LX/IWJ;

    .line 2583690
    iget-object v5, v1, LX/IWT;->e:LX/IWJ;

    if-eqz v5, :cond_0

    iget-object v5, v1, LX/IWT;->e:LX/IWJ;

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2583691
    const/4 v5, 0x0

    iput-object v5, v1, LX/IWT;->e:LX/IWJ;

    .line 2583692
    :cond_0
    iput-object v4, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->p:LX/IWJ;

    .line 2583693
    iput-object v4, p0, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 2583694
    const/16 v1, 0x2b

    const v2, 0x3d14793f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
