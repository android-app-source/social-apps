.class public Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DZ8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2583583
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2583560
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2583561
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b(LX/0QB;)Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object p1

    check-cast p1, LX/DZ8;

    invoke-static {v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->a:Landroid/content/res/Resources;

    iput-object v3, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->b:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    iput-object p1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->c:LX/DZ8;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2583562
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2583579
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2583580
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    .line 2583581
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2583582
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x19932cd8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583578
    const v1, 0x7f0307f4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2a93e969

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2583563
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2583564
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->c:LX/DZ8;

    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f083894

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, LX/DZ8;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2583565
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2583566
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2583567
    :cond_0
    new-instance v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;-><init>()V

    .line 2583568
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2583569
    const-string v2, "extra_album_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2583570
    const-string v3, "group_feed_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2583571
    const-string p1, "group_name"

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2583572
    iget-object p1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumPandoraFragment;->b:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    invoke-virtual {p1, v2, v3, v1}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    move-object v1, v1

    .line 2583573
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2583574
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2583575
    const v2, 0x7f0d002f

    const-string v3, "AlbumMediaSetFragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2583576
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2583577
    return-void
.end method
