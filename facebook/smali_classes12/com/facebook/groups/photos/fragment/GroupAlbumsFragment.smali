.class public Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/IWR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IWD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:LX/IWQ;

.field public i:LX/IWC;

.field public j:Z

.field public k:Lcom/facebook/widget/text/BetterTextView;

.field private l:Lcom/facebook/widget/listview/BetterListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2583610
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2583611
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2583657
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->d:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2583658
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2583659
    return-void
.end method

.method public static b(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V
    .locals 1

    .prologue
    .line 2583660
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    invoke-virtual {v0}, LX/IWQ;->a()V

    .line 2583661
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2583633
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2583634
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    const-class v3, LX/IWR;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/IWR;

    const-class v4, LX/IWD;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/IWD;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;->b(LX/0QB;)Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    move-result-object v6

    check-cast v6, Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    const/16 v0, 0x2dfd

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->a:LX/IWR;

    iput-object v4, v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->b:LX/IWD;

    iput-object v5, v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->d:Lcom/facebook/groups/photos/intent/GroupsPhotosIntentBuilder;

    iput-object p1, v2, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->e:LX/0Ot;

    .line 2583635
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2583636
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->f:Ljava/lang/String;

    .line 2583637
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2583638
    const-string v1, "group_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->g:Ljava/lang/String;

    .line 2583639
    new-instance v0, LX/IWF;

    invoke-direct {v0, p0}, LX/IWF;-><init>(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    .line 2583640
    new-instance v1, LX/IWC;

    invoke-direct {v1, v0}, LX/IWC;-><init>(LX/IWF;)V

    .line 2583641
    move-object v0, v1

    .line 2583642
    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->i:LX/IWC;

    .line 2583643
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->a:LX/IWR;

    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->f:Ljava/lang/String;

    new-instance v2, LX/IWG;

    invoke-direct {v2, p0}, LX/IWG;-><init>(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    .line 2583644
    new-instance v3, LX/IWQ;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    move-object v8, v1

    move-object v9, v2

    invoke-direct/range {v3 .. v9}, LX/IWQ;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;LX/IWG;)V

    .line 2583645
    move-object v0, v3

    .line 2583646
    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    .line 2583647
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2583648
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2583649
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2583650
    :cond_0
    :goto_0
    return-void

    .line 2583651
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2583652
    :pswitch_0
    const-string v0, "extra_album"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2583653
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->a$redex0(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;Ljava/lang/String;)V

    .line 2583654
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    if-eqz v0, :cond_0

    .line 2583655
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    invoke-virtual {v0}, LX/IWQ;->b()V

    .line 2583656
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    invoke-virtual {v0}, LX/IWQ;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7c7
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5a91860e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583632
    const v1, 0x7f0307f6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7fc00030

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1457ec58

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583627
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2583628
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->h:LX/IWQ;

    .line 2583629
    invoke-virtual {v1}, LX/IWQ;->b()V

    .line 2583630
    iget-object v2, v1, LX/IWQ;->e:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2583631
    const/16 v1, 0x2b

    const v2, -0x4022dca6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2583614
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2583615
    const v0, 0x7f0d14f1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2583616
    const v1, 0x7f030860

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2583617
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2583618
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->k:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f083897

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2583619
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->k:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2583620
    const v0, 0x7f0d14f2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->l:Lcom/facebook/widget/listview/BetterListView;

    .line 2583621
    const v0, 0x7f0d14f2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2583622
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->i:LX/IWC;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2583623
    new-instance v1, LX/IWH;

    invoke-direct {v1, p0}, LX/IWH;-><init>(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2583624
    new-instance v1, LX/IWI;

    invoke-direct {v1, p0}, LX/IWI;-><init>(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2583625
    invoke-static {p0}, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->b(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    .line 2583626
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 2583612
    iput-boolean p1, p0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->j:Z

    .line 2583613
    return-void
.end method
