.class public Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;
.super Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DZ8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/support/v4/view/ViewPager;

.field private g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field private j:LX/IWE;

.field private k:I

.field public l:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

.field public m:LX/DLO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2583726
    invoke-direct {p0}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2583727
    const-string v0, "group_photos"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2583728
    invoke-super {p0, p1}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->a(Landroid/os/Bundle;)V

    .line 2583729
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v4

    check-cast v4, LX/DZ8;

    const/16 v5, 0xbde

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    iput-object v3, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->b:LX/DZ8;

    iput-object v5, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->c:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->d:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->e:LX/0tX;

    .line 2583730
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2583731
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->h:Ljava/lang/String;

    .line 2583732
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2583733
    const-string v1, "group_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    .line 2583734
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2583735
    const-string v1, "group_mall_type"

    sget-object v2, LX/5QT;->WITHOUT_TABS_LEGACY:LX/5QT;

    invoke-virtual {v2}, LX/5QT;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->k:I

    .line 2583736
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0xd2b59d7

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583737
    invoke-super {p0, p1}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2583738
    new-instance v1, LX/IWE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->a:Landroid/content/res/Resources;

    invoke-direct {v1, v2, v3, v4, v5}, LX/IWE;-><init>(LX/0gc;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->j:LX/IWE;

    .line 2583739
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->j:LX/IWE;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2583740
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2583741
    const/16 v1, 0x2b

    const v2, 0x6c6b0598

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x19c572da

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583742
    const v1, 0x7f030824

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x10e5893c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x46658bf8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2583743
    invoke-super {p0}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onPause()V

    .line 2583744
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2583745
    const/16 v1, 0x2b

    const v2, -0x1b2a0c3b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 2583746
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2583747
    const v0, 0x7f0d1551

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    .line 2583748
    const v0, 0x7f0d1550

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2583749
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2583750
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083893

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2583751
    :goto_0
    new-instance v1, LX/IWK;

    invoke-direct {v1, p0}, LX/IWK;-><init>(Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->m:LX/DLO;

    .line 2583752
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2583753
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2583754
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->b:LX/DZ8;

    iget-object v2, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->m:LX/DLO;

    invoke-virtual {v1, p0, v0, v2}, LX/DZ8;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2583755
    iget-object v0, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->d:LX/1Ck;

    const-string v1, "fetch_photos_header"

    new-instance v2, LX/IWL;

    invoke-direct {v2, p0}, LX/IWL;-><init>(Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;)V

    new-instance v3, LX/IWM;

    invoke-direct {v3, p0}, LX/IWM;-><init>(Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2583756
    return-void

    .line 2583757
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083891

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
