.class public Lcom/facebook/groups/photos/view/GroupAlbumRow;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/graphics/Paint;

.field public c:F

.field public d:F

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2585248
    const-class v0, Lcom/facebook/groups/photos/view/GroupAlbumRow;

    const-string v1, "group_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2585249
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2585250
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->b:Landroid/graphics/Paint;

    .line 2585251
    const/4 v3, 0x0

    .line 2585252
    const-class v0, Lcom/facebook/groups/photos/view/GroupAlbumRow;

    invoke-static {v0, p0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2585253
    invoke-virtual {p0, v3}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->setOrientation(I)V

    .line 2585254
    const v0, 0x7f0307f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2585255
    invoke-virtual {p0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 2585256
    const v0, 0x7f0d14ee

    invoke-virtual {p0, v0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayout;

    .line 2585257
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    move v2, v3

    .line 2585258
    :goto_0
    const/4 v1, 0x4

    if-ge v2, v1, :cond_0

    .line 2585259
    const v1, 0x7f0307f3

    invoke-virtual {v4, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2585260
    invoke-virtual {p1, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2585261
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 2585262
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2585263
    :cond_0
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->f:LX/0Px;

    .line 2585264
    const v0, 0x7f0d14ef

    invoke-virtual {p0, v0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2585265
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a05ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2585266
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2585267
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->c:F

    .line 2585268
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->d:F

    .line 2585269
    invoke-virtual {p0, v3}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->setWillNotDraw(Z)V

    .line 2585270
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/photos/view/GroupAlbumRow;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bind"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x4

    const/4 v2, 0x0

    .line 2585271
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->f:LX/0Px;

    if-eqz v0, :cond_2

    .line 2585272
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2585273
    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2585274
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2585275
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2585276
    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->c()Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->a()LX/0Px;

    move-result-object v3

    .line 2585277
    if-eqz v3, :cond_1

    .line 2585278
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v0, v6, :cond_3

    .line 2585279
    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2585280
    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2585281
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2585282
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2585283
    iget-object v1, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/groups/photos/view/GroupAlbumRow;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2585284
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2585285
    :cond_1
    const v0, 0x7f0d0510

    invoke-virtual {p0, v0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2585286
    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->d()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2585287
    const v0, 0x7f0d14f0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2585288
    iget-object v1, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a:Landroid/content/res/Resources;

    const v3, 0x7f0f017d

    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->c()Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->b()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;->c()Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2585289
    :cond_2
    return-void

    .line 2585290
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v1, v2

    .line 2585291
    :goto_1
    if-ge v1, v6, :cond_1

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2585292
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$MediaNodesModel;

    .line 2585293
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$MediaNodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2585294
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2585295
    if-eqz v0, :cond_4

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 2585296
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2585297
    iget-object v0, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2585298
    sget-object v5, Lcom/facebook/groups/photos/view/GroupAlbumRow;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2585299
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2585300
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2585301
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2585302
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2585303
    iget v1, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->c:F

    .line 2585304
    invoke-virtual {p0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->c:F

    sub-float v3, v0, v2

    .line 2585305
    invoke-virtual {p0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->d:F

    sub-float v2, v0, v2

    .line 2585306
    invoke-virtual {p0}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->getHeight()I

    move-result v0

    int-to-float v4, v0

    .line 2585307
    iget-object v5, p0, Lcom/facebook/groups/photos/view/GroupAlbumRow;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2585308
    return-void
.end method
