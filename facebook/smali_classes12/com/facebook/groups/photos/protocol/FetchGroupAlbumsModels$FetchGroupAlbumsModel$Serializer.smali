.class public final Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2584422
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    new-instance v1, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2584423
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2584424
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2584425
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2584426
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2584427
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2584428
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2584429
    if-eqz v2, :cond_0

    .line 2584430
    const-string v3, "group_albums"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584431
    invoke-static {v1, v2, p1, p2}, LX/IWh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2584432
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2584433
    if-eqz v2, :cond_1

    .line 2584434
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584435
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584436
    :cond_1
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2584437
    if-eqz v2, :cond_2

    .line 2584438
    const-string v2, "viewer_post_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584439
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584440
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2584441
    if-eqz v2, :cond_3

    .line 2584442
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584443
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584444
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2584445
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2584446
    check-cast p1, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$Serializer;->a(Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel;LX/0nX;LX/0my;)V

    return-void
.end method
