.class public final Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2584882
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    new-instance v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2584883
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2584852
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2584853
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2584854
    const/4 v2, 0x0

    .line 2584855
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 2584856
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2584857
    :goto_0
    move v1, v2

    .line 2584858
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2584859
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2584860
    new-instance v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    invoke-direct {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;-><init>()V

    .line 2584861
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2584862
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2584863
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2584864
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2584865
    :cond_0
    return-object v1

    .line 2584866
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2584867
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_5

    .line 2584868
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2584869
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2584870
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 2584871
    const-string p0, "id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2584872
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2584873
    :cond_3
    const-string p0, "name"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2584874
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2584875
    :cond_4
    const-string p0, "viewer_post_status"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2584876
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_1

    .line 2584877
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2584878
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2584879
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2584880
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2584881
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1
.end method
