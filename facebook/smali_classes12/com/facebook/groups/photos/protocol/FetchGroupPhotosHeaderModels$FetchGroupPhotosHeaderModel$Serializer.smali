.class public final Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2584886
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    new-instance v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2584887
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2584888
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2584889
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2584890
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x2

    .line 2584891
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2584892
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2584893
    if-eqz v2, :cond_0

    .line 2584894
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584895
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584896
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2584897
    if-eqz v2, :cond_1

    .line 2584898
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584899
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584900
    :cond_1
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2584901
    if-eqz v2, :cond_2

    .line 2584902
    const-string v2, "viewer_post_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584903
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584904
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2584905
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2584906
    check-cast p1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;->a(Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;LX/0nX;LX/0my;)V

    return-void
.end method
