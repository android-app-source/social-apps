.class public final Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xbd75371
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2584948
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2584947
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2584945
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2584946
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2584939
    iput-object p1, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->f:Ljava/lang/String;

    .line 2584940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2584941
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2584942
    if-eqz v0, :cond_0

    .line 2584943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2584944
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2584937
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->e:Ljava/lang/String;

    .line 2584938
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2584935
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->f:Ljava/lang/String;

    .line 2584936
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2584925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2584926
    invoke-direct {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2584927
    invoke-direct {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2584928
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2584929
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2584930
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2584931
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2584932
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2584933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2584934
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2584949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2584950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2584951
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2584924
    new-instance v0, LX/IWn;

    invoke-direct {v0, p1}, LX/IWn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2584923
    invoke-direct {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2584917
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2584918
    invoke-direct {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2584919
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2584920
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2584921
    :goto_0
    return-void

    .line 2584922
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2584914
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2584915
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->a(Ljava/lang/String;)V

    .line 2584916
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2584911
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    invoke-direct {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;-><init>()V

    .line 2584912
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2584913
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2584910
    const v0, 0x2fa0037a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2584907
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2584908
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 2584909
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method
