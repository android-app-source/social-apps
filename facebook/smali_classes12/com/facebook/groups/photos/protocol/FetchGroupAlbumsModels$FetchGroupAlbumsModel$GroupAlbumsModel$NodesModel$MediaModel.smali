.class public final Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x618007d6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$MediaNodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2584249
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2584248
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2584246
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2584247
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2584219
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2584220
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2584221
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2584222
    iget v1, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 2584223
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2584224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2584225
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$MediaNodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2584244
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel$MediaNodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->f:Ljava/util/List;

    .line 2584245
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2584236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2584237
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2584238
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2584239
    if-eqz v1, :cond_0

    .line 2584240
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;

    .line 2584241
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->f:Ljava/util/List;

    .line 2584242
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2584243
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2584233
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2584234
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->e:I

    .line 2584235
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2584231
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2584232
    iget v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;->e:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2584228
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel$MediaModel;-><init>()V

    .line 2584229
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2584230
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2584227
    const v0, -0x68d96081

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2584226
    const v0, 0x2c8ffc04

    return v0
.end method
