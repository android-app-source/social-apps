.class public final Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ac759b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2585096
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2585095
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2585093
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2585094
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585090
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2585091
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2585092
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2585080
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2585081
    invoke-direct {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2585082
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2585083
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2585084
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2585085
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2585086
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2585087
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2585088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2585089
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2585062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2585063
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2585064
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    .line 2585065
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2585066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    .line 2585067
    iput-object v0, v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->g:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    .line 2585068
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2585069
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585079
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2585076
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    invoke-direct {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;-><init>()V

    .line 2585077
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2585078
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2585075
    const v0, -0xb1c1957

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2585074
    const v0, -0x31d68202

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585072
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->f:Ljava/lang/String;

    .line 2585073
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585070
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->g:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->g:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    .line 2585071
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->g:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    return-object v0
.end method
