.class public final Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5f16b66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2585182
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2585181
    const-class v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2585179
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2585180
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 2585172
    iput-object p1, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2585173
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2585174
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2585175
    if-eqz v0, :cond_0

    .line 2585176
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2585177
    :cond_0
    return-void

    .line 2585178
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2585165
    iput-object p1, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2585166
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2585167
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2585168
    if-eqz v0, :cond_0

    .line 2585169
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2585170
    :cond_0
    return-void

    .line 2585171
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2585155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2585156
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2585157
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2585158
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2585159
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2585160
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2585161
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2585162
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2585163
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2585164
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2585147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2585148
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2585149
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    .line 2585150
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2585151
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    .line 2585152
    iput-object v0, v1, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->e:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    .line 2585153
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2585154
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2585146
    new-instance v0, LX/IWq;

    invoke-direct {v0, p1}, LX/IWq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585144
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->e:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->e:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    .line 2585145
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->e:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2585120
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2585121
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2585122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2585123
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2585124
    :goto_0
    return-void

    .line 2585125
    :cond_0
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2585126
    invoke-virtual {p0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2585127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2585128
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2585129
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2585139
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2585140
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 2585141
    :cond_0
    :goto_0
    return-void

    .line 2585142
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2585143
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2585136
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    invoke-direct {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;-><init>()V

    .line 2585137
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2585138
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2585135
    const v0, -0x6034ce57    # -8.6056E-20f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2585134
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585132
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2585133
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2585130
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2585131
    iget-object v0, p0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
