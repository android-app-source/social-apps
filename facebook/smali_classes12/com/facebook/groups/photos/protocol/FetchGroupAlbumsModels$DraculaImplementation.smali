.class public final Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2583996
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2583997
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2584075
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2584076
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2584044
    if-nez p1, :cond_0

    .line 2584045
    :goto_0
    return v0

    .line 2584046
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2584047
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2584048
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2584049
    const v2, 0x71901864

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2584050
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2584051
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2584052
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2584053
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584054
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2584055
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2584056
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2584057
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2584058
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584059
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2584060
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2584061
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2584062
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2584063
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584064
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2584065
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2584066
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2584067
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2584068
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584069
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2584070
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2584071
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2584072
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2584073
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2584074
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6c3b0c89 -> :sswitch_3
        -0x6ab5ba42 -> :sswitch_0
        -0x6ff176a -> :sswitch_2
        0x5ce2b852 -> :sswitch_4
        0x71901864 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2584043
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2584038
    sparse-switch p2, :sswitch_data_0

    .line 2584039
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2584040
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2584041
    const v1, 0x71901864

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2584042
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6c3b0c89 -> :sswitch_1
        -0x6ab5ba42 -> :sswitch_0
        -0x6ff176a -> :sswitch_1
        0x5ce2b852 -> :sswitch_1
        0x71901864 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2584032
    if-eqz p1, :cond_0

    .line 2584033
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;

    move-result-object v1

    .line 2584034
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;

    .line 2584035
    if-eq v0, v1, :cond_0

    .line 2584036
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2584037
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2584031
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2584029
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2584030
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2584024
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2584025
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2584026
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a:LX/15i;

    .line 2584027
    iput p2, p0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->b:I

    .line 2584028
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2584023
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2584022
    new-instance v0, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2584019
    iget v0, p0, LX/1vt;->c:I

    .line 2584020
    move v0, v0

    .line 2584021
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2584016
    iget v0, p0, LX/1vt;->c:I

    .line 2584017
    move v0, v0

    .line 2584018
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2584013
    iget v0, p0, LX/1vt;->b:I

    .line 2584014
    move v0, v0

    .line 2584015
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2584010
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2584011
    move-object v0, v0

    .line 2584012
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2584001
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2584002
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2584003
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2584004
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2584005
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2584006
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2584007
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2584008
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2584009
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2583998
    iget v0, p0, LX/1vt;->c:I

    .line 2583999
    move v0, v0

    .line 2584000
    return v0
.end method
