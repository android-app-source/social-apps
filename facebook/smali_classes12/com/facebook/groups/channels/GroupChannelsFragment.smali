.class public Lcom/facebook/groups/channels/GroupChannelsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field public a:LX/IK2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IK6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IK9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/widget/FbSwipeRefreshLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2565036
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2565037
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2565038
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/channels/GroupChannelsFragment;

    invoke-static {v0}, LX/IK2;->a(LX/0QB;)LX/IK2;

    move-result-object v2

    check-cast v2, LX/IK2;

    invoke-static {v0}, LX/IK6;->a(LX/0QB;)LX/IK6;

    move-result-object p1

    check-cast p1, LX/IK6;

    invoke-static {v0}, LX/IK9;->a(LX/0QB;)LX/IK9;

    move-result-object v0

    check-cast v0, LX/IK9;

    iput-object v2, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->a:LX/IK2;

    iput-object p1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->b:LX/IK6;

    iput-object v0, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->c:LX/IK9;

    .line 2565039
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v0, 0x2a

    const v1, 0x9152da4

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2565040
    const v0, 0x7f0307fb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2565041
    const v1, 0x7f0d1501

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2565042
    new-instance v3, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2565043
    iget-object v3, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->a:LX/IK2;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2565044
    new-instance v3, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00fc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4, v6}, LX/62X;-><init>(II)V

    .line 2565045
    iput-boolean v6, v3, LX/62X;->e:Z

    .line 2565046
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2565047
    new-instance v3, LX/0g7;

    invoke-direct {v3, v1}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 2565048
    new-instance v1, LX/IK7;

    invoke-direct {v1, p0}, LX/IK7;-><init>(Lcom/facebook/groups/channels/GroupChannelsFragment;)V

    invoke-virtual {v3, v1}, LX/0g7;->b(LX/0fx;)V

    .line 2565049
    const v1, 0x7f0d1500

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2565050
    iget-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v3, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->b:LX/IK6;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2565051
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2565052
    const-string v3, "group_feed_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2565053
    const-string v3, "Cannot start group channels fragment without a groupId."

    invoke-static {v1, v3}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2565054
    iget-object v3, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->b:LX/IK6;

    invoke-virtual {v3, v1}, LX/IK6;->a(Ljava/lang/String;)V

    .line 2565055
    iget-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->c:LX/IK9;

    invoke-virtual {v1, p0}, LX/IK9;->addObserver(Ljava/util/Observer;)V

    .line 2565056
    const/16 v1, 0x2b

    const v3, 0x16a74461

    invoke-static {v7, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x13cab569

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2565057
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2565058
    iget-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->b:LX/IK6;

    .line 2565059
    iget-object v2, v1, LX/IK6;->a:LX/IK9;

    invoke-virtual {v2}, LX/IK9;->deleteObservers()V

    .line 2565060
    iget-object v2, v1, LX/IK6;->k:LX/2kW;

    invoke-virtual {v2}, LX/2kW;->c()V

    .line 2565061
    iget-object v2, v1, LX/IK6;->l:LX/2kW;

    invoke-virtual {v2}, LX/2kW;->c()V

    .line 2565062
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2565063
    const/16 v1, 0x2b

    const v2, -0x2cd00bc2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x64acdc74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2565064
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2565065
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2565066
    if-eqz v0, :cond_0

    .line 2565067
    const v2, 0x7f081b52

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2565068
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x2d6b7b4f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2565069
    iget-object v0, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->d:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v1, p0, Lcom/facebook/groups/channels/GroupChannelsFragment;->c:LX/IK9;

    .line 2565070
    iget-boolean p0, v1, LX/IK9;->e:Z

    move v1, p0

    .line 2565071
    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2565072
    return-void
.end method
