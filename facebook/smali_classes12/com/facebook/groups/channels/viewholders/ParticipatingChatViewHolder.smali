.class public Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final l:Lcom/facebook/common/callercontext/CallerContext;

.field public m:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

.field private final n:Lcom/facebook/fig/listitem/FigListItem;

.field private final o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:LX/11S;

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IK8;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/11S;LX/0Ot;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/11S;",
            "LX/0Ot",
            "<",
            "LX/IK8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2567367
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2567368
    const-class v0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 2567369
    new-instance v0, LX/IKv;

    invoke-direct {v0, p0}, LX/IKv;-><init>(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;)V

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->u:Landroid/view/View$OnClickListener;

    .line 2567370
    const v0, 0x7f0d14fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    .line 2567371
    const v0, 0x7f0d14fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    .line 2567372
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 2567373
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    .line 2567374
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaMaxLines(I)V

    .line 2567375
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2567376
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 2567377
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->p:Ljava/lang/String;

    .line 2567378
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->q:Ljava/lang/String;

    .line 2567379
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->r:Ljava/lang/String;

    .line 2567380
    iput-object p2, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->s:LX/11S;

    .line 2567381
    iput-object p3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->t:LX/0Ot;

    .line 2567382
    return-void
.end method

.method private static a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2567383
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2567384
    return-void
.end method

.method private static a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2567385
    if-nez p2, :cond_0

    .line 2567386
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/fig/listitem/FigListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2567387
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->setPlaceholderColor(I)V

    .line 2567388
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->setGroupName(Ljava/lang/String;)V

    .line 2567389
    return-void

    .line 2567390
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2567391
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2567392
    return-void
.end method

.method private static c(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2567393
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2567394
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2567395
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->m:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    .line 2567396
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2567397
    const-string v2, ""

    .line 2567398
    const-string v4, ""

    .line 2567399
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2567400
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2567401
    if-eqz v0, :cond_5

    .line 2567402
    const v5, 0x6e8d735a

    invoke-static {v1, v0, v10, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2567403
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    move-object v1, v0

    .line 2567404
    :goto_0
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    if-ne v0, v10, :cond_1

    .line 2567405
    invoke-virtual {v1, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 2567406
    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->p:Ljava/lang/String;

    new-array v6, v10, [Ljava/lang/Object;

    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v1, v2, v9, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2567407
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v2

    .line 2567408
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2567409
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v1}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2567410
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2567411
    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    invoke-virtual {v2}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2567412
    :goto_3
    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2567413
    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v1, v4}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2567414
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->b(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567415
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567416
    invoke-static {p0, v0}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->c(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567417
    return-void

    .line 2567418
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2567419
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2567420
    :cond_1
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    if-ne v0, v8, :cond_2

    .line 2567421
    invoke-virtual {v1, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2567422
    invoke-virtual {v1, v10}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 2567423
    iget-object v7, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->q:Ljava/lang/String;

    new-array v8, v8, [Ljava/lang/Object;

    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v2, v5, v9, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v9

    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v1, v6, v9, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v10

    invoke-static {v7, v8}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2567424
    :cond_2
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    if-le v0, v8, :cond_5

    .line 2567425
    invoke-virtual {v1, v9}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2567426
    iget-object v6, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->r:Ljava/lang/String;

    new-array v7, v8, [Ljava/lang/Object;

    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v2, v5, v9, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-static {v6, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    move-object v1, v3

    .line 2567427
    goto/16 :goto_2

    .line 2567428
    :cond_4
    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iget-object v2, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_3

    :cond_5
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2567429
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->m:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    .line 2567430
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2567431
    const-string v0, ""

    .line 2567432
    const-string v3, ""

    .line 2567433
    const-string v2, ""

    .line 2567434
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->o()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel;

    move-result-object v1

    .line 2567435
    if-eqz v1, :cond_5

    .line 2567436
    invoke-virtual {v1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel;->a()LX/0Px;

    move-result-object v1

    .line 2567437
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 2567438
    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->p:Ljava/lang/String;

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2567439
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel;

    move-result-object v0

    .line 2567440
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 2567441
    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    move-result-object v2

    .line 2567442
    iget-object v0, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->s:LX/11S;

    sget-object v3, LX/1lB;->WEEK_DAY_STYLE:LX/1lB;

    invoke-virtual {v2}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;->j()J

    move-result-wide v6

    invoke-interface {v0, v3, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2567443
    invoke-virtual {v2}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2567444
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v6, v3, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2567445
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2567446
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v3}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2567447
    if-eqz v6, :cond_3

    invoke-virtual {v5, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2567448
    iget-object v3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    invoke-virtual {v5, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2567449
    :goto_3
    iget-object v3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v9}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2567450
    iget-object v3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->n:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2567451
    invoke-static {p0, v2}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->b(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567452
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->a(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567453
    invoke-static {p0, v1}, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->c(Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;Ljava/lang/String;)V

    .line 2567454
    return-void

    .line 2567455
    :cond_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-ne v5, v9, :cond_1

    .line 2567456
    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->q:Ljava/lang/String;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 2567457
    :cond_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-le v5, v9, :cond_5

    .line 2567458
    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->r:Ljava/lang/String;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 2567459
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v3, v4

    .line 2567460
    goto/16 :goto_2

    .line 2567461
    :cond_3
    iget-object v3, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->o:Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;

    iget-object v5, p0, Lcom/facebook/groups/channels/viewholders/ParticipatingChatViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_3

    :cond_4
    move-object v0, v2

    move-object v2, v3

    goto/16 :goto_1

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method
