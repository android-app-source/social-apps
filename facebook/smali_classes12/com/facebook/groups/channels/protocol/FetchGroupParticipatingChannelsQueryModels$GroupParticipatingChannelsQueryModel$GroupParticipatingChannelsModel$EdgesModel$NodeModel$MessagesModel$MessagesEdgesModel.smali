.class public final Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2345fa4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2566257
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2566256
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2566233
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2566234
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2566250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2566251
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2566252
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2566253
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2566254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2566255
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2566242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2566243
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2566244
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    .line 2566245
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2566246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;

    .line 2566247
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    .line 2566248
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2566249
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2566240
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    .line 2566241
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel$MessagesEdgesNodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2566237
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$MessagesModel$MessagesEdgesModel;-><init>()V

    .line 2566238
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2566239
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2566236
    const v0, -0x79715e6e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2566235
    const v0, 0x1dc19baa

    return v0
.end method
