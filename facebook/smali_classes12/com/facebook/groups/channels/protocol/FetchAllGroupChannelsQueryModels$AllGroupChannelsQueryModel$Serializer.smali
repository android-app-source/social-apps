.class public final Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2565428
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;

    new-instance v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2565429
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2565459
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2565431
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2565432
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2565433
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2565434
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2565435
    if-eqz v2, :cond_4

    .line 2565436
    const-string v3, "group_channels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565437
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2565438
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2565439
    if-eqz v3, :cond_2

    .line 2565440
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565441
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2565442
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 2565443
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2565444
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2565445
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2565446
    if-eqz p0, :cond_0

    .line 2565447
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565448
    invoke-static {v1, p0, p1, p2}, LX/IKI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2565449
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2565450
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2565451
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2565452
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2565453
    if-eqz v3, :cond_3

    .line 2565454
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2565455
    invoke-static {v1, v3, p1}, LX/5Mh;->a(LX/15i;ILX/0nX;)V

    .line 2565456
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2565457
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2565458
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2565430
    check-cast p1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$Serializer;->a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
