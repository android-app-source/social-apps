.class public final Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xcb1b287
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2567236
    const-class v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2567260
    const-class v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2567258
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2567259
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2567252
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2567253
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2567254
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2567255
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2567256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2567257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2567244
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2567245
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2567246
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    .line 2567247
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2567248
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    .line 2567249
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->e:Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    .line 2567250
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2567251
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2567242
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->e:Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->e:Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    .line 2567243
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;->e:Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$GeneralRoomModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2567239
    new-instance v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;-><init>()V

    .line 2567240
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2567241
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2567238
    const v0, 0x7c10ed21    # 3.0100012E36f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2567237
    const v0, -0x1f1a21bb

    return v0
.end method
