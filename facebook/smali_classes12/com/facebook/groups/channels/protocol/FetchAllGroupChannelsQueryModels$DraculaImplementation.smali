.class public final Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2565600
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2565601
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2565598
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2565599
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2565569
    if-nez p1, :cond_0

    move v0, v1

    .line 2565570
    :goto_0
    return v0

    .line 2565571
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2565572
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2565573
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2565574
    const v2, 0x50d7aba7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2565575
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2565576
    const v3, 0x4e363642    # 7.6425229E8f

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2565577
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2565578
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2565579
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2565580
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2565581
    :sswitch_1
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565582
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2565583
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2565584
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2565585
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2565586
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2565587
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2565588
    const v3, 0x6e8d735a

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 2565589
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2565590
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2565591
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2565592
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2565593
    :sswitch_3
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    .line 2565594
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2565595
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2565596
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2565597
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x54d1c2ae -> :sswitch_2
        -0x300413e1 -> :sswitch_0
        0x50d7aba7 -> :sswitch_1
        0x6e8d735a -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2565568
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2565563
    if-eqz p0, :cond_0

    .line 2565564
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2565565
    if-eq v0, p0, :cond_0

    .line 2565566
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2565567
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2565550
    sparse-switch p2, :sswitch_data_0

    .line 2565551
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2565552
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2565553
    const v1, 0x50d7aba7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2565554
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2565555
    const v1, 0x4e363642    # 7.6425229E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2565556
    :goto_0
    return-void

    .line 2565557
    :sswitch_1
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565558
    invoke-static {v0, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 2565559
    :sswitch_2
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 2565560
    const v1, 0x6e8d735a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2565561
    :sswitch_3
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ThreadQueueParticipantsModel$ThreadQueueParticipantsEdgesModel$ThreadQueueParticipantsEdgesNodeModel;

    .line 2565562
    invoke-static {v0, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x54d1c2ae -> :sswitch_2
        -0x300413e1 -> :sswitch_0
        0x50d7aba7 -> :sswitch_1
        0x6e8d735a -> :sswitch_3
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2565540
    if-nez p1, :cond_0

    move v0, v1

    .line 2565541
    :goto_0
    return v0

    .line 2565542
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2565543
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2565544
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2565545
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2565546
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2565547
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2565548
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2565549
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2565533
    if-eqz p1, :cond_0

    .line 2565534
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2565535
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2565536
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2565537
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2565538
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2565539
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2565527
    if-eqz p1, :cond_0

    .line 2565528
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2565529
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    .line 2565530
    if-eq v0, v1, :cond_0

    .line 2565531
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2565532
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2565602
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2565525
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2565526
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2565520
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2565521
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2565522
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2565523
    iput p2, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->b:I

    .line 2565524
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2565519
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2565518
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2565515
    iget v0, p0, LX/1vt;->c:I

    .line 2565516
    move v0, v0

    .line 2565517
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2565512
    iget v0, p0, LX/1vt;->c:I

    .line 2565513
    move v0, v0

    .line 2565514
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2565509
    iget v0, p0, LX/1vt;->b:I

    .line 2565510
    move v0, v0

    .line 2565511
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565506
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2565507
    move-object v0, v0

    .line 2565508
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2565494
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2565495
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2565496
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2565497
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2565498
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2565499
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2565500
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2565501
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2565502
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2565503
    iget v0, p0, LX/1vt;->c:I

    .line 2565504
    move v0, v0

    .line 2565505
    return v0
.end method
