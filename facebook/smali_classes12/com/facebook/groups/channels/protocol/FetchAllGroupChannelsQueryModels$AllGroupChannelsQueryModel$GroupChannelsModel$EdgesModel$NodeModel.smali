.class public final Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x420a102c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2565422
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2565421
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2565419
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2565420
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565417
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 2565418
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2565398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2565399
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2565400
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2565401
    invoke-direct {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2565402
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2565403
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2565404
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2565405
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, -0x54d1c2ae

    invoke-static {v7, v6, v8}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2565406
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2565407
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2565408
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2565409
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2565410
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2565411
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2565412
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2565413
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2565414
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2565415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2565416
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2565354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2565355
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2565356
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    .line 2565357
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2565358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565359
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    .line 2565360
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2565361
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    .line 2565362
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2565363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565364
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->h:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    .line 2565365
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2565366
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    .line 2565367
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2565368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565369
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    .line 2565370
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2565371
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x54d1c2ae

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2565372
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2565373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    .line 2565374
    iput v3, v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l:I

    move-object v1, v0

    .line 2565375
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2565376
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 2565377
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 2565378
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565397
    invoke-direct {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2565393
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2565394
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->i:Z

    .line 2565395
    const/4 v0, 0x7

    const v1, -0x54d1c2ae

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l:I

    .line 2565396
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2565423
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;-><init>()V

    .line 2565424
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2565425
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2565392
    const v0, 0x6c744fa5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2565391
    const v0, -0x20950cd6

    return v0
.end method

.method public final j()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565389
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    .line 2565390
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565387
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 2565388
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565385
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->h:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->h:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    .line 2565386
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->h:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$ImageModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565383
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    .line 2565384
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->j:Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565381
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    .line 2565382
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadQueueParticipants"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565379
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2565380
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
