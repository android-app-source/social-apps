.class public final Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5167f2a7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2565243
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2565242
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2565224
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2565225
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2565236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2565237
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2565238
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2565239
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2565240
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2565241
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2565233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2565234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2565235
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565231
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->e:Ljava/lang/String;

    .line 2565232
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2565228
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;

    invoke-direct {v0}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel$CustomizationInfoModel;-><init>()V

    .line 2565229
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2565230
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2565227
    const v0, -0xfeeadc0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2565226
    const v0, -0x3b2f557e

    return v0
.end method
