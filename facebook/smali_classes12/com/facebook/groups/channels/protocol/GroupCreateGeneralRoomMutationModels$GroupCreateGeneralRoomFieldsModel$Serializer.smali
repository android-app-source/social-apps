.class public final Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2567224
    const-class v0, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    new-instance v1, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2567225
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2567223
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2567227
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2567228
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2567229
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2567230
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2567231
    if-eqz v2, :cond_0

    .line 2567232
    const-string p0, "general_room"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2567233
    invoke-static {v1, v2, p1, p2}, LX/IKj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2567234
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2567235
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2567226
    check-cast p1, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel$Serializer;->a(Lcom/facebook/groups/channels/protocol/GroupCreateGeneralRoomMutationModels$GroupCreateGeneralRoomFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
