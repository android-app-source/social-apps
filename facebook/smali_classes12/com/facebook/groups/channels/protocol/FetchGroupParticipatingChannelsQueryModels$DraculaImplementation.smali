.class public final Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2565910
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2565911
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2565922
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2565923
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2565913
    if-nez p1, :cond_0

    .line 2565914
    :goto_0
    return v0

    .line 2565915
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2565916
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2565917
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2565918
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2565919
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2565920
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2565921
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3ffe5925
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2565912
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2565907
    packed-switch p0, :pswitch_data_0

    .line 2565908
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2565909
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3ffe5925
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2565906
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2565904
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->b(I)V

    .line 2565905
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2565924
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2565925
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2565926
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2565927
    iput p2, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->b:I

    .line 2565928
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2565903
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2565902
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2565899
    iget v0, p0, LX/1vt;->c:I

    .line 2565900
    move v0, v0

    .line 2565901
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2565896
    iget v0, p0, LX/1vt;->c:I

    .line 2565897
    move v0, v0

    .line 2565898
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2565878
    iget v0, p0, LX/1vt;->b:I

    .line 2565879
    move v0, v0

    .line 2565880
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2565893
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2565894
    move-object v0, v0

    .line 2565895
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2565884
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2565885
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2565886
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2565887
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2565888
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2565889
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2565890
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2565891
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2565892
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2565881
    iget v0, p0, LX/1vt;->c:I

    .line 2565882
    move v0, v0

    .line 2565883
    return v0
.end method
