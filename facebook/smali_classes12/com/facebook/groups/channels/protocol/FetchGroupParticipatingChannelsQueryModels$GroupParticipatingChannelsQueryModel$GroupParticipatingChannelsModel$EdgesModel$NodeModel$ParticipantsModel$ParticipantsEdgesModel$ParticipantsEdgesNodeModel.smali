.class public final Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1320605b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2566413
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2566412
    const-class v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2566410
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2566411
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2566408
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->f:Ljava/lang/String;

    .line 2566409
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2566400
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2566401
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2566402
    invoke-direct {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2566403
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2566404
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2566405
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2566406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2566407
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2566414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2566415
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2566416
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    .line 2566417
    invoke-virtual {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2566418
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    .line 2566419
    iput-object v0, v1, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    .line 2566420
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2566421
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2566399
    invoke-direct {p0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2566396
    new-instance v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;-><init>()V

    .line 2566397
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2566398
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2566395
    const v0, -0x329d7368

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2566394
    const v0, -0x3cd03651

    return v0
.end method

.method public final j()Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2566392
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    .line 2566393
    iget-object v0, p0, Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel;->e:Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel$ParticipantsModel$ParticipantsEdgesModel$ParticipantsEdgesNodeModel$ActorModel;

    return-object v0
.end method
