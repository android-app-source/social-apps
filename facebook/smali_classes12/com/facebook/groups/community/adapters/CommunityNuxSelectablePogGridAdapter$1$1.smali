.class public final Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/IL4;


# direct methods
.method public constructor <init>(LX/IL4;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2567615
    iput-object p1, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iput-object p2, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2567616
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v0, v0, LX/IL4;->c:LX/IL6;

    iget-object v0, v0, LX/IL6;->a:LX/DKB;

    iget-object v1, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v1, v1, LX/IL4;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DKB;->c(Ljava/lang/String;)Z

    move-result v1

    .line 2567617
    if-eqz v1, :cond_1

    .line 2567618
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v0, v0, LX/IL4;->b:LX/IL5;

    iget-object v0, v0, LX/IL5;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Z)V

    .line 2567619
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v0, v0, LX/IL4;->c:LX/IL6;

    iget-object v0, v0, LX/IL6;->a:LX/DKB;

    iget-object v2, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v2, v2, LX/IL4;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2567620
    iget-object v3, v0, LX/DKB;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2567621
    iget-object v3, v0, LX/DKB;->a:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2567622
    :cond_0
    iget-object v3, v0, LX/DKB;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    if-nez v3, :cond_2

    .line 2567623
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    iget-object v2, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v2, v2, LX/IL4;->c:LX/IL6;

    iget-object v2, v2, LX/IL6;->c:LX/DKA;

    iget-object v3, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v3, v3, LX/IL4;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/DKA;->a(Ljava/lang/String;Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/DaX;->setPlaceholderImage(I)V

    .line 2567624
    return-void

    .line 2567625
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v0, v0, LX/IL4;->b:LX/IL5;

    iget-object v0, v0, LX/IL5;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Z)V

    .line 2567626
    iget-object v0, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v0, v0, LX/IL4;->c:LX/IL6;

    iget-object v0, v0, LX/IL6;->a:LX/DKB;

    iget-object v2, p0, Lcom/facebook/groups/community/adapters/CommunityNuxSelectablePogGridAdapter$1$1;->b:LX/IL4;

    iget-object v2, v2, LX/IL4;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/DKB;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2567627
    :cond_2
    iget-object v3, v0, LX/DKB;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    .line 2567628
    iget-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    .line 2567629
    iget-object v5, v4, LX/DKB;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_1
    move v4, v5

    .line 2567630
    if-nez v4, :cond_3

    .line 2567631
    const/4 v2, 0x1

    .line 2567632
    iget-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    if-nez v4, :cond_5

    .line 2567633
    :cond_3
    :goto_2
    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 2567634
    :cond_5
    iget-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->l:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-nez v4, :cond_6

    .line 2567635
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v0, 0x7f080030

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2567636
    iput-object v5, v4, LX/108;->g:Ljava/lang/String;

    .line 2567637
    move-object v4, v4

    .line 2567638
    iput-boolean v2, v4, LX/108;->q:Z

    .line 2567639
    move-object v4, v4

    .line 2567640
    const v5, 0x7f0a00a3

    .line 2567641
    iput v5, v4, LX/108;->m:I

    .line 2567642
    move-object v4, v4

    .line 2567643
    iput-boolean v2, v4, LX/108;->d:Z

    .line 2567644
    move-object v4, v4

    .line 2567645
    invoke-virtual {v4}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->l:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2567646
    :cond_6
    iget-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    iget-object v5, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->l:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v4, v5}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2567647
    iget-object v4, v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    new-instance v5, LX/ILV;

    invoke-direct {v5, v3}, LX/ILV;-><init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V

    invoke-interface {v4, v5}, LX/1ZF;->a(LX/63W;)V

    goto :goto_2
.end method
