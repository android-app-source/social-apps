.class public Lcom/facebook/groups/community/search/CommunitySearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;


# instance fields
.field public a:LX/IN2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ING;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FgS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FiU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/IM6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/INF;

.field public j:Lcom/facebook/widget/listview/BetterListView;

.field private k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

.field public l:Landroid/widget/ViewSwitcher;

.field private m:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

.field public n:Lcom/facebook/ui/search/SearchEditText;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:LX/7HZ;

.field public r:LX/DSo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DSo",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/IN9;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

.field private w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2570423
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2570424
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->o:Ljava/lang/String;

    .line 2570425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    .line 2570426
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2570427
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2570428
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2570429
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2570430
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2570431
    const/4 v0, 0x0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2570451
    const-string v0, "search"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2570432
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2570433
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;

    new-instance v7, LX/IN2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/IM6;->a(LX/0QB;)LX/IM6;

    move-result-object v4

    check-cast v4, LX/IM6;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    invoke-direct {v7, v3, v4, v5, v6}, LX/IN2;-><init>(Landroid/content/Context;LX/IM6;LX/03V;LX/0if;)V

    move-object v3, v7

    check-cast v3, LX/IN2;

    const-class v4, LX/ING;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/ING;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v6

    check-cast v6, LX/FgS;

    invoke-static {v0}, LX/FiU;->a(LX/0QB;)LX/FiU;

    move-result-object v7

    check-cast v7, LX/FiU;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v0}, LX/IM6;->a(LX/0QB;)LX/IM6;

    move-result-object p1

    check-cast p1, LX/IM6;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->a:LX/IN2;

    iput-object v4, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->b:LX/ING;

    iput-object v5, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    iput-object v6, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->d:LX/FgS;

    iput-object v7, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->e:LX/FiU;

    iput-object v8, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->f:LX/17W;

    iput-object p1, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->g:LX/IM6;

    iput-object v0, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->h:LX/0if;

    .line 2570434
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2570435
    const-string v1, "group_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    .line 2570436
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2570437
    const-string v1, "group_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->u:Ljava/lang/String;

    .line 2570438
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2570439
    const-string v1, "community_default_search_query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->w:Ljava/util/ArrayList;

    .line 2570440
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2570441
    const-string v1, "community_nux_question"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2570442
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->b:LX/ING;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2570443
    new-instance v2, LX/INF;

    invoke-static {v0}, LX/7Hg;->b(LX/0QB;)LX/7Hg;

    move-result-object v4

    check-cast v4, LX/7Hg;

    const-class v3, LX/INE;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/INE;

    invoke-static {v0}, LX/7Hl;->b(LX/0QB;)LX/7Hl;

    move-result-object v6

    check-cast v6, LX/7Hl;

    .line 2570444
    new-instance v3, LX/INJ;

    invoke-direct {v3}, LX/INJ;-><init>()V

    .line 2570445
    move-object v3, v3

    .line 2570446
    move-object v7, v3

    .line 2570447
    check-cast v7, LX/7HW;

    const-class v3, LX/7Hk;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/7Hk;

    invoke-static {v0}, LX/INI;->a(LX/0QB;)LX/INI;

    move-result-object v9

    check-cast v9, LX/INI;

    invoke-static {v0}, LX/7Ho;->b(LX/0QB;)LX/7Ho;

    move-result-object v10

    check-cast v10, LX/7Ho;

    invoke-static {v0}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v11

    check-cast v11, LX/2Sd;

    move-object v3, v1

    invoke-direct/range {v2 .. v11}, LX/INF;-><init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;LX/7Hg;LX/INE;LX/7Hl;LX/7HW;LX/7Hk;LX/INI;LX/7Ho;LX/2Sd;)V

    .line 2570448
    move-object v0, v2

    .line 2570449
    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    .line 2570450
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 2570384
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2570385
    if-eq p2, v6, :cond_0

    .line 2570386
    :goto_0
    return-void

    .line 2570387
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2570388
    :pswitch_0
    const-string v0, "group_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2570389
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570390
    const-string v0, "group_feed_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2570391
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570392
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->g:LX/IM6;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v5}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IM6;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2570393
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "question_id"

    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "community_id"

    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "group_id"

    invoke-virtual {v0, v1, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2570394
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->h:LX/0if;

    sget-object v2, LX/0ig;->aS:LX/0ih;

    const-string v3, "did_create_group"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2570395
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2570396
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2570397
    invoke-virtual {v1, v6, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2570398
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x381056f2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2570399
    const v0, 0x7f0302f3

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2570400
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307f0

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->m:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2570401
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->e:LX/FiU;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->m:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/FiU;->a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Ljava/lang/String;)V

    .line 2570402
    const v0, 0x7f0d0a28

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->l:Landroid/widget/ViewSwitcher;

    .line 2570403
    const v0, 0x7f0d0a2a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    .line 2570404
    const v0, 0x7f0d0a29

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    .line 2570405
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    if-eqz v0, :cond_0

    .line 2570406
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setDescription(Ljava/lang/String;)V

    .line 2570407
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->hM_()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setSuggestedKeywords(Ljava/util/List;)V

    .line 2570408
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    new-instance v1, LX/IMq;

    invoke-direct {v1, p0}, LX/IMq;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setSuggestedKeywordsOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570409
    new-instance v0, LX/IN9;

    invoke-direct {v0}, LX/IN9;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->s:LX/IN9;

    .line 2570410
    new-instance v0, LX/DSo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->s:LX/IN9;

    iget-object v3, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->s:LX/IN9;

    iget-object v4, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->a:LX/IN2;

    iget-object v5, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    invoke-direct/range {v0 .. v5}, LX/DSo;-><init>(Landroid/content/Context;LX/BWf;LX/DSX;LX/DS5;LX/BWd;)V

    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->r:LX/DSo;

    .line 2570411
    new-instance v0, LX/INH;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/INH;-><init>(Landroid/content/Context;)V

    .line 2570412
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0, v10, v8}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2570413
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v8}, Lcom/facebook/widget/listview/BetterListView;->setFooterDividersEnabled(Z)V

    .line 2570414
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->r:LX/DSo;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2570415
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v9}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2570416
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/IMr;

    invoke-direct {v2, p0}, LX/IMr;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2570417
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    new-instance v2, LX/IMx;

    iget-object v3, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->s:LX/IN9;

    iget-object v4, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->r:LX/DSo;

    invoke-direct {v2, p0, v3, v4}, LX/IMx;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;LX/IN9;LX/DSo;)V

    .line 2570418
    iput-object v2, v1, LX/7HQ;->k:LX/7HP;

    .line 2570419
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    new-instance v2, LX/IMy;

    invoke-direct {v2, p0, v0}, LX/IMy;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;LX/INH;)V

    invoke-virtual {v1, v2}, LX/7HQ;->a(LX/2Sp;)V

    .line 2570420
    const v0, -0x6229bf6e

    invoke-static {v0, v6}, LX/02F;->f(II)V

    return-object v7

    .line 2570421
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08301d

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->u:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setDescription(Ljava/lang/String;)V

    .line 2570422
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setSuggestedKeywords(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2c2e069

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2570376
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2570377
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    .line 2570378
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2570379
    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->k:Lcom/facebook/groups/community/views/CommunitySearchNullStateView;

    invoke-virtual {v1, v2}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setSuggestedKeywordsOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570380
    iput-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    .line 2570381
    iput-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->m:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2570382
    iput-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    .line 2570383
    const/16 v1, 0x2b

    const v2, -0x2bf54840

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4ec3d8a9    # 1.64287808E9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2570371
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2570372
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2570373
    if-eqz v1, :cond_0

    .line 2570374
    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2570375
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x19e2d719

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x561c685d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2570366
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2570367
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2570368
    if-eqz v1, :cond_0

    .line 2570369
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2570370
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x11900e9a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2570363
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2570364
    const-string v0, "state_text"

    iget-object v1, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570365
    return-void
.end method

.method public final onStart()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x28a0470

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2570346
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2570347
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2570348
    if-eqz v0, :cond_0

    .line 2570349
    iget-object v2, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->e:LX/FiU;

    const/4 v3, 0x0

    const-string v4, ""

    invoke-virtual {v2, v3, v0, v4}, LX/FiU;->a(ZLX/1ZF;Ljava/lang/String;)V

    .line 2570350
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->d:LX/FgS;

    .line 2570351
    iget-object v2, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2570352
    if-nez v0, :cond_1

    .line 2570353
    :goto_0
    const/16 v0, 0x2b

    const v2, 0x487bc2c8    # 257803.12f

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2570354
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->d:LX/FgS;

    .line 2570355
    iget-object v2, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2570356
    iget-object v2, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 2570357
    iput-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    .line 2570358
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083002

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->u:Ljava/lang/String;

    aput-object v7, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2570359
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2570360
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    new-instance v2, LX/IMs;

    invoke-direct {v2, p0}, LX/IMs;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2570361
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    new-instance v2, LX/IMt;

    invoke-direct {v2, p0}, LX/IMt;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2570362
    iget-object v0, p0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    new-instance v2, LX/IMu;

    invoke-direct {v2, p0}, LX/IMu;-><init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0
.end method
