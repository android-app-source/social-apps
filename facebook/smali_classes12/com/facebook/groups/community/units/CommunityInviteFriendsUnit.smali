.class public Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;
.super LX/INP;
.source ""


# instance fields
.field public d:LX/DVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2570824
    invoke-direct {p0, p1}, LX/INP;-><init>(Landroid/content/Context;)V

    .line 2570825
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;

    invoke-static {p1}, LX/Jaq;->b(LX/0QB;)LX/Jaq;

    move-result-object v2

    check-cast v2, LX/DVF;

    const/16 v0, 0x455

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    iput-object v2, p0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->d:LX/DVF;

    iput-object v0, p0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->e:LX/0Ot;

    iput-object p1, p0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->f:LX/0Zb;

    .line 2570826
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2570820
    iget-object v0, p0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->f:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2570821
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2570822
    const-string v1, "community"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "community_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2570823
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGroupVisibility;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2570827
    new-instance v0, LX/INS;

    invoke-direct {v0, p0, p2, p3, p4}, LX/INS;-><init>(Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V

    move-object v0, v0

    .line 2570828
    invoke-super {p0, v0}, LX/INP;->a(Landroid/view/View$OnClickListener;)V

    .line 2570829
    if-eqz p5, :cond_0

    invoke-virtual {p5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2570830
    :cond_0
    return-void

    .line 2570831
    :cond_1
    const-string v0, "community_invite_friends_unit_shown"

    invoke-static {p0, v0, p2}, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->a$redex0(Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;Ljava/lang/String;Ljava/lang/String;)V

    .line 2570832
    iget-object v0, p0, LX/INP;->b:Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2570833
    iget-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 2570834
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    .line 2570835
    new-instance v3, LX/IO9;

    invoke-virtual {p0}, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/IO9;-><init>(Landroid/content/Context;)V

    .line 2570836
    const/4 p4, 0x0

    .line 2570837
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v4

    if-nez v4, :cond_3

    .line 2570838
    :cond_2
    :goto_1
    iget-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2570839
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2570840
    :cond_3
    iget-object v4, v3, LX/IO9;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->hK_()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570841
    iget-object v4, v3, LX/IO9;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570842
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_4

    .line 2570843
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v4

    iget-object p1, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2570844
    iget-object p3, v3, LX/IO9;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1, v4, p4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-class p1, LX/IO9;

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    invoke-virtual {p3, v4, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2570845
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v4

    .line 2570846
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    .line 2570847
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel;->a()LX/0Px;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel$NodesModel;

    .line 2570848
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel$NodesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$EducationExperiencesModel$NodesModel$SchoolModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 2570849
    :goto_2
    move-object v4, p1

    .line 2570850
    if-eqz v4, :cond_5

    .line 2570851
    iget-object p1, v3, LX/IO9;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570852
    iget-object v4, v3, LX/IO9;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2570853
    :cond_5
    iget-object v4, v3, LX/IO9;->e:LX/IOF;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object p1

    .line 2570854
    iget-object p3, v4, LX/IOF;->a:Ljava/util/HashMap;

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/IOE;

    move-object v4, p3

    .line 2570855
    if-eqz v4, :cond_8

    .line 2570856
    :goto_3
    invoke-static {v3, v4}, LX/IO9;->setInviteButtonState(LX/IO9;LX/IOE;)V

    .line 2570857
    iget-object v4, v3, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    new-instance p1, LX/IO6;

    invoke-direct {p1, v3, v0, p2}, LX/IO6;-><init>(LX/IO9;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570858
    new-instance v4, LX/IO7;

    invoke-direct {v4, v3, v0}, LX/IO7;-><init>(LX/IO9;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;)V

    invoke-virtual {v3, v4}, LX/IO9;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$CurrentCityModel;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$CurrentCityModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    goto :goto_2

    .line 2570859
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->b()Z

    move-result v4

    if-eqz v4, :cond_9

    sget-object v4, LX/IOE;->INVITED:LX/IOE;

    goto :goto_3

    :cond_9
    sget-object v4, LX/IOE;->NOT_INVITED:LX/IOE;

    goto :goto_3
.end method

.method public getHeaderTextResId()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2570819
    const v0, 0x7f082fea

    return v0
.end method
