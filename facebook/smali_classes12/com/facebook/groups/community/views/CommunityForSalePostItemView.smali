.class public Lcom/facebook/groups/community/views/CommunityForSalePostItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2571262
    const-class v0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2571280
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2571281
    const-class v0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2571282
    iput-object p1, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->d:Landroid/content/Context;

    .line 2571283
    const v0, 0x7f0302f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2571284
    const v0, 0x7f0d0a1f

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2571285
    const v0, 0x7f0d0a20

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2571286
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p0, p1, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2571265
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2571266
    :cond_0
    :goto_0
    return-void

    .line 2571267
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;

    .line 2571268
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->c()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->c()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/39O;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2571269
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->c()LX/2uF;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2571270
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2571271
    invoke-virtual {v3, v1, v2}, LX/15i;->g(II)I

    move-result v1

    iget-object v4, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v3, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2571272
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2571273
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2571274
    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_2

    .line 2571275
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->b()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v4, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571276
    :cond_2
    new-instance v1, LX/INt;

    invoke-direct {v1, p0, v0}, LX/INt;-><init>(Lcom/facebook/groups/community/views/CommunityForSalePostItemView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;)V

    invoke-virtual {p0, v1}, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2571277
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2571278
    :cond_3
    iget-object v1, p0, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 2571279
    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2571263
    invoke-super {p0, p1, p1}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2571264
    return-void
.end method
