.class public Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/pm/PackageManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2571493
    const-class v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2571494
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571495
    const-class v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2571496
    const v0, 0x7f0302f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571497
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->setOrientation(I)V

    .line 2571498
    const v0, 0x7f0d0a33

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2571499
    const v0, 0x7f0d0a34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2571500
    const v0, 0x7f0d0a35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2571501
    const v0, 0x7f020a3c

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->setBackgroundResource(I)V

    .line 2571502
    invoke-virtual {p0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b0034

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2571503
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->setPadding(IIII)V

    .line 2571504
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->a:Landroid/content/pm/PackageManager;

    iput-object p0, p1, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method
