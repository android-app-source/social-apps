.class public Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Landroid/widget/ImageView;

.field public d:Lcom/facebook/fbui/drawable/NetworkDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2571516
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2571517
    invoke-direct {p0, p1}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a(Landroid/content/Context;)V

    .line 2571518
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2571519
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2571520
    invoke-direct {p0, p1}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a(Landroid/content/Context;)V

    .line 2571521
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2571522
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2571523
    invoke-direct {p0, p1}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a(Landroid/content/Context;)V

    .line 2571524
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2571525
    const-class v0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2571526
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0302f9

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2571527
    const v1, 0x7f0d0a3c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->b:Landroid/view/View;

    .line 2571528
    iget-object v1, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->b:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2571529
    const v1, 0x7f0d0a3d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->c:Landroid/widget/ImageView;

    .line 2571530
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/drawable/NetworkDrawable;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->d:Lcom/facebook/fbui/drawable/NetworkDrawable;

    .line 2571531
    invoke-virtual {p0}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02027d

    new-instance v2, LX/IOB;

    invoke-direct {v2, p0}, LX/IOB;-><init>(Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;)V

    invoke-static {v0, v1, v2}, LX/99C;->a(Landroid/content/res/Resources;ILX/99A;)V

    .line 2571532
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->a:LX/0Sh;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2571533
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2571534
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2571535
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunityQuestionsNuxRobotView;->c:Landroid/widget/ImageView;

    const v1, 0x7f02027c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2571536
    return-void
.end method
