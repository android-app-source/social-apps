.class public Lcom/facebook/groups/community/views/CommunitySearchNullStateView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2571569
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571570
    invoke-direct {p0}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a()V

    .line 2571571
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2571566
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2571567
    invoke-direct {p0}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a()V

    .line 2571568
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2571563
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2571564
    invoke-direct {p0}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a()V

    .line 2571565
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2571537
    const v0, 0x7f0302f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571538
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->setOrientation(I)V

    .line 2571539
    const v0, 0x7f0d0a39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2571540
    const v0, 0x7f0d0a3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2571541
    const v0, 0x7f0d0a3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2571542
    const v0, 0x7f0d0a37

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2571543
    const v0, 0x7f0d0a38

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2571544
    return-void
.end method


# virtual methods
.method public setDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2571561
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571562
    return-void
.end method

.method public setSuggestedKeywords(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v2, 0x8

    .line 2571549
    if-nez p1, :cond_0

    .line 2571550
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2571551
    :goto_0
    return-void

    .line 2571552
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2571553
    iget-object v1, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571554
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 2571555
    iget-object v1, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571556
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 2571557
    iget-object v1, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2571558
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 2571559
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_2

    .line 2571560
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSuggestedKeywordsOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2571545
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571546
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571547
    iget-object v0, p0, Lcom/facebook/groups/community/views/CommunitySearchNullStateView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571548
    return-void
.end method
