.class public Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static a:I


# instance fields
.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/ILF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IL7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/IL9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/IL6;

.field public j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

.field public k:LX/DKB;

.field public l:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private m:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public n:LX/1ZF;

.field public o:LX/IL1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2568105
    const/4 v0, 0x0

    sput v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568028
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2568029
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2568101
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2568102
    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/DKB;->a(Ljava/lang/String;)V

    .line 2568103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2568104
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2568099
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->b:LX/1Ck;

    sget-object v1, LX/ILX;->FETCH_COMMUNITY_FORUM_GROUPS:LX/ILX;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->c:LX/ILF;

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-virtual {v2, p1, v3, v4}, LX/ILF;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/ILQ;

    invoke-direct {v3, p0, p1}, LX/ILQ;-><init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2568100
    return-void
.end method

.method public static b$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2568095
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->i:LX/IL6;

    .line 2568096
    iput-object p1, v0, LX/IL6;->b:LX/0Px;

    .line 2568097
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->i:LX/IL6;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2568098
    return-void
.end method

.method public static k(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V
    .locals 11

    .prologue
    .line 2568080
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082ffa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2568081
    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2568082
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->h:LX/0if;

    sget-object v2, LX/0ig;->aT:LX/0ih;

    const-string v3, "did_request_join_forum_groups"

    const/4 v4, 0x0

    .line 2568083
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v7

    .line 2568084
    iget-object v5, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    invoke-virtual {v5}, LX/DKB;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v8

    .line 2568085
    const/4 v5, 0x1

    move v6, v5

    .line 2568086
    :goto_0
    invoke-virtual {v8}, LX/0Rc;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2568087
    invoke-virtual {v8}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2568088
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "selected_group_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2568089
    add-int/lit8 v6, v6, 0x1

    .line 2568090
    invoke-virtual {v7, v9, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    goto :goto_0

    .line 2568091
    :cond_0
    move-object v5, v7

    .line 2568092
    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2568093
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->b:LX/1Ck;

    sget-object v2, LX/ILX;->JOIN_GROUPS:LX/ILX;

    new-instance v3, LX/ILR;

    invoke-direct {v3, p0}, LX/ILR;-><init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V

    new-instance v4, LX/ILT;

    invoke-direct {v4, p0, v0}, LX/ILT;-><init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/4At;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2568094
    return-void
.end method

.method public static n(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2568067
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    if-nez v0, :cond_0

    .line 2568068
    :goto_0
    return-void

    .line 2568069
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->m:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-nez v0, :cond_1

    .line 2568070
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2568071
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2568072
    move-object v0, v0

    .line 2568073
    iput-boolean v3, v0, LX/108;->q:Z

    .line 2568074
    move-object v0, v0

    .line 2568075
    iput-boolean v3, v0, LX/108;->d:Z

    .line 2568076
    move-object v0, v0

    .line 2568077
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->m:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2568078
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->m:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2568079
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    new-instance v1, LX/ILW;

    invoke-direct {v1, p0}, LX/ILW;-><init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2568047
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2568048
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/ILF;->b(LX/0QB;)LX/ILF;

    move-result-object v4

    check-cast v4, LX/ILF;

    const/16 v5, 0x12cb

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v6

    check-cast v6, LX/3mF;

    const-class v7, LX/IL7;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/IL7;

    invoke-static {v0}, LX/IL9;->b(LX/0QB;)LX/IL9;

    move-result-object p1

    check-cast p1, LX/IL9;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->b:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->c:LX/ILF;

    iput-object v5, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->d:LX/0Or;

    iput-object v6, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->e:LX/3mF;

    iput-object v7, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->f:LX/IL7;

    iput-object p1, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->g:LX/IL9;

    iput-object v0, v2, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->h:LX/0if;

    .line 2568049
    new-instance v0, LX/DKB;

    invoke-direct {v0}, LX/DKB;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    .line 2568050
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    .line 2568051
    iput-object p0, v0, LX/DKB;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    .line 2568052
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->f:LX/IL7;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    .line 2568053
    new-instance v3, LX/IL6;

    invoke-static {v0}, LX/DKA;->a(LX/0QB;)LX/DKA;

    move-result-object v2

    check-cast v2, LX/DKA;

    invoke-direct {v3, v1, v2}, LX/IL6;-><init>(LX/DKB;LX/DKA;)V

    .line 2568054
    move-object v0, v3

    .line 2568055
    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->i:LX/IL6;

    .line 2568056
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568057
    const-string v1, "community_forum_groups"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    .line 2568058
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2568059
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568060
    const-string v1, "community_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2568061
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Community id cannot be empty"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2568062
    invoke-static {p0, v1}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;Ljava/lang/String;)V

    .line 2568063
    :goto_1
    return-void

    .line 2568064
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2568065
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V

    .line 2568066
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->b$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v0, 0x2a

    const v1, -0x51387e8c

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2568036
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2568037
    const v0, 0x7f0302f5

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2568038
    const v0, 0x7f0d0a30

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2568039
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2568040
    const-string v4, "community_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2568041
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082ff9

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2568042
    const v0, 0x7f0d0a32

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2568043
    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->i:LX/IL6;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2568044
    new-instance v3, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, LX/3wu;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2568045
    iput-boolean v8, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2568046
    const/16 v0, 0x2b

    const v3, 0x332a32e4

    invoke-static {v9, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5edcecf1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568030
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2568031
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    .line 2568032
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    if-eqz v1, :cond_0

    .line 2568033
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2568034
    invoke-static {p0}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->n(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V

    .line 2568035
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x23e92d49

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
