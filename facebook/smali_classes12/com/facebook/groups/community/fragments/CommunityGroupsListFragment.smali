.class public Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/ILF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/ILp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public h:LX/5QS;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public l:Landroid/widget/ProgressBar;

.field public m:Lcom/facebook/widget/error/GenericErrorViewStub;

.field public n:Lcom/facebook/widget/error/GenericErrorView;

.field public o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public p:LX/1P0;

.field public q:LX/ILI;

.field private final r:LX/1OX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2567953
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2567954
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567955
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->j:Z

    .line 2567956
    new-instance v0, LX/ILJ;

    invoke-direct {v0, p0}, LX/ILJ;-><init>(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->r:LX/1OX;

    return-void
.end method

.method private a(LX/5QS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5QS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/ILO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2567924
    sget-object v0, LX/ILN;->a:[I

    invoke-virtual {p1}, LX/5QS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2567925
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567926
    iget-object v4, v0, LX/ILF;->a:LX/0tX;

    .line 2567927
    new-instance v3, LX/IMb;

    invoke-direct {v3}, LX/IMb;-><init>()V

    move-object v3, v3

    .line 2567928
    const-string v5, "group_id"

    invoke-virtual {v3, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "after_cursor"

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "first_count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "cover_photo_height"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "cover_photo_width"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/IMb;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2567929
    new-instance v4, LX/ILC;

    invoke-direct {v4, v0}, LX/ILC;-><init>(LX/ILF;)V

    iget-object v5, v0, LX/ILF;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2567930
    :goto_0
    return-object v0

    .line 2567931
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567932
    iget-object v3, v0, LX/ILF;->a:LX/0tX;

    .line 2567933
    new-instance v4, LX/IMV;

    invoke-direct {v4}, LX/IMV;-><init>()V

    move-object v4, v4

    .line 2567934
    const-string v5, "group_id"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after_cursor"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "first_count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cover_photo_height"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cover_photo_width"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2567935
    new-instance v4, LX/ILA;

    invoke-direct {v4, v0}, LX/ILA;-><init>(LX/ILF;)V

    iget-object v5, v0, LX/ILF;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2567936
    goto :goto_0

    .line 2567937
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567938
    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, LX/ILF;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2567939
    new-instance v4, LX/ILB;

    invoke-direct {v4, v0}, LX/ILB;-><init>(LX/ILF;)V

    iget-object v5, v0, LX/ILF;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2567940
    goto :goto_0

    .line 2567941
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567942
    iget-object v4, v0, LX/ILF;->a:LX/0tX;

    .line 2567943
    new-instance v3, LX/IMD;

    invoke-direct {v3}, LX/IMD;-><init>()V

    move-object v3, v3

    .line 2567944
    const-string v5, "group_id"

    invoke-virtual {v3, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "after_cursor"

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v5, "first_count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "cover_photo_height"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v5, "cover_photo_width"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/IMD;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2567945
    new-instance v4, LX/ILD;

    invoke-direct {v4, v0}, LX/ILD;-><init>(LX/ILF;)V

    iget-object v5, v0, LX/ILF;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2567946
    goto/16 :goto_0

    .line 2567947
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567948
    iget-object v3, v0, LX/ILF;->a:LX/0tX;

    .line 2567949
    new-instance v4, LX/IM7;

    invoke-direct {v4}, LX/IM7;-><init>()V

    move-object v4, v4

    .line 2567950
    const-string v5, "group_id"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after_cursor"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "first_count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cover_photo_height"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cover_photo_width"

    iget-object p0, v0, LX/ILF;->c:Landroid/content/res/Resources;

    const p1, 0x7f0b008b

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2567951
    new-instance v4, LX/ILE;

    invoke-direct {v4, v0}, LX/ILE;-><init>(LX/ILF;)V

    iget-object v5, v0, LX/ILF;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2567952
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    invoke-static {p0}, LX/ILF;->b(LX/0QB;)LX/ILF;

    move-result-object v1

    check-cast v1, LX/ILF;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/ILp;->a(LX/0QB;)LX/ILp;

    move-result-object v3

    check-cast v3, LX/ILp;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a:LX/ILF;

    iput-object v2, p1, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->b:LX/1Ck;

    iput-object v3, p1, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->c:LX/ILp;

    iput-object p0, p1, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->d:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static c(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V
    .locals 4

    .prologue
    .line 2567922
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->b:LX/1Ck;

    sget-object v1, LX/ILP;->FETCH_COMMUNITY_GROUPS_TASK:LX/ILP;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    invoke-direct {p0, v2}, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a(LX/5QS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/ILM;

    invoke-direct {v3, p0}, LX/ILM;-><init>(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2567923
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2567911
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2567912
    const-class v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2567913
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2567914
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->e:Ljava/lang/String;

    .line 2567915
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2567916
    const-string v1, "group_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->f:Ljava/lang/String;

    .line 2567917
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2567918
    const-string v1, "group_category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 2567919
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2567920
    const-string v1, "community_groups_list_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5QS;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    .line 2567921
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, 0x53c8394a

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2567869
    const v0, 0x7f0302fd

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2567870
    const v0, 0x7f0d0a4b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->k:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2567871
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->k:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v3, LX/ILK;

    invoke-direct {v3, p0}, LX/ILK;-><init>(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2567872
    const v0, 0x7f0d0a4c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2567873
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->p:LX/1P0;

    .line 2567874
    new-instance v0, LX/ILI;

    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    invoke-direct {v0, v3}, LX/ILI;-><init>(LX/5QS;)V

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->q:LX/ILI;

    .line 2567875
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->p:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2567876
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->q:LX/ILI;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2567877
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->r:LX/1OX;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2567878
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0117

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0036

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/62X;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2567879
    const v0, 0x7f0d0a49

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->l:Landroid/widget/ProgressBar;

    .line 2567880
    const v0, 0x7f0d0a4a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorViewStub;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->m:Lcom/facebook/widget/error/GenericErrorViewStub;

    .line 2567881
    const/16 v0, 0x2b

    const v3, -0x13e724d4

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x405653a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2567903
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2567904
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2567905
    iput-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->l:Landroid/widget/ProgressBar;

    .line 2567906
    iput-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->m:Lcom/facebook/widget/error/GenericErrorViewStub;

    .line 2567907
    iput-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->n:Lcom/facebook/widget/error/GenericErrorView;

    .line 2567908
    iput-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2567909
    iput-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->k:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2567910
    const/16 v1, 0x2b

    const v2, 0x44dc6331

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x572460b4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2567885
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2567886
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2567887
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2567888
    if-eqz v1, :cond_1

    .line 2567889
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    sget-object v4, LX/5QS;->ALL_CHILD_GROUPS:LX/5QS;

    if-eq v2, v4, :cond_0

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    sget-object v4, LX/5QS;->ALL_SUB_GROUPS:LX/5QS;

    if-ne v2, v4, :cond_2

    .line 2567890
    :cond_0
    new-instance v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2567891
    const v4, 0x7f0820b7

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->f:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2567892
    iget-object v5, v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v5, v5

    .line 2567893
    invoke-virtual {v5, v4}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2567894
    iget-object v4, v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, v4

    .line 2567895
    invoke-virtual {v4, v8}, Lcom/facebook/ui/search/SearchEditText;->setFocusable(Z)V

    .line 2567896
    invoke-virtual {v2}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->d()V

    .line 2567897
    new-instance v4, LX/ILL;

    invoke-direct {v4, p0}, LX/ILL;-><init>(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V

    invoke-virtual {v2, v4}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(Landroid/view/View$OnClickListener;)V

    .line 2567898
    invoke-interface {v1, v2}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2567899
    invoke-interface {v1, v7}, LX/1ZF;->k_(Z)V

    .line 2567900
    :cond_1
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x43406d3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2567901
    :cond_2
    invoke-interface {v1, v7}, LX/1ZF;->k_(Z)V

    .line 2567902
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->h:LX/5QS;

    invoke-static {v2}, LX/DKD;->b(LX/5QS;)I

    move-result v2

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2567882
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2567883
    invoke-static {p0}, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->c(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V

    .line 2567884
    return-void
.end method
