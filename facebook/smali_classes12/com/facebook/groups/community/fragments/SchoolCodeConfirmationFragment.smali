.class public Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/IMk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/text/BetterEditTextView;

.field public d:Lcom/facebook/fig/button/FigButton;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568225
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-static {p0}, LX/IMk;->a(LX/0QB;)LX/IMk;

    move-result-object v1

    check-cast v1, LX/IMk;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object v1, p1, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->a:LX/IMk;

    iput-object p0, p1, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->b:LX/0Zb;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2568226
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2568227
    const-class v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2568228
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568229
    const-string v1, "submitted_email"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->f:Ljava/lang/String;

    .line 2568230
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568231
    const-string v1, "group_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->g:Ljava/lang/String;

    .line 2568232
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x47d4d9c5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2568233
    const v0, 0x7f031268

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2568234
    const v0, 0x7f0d2b3f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2568235
    const v0, 0x7f0d2b40

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->c:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2568236
    const v0, 0x7f0d2b41

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->d:Lcom/facebook/fig/button/FigButton;

    .line 2568237
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->d:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v7}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2568238
    iget-object v3, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f083008    # 1.810244E38f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2568239
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->c:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v3, LX/ILa;

    invoke-direct {v3, p0}, LX/ILa;-><init>(Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2568240
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->d:Lcom/facebook/fig/button/FigButton;

    new-instance v3, LX/ILe;

    invoke-direct {v3, p0}, LX/ILe;-><init>(Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2568241
    const v0, 0x38f86dae

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2568242
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f083007

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->f:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x73cf8c36

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568243
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2568244
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2568245
    if-eqz v1, :cond_0

    .line 2568246
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2568247
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2db2df1c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
