.class public Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;
.super Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/9Lx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IQK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568158
    invoke-direct {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;

    const-class v1, LX/9Lx;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9Lx;

    const-class p0, LX/IQK;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/IQK;

    iput-object v1, p1, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;->a:LX/9Lx;

    iput-object v2, p1, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;->b:LX/IQK;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;)LX/B1W;
    .locals 2

    .prologue
    .line 2568177
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;->a:LX/9Lx;

    .line 2568178
    new-instance v1, LX/9Lw;

    const/16 p0, 0xe8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, p1, p0}, LX/9Lw;-><init>(Lcom/facebook/api/feedtype/FeedType;LX/0Ot;)V

    .line 2568179
    move-object v0, v1

    .line 2568180
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2568176
    const-string v0, "community_trending_stories"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2568173
    const-class v0, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2568174
    invoke-super {p0, p1}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2568175
    return-void
.end method

.method public final b()Lcom/facebook/api/feedtype/FeedType;
    .locals 3

    .prologue
    .line 2568166
    new-instance v0, LX/B1S;

    invoke-direct {v0}, LX/B1S;-><init>()V

    .line 2568167
    invoke-virtual {p0}, Lcom/facebook/groups/feed/ui/GroupsFeedSearchQueryFragment;->m()Ljava/lang/String;

    move-result-object v1

    .line 2568168
    iput-object v1, v0, LX/B1S;->a:Ljava/lang/String;

    .line 2568169
    move-object v1, v0

    .line 2568170
    sget-object v2, LX/B1T;->CommunityTrendingStoriesPosts:LX/B1T;

    .line 2568171
    iput-object v2, v1, LX/B1S;->b:LX/B1T;

    .line 2568172
    new-instance v1, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, LX/B1S;->a()Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->y:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v1
.end method

.method public final d()LX/DNC;
    .locals 11

    .prologue
    .line 2568160
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/CommunityTrendingStoriesFragment;->b:LX/IQK;

    .line 2568161
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->m:LX/DNR;

    move-object v1, v1

    .line 2568162
    iget-object v2, p0, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->l:LX/DNJ;

    move-object v2, v2

    .line 2568163
    new-instance v3, LX/IQJ;

    invoke-static {v0}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v6

    check-cast v6, LX/DOp;

    const-class v4, LX/IRJ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/IRJ;

    const/16 v4, 0x2431

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v9

    check-cast v9, LX/IVs;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    move-object v4, v1

    move-object v5, v2

    invoke-direct/range {v3 .. v10}, LX/IQJ;-><init>(LX/DNR;LX/DNJ;LX/DOp;LX/IRJ;LX/0Ot;LX/IVs;Landroid/content/Context;)V

    .line 2568164
    move-object v0, v3

    .line 2568165
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2568159
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08301b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
