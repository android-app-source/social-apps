.class public Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field public a:LX/IMk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/widget/text/BetterEditTextView;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Lcom/facebook/fig/button/FigButton;

.field public m:Landroid/view/View;

.field public n:LX/1ZF;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field public r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field public t:LX/IL2;

.field public u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2568332
    const-class v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2568333
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2568334
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2568335
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->b:LX/0Zb;

    invoke-interface {v0, p1, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2568336
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2568337
    const-string v1, "group_email_verification"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "group_id"

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "email_address"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "email_domains"

    const-string v2, ";"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2568338
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2568339
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2568340
    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2568341
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->f:LX/0Uh;

    const/16 v2, 0x39

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2568342
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->aT:LX/0ih;

    const-string v3, "did_submit_email"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2568343
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->a:LX/IMk;

    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    new-instance v4, LX/ILo;

    invoke-direct {v4, p0, v0, p1}, LX/ILo;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;LX/4At;Ljava/lang/String;)V

    invoke-virtual {v1, p1, v2, v3, v4}, LX/IMk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;LX/ILc;)V

    .line 2568344
    return-void
.end method

.method public static c(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V
    .locals 2

    .prologue
    .line 2568345
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    new-instance v1, LX/ILm;

    invoke-direct {v1, p0}, LX/ILm;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2568346
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2568347
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2568348
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-static {v0}, LX/IMk;->a(LX/0QB;)LX/IMk;

    move-result-object v5

    check-cast v5, LX/IMk;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v5, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->a:LX/IMk;

    iput-object v6, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->b:LX/0Zb;

    iput-object v7, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->c:LX/03V;

    iput-object v8, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->d:LX/17Y;

    iput-object v9, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->f:LX/0Uh;

    iput-object v0, v4, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->g:LX/0if;

    .line 2568349
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568350
    const-string v3, "group_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->o:Ljava/lang/String;

    .line 2568351
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568352
    const-string v3, "group_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->p:Ljava/lang/String;

    .line 2568353
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568354
    const-string v3, "school_domains"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    .line 2568355
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2568356
    const-string v3, "is_community_group"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->s:Z

    .line 2568357
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Group name can\'t be empty!"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2568358
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Supported school email domains can\'t be empty"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2568359
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->q:Ljava/lang/String;

    .line 2568360
    return-void

    :cond_0
    move v0, v2

    .line 2568361
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2568362
    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2a

    const v4, 0x1bdf0535

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2568363
    const v0, 0x7f031269

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2568364
    const v0, 0x7f0d2b42

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2568365
    const v0, 0x7f0d2b43

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2568366
    const v0, 0x7f0d2b44

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2568367
    const v0, 0x7f0d2b45

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->m:Landroid/view/View;

    .line 2568368
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->m:Landroid/view/View;

    new-instance v5, LX/ILg;

    invoke-direct {v5, p0}, LX/ILg;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2568369
    iget-object v5, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2568370
    const v0, 0x7f0d2b46

    invoke-static {v4, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->l:Lcom/facebook/fig/button/FigButton;

    .line 2568371
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2568372
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->l:Lcom/facebook/fig/button/FigButton;

    iget-object v5, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->f:LX/0Uh;

    const/16 v6, 0x39

    invoke-virtual {v5, v6, v1}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-nez v5, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2568373
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08300f

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->q:Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2568374
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2568375
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v5, LX/ILh;

    invoke-direct {v5, p0, v0}, LX/ILh;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2568376
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v2, LX/ILi;

    invoke-direct {v2, p0}, LX/ILi;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2568377
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    iget-boolean v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->s:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f083005

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->o:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2568378
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f08300a

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->o:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2568379
    iget-object v0, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->l:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/ILl;

    invoke-direct {v1, p0}, LX/ILl;-><init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2568380
    const v0, 0x459f1a1f

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-object v4

    :cond_1
    move v0, v2

    .line 2568381
    goto/16 :goto_0

    .line 2568382
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f083009

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->o:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2450215a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568383
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2568384
    invoke-static {p0}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->c(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    .line 2568385
    const/16 v1, 0x2b

    const v2, -0x2e886991

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c57abd7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568386
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2568387
    iget-boolean v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->u:Z

    if-nez v1, :cond_0

    .line 2568388
    const/4 v5, 0x1

    .line 2568389
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    .line 2568390
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    if-nez v1, :cond_1

    .line 2568391
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x249085fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2568392
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    invoke-interface {v1, v5}, LX/1ZF;->k_(Z)V

    .line 2568393
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->f:LX/0Uh;

    const/16 v2, 0x39

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2568394
    iget-object v1, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    const v2, 0x7f08300d

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2568395
    :goto_1
    iput-boolean v5, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->u:Z

    goto :goto_0

    .line 2568396
    :cond_2
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 2568397
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2568398
    const-string v4, "college_community_email_needs_confirmation"

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move v1, v1

    .line 2568399
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080031

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2568400
    :goto_2
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 2568401
    move-object v1, v2

    .line 2568402
    iput-boolean v5, v1, LX/108;->q:Z

    .line 2568403
    move-object v1, v1

    .line 2568404
    iput-boolean v5, v1, LX/108;->d:Z

    .line 2568405
    move-object v1, v1

    .line 2568406
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2568407
    iget-object v2, p0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->n:LX/1ZF;

    invoke-interface {v2, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2568408
    invoke-static {p0}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->c(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V

    goto :goto_1

    .line 2568409
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080029

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
