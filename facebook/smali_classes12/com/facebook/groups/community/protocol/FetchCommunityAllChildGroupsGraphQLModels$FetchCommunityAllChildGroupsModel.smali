.class public final Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x37971a41
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569377
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569376
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2569374
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2569375
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2569351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569352
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2569353
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2569354
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2569355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569356
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2569366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569367
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2569368
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    .line 2569369
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2569370
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    .line 2569371
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    .line 2569372
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569373
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2569365
    new-instance v0, LX/IMF;

    invoke-direct {v0, p1}, LX/IMF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2569378
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    .line 2569379
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2569363
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2569364
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2569362
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2569359
    new-instance v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;-><init>()V

    .line 2569360
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2569361
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2569358
    const v0, -0x244f9f2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2569357
    const v0, 0x41e065f

    return v0
.end method
