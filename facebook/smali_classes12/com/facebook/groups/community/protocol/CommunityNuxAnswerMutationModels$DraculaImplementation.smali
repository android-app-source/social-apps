.class public final Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2568815
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2568816
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2568813
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2568814
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2568854
    if-nez p1, :cond_0

    .line 2568855
    :goto_0
    return v0

    .line 2568856
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2568857
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2568858
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2568859
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2568860
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2568861
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2568862
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2568863
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2568864
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2568865
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x17dd84c8 -> :sswitch_1
        0x46bc2803 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2568853
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2568850
    sparse-switch p0, :sswitch_data_0

    .line 2568851
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2568852
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x17dd84c8 -> :sswitch_0
        0x46bc2803 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2568849
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2568847
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->b(I)V

    .line 2568848
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2568842
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2568843
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2568844
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a:LX/15i;

    .line 2568845
    iput p2, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->b:I

    .line 2568846
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2568866
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2568841
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2568838
    iget v0, p0, LX/1vt;->c:I

    .line 2568839
    move v0, v0

    .line 2568840
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2568835
    iget v0, p0, LX/1vt;->c:I

    .line 2568836
    move v0, v0

    .line 2568837
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2568832
    iget v0, p0, LX/1vt;->b:I

    .line 2568833
    move v0, v0

    .line 2568834
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568829
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2568830
    move-object v0, v0

    .line 2568831
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2568820
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2568821
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2568822
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2568823
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2568824
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2568825
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2568826
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2568827
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2568828
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2568817
    iget v0, p0, LX/1vt;->c:I

    .line 2568818
    move v0, v0

    .line 2568819
    return v0
.end method
