.class public final Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6542b7b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568812
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568809
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2568784
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2568785
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568810
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->e:Ljava/lang/String;

    .line 2568811
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommunityNuxAnswer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568799
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->f:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->f:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    .line 2568800
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->f:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2568801
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568802
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2568803
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2568804
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2568805
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2568806
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2568807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568808
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2568791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568792
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2568793
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    .line 2568794
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2568795
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;

    .line 2568796
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;->f:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    .line 2568797
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568798
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2568788
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel;-><init>()V

    .line 2568789
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2568790
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2568787
    const v0, -0x10ef2ffb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2568786
    const v0, -0x7fbee3d5

    return v0
.end method
