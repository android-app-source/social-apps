.class public final Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6fa39460
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568675
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568674
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2568672
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2568673
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 2568626
    iput-object p1, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2568627
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2568628
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2568629
    if-eqz v0, :cond_0

    .line 2568630
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2568631
    :cond_0
    return-void

    .line 2568632
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2568666
    iput-object p1, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->g:Ljava/lang/String;

    .line 2568667
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2568668
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2568669
    if-eqz v0, :cond_0

    .line 2568670
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2568671
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminAwareGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568664
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    .line 2568665
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568662
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->f:Ljava/lang/String;

    .line 2568663
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568660
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->g:Ljava/lang/String;

    .line 2568661
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568658
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2568659
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2568676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568677
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2568678
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2568679
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2568680
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2568681
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2568682
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2568683
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2568684
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2568685
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2568686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568687
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2568650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568651
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2568652
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    .line 2568653
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2568654
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    .line 2568655
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    .line 2568656
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568657
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2568649
    new-instance v0, LX/ILy;

    invoke-direct {v0, p1}, LX/ILy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568648
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2568638
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2568639
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2568640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2568641
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 2568642
    :goto_0
    return-void

    .line 2568643
    :cond_0
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2568644
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2568645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2568646
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2568647
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2568633
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2568634
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->a(Ljava/lang/String;)V

    .line 2568635
    :cond_0
    :goto_0
    return-void

    .line 2568636
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2568637
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2568623
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;-><init>()V

    .line 2568624
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2568625
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2568622
    const v0, 0x10666080

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2568621
    const v0, 0x41e065f

    return v0
.end method
