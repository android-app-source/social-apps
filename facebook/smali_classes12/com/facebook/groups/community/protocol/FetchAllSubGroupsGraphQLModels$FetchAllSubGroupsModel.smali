.class public final Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x220df6ba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569206
    const-class v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569205
    const-class v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2569180
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2569181
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2569199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569200
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2569201
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2569202
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2569203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569204
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2569191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569192
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2569193
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    .line 2569194
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2569195
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    .line 2569196
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    .line 2569197
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569198
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2569190
    new-instance v0, LX/IM9;

    invoke-direct {v0, p1}, LX/IM9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2569207
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    .line 2569208
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2569188
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2569189
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2569187
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2569184
    new-instance v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;-><init>()V

    .line 2569185
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2569186
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2569183
    const v0, 0x151daa5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2569182
    const v0, 0x41e065f

    return v0
.end method
