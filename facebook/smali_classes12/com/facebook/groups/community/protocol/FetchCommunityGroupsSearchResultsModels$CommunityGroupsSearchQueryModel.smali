.class public final Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2cb9adf3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569714
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569717
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2569715
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2569716
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2569693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569694
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2569695
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2569696
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2569697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569698
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2569706
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569707
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2569708
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    .line 2569709
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2569710
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    .line 2569711
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    .line 2569712
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569713
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2569718
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2569703
    new-instance v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;-><init>()V

    .line 2569704
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2569705
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2569702
    const v0, -0x2cb7c765

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2569701
    const v0, -0x1bce060e

    return v0
.end method

.method public final j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2569699
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    .line 2569700
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    return-object v0
.end method
