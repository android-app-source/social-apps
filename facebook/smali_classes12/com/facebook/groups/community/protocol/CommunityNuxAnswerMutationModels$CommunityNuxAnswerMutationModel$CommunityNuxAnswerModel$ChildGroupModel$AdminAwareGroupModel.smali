.class public final Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1aed4427
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568598
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568597
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2568595
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2568596
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupFriendMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2568593
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2568594
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMemberProfiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568591
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2568592
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568589
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    .line 2568590
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2568579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568580
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x46bc2803

    invoke-static {v1, v0, v2}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2568581
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x17dd84c8

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2568582
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2568583
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2568584
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2568585
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2568586
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2568587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568588
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2568563
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568564
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2568565
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x46bc2803

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2568566
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2568567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    .line 2568568
    iput v3, v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->e:I

    move-object v1, v0

    .line 2568569
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2568570
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x17dd84c8

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2568571
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2568572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    .line 2568573
    iput v3, v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->f:I

    move-object v1, v0

    .line 2568574
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568575
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2568576
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2568577
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2568578
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2568549
    new-instance v0, LX/ILx;

    invoke-direct {v0, p1}, LX/ILx;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568562
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2568558
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2568559
    const/4 v0, 0x0

    const v1, 0x46bc2803

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->e:I

    .line 2568560
    const/4 v0, 0x1

    const v1, -0x17dd84c8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;->f:I

    .line 2568561
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2568556
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2568557
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2568555
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2568552
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel$AdminAwareGroupModel;-><init>()V

    .line 2568553
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2568554
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2568551
    const v0, -0x556c5417

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2568550
    const v0, 0x41e065f

    return v0
.end method
