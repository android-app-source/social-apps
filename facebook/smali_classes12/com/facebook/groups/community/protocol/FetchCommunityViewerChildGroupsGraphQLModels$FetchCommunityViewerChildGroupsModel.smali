.class public final Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xb2b2154
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2570059
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2570058
    const-class v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2570056
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2570057
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2570050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2570051
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2570052
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2570053
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2570054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2570055
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2570042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2570043
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2570044
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    .line 2570045
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2570046
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    .line 2570047
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    .line 2570048
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2570049
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2570060
    new-instance v0, LX/IMd;

    invoke-direct {v0, p1}, LX/IMd;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2570040
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    .line 2570041
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;->e:Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel$ViewerChildGroupsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2570038
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2570039
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2570037
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2570034
    new-instance v0, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityViewerChildGroupsGraphQLModels$FetchCommunityViewerChildGroupsModel;-><init>()V

    .line 2570035
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2570036
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2570033
    const v0, 0x7c3e1d7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2570032
    const v0, 0x41e065f

    return v0
.end method
