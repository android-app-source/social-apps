.class public final Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4325c228
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568729
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2568728
    const-class v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2568726
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2568727
    return-void
.end method

.method private j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getChildGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568708
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    .line 2568709
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568724
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->f:Ljava/lang/String;

    .line 2568725
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2568730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568731
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2568732
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2568733
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2568734
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2568735
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2568736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568737
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2568716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2568717
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2568718
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    .line 2568719
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->j()Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2568720
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    .line 2568721
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->e:Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel$ChildGroupModel;

    .line 2568722
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2568723
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2568715
    invoke-direct {p0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2568712
    new-instance v0, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/CommunityNuxAnswerMutationModels$CommunityNuxAnswerMutationModel$CommunityNuxAnswerModel;-><init>()V

    .line 2568713
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2568714
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2568711
    const v0, -0x2d873ad6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2568710
    const v0, 0x6e19a8e6

    return v0
.end method
