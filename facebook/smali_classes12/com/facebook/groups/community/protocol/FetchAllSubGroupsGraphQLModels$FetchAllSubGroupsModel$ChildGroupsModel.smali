.class public final Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6890d00a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569105
    const-class v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2569106
    const-class v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2569107
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2569108
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2569109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569110
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2569111
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2569112
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2569113
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2569114
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2569115
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569116
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2569117
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->e:Ljava/util/List;

    .line 2569118
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2569119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2569120
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2569121
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2569122
    if-eqz v1, :cond_2

    .line 2569123
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    .line 2569124
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2569125
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2569126
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2569127
    invoke-virtual {p0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2569128
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    .line 2569129
    iput-object v0, v1, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2569130
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2569131
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2569132
    new-instance v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    invoke-direct {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;-><init>()V

    .line 2569133
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2569134
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2569135
    const v0, -0x697fa028    # -2.074E-25f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2569136
    const v0, 0x3dd0ccee

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2569137
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2569138
    iget-object v0, p0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method
