.class public Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/IL1;
.implements LX/IL2;
.implements LX/1ZF;


# instance fields
.field private p:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/ILF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/IL9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0h5;

.field public u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field public w:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

.field public x:I

.field private y:Ljava/lang/String;

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2567538
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2567539
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->x:I

    .line 2567540
    new-instance v0, LX/IKx;

    invoke-direct {v0, p0}, LX/IKx;-><init>(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;)V

    iput-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->z:Landroid/view/View$OnClickListener;

    .line 2567541
    return-void
.end method

.method private static a(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;LX/1Ck;LX/ILF;LX/IL9;LX/0if;)V
    .locals 0

    .prologue
    .line 2567537
    iput-object p1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->p:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->q:LX/ILF;

    iput-object p3, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->r:LX/IL9;

    iput-object p4, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->s:LX/0if;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v3}, LX/ILF;->b(LX/0QB;)LX/ILF;

    move-result-object v1

    check-cast v1, LX/ILF;

    invoke-static {v3}, LX/IL9;->b(LX/0QB;)LX/IL9;

    move-result-object v2

    check-cast v2, LX/IL9;

    invoke-static {v3}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->a(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;LX/1Ck;LX/ILF;LX/IL9;LX/0if;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2567542
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2567543
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    .line 2567544
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    invoke-interface {v0, v1}, LX/0h5;->setShowDividers(Z)V

    .line 2567545
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2567546
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    iget-object v1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->z:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2567547
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;Z)V
    .locals 2

    .prologue
    .line 2567548
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    iget-object v1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->z:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2567549
    if-eqz p1, :cond_0

    .line 2567550
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2567551
    :cond_0
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    .line 2567552
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->p:LX/1Ck;

    sget-object v1, LX/IL0;->FETCH_COMMUNITY_FORUM_GROUPS_TASK:LX/IL0;

    iget-object v2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->q:LX/ILF;

    iget-object v3, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->u:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x6

    invoke-virtual {v2, v3, v4, v5}, LX/ILF;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/IKy;

    invoke-direct {v3, p0}, LX/IKy;-><init>(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2567553
    return-void
.end method

.method private m()V
    .locals 7

    .prologue
    .line 2567554
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->u:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->v:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->w:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    .line 2567555
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2567556
    const-string v4, "community_id"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567557
    const-string v4, "community_name"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567558
    const-string v4, "community_forum_groups"

    invoke-static {v3, v4, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2567559
    new-instance v4, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    invoke-direct {v4}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;-><init>()V

    .line 2567560
    invoke-virtual {v4, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2567561
    move-object v1, v4

    .line 2567562
    iput-object p0, v1, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->o:LX/IL1;

    .line 2567563
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2567564
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f040056

    const v4, 0x7f040092

    const v5, 0x7f040055

    const v6, 0x7f040093

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0hH;->d(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const-class v1, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2567565
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 2567566
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02027e

    new-instance v2, LX/IKz;

    invoke-direct {v2, p0}, LX/IKz;-><init>(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;)V

    invoke-static {v0, v1, v2}, LX/99C;->a(Landroid/content/res/Resources;ILX/99A;)V

    .line 2567567
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 2567568
    const v0, 0x7f0d1899

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2567569
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2567570
    const v0, 0x7f0d09a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2567571
    iget v2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->x:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2567572
    iget v2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->x:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2567573
    :cond_0
    const v0, 0x7f0d09a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2567574
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->p()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2567575
    return-void
.end method

.method private p()Landroid/text/SpannableStringBuilder;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2567576
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083000

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->y:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2567577
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2567578
    iget-object v2, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2567579
    iget-object v3, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->y:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v0, v3

    .line 2567580
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2567581
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2567582
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2567583
    instance-of v0, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    if-eqz v0, :cond_0

    .line 2567584
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 2567585
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->o()V

    .line 2567586
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->b(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;Z)V

    .line 2567587
    return-void
.end method

.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 2567497
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2567498
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2567588
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2567589
    const v0, 0x7f0400c3

    const v1, 0x7f04007d

    invoke-virtual {p0, v0, v1}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->overridePendingTransition(II)V

    .line 2567590
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2567535
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2567536
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2567534
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2567519
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2567520
    invoke-static {p0, p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2567521
    const v0, 0x7f0309a5

    invoke-virtual {p0, v0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->setContentView(I)V

    .line 2567522
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "group_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->u:Ljava/lang/String;

    .line 2567523
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "group_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->v:Ljava/lang/String;

    .line 2567524
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->b()V

    .line 2567525
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2567526
    new-instance v1, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-direct {v1}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;-><init>()V

    .line 2567527
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2567528
    move-object v0, v1

    .line 2567529
    iput-object p0, v0, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->t:LX/IL2;

    .line 2567530
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2567531
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->l()V

    .line 2567532
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->aT:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2567533
    return-void
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 2567518
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2567510
    iput-object p1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->y:Ljava/lang/String;

    .line 2567511
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2567512
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "college_community_email_needs_confirmation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2567513
    invoke-virtual {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->finish()V

    .line 2567514
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->t:LX/0h5;

    iget-object v1, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->z:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2567515
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->n()V

    .line 2567516
    return-void

    .line 2567517
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->m()V

    goto :goto_0
.end method

.method public final f()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2567509
    const/4 v0, 0x0

    return-object v0
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 2567503
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->setResult(ILandroid/content/Intent;)V

    .line 2567504
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2567505
    iget-object v0, p0, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->aT:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2567506
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2567507
    const v0, 0x7f04007d

    const v1, 0x7f0400c4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->overridePendingTransition(II)V

    .line 2567508
    return-void
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2567502
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 2567501
    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2567500
    return-void
.end method

.method public final x_(I)V
    .locals 0

    .prologue
    .line 2567499
    return-void
.end method
