.class public final Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/J7U;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2650892
    new-instance v0, LX/J7T;

    invoke-direct {v0}, LX/J7T;-><init>()V

    sput-object v0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2650893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650894
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/J7U;->valueOf(Ljava/lang/String;)LX/J7U;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->a:LX/J7U;

    .line 2650895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->b:Ljava/lang/String;

    .line 2650896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->c:Ljava/lang/String;

    .line 2650897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->d:Ljava/lang/String;

    .line 2650898
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->e:Ljava/lang/String;

    .line 2650899
    const-class v0, LX/J7V;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->f:LX/0P1;

    .line 2650900
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2650901
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2650902
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->a:LX/J7U;

    invoke-virtual {v0}, LX/J7U;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650903
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650904
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650905
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650906
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650907
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->f:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 2650908
    return-void
.end method
