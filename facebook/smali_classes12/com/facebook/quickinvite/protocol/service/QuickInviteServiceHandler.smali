.class public Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/J7V;


# direct methods
.method public constructor <init>(LX/18V;LX/J7V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650946
    iput-object p1, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->a:LX/18V;

    .line 2650947
    iput-object p2, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->b:LX/J7V;

    .line 2650948
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;
    .locals 5

    .prologue
    .line 2650949
    const-class v1, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;

    monitor-enter v1

    .line 2650950
    :try_start_0
    sget-object v0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2650951
    sput-object v2, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2650952
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2650953
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2650954
    new-instance p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    .line 2650955
    new-instance v4, LX/J7V;

    invoke-direct {v4}, LX/J7V;-><init>()V

    .line 2650956
    move-object v4, v4

    .line 2650957
    move-object v4, v4

    .line 2650958
    check-cast v4, LX/J7V;

    invoke-direct {p0, v3, v4}, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;-><init>(LX/18V;LX/J7V;)V

    .line 2650959
    move-object v0, p0

    .line 2650960
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2650961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2650962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2650963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2650964
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2650965
    const-string v1, "quickinvite_send_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2650966
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650967
    const-string v1, "sendInviteMethodParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;

    .line 2650968
    :try_start_0
    iget-object v1, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->a:LX/18V;

    iget-object v2, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->b:LX/J7V;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2650969
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2650970
    :goto_0
    move-object v0, v0

    .line 2650971
    :goto_1
    return-object v0

    .line 2650972
    :cond_0
    const-string v1, "quickinvite_send_batch_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2650973
    const/4 v1, 0x0

    .line 2650974
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650975
    const-string v2, "sendBatchInviteParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2650976
    iget-object v0, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->a:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v3

    move v0, v1

    .line 2650977
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 2650978
    iget-object v4, p0, Lcom/facebook/quickinvite/protocol/service/QuickInviteServiceHandler;->b:LX/J7V;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "batch-invite-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2650979
    iput-object v5, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 2650980
    move-object v4, v4

    .line 2650981
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v3, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 2650982
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2650983
    :cond_1
    :try_start_1
    const-string v0, "batchInvite"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-interface {v3, v0, v4}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2650984
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2650985
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "batch-invite-"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, LX/2VK;->b(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    .line 2650986
    if-eqz v0, :cond_3

    .line 2650987
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2650988
    :goto_4
    move-object v0, v0

    .line 2650989
    goto :goto_1

    .line 2650990
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2650991
    :catch_0
    move-exception v0

    .line 2650992
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2650993
    :catch_1
    move-exception v0

    .line 2650994
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_4

    .line 2650995
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2650996
    :cond_4
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2650997
    goto :goto_4
.end method
