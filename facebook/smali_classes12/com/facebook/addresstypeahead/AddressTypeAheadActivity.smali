.class public Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/HiY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

.field private r:LX/HiX;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2497260
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;

    const-class v1, LX/HiY;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/HiY;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->p:LX/HiY;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2497232
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2497233
    invoke-static {p0, p0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2497234
    const v0, 0x7f0300a9

    invoke-virtual {p0, v0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->setContentView(I)V

    .line 2497235
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->setRequestedOrientation(I)V

    .line 2497236
    const v0, 0x7f0d04b1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    .line 2497237
    invoke-virtual {p0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2497238
    iget-object v2, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    const-string v0, "current_location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 2497239
    iput-object v0, v2, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->t:Landroid/location/Location;

    .line 2497240
    iget-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    const-string v2, "is_using_google_api"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->setIsUsingGoogleApi(Z)V

    .line 2497241
    iget-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    const-string v2, "product_tag"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2497242
    iput-object v2, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->u:Ljava/lang/String;

    .line 2497243
    iget-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->p:LX/HiY;

    iget-object v2, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    .line 2497244
    new-instance v3, LX/HiX;

    .line 2497245
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p1, LX/HiZ;

    invoke-direct {p1, v0}, LX/HiZ;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 2497246
    invoke-direct {v3, v4, v2}, LX/HiX;-><init>(Ljava/util/Set;Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    .line 2497247
    move-object v0, v3

    .line 2497248
    iput-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->r:LX/HiX;

    .line 2497249
    iget-object v2, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->r:LX/HiX;

    const-string v0, "result_handler_params"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2497250
    iput-object v0, v2, LX/HiX;->d:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2497251
    iget-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->r:LX/HiX;

    new-instance v1, LX/HiJ;

    invoke-direct {v1, p0}, LX/HiJ;-><init>(Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;)V

    .line 2497252
    iput-object v1, v0, LX/HiX;->c:LX/HiJ;

    .line 2497253
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2497254
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2497255
    iget-object v0, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b()V

    .line 2497256
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5ec405e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2497257
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2497258
    iget-object v1, p0, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->q:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c()V

    .line 2497259
    const/16 v1, 0x23

    const v2, 0x77b773f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
