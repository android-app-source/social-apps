.class public Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field public a:LX/HiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HiK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HiN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Hio;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:LX/Hit;

.field private final j:LX/Hiu;

.field private k:Landroid/os/Handler;

.field private l:Landroid/support/v7/widget/SearchView;

.field private m:Lcom/facebook/fbui/glyph/GlyphView;

.field private n:Landroid/support/v7/widget/RecyclerView;

.field private o:Lcom/facebook/widget/text/BetterTextView;

.field private p:Landroid/widget/ProgressBar;

.field private q:Landroid/widget/ProgressBar;

.field public r:Z

.field public s:LX/Hin;

.field public t:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/HiW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2498171
    const-class v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2498172
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2498173
    new-instance v0, LX/Hit;

    invoke-direct {v0, p0}, LX/Hit;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i:LX/Hit;

    .line 2498174
    new-instance v0, LX/Hiu;

    invoke-direct {v0, p0}, LX/Hiu;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->j:LX/Hiu;

    .line 2498175
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e()V

    .line 2498176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498177
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2498178
    new-instance v0, LX/Hit;

    invoke-direct {v0, p0}, LX/Hit;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i:LX/Hit;

    .line 2498179
    new-instance v0, LX/Hiu;

    invoke-direct {v0, p0}, LX/Hiu;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->j:LX/Hiu;

    .line 2498180
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e()V

    .line 2498181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498182
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2498183
    new-instance v0, LX/Hit;

    invoke-direct {v0, p0}, LX/Hit;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i:LX/Hit;

    .line 2498184
    new-instance v0, LX/Hiu;

    invoke-direct {v0, p0}, LX/Hiu;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->j:LX/Hiu;

    .line 2498185
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e()V

    .line 2498186
    return-void
.end method

.method private static a(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;LX/HiS;LX/1Ck;LX/03V;Landroid/view/inputmethod/InputMethodManager;LX/HiK;LX/HiN;LX/Hio;)V
    .locals 0

    .prologue
    .line 2498187
    iput-object p1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a:LX/HiS;

    iput-object p2, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b:LX/1Ck;

    iput-object p3, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c:LX/03V;

    iput-object p4, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->d:Landroid/view/inputmethod/InputMethodManager;

    iput-object p5, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e:LX/HiK;

    iput-object p6, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->f:LX/HiN;

    iput-object p7, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->g:LX/Hio;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v7}, LX/HiS;->b(LX/0QB;)LX/HiS;

    move-result-object v1

    check-cast v1, LX/HiS;

    invoke-static {v7}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v7}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    new-instance v6, LX/HiK;

    invoke-static {v7}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {v6, v5}, LX/HiK;-><init>(LX/0Zb;)V

    move-object v5, v6

    check-cast v5, LX/HiK;

    invoke-static {v7}, LX/HiN;->b(LX/0QB;)LX/HiN;

    move-result-object v6

    check-cast v6, LX/HiN;

    const-class v8, LX/Hio;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Hio;

    invoke-static/range {v0 .. v7}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;LX/HiS;LX/1Ck;LX/03V;Landroid/view/inputmethod/InputMethodManager;LX/HiK;LX/HiN;LX/Hio;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V
    .locals 7

    .prologue
    .line 2498188
    invoke-virtual {p1}, Landroid/location/Address;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "google_place_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2498189
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498190
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c:LX/03V;

    sget-object v1, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h:Ljava/lang/String;

    const-string v2, "Can\'t get Google Place id."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498191
    :goto_0
    return-void

    .line 2498192
    :cond_0
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->k()V

    .line 2498193
    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b:LX/1Ck;

    sget-object v2, LX/Hiz;->FETCH_ADDRESS_DETAIL:LX/Hiz;

    iget-object v3, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a:LX/HiS;

    .line 2498194
    iget-object v4, v3, LX/HiS;->b:LX/1wc;

    sget-object v5, LX/JF3;->c:LX/2vs;

    invoke-virtual {v4, v5}, LX/1wc;->a(LX/2vs;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HiR;

    invoke-direct {v5, v3, v0}, LX/HiR;-><init>(LX/HiS;Ljava/lang/String;)V

    iget-object v6, v3, LX/HiS;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2498195
    new-instance v3, LX/Hiy;

    invoke-direct {v3, p0, p1}, LX/Hiy;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 2498196
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->j()V

    .line 2498197
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b:LX/1Ck;

    sget-object v1, LX/Hiz;->FETCH_ADDRESS_SUGGESTIONS:LX/Hiz;

    iget-object v2, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a:LX/HiS;

    iget-object v3, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->t:Landroid/location/Location;

    const/16 v4, 0xa

    iget-boolean v5, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    .line 2498198
    if-eqz v5, :cond_0

    .line 2498199
    if-nez v3, :cond_2

    sget-object v7, LX/HiS;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 2498200
    :goto_0
    iget-object v8, v2, LX/HiS;->b:LX/1wc;

    sget-object v9, LX/JF3;->c:LX/2vs;

    invoke-virtual {v8, v9}, LX/1wc;->a(LX/2vs;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v9, LX/HiQ;

    invoke-direct {v9, v2, p1, v7}, LX/HiQ;-><init>(LX/HiS;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    iget-object v7, v2, LX/HiS;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8, v9, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v6, v7

    .line 2498201
    :goto_1
    move-object v2, v6

    .line 2498202
    new-instance v3, LX/Hix;

    invoke-direct {v3, p0}, LX/Hix;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2498203
    return-void

    .line 2498204
    :cond_0
    new-instance v7, LX/4DK;

    invoke-direct {v7}, LX/4DK;-><init>()V

    invoke-virtual {v7, p1}, LX/4DK;->a(Ljava/lang/String;)LX/4DK;

    move-result-object v7

    .line 2498205
    if-eqz v3, :cond_1

    .line 2498206
    new-instance v8, LX/3Aj;

    invoke-direct {v8}, LX/3Aj;-><init>()V

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v8

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4DK;->a(LX/3Aj;)LX/4DK;

    .line 2498207
    :cond_1
    const-string v8, "STREET_PLACE_TYPEAHEAD"

    invoke-virtual {v7, v8}, LX/4DK;->c(Ljava/lang/String;)LX/4DK;

    move-result-object v8

    const-string v9, "HERE_THRIFT"

    invoke-virtual {v8, v9}, LX/4DK;->b(Ljava/lang/String;)LX/4DK;

    move-result-object v8

    const-string v9, "MESSENGER_TRANSPORTATION_ANDROID"

    invoke-virtual {v8, v9}, LX/4DK;->f(Ljava/lang/String;)LX/4DK;

    move-result-object v8

    const-string v9, "INTERLEAVE"

    .line 2498208
    const-string v10, "result_ordering"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498209
    move-object v8, v8

    .line 2498210
    const-string v9, "STRING_MATCH"

    .line 2498211
    const-string v10, "integration_strategy"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498212
    new-instance v8, LX/Hia;

    invoke-direct {v8}, LX/Hia;-><init>()V

    move-object v8, v8

    .line 2498213
    const-string v9, "address"

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    const-string v8, "limit"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/Hia;

    .line 2498214
    iget-object v8, v2, LX/HiS;->d:LX/0tX;

    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    new-instance v8, LX/HiP;

    invoke-direct {v8, v2}, LX/HiP;-><init>(LX/HiS;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v9

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v6, v7

    .line 2498215
    goto/16 :goto_1

    .line 2498216
    :cond_2
    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()LX/7c9;

    move-result-object v7

    new-instance v8, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v9

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v11

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v7, v8}, LX/7c9;->a(Lcom/google/android/gms/maps/model/LatLng;)LX/7c9;

    move-result-object v7

    invoke-virtual {v7}, LX/7c9;->a()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v7

    goto/16 :goto_0
.end method

.method public static b$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V
    .locals 2

    .prologue
    .line 2498217
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    invoke-virtual {v0}, LX/Hin;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498218
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->f:LX/HiN;

    sget-object v1, LX/HiM;->RECENT:LX/HiM;

    invoke-virtual {v0, p1, v1}, LX/HiN;->a(Landroid/location/Address;LX/HiM;)V

    .line 2498219
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2498220
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    .line 2498221
    iget-boolean v1, v0, LX/Hin;->e:Z

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eq v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2498222
    if-eqz v0, :cond_0

    .line 2498223
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    invoke-virtual {v1, p1}, LX/Hin;->b(Ljava/lang/String;)LX/1OM;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2498224
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2498225
    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->v:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->v:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2498226
    :goto_1
    return-void

    .line 2498227
    :cond_1
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i()V

    .line 2498228
    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->v:Ljava/lang/String;

    .line 2498229
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2498230
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    .line 2498231
    invoke-static {v0}, LX/Hin;->c(LX/Hin;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/Hin;->d(LX/Hin;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_2
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2498232
    if-eqz v0, :cond_3

    .line 2498233
    invoke-static {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    goto :goto_1

    .line 2498234
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083a2b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V

    goto :goto_1

    .line 2498235
    :cond_4
    new-instance v1, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView$7;

    invoke-direct {v1, p0, v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView$7;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->x:Ljava/lang/Runnable;

    .line 2498236
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->x:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    const v4, 0x2c4f1a7a

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static c$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2498237
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2498238
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2498239
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2498240
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->o:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2498241
    return-void
.end method

.method private e()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2498154
    const-class v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v0, p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2498155
    const v0, 0x7f0300ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2498156
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->setOrientation(I)V

    .line 2498157
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->k:Landroid/os/Handler;

    .line 2498158
    const v0, 0x7f0d04b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    .line 2498159
    const v0, 0x7f0d04b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2498160
    const v0, 0x7f0d04b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    .line 2498161
    const v0, 0x7f0d04b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 2498162
    const v0, 0x7f0d04b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->p:Landroid/widget/ProgressBar;

    .line 2498163
    const v0, 0x7f0d04b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->q:Landroid/widget/ProgressBar;

    .line 2498164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    .line 2498165
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h()V

    .line 2498166
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->f()V

    .line 2498167
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->g()V

    .line 2498168
    return-void
.end method

.method private f()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2498092
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a29

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2498093
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 2498094
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    new-instance v1, LX/Hiv;

    invoke-direct {v1, p0}, LX/Hiv;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    .line 2498095
    iput-object v1, v0, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2498096
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    const v1, 0x7f0d0352

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2498097
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2498098
    return-void
.end method

.method private g()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2498169
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Hiw;

    invoke-direct {v1, p0}, LX/Hiw;-><init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2498170
    return-void
.end method

.method public static getInputString(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2498091
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2498099
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2498100
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2498101
    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->g:LX/Hio;

    iget-object v2, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->j:LX/Hiu;

    iget-object v3, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i:LX/Hit;

    .line 2498102
    new-instance v4, LX/Hin;

    const-class v5, Landroid/content/Context;

    invoke-interface {v1, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    .line 2498103
    new-instance v7, LX/His;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-direct {v7, v6}, LX/His;-><init>(Landroid/view/LayoutInflater;)V

    .line 2498104
    move-object v6, v7

    .line 2498105
    check-cast v6, LX/His;

    invoke-static {v1}, LX/HiN;->b(LX/0QB;)LX/HiN;

    move-result-object v7

    check-cast v7, LX/HiN;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    move-object v9, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/Hin;-><init>(Landroid/content/Context;LX/His;LX/HiN;LX/0Uh;LX/Hiu;LX/Hit;)V

    .line 2498106
    move-object v1, v4

    .line 2498107
    iput-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    .line 2498108
    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2498109
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    const-string v2, ""

    invoke-virtual {v1, v2}, LX/Hin;->b(Ljava/lang/String;)LX/1OM;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2498110
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    invoke-virtual {v0}, LX/Hin;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498111
    const-string v0, ""

    invoke-static {p0, v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V

    .line 2498112
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2498113
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->x:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2498114
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->x:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2498115
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2498116
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2498117
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2498118
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->p:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2498119
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2498120
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->q:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2498121
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2498122
    return-void
.end method

.method public static l(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V
    .locals 2

    .prologue
    .line 2498123
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2498124
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->q:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2498125
    return-void
.end method

.method public static m(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2498126
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2498127
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2498128
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->n:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2498129
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2498130
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->d:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->l:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2498131
    return-void
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 2498132
    iget-object v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e:LX/HiK;

    invoke-static {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getInputString(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "back_button_pressed"

    iget-boolean v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    if-eqz v0, :cond_0

    const-string v0, "google"

    :goto_0
    iget-object v4, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->u:Ljava/lang/String;

    .line 2498133
    const-string v5, "address_typeahead_drop"

    invoke-static {v1, v5}, LX/HiK;->a(LX/HiK;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    .line 2498134
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-nez v6, :cond_1

    .line 2498135
    :goto_1
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i()V

    .line 2498136
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2498137
    return-void

    .line 2498138
    :cond_0
    const-string v0, "here_thrift"

    goto :goto_0

    .line 2498139
    :cond_1
    const-string v6, "input_string"

    invoke-virtual {v5, v6, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "drop_type"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "product_tag"

    invoke-virtual {v5, v6, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "ta_provider"

    invoke-virtual {v5, v6, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2498140
    invoke-direct {p0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->i()V

    .line 2498141
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2498142
    return-void
.end method

.method public setAddressSelectedListener(LX/HiW;)V
    .locals 0

    .prologue
    .line 2498143
    iput-object p1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    .line 2498144
    return-void
.end method

.method public setCurrentLocation(Landroid/location/Location;)V
    .locals 0
    .param p1    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498145
    iput-object p1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->t:Landroid/location/Location;

    .line 2498146
    return-void
.end method

.method public setIsUsingGoogleApi(Z)V
    .locals 2

    .prologue
    .line 2498147
    iput-boolean p1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    .line 2498148
    iget-object v0, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    iget-boolean v1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    .line 2498149
    iget-object p0, v0, LX/Hin;->a:LX/His;

    .line 2498150
    iput-boolean v1, p0, LX/His;->c:Z

    .line 2498151
    return-void
.end method

.method public setProductTag(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2498152
    iput-object p1, p0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->u:Ljava/lang/String;

    .line 2498153
    return-void
.end method
