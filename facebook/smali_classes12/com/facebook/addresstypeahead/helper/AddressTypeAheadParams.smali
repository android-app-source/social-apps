.class public Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2497424
    new-instance v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2497425
    new-instance v1, LX/HiU;

    invoke-direct {v1}, LX/HiU;-><init>()V

    move-object v1, v1

    .line 2497426
    invoke-direct {v0, v1}, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;-><init>(LX/HiU;)V

    sput-object v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->a:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2497427
    new-instance v0, LX/HiT;

    invoke-direct {v0}, LX/HiT;-><init>()V

    sput-object v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/HiU;)V
    .locals 1

    .prologue
    .line 2497436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497437
    iget-object v0, p1, LX/HiU;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->b:Ljava/lang/String;

    .line 2497438
    iget-object v0, p1, LX/HiU;->b:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->c:Landroid/os/Bundle;

    .line 2497439
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2497432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497433
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->b:Ljava/lang/String;

    .line 2497434
    const-class v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->c:Landroid/os/Bundle;

    .line 2497435
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2497431
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2497428
    iget-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2497429
    iget-object v0, p0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2497430
    return-void
.end method
