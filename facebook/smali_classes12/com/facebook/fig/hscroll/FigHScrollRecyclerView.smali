.class public Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;
.super Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
.source ""


# static fields
.field private static m:Landroid/util/SparseIntArray;


# instance fields
.field private n:I

.field public o:I

.field public p:I

.field private q:F

.field private r:Z

.field private s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2549509
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2549510
    sput-object v0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x0

    const v2, 0x7f0e0bd3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2549511
    sget-object v0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0e0bd4

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2549512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2549552
    invoke-direct {p0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;)V

    .line 2549553
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    .line 2549554
    invoke-direct {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->m()V

    .line 2549555
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2549546
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2549547
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    .line 2549548
    invoke-direct {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->m()V

    .line 2549549
    if-eqz p2, :cond_0

    .line 2549550
    invoke-direct {p0, p2}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->a(Landroid/util/AttributeSet;)V

    .line 2549551
    :cond_0
    return-void
.end method

.method private a(I[I)V
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2549539
    invoke-virtual {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2549540
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->o:I

    .line 2549541
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->p:I

    .line 2549542
    const/16 v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->q:F

    .line 2549543
    new-instance v1, LX/ICY;

    invoke-direct {v1, p0}, LX/ICY;-><init>(Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2549544
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2549545
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2549535
    invoke-virtual {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigHScrollRecyclerView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2549536
    const/16 v1, 0x0

    iget v2, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    .line 2549537
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2549538
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2549532
    const/4 v0, 0x1

    .line 2549533
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2549534
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2549528
    iget-boolean v0, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->r:Z

    if-nez v0, :cond_0

    .line 2549529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->r:Z

    .line 2549530
    sget-object v0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->m:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    sget-object v1, LX/03r;->FigHScrollRecyclerViewAttrs:[I

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->a(I[I)V

    .line 2549531
    :cond_0
    return-void
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2

    .prologue
    .line 2549527
    if-eqz p1, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 2549526
    new-instance v0, LX/1a3;

    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n:I

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/1a3;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 2549525
    new-instance v0, LX/1a3;

    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n:I

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/1a3;-><init>(II)V

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2549520
    invoke-virtual {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2549521
    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->o:I

    shl-int/lit8 v1, v1, 0x1

    sub-int v1, v0, v1

    iget v2, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->p:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->q:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n:I

    .line 2549522
    iget v1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n:I

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 2549523
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->onMeasure(II)V

    .line 2549524
    return-void
.end method

.method public setAdapter(LX/1OM;)V
    .locals 0

    .prologue
    .line 2549517
    invoke-direct {p0}, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->n()V

    .line 2549518
    invoke-super {p0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setAdapter(LX/1OM;)V

    .line 2549519
    return-void
.end method

.method public setType(I)V
    .locals 2

    .prologue
    .line 2549513
    iget-boolean v0, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->r:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "setType can only called before setAdapter"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2549514
    iput p1, p0, Lcom/facebook/fig/hscroll/FigHScrollRecyclerView;->s:I

    .line 2549515
    return-void

    .line 2549516
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
