.class public final Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a74fafc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2651736
    const-class v0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2651735
    const-class v0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2651733
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2651734
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2651712
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->e:Ljava/lang/String;

    .line 2651713
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2651731
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->f:Ljava/lang/String;

    .line 2651732
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2651719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2651720
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2651721
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2651722
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2651723
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2651724
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2651725
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2651726
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2651727
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2651728
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2651729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2651730
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2651737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2651738
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2651739
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2651740
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2651741
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;

    .line 2651742
    iput-object v0, v1, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->g:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2651743
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2651744
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2651717
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->g:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->g:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2651718
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->g:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2651714
    new-instance v0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;-><init>()V

    .line 2651715
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2651716
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2651711
    const v0, -0x7bf354b1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2651710
    const v0, 0x7fb9e89b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2651708
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->h:Ljava/lang/String;

    .line 2651709
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel$NodesModel;->h:Ljava/lang/String;

    return-object v0
.end method
