.class public Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J7f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/J8C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Lcom/facebook/resources/ui/FbRadioButton;

.field private l:Lcom/facebook/resources/ui/FbRadioButton;

.field private m:Lcom/facebook/resources/ui/FbRadioButton;

.field private n:Lcom/facebook/resources/ui/FbRadioButton;

.field private o:Lcom/facebook/resources/ui/FbRadioButton;

.field public p:LX/0g8;

.field private q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private r:LX/J7b;

.field public s:LX/1Qq;

.field private t:LX/J82;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2651247
    const-class v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;

    sput-object v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2651248
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2651249
    return-void
.end method

.method private static a(Landroid/view/View;)LX/0g8;
    .locals 2

    .prologue
    .line 2651240
    const v0, 0x7f0d2dcf    # 1.87659E38f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2651241
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2651242
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    return-object v1
.end method

.method private a(LX/J8B;)LX/1Qq;
    .locals 3

    .prologue
    .line 2651250
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->d:LX/1DS;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->e:LX/0Ot;

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->t:LX/J82;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 2651251
    iput-object p1, v0, LX/1Ql;->f:LX/1PW;

    .line 2651252
    move-object v0, v0

    .line 2651253
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;LX/03V;LX/J7f;LX/1Db;LX/1DS;LX/0Ot;Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;LX/J8C;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/J7f;",
            "LX/1Db;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;",
            "LX/J8C;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2651254
    iput-object p1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->b:LX/J7f;

    iput-object p3, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->c:LX/1Db;

    iput-object p4, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->d:LX/1DS;

    iput-object p5, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    iput-object p7, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->g:LX/J8C;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v7}, LX/J7f;->b(LX/0QB;)LX/J7f;

    move-result-object v2

    check-cast v2, LX/J7f;

    invoke-static {v7}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v3

    check-cast v3, LX/1Db;

    invoke-static {v7}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v4

    check-cast v4, LX/1DS;

    const/16 v5, 0x6bd

    invoke-static {v7, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v7}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    const-class v8, LX/J8C;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/J8C;

    invoke-static/range {v0 .. v7}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;LX/03V;LX/J7f;LX/1Db;LX/1DS;LX/0Ot;Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;LX/J8C;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;LX/ICR;)V
    .locals 7

    .prologue
    .line 2651255
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->e()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v2}, LX/J7b;->d()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->t:LX/J82;

    .line 2651256
    iget-object v4, v3, LX/J82;->b:Ljava/util/List;

    move-object v3, v4

    .line 2651257
    invoke-virtual {p1}, LX/ICR;->toEventName()Ljava/lang/String;

    move-result-object v4

    .line 2651258
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "count"

    invoke-virtual {v5, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "final_count"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "selection"

    invoke-virtual {p1}, LX/ICR;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2651259
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2651260
    const-string v6, "first_story_tracking_data"

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2651261
    :cond_0
    iget-object v4, v0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b:LX/0Zb;

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2651262
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->d()V

    .line 2651263
    return-void
.end method

.method private b()V
    .locals 0

    .prologue
    .line 2651264
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->c()V

    .line 2651265
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->e()V

    .line 2651266
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2651153
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->k:Lcom/facebook/resources/ui/FbRadioButton;

    new-instance v1, LX/J7h;

    invoke-direct {v1, p0}, LX/J7h;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651154
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->l:Lcom/facebook/resources/ui/FbRadioButton;

    new-instance v1, LX/J7i;

    invoke-direct {v1, p0}, LX/J7i;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651155
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->m:Lcom/facebook/resources/ui/FbRadioButton;

    new-instance v1, LX/J7j;

    invoke-direct {v1, p0}, LX/J7j;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651156
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->n:Lcom/facebook/resources/ui/FbRadioButton;

    new-instance v1, LX/J7k;

    invoke-direct {v1, p0}, LX/J7k;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651157
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->o:Lcom/facebook/resources/ui/FbRadioButton;

    new-instance v1, LX/J7l;

    invoke-direct {v1, p0}, LX/J7l;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651158
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2651267
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    if-eqz v0, :cond_0

    .line 2651268
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v0}, LX/J7b;->b()V

    .line 2651269
    :goto_0
    return-void

    .line 2651270
    :cond_0
    sget-object v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->h:Ljava/lang/Class;

    const-string v1, "Fail to initialize story gallery survey controller"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2651271
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    new-instance v1, LX/J7m;

    invoke-direct {v1, p0}, LX/J7m;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/62F;)V

    .line 2651272
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->c:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2651273
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 2651274
    return-void
.end method

.method public static k(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2651275
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2651276
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2651277
    return-void
.end method

.method public static l(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V
    .locals 2

    .prologue
    .line 2651243
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->i:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2651244
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->n()V

    .line 2651245
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p()V

    .line 2651246
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2651152
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->e()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2651159
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->q()V

    .line 2651160
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    if-nez v0, :cond_0

    .line 2651161
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->o()LX/J8B;

    move-result-object v0

    .line 2651162
    invoke-direct {p0, v0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a(LX/J8B;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    .line 2651163
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2651164
    :goto_0
    return-void

    .line 2651165
    :cond_0
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private o()LX/J8B;
    .locals 4

    .prologue
    .line 2651166
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->g:LX/J8C;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2651167
    sget-object v2, LX/J8D;->a:LX/J8D;

    move-object v2, v2

    .line 2651168
    new-instance v3, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment$8;

    invoke-direct {v3, p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment$8;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/J8C;->a(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;)LX/J8B;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2651169
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->k:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2651170
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->l:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2651171
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->m:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2651172
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->n:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2651173
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->o:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2651174
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2651175
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->t:LX/J82;

    if-nez v0, :cond_0

    .line 2651176
    new-instance v0, LX/J82;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->c()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J82;-><init>(LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->t:LX/J82;

    .line 2651177
    :goto_0
    return-void

    .line 2651178
    :cond_0
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->t:LX/J82;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J82;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public static r(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V
    .locals 4

    .prologue
    .line 2651179
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p()V

    .line 2651180
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;

    .line 2651181
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v2, LX/ICQ;->FINISH:LX/ICQ;

    .line 2651182
    iget-object v3, v0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->p:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    move-object v0, v3

    .line 2651183
    invoke-virtual {v1, v2, v0}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    .line 2651184
    new-instance v0, LX/J84;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J84;-><init>(Landroid/content/Context;)V

    .line 2651185
    new-instance v1, LX/J7o;

    invoke-direct {v1, p0}, LX/J7o;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    .line 2651186
    iput-object v1, v0, LX/J84;->a:LX/J7n;

    .line 2651187
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2651188
    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2651189
    return-void
.end method

.method public static s(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V
    .locals 1

    .prologue
    .line 2651190
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2651191
    if-eqz v0, :cond_0

    .line 2651192
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 2651193
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2651194
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2651195
    const-class v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;

    invoke-static {v0, p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2651196
    new-instance v0, LX/J7b;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a:LX/03V;

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->b:LX/J7f;

    invoke-direct {v0, v1, v2}, LX/J7b;-><init>(LX/03V;LX/J7f;)V

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    .line 2651197
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    const/4 v1, 0x1

    .line 2651198
    iput v1, v0, LX/J7b;->h:I

    .line 2651199
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x48e51bce

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2651200
    const v0, 0x7f0313d9

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2651201
    invoke-static {v2}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->a(Landroid/view/View;)LX/0g8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    .line 2651202
    const v0, 0x7f0d2dce

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2651203
    const v0, 0x7f0313d3

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2651204
    const v0, 0x7f0d2db8

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->i:Landroid/widget/TextView;

    .line 2651205
    const v0, 0x7f0d2dba

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->j:Landroid/widget/LinearLayout;

    .line 2651206
    const v0, 0x7f0d2dc3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->k:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651207
    const v0, 0x7f0d2dc1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->l:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651208
    const v0, 0x7f0d2dbf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->m:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651209
    const v0, 0x7f0d2dbd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->n:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651210
    const v0, 0x7f0d2dbb

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->o:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651211
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 2651212
    invoke-direct {p0}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->b()V

    .line 2651213
    const/16 v0, 0x2b

    const v3, -0x228054c8

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x566e0768

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2651214
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2651215
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2651216
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->s:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2651217
    :cond_0
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->i:Landroid/widget/TextView;

    .line 2651218
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->j:Landroid/widget/LinearLayout;

    .line 2651219
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->k:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651220
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->l:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651221
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->m:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651222
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->n:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651223
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->o:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2651224
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2651225
    iput-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->p:LX/0g8;

    .line 2651226
    const/16 v1, 0x2b

    const v2, -0x1af305a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1610c835

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2651227
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2651228
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    new-instance v2, LX/J7p;

    invoke-direct {v2, p0}, LX/J7p;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    .line 2651229
    iput-object v2, v1, LX/J7b;->e:LX/J7a;

    .line 2651230
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->r:LX/J7b;

    .line 2651231
    iget-object v2, v1, LX/J7b;->c:LX/J7f;

    iget-object v3, v1, LX/J7b;->d:LX/0TF;

    .line 2651232
    const/4 v1, 0x1

    invoke-virtual {v2, v3, v1}, LX/J7f;->a(LX/0TF;I)V

    .line 2651233
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;->q:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2651234
    const/16 v1, 0x2b

    const v2, 0x3fe4ee21

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2651235
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2651236
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 2651237
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2651238
    new-instance v1, LX/J7g;

    invoke-direct {v1, p0}, LX/J7g;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithCS5StoryFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2651239
    return-void
.end method
