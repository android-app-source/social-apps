.class public Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J7f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/J8C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0h5;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/LinearLayout;

.field public l:Landroid/widget/RadioButton;

.field public m:Landroid/widget/RadioButton;

.field public n:Landroid/widget/RadioButton;

.field public o:LX/0g8;

.field public p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public q:LX/J7b;

.field public r:LX/1Qq;

.field public s:LX/J82;

.field public t:I

.field public u:F

.field public v:I

.field public w:Z

.field public x:LX/J81;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2651466
    const-class v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    sput-object v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2651467
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2651468
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    .line 2651469
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    .line 2651470
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2651471
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2651472
    if-nez v1, :cond_1

    .line 2651473
    :cond_0
    :goto_0
    return v0

    .line 2651474
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    .line 2651475
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;LX/ICS;)V
    .locals 1

    .prologue
    .line 2651476
    invoke-direct {p0, p1}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->b(LX/ICS;)V

    .line 2651477
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    if-eqz v0, :cond_0

    .line 2651478
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v0}, LX/J7b;->b()V

    .line 2651479
    :goto_0
    return-void

    .line 2651480
    :cond_0
    sget-object v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->h:Ljava/lang/Class;

    const-string p1, "Fail to initialize story gallery survey controller"

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(LX/ICS;)V
    .locals 6

    .prologue
    .line 2651460
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v1}, LX/J7b;->e()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    invoke-virtual {v2}, LX/J7b;->d()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    .line 2651461
    iget-object p0, v3, LX/J82;->b:Ljava/util/List;

    move-object v3, p0

    .line 2651462
    invoke-virtual {p1}, LX/ICS;->toEventName()Ljava/lang/String;

    move-result-object v4

    .line 2651463
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "count"

    invoke-virtual {v5, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "final_count"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "selection"

    invoke-virtual {p1}, LX/ICS;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "first_story_tracking_data"

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "second_story_tracking_data"

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2651464
    iget-object v5, v0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2651465
    return-void
.end method

.method public static p(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2651481
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->l:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2651482
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->m:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2651483
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->n:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2651484
    return-void
.end method

.method public static s(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V
    .locals 1

    .prologue
    .line 2651456
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2651457
    if-eqz v0, :cond_0

    .line 2651458
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 2651459
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2651448
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2651449
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p1}, LX/J7f;->b(LX/0QB;)LX/J7f;

    move-result-object v4

    check-cast v4, LX/J7f;

    invoke-static {p1}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v5

    check-cast v5, LX/1Db;

    invoke-static {p1}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v6

    check-cast v6, LX/1DS;

    const/16 v7, 0x6bd

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p1}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    const-class v0, LX/J8C;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/J8C;

    iput-object v3, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->b:LX/J7f;

    iput-object v5, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->c:LX/1Db;

    iput-object v6, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->d:LX/1DS;

    iput-object v7, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->e:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->f:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    iput-object p1, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->g:LX/J8C;

    .line 2651450
    sget-object v0, LX/J81;->DEFAULT:LX/J81;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->x:LX/J81;

    .line 2651451
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2651452
    if-eqz v0, :cond_0

    .line 2651453
    const-string v1, "survey_type"

    sget-object v2, LX/J81;->DEFAULT:LX/J81;

    invoke-virtual {v2}, LX/J81;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/J81;->fromStringToSurveyType(Ljava/lang/String;)LX/J81;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->x:LX/J81;

    .line 2651454
    :cond_0
    new-instance v0, LX/J7b;

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->a:LX/03V;

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->b:LX/J7f;

    invoke-direct {v0, v1, v2}, LX/J7b;-><init>(LX/03V;LX/J7f;)V

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    .line 2651455
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5751062

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2651423
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2651424
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->t:I

    .line 2651425
    const v0, 0x7f0313da

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2651426
    const v0, 0x7f0d2db8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->j:Landroid/widget/TextView;

    .line 2651427
    const v0, 0x7f0d2dd0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->k:Landroid/widget/LinearLayout;

    .line 2651428
    const v0, 0x7f0d2dd1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->l:Landroid/widget/RadioButton;

    .line 2651429
    const v0, 0x7f0d2dd3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->m:Landroid/widget/RadioButton;

    .line 2651430
    const v0, 0x7f0d2dd5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->n:Landroid/widget/RadioButton;

    .line 2651431
    const v0, 0x7f0d2dd7

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2651432
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2651433
    new-instance v3, LX/1Oz;

    invoke-direct {v3, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2651434
    new-instance v3, LX/0g7;

    invoke-direct {v3, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    move-object v0, v3

    .line 2651435
    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->o:LX/0g8;

    .line 2651436
    const v0, 0x7f0d2dce

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2651437
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->x:LX/J81;

    sget-object v3, LX/J81;->BAKEOFF:LX/J81;

    if-ne v0, v3, :cond_0

    .line 2651438
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->l:Landroid/widget/RadioButton;

    new-instance v3, LX/J7s;

    invoke-direct {v3, p0}, LX/J7s;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651439
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->m:Landroid/widget/RadioButton;

    new-instance v3, LX/J7t;

    invoke-direct {v3, p0}, LX/J7t;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651440
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->n:Landroid/widget/RadioButton;

    new-instance v3, LX/J7u;

    invoke-direct {v3, p0}, LX/J7u;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651441
    :goto_0
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->o:LX/0g8;

    new-instance v3, LX/J7y;

    invoke-direct {v3, p0}, LX/J7y;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-interface {v0, v3}, LX/0g8;->a(LX/62F;)V

    .line 2651442
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->o:LX/0g8;

    iget-object v3, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->c:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->a(LX/1St;)V

    .line 2651443
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->o:LX/0g8;

    iget-object v3, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v3}, LX/0g8;->f(Landroid/view/View;)V

    .line 2651444
    const/16 v0, 0x2b

    const v3, 0x1e7cb6de

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2

    .line 2651445
    :cond_0
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->l:Landroid/widget/RadioButton;

    new-instance v3, LX/J7v;

    invoke-direct {v3, p0}, LX/J7v;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651446
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->m:Landroid/widget/RadioButton;

    new-instance v3, LX/J7w;

    invoke-direct {v3, p0}, LX/J7w;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2651447
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->n:Landroid/widget/RadioButton;

    new-instance v3, LX/J7x;

    invoke-direct {v3, p0}, LX/J7x;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3e80a7f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2651419
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2651420
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2651421
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->r:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2651422
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x4c5c00a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x364c8f93

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2651413
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2651414
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    new-instance v2, LX/J80;

    invoke-direct {v2, p0}, LX/J80;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    .line 2651415
    iput-object v2, v1, LX/J7b;->e:LX/J7a;

    .line 2651416
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->q:LX/J7b;

    iget-object v2, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->x:LX/J81;

    invoke-virtual {v1, v2}, LX/J7b;->a(LX/J81;)V

    .line 2651417
    iget-object v1, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->p:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2651418
    const/16 v1, 0x2b

    const v2, -0x4bbfd057

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2651406
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2651407
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 2651408
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->i:LX/0h5;

    .line 2651409
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->x:LX/J81;

    sget-object v1, LX/J81;->BAKEOFF:LX/J81;

    if-ne v0, v1, :cond_0

    .line 2651410
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->i:LX/0h5;

    const v1, 0x7f083912

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2651411
    :cond_0
    iget-object v0, p0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->i:LX/0h5;

    new-instance v1, LX/J7r;

    invoke-direct {v1, p0}, LX/J7r;-><init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2651412
    return-void
.end method
