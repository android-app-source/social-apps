.class public Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

.field public q:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2651033
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2651034
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->p:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->q:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2651016
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2651017
    invoke-static {p0, p0}, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2651018
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2651019
    sget-object v1, LX/J81;->BAKEOFF:LX/J81;

    invoke-virtual {v1}, LX/J81;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->q:LX/0Uh;

    const/16 v2, 0x320

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2651020
    const v0, 0x7f0313d4

    invoke-virtual {p0, v0}, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->setContentView(I)V

    .line 2651021
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2651022
    if-eqz v0, :cond_0

    .line 2651023
    const-string v1, "story_gallery_survey_feed_unit"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    iput-object v0, p0, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->p:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 2651024
    :cond_0
    return-void

    .line 2651025
    :cond_1
    const v1, 0x7f0313dc

    invoke-virtual {p0, v1}, Lcom/facebook/storygallerysurvey/activity/StoryGallerySurveyWithStoryActivity;->setContentView(I)V

    .line 2651026
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2651027
    new-instance v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-direct {v2}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;-><init>()V

    .line 2651028
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2651029
    const-string v4, "survey_type"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2651030
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2651031
    const v0, 0x7f0d2dd9

    invoke-virtual {v1, v0, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2651032
    invoke-virtual {v1}, LX/0hH;->b()I

    goto :goto_0
.end method
