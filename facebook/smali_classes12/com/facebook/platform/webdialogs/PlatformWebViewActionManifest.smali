.class public Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifestDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "action"
    .end annotation
.end field

.field private mFetchState:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fetchState"
    .end annotation
.end field

.field private mResultAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "resultAction"
    .end annotation
.end field

.field private mUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation
.end field

.field private mVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2480167
    const-class v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifestDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2480166
    new-instance v0, LX/HY5;

    invoke-direct {v0}, LX/HY5;-><init>()V

    sput-object v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480165
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2480157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mAction:Ljava/lang/String;

    .line 2480159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mResultAction:Ljava/lang/String;

    .line 2480160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mVersion:Ljava/lang/String;

    .line 2480161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mUri:Ljava/lang/String;

    .line 2480162
    const-class v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mFetchState:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    .line 2480163
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480156
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mAction:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;)V
    .locals 0

    .prologue
    .line 2480136
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mFetchState:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    .line 2480137
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2480154
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mAction:Ljava/lang/String;

    .line 2480155
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480153
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2480151
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mResultAction:Ljava/lang/String;

    .line 2480152
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480150
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2480148
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mVersion:Ljava/lang/String;

    .line 2480149
    return-void
.end method

.method public final d()Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;
    .locals 1

    .prologue
    .line 2480147
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mFetchState:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2480145
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mUri:Ljava/lang/String;

    .line 2480146
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2480144
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2480138
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2480139
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mResultAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2480140
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2480141
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2480142
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->mFetchState:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2480143
    return-void
.end method
