.class public Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/HY4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2479666
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;

    invoke-static {v0}, LX/HY4;->a(LX/0QB;)LX/HY4;

    move-result-object v0

    check-cast v0, LX/HY4;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->p:LX/HY4;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2479710
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2479711
    invoke-static {p0, p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2479712
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->p:LX/HY4;

    .line 2479713
    const-string v1, "PlatformWebDialogs_startActivity"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479714
    const-string v1, "PlatformWebDialogs_addFragment"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479715
    const v0, 0x7f031618

    invoke-virtual {p0, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->setContentView(I)V

    .line 2479716
    new-instance v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-direct {v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    .line 2479717
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2479718
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d31ad

    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2479719
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2479677
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    if-eqz v0, :cond_3

    .line 2479678
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    .line 2479679
    iget-object v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->o:LX/HYI;

    if-eqz v1, :cond_3

    .line 2479680
    iget-object v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->o:LX/HYI;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    .line 2479681
    iget-object p1, v1, LX/HYI;->a:LX/4hz;

    .line 2479682
    iget-object p2, v1, LX/HYB;->a:Lorg/json/JSONObject;

    move-object p2, p2

    .line 2479683
    invoke-virtual {p1, p2}, LX/4hz;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object p1

    .line 2479684
    if-nez p1, :cond_0

    .line 2479685
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2479686
    :cond_0
    invoke-virtual {p1, p0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2479687
    const-string p2, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 2479688
    const-string p3, "com.facebook.platform.extra.APPLICATION_ID"

    invoke-virtual {p1, p3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479689
    const-string p3, "app_id"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479690
    const-string p2, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    .line 2479691
    const-string p3, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-virtual {p1, p3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479692
    const-string p3, "version"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2479693
    const-string p2, "com.facebook.platform.protocol.RESULT_ARGS"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p2

    .line 2479694
    if-eqz p2, :cond_1

    .line 2479695
    const-string p3, "com.facebook.platform.protocol.RESULT_ARGS"

    invoke-virtual {p1, p3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479696
    iget-object p3, v1, LX/HYI;->a:LX/4hz;

    invoke-virtual {p3, p2}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object p2

    .line 2479697
    if-eqz p2, :cond_1

    .line 2479698
    const-string p3, "method_results"

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479699
    :cond_1
    const-string p2, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p2

    .line 2479700
    if-eqz p2, :cond_2

    .line 2479701
    const-string p3, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p1, p3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479702
    iget-object p3, v1, LX/HYI;->a:LX/4hz;

    invoke-virtual {p3, p2}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object p2

    .line 2479703
    if-eqz p2, :cond_2

    .line 2479704
    const-string p3, "bridge_args"

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479705
    :cond_2
    iget-object p2, v1, LX/HYI;->a:LX/4hz;

    invoke-virtual {p2, p1}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object p1

    .line 2479706
    iget-object p2, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object p3, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    iget-object v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n:LX/HYB;

    .line 2479707
    iget-object v0, v1, LX/HYB;->b:Ljava/lang/String;

    move-object v1, v0

    .line 2479708
    invoke-virtual {p2, p3, v1, p1}, LX/HY9;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2479709
    :cond_3
    return-void
.end method

.method public final onBackPressed()V
    .locals 6

    .prologue
    .line 2479667
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    if-eqz v0, :cond_0

    .line 2479668
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;->q:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    .line 2479669
    iget-boolean v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->q:Z

    if-eqz v1, :cond_1

    .line 2479670
    :goto_0
    return-void

    .line 2479671
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 2479672
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->q:Z

    .line 2479673
    iget-object v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object v2, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2479674
    const-string v3, "fbPlatformDialogMustClose"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/HY9;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2479675
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2479676
    new-instance v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment$1;

    invoke-direct {v2, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment$1;-><init>(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    const-wide/16 v3, 0x1f4

    const v5, -0x380e66c2

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
