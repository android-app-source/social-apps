.class public final Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2QR;


# direct methods
.method public constructor <init>(LX/2QR;)V
    .locals 0

    .prologue
    .line 2479720
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2479721
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    iget-object v1, v0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 2479722
    :try_start_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    iget-object v0, v0, LX/2QR;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 2479723
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    iget-object v2, v2, LX/2QR;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2QS;->f:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2479724
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    invoke-static {v0}, LX/2QR;->i(LX/2QR;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2479725
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    const/4 v2, 0x0

    .line 2479726
    iput-boolean v2, v0, LX/2QR;->n:Z

    .line 2479727
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$1;->a:LX/2QR;

    iget-object v0, v0, LX/2QR;->c:LX/03V;

    const-string v2, "PlatformWebDialogsCache"

    const-string v3, "Caching disabled for this device. Too many cache failures."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479728
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
