.class public final Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:LX/2QR;


# direct methods
.method public constructor <init>(LX/2QR;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 2479729
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->b:LX/2QR;

    iput-object p2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2479730
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->b:LX/2QR;

    iget-object v2, v0, LX/2QR;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 2479731
    :try_start_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2479732
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2479733
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2479734
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2479735
    invoke-static {v1}, LX/2QR;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2479736
    invoke-static {v0}, LX/2QR;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2479737
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2479738
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->b:LX/2QR;

    iget-object v0, v0, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2479739
    if-eqz v0, :cond_0

    .line 2479740
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$2;->b:LX/2QR;

    iget-object v1, v1, LX/2QR;->j:Ljava/util/Map;

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2479741
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
