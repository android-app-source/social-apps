.class public Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/4hz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HY9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2QQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HY4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HXy;

.field public i:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

.field public k:Landroid/widget/FrameLayout;

.field public l:Landroid/widget/ProgressBar;

.field public m:Lcom/facebook/webview/FacebookWebView;

.field public n:LX/HYB;

.field public o:LX/HYI;

.field public p:Z

.field public q:Z

.field public r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2479938
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2479932
    iget-boolean v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->r:Z

    if-eqz v0, :cond_0

    .line 2479933
    :goto_0
    return-void

    .line 2479934
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->r:Z

    .line 2479935
    invoke-direct {p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->p()V

    .line 2479936
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2479937
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V
    .locals 14

    .prologue
    const/4 v6, 0x1

    .line 2479881
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2479882
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    if-nez v0, :cond_0

    .line 2479883
    invoke-static {p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    .line 2479884
    :goto_0
    return-void

    .line 2479885
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479886
    const-string v13, "PlatformWebDialogs_Fragment_setupWebView"

    const-string v7, "call_id"

    .line 2479887
    iget-object v8, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2479888
    const-string v9, "action_name"

    .line 2479889
    iget-object v10, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v10, v10

    .line 2479890
    const-string v11, "dialog_version"

    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->b()Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v7 .. v12}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v7

    invoke-static {v0, v13, v7}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479891
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c()Ljava/lang/String;

    move-result-object v1

    .line 2479892
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2479893
    const-string v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2479894
    iget-object v3, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->b:LX/4hz;

    invoke-virtual {v3, v0}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object v3

    .line 2479895
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->b:LX/4hz;

    const-string v4, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2479896
    iget-object v4, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->b:LX/4hz;

    const-string v5, "com.facebook.platform.webdialogs.HOST_ARGS"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2479897
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v3, :cond_1

    if-nez v2, :cond_2

    .line 2479898
    :cond_1
    invoke-static {p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    goto :goto_0

    .line 2479899
    :cond_2
    if-nez v0, :cond_3

    .line 2479900
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2479901
    :cond_3
    const-string v4, "#"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 2479902
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2479903
    if-eqz v4, :cond_4

    const-string v1, "&"

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479904
    const-string v1, "bridge_args="

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479905
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479906
    const-string v1, "&method_args"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479907
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479908
    const-string v0, "&host_app_args"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479909
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479910
    const-string v0, "&app_id"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479911
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479912
    iget-object v1, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2479913
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479914
    const-string v0, "&version"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2479915
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479916
    iget v1, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    move v0, v1

    .line 2479917
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2479918
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v6}, Lcom/facebook/webview/FacebookWebView;->setFocusable(Z)V

    .line 2479919
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v6}, Lcom/facebook/webview/FacebookWebView;->setFocusableInTouchMode(Z)V

    .line 2479920
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    new-instance v1, LX/HXz;

    invoke-direct {v1, p0}, LX/HXz;-><init>(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2479921
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2479922
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479923
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479924
    iget-object v2, v0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2479925
    :goto_2
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479926
    const-string v1, "PlatformWebDialogs_Fragment_setupWebView"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479927
    const-string v1, "PlatformWebDialogs_Fragment_loadJSDialog"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479928
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->d:LX/48V;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2479929
    :cond_4
    const-string v1, "#"

    goto/16 :goto_1

    .line 2479930
    :cond_5
    iget-object v2, v0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479931
    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2479859
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a:LX/0Zb;

    .line 2479860
    new-instance v1, LX/8Or;

    const-string v2, "platform_web_view"

    invoke-direct {v1, p1, v2}, LX/8Or;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v2, -0x80000000

    .line 2479861
    iput v2, v1, LX/8Or;->i:I

    .line 2479862
    move-object v1, v1

    .line 2479863
    const-string v2, "webdialog"

    .line 2479864
    iput-object v2, v1, LX/8Or;->h:Ljava/lang/String;

    .line 2479865
    move-object v1, v1

    .line 2479866
    move-object v1, v1

    .line 2479867
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479868
    iget-object p1, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2479869
    iput-object v2, v1, LX/8Or;->b:Ljava/lang/String;

    .line 2479870
    move-object v1, v1

    .line 2479871
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479872
    iget-object p1, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v2, p1

    .line 2479873
    iput-object v2, v1, LX/8Or;->c:Ljava/lang/String;

    .line 2479874
    move-object v1, v1

    .line 2479875
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479876
    iget-object p0, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v2, p0

    .line 2479877
    iput-object v2, v1, LX/8Or;->g:Ljava/lang/String;

    .line 2479878
    move-object v1, v1

    .line 2479879
    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2479880
    return-void
.end method

.method public static n(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    .locals 4

    .prologue
    .line 2479854
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2479855
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v2, "UnknownError"

    const-string v3, "Dialog failed with unknown error"

    invoke-static {v1, v2, v3}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2479856
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;ILandroid/content/Intent;)V

    .line 2479857
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    invoke-virtual {v0}, LX/HY4;->l()V

    .line 2479858
    return-void
.end method

.method public static o(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    .locals 4

    .prologue
    .line 2479849
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2479850
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v2, "NetworkError"

    const-string v3, "Could not load the dialog from network"

    invoke-static {v1, v2, v3}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2479851
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;ILandroid/content/Intent;)V

    .line 2479852
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    invoke-virtual {v0}, LX/HY4;->l()V

    .line 2479853
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 2479779
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2479780
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479781
    iget-object v3, v0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2479782
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->f:LX/2QQ;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->h:LX/HXy;

    .line 2479783
    iget-object v2, v0, LX/2QQ;->t:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2479784
    iget-object v2, v0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479785
    return-void

    :cond_1
    iget-object v3, v0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/HYB;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2479844
    iget-object v0, p1, LX/HYB;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2479845
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2479846
    :goto_0
    return-void

    .line 2479847
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->b:LX/4hz;

    invoke-virtual {v1, p2}, LX/4hz;->a(Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2479848
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object v3, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v2, v3, v0, v1}, LX/HY9;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2479829
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2479830
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/4hz;->a(LX/0QB;)LX/4hz;

    move-result-object v4

    check-cast v4, LX/4hz;

    invoke-static {v0}, LX/HY9;->a(LX/0QB;)LX/HY9;

    move-result-object v5

    check-cast v5, LX/HY9;

    invoke-static {v0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v6

    check-cast v6, LX/48V;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/2QQ;->a(LX/0QB;)LX/2QQ;

    move-result-object v8

    check-cast v8, LX/2QQ;

    invoke-static {v0}, LX/HY4;->a(LX/0QB;)LX/HY4;

    move-result-object v0

    check-cast v0, LX/HY4;

    iput-object v3, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a:LX/0Zb;

    iput-object v4, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->b:LX/4hz;

    iput-object v5, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iput-object v6, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->d:LX/48V;

    iput-object v7, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->f:LX/2QQ;

    iput-object v0, v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479831
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479832
    const-string v1, "PlatformWebDialogs_addFragment"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479833
    const-string v1, "PlatformWebDialogs_Fragment_onCreate"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479834
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2479835
    const-string v1, "com.facebook.platform.webdialogs.APPCALL_PARCEL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/common/action/PlatformAppCall;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479836
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    if-nez v0, :cond_0

    .line 2479837
    invoke-static {p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    .line 2479838
    :goto_0
    return-void

    .line 2479839
    :cond_0
    if-eqz p1, :cond_1

    .line 2479840
    const-string v0, "com.facebook.platform.webdialogs.ACTION_MANIFEST_PARCEL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2479841
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479842
    const-string v1, "PlatformWebDialogs_Fragment_onCreate"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479843
    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x66840242

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2479805
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2479806
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    if-nez v1, :cond_0

    .line 2479807
    const/4 v3, 0x0

    .line 2479808
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479809
    iget-object v2, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2479810
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479811
    const-string v4, "PlatformWebDialogs_Fragment_fetchManifest"

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479812
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->f:LX/2QQ;

    invoke-virtual {v2, v1}, LX/2QQ;->a(Ljava/lang/String;)Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2479813
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    if-eqz v2, :cond_1

    .line 2479814
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-virtual {v1, v2, v3, v3}, LX/HY4;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;ZZ)V

    .line 2479815
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-static {p0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V

    .line 2479816
    :goto_0
    const v1, 0x5f07444d    # 9.747E18f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2479817
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-static {p0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V

    goto :goto_0

    .line 2479818
    :cond_1
    const/4 p1, -0x2

    .line 2479819
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->l:Landroid/widget/ProgressBar;

    .line 2479820
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->k:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->l:Landroid/widget/ProgressBar;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x11

    invoke-direct {v4, p1, p1, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2479821
    new-instance v2, LX/HXy;

    invoke-direct {v2, p0, v1}, LX/HXy;-><init>(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->h:LX/HXy;

    .line 2479822
    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->f:LX/2QQ;

    iget-object v3, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->h:LX/HXy;

    .line 2479823
    iget-object v4, v2, LX/2QQ;->t:LX/0Sh;

    const-string v5, "Cannot refresh the manifest off the UI thread."

    invoke-virtual {v4, v5}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 2479824
    invoke-static {v2}, LX/2QQ;->h(LX/2QQ;)Ljava/lang/String;

    move-result-object v4

    .line 2479825
    if-eqz v3, :cond_2

    .line 2479826
    iget-object v5, v2, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2479827
    :cond_2
    invoke-static {v2, v4, v1}, LX/2QQ;->a(LX/2QQ;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2479828
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, -0x1

    const/16 v0, 0x2a

    const v1, -0x265c98ce

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2479792
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479793
    const-string v2, "PlatformWebDialogs_Fragment_onCreateView"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479794
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->k:Landroid/widget/FrameLayout;

    .line 2479795
    new-instance v1, Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2479796
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/webview/FacebookWebView;->setVisibility(I)V

    .line 2479797
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->k:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2479798
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->c:LX/HY9;

    iget-object v2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2479799
    invoke-virtual {v2, v1}, Lcom/facebook/webview/FacebookWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2479800
    const-string v3, "fbplatdialog"

    invoke-virtual {v2, v3}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;)LX/BWE;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2479801
    const-string v3, "fbplatdialog"

    iget-object v4, v1, LX/HY9;->i:LX/BWE;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWE;)V

    .line 2479802
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    .line 2479803
    const-string v2, "PlatformWebDialogs_Fragment_onCreateView"

    invoke-static {v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479804
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->k:Landroid/widget/FrameLayout;

    const/16 v2, 0x2b

    const v3, 0x5754973a

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4fca6e16

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2479789
    invoke-direct {p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->p()V

    .line 2479790
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2479791
    const/16 v1, 0x2b

    const v2, -0x77d5f794

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2479786
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    if-eqz v0, :cond_0

    .line 2479787
    const-string v0, "com.facebook.platform.webdialogs.ACTION_MANIFEST_PARCEL"

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->j:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2479788
    :cond_0
    return-void
.end method
