.class public Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;
.super Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;
.source ""

# interfaces
.implements LX/63T;


# instance fields
.field private r:Z

.field private final s:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2490536
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2490537
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2490538
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490539
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2490540
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490541
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->r:Z

    .line 2490542
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020d7a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->s:Landroid/graphics/drawable/Drawable;

    .line 2490543
    return-void
.end method

.method private c(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 2490544
    const/4 v0, 0x0

    .line 2490545
    cmpl-float v1, p1, v2

    if-lez v1, :cond_0

    .line 2490546
    rem-float v0, p1, v2

    div-float/2addr v0, v2

    .line 2490547
    :cond_0
    iget-object v1, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    .line 2490548
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2490549
    return-void
.end method

.method private d(F)V
    .locals 5

    .prologue
    const v3, 0x3e4ccccd    # 0.2f

    .line 2490528
    cmpg-float v0, p1, v3

    if-gez v0, :cond_0

    .line 2490529
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->s:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2490530
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2490531
    :goto_0
    return-void

    .line 2490532
    :cond_0
    cmpl-float v0, p1, v3

    if-ltz v0, :cond_1

    const/high16 v0, 0x3f000000    # 0.5f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 2490533
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->s:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v3, p1, v3

    const v4, 0x3e99999a    # 0.3f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2490534
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2490535
    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private e(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2490550
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 2490551
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f01026b

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 2490552
    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundColor(I)V

    .line 2490553
    :goto_0
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2490554
    return-void

    .line 2490555
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2490526
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->e(F)V

    .line 2490527
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2490516
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 2490517
    invoke-direct {p0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->i()V

    .line 2490518
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f01026b

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 2490519
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2490520
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setClickable(Z)V

    .line 2490521
    :goto_0
    return-void

    .line 2490522
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->setClickable(Z)V

    .line 2490523
    invoke-direct {p0, p1}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->c(F)V

    .line 2490524
    invoke-virtual {p0, p1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->b(F)V

    .line 2490525
    invoke-direct {p0, p1}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->d(F)V

    goto :goto_0
.end method

.method public getActualHeight()I
    .locals 1

    .prologue
    .line 2490515
    invoke-virtual {p0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->getHeight()I

    move-result v0

    return v0
.end method

.method public setFadingModeEnabled(Z)V
    .locals 1

    .prologue
    .line 2490511
    iput-boolean p1, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->r:Z

    .line 2490512
    iget-boolean v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->r:Z

    if-eqz v0, :cond_0

    .line 2490513
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/titlebar/search/Fb4aFadingTitleBar;->a(F)V

    .line 2490514
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2490502
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    sget-object v1, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2490503
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2490504
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490505
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2490506
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2490507
    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2490508
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->b()V

    .line 2490509
    :goto_0
    return-void

    .line 2490510
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTitleHint(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2490493
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->j:LX/0wI;

    sget-object v1, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2490494
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2490495
    if-eqz v0, :cond_0

    .line 2490496
    iget-object v0, p0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    .line 2490497
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2490498
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2490499
    invoke-virtual {v0, p1}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2490500
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2490501
    :cond_0
    return-void
.end method
