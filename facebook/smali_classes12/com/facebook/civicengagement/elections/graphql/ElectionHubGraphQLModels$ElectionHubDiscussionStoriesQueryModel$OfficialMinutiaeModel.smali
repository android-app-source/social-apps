.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d3d9444
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504234
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504218
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2504243
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2504244
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2504235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504236
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2504237
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2504238
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2504239
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2504240
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2504241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504242
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2504219
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504220
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2504221
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    .line 2504222
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2504223
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    .line 2504224
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    .line 2504225
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2504226
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    .line 2504227
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2504228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    .line 2504229
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    .line 2504230
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504231
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504232
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    .line 2504233
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2504211
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    invoke-direct {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;-><init>()V

    .line 2504212
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2504213
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2504214
    const v0, -0x6809fcf6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2504215
    const v0, 0x4cff1ce8    # 1.3375264E8f

    return v0
.end method

.method public final j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504216
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    .line 2504217
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    return-object v0
.end method
