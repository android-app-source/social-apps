.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2504451
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    new-instance v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2504452
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2504453
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2504454
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2504455
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2504456
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2504457
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2504458
    if-eqz v2, :cond_1

    .line 2504459
    const-string v3, "header_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504460
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2504461
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2504462
    if-eqz v3, :cond_0

    .line 2504463
    const-string v4, "uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504464
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2504465
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2504466
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2504467
    if-eqz v2, :cond_2

    .line 2504468
    const-string v3, "live_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504469
    invoke-static {v1, v2, p1, p2}, LX/Hnz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2504470
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2504471
    if-eqz v2, :cond_8

    .line 2504472
    const-string v3, "tabs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504473
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2504474
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 2504475
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 p2, 0x0

    .line 2504476
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2504477
    invoke-virtual {v1, v4, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2504478
    if-eqz p0, :cond_3

    .line 2504479
    const-string v0, "display_name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504480
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2504481
    :cond_3
    const/4 p0, 0x1

    invoke-virtual {v1, v4, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2504482
    if-eqz p0, :cond_4

    .line 2504483
    const-string v0, "is_nt"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504484
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2504485
    :cond_4
    const/4 p0, 0x2

    invoke-virtual {v1, v4, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 2504486
    if-eqz p0, :cond_5

    .line 2504487
    const-string v0, "poll_interval"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504488
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 2504489
    :cond_5
    const/4 p0, 0x3

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2504490
    if-eqz p0, :cond_6

    .line 2504491
    const-string v0, "tab_id"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504492
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2504493
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2504494
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2504495
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2504496
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2504497
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2504498
    check-cast p1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;->a(Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
