.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x24bdc496
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504450
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504449
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2504447
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2504448
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2504441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504442
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2504443
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2504444
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2504445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504446
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2504433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504434
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2504435
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2504436
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2504437
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    .line 2504438
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2504439
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504440
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504431
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2504432
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2504428
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    invoke-direct {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;-><init>()V

    .line 2504429
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2504430
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2504427
    const v0, 0x1e4eea6c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2504426
    const v0, 0x4ed245b

    return v0
.end method
