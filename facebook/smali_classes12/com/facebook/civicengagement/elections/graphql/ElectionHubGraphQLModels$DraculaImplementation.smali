.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2503896
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2503897
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2503898
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2503899
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2503900
    if-nez p1, :cond_0

    .line 2503901
    :goto_0
    return v0

    .line 2503902
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2503903
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2503904
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2503905
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2503906
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2503907
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 2503908
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2503909
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2503910
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2503911
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2503912
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2503913
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 2503914
    invoke-virtual {p3, v7, v4}, LX/186;->b(II)V

    .line 2503915
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2503916
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2503917
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2503918
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2503919
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2503920
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2503921
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2503922
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2503923
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2503924
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 2503925
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2503926
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2503927
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2503928
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2503929
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2503930
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 2503931
    invoke-virtual {p3, v7, v4}, LX/186;->b(II)V

    .line 2503932
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2503933
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2503934
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2503935
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2503936
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 2503937
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2503938
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2503939
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2503940
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2503941
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2503942
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 2503943
    invoke-virtual {p3, v7, v4}, LX/186;->b(II)V

    .line 2503944
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x47ea9b61 -> :sswitch_0
        -0x39d791f5 -> :sswitch_2
        0x2571f6da -> :sswitch_1
        0x627053d0 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2503945
    if-nez p0, :cond_0

    move v0, v1

    .line 2503946
    :goto_0
    return v0

    .line 2503947
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2503948
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2503949
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2503950
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2503951
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2503952
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2503953
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2503955
    const/4 v7, 0x0

    .line 2503956
    const/4 v1, 0x0

    .line 2503957
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2503958
    invoke-static {v2, v3, v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2503959
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2503960
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2503961
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2503954
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2503963
    sparse-switch p0, :sswitch_data_0

    .line 2503964
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2503965
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x47ea9b61 -> :sswitch_0
        -0x39d791f5 -> :sswitch_0
        0x2571f6da -> :sswitch_0
        0x627053d0 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2503962
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2503894
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->b(I)V

    .line 2503895
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2503863
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2503864
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2503865
    :cond_0
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2503866
    iput p2, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->b:I

    .line 2503867
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2503893
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2503892
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2503889
    iget v0, p0, LX/1vt;->c:I

    .line 2503890
    move v0, v0

    .line 2503891
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2503886
    iget v0, p0, LX/1vt;->c:I

    .line 2503887
    move v0, v0

    .line 2503888
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2503883
    iget v0, p0, LX/1vt;->b:I

    .line 2503884
    move v0, v0

    .line 2503885
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2503880
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2503881
    move-object v0, v0

    .line 2503882
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2503871
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2503872
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2503873
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2503874
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2503875
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2503876
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2503877
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2503878
    invoke-static {v3, v9, v2}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2503879
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2503868
    iget v0, p0, LX/1vt;->c:I

    .line 2503869
    move v0, v0

    .line 2503870
    return v0
.end method
