.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3dab0dcd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504295
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504294
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2504262
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2504263
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2504286
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504287
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2504288
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2504289
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2504290
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2504291
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2504292
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2504273
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504274
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2504275
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    .line 2504276
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2504277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    .line 2504278
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    .line 2504279
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2504280
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    .line 2504281
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2504282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    .line 2504283
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    .line 2504284
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504285
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDiscussionStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504271
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    .line 2504272
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->e:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2504268
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-direct {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;-><init>()V

    .line 2504269
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2504270
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2504267
    const v0, -0x5f902e9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2504266
    const v0, -0x727cf204

    return v0
.end method

.method public final j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504264
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    .line 2504265
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    return-object v0
.end method
