.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x46783aca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504544
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2504543
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2504541
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2504542
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2504531
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504532
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x2571f6da

    invoke-static {v1, v0, v2}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2504533
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2504534
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 2504535
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2504536
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2504537
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2504538
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2504539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504540
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2504511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2504512
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2504513
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2571f6da

    invoke-static {v2, v0, v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2504514
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2504515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    .line 2504516
    iput v3, v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->e:I

    move-object v1, v0

    .line 2504517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2504518
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    .line 2504519
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2504520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    .line 2504521
    iput-object v0, v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    .line 2504522
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2504523
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2504524
    if-eqz v2, :cond_2

    .line 2504525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    .line 2504526
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->g:LX/3Sb;

    move-object v1, v0

    .line 2504527
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2504528
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2504529
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2504530
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHeaderImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2504545
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2504546
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2504508
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2504509
    const/4 v0, 0x0

    const v1, 0x2571f6da

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->e:I

    .line 2504510
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2504499
    new-instance v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;

    invoke-direct {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;-><init>()V

    .line 2504500
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2504501
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2504507
    const v0, 0x74e16a82

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2504506
    const v0, -0x727cf204

    return v0
.end method

.method public final j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2504504
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    .line 2504505
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->f:Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel$LiveVideoModel;

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTabs"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2504502
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, -0x39d791f5

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->g:LX/3Sb;

    .line 2504503
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubSurfaceQueryModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
