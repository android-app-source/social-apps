.class public final Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2504601
    const-class v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    new-instance v1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2504602
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2504600
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2504583
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2504584
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2504585
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2504586
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2504587
    if-eqz v2, :cond_1

    .line 2504588
    const-string v3, "top_videos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504589
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2504590
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 2504591
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/Ho1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2504592
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2504593
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2504594
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2504595
    if-eqz v2, :cond_2

    .line 2504596
    const-string v3, "video_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2504597
    invoke-static {v1, v2, p1, p2}, LX/Ho3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2504598
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2504599
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2504582
    check-cast p1, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$Serializer;->a(Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
