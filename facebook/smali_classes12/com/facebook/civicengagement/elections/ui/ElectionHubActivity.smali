.class public Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0f8;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private A:LX/HoR;

.field private B:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private C:Landroid/support/v4/view/ViewPager;

.field public D:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderView;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field public G:I

.field public H:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

.field public p:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Hnj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/0hE;

.field public z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/HoQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2505274
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2505275
    new-instance v0, LX/Hnp;

    invoke-direct {v0}, LX/Hnp;-><init>()V

    move-object v0, v0

    .line 2505276
    const-string v1, "scale"

    sget-object v2, LX/0wC;->NUMBER_2:LX/0wC;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2505277
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2505278
    new-instance v1, LX/Ho6;

    invoke-direct {v1, p0}, LX/Ho6;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V

    .line 2505279
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->q:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2505280
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->p:LX/1Ck;

    const-string v3, "task_election_hub_header_fetch"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2505281
    return-void
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 2505282
    new-instance v0, LX/Ho8;

    invoke-direct {v0, p0}, LX/Ho8;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2505283
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->t:LX/0hL;

    const v1, 0x7f020756

    invoke-virtual {v0, v1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2505284
    return-void
.end method

.method private static a(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;LX/1Ck;LX/0tX;LX/0Ot;LX/Hnj;LX/0hL;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/Hnj;",
            "LX/0hL;",
            "LX/0Ot",
            "<",
            "LX/0iL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2505285
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->p:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->q:LX/0tX;

    iput-object p3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->s:LX/Hnj;

    iput-object p5, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->t:LX/0hL;

    iput-object p6, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->w:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->x:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    invoke-static {v9}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const/16 v3, 0x259

    invoke-static {v9, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v9}, LX/Hnj;->a(LX/0QB;)LX/Hnj;

    move-result-object v4

    check-cast v4, LX/Hnj;

    invoke-static {v9}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v5

    check-cast v5, LX/0hL;

    const/16 v6, 0x1302

    invoke-static {v9, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x78e

    invoke-static {v9, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1390

    invoke-static {v9, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v10, 0xbc3

    invoke-static {v9, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->a(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;LX/1Ck;LX/0tX;LX/0Ot;LX/Hnj;LX/0hL;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2505286
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->E:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2505287
    iput v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->G:I

    .line 2505288
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 2505289
    :goto_0
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2505290
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HoQ;

    iget-object v0, v0, LX/HoQ;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->E:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0YN;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 2505291
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2505292
    iput v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->G:I

    .line 2505293
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V
    .locals 3

    .prologue
    .line 2505251
    new-instance v0, LX/HoR;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/HoR;-><init>(Ljava/util/List;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->A:LX/HoR;

    .line 2505252
    const v0, 0x7f0d0d51

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->C:Landroid/support/v4/view/ViewPager;

    .line 2505253
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->C:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->A:LX/HoR;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2505254
    const v0, 0x7f0d0d50

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->B:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2505255
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->B:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Ho7;

    invoke-direct {v1, p0}, LX/Ho7;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V

    .line 2505256
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2505257
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->B:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->C:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2505258
    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->b()V

    .line 2505259
    return-void
.end method


# virtual methods
.method public final a(LX/0hE;)V
    .locals 0

    .prologue
    .line 2505249
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    .line 2505250
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2505260
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2505261
    invoke-static {p0, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2505262
    const v0, 0x7f03046a

    invoke-virtual {p0, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->setContentView(I)V

    .line 2505263
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->E:Ljava/lang/String;

    .line 2505264
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->F:Ljava/lang/String;

    .line 2505265
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->s:LX/Hnj;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->F:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Hnj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2505266
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->s:LX/Hnj;

    .line 2505267
    const-string v1, "election_hub_page_load"

    invoke-static {v0, v1}, LX/Hnj;->b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2505268
    iget-object v2, v0, LX/Hnj;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2505269
    const v0, 0x7f0d0d4e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderView;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->D:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderView;

    .line 2505270
    const v0, 0x7f0d0d56

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->a(Landroid/widget/ImageView;)V

    .line 2505271
    const v0, 0x7f0d0d4f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->H:Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

    .line 2505272
    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->a()V

    .line 2505273
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2505248
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/0hE;
    .locals 1

    .prologue
    .line 2505246
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iI;

    invoke-virtual {v0, p0}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    .line 2505247
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    return-object v0
.end method

.method public final l()LX/0hE;
    .locals 1

    .prologue
    .line 2505244
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iL;

    invoke-virtual {v0, p0}, LX/0iL;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    .line 2505245
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    return-object v0
.end method

.method public final m()LX/0hE;
    .locals 1

    .prologue
    .line 2505242
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iM;

    invoke-virtual {v0, p0}, LX/0iM;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    .line 2505243
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    return-object v0
.end method

.method public final o()LX/0hE;
    .locals 1

    .prologue
    .line 2505240
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iQ;

    invoke-virtual {v0, p0}, LX/0iQ;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    .line 2505241
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2505237
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505238
    :goto_0
    return-void

    .line 2505239
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2505234
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505235
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->y:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    .line 2505236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
