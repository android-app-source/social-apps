.class public abstract Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Qq;

.field private b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public c:LX/1P0;

.field public d:LX/0g7;

.field public e:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public f:Landroid/view/View;

.field public g:LX/HoI;

.field private h:I

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Hnl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Hnj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public p:LX/HoL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2505465
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2505466
    return-void
.end method

.method public static a(Ljava/lang/String;I)Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;
    .locals 3

    .prologue
    .line 2505450
    const-string v0, "discussion"

    invoke-static {p0, v0}, LX/0YN;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2505451
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2505452
    const-string v1, "pollInterval"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2505453
    new-instance v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-direct {v1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;-><init>()V

    .line 2505454
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2505455
    move-object v0, v1

    .line 2505456
    :goto_0
    return-object v0

    .line 2505457
    :cond_0
    const-string v0, "video"

    invoke-static {p0, v0}, LX/0YN;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 2505458
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2505459
    const-string v1, "pollInterval"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2505460
    new-instance v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    invoke-direct {v1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;-><init>()V

    .line 2505461
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2505462
    move-object v0, v1

    .line 2505463
    goto :goto_0

    .line 2505464
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to construct unexpected tab id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static n(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V
    .locals 5

    .prologue
    .line 2505447
    iget v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->h:I

    if-gtz v0, :cond_0

    .line 2505448
    :goto_0
    return-void

    .line 2505449
    :cond_0
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->l:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$5;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    iget v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->h:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->o:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;
    .locals 7

    .prologue
    .line 2505438
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a:LX/1Qq;

    if-nez v0, :cond_0

    .line 2505439
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->m:LX/Hnl;

    const-string v1, "unknown"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2505440
    sget-object v3, LX/HoB;->a:LX/HoB;

    move-object v3, v3

    .line 2505441
    new-instance v4, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$6;

    invoke-direct {v4, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$6;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    iget-object v5, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    invoke-static {v5}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    new-instance v6, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$7;

    invoke-direct {v6, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment$7;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual/range {v0 .. v6}, LX/Hnl;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;Ljava/lang/Runnable;)LX/Hnk;

    move-result-object v0

    .line 2505442
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->k:LX/1DS;

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->i:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2505443
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2505444
    move-object v0, v1

    .line 2505445
    new-instance v1, LX/HoG;

    invoke-direct {v1, p0}, LX/HoG;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual {v0, v1}, LX/1Ql;->b(LX/99g;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a:LX/1Qq;

    .line 2505446
    :cond_0
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a:LX/1Qq;

    return-object v0
.end method


# virtual methods
.method public abstract a(LX/HoH;)V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2505432
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2505433
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    const/16 v3, 0x6bd

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v5

    check-cast v5, LX/1DS;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    const-class p1, LX/Hnl;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Hnl;

    invoke-static {v0}, LX/Hnj;->a(LX/0QB;)LX/Hnj;

    move-result-object v0

    check-cast v0, LX/Hnj;

    iput-object v3, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->i:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->j:LX/1Db;

    iput-object v5, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->k:LX/1DS;

    iput-object v6, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->l:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p1, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->m:LX/Hnl;

    iput-object v0, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    .line 2505434
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2505435
    const-string v1, "pollInterval"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->h:I

    .line 2505436
    new-instance v0, LX/HoL;

    invoke-direct {v0}, LX/HoL;-><init>()V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    .line 2505437
    return-void
.end method

.method public final a(Ljava/util/List;LX/HoH;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/HoH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2505411
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2505412
    :cond_0
    :goto_0
    return-void

    .line 2505413
    :cond_1
    sget-object v0, LX/HoH;->HEAD_LOAD:LX/HoH;

    if-ne p2, v0, :cond_4

    .line 2505414
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 2505415
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2505416
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    invoke-virtual {v2}, LX/0g7;->t()I

    move-result v2

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    .line 2505417
    iget v2, v0, LX/HoL;->b:I

    move v0, v2

    .line 2505418
    if-ge v1, v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2505419
    if-nez v0, :cond_3

    .line 2505420
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 2505421
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2505422
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Qr;->m_(I)I

    move-result v2

    .line 2505423
    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c:LX/1P0;

    invoke-virtual {v3, v0}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v3

    .line 2505424
    new-instance v4, LX/HoI;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v1, v5

    sub-int/2addr v0, v2

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-direct {v4, v1, v0, v2}, LX/HoI;-><init>(III)V

    iput-object v4, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->g:LX/HoI;

    .line 2505425
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2505426
    :cond_3
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    .line 2505427
    iget-object v1, v0, LX/HoL;->a:Ljava/util/List;

    iget v2, v0, LX/HoL;->b:I

    invoke-interface {v1, v2, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 2505428
    goto :goto_0

    .line 2505429
    :cond_4
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    .line 2505430
    iget-object v1, v0, LX/HoL;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2505431
    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()Z
.end method

.method public e()V
    .locals 2

    .prologue
    .line 2505405
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    .line 2505406
    iget-object v1, v0, LX/HoL;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2505407
    const/4 v1, 0x0

    iput v1, v0, LX/HoL;->b:I

    .line 2505408
    sget-object v0, LX/HoH;->FIRST_FETCH:LX/HoH;

    invoke-virtual {p0, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(LX/HoH;)V

    .line 2505409
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    invoke-virtual {v0}, LX/Hnj;->d()V

    .line 2505410
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2505401
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2505402
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2505403
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2505404
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x70dab168

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2505377
    const v0, 0x7f03046b

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2505378
    const v0, 0x102000a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2505379
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c:LX/1P0;

    .line 2505380
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c:LX/1P0;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2505381
    const v0, 0x7f0d0d52

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2505382
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v3, LX/HoC;

    invoke-direct {v3, p0}, LX/HoC;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2505383
    new-instance v0, LX/0g7;

    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v3}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    .line 2505384
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    invoke-virtual {v0, v4}, LX/0g7;->b(Z)V

    .line 2505385
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/0g7;->d(Z)V

    .line 2505386
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2505387
    iget-object v4, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    invoke-virtual {v4, v0}, LX/0g7;->d(Landroid/view/View;)V

    goto :goto_0

    .line 2505388
    :cond_0
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    new-instance v3, LX/HoD;

    invoke-direct {v3, p0}, LX/HoD;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/0fx;)V

    .line 2505389
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    new-instance v3, LX/HoE;

    invoke-direct {v3, p0}, LX/HoE;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/0fu;)V

    .line 2505390
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)LX/1Qq;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 2505391
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->d:LX/0g7;

    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->j:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/1St;)V

    .line 2505392
    const v0, 0x7f0d0d53

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->f:Landroid/view/View;

    .line 2505393
    const v0, 0x7f0d1133

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2505394
    const v3, 0x7f08370e

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2505395
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->f:Landroid/view/View;

    new-instance v3, LX/HoF;

    invoke-direct {v3, p0}, LX/HoF;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2505396
    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e()V

    .line 2505397
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/Hnj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2505398
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    invoke-virtual {v0}, LX/Hnj;->d()V

    .line 2505399
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n(Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;)V

    .line 2505400
    const/16 v0, 0x2b

    const v3, 0x65d54df9

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x22bc5509

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2505370
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2505371
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->o:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 2505372
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->o:Ljava/util/concurrent/ScheduledFuture;

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2505373
    :cond_0
    iput-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2505374
    iput-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2505375
    iput-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->f:Landroid/view/View;

    .line 2505376
    const/16 v1, 0x2b

    const v2, 0x1a173c76

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2505369
    return-void
.end method
