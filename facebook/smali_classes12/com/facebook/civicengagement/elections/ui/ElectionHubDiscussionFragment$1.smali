.class public final Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HoH;

.field public final synthetic b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;LX/HoH;)V
    .locals 0

    .prologue
    .line 2505301
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iput-object p2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->a:LX/HoH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2505302
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchElectionHubDiscussion"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2505303
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2505304
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2505305
    if-eqz p1, :cond_0

    .line 2505306
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505307
    if-nez v0, :cond_1

    .line 2505308
    :cond_0
    :goto_0
    return-void

    .line 2505309
    :cond_1
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    .line 2505310
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v0, v2

    .line 2505311
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Vd;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2505312
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505313
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2505314
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505315
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2505316
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505317
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2505318
    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    const/4 v4, 0x3

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2505319
    iput-object v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->o:Ljava/lang/String;

    .line 2505320
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505321
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2505322
    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2505323
    iput-object v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->p:Ljava/lang/String;

    .line 2505324
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505325
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2505326
    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    .line 2505327
    iput-boolean v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->q:Z

    .line 2505328
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505329
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2505330
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2505331
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505332
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2505333
    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel$EdgesModel;

    .line 2505334
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2505335
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$DiscussionStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2505336
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2505337
    :cond_4
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->a:LX/HoH;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(Ljava/util/List;LX/HoH;)V

    .line 2505338
    :cond_5
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->a:LX/HoH;

    sget-object v1, LX/HoH;->FIRST_FETCH:LX/HoH;

    if-ne v0, v1, :cond_7

    .line 2505339
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505340
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2505341
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505342
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2505343
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505344
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2505345
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505346
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2505347
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505348
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2505349
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->d:LX/929;

    const-string v1, "election_hub_single_minutiae"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v2, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->e:Ljava/util/concurrent/ExecutorService;

    .line 2505350
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 2505351
    check-cast v3, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2505352
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2505353
    check-cast v4, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;

    invoke-virtual {v4}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel;->a()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubDiscussionStoriesQueryModel$OfficialMinutiaeModel$ObjectModel;->j()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Ho9;

    invoke-direct {v5, p0}, LX/Ho9;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;)V

    invoke-virtual/range {v0 .. v5}, LX/929;->a(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    .line 2505354
    :cond_6
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    .line 2505355
    iget-object v1, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2505356
    iget-object v2, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->r:LX/HoK;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, LX/HoA;

    invoke-direct {v3, v0}, LX/HoA;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;)V

    .line 2505357
    iget-object v4, v2, LX/HoK;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2505358
    invoke-virtual {v2, v3}, LX/HoK;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2505359
    :cond_7
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->k()V

    goto/16 :goto_0
.end method
