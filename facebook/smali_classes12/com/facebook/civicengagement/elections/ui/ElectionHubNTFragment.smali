.class public Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CNt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/H1c;

.field public f:LX/CNc;

.field public g:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2505696
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2505697
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-class v2, LX/CNt;

    invoke-interface {v4, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/CNt;

    invoke-static {v4}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    const/16 p0, 0x259

    invoke-static {v4, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v1, p1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->a:LX/1Ck;

    iput-object v2, p1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->b:LX/CNt;

    iput-object v3, p1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->c:LX/0tX;

    iput-object v4, p1, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->d:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V
    .locals 4

    .prologue
    .line 2505687
    new-instance v0, LX/0zR;

    invoke-direct {v0}, LX/0zR;-><init>()V

    const-string v1, "701268200040015"

    invoke-virtual {v0, v1}, LX/0zR;->a(Ljava/lang/String;)LX/0zR;

    move-result-object v0

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v1

    invoke-virtual {v1}, LX/0wC;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zR;->a(Ljava/lang/Double;)LX/0zR;

    move-result-object v0

    .line 2505688
    new-instance v1, LX/Hno;

    invoke-direct {v1}, LX/Hno;-><init>()V

    move-object v1, v1

    .line 2505689
    const-string v2, "nt_context"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2505690
    const-string v0, "election_hub_tab"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2505691
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2505692
    new-instance v1, LX/HoO;

    invoke-direct {v1, p0}, LX/HoO;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V

    .line 2505693
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->c:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2505694
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->a:LX/1Ck;

    const-string v3, "task_election_hub_nt_tab_fetch"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2505695
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2505678
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2505679
    const-class v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    invoke-static {v0, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2505680
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2505681
    sget-object v1, LX/3j6;->a:LX/0P1;

    sget-object v2, LX/3jH;->a:LX/0P1;

    sget-object v3, LX/3jL;->a:LX/0P1;

    invoke-static {v1, v2, v3}, LX/3jU;->a(LX/0P1;LX/0P1;LX/0P1;)V

    .line 2505682
    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->h:Ljava/lang/String;

    .line 2505683
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->b:LX/CNt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CNt;->a(Landroid/content/Context;)LX/CNq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->f:LX/CNc;

    .line 2505684
    new-instance v0, LX/H1c;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->f:LX/CNc;

    invoke-direct {v0, v1, v2}, LX/H1c;-><init>(Landroid/content/Context;LX/CNc;)V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->e:LX/H1c;

    .line 2505685
    invoke-static {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->b(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V

    .line 2505686
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x1378b7ac

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2505669
    const v0, 0x7f03046f

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2505670
    const v0, 0x7f0d0be2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->g:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2505671
    const v0, 0x7f0d0be3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 2505672
    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->g:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v4, LX/HoM;

    invoke-direct {v4, p0}, LX/HoM;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2505673
    new-instance v3, LX/HoN;

    invoke-direct {v3, p0}, LX/HoN;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;)V

    .line 2505674
    new-instance v4, LX/1De;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2505675
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0060

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2505676
    invoke-static {v4}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;->e:LX/H1c;

    invoke-virtual {v6, v7}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/5Jz;->a(LX/1Of;)LX/5Jz;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object v3

    new-instance v6, LX/HoP;

    invoke-direct {v6, p0, v5}, LX/HoP;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;I)V

    invoke-virtual {v3, v6}, LX/5Jz;->a(LX/3x6;)LX/5Jz;

    move-result-object v3

    invoke-static {v4, v3}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v3

    invoke-virtual {v3}, LX/1me;->b()LX/1dV;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2505677
    const/16 v0, 0x2b

    const v3, -0x426be8f1

    invoke-static {v8, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
