.class public Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;
.super Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/929;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Z

.field public r:LX/HoK;

.field public s:Lcom/facebook/composer/minutiae/model/MinutiaeObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2505498
    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HoH;)V
    .locals 5

    .prologue
    .line 2505481
    new-instance v0, LX/Hnn;

    invoke-direct {v0}, LX/Hnn;-><init>()V

    move-object v0, v0

    .line 2505482
    sget-object v1, LX/HoH;->TAIL_LOAD:LX/HoH;

    if-ne p1, v1, :cond_0

    .line 2505483
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->p:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2505484
    const-string v1, "after"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2505485
    :cond_0
    sget-object v1, LX/HoH;->HEAD_LOAD:LX/HoH;

    if-ne p1, v1, :cond_1

    .line 2505486
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->o:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2505487
    const-string v1, "before"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2505488
    :cond_1
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2505489
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2505490
    move-object v0, v0

    .line 2505491
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2505492
    new-instance v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;LX/HoH;)V

    .line 2505493
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->b:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2505494
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->a:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task_election_hub_discussion_fetch_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/HoH;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2505495
    :goto_0
    return-void

    .line 2505496
    :cond_2
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ElectionHubTailLoadReq"

    const-string v2, "Tail load requested but no end cursor set"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2505497
    :cond_3
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ElectionHubHeadLoadReq"

    const-string v2, "Head load requested but no start cursor set"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2505467
    invoke-super {p0, p1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(Landroid/os/Bundle;)V

    .line 2505468
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/929;->b(LX/0QB;)LX/929;

    move-result-object v6

    check-cast v6, LX/929;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 v8, 0x17d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object p1

    check-cast p1, LX/1Kf;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v0

    check-cast v0, LX/1Nq;

    iput-object v3, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->a:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->d:LX/929;

    iput-object v7, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->f:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->g:LX/1Kf;

    iput-object v0, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->h:LX/1Nq;

    .line 2505469
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2505480
    const-string v0, "discussion"

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2505476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2505477
    new-instance v1, LX/HoK;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/HoK;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->r:LX/HoK;

    .line 2505478
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->r:LX/HoK;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2505479
    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2505475
    iget-boolean v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->q:Z

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2505470
    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->o:Ljava/lang/String;

    .line 2505471
    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->p:Ljava/lang/String;

    .line 2505472
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->q:Z

    .line 2505473
    invoke-super {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e()V

    .line 2505474
    return-void
.end method
