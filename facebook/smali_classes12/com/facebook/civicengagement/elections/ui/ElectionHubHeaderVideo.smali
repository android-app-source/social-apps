.class public Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hnl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Qq;

.field private g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private h:LX/0g7;

.field private i:LX/HoL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2505570
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2505571
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2505572
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2505573
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2505574
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2505575
    const-class v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

    invoke-static {v0, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2505576
    new-instance v0, LX/HoL;

    invoke-direct {v0}, LX/HoL;-><init>()V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->i:LX/HoL;

    .line 2505577
    const v0, 0x7f03046d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2505578
    const v0, 0x7f0d0d58

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2505579
    new-instance v0, LX/HoJ;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/HoJ;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;Landroid/content/Context;)V

    .line 2505580
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2505581
    new-instance v0, LX/0g7;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v1}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->h:LX/0g7;

    .line 2505582
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->h:LX/0g7;

    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->getAdapter()LX/1Qq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0g7;->a(Landroid/widget/ListAdapter;)V

    .line 2505583
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->h:LX/0g7;

    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->e:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0g7;->a(LX/1St;)V

    .line 2505584
    return-void
.end method

.method private static a(Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;LX/2mn;LX/Hnl;LX/1DS;LX/0Ot;LX/1Db;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;",
            "LX/2mn;",
            "LX/Hnl;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;",
            ">;",
            "LX/1Db;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2505585
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a:LX/2mn;

    iput-object p2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->b:LX/Hnl;

    iput-object p3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->c:LX/1DS;

    iput-object p4, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->e:LX/1Db;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;

    invoke-static {v5}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v1

    check-cast v1, LX/2mn;

    const-class v2, LX/Hnl;

    invoke-interface {v5, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Hnl;

    invoke-static {v5}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v3

    check-cast v3, LX/1DS;

    const/16 v4, 0x18be

    invoke-static {v5, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v5}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v5

    check-cast v5, LX/1Db;

    invoke-static/range {v0 .. v5}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a(Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;LX/2mn;LX/Hnl;LX/1DS;LX/0Ot;LX/1Db;)V

    return-void
.end method

.method private getAdapter()LX/1Qq;
    .locals 7

    .prologue
    .line 2505586
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->f:LX/1Qq;

    if-nez v0, :cond_0

    .line 2505587
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->b:LX/Hnl;

    const-string v1, "unknown"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2505588
    sget-object v3, LX/HoB;->a:LX/HoB;

    move-object v3, v3

    .line 2505589
    new-instance v4, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo$2;

    invoke-direct {v4, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo$2;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;)V

    iget-object v5, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->h:LX/0g7;

    invoke-static {v5}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    new-instance v6, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo$3;

    invoke-direct {v6, p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo$3;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;)V

    invoke-virtual/range {v0 .. v6}, LX/Hnl;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;Ljava/lang/Runnable;)LX/Hnk;

    move-result-object v0

    .line 2505590
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->c:LX/1DS;

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->d:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->i:LX/HoL;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2505591
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2505592
    move-object v0, v1

    .line 2505593
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->f:LX/1Qq;

    .line 2505594
    :cond_0
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->f:LX/1Qq;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 2505595
    if-nez p1, :cond_1

    .line 2505596
    :cond_0
    :goto_0
    return-void

    .line 2505597
    :cond_1
    invoke-static {p1}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2505598
    if-eqz v0, :cond_0

    .line 2505599
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->a:LX/2mn;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 2505600
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2505601
    iget v2, v0, LX/3FO;->a:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2505602
    iget v0, v0, LX/3FO;->b:I

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2505603
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2505604
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2505605
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->i:LX/HoL;

    invoke-virtual {v0, p1}, LX/HoL;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2505606
    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubHeaderVideo;->getAdapter()LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0
.end method
