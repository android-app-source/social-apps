.class public Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;
.super Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2505821
    invoke-direct {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HoH;)V
    .locals 5

    .prologue
    .line 2505804
    new-instance v0, LX/Hnq;

    invoke-direct {v0}, LX/Hnq;-><init>()V

    move-object v0, v0

    .line 2505805
    sget-object v1, LX/HoH;->TAIL_LOAD:LX/HoH;

    if-ne p1, v1, :cond_0

    .line 2505806
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2505807
    const-string v1, "after"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2505808
    :cond_0
    sget-object v1, LX/HoH;->HEAD_LOAD:LX/HoH;

    if-ne p1, v1, :cond_1

    .line 2505809
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2505810
    const-string v1, "before"

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2505811
    :cond_1
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2505812
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2505813
    move-object v0, v0

    .line 2505814
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2505815
    new-instance v1, LX/HoS;

    invoke-direct {v1, p0, p1}, LX/HoS;-><init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;LX/HoH;)V

    .line 2505816
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->b:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2505817
    iget-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->a:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task_election_hub_videos_fetch_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/HoH;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2505818
    :goto_0
    return-void

    .line 2505819
    :cond_2
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ElectionHubTailLoadReq"

    const-string v2, "Tail load requested but no end cursor set"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2505820
    :cond_3
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ElectionHubHeadLoadReq"

    const-string v2, "Head load requested but no start cursor set"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2505793
    invoke-super {p0, p1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(Landroid/os/Bundle;)V

    .line 2505794
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    const/16 v0, 0x259

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->a:LX/1Ck;

    iput-object v3, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->b:LX/0tX;

    iput-object p1, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->c:LX/0Ot;

    .line 2505795
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2505803
    const-string v0, "video"

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2505802
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2505801
    iget-boolean v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->f:Z

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2505796
    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->d:Ljava/lang/String;

    .line 2505797
    iput-object v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->e:Ljava/lang/String;

    .line 2505798
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->f:Z

    .line 2505799
    invoke-super {p0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->e()V

    .line 2505800
    return-void
.end method
