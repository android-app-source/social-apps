.class public Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "Lcom/facebook/civicengagement/elections/environment/ElectionHubEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

.field private final b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2505160
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2505161
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->a:Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    .line 2505162
    iput-object p2, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    .line 2505163
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;
    .locals 5

    .prologue
    .line 2505164
    const-class v1, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;

    monitor-enter v1

    .line 2505165
    :try_start_0
    sget-object v0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2505166
    sput-object v2, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2505167
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505168
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2505169
    new-instance p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;-><init>(Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;)V

    .line 2505170
    move-object v0, p0

    .line 2505171
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2505172
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2505173
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2505174
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2505175
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2505176
    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2505177
    iget-object v1, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->a:Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    new-instance v2, LX/Ho5;

    invoke-direct {v2, v0}, LX/Ho5;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p1, v1, v2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubVideoHeaderPartDefinition;->b:Lcom/facebook/feedplugins/video/FullscreenVideoAttachmentPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2505178
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2505179
    const/4 v0, 0x1

    return v0
.end method
