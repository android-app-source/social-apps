.class public Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Ho5;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation runtime Lcom/facebook/video/abtest/VideoInline;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoInline;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2505155
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2505156
    iput-object p1, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->a:LX/0Ot;

    .line 2505157
    iput-object p2, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->b:LX/0Or;

    .line 2505158
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;
    .locals 5

    .prologue
    .line 2505144
    const-class v1, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    monitor-enter v1

    .line 2505145
    :try_start_0
    sget-object v0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2505146
    sput-object v2, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2505147
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2505148
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2505149
    new-instance v3, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;

    const/16 v4, 0xa59

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x382

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;-><init>(LX/0Ot;LX/0Or;)V

    .line 2505150
    move-object v0, v3

    .line 2505151
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2505152
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2505153
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2505154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2505159
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2505140
    check-cast p2, LX/Ho5;

    .line 2505141
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 2505142
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3EE;

    iget-object v3, p2, LX/Ho5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2505143
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2505136
    check-cast p1, LX/Ho5;

    const/4 v1, 0x0

    .line 2505137
    iget-object v0, p0, Lcom/facebook/civicengagement/elections/multirow/ElectionHubRichVideoRowPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/Ho5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2505138
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2505139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
