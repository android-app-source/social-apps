.class public Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/I6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I6r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I6k;

.field private e:Landroid/support/v7/widget/RecyclerView;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2537943
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;

    const-class v1, LX/I6l;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/I6l;

    new-instance v0, LX/I6r;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v4

    check-cast v4, LX/1My;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {v0, v2, v4, p0}, LX/I6r;-><init>(LX/0tX;LX/1My;LX/1Ck;)V

    move-object v2, v0

    check-cast v2, LX/I6r;

    invoke-static {v3}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    iput-object v1, p1, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->a:LX/I6l;

    iput-object v2, p1, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->b:LX/I6r;

    iput-object v3, p1, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->c:LX/0zG;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2537916
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2537917
    const-class v0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2537918
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2537919
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->f:Ljava/lang/String;

    .line 2537920
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->a:LX/I6l;

    iget-object v1, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->f:Ljava/lang/String;

    .line 2537921
    new-instance p1, LX/I6k;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {p1, v1, v2}, LX/I6k;-><init>(Ljava/lang/String;LX/0tX;)V

    .line 2537922
    move-object v0, p1

    .line 2537923
    iput-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->d:LX/I6k;

    .line 2537924
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x625c0452

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2537925
    const v1, 0x7f030586

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x31841cd5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2f781bad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2537926
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2537927
    iget-object v1, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->b:LX/I6r;

    .line 2537928
    iget-object v2, v1, LX/I6r;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2537929
    const/16 v1, 0x2b

    const v2, 0x581e08d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7baa497e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2537930
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2537931
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->c:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f083805

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2537932
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->b:LX/I6r;

    iget-object v2, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->f:Ljava/lang/String;

    new-instance v4, LX/I6m;

    invoke-direct {v4, p0}, LX/I6m;-><init>(Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;)V

    .line 2537933
    iput-object v4, v0, LX/I6r;->d:LX/I6m;

    .line 2537934
    new-instance v5, LX/I6p;

    invoke-direct {v5, v0, v2}, LX/I6p;-><init>(LX/I6r;Ljava/lang/String;)V

    .line 2537935
    new-instance v6, LX/I6q;

    invoke-direct {v6, v0, v2}, LX/I6q;-><init>(LX/I6r;Ljava/lang/String;)V

    .line 2537936
    iget-object v7, v0, LX/I6r;->c:LX/1Ck;

    const-string p0, "fetchNotificationSettings"

    invoke-virtual {v7, p0, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2537937
    const/16 v0, 0x2b

    const v2, 0x55bac648

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2537938
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2537939
    const v0, 0x7f0d0f4b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2537940
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2537941
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->d:LX/I6k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2537942
    return-void
.end method
