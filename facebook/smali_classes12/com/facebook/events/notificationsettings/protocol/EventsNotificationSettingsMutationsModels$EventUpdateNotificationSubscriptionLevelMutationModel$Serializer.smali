.class public final Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2538441
    const-class v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel;

    new-instance v1, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2538442
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2538457
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2538444
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2538445
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2538446
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2538447
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2538448
    if-eqz v2, :cond_0

    .line 2538449
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538450
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2538451
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2538452
    if-eqz v2, :cond_1

    .line 2538453
    const-string p0, "event"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538454
    invoke-static {v1, v2, p1}, LX/I73;->a(LX/15i;ILX/0nX;)V

    .line 2538455
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2538456
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2538443
    check-cast p1, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$Serializer;->a(Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
