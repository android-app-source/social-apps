.class public final Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x66ffa261
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2538431
    const-class v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2538432
    const-class v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2538429
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2538430
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2538426
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2538427
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2538428
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 2538419
    iput-object p1, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->f:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 2538420
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2538421
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2538422
    if-eqz v0, :cond_0

    .line 2538423
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2538424
    :cond_0
    return-void

    .line 2538425
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2538417
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->e:Ljava/lang/String;

    .line 2538418
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2538415
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->f:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->f:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 2538416
    iget-object v0, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->f:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2538433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2538434
    invoke-direct {p0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2538435
    invoke-direct {p0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->k()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2538436
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2538437
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2538438
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2538439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2538440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2538412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2538413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2538414
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2538411
    new-instance v0, LX/I71;

    invoke-direct {v0, p1}, LX/I71;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2538410
    invoke-direct {p0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2538404
    const-string v0, "viewer_notification_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538405
    invoke-direct {p0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->k()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2538406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2538407
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2538408
    :goto_0
    return-void

    .line 2538409
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2538401
    const-string v0, "viewer_notification_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538402
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    .line 2538403
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2538398
    new-instance v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;

    invoke-direct {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsMutationsModels$EventUpdateNotificationSubscriptionLevelMutationModel$EventModel;-><init>()V

    .line 2538399
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2538400
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2538396
    const v0, 0x1cc19702

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2538397
    const v0, 0x403827a

    return v0
.end method
