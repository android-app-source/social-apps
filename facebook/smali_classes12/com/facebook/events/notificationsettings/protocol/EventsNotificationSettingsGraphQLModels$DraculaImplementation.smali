.class public final Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2538084
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2538085
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2538018
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2538019
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2538064
    if-nez p1, :cond_0

    .line 2538065
    :goto_0
    return v0

    .line 2538066
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2538067
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2538068
    :sswitch_0
    const-class v1, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2538069
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2538070
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2538071
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2538072
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2538073
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2538074
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2538075
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2538076
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2538077
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v3

    .line 2538078
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2538079
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2538080
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2538081
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2538082
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2538083
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2e98368b -> :sswitch_0
        0xd89bab8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2538063
    new-instance v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2538059
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2538060
    if-eqz v0, :cond_0

    .line 2538061
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2538062
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2538054
    sparse-switch p2, :sswitch_data_0

    .line 2538055
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2538056
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2538057
    invoke-static {v0, p3}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 2538058
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2e98368b -> :sswitch_0
        0xd89bab8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2538053
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2538051
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2538052
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2538046
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2538047
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2538048
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2538049
    iput p2, p0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->b:I

    .line 2538050
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2538045
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2538044
    new-instance v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2538041
    iget v0, p0, LX/1vt;->c:I

    .line 2538042
    move v0, v0

    .line 2538043
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2538038
    iget v0, p0, LX/1vt;->c:I

    .line 2538039
    move v0, v0

    .line 2538040
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2538035
    iget v0, p0, LX/1vt;->b:I

    .line 2538036
    move v0, v0

    .line 2538037
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2538032
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2538033
    move-object v0, v0

    .line 2538034
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2538023
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2538024
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2538025
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2538026
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2538027
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2538028
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2538029
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2538030
    invoke-static {v3, v9, v2}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2538031
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2538020
    iget v0, p0, LX/1vt;->c:I

    .line 2538021
    move v0, v0

    .line 2538022
    return v0
.end method
