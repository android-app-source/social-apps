.class public Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/permalink/multirow/environment/EventPermalinkEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/IBD;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546706
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2546707
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a:Ljava/util/Map;

    .line 2546708
    sget-object v0, LX/IBD;->a:LX/IBD;

    invoke-direct {p0, v0, p1}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/IBD;LX/0Ot;)V

    .line 2546709
    sget-object v0, LX/IBD;->c:LX/IBD;

    invoke-direct {p0, v0, p3}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/IBD;LX/0Ot;)V

    .line 2546710
    sget-object v0, LX/IBD;->b:LX/IBD;

    invoke-direct {p0, v0, p4}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/IBD;LX/0Ot;)V

    .line 2546711
    sget-object v0, LX/IBD;->e:LX/IBD;

    invoke-direct {p0, v0, p2}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/IBD;LX/0Ot;)V

    .line 2546712
    return-void
.end method

.method private a(LX/1RF;LX/IBD;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "Lcom/facebook/events/permalink/multirow/environment/EventPermalinkEnvironment;",
            ">;",
            "LX/IBD",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2546713
    iget-object v0, p0, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    .line 2546714
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Part definition is not specified for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546715
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2546716
    invoke-virtual {p1, v0, p3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2546717
    return-void
.end method

.method private a(LX/IBD;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Props:",
            "Ljava/lang/Object;",
            ">(",
            "LX/IBD",
            "<TProps;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<-TProps;+",
            "LX/1PW;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2546718
    iget-object v0, p0, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546719
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2546720
    instance-of v0, p2, LX/IBE;

    if-eqz v0, :cond_1

    .line 2546721
    check-cast p2, LX/IBE;

    .line 2546722
    iget-object v0, p2, LX/IBE;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2546723
    iget-object v1, p2, LX/IBE;->b:LX/IBD;

    move-object v1, v1

    .line 2546724
    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/1RF;LX/IBD;Ljava/lang/Object;)V

    .line 2546725
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2546726
    :cond_1
    instance-of v0, p2, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    .line 2546727
    sget-object v0, LX/IBD;->b:LX/IBD;

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;->a(LX/1RF;LX/IBD;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2546728
    const/4 v0, 0x1

    return v0
.end method
