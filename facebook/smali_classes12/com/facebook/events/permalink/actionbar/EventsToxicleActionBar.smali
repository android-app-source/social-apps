.class public Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;
.super LX/AhO;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0kv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/events/model/Event;

.field private d:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2540973
    invoke-direct {p0, p1}, LX/AhO;-><init>(Landroid/content/Context;)V

    .line 2540974
    invoke-direct {p0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a()V

    .line 2540975
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2540970
    invoke-direct {p0, p1, p2}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2540971
    invoke-direct {p0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a()V

    .line 2540972
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2540967
    invoke-direct {p0, p1, p2, p3}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2540968
    invoke-direct {p0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a()V

    .line 2540969
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2540943
    const-class v0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2540944
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->setGravity(I)V

    .line 2540945
    const v0, 0x7f021903

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->setBackgroundResource(I)V

    .line 2540946
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->setVisibility(I)V

    .line 2540947
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;LX/0Zb;LX/0kv;)V
    .locals 0

    .prologue
    .line 2540966
    iput-object p1, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->b:LX/0kv;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v1}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v1

    check-cast v1, LX/0kv;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a(Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;LX/0Zb;LX/0kv;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 2540963
    iput-object p1, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->c:Lcom/facebook/events/model/Event;

    .line 2540964
    iput-object p2, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2540965
    return-void
.end method

.method public final b_(LX/3v0;)V
    .locals 3

    .prologue
    .line 2540948
    iget-object v0, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->a:LX/0Zb;

    const-string v1, "event_action_bar_overflow_button_click"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2540949
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2540950
    const-string v0, "event_permalink"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2540951
    iget-object v0, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->b:LX/0kv;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2540952
    const-string v0, "Event"

    invoke-virtual {v1, v0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2540953
    iget-object v0, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->c:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->c:Lcom/facebook/events/model/Event;

    .line 2540954
    iget-object v2, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2540955
    :goto_0
    invoke-virtual {v1, v0}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2540956
    iget-object v0, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v0, :cond_0

    .line 2540957
    const-string v0, "ref_module"

    iget-object v2, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2540958
    const-string v0, "source_module"

    iget-object v2, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2540959
    const-string v0, "ref_mechanism"

    iget-object v2, p0, Lcom/facebook/events/permalink/actionbar/EventsToxicleActionBar;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2540960
    :cond_0
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2540961
    :cond_1
    return-void

    .line 2540962
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
