.class public Lcom/facebook/events/permalink/draft/DraftEventBannerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/text/style/MetricAffectingSpan;

.field private e:Landroid/text/style/MetricAffectingSpan;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Landroid/content/res/Resources;

.field private h:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542735
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2542736
    invoke-direct {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a()V

    .line 2542737
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542732
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542733
    invoke-direct {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a()V

    .line 2542734
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2542729
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2542730
    invoke-direct {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a()V

    .line 2542731
    return-void
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2542684
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542685
    iget-object v1, v0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v0, v1

    .line 2542686
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v2, v0

    .line 2542687
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 2542688
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->c:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->i(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2542689
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2542690
    iget-object v1, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542691
    iget-object v2, v1, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v1, v2

    .line 2542692
    iget-object v2, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542693
    iget-object v3, v2, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v2, v3

    .line 2542694
    invoke-virtual {v2, p1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->b:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v2, v6, v3}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2542695
    const-string v2, " "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2542696
    :cond_0
    return-object v0
.end method

.method private a(Ljava/util/Date;J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2542727
    invoke-static {p1, p2, p3}, LX/5O7;->a(Ljava/util/Date;J)Ljava/lang/String;

    move-result-object v0

    .line 2542728
    iget-object v1, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->g:Landroid/content/res/Resources;

    const v2, 0x7f081f82

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2542719
    const-class v0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2542720
    const v0, 0x7f0304f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2542721
    invoke-virtual {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->g:Landroid/content/res/Resources;

    .line 2542722
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->setOrientation(I)V

    .line 2542723
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0618

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->d:Landroid/text/style/MetricAffectingSpan;

    .line 2542724
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0619

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->e:Landroid/text/style/MetricAffectingSpan;

    .line 2542725
    const v0, 0x7f0d0e39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2542726
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/draft/DraftEventBannerView;LX/0SG;LX/0W9;LX/6RZ;)V
    .locals 0

    .prologue
    .line 2542718
    iput-object p1, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a:LX/0SG;

    iput-object p2, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->b:LX/0W9;

    iput-object p3, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->c:LX/6RZ;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {v2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {v2}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v2

    check-cast v2, LX/6RZ;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Lcom/facebook/events/permalink/draft/DraftEventBannerView;LX/0SG;LX/0W9;LX/6RZ;)V

    return-void
.end method

.method private getBannerText()Landroid/text/SpannableStringBuilder;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 2542704
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->g:Landroid/content/res/Resources;

    const v1, 0x7f081ed5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2542705
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->g:Landroid/content/res/Resources;

    const v2, 0x7f081ed6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2542706
    iget-object v2, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_0

    .line 2542707
    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542708
    iget-wide v7, v3, Lcom/facebook/events/model/Event;->A:J

    move-wide v4, v7

    .line 2542709
    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 2542710
    invoke-static {v2}, LX/5O7;->a(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2542711
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Ljava/util/Date;J)Ljava/lang/String;

    move-result-object v1

    .line 2542712
    invoke-direct {p0, v2}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2542713
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2542714
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2542715
    iget-object v3, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->d:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2542716
    iget-object v3, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->e:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v3, v1, v0, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2542717
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2542697
    iput-object p1, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542698
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->h:Lcom/facebook/events/model/Event;

    .line 2542699
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, v1

    .line 2542700
    if-nez v0, :cond_0

    .line 2542701
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->setVisibility(I)V

    .line 2542702
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->getBannerText()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542703
    return-void
.end method
