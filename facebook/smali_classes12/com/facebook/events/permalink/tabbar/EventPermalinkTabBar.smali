.class public Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public b:LX/I8d;

.field private c:LX/5O6;

.field public d:LX/IBr;

.field private e:[LX/IBr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2547695
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2547696
    invoke-direct {p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a()V

    .line 2547697
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547744
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2547745
    invoke-direct {p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a()V

    .line 2547746
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547741
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2547742
    invoke-direct {p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a()V

    .line 2547743
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2547730
    const v0, 0x7f030503

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2547731
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->c:LX/5O6;

    .line 2547732
    const v0, 0x7f0d0e44

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2547733
    const v1, 0x7f0d0e45

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2547734
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/facebook/fbui/widget/text/BadgeTextView;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    iput-object v3, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2547735
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setSelected(Z)V

    .line 2547736
    new-instance v1, LX/IBp;

    invoke-direct {v1, p0}, LX/IBp;-><init>(Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;)V

    .line 2547737
    iget-object v3, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v2, v3, v0

    .line 2547738
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2547740
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2547722
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    array-length v0, v0

    iget-object v2, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->e:[LX/IBr;

    array-length v2, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "The number of TabTypes should equal the number of tabs!"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2547723
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2547724
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    aget-object v0, v0, v1

    .line 2547725
    iget-object v2, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->e:[LX/IBr;

    aget-object v2, v2, v1

    invoke-virtual {v2}, LX/IBr;->getTabNameStringRes()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(I)V

    .line 2547726
    iget-object v2, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->e:[LX/IBr;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTag(Ljava/lang/Object;)V

    .line 2547727
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2547728
    goto :goto_0

    .line 2547729
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/IBr;)Z
    .locals 1

    .prologue
    .line 2547721
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->d:LX/IBr;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2547717
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2547718
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->c:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->a(Landroid/graphics/Canvas;)V

    .line 2547719
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->c:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->b(Landroid/graphics/Canvas;)V

    .line 2547720
    return-void
.end method

.method public getSelectedTabType()LX/IBr;
    .locals 1

    .prologue
    .line 2547716
    iget-object v0, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->d:LX/IBr;

    return-object v0
.end method

.method public setBadges(Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/IBr;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2547709
    iget-object v2, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2547710
    invoke-virtual {v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2547711
    invoke-virtual {v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2547712
    :goto_1
    invoke-virtual {v4, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2547713
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2547714
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2547715
    :cond_2
    return-void
.end method

.method public setOnTabChangeListener(LX/I8d;)V
    .locals 0

    .prologue
    .line 2547707
    iput-object p1, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->b:LX/I8d;

    .line 2547708
    return-void
.end method

.method public setSelected(LX/IBr;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2547701
    iget-object v3, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a:[Lcom/facebook/fbui/widget/text/BadgeTextView;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2547702
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setSelected(Z)V

    .line 2547703
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2547704
    goto :goto_1

    .line 2547705
    :cond_1
    iput-object p1, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->d:LX/IBr;

    .line 2547706
    return-void
.end method

.method public setTabTypes([LX/IBr;)V
    .locals 0

    .prologue
    .line 2547698
    iput-object p1, p0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->e:[LX/IBr;

    .line 2547699
    invoke-direct {p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->b()V

    .line 2547700
    return-void
.end method
