.class public Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545289
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2545290
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2545287
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2545288
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2545285
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545286
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2545280
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setVisibility(I)V

    .line 2545281
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545282
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a(Ljava/lang/String;)V

    .line 2545283
    :goto_0
    return-void

    .line 2545284
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(LX/0Px;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2545230
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->c(LX/0Px;I)V

    .line 2545231
    invoke-direct {p0, p2}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setGoingShortSummary(I)V

    .line 2545232
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2545276
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2545277
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setVisibility(I)V

    .line 2545278
    invoke-virtual {p0, p1}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setText(Ljava/lang/CharSequence;)V

    .line 2545279
    :cond_0
    return-void
.end method

.method private b(LX/0Px;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2545273
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->d(LX/0Px;I)V

    .line 2545274
    invoke-direct {p0, p2}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setMaybeShortSummary(I)V

    .line 2545275
    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2545272
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2545291
    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    .line 2545292
    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b:Ljava/lang/String;

    .line 2545293
    return-void
.end method

.method private c(LX/0Px;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2545265
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 2545266
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081efc

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    .line 2545267
    :cond_0
    :goto_0
    return-void

    .line 2545268
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 2545269
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081efd

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    goto :goto_0

    .line 2545270
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-le v0, v7, :cond_0

    .line 2545271
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00d6

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x2

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    add-int/lit8 v0, p2, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private d(LX/0Px;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2545258
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 2545259
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081efe

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    .line 2545260
    :cond_0
    :goto_0
    return-void

    .line 2545261
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 2545262
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081eff

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    goto :goto_0

    .line 2545263
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-le v0, v7, :cond_0

    .line 2545264
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00d8

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x2

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    add-int/lit8 v0, p2, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private setGoingShortSummary(I)V
    .locals 5

    .prologue
    .line 2545255
    if-lez p1, :cond_0

    .line 2545256
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00d7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b:Ljava/lang/String;

    .line 2545257
    :cond_0
    return-void
.end method

.method private setMaybeShortSummary(I)V
    .locals 5

    .prologue
    .line 2545252
    if-lez p1, :cond_0

    .line 2545253
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00d9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b:Ljava/lang/String;

    .line 2545254
    :cond_0
    return-void
.end method

.method private setUserOnlySummary(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 2

    .prologue
    .line 2545247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_1

    .line 2545248
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081efa

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    .line 2545249
    :cond_0
    :goto_0
    return-void

    .line 2545250
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_0

    .line 2545251
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081efb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;LX/0Px;ILX/0Px;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2545239
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->c()V

    .line 2545240
    if-lez p3, :cond_0

    .line 2545241
    invoke-direct {p0, p2, p3}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a(LX/0Px;I)V

    .line 2545242
    :goto_0
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a()V

    .line 2545243
    return-void

    .line 2545244
    :cond_0
    if-lez p5, :cond_1

    .line 2545245
    invoke-direct {p0, p4, p5}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b(LX/0Px;I)V

    goto :goto_0

    .line 2545246
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setUserOnlySummary(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2545236
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 2545237
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a()V

    .line 2545238
    return-void
.end method

.method public setDefaultText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2545233
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->b:Ljava/lang/String;

    .line 2545234
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a()V

    .line 2545235
    return-void
.end method
