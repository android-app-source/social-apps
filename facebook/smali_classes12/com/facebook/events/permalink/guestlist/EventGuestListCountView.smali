.class public Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Landroid/widget/ProgressBar;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542885
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2542886
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->c()V

    .line 2542887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542882
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542883
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->c()V

    .line 2542884
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2542866
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2542867
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->c()V

    .line 2542868
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a:LX/154;

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2542888
    const-class v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2542889
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->setOrientation(I)V

    .line 2542890
    const v0, 0x7f0304ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2542891
    const v0, 0x7f0d0e02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2542892
    const v0, 0x7f0d0e04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2542893
    const v0, 0x7f0d0e03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d:Landroid/widget/ProgressBar;

    .line 2542894
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2542880
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a:LX/154;

    iget v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    invoke-virtual {v1, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542881
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2542877
    iget v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    .line 2542878
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d()V

    .line 2542879
    return-void
.end method

.method public final a(LX/I9N;)V
    .locals 2

    .prologue
    .line 2542869
    iget v0, p1, LX/I9N;->a:I

    move v0, v0

    .line 2542870
    iput v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    .line 2542871
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d()V

    .line 2542872
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2542873
    iget-object v1, p1, LX/I9N;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2542874
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542875
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->setInProgress(Z)V

    .line 2542876
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2542863
    iget v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->e:I

    .line 2542864
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d()V

    .line 2542865
    return-void
.end method

.method public setInProgress(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2542857
    if-eqz p1, :cond_0

    .line 2542858
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2542859
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2542860
    :goto_0
    return-void

    .line 2542861
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2542862
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
