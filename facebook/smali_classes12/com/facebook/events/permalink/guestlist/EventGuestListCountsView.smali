.class public Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public d:LX/Ble;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542922
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2542923
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a()V

    .line 2542924
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542919
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542920
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a()V

    .line 2542921
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2542900
    const v0, 0x7f0304cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2542901
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->setOrientation(I)V

    .line 2542902
    const-class v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2542903
    const v0, 0x7f0d0e05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    .line 2542904
    const v0, 0x7f0d0e06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    .line 2542905
    const v0, 0x7f0d0e07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    .line 2542906
    return-void
.end method

.method private a(Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;LX/I9N;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)V
    .locals 1

    .prologue
    .line 2542915
    invoke-virtual {p1, p2}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a(LX/I9N;)V

    .line 2542916
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->setVisibility(I)V

    .line 2542917
    new-instance v0, LX/I9O;

    invoke-direct {v0, p0, p3, p2}, LX/I9O;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/I9N;)V

    invoke-virtual {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542918
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    invoke-static {v0}, LX/Ble;->b(LX/0QB;)LX/Ble;

    move-result-object v0

    check-cast v0, LX/Ble;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->d:LX/Ble;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/I9N;LX/I9N;LX/I9N;)V
    .locals 1

    .prologue
    .line 2542910
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->setVisibility(I)V

    .line 2542911
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    invoke-direct {p0, v0, p2, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;LX/I9N;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)V

    .line 2542912
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    invoke-direct {p0, v0, p3, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;LX/I9N;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)V

    .line 2542913
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    invoke-direct {p0, v0, p4, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;LX/I9N;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)V

    .line 2542914
    return-void
.end method

.method public getCountView1()Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .locals 1

    .prologue
    .line 2542909
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    return-object v0
.end method

.method public getCountView2()Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .locals 1

    .prologue
    .line 2542908
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    return-object v0
.end method

.method public getCountView3()Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;
    .locals 1

    .prologue
    .line 2542907
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    return-object v0
.end method
