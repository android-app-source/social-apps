.class public Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IAH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/I9p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/I9o;

.field public i:I

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/I9X;

.field public final l:LX/I9Y;

.field public m:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2543256
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2543257
    new-instance v0, LX/I9X;

    invoke-direct {v0, p0}, LX/I9X;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->k:LX/I9X;

    .line 2543258
    new-instance v0, LX/I9Y;

    invoke-direct {v0, p0}, LX/I9Y;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->l:LX/I9Y;

    .line 2543259
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2543262
    if-eqz p1, :cond_0

    .line 2543263
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2543264
    if-nez v0, :cond_1

    .line 2543265
    :cond_0
    :goto_0
    return-void

    .line 2543266
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2543267
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    const/4 v3, 0x0

    .line 2543268
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v5, :cond_6

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    .line 2543269
    sget-object v2, LX/I9W;->a:[I

    .line 2543270
    iget-object p1, v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    move-object p1, p1

    .line 2543271
    invoke-virtual {p1}, LX/Blc;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_0

    .line 2543272
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 2543273
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2543274
    :goto_3
    iput-object v2, v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 2543275
    goto :goto_2

    :cond_2
    move-object v2, v3

    goto :goto_3

    .line 2543276
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2543277
    :goto_4
    iput-object v2, v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 2543278
    goto :goto_2

    :cond_3
    move-object v2, v3

    goto :goto_4

    .line 2543279
    :pswitch_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2543280
    :goto_5
    iput-object v2, v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 2543281
    goto :goto_2

    :cond_4
    move-object v2, v3

    goto :goto_5

    .line 2543282
    :pswitch_3
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2543283
    :goto_6
    iput-object v2, v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 2543284
    goto :goto_2

    :cond_5
    move-object v2, v3

    goto :goto_6

    .line 2543285
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->m:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2543286
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private d()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;
    .locals 2

    .prologue
    .line 2543260
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2543261
    const-string v1, "GUEST_LIST_INITIALIZATION_MODEL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    return-object v0
.end method

.method public static e(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2543193
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->d()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v0

    .line 2543194
    iget-object p0, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2543195
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2543191
    const-string v0, "event_guest_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2543196
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2543197
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;

    invoke-static {p1}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {p1}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    invoke-static {p1}, LX/IAH;->b(LX/0QB;)LX/IAH;

    move-result-object v5

    check-cast v5, LX/IAH;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p1}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v8

    check-cast v8, LX/1My;

    const-class v0, LX/I9p;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/I9p;

    iput-object v3, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->b:LX/1nQ;

    iput-object v5, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->c:LX/IAH;

    iput-object v6, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->d:LX/0tX;

    iput-object v7, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->f:LX/1My;

    iput-object p1, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->g:LX/I9p;

    .line 2543198
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->d()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v0

    .line 2543199
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->h:LX/0Px;

    move-object v1, v1

    .line 2543200
    move-object v1, v1

    .line 2543201
    iput-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    .line 2543202
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->g:LX/I9p;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v4

    .line 2543203
    new-instance v5, LX/I9o;

    invoke-static {v1}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v6

    check-cast v6, LX/BiT;

    invoke-static {v1}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v7

    check-cast v7, LX/154;

    move-object v8, v2

    move-object v9, v3

    move v10, v4

    invoke-direct/range {v5 .. v10}, LX/I9o;-><init>(LX/BiT;LX/154;LX/0gc;Landroid/content/Context;Z)V

    .line 2543204
    move-object v1, v5

    .line 2543205
    iput-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    .line 2543206
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    .line 2543207
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2543208
    const/4 v5, 0x0

    .line 2543209
    iput-object v2, v1, LX/I9o;->a:LX/0Px;

    .line 2543210
    if-eqz v2, :cond_0

    .line 2543211
    if-nez v2, :cond_4

    .line 2543212
    const/4 v4, 0x0

    .line 2543213
    :goto_0
    move-object v4, v4

    .line 2543214
    :goto_1
    iput-object v4, v1, LX/I9o;->i:LX/0Px;

    .line 2543215
    iget-object v4, v1, LX/I9o;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    .line 2543216
    new-array v4, v6, [I

    iput-object v4, v1, LX/I9o;->j:[I

    .line 2543217
    new-array v4, v6, [Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iput-object v4, v1, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2543218
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2543219
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    move v4, v5

    .line 2543220
    :goto_2
    if-ge v4, v6, :cond_1

    .line 2543221
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543222
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543223
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2543224
    :cond_0
    sget-object v4, LX/I9o;->b:LX/0Px;

    goto :goto_1

    .line 2543225
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iput-object v4, v1, LX/I9o;->l:LX/0Px;

    .line 2543226
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iput-object v4, v1, LX/I9o;->m:LX/0Px;

    .line 2543227
    :goto_3
    if-ge v5, v6, :cond_2

    .line 2543228
    iget-object v7, v1, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v4, v1, LX/I9o;->i:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Blc;

    .line 2543229
    new-instance v8, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    invoke-direct {v8}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;-><init>()V

    .line 2543230
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 2543231
    const-string v10, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {v4}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v10, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2543232
    invoke-virtual {v8, v9}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2543233
    move-object v4, v8

    .line 2543234
    aput-object v4, v7, v5

    .line 2543235
    iget-object v4, v1, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    aget-object v4, v4, v5

    new-instance v7, LX/I9m;

    invoke-direct {v7, v1, v6}, LX/I9m;-><init>(LX/I9o;I)V

    .line 2543236
    iput-object v7, v4, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->n:LX/I9m;

    .line 2543237
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 2543238
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2543239
    iget-boolean v1, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    move v1, v1

    .line 2543240
    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2543241
    new-instance v0, LX/7oK;

    invoke-direct {v0}, LX/7oK;-><init>()V

    .line 2543242
    const-string v1, "event_id"

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->e(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543243
    new-instance v1, LX/7oK;

    invoke-direct {v1}, LX/7oK;-><init>()V

    move-object v1, v1

    .line 2543244
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2543245
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2543246
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2543247
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2543248
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->e:LX/1Ck;

    const-string v2, "fetch_guest_list_graphql"

    iget-object v3, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->k:LX/I9X;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2543249
    :cond_3
    return-void

    .line 2543250
    :cond_4
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2543251
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_4
    if-ge v6, v8, :cond_5

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    .line 2543252
    iget-object v9, v4, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    move-object v4, v9

    .line 2543253
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543254
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_4

    .line 2543255
    :cond_5
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2543192
    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->e(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x413fd0b9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2543190
    const v1, 0x7f0304d1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4af883c6    # 8143331.0f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 15

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, -0x70eb6e87

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2543156
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2543157
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->j:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    .line 2543158
    iget-object v5, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    move-object v0, v5

    .line 2543159
    invoke-virtual {v0}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543160
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2543161
    :cond_0
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543162
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2543163
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2543164
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2543165
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 2543166
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 2543167
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2543168
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    invoke-virtual {v0, v1}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2543169
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    .line 2543170
    iget-object v11, v2, LX/I9o;->j:[I

    if-eqz v11, :cond_1

    iget-object v11, v2, LX/I9o;->j:[I

    array-length v11, v11

    if-lt v1, v11, :cond_4

    .line 2543171
    :cond_1
    const/4 v11, 0x0

    .line 2543172
    :goto_2
    move v2, v11

    .line 2543173
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543174
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543175
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543176
    iget v2, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->k:I

    move v2, v2

    .line 2543177
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543178
    iget v2, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->m:I

    move v2, v2

    .line 2543179
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543180
    iget v2, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->l:I

    move v0, v2

    .line 2543181
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2543183
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->b:LX/1nQ;

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->e(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget v3, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->i:I

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    const/4 v14, 0x0

    .line 2543184
    iget-object v11, v0, LX/1nQ;->i:LX/0Zb;

    const-string v12, "events_guest_list_session"

    invoke-interface {v11, v12, v14}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v11

    .line 2543185
    invoke-virtual {v11}, LX/0oG;->a()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2543186
    const-string v12, "event_guest_list"

    invoke-virtual {v11, v12}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v11

    iget-object v12, v0, LX/1nQ;->j:LX/0kv;

    iget-object v13, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v12, v13}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v11

    const-string v12, "Event"

    invoke-virtual {v11, v12}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v11

    invoke-virtual {v11, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v11

    const-string v12, "guest_statuses"

    invoke-virtual {v11, v12, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_tab_switches"

    invoke-virtual {v11, v12, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v11

    const-string v12, "count_guest_statuses_tab_viewed"

    invoke-virtual {v11, v12, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_guest_statuses_loaded"

    invoke-virtual {v11, v12, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_guest_statuses_viewed"

    invoke-virtual {v11, v12, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_guest_statuses_profile_viewed"

    invoke-virtual {v11, v12, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_messaged"

    invoke-virtual {v11, v12, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "count_friends_requested"

    invoke-virtual {v11, v12, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v11

    const-string v12, "has_installed_launcher"

    invoke-virtual {v11, v12, v14}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v11

    invoke-virtual {v11}, LX/0oG;->d()V

    .line 2543187
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->f:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 2543188
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2543189
    const v0, 0x2b83c033

    invoke-static {v0, v10}, LX/02F;->f(II)V

    return-void

    :cond_4
    iget-object v11, v2, LX/I9o;->j:[I

    aget v11, v11, v1

    goto/16 :goto_2
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x770c2b61

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2543149
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2543150
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2543151
    if-eqz v0, :cond_0

    .line 2543152
    const v2, 0x7f081f1b

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2543153
    instance-of v2, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    if-eqz v2, :cond_0

    .line 2543154
    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2543155
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x697fffab

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2543122
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2543123
    const v0, 0x7f0d0e0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2543124
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2543125
    const v1, 0x7f0d0e0b

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->m:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2543126
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->m:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2543127
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->m:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v2, LX/I9U;

    invoke-direct {v2, p0}, LX/I9U;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)V

    .line 2543128
    iput-object v2, v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2543129
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    .line 2543130
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2543131
    const-string p1, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Blc;->fromString(Ljava/lang/String;)LX/Blc;

    move-result-object v2

    move-object v2, v2

    .line 2543132
    iget-object p1, v1, LX/I9o;->i:LX/0Px;

    invoke-virtual {p1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result p1

    move v1, p1

    .line 2543133
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2543134
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->d()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v0

    .line 2543135
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->c:LX/IAH;

    const v2, 0x7f0d0e0c

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    .line 2543136
    iget-object p1, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    move-object p1, p1

    .line 2543137
    iput-object p1, v1, LX/IAH;->c:Ljava/lang/String;

    .line 2543138
    iget-boolean p1, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->i:Z

    move p1, p1

    .line 2543139
    iput-boolean p1, v1, LX/IAH;->d:Z

    .line 2543140
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object p1

    invoke-static {p1}, LX/IAH;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result p1

    iput-boolean p1, v1, LX/IAH;->e:Z

    .line 2543141
    iput-object v2, v1, LX/IAH;->g:Landroid/view/View;

    .line 2543142
    iget-boolean p1, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    move p1, p1

    .line 2543143
    if-eqz p1, :cond_0

    iget-object p1, v1, LX/IAH;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f081f83

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, v1, LX/IAH;->i:Ljava/lang/String;

    .line 2543144
    invoke-static {v1}, LX/IAH;->a(LX/IAH;)V

    .line 2543145
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    new-instance v1, LX/I9V;

    invoke-direct {v1, p0}, LX/I9V;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;)V

    .line 2543146
    iput-object v1, v0, LX/I9o;->h:LX/I9V;

    .line 2543147
    return-void

    .line 2543148
    :cond_0
    iget-object p1, v1, LX/IAH;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f081f84

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
