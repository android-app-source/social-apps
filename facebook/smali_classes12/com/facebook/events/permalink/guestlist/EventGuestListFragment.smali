.class public Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/I9S;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I9M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I9l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/widget/listview/BetterListView;

.field public g:LX/I9L;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Landroid/widget/ProgressBar;

.field private final j:Z

.field public k:I

.field public l:I

.field public m:I

.field public n:LX/I9m;

.field public o:Lcom/facebook/events/common/EventActionContext;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2543059
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2543060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->j:Z

    .line 2543061
    return-void
.end method

.method private b(LX/Blc;)I
    .locals 3

    .prologue
    .line 2543070
    sget-object v0, LX/I9R;->a:[I

    invoke-virtual {p1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2543071
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No empty guest list text for currentRsvpType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2543072
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f081f31

    .line 2543073
    :goto_0
    return v0

    .line 2543074
    :cond_0
    const v0, 0x7f081f30

    goto :goto_0

    .line 2543075
    :pswitch_1
    const v0, 0x7f081f36

    goto :goto_0

    .line 2543076
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f081f33

    goto :goto_0

    :cond_1
    const v0, 0x7f081f32

    goto :goto_0

    .line 2543077
    :pswitch_3
    const v0, 0x7f081f3a

    goto :goto_0

    .line 2543078
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f081f35

    goto :goto_0

    :cond_2
    const v0, 0x7f081f34

    goto :goto_0

    .line 2543079
    :pswitch_5
    const v0, 0x7f081f3b

    goto :goto_0

    .line 2543080
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f081f38

    goto :goto_0

    :cond_3
    const v0, 0x7f081f37

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private r()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;
    .locals 2

    .prologue
    .line 2543068
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2543069
    const-string v1, "GUEST_LIST_INITIALIZATION_MODEL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    return-object v0
.end method

.method public static s(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2543065
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->r()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v0

    .line 2543066
    iget-object p0, v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2543067
    return-object v0
.end method

.method private t()LX/Blc;
    .locals 2

    .prologue
    .line 2543063
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2543064
    const-string v1, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Blc;->fromString(Ljava/lang/String;)LX/Blc;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2543062
    const-string v0, "event_guest_list"

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IA6;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2543057
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v0, p1}, LX/I9K;->a(LX/0Px;)V

    .line 2543058
    return-void
.end method

.method public final a(LX/Blc;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2543046
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2543047
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2543048
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_2

    .line 2543049
    :cond_1
    :goto_0
    return-void

    .line 2543050
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v0}, LX/3Tf;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 2543051
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2543052
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->b(LX/Blc;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2543053
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2543054
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000c

    const-string v2, "EventGuestListTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    goto :goto_0

    .line 2543055
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2543056
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2542999
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2543000
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    const-class v4, LX/I9M;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/I9M;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v5

    check-cast v5, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/I9l;->b(LX/0QB;)LX/I9l;

    move-result-object p1

    check-cast p1, LX/I9l;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v3, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->a:LX/1nQ;

    iput-object v4, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->b:LX/I9M;

    iput-object v5, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object p1, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    iput-object v0, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->e:Ljava/lang/Boolean;

    .line 2543001
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000c

    const-string v2, "EventGuestListTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2543002
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->r()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v1

    .line 2543003
    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2543004
    iget-boolean v0, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    move v0, v0

    .line 2543005
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    .line 2543006
    :goto_0
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->b:LX/I9M;

    .line 2543007
    iget-object v3, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->a:Lcom/facebook/events/common/EventActionContext;

    move-object v3, v3

    .line 2543008
    iget-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->o:Lcom/facebook/events/common/EventActionContext;

    if-nez v4, :cond_0

    .line 2543009
    sget-object v4, Lcom/facebook/events/common/ActionSource;->GUESTS_VIEW:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v3, v4}, Lcom/facebook/events/common/EventActionContext;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventActionContext;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->o:Lcom/facebook/events/common/EventActionContext;

    .line 2543010
    :cond_0
    iget-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->o:Lcom/facebook/events/common/EventActionContext;

    move-object v3, v4

    .line 2543011
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->t()LX/Blc;

    move-result-object v4

    .line 2543012
    new-instance v5, LX/I9L;

    invoke-static {v2}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v6

    check-cast v6, LX/2do;

    const-class v7, Landroid/content/Context;

    invoke-interface {v2, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    move-object v8, v3

    move-object v9, v1

    move-object v10, v4

    move v11, v0

    invoke-direct/range {v5 .. v11}, LX/I9L;-><init>(LX/2do;Landroid/content/Context;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;Z)V

    .line 2543013
    move-object v2, v5

    .line 2543014
    iput-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    .line 2543015
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    .line 2543016
    iput-object p0, v2, LX/I9L;->e:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2543017
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->t()LX/Blc;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2543018
    iget-object v4, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2543019
    iput-object v4, v2, LX/I9l;->m:Ljava/lang/String;

    .line 2543020
    iget-boolean v4, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    move v4, v4

    .line 2543021
    iput-boolean v4, v2, LX/I9l;->n:Z

    .line 2543022
    iput-object v3, v2, LX/I9l;->c:LX/Blc;

    .line 2543023
    iput-object p0, v2, LX/I9l;->b:LX/I9S;

    .line 2543024
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v2, LX/I9l;->r:Z

    .line 2543025
    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2543026
    iget-object v4, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eqz v4, :cond_8

    iget-object v4, v1, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    :goto_1
    move-object v4, v4

    .line 2543027
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v7, :cond_1

    .line 2543028
    iget-object v7, v2, LX/I9l;->c:LX/Blc;

    move-object v7, v7

    .line 2543029
    sget-object v8, LX/Blc;->PRIVATE_GOING:LX/Blc;

    if-eq v7, v8, :cond_4

    :cond_1
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v7, :cond_2

    .line 2543030
    iget-object v7, v2, LX/I9l;->c:LX/Blc;

    move-object v7, v7

    .line 2543031
    sget-object v8, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-eq v7, v8, :cond_4

    :cond_2
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v7, :cond_3

    .line 2543032
    iget-object v7, v2, LX/I9l;->c:LX/Blc;

    move-object v7, v7

    .line 2543033
    sget-object v8, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    if-eq v7, v8, :cond_4

    :cond_3
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v4, v7, :cond_9

    .line 2543034
    iget-object v7, v2, LX/I9l;->c:LX/Blc;

    move-object v7, v7

    .line 2543035
    sget-object v8, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    if-ne v7, v8, :cond_9

    :cond_4
    const/4 v7, 0x1

    :goto_2
    move v4, v7

    .line 2543036
    if-eqz v4, :cond_7

    move v4, v5

    .line 2543037
    :goto_3
    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-boolean v7, v2, LX/I9l;->n:Z

    if-eqz v7, :cond_5

    invoke-virtual {v1}, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2543038
    iget-object v7, v2, LX/I9l;->c:LX/Blc;

    move-object v7, v7

    .line 2543039
    sget-object v8, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-eq v7, v8, :cond_5

    move v6, v5

    .line 2543040
    :cond_5
    iget-object v7, v2, LX/I9l;->l:LX/0Uh;

    const/16 v8, 0x3a4

    invoke-virtual {v7, v8, v5}, LX/0Uh;->a(IZ)Z

    move-result v7

    .line 2543041
    iget-object v8, v2, LX/I9l;->l:LX/0Uh;

    const/16 v9, 0x3a9

    invoke-virtual {v8, v9, v5}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 2543042
    invoke-static {v2, v4, v6, v7, v5}, LX/I9l;->a(LX/I9l;ZZZZ)V

    .line 2543043
    return-void

    .line 2543044
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    move v4, v6

    .line 2543045
    goto :goto_3

    :cond_8
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_1

    :cond_9
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/events/model/EventUser;LX/Blc;)V
    .locals 4
    .param p2    # LX/Blc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2543081
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->n:LX/I9m;

    if-eqz v0, :cond_2

    .line 2543082
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->n:LX/I9m;

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->t()LX/Blc;

    move-result-object v1

    .line 2543083
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget v2, v0, LX/I9m;->a:I

    if-ge v3, v2, :cond_2

    .line 2543084
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->m:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2543085
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->l:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2543086
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->i:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v1, :cond_0

    .line 2543087
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->m:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2543088
    :cond_0
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->i:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p2, :cond_1

    .line 2543089
    iget-object v2, v0, LX/I9m;->b:LX/I9o;

    iget-object v2, v2, LX/I9o;->l:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2543090
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2543091
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    if-eqz v0, :cond_3

    .line 2543092
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    .line 2543093
    iget-object v1, p1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2543094
    iget-object v2, v0, LX/I9L;->j:Ljava/util/Map;

    invoke-interface {v2, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2543095
    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2542997
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v0, p1}, LX/I9K;->a(Z)V

    .line 2542998
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2542996
    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->s(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2542988
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v0}, LX/I9L;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2542989
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2542990
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2542991
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2542992
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2542993
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_2

    .line 2542994
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2542995
    :cond_2
    return-void
.end method

.method public final d()I
    .locals 4

    .prologue
    .line 2542982
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    const/4 v1, 0x0

    .line 2542983
    iget-object v2, v0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p0

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, p0, :cond_1

    iget-object v1, v0, LX/I9K;->h:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IA6;

    .line 2542984
    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v3, v1

    .line 2542985
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2542986
    :cond_1
    move v0, v3

    .line 2542987
    goto :goto_0
.end method

.method public final e()I
    .locals 5

    .prologue
    .line 2542942
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    const/4 v1, 0x0

    .line 2542943
    iget-object v2, v0, LX/I9K;->f:[I

    move-object v2, v2

    .line 2542944
    if-eqz v2, :cond_1

    .line 2542945
    iget-object v2, v0, LX/I9K;->f:[I

    move-object v4, v2

    .line 2542946
    array-length p0, v4

    move v2, v1

    :goto_1
    if-ge v2, p0, :cond_1

    aget v3, v4, v2

    .line 2542947
    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v1

    .line 2542948
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    .line 2542949
    :cond_1
    move v0, v1

    .line 2542950
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x550799fd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542981
    const v1, 0x7f0304d0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0xff61ae4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5f81b21d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542976
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    .line 2542977
    iget-object v2, v1, LX/I9L;->l:LX/2do;

    iget-object v4, v1, LX/I9L;->m:LX/I9I;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2542978
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    invoke-virtual {v1}, LX/I9l;->d()V

    .line 2542979
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2542980
    const/16 v1, 0x2b

    const v2, 0x9f6915f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x7c872c5a

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542970
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x6000c

    const-string v3, "EventGuestListTTI"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2542971
    iput-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2542972
    iput-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    .line 2542973
    iput-object v4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2542974
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2542975
    const/16 v1, 0x2b

    const v2, -0x48d90d2b

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x572c7e1a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542962
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2542963
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->a:LX/1nQ;

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->s(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2542964
    iget-boolean v4, v1, LX/1nQ;->b:Z

    if-eqz v4, :cond_0

    .line 2542965
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x25e8d624

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2542966
    :cond_0
    iget-object v4, v1, LX/1nQ;->i:LX/0Zb;

    const-string v5, "view_event_guest_list"

    const/4 p0, 0x0

    invoke-interface {v4, v5, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2542967
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2542968
    const-string v5, "event_permalink"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    iget-object v5, v1, LX/1nQ;->j:LX/0kv;

    iget-object p0, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2542969
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, v1, LX/1nQ;->b:Z

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2542951
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2542952
    const v0, 0x7f0d0e09

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2542953
    const v0, 0x7f0d0e0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->i:Landroid/widget/ProgressBar;

    .line 2542954
    const v0, 0x7f0d0e08

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2542955
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/I9P;

    invoke-direct {v1, p0}, LX/I9P;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2542956
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2542957
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2542958
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->requestLayout()V

    .line 2542959
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    invoke-virtual {v0}, LX/I9l;->c()V

    .line 2542960
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/I9Q;

    invoke-direct {v1, p0}, LX/I9Q;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2542961
    return-void
.end method
