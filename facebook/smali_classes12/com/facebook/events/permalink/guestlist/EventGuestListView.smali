.class public Lcom/facebook/events/permalink/guestlist/EventGuestListView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ble;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/events/model/Event;

.field private j:Z

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field private l:Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;

.field public m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

.field private n:Landroid/view/View;

.field public o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

.field private final p:LX/IAB;

.field private final q:LX/IAC;

.field private final r:LX/IAA;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2544976
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2544977
    new-instance v0, LX/IAB;

    invoke-direct {v0, p0}, LX/IAB;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->p:LX/IAB;

    .line 2544978
    new-instance v0, LX/IAC;

    invoke-direct {v0, p0}, LX/IAC;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->q:LX/IAC;

    .line 2544979
    new-instance v0, LX/IAA;

    invoke-direct {v0, p0}, LX/IAA;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->r:LX/IAA;

    .line 2544980
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a()V

    .line 2544981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2544807
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2544808
    new-instance v0, LX/IAB;

    invoke-direct {v0, p0}, LX/IAB;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->p:LX/IAB;

    .line 2544809
    new-instance v0, LX/IAC;

    invoke-direct {v0, p0}, LX/IAC;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->q:LX/IAC;

    .line 2544810
    new-instance v0, LX/IAA;

    invoke-direct {v0, p0}, LX/IAA;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->r:LX/IAA;

    .line 2544811
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a()V

    .line 2544812
    return-void
.end method

.method private a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2544982
    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v0

    .line 2544983
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c()Z

    move-result v1

    invoke-static {p1, v0, v1}, LX/I9i;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;Z)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;
    .locals 1

    .prologue
    .line 2544984
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2544985
    const-class v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2544986
    const v0, 0x7f0304d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2544987
    const v0, 0x7f0d0e0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->n:Landroid/view/View;

    .line 2544988
    return-void
.end method

.method private a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 2
    .param p1    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2544989
    const v0, 0x7f0d0e00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2544990
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2544991
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->k:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/IA8;

    invoke-direct {v1, p0, p1, p2}, LX/IA8;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544992
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2544993
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    invoke-virtual {v0, p1, p3, p4}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;LX/0Px;LX/0Px;)V

    .line 2544994
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->d:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2544995
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2544996
    invoke-direct {p0, v0, p3, p4}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2544997
    const v0, 0x7f0d0dfc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2544998
    :goto_0
    return-void

    .line 2544999
    :cond_0
    const v0, 0x7f0d0dfc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/permalink/guestlist/EventGuestListView;LX/Bl6;LX/I76;LX/Ble;LX/0SI;LX/14x;LX/1nQ;LX/BiT;LX/DB4;)V
    .locals 0

    .prologue
    .line 2545000
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iput-object p2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b:LX/I76;

    iput-object p3, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c:LX/Ble;

    iput-object p4, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->d:LX/0SI;

    iput-object p5, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->e:LX/14x;

    iput-object p6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->f:LX/1nQ;

    iput-object p7, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->g:LX/BiT;

    iput-object p8, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->h:LX/DB4;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-static {v8}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v1

    check-cast v1, LX/Bl6;

    invoke-static {v8}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v2

    check-cast v2, LX/I76;

    invoke-static {v8}, LX/Ble;->b(LX/0QB;)LX/Ble;

    move-result-object v3

    check-cast v3, LX/Ble;

    invoke-static {v8}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    invoke-static {v8}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v5

    check-cast v5, LX/14x;

    invoke-static {v8}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-static {v8}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v7

    check-cast v7, LX/BiT;

    invoke-static {v8}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v8

    check-cast v8, LX/DB4;

    invoke-static/range {v0 .. v8}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestListView;LX/Bl6;LX/I76;LX/Ble;LX/0SI;LX/14x;LX/1nQ;LX/BiT;LX/DB4;)V

    return-void
.end method

.method private a(Ljava/lang/String;LX/0Px;LX/0Px;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<*>;",
            "LX/0Px",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2544968
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2544969
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v0

    .line 2544970
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2544971
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2544972
    return v0

    :cond_0
    move v0, v1

    .line 2544973
    goto :goto_0

    .line 2544974
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;->a()I

    move-result v2

    goto :goto_1

    .line 2544975
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;->a()I

    move-result v1

    goto :goto_2
.end method

.method private b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 14

    .prologue
    .line 2544929
    const/4 v2, 0x0

    .line 2544930
    const/4 v1, 0x0

    .line 2544931
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2544932
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v2

    .line 2544933
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    .line 2544934
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v1, v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v7, v1

    move v8, v2

    .line 2544935
    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I

    move-result v9

    .line 2544936
    invoke-static/range {p2 .. p2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I

    move-result v10

    .line 2544937
    invoke-static/range {p2 .. p2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->d(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I

    move-result v11

    .line 2544938
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v4

    .line 2544939
    if-eqz v4, :cond_3

    .line 2544940
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2544941
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    .line 2544942
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544943
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2544944
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2544945
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2544946
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v4

    .line 2544947
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v6

    .line 2544948
    if-eqz v6, :cond_5

    .line 2544949
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544950
    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->a()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v13, :cond_4

    invoke-virtual {v12, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model$EdgesModel;

    .line 2544951
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544952
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 2544953
    :cond_3
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v3

    .line 2544954
    const/4 v4, 0x0

    goto :goto_3

    .line 2544955
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2544956
    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->b()I

    move-result v6

    .line 2544957
    :goto_5
    move-object/from16 v0, p2

    invoke-direct {p0, p1, v0, v3, v5}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;LX/0Px;)V

    .line 2544958
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->l:Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;LX/0Px;ILX/0Px;I)V

    .line 2544959
    new-instance v2, LX/Blg;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/Blg;-><init>(Ljava/lang/String;)V

    .line 2544960
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/Blg;->a(Ljava/lang/String;)LX/Blg;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/Blg;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)LX/Blg;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/Blg;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)LX/Blg;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/Blg;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/Blg;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/Blg;->a(Z)LX/Blg;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/Blg;->b(Z)LX/Blg;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/Blg;->a(LX/0Px;)LX/Blg;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, LX/Blg;->a(Lcom/facebook/events/common/EventActionContext;)LX/Blg;

    .line 2544961
    new-instance v3, LX/I9N;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f081f00

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Blc;

    invoke-direct {v3, v10, v4, v1}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544962
    new-instance v4, LX/I9N;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v5, 0x7f081f01

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Blc;

    invoke-direct {v4, v11, v5, v1}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544963
    new-instance v5, LX/I9N;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f081f02

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v1

    const/4 v7, 0x2

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Blc;

    invoke-direct {v5, v9, v6, v1}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544964
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    invoke-virtual {v2}, LX/Blg;->a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/I9N;LX/I9N;LX/I9N;)V

    .line 2544965
    return-void

    .line 2544966
    :cond_5
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v5

    .line 2544967
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_6
    move v7, v1

    move v8, v2

    goto/16 :goto_1
.end method

.method public static b(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)Z
    .locals 2

    .prologue
    .line 2544928
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->i:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    return v0
.end method

.method private static c(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2544921
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v0

    .line 2544922
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2544923
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2544924
    return v0

    :cond_0
    move v0, v1

    .line 2544925
    goto :goto_0

    .line 2544926
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;->a()I

    move-result v2

    goto :goto_1

    .line 2544927
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;->a()I

    move-result v1

    goto :goto_2
.end method

.method private c(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 11

    .prologue
    .line 2544859
    const/4 v1, 0x0

    .line 2544860
    const/4 v0, 0x0

    .line 2544861
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2544862
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v1

    .line 2544863
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v0

    .line 2544864
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v2, v1

    move v1, v0

    .line 2544865
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v0

    move v3, v0

    .line 2544866
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;->a()I

    move-result v0

    move v4, v0

    .line 2544867
    :goto_3
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v0

    move v5, v0

    .line 2544868
    :goto_4
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    .line 2544869
    if-eqz v0, :cond_6

    .line 2544870
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2544871
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_5
    if-ge v6, v9, :cond_5

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    .line 2544872
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544873
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 2544874
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2544875
    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 2544876
    :cond_3
    const/4 v0, 0x0

    move v4, v0

    goto :goto_3

    .line 2544877
    :cond_4
    const/4 v0, 0x0

    move v5, v0

    goto :goto_4

    .line 2544878
    :cond_5
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v6, v0

    .line 2544879
    :goto_6
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    .line 2544880
    if-eqz v0, :cond_8

    .line 2544881
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 2544882
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v7, v0

    :goto_7
    if-ge v7, v10, :cond_7

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;

    .line 2544883
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544884
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_7

    .line 2544885
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2544886
    move-object v6, v0

    goto :goto_6

    .line 2544887
    :cond_7
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2544888
    :goto_8
    invoke-direct {p0, p1, p2, v6, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;LX/0Px;)V

    .line 2544889
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->l:Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;

    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->h:LX/DB4;

    invoke-virtual {v6, p1}, LX/DB4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;->setDefaultText(Ljava/lang/String;)V

    .line 2544890
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f081f00

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2544891
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v7, 0x7f081f03

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2544892
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v8, 0x7f081f02

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2544893
    new-instance v9, LX/Blg;

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, LX/Blg;-><init>(Ljava/lang/String;)V

    .line 2544894
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v0

    .line 2544895
    iput-object v0, v9, LX/Blg;->c:Ljava/lang/String;

    .line 2544896
    move-object v0, v9

    .line 2544897
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v10

    .line 2544898
    iput-object v10, v0, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2544899
    move-object v0, v0

    .line 2544900
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v10

    .line 2544901
    iput-object v10, v0, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2544902
    move-object v0, v0

    .line 2544903
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v10

    .line 2544904
    iput-object v10, v0, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2544905
    move-object v0, v0

    .line 2544906
    iput-boolean v2, v0, LX/Blg;->g:Z

    .line 2544907
    move-object v0, v0

    .line 2544908
    iput-boolean v1, v0, LX/Blg;->i:Z

    .line 2544909
    move-object v0, v0

    .line 2544910
    invoke-direct {p0, p2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v1

    .line 2544911
    iput-object v1, v0, LX/Blg;->h:LX/0Px;

    .line 2544912
    move-object v0, v0

    .line 2544913
    iput-object p3, v0, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2544914
    new-instance v1, LX/I9N;

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    invoke-direct {v1, v4, v7, v0}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544915
    new-instance v2, LX/I9N;

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    invoke-direct {v2, v3, v6, v0}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544916
    new-instance v3, LX/I9N;

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v0

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    invoke-direct {v3, v5, v8, v0}, LX/I9N;-><init>(ILjava/lang/String;LX/Blc;)V

    .line 2544917
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    invoke-virtual {v9}, LX/Blg;->a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v4

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/I9N;LX/I9N;LX/I9N;)V

    .line 2544918
    return-void

    .line 2544919
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2544920
    goto/16 :goto_8

    :cond_9
    move v2, v1

    move v1, v0

    goto/16 :goto_1
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2544856
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->i:Lcom/facebook/events/model/Event;

    .line 2544857
    iget-object p0, v0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v0, p0

    .line 2544858
    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v0

    return v0
.end method

.method private static d(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2544849
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a()I

    move-result v0

    .line 2544850
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2544851
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2544852
    return v0

    :cond_0
    move v0, v1

    .line 2544853
    goto :goto_0

    .line 2544854
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;->a()I

    move-result v2

    goto :goto_1

    .line 2544855
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;->a()I

    move-result v1

    goto :goto_2
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2544844
    const v0, 0x7f0d0dff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544845
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->p:LX/IAB;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2544846
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->q:LX/IAC;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2544847
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->r:LX/IAA;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2544848
    return-void
.end method

.method public static getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2544843
    iget-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->j:Z

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c()Z

    move-result v1

    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/I9i;->a(ZZZ)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 2
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2544819
    if-nez p2, :cond_0

    .line 2544820
    :goto_0
    return-void

    .line 2544821
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2544822
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->setVisibility(I)V

    goto :goto_0

    .line 2544823
    :cond_1
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->i:Lcom/facebook/events/model/Event;

    .line 2544824
    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->setVisibility(I)V

    .line 2544825
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2544826
    const/4 v1, 0x1

    move v1, v1

    .line 2544827
    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->j:Z

    .line 2544828
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->n:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 2544829
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->n:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->removeView(Landroid/view/View;)V

    .line 2544830
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->n:Landroid/view/View;

    .line 2544831
    const v0, 0x7f0304cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2544832
    const v0, 0x7f0d0dfe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->l:Lcom/facebook/events/permalink/guestlist/PlaintextGuestSummaryView;

    .line 2544833
    const v0, 0x7f0d0dfd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    .line 2544834
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->d()V

    .line 2544835
    invoke-static {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2544836
    iget-object v0, p1, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v0, v0

    .line 2544837
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->e:LX/14x;

    const-string v1, "19.0"

    invoke-virtual {v0, v1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2544838
    invoke-direct {p0, p2, p3}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    .line 2544839
    :cond_3
    new-instance v0, LX/IA7;

    invoke-direct {v0, p0, p2, p3}, LX/IA7;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544840
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->j:Z

    if-eqz v0, :cond_5

    .line 2544841
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    goto :goto_0

    .line 2544842
    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x42aec131

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544813
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    if-eqz v1, :cond_0

    .line 2544814
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->r:LX/IAA;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2544815
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->q:LX/IAC;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2544816
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->p:LX/IAB;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2544817
    :cond_0
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 2544818
    const/16 v1, 0x2d

    const v2, -0x1db03f7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
