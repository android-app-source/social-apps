.class public Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;
.super Lcom/facebook/fbui/facepile/FacepileGridView;
.source ""


# instance fields
.field public b:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/events/model/Event;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2545118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2545119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2545116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2545112
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/facepile/FacepileGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545113
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    .line 2545114
    const-class v0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2545115
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 2545105
    iget v0, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->l:I

    move v0, v0

    .line 2545106
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v1, p1, v1

    .line 2545107
    if-lez v1, :cond_0

    if-ge v1, v0, :cond_1

    .line 2545108
    :cond_0
    const/4 v0, 0x0

    .line 2545109
    :goto_0
    return v0

    :cond_1
    sub-int/2addr v1, v0

    .line 2545110
    iget v2, p0, Lcom/facebook/fbui/facepile/FacepileGridView;->f:I

    move v2, v2

    .line 2545111
    add-int/2addr v0, v2

    div-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(LX/7ob;)LX/6UY;
    .locals 3

    .prologue
    .line 2545101
    invoke-interface {p1}, LX/7ob;->e()LX/1Fb;

    move-result-object v0

    .line 2545102
    if-nez v0, :cond_0

    .line 2545103
    const/4 v0, 0x0

    .line 2545104
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xff

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getGoingBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2545077
    if-nez p0, :cond_0

    .line 2545078
    const/4 v0, 0x0

    .line 2545079
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/6UY;

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v2, p2

    move v4, v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;III)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;LX/BiT;LX/0hB;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;",
            "LX/BiT;",
            "LX/0hB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545100
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->b:LX/BiT;

    iput-object p2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->c:LX/0hB;

    iput-object p3, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->d:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    invoke-static {v2}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v0

    check-cast v0, LX/BiT;

    invoke-static {v2}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    const/16 v3, 0x12cb

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;LX/BiT;LX/0hB;LX/0Or;)V

    return-void
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 2545095
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 2545096
    sparse-switch v0, :sswitch_data_0

    .line 2545097
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown measure mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2545098
    :sswitch_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2545099
    :goto_0
    return v0

    :sswitch_1
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private b(LX/7ob;)LX/6UY;
    .locals 3

    .prologue
    .line 2545091
    invoke-interface {p1}, LX/7ob;->e()LX/1Fb;

    move-result-object v0

    .line 2545092
    if-nez v0, :cond_0

    .line 2545093
    const/4 v0, 0x0

    .line 2545094
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7f

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getMaybeBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(LX/7ob;)LX/6UY;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2545088
    invoke-interface {p0}, LX/7ob;->e()LX/1Fb;

    move-result-object v1

    .line 2545089
    if-nez v1, :cond_0

    .line 2545090
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xff

    invoke-static {v1, v2, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0
.end method

.method private getGoingBadge()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2545085
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020ca8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2545086
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 2545087
    return-object v0
.end method

.method private getMaxFacesOnScreen()I
    .locals 2

    .prologue
    .line 2545083
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->c:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2545084
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(I)I

    move-result v0

    return v0
.end method

.method private getMaybeBadge()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2545080
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020ca8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2545081
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 2545082
    return-object v0
.end method

.method private getViewerFace()LX/6UY;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0xff

    .line 2545061
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2545062
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2545063
    :goto_0
    return-object v0

    .line 2545064
    :cond_0
    iget-boolean v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    invoke-static {v2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2545065
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545066
    iget-object v3, v2, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v3

    .line 2545067
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v3, :cond_1

    .line 2545068
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getGoingBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0

    .line 2545069
    :cond_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v3, :cond_4

    .line 2545070
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0

    .line 2545071
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 2545072
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v3, :cond_3

    .line 2545073
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getGoingBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0

    .line 2545074
    :cond_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v3, :cond_4

    .line 2545075
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7f

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getMaybeBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)LX/6UY;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2545076
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2545047
    iget-boolean v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->g:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 2545048
    iget-object v1, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v1, v1

    .line 2545049
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545050
    iget-object v3, v2, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v3

    .line 2545051
    if-ne v1, v2, :cond_1

    .line 2545052
    :cond_0
    :goto_0
    return-void

    .line 2545053
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2545054
    :cond_2
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545055
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2545056
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getViewerFace()LX/6UY;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2545057
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 2545058
    invoke-super {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 2545059
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->requestLayout()V

    goto :goto_0

    .line 2545060
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/events/model/Event;LX/0Px;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;",
            "LX/0Px",
            "<",
            "LX/7ob;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2545016
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545017
    const/4 v0, 0x1

    move v0, v0

    .line 2545018
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->g:Z

    .line 2545019
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2545020
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545021
    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getMaxFacesOnScreen()I

    move-result v0

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2545022
    if-nez v4, :cond_1

    .line 2545023
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 2545024
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 2545025
    goto :goto_0

    .line 2545026
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-direct {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->getViewerFace()LX/6UY;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545027
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {p2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    .line 2545028
    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v6, v4, :cond_3

    .line 2545029
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(LX/7ob;)LX/6UY;

    move-result-object v0

    .line 2545030
    if-eqz v0, :cond_2

    .line 2545031
    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545032
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2545033
    :cond_3
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_6

    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    .line 2545034
    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v6, v4, :cond_6

    .line 2545035
    iget-boolean v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->g:Z

    if-eqz v6, :cond_5

    .line 2545036
    invoke-static {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->c(LX/7ob;)LX/6UY;

    move-result-object v0

    .line 2545037
    if-eqz v0, :cond_4

    .line 2545038
    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545039
    :cond_4
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 2545040
    :cond_5
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->b(LX/7ob;)LX/6UY;

    move-result-object v0

    .line 2545041
    if-eqz v0, :cond_4

    .line 2545042
    iget-object v6, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2545043
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    :goto_5
    iget-object v1, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 2545044
    invoke-super {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 2545045
    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->requestLayout()V

    goto :goto_1

    :cond_7
    move v2, v1

    .line 2545046
    goto :goto_5
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2545009
    iget-boolean v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->g:Z

    if-eqz v2, :cond_2

    .line 2545010
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545011
    iget-object v3, v2, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v3

    .line 2545012
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    .line 2545013
    iget-object v3, v2, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v3

    .line 2545014
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 2545015
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->f:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2545001
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->b(I)I

    move-result v1

    .line 2545002
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-int v0, v2, v0

    .line 2545003
    invoke-direct {p0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2545004
    if-lez v0, :cond_0

    .line 2545005
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/facepile/FacepileGridView;->setNumCols(I)V

    .line 2545006
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/facepile/FacepileGridView;->onMeasure(II)V

    .line 2545007
    return-void

    .line 2545008
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
