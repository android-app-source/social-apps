.class public Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B9v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/events/model/Event;

.field public f:Lcom/facebook/events/common/EventAnalyticsParams;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:LX/5O6;

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2546636
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2546637
    invoke-direct {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a()V

    .line 2546638
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2546639
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2546640
    invoke-direct {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a()V

    .line 2546641
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2546642
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2546643
    invoke-direct {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a()V

    .line 2546644
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2546645
    const-class v0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2546646
    const v0, 0x7f0304fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2546647
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->h:LX/5O6;

    .line 2546648
    invoke-virtual {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->i:I

    .line 2546649
    const v0, 0x7f0d0e40

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2546650
    iget-object v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->g:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->d:LX/0wM;

    const v2, 0x7f020740

    const v3, -0xc4a668

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1, v4, v4, v4}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2546651
    new-instance v0, LX/IB7;

    invoke-direct {v0, p0}, LX/IB7;-><init>(Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2546652
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;LX/14x;LX/1nQ;LX/B9v;LX/0wM;)V
    .locals 0

    .prologue
    .line 2546653
    iput-object p1, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a:LX/14x;

    iput-object p2, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->b:LX/1nQ;

    iput-object p3, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->c:LX/B9v;

    iput-object p4, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->d:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;

    invoke-static {v3}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-static {v3}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {v3}, LX/B9v;->b(LX/0QB;)LX/B9v;

    move-result-object v2

    check-cast v2, LX/B9v;

    invoke-static {v3}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->a(Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;LX/14x;LX/1nQ;LX/B9v;LX/0wM;)V

    return-void
.end method

.method private setMessageInviterButtonText(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2546654
    iget-object v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f81

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2546655
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 1

    .prologue
    .line 2546656
    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->j:Z

    .line 2546657
    iput-object p1, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->e:Lcom/facebook/events/model/Event;

    .line 2546658
    iput-object p2, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2546659
    iget-object v0, p1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v0

    .line 2546660
    iget-object p1, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, p1

    .line 2546661
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->setMessageInviterButtonText(Ljava/lang/String;)V

    .line 2546662
    return-void

    .line 2546663
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2546664
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2546665
    iget-boolean v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->j:Z

    if-eqz v0, :cond_0

    .line 2546666
    iget-object v0, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->h:LX/5O6;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/events/permalink/messageinviter/EventMessageInviterToxicleView;->i:I

    invoke-virtual {v0, p1, v1, v2}, LX/5O6;->a(Landroid/graphics/Canvas;FI)V

    .line 2546667
    :cond_0
    return-void
.end method
