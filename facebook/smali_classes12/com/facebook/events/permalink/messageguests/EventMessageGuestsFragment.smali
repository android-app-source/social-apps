.class public Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/I9S;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IAu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I9l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Landroid/widget/ProgressBar;

.field private i:Z

.field public j:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

.field public k:LX/IAt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2546395
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2546396
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e:Ljava/util/Set;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    const-class v2, LX/IAu;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/IAu;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/I9l;->b(LX/0QB;)LX/I9l;

    move-result-object p0

    check-cast p0, LX/I9l;

    iput-object v1, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->a:LX/1nQ;

    iput-object v2, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->b:LX/IAu;

    iput-object v3, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object p0, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d:LX/I9l;

    return-void
.end method

.method private static b(LX/Blc;)I
    .locals 3

    .prologue
    .line 2546397
    sget-object v0, LX/IAy;->a:[I

    invoke-virtual {p0}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2546398
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No empty guest list text for currentRsvpType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2546399
    :pswitch_0
    const v0, 0x7f081f30

    .line 2546400
    :goto_0
    return v0

    .line 2546401
    :pswitch_1
    const v0, 0x7f081f32

    goto :goto_0

    .line 2546402
    :pswitch_2
    const v0, 0x7f081f34

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static l(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2546403
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546404
    const-string v1, "EVENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2546405
    const-string v0, "event_message_guests"

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IA6;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2546406
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    invoke-virtual {v0, p1}, LX/I9K;->a(LX/0Px;)V

    .line 2546407
    return-void
.end method

.method public final a(LX/Blc;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2546408
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2546409
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2546410
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_2

    .line 2546411
    :cond_1
    :goto_0
    return-void

    .line 2546412
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    invoke-virtual {v0}, LX/3Tf;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 2546413
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546414
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->b(LX/Blc;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2546415
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2546416
    iget-boolean v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->i:Z

    if-eqz v0, :cond_1

    .line 2546417
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000d

    const-string v2, "EventMessageGuestsTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    goto :goto_0

    .line 2546418
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546419
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2546420
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2546421
    const-class v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2546422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->i:Z

    .line 2546423
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000d

    const-string v2, "EventMessageGuestsTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2546424
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d:LX/I9l;

    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->l(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2546425
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2546426
    const-string v3, "EVENT_IS_HOST"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move v2, v2

    .line 2546427
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e()LX/Blc;

    move-result-object v3

    const/4 p1, 0x0

    .line 2546428
    iput-object v1, v0, LX/I9l;->m:Ljava/lang/String;

    .line 2546429
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v0, LX/I9l;->n:Z

    .line 2546430
    iput-object v3, v0, LX/I9l;->c:LX/Blc;

    .line 2546431
    iput-object p0, v0, LX/I9l;->b:LX/I9S;

    .line 2546432
    iput-boolean p1, v0, LX/I9l;->r:Z

    .line 2546433
    invoke-static {v0, p1, p1, p1, p1}, LX/I9l;->a(LX/I9l;ZZZZ)V

    .line 2546434
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2546435
    instance-of v1, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    const-string v2, "Parent fragment must implement EventMessageGuestsListener"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2546436
    check-cast v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->j:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546437
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->b:LX/IAu;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e()LX/Blc;

    move-result-object v1

    .line 2546438
    new-instance v3, LX/IAt;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v2, v1}, LX/IAt;-><init>(Landroid/content/Context;LX/Blc;)V

    .line 2546439
    move-object v0, v3

    .line 2546440
    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    .line 2546441
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2546392
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    invoke-virtual {v0, p1}, LX/I9K;->a(Z)V

    .line 2546393
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2546394
    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->l(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2546384
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    invoke-virtual {v0}, LX/IAt;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2546385
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2546386
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2546387
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2546388
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2546389
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_2

    .line 2546390
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2546391
    :cond_2
    return-void
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2546383
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/Blc;
    .locals 2

    .prologue
    .line 2546381
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546382
    const-string v1, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Blc;->fromString(Ljava/lang/String;)LX/Blc;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x44fed032

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546379
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2546380
    const v2, 0x7f0304d0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x736f03f5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6654c699

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546376
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d:LX/I9l;

    invoke-virtual {v1}, LX/I9l;->d()V

    .line 2546377
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2546378
    const/16 v1, 0x2b

    const v2, -0x4be40caf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x21965ac5

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546369
    iget-boolean v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->i:Z

    if-eqz v1, :cond_0

    .line 2546370
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x6000d

    const-string v3, "EventMessageGuestsTTI"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2546371
    :cond_0
    iput-object v4, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2546372
    iput-object v4, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    .line 2546373
    iput-object v4, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2546374
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2546375
    const/16 v1, 0x2b

    const v2, -0x7caa61d3

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x222ff6d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546361
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2546362
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->a:LX/1nQ;

    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->l(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2546363
    iget-boolean v4, v1, LX/1nQ;->c:Z

    if-eqz v4, :cond_0

    .line 2546364
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x7e32d191

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2546365
    :cond_0
    iget-object v4, v1, LX/1nQ;->i:LX/0Zb;

    const-string v5, "message_event_guests"

    const/4 p0, 0x0

    invoke-interface {v4, v5, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2546366
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2546367
    const-string v5, "event_permalink"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    iget-object v5, v1, LX/1nQ;->j:LX/0kv;

    iget-object p0, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2546368
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, v1, LX/1nQ;->c:Z

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2546349
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2546350
    const v0, 0x7f0d0e09

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2546351
    const v0, 0x7f0d0e0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->h:Landroid/widget/ProgressBar;

    .line 2546352
    const v0, 0x7f0d0e08

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2546353
    iget-boolean v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->i:Z

    if-eqz v0, :cond_0

    .line 2546354
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IAv;

    invoke-direct {v1, p0}, LX/IAv;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2546355
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->k:LX/IAt;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2546356
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->requestLayout()V

    .line 2546357
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d:LX/I9l;

    invoke-virtual {v0}, LX/I9l;->c()V

    .line 2546358
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IAw;

    invoke-direct {v1, p0}, LX/IAw;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2546359
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IAx;

    invoke-direct {v1, p0}, LX/IAx;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2546360
    return-void
.end method
