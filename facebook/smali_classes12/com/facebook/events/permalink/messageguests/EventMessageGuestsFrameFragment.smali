.class public Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fj;


# instance fields
.field public a:LX/IAH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IB5;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/1ZF;

.field public g:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Z

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2546574
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2546575
    return-void
.end method

.method public static a(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2546487
    if-nez p1, :cond_0

    .line 2546488
    const v0, 0x7f081ef1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2546489
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f081f7c

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    invoke-static {p0}, LX/IAH;->b(LX/0QB;)LX/IAH;

    move-result-object v1

    check-cast v1, LX/IAH;

    invoke-static {p0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v2

    check-cast v2, LX/B9y;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object p0

    check-cast p0, LX/1nQ;

    iput-object v1, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a:LX/IAH;

    iput-object v2, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->b:LX/B9y;

    iput-object p0, p1, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->c:LX/1nQ;

    return-void
.end method

.method public static d(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2546572
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546573
    const-string v1, "EVENT_GUEST_LIST_TYPES"

    invoke-static {v0, v1}, LX/Blc;->readGuestListTypesList(Landroid/os/Bundle;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static n(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2546570
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546571
    const-string v1, "EVENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2546562
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v1}, LX/IB5;->d()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v1}, LX/IB5;->d()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2546563
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->c:LX/1nQ;

    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->n(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/1nQ;->a(Ljava/lang/String;I)V

    .line 2546564
    :goto_0
    return v0

    .line 2546565
    :cond_1
    new-instance v1, LX/IB2;

    invoke-direct {v1, p0}, LX/IB2;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)V

    .line 2546566
    new-instance v2, LX/IB3;

    invoke-direct {v2, p0}, LX/IB3;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)V

    .line 2546567
    new-instance v3, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v4, 0x7f081f6f

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f081f70

    invoke-virtual {v3, v4}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f081f71

    invoke-virtual {v3, v4, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f081f72

    invoke-virtual {v2, v3, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2546568
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2546569
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2546561
    const-string v0, "event_message_guests"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2546546
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2546547
    const-class v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2546548
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v0

    .line 2546549
    new-instance v0, LX/IB5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)LX/0Px;

    move-result-object v3

    .line 2546550
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2546551
    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v4, 0x0

    move v6, v4

    :goto_0
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Blc;

    .line 2546552
    sget-object p1, LX/IB4;->a:[I

    invoke-virtual {v4}, LX/Blc;->ordinal()I

    move-result v4

    aget v4, p1, v4

    packed-switch v4, :pswitch_data_0

    .line 2546553
    :goto_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2546554
    :pswitch_0
    const v4, 0x7f081f1c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2546555
    :pswitch_1
    const v4, 0x7f081f1d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2546556
    :pswitch_2
    const v4, 0x7f081f80

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2546557
    :pswitch_3
    const v4, 0x7f081f1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2546558
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 2546559
    invoke-direct/range {v0 .. v5}, LX/IB5;-><init>(LX/0gc;Landroid/content/Context;Ljava/util/List;LX/0Px;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    .line 2546560
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2546545
    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->n(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2546538
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2546539
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2546540
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2546541
    if-eqz v0, :cond_0

    .line 2546542
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2546543
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2546544
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x267e96c8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546537
    const v1, 0x7f0304de

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6b11aa34

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1c8e4d00

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546535
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2546536
    const/16 v1, 0x2b

    const v2, 0x39820ffb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ca1bb23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546513
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2546514
    const/4 v4, 0x1

    .line 2546515
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    .line 2546516
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    if-nez v1, :cond_0

    .line 2546517
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x76477727

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2546518
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2546519
    iput v4, v1, LX/108;->a:I

    .line 2546520
    move-object v1, v1

    .line 2546521
    const v2, 0x7f081f73

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2546522
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2546523
    move-object v1, v1

    .line 2546524
    const/4 v2, -0x2

    .line 2546525
    iput v2, v1, LX/108;->h:I

    .line 2546526
    move-object v1, v1

    .line 2546527
    iget-boolean v2, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->i:Z

    .line 2546528
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2546529
    move-object v1, v1

    .line 2546530
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->g:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2546531
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    iget-object v2, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->g:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2546532
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    new-instance v2, LX/IB1;

    invoke-direct {v2, p0}, LX/IB1;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2546533
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    iget v2, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->j:I

    invoke-static {p0, v2}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2546534
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->f:LX/1ZF;

    invoke-interface {v1, v4}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2546490
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2546491
    const v0, 0x7f0d0e0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2546492
    iget-object v1, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2546493
    const v1, 0x7f0d0e25

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2546494
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2546495
    const v0, 0x7f0d0e27

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2546496
    const v0, 0x7f0d0e28

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2546497
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a:LX/IAH;

    invoke-static {p0}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->n(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2546498
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2546499
    const-string v3, "EVENT_HAS_SEEN_STATE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move v2, v2

    .line 2546500
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2546501
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2546502
    const-string v4, "EVENT_KIND"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    move-object v3, v3

    .line 2546503
    const v4, 0x7f0d0e26

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    .line 2546504
    iput-object v1, v0, LX/IAH;->c:Ljava/lang/String;

    .line 2546505
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, v0, LX/IAH;->d:Z

    .line 2546506
    invoke-static {v3}, LX/IAH;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result p1

    iput-boolean p1, v0, LX/IAH;->e:Z

    .line 2546507
    iput-object v4, v0, LX/IAH;->g:Landroid/view/View;

    .line 2546508
    iget-object p1, v0, LX/IAH;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f081f83

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, LX/IAH;->i:Ljava/lang/String;

    .line 2546509
    invoke-static {v0}, LX/IAH;->a(LX/IAH;)V

    .line 2546510
    iget-object v0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    new-instance v1, LX/IB0;

    invoke-direct {v1, p0}, LX/IB0;-><init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)V

    .line 2546511
    iput-object v1, v0, LX/IB5;->f:LX/IB0;

    .line 2546512
    return-void
.end method
