.class public Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/DB9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I82;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/text/style/MetricAffectingSpan;

.field private d:Landroid/text/style/MetricAffectingSpan;

.field private e:Landroid/content/res/Resources;

.field public f:Lcom/facebook/fbui/glyph/GlyphView;

.field public g:Lcom/facebook/events/model/Event;

.field public h:LX/I81;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542616
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2542617
    invoke-direct {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a()V

    .line 2542618
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542613
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542614
    invoke-direct {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a()V

    .line 2542615
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2542610
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2542611
    invoke-direct {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a()V

    .line 2542612
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2542595
    const-class v0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2542596
    const v0, 0x7f0304e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2542597
    invoke-virtual {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->e:Landroid/content/res/Resources;

    .line 2542598
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->setOrientation(I)V

    .line 2542599
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2542600
    const v0, 0x7f0a00e3

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->setBackgroundResource(I)V

    .line 2542601
    invoke-virtual {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1456

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2542602
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->setPadding(IIII)V

    .line 2542603
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0618

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->c:Landroid/text/style/MetricAffectingSpan;

    .line 2542604
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0619

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->d:Landroid/text/style/MetricAffectingSpan;

    .line 2542605
    const v0, 0x7f0d0e2f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2542606
    invoke-direct {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getBannerText()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542607
    const v0, 0x7f0d0e30

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2542608
    iget-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-direct {p0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542609
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;LX/DB9;LX/I82;)V
    .locals 0

    .prologue
    .line 2542594
    iput-object p1, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a:LX/DB9;

    iput-object p2, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->b:LX/I82;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-static {v1}, LX/DB9;->b(LX/0QB;)LX/DB9;

    move-result-object v0

    check-cast v0, LX/DB9;

    const-class v2, LX/I82;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/I82;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a(Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;LX/DB9;LX/I82;)V

    return-void
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2542593
    new-instance v0, LX/I9E;

    invoke-direct {v0, p0}, LX/I9E;-><init>(Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;)V

    return-object v0
.end method

.method private getBannerText()Landroid/text/SpannableStringBuilder;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 2542586
    iget-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->e:Landroid/content/res/Resources;

    const v1, 0x7f081ed1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2542587
    iget-object v1, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->e:Landroid/content/res/Resources;

    const v2, 0x7f081ed2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2542588
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2542589
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2542590
    iget-object v3, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->c:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2542591
    iget-object v3, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->d:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2542592
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1

    .prologue
    .line 2542582
    iput-object p1, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->g:Lcom/facebook/events/model/Event;

    .line 2542583
    iget-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->b:LX/I82;

    invoke-virtual {v0, p1}, LX/I82;->a(Lcom/facebook/events/model/Event;)LX/I81;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->h:LX/I81;

    .line 2542584
    iget-object v0, p0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a:LX/DB9;

    invoke-virtual {v0, p1, p2}, LX/DB9;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2542585
    return-void
.end method
