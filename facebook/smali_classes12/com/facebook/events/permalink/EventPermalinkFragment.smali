.class public Lcom/facebook/events/permalink/EventPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0yL;
.implements LX/CfG;
.implements LX/0o8;
.implements LX/0o2;
.implements LX/63S;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final al:Z


# instance fields
.field public A:LX/DCC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/DCI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/DCI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/DCL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/DCL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/DCO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0fz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/DCQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/1CY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0gX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I6W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/3Cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/IBL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/8wR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final aA:LX/I7S;

.field private final aB:LX/I7T;

.field private aC:LX/I7P;

.field private final aD:LX/I7U;

.field private final aE:LX/I7O;

.field public final aF:LX/I7Q;

.field public aG:Ljava/lang/String;

.field public aH:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;

.field private aI:Landroid/content/Context;

.field public aJ:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:LX/I8k;

.field public aL:LX/E93;

.field public aM:LX/1P0;

.field public aN:LX/2ja;

.field public aO:LX/0g7;

.field public aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public aQ:LX/IBq;

.field public aR:Z

.field private aS:Z

.field public aT:Z

.field private aU:LX/5O9;

.field public aV:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field private aW:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public aX:Lcom/facebook/events/model/Event;

.field public aY:Lcom/facebook/events/model/Event;

.field public aZ:LX/0ta;

.field public aa:LX/I8l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ab:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Amz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ac:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/E94;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ae:LX/CfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public af:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:LX/0hI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ai:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public ak:Lcom/facebook/events/common/EventAnalyticsParams;

.field private am:Landroid/view/ViewGroup;

.field private an:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:LX/0g7;

.field public ap:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private aq:Lcom/facebook/feed/banner/GenericNotificationBanner;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:LX/0Yb;

.field public at:LX/195;

.field public au:I

.field public av:Z

.field private aw:Z

.field public ax:LX/0gM;

.field private ay:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:LX/I8k;

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ba:LX/63Q;

.field public bb:Z

.field private bc:LX/8wT;

.field public c:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DBC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/I7i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/BiW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Bkv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/I8x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/I94;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/I91;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/I98;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/I5f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/I5h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Bm1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/I77;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/IBS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/IBN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/IBQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/I6X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/Bky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2539472
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->al:Z

    .line 2539473
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->a:Ljava/lang/String;

    return-void

    .line 2539474
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2539463
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2539464
    new-instance v0, LX/I7S;

    invoke-direct {v0, p0}, LX/I7S;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aA:LX/I7S;

    .line 2539465
    new-instance v0, LX/I7T;

    invoke-direct {v0, p0}, LX/I7T;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aB:LX/I7T;

    .line 2539466
    new-instance v0, LX/I7P;

    invoke-direct {v0, p0}, LX/I7P;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aC:LX/I7P;

    .line 2539467
    new-instance v0, LX/I7U;

    invoke-direct {v0, p0}, LX/I7U;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aD:LX/I7U;

    .line 2539468
    new-instance v0, LX/I7O;

    invoke-direct {v0, p0}, LX/I7O;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aE:LX/I7O;

    .line 2539469
    new-instance v0, LX/I7Q;

    invoke-direct {v0, p0}, LX/I7Q;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aF:LX/I7Q;

    .line 2539470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    .line 2539471
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    .line 2539454
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2539455
    :goto_0
    return-void

    .line 2539456
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539457
    iget-object v1, v0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2539458
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2539459
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v1, 0x7f081f11

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_0

    .line 2539460
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539461
    iget-object p0, v1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, p0

    .line 2539462
    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static E(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 4

    .prologue
    .line 2539450
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v0, v0

    .line 2539451
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2539452
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    sget-object v2, LX/I7V;->FETCH_PERMALINK_GRAPHQL:LX/I7V;

    iget-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aF:LX/I7Q;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2539453
    return-void
.end method

.method private G()V
    .locals 1

    .prologue
    .line 2539447
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2539448
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2539449
    :cond_0
    return-void
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 2539443
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aO:LX/0g7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aO:LX/0g7;

    .line 2539444
    iget-object p0, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, p0

    .line 2539445
    iget-object p0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, p0

    .line 2539446
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z
    .locals 1

    .prologue
    .line 2539442
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static J(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 2

    .prologue
    .line 2539352
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->L()V

    .line 2539353
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 2539354
    if-nez v0, :cond_0

    .line 2539355
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    iget-boolean v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->bb:Z

    invoke-virtual {v0, v1}, LX/I5f;->a(Z)V

    .line 2539356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->bb:Z

    .line 2539357
    :cond_0
    return-void
.end method

.method private L()V
    .locals 4

    .prologue
    .line 2539425
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v0, v0

    .line 2539426
    sget-object v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2539427
    :goto_0
    return-void

    .line 2539428
    :cond_0
    new-instance v0, LX/I5q;

    invoke-direct {v0}, LX/I5q;-><init>()V

    move-object v0, v0

    .line 2539429
    const-string v1, "event_id"

    .line 2539430
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v2, v2

    .line 2539431
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2539432
    const-string v1, "fetch_creation_story"

    .line 2539433
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    sget-object v3, LX/7vK;->PROMOTE:LX/7vK;

    invoke-virtual {v2, v3}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    sget-object v3, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    invoke-virtual {v2, v3}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2539434
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2539435
    const-string v1, "enable_download"

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ai:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2539436
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->u:LX/I6X;

    invoke-virtual {v1, v0}, LX/I6X;->a(LX/0gW;)V

    .line 2539437
    new-instance v1, LX/I7D;

    invoke-direct {v1, p0}, LX/I7D;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539438
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2539439
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->O:LX/1My;

    const-string v3, "posts"

    invoke-virtual {v2, v0, v1, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2539440
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    sget-object v3, LX/I7V;->FETCH_PERMALINK_STORIES_GRAPHQL:LX/I7V;

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private N()V
    .locals 2

    .prologue
    .line 2539415
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->B:LX/DCI;

    invoke-virtual {v0}, LX/DCI;->a()V

    .line 2539416
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->D:LX/DCL;

    invoke-virtual {v0}, LX/DCL;->a()V

    .line 2539417
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->A:LX/DCC;

    invoke-virtual {v0}, LX/DCC;->a()V

    .line 2539418
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->F:LX/DCO;

    invoke-virtual {v0}, LX/DCO;->a()V

    .line 2539419
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->H:LX/DCQ;

    invoke-virtual {v0}, LX/DCQ;->a()V

    .line 2539420
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->I:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->e()V

    .line 2539421
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    .line 2539422
    invoke-static {v0}, LX/I5f;->l(LX/I5f;)V

    .line 2539423
    iget-object v1, v0, LX/I5f;->e:LX/0Xw;

    iget-object p0, v0, LX/I5f;->h:LX/I5d;

    invoke-virtual {v1, p0}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 2539424
    return-void
.end method

.method public static O(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2539411
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    .line 2539412
    iget-object v3, v2, LX/IBq;->k:[LX/IBr;

    move-object v2, v3

    .line 2539413
    if-nez v2, :cond_1

    .line 2539414
    :cond_0
    :goto_0
    return v0

    :cond_1
    aget-object v3, v2, v0

    sget-object v4, LX/IBr;->ABOUT:LX/IBr;

    if-ne v3, v4, :cond_0

    aget-object v2, v2, v1

    sget-object v3, LX/IBr;->DISCUSSION:LX/IBr;

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private V()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2539379
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2539380
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_1

    .line 2539381
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/events/common/ActionSource;->UNKNOWN:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v0}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    .line 2539382
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v0, :cond_4

    .line 2539383
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v0, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    .line 2539384
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2539385
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v0, v1

    .line 2539386
    if-eqz v0, :cond_0

    .line 2539387
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2539388
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v0, v1

    .line 2539389
    invoke-virtual {v0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2539390
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->m:LX/1nQ;

    .line 2539391
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v1, v1

    .line 2539392
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->w()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v6, :cond_3

    :goto_2
    const/4 p0, 0x0

    .line 2539393
    iget-boolean v6, v0, LX/1nQ;->a:Z

    if-eqz v6, :cond_5

    .line 2539394
    :cond_1
    :goto_3
    return-void

    .line 2539395
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2539396
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v0, v1

    .line 2539397
    invoke-virtual {v0}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    goto :goto_0

    .line 2539398
    :cond_3
    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object v3, v5

    goto :goto_1

    .line 2539399
    :cond_5
    iget-object v6, v0, LX/1nQ;->i:LX/0Zb;

    const-string v7, "view"

    invoke-interface {v6, v7, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2539400
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2539401
    const-string v7, "event_permalink"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "Event"

    invoke-virtual {v7, v8}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    iget-object v8, v0, LX/1nQ;->j:LX/0kv;

    iget-object v9, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v8, v9}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "action_ref"

    invoke-virtual {v7, v8, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v7

    const-string v8, "ref_mechanism"

    invoke-virtual {v7, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "ref_module"

    invoke-virtual {v7, v8, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "has_installed_launcher"

    invoke-virtual {v7, v8, p0}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2539402
    const/4 v7, 0x0

    .line 2539403
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2539404
    :goto_4
    move-object v7, v7

    .line 2539405
    if-eqz v7, :cond_6

    .line 2539406
    const-string v8, "tracking"

    invoke-virtual {v6, v8, v7}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 2539407
    :cond_6
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2539408
    :cond_7
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/1nQ;->a:Z

    goto :goto_3

    .line 2539409
    :cond_8
    :try_start_0
    iget-object v8, v0, LX/1nQ;->h:LX/0lB;

    invoke-virtual {v8, v5}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_4

    .line 2539410
    :catch_0
    goto :goto_4
.end method

.method public static X(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z
    .locals 2

    .prologue
    .line 2539371
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2539372
    const-string v1, "extra_launch_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2539373
    const/4 v0, 0x0

    .line 2539374
    :goto_0
    return v0

    .line 2539375
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2539376
    const-string v1, "extra_launch_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2539377
    const-string v1, "ref"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2539378
    const-string v1, "notifications_view"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2539367
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->DB_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->a(LX/IBP;)J

    .line 2539368
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->r:LX/IBS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2539369
    iget-object v2, v0, LX/IBS;->b:LX/0TD;

    new-instance p0, LX/IBR;

    invoke-direct {p0, v0, v1, p1}, LX/IBR;-><init>(LX/IBS;Landroid/content/Context;Landroid/net/Uri;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2539370
    return-object v0
.end method

.method private a(LX/5O9;)V
    .locals 1

    .prologue
    .line 2539364
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aU:LX/5O9;

    if-nez v0, :cond_0

    .line 2539365
    iput-object p1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aU:LX/5O9;

    .line 2539366
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V
    .locals 4

    .prologue
    .line 2539358
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->an:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2539359
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 2539360
    :goto_0
    return-void

    .line 2539361
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0304f5

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->am:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2539362
    const v0, 0x7f0d0e3b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->an:Landroid/view/View;

    .line 2539363
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->an:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/0TD;Ljava/lang/Boolean;LX/DBC;LX/I7i;LX/BiW;LX/0yc;LX/Bkv;LX/I8x;LX/I94;LX/I91;LX/I98;LX/1nQ;LX/I5f;LX/I5h;LX/Bm1;LX/I77;LX/IBS;LX/IBN;LX/IBQ;LX/I6X;LX/Bky;LX/BiT;LX/Bl6;LX/0kb;LX/0zG;LX/DCC;LX/DCI;LX/DCI;LX/DCL;LX/DCL;LX/DCO;LX/0fz;LX/DCQ;LX/1CY;LX/193;LX/0Uh;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/0tX;LX/0gX;LX/1My;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/3Cm;LX/IBL;LX/0Or;LX/0ad;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1Ck;LX/8wR;LX/I8l;LX/0Ot;LX/Cfr;LX/E94;LX/CfW;LX/2iz;LX/3Fx;LX/0hI;LX/0tQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/permalink/EventPermalinkFragment;",
            "LX/0TD;",
            "Ljava/lang/Boolean;",
            "LX/DBC;",
            "LX/I7i;",
            "LX/BiW;",
            "LX/0yc;",
            "LX/Bkv;",
            "LX/I8x;",
            "LX/I94;",
            "LX/I91;",
            "LX/I98;",
            "LX/1nQ;",
            "LX/I5f;",
            "LX/I5h;",
            "LX/Bm1;",
            "LX/I77;",
            "LX/IBS;",
            "LX/IBN;",
            "LX/IBQ;",
            "LX/I6X;",
            "LX/Bky;",
            "LX/BiT;",
            "LX/Bl6;",
            "LX/0kb;",
            "LX/0zG;",
            "LX/DCC;",
            "LX/DCI;",
            "LX/DCI;",
            "LX/DCL;",
            "LX/DCL;",
            "LX/DCO;",
            "LX/0fz;",
            "LX/DCQ;",
            "LX/1CY;",
            "LX/193;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/0tX;",
            "LX/0gX;",
            "LX/1My;",
            "LX/0Ot",
            "<",
            "LX/I6W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Cm;",
            "LX/IBL;",
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1Ck;",
            "LX/8wR;",
            "LX/I8l;",
            "LX/0Ot",
            "<",
            "LX/Amz;",
            ">;",
            "LX/Cfr;",
            "LX/E94;",
            "LX/CfW;",
            "LX/2iz;",
            "LX/3Fx;",
            "LX/0hI;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2539720
    iput-object p1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->b:LX/0TD;

    iput-object p2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->c:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->d:LX/DBC;

    iput-object p4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    iput-object p5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->f:LX/BiW;

    iput-object p6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->g:LX/0yc;

    iput-object p7, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    iput-object p8, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->i:LX/I8x;

    iput-object p9, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->j:LX/I94;

    iput-object p10, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->k:LX/I91;

    iput-object p11, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->l:LX/I98;

    iput-object p12, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->m:LX/1nQ;

    iput-object p13, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    iput-object p14, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->o:LX/I5h;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->p:LX/Bm1;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->q:LX/I77;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->r:LX/IBS;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->u:LX/I6X;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->v:LX/Bky;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->w:LX/BiT;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->y:LX/0kb;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->A:LX/DCC;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->B:LX/DCI;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->C:LX/DCI;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->D:LX/DCL;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->E:LX/DCL;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->F:LX/DCO;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->H:LX/DCQ;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->I:LX/1CY;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->J:LX/193;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->K:LX/0Uh;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->L:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->M:LX/0tX;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->N:LX/0gX;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->O:LX/1My;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->P:LX/0Ot;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Q:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->R:LX/0Ot;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->S:LX/0Or;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->T:LX/3Cm;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->U:LX/IBL;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->V:LX/0Or;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->W:LX/0ad;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->X:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Z:LX/8wR;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aa:LX/I8l;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ab:LX/0Ot;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ac:LX/Cfr;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ad:LX/E94;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ae:LX/CfW;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->af:LX/2iz;

    move-object/from16 v0, p58

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ag:LX/3Fx;

    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ah:LX/0hI;

    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ai:LX/0tQ;

    return-void
.end method

.method public static a(Lcom/facebook/events/permalink/EventPermalinkFragment;Lcom/facebook/events/model/Event;)V
    .locals 2
    .param p0    # Lcom/facebook/events/permalink/EventPermalinkFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2539475
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->DB_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->d(LX/IBP;)V

    .line 2539476
    if-eqz p1, :cond_0

    .line 2539477
    sget-object v0, LX/5O9;->DB_FETCH:LX/5O9;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(LX/5O9;)V

    .line 2539478
    :cond_0
    if-nez p1, :cond_1

    .line 2539479
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->y(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539480
    :goto_0
    return-void

    .line 2539481
    :cond_1
    iput-object p1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539482
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 63

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v62

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static/range {v62 .. v62}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static/range {v62 .. v62}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-static/range {v62 .. v62}, LX/DBC;->a(LX/0QB;)LX/DBC;

    move-result-object v5

    check-cast v5, LX/DBC;

    invoke-static/range {v62 .. v62}, LX/I7i;->a(LX/0QB;)LX/I7i;

    move-result-object v6

    check-cast v6, LX/I7i;

    invoke-static/range {v62 .. v62}, LX/BiW;->a(LX/0QB;)LX/BiW;

    move-result-object v7

    check-cast v7, LX/BiW;

    invoke-static/range {v62 .. v62}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    invoke-static/range {v62 .. v62}, LX/Bkv;->a(LX/0QB;)LX/Bkv;

    move-result-object v9

    check-cast v9, LX/Bkv;

    invoke-static/range {v62 .. v62}, LX/I8x;->a(LX/0QB;)LX/I8x;

    move-result-object v10

    check-cast v10, LX/I8x;

    invoke-static/range {v62 .. v62}, LX/I94;->a(LX/0QB;)LX/I94;

    move-result-object v11

    check-cast v11, LX/I94;

    invoke-static/range {v62 .. v62}, LX/I91;->a(LX/0QB;)LX/I91;

    move-result-object v12

    check-cast v12, LX/I91;

    invoke-static/range {v62 .. v62}, LX/I98;->a(LX/0QB;)LX/I98;

    move-result-object v13

    check-cast v13, LX/I98;

    invoke-static/range {v62 .. v62}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v14

    check-cast v14, LX/1nQ;

    invoke-static/range {v62 .. v62}, LX/I5f;->a(LX/0QB;)LX/I5f;

    move-result-object v15

    check-cast v15, LX/I5f;

    invoke-static/range {v62 .. v62}, LX/I5h;->a(LX/0QB;)LX/I5h;

    move-result-object v16

    check-cast v16, LX/I5h;

    invoke-static/range {v62 .. v62}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v17

    check-cast v17, LX/Bm1;

    invoke-static/range {v62 .. v62}, LX/I77;->a(LX/0QB;)LX/I77;

    move-result-object v18

    check-cast v18, LX/I77;

    invoke-static/range {v62 .. v62}, LX/IBS;->a(LX/0QB;)LX/IBS;

    move-result-object v19

    check-cast v19, LX/IBS;

    invoke-static/range {v62 .. v62}, LX/IBN;->a(LX/0QB;)LX/IBN;

    move-result-object v20

    check-cast v20, LX/IBN;

    invoke-static/range {v62 .. v62}, LX/IBQ;->a(LX/0QB;)LX/IBQ;

    move-result-object v21

    check-cast v21, LX/IBQ;

    invoke-static/range {v62 .. v62}, LX/I6X;->a(LX/0QB;)LX/I6X;

    move-result-object v22

    check-cast v22, LX/I6X;

    invoke-static/range {v62 .. v62}, LX/Bky;->a(LX/0QB;)LX/Bky;

    move-result-object v23

    check-cast v23, LX/Bky;

    invoke-static/range {v62 .. v62}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v24

    check-cast v24, LX/BiT;

    invoke-static/range {v62 .. v62}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v25

    check-cast v25, LX/Bl6;

    invoke-static/range {v62 .. v62}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v26

    check-cast v26, LX/0kb;

    invoke-static/range {v62 .. v62}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v27

    check-cast v27, LX/0zG;

    invoke-static/range {v62 .. v62}, LX/DCC;->a(LX/0QB;)LX/DCC;

    move-result-object v28

    check-cast v28, LX/DCC;

    invoke-static/range {v62 .. v62}, LX/DCI;->a(LX/0QB;)LX/DCI;

    move-result-object v29

    check-cast v29, LX/DCI;

    invoke-static/range {v62 .. v62}, LX/DCI;->a(LX/0QB;)LX/DCI;

    move-result-object v30

    check-cast v30, LX/DCI;

    invoke-static/range {v62 .. v62}, LX/DCL;->a(LX/0QB;)LX/DCL;

    move-result-object v31

    check-cast v31, LX/DCL;

    invoke-static/range {v62 .. v62}, LX/DCL;->a(LX/0QB;)LX/DCL;

    move-result-object v32

    check-cast v32, LX/DCL;

    invoke-static/range {v62 .. v62}, LX/DCO;->a(LX/0QB;)LX/DCO;

    move-result-object v33

    check-cast v33, LX/DCO;

    invoke-static/range {v62 .. v62}, LX/0fz;->a(LX/0QB;)LX/0fz;

    move-result-object v34

    check-cast v34, LX/0fz;

    invoke-static/range {v62 .. v62}, LX/DCQ;->a(LX/0QB;)LX/DCQ;

    move-result-object v35

    check-cast v35, LX/DCQ;

    invoke-static/range {v62 .. v62}, LX/1CY;->a(LX/0QB;)LX/1CY;

    move-result-object v36

    check-cast v36, LX/1CY;

    const-class v37, LX/193;

    move-object/from16 v0, v62

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/193;

    invoke-static/range {v62 .. v62}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v38

    check-cast v38, LX/0Uh;

    invoke-static/range {v62 .. v62}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v39

    check-cast v39, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {v62 .. v62}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v40

    check-cast v40, LX/0tX;

    invoke-static/range {v62 .. v62}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v41

    check-cast v41, LX/0gX;

    invoke-static/range {v62 .. v62}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v42

    check-cast v42, LX/1My;

    const/16 v43, 0x1b45

    move-object/from16 v0, v62

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v43

    const/16 v44, 0x259

    move-object/from16 v0, v62

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v44

    const/16 v45, 0xf4d

    move-object/from16 v0, v62

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v45

    const/16 v46, 0x15e7

    move-object/from16 v0, v62

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v46

    invoke-static/range {v62 .. v62}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v47

    check-cast v47, LX/3Cm;

    invoke-static/range {v62 .. v62}, LX/IBL;->a(LX/0QB;)LX/IBL;

    move-result-object v48

    check-cast v48, LX/IBL;

    const/16 v49, 0x19c6

    move-object/from16 v0, v62

    move/from16 v1, v49

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v49

    invoke-static/range {v62 .. v62}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v50

    check-cast v50, LX/0ad;

    invoke-static/range {v62 .. v62}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v51

    check-cast v51, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v62 .. v62}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v52

    check-cast v52, LX/1Ck;

    invoke-static/range {v62 .. v62}, LX/8wR;->a(LX/0QB;)LX/8wR;

    move-result-object v53

    check-cast v53, LX/8wR;

    const-class v54, LX/I8l;

    move-object/from16 v0, v62

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v54

    check-cast v54, LX/I8l;

    const/16 v55, 0x1d60

    move-object/from16 v0, v62

    move/from16 v1, v55

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v55

    const-class v56, LX/Cfr;

    move-object/from16 v0, v62

    move-object/from16 v1, v56

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v56

    check-cast v56, LX/Cfr;

    const-class v57, LX/E94;

    move-object/from16 v0, v62

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v57

    check-cast v57, LX/E94;

    invoke-static/range {v62 .. v62}, LX/CfW;->a(LX/0QB;)LX/CfW;

    move-result-object v58

    check-cast v58, LX/CfW;

    invoke-static/range {v62 .. v62}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v59

    check-cast v59, LX/2iz;

    invoke-static/range {v62 .. v62}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v60

    check-cast v60, LX/3Fx;

    invoke-static/range {v62 .. v62}, LX/0hI;->a(LX/0QB;)LX/0hI;

    move-result-object v61

    check-cast v61, LX/0hI;

    invoke-static/range {v62 .. v62}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v62

    check-cast v62, LX/0tQ;

    invoke-static/range {v2 .. v62}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/0TD;Ljava/lang/Boolean;LX/DBC;LX/I7i;LX/BiW;LX/0yc;LX/Bkv;LX/I8x;LX/I94;LX/I91;LX/I98;LX/1nQ;LX/I5f;LX/I5h;LX/Bm1;LX/I77;LX/IBS;LX/IBN;LX/IBQ;LX/I6X;LX/Bky;LX/BiT;LX/Bl6;LX/0kb;LX/0zG;LX/DCC;LX/DCI;LX/DCI;LX/DCL;LX/DCL;LX/DCO;LX/0fz;LX/DCQ;LX/1CY;LX/193;LX/0Uh;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/0tX;LX/0gX;LX/1My;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/3Cm;LX/IBL;LX/0Or;LX/0ad;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1Ck;LX/8wR;LX/I8l;LX/0Ot;LX/Cfr;LX/E94;LX/CfW;LX/2iz;LX/3Fx;LX/0hI;LX/0tQ;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/DBa;)V
    .locals 6

    .prologue
    .line 2539715
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aq:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-nez v0, :cond_0

    .line 2539716
    new-instance v0, Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aq:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2539717
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->am:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aq:Lcom/facebook/feed/banner/GenericNotificationBanner;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b090c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2539718
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aq:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    .line 2539719
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;Lcom/facebook/graphql/executor/GraphQLResult;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2539690
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2539691
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539692
    if-eqz v0, :cond_3

    .line 2539693
    sget-object v1, LX/5O9;->GRAPHQL:LX/5O9;

    invoke-direct {p0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(LX/5O9;)V

    .line 2539694
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v2, LX/IBP;->NETWORK_FETCH:LX/IBP;

    invoke-virtual {v1, v2}, LX/IBQ;->d(LX/IBP;)V

    .line 2539695
    const/16 v1, 0x8

    invoke-static {p0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2539696
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v1, v4}, LX/0g7;->a(I)V

    .line 2539697
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v1, v1

    .line 2539698
    const/4 v2, 0x1

    move v2, v2

    .line 2539699
    if-eqz v2, :cond_0

    .line 2539700
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->O:LX/1My;

    new-instance v3, LX/I7R;

    invoke-direct {v3, p0}, LX/I7R;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v2, v3, v1, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2539701
    :cond_0
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 2539702
    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539703
    iput-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aZ:LX/0ta;

    .line 2539704
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539705
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->v:LX/Bky;

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    iget-object p1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->b:LX/0TD;

    invoke-static {v2, v3, v4, p1}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 2539706
    invoke-static {p0, p2}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    .line 2539707
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2539708
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2539709
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Bkv;->a(Ljava/lang/String;)V

    .line 2539710
    :cond_1
    :goto_0
    return-void

    .line 2539711
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Bkv;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2539712
    :cond_3
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->b(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    .line 2539713
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    invoke-virtual {v0}, LX/IBN;->a()V

    .line 2539714
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->c(LX/IBP;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2539666
    if-eqz p1, :cond_2

    .line 2539667
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v0, v0

    .line 2539668
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    sget-object v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2539669
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->b:LX/0TD;

    new-instance v2, Lcom/facebook/events/permalink/EventPermalinkFragment$13;

    invoke-direct {v2, p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment$13;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;Ljava/lang/String;)V

    const v0, -0x565b156

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2539670
    :cond_0
    invoke-static {p0, v3}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2539671
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v0, v4}, LX/0g7;->a(I)V

    .line 2539672
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2539673
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081f04

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2539674
    :cond_1
    :goto_0
    return-void

    .line 2539675
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_4

    .line 2539676
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->y:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2539677
    sget-object v0, LX/DBa;->FETCH_EVENT_FAILED:LX/DBa;

    invoke-static {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/DBa;)V

    .line 2539678
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2539679
    iget-object v1, v0, LX/I8k;->c:LX/I8R;

    .line 2539680
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/I8R;->k:Z

    .line 2539681
    invoke-static {v1}, LX/I8R;->c(LX/I8R;)V

    .line 2539682
    invoke-virtual {v0}, LX/I8k;->j()V

    .line 2539683
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2539684
    invoke-static {p0, v4}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2539685
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v0, v3}, LX/0g7;->a(I)V

    goto :goto_0

    .line 2539686
    :cond_4
    invoke-static {p0, v3}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2539687
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v0, v4}, LX/0g7;->a(I)V

    .line 2539688
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 2539689
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081f04

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2539664
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->a(LX/IBP;)J

    .line 2539665
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->r:LX/IBS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/IBS;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V
    .locals 9

    .prologue
    .line 2539018
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->V()V

    .line 2539019
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->C()V

    .line 2539020
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539021
    iput-object v1, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2539022
    iget-object v3, v0, LX/I8k;->c:LX/I8R;

    const/4 v4, 0x1

    .line 2539023
    iput-boolean v4, v3, LX/I8R;->n:Z

    .line 2539024
    iput-object v1, v3, LX/I8R;->j:Lcom/facebook/events/model/Event;

    .line 2539025
    iput-object v2, v3, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539026
    iget-object v5, v3, LX/I8R;->q:LX/IBk;

    .line 2539027
    iget v6, v5, LX/IBk;->b:I

    if-eqz v6, :cond_6

    .line 2539028
    :goto_0
    iget-object v5, v3, LX/I8R;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v5, :cond_0

    .line 2539029
    const/4 v5, 0x0

    iput-boolean v5, v3, LX/I8R;->k:Z

    .line 2539030
    :cond_0
    invoke-static {v3}, LX/I8R;->c(LX/I8R;)V

    .line 2539031
    iget-object v3, v0, LX/I8k;->k:LX/I8V;

    iget-object v4, v0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2539032
    iput-object v2, v3, LX/I8V;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539033
    iput-object v4, v3, LX/I8V;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2539034
    iget-object v3, v0, LX/I8k;->e:LX/I8O;

    invoke-virtual {v3, v1}, LX/I8O;->a(Lcom/facebook/events/model/Event;)V

    .line 2539035
    iget-object v3, v0, LX/I8k;->l:LX/I8T;

    .line 2539036
    iput-object v1, v3, LX/I8T;->b:Lcom/facebook/events/model/Event;

    .line 2539037
    iput-object v2, v3, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2539038
    const/4 v4, 0x0

    .line 2539039
    iget-object v1, v3, LX/I8T;->b:Lcom/facebook/events/model/Event;

    if-eqz v1, :cond_1

    iget-object v1, v3, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v1, :cond_8

    .line 2539040
    :cond_1
    :goto_1
    move v4, v4

    .line 2539041
    iput-boolean v4, v3, LX/I8T;->d:Z

    .line 2539042
    iget-object v3, v0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2539043
    iget-object v4, v3, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v3, v4

    .line 2539044
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v3, v4, :cond_5

    .line 2539045
    iget-object v3, v0, LX/I8k;->F:LX/IBq;

    sget-object v4, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v3, v4}, LX/IBq;->a(LX/IBr;)V

    .line 2539046
    :cond_2
    :goto_2
    invoke-virtual {v0}, LX/I8k;->j()V

    .line 2539047
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->G()V

    .line 2539048
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->U:LX/IBL;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539049
    iget-object v2, v1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v2, v2

    .line 2539050
    iget-object v3, v0, LX/IBL;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2539051
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539052
    iput-object v1, v0, LX/I7i;->k:Lcom/facebook/events/model/Event;

    .line 2539053
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    .line 2539054
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->p()V

    .line 2539055
    :cond_4
    return-void

    .line 2539056
    :cond_5
    iget-object v3, v0, LX/I8k;->F:LX/IBq;

    sget-object v4, LX/IBr;->EVENT:LX/IBr;

    invoke-virtual {v3, v4}, LX/IBq;->a(LX/IBr;)V

    .line 2539057
    if-eqz v2, :cond_2

    .line 2539058
    sget-object v3, LX/IBr;->ACTIVITY:LX/IBr;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as()I

    move-result v4

    invoke-virtual {v0, v3, v4}, LX/I8k;->a(LX/IBr;I)V

    goto :goto_2

    .line 2539059
    :cond_6
    if-eqz v2, :cond_7

    .line 2539060
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r()Z

    move-result v6

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-static {v6, v7, v8, v4}, LX/IBk;->a(ZLcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I

    move-result v6

    iput v6, v5, LX/IBk;->b:I

    goto/16 :goto_0

    .line 2539061
    :cond_7
    iget-boolean v6, v1, Lcom/facebook/events/model/Event;->H:Z

    move v6, v6

    .line 2539062
    iget-object v7, v1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v7, v7

    .line 2539063
    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v8

    .line 2539064
    iget-object v4, v1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v4, v4

    .line 2539065
    invoke-static {v6, v7, v8, v4}, LX/IBk;->a(ZLcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I

    move-result v6

    iput v6, v5, LX/IBk;->a:I

    goto/16 :goto_0

    :cond_8
    iget-object v1, v3, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->U()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v3, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aW()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v4, 0x1

    goto/16 :goto_1

    .line 2539066
    :cond_9
    iget-object v2, v1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v2, v2

    .line 2539067
    iput-object v2, v0, LX/IBL;->d:Ljava/lang/String;

    .line 2539068
    iget-object v2, v0, LX/IBL;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 2539069
    const/4 v2, 0x1

    move v2, v2

    .line 2539070
    if-eqz v2, :cond_3

    .line 2539071
    iget-object v2, v0, LX/IBL;->c:LX/3iH;

    iget-object v3, v0, LX/IBL;->d:Ljava/lang/String;

    new-instance v4, LX/8E9;

    invoke-direct {v4}, LX/8E9;-><init>()V

    iget-object v5, v0, LX/IBL;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v3, v4, v5}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    goto :goto_3
.end method

.method public static d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V
    .locals 4

    .prologue
    .line 2539658
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_1

    .line 2539659
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 2539660
    :goto_0
    return-void

    .line 2539661
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0304f4

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->am:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2539662
    const v0, 0x7f0d0e3a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    .line 2539663
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private r()LX/I8k;
    .locals 11

    .prologue
    .line 2539646
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ad:LX/E94;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aI:Landroid/content/Context;

    .line 2539647
    sget-object v0, LX/I6Y;->a:LX/I6Y;

    move-object v3, v0

    .line 2539648
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ab:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SX;

    invoke-virtual {v1, v2, v3, v0, p0}, LX/E94;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;)LX/E93;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    .line 2539649
    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    const-string v4, "unknown"

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v6, v0}, LX/E8m;->a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2539650
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aa:LX/I8l;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    iget-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->d:LX/DBC;

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aI:Landroid/content/Context;

    iget-object v7, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    iget-object v8, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->k:LX/I91;

    iget-object v9, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->l:LX/I98;

    iget-object v10, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aM:LX/1P0;

    move-object v5, p0

    invoke-virtual/range {v0 .. v10}, LX/I8l;->a(LX/I5f;LX/I7i;LX/DBC;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/permalink/EventPermalinkFragment;Landroid/content/Context;LX/E93;LX/I91;LX/I98;LX/1P0;)LX/I8k;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    .line 2539651
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    new-instance v1, LX/I7G;

    invoke-direct {v1, p0}, LX/I7G;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539652
    iput-object v1, v0, LX/I8k;->K:LX/I7G;

    .line 2539653
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    new-instance v1, LX/I7H;

    invoke-direct {v1, p0}, LX/I7H;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539654
    iput-object v1, v0, LX/I8k;->L:LX/I7H;

    .line 2539655
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    .line 2539656
    iget-object v1, v0, LX/I8k;->q:LX/E93;

    new-instance v2, LX/I8Y;

    invoke-direct {v2, v0}, LX/I8Y;-><init>(LX/I8k;)V

    invoke-virtual {v1, v2}, LX/1OM;->a(LX/1OD;)V

    .line 2539657
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2539635
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2539636
    const-string v1, "extra_reaction_analytics_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2539637
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2539638
    iget-object v0, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    .line 2539639
    :cond_0
    :goto_0
    return-object v0

    .line 2539640
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2539641
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2539642
    const-string v1, "unknown"

    .line 2539643
    if-eqz v0, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->X(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2539644
    const-string v0, "push_notifications_tray"

    goto :goto_0

    .line 2539645
    :cond_3
    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public static y(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 4

    .prologue
    .line 2539631
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 2539632
    :goto_0
    return-void

    .line 2539633
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    sget-object v1, LX/I7V;->FETCH_PERMALINK_GRAPHQL:LX/I7V;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aF:LX/I7Q;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2539634
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2539630
    const-string v0, "event_permalink"

    return-object v0
.end method

.method public final a(LX/9qT;)V
    .locals 1

    .prologue
    .line 2539628
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v0, p1}, LX/E8m;->a(LX/9qT;)V

    .line 2539629
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2539505
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2539506
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2539507
    if-eqz p1, :cond_0

    .line 2539508
    const-string v0, "has_logged_first_scroll"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aR:Z

    .line 2539509
    const-string v0, "show_reaction_units"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aS:Z

    .line 2539510
    const-string v0, "on_demand_waiting_for_load"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    .line 2539511
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ANDROID_EVENT_PERMALINK"

    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aI:Landroid/content/Context;

    .line 2539512
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aM:LX/1P0;

    .line 2539513
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->w()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    .line 2539514
    new-instance v3, LX/0Yj;

    const v4, 0x60015

    const-string v5, "EventPermalinkFragment"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const-string v4, "navigation_source"

    invoke-static {v4, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    .line 2539515
    iput-object v4, v3, LX/0Yj;->l:Ljava/util/Map;

    .line 2539516
    move-object v3, v3

    .line 2539517
    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "event_permalink"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v3

    .line 2539518
    iput-boolean v7, v3, LX/0Yj;->n:Z

    .line 2539519
    move-object v3, v3

    .line 2539520
    iget-object v4, v0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v4, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2539521
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->X:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x60016

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2539522
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->w()Ljava/lang/String;

    move-result-object v1

    .line 2539523
    iget-object v8, v0, LX/IBQ;->c:LX/11i;

    sget-object v9, LX/IBQ;->a:LX/IBO;

    const-string v10, "navigation_source"

    invoke-static {v10, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v10

    iget-object v11, v0, LX/IBQ;->b:LX/0So;

    invoke-interface {v11}, LX/0So;->now()J

    move-result-wide v12

    invoke-interface {v8, v9, v10, v12, v13}, LX/11i;->a(LX/0Pq;LX/0P1;J)LX/11o;

    .line 2539524
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->K:LX/0Uh;

    const/16 v1, 0x3a8

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->av:Z

    .line 2539525
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->J:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "events_permalink_scroll"

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->at:LX/195;

    .line 2539526
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v0

    .line 2539527
    const-string v0, "event_id"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    .line 2539528
    const-string v0, "extra_original_event_id"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ay:Ljava/lang/String;

    .line 2539529
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->g:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2539530
    const/4 v0, 0x0

    .line 2539531
    const-string v1, "story_cache_id"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2539532
    :cond_1
    :goto_0
    move-object v0, v0

    .line 2539533
    if-eqz v0, :cond_5

    .line 2539534
    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aY:Lcom/facebook/events/model/Event;

    .line 2539535
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->q:LX/I77;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2539536
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_6

    .line 2539537
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aW:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2539538
    :goto_2
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    const/4 v13, 0x0

    .line 2539539
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539540
    const-string v9, "extras_event_action_context"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2539541
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539542
    const-string v9, "extras_event_action_context"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/facebook/events/common/EventActionContext;

    .line 2539543
    sget-object v9, Lcom/facebook/events/common/ActionSource;->PERMALINK:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v8, v9}, Lcom/facebook/events/common/EventActionContext;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventActionContext;

    move-result-object v8

    .line 2539544
    :goto_3
    move-object v1, v8

    .line 2539545
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->w()Ljava/lang/String;

    move-result-object v2

    .line 2539546
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2539547
    const-string v4, "extra_reaction_analytics_params"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2539548
    if-eqz v3, :cond_d

    iget-object v4, v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2539549
    iget-object v3, v3, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    .line 2539550
    :cond_2
    :goto_4
    move-object v3, v3

    .line 2539551
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a()Ljava/lang/String;

    move-result-object v4

    const-string v6, "tracking_codes"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2539552
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->d:LX/DBC;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2539553
    iput-object v1, v0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2539554
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    .line 2539555
    iput-object p0, v0, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    .line 2539556
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->r()LX/I8k;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2539557
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ay:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2539558
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ay:Ljava/lang/String;

    .line 2539559
    iget-object v2, v0, LX/I8k;->c:LX/I8R;

    .line 2539560
    iput-object v1, v2, LX/I8R;->s:Ljava/lang/String;

    .line 2539561
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    .line 2539562
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    .line 2539563
    iget-object v1, v0, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_e

    .line 2539564
    :cond_4
    :goto_5
    new-instance v0, LX/0Xj;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Xj;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/0Xk;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/I7F;

    invoke-direct {v2, p0}, LX/I7F;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->as:LX/0Yb;

    .line 2539565
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->as:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2539566
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->j:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, v1, v2}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    .line 2539567
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->o:LX/I5h;

    const/16 v2, 0xa

    .line 2539568
    iput-object v0, v1, LX/I5h;->e:Lcom/facebook/api/feedtype/FeedType;

    .line 2539569
    iput v2, v1, LX/I5h;->d:I

    .line 2539570
    iget-object v3, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v3, v3

    .line 2539571
    check-cast v3, Ljava/lang/String;

    .line 2539572
    new-instance v4, Lcom/facebook/api/feed/FeedFetchContext;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lcom/facebook/api/feed/FeedFetchContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v1, LX/I5h;->f:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2539573
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->o:LX/I5h;

    new-instance v2, LX/I7I;

    invoke-direct {v2, p0}, LX/I7I;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539574
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/I5h;

    iput-object v3, v0, LX/I5f;->m:LX/I5h;

    .line 2539575
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/I7I;

    iput-object v3, v0, LX/I5f;->n:LX/I7I;

    .line 2539576
    new-instance v3, LX/I5b;

    iget-object v4, v0, LX/I5f;->b:LX/IBQ;

    sget-object v5, LX/IBP;->CACHED_STORIES_INITIAL_FETCH:LX/IBP;

    invoke-direct {v3, v4, v5}, LX/I5b;-><init>(LX/IBQ;LX/IBP;)V

    iput-object v3, v0, LX/I5f;->o:LX/I5b;

    .line 2539577
    new-instance v3, LX/I5b;

    iget-object v4, v0, LX/I5f;->b:LX/IBQ;

    sget-object v5, LX/IBP;->FRESH_STORIES_INITIAL_FETCH:LX/IBP;

    invoke-direct {v3, v4, v5}, LX/I5b;-><init>(LX/IBQ;LX/IBP;)V

    iput-object v3, v0, LX/I5f;->p:LX/I5b;

    .line 2539578
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    .line 2539579
    iget-object v1, v0, LX/I5f;->c:LX/0fz;

    move-object v0, v1

    .line 2539580
    new-instance v1, LX/I7J;

    invoke-direct {v1, p0}, LX/I7J;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539581
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->B:LX/DCI;

    .line 2539582
    iget-object v3, v0, LX/0fz;->a:LX/0qq;

    move-object v3, v3

    .line 2539583
    invoke-virtual {v2, v3, v1}, LX/DCI;->a(LX/0qq;LX/0g4;)V

    .line 2539584
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->D:LX/DCL;

    .line 2539585
    iget-object v3, v0, LX/0fz;->a:LX/0qq;

    move-object v3, v3

    .line 2539586
    invoke-virtual {v2, v3, v1}, LX/DCL;->a(LX/0qq;LX/0g4;)V

    .line 2539587
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->A:LX/DCC;

    .line 2539588
    iget-object v3, v0, LX/0fz;->a:LX/0qq;

    move-object v3, v3

    .line 2539589
    invoke-virtual {v2, v3, v1}, LX/DCC;->a(LX/0qq;LX/0g4;)V

    .line 2539590
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->F:LX/DCO;

    .line 2539591
    iget-object v3, v0, LX/0fz;->a:LX/0qq;

    move-object v3, v3

    .line 2539592
    invoke-virtual {v2, v3, v1}, LX/DCO;->a(LX/0qq;LX/0g4;)V

    .line 2539593
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->H:LX/DCQ;

    .line 2539594
    iget-object v3, v0, LX/0fz;->a:LX/0qq;

    move-object v3, v3

    .line 2539595
    invoke-virtual {v2, v3, v1}, LX/DCQ;->a(LX/0qq;LX/0g4;)V

    .line 2539596
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->I:LX/1CY;

    invoke-virtual {v2, v0, v1}, LX/1CY;->a(LX/0fz;LX/0g4;)V

    .line 2539597
    return-void

    .line 2539598
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->v:LX/Bky;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Bky;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_1

    .line 2539599
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->EARLY_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->a(LX/IBP;)J

    move-result-wide v0

    .line 2539600
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v3, LX/IBP;->EARLY_FETCH:LX/IBP;

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->q:LX/I77;

    invoke-virtual {v4}, LX/98h;->d()J

    move-result-wide v6

    add-long/2addr v0, v6

    invoke-virtual {v2, v3, v0, v1}, LX/IBQ;->a(LX/IBP;J)V

    .line 2539601
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->a(LX/IBP;)J

    goto/16 :goto_2

    .line 2539602
    :cond_7
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->L:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const-string v2, "story_cache_id"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2539603
    if-eqz v1, :cond_1

    .line 2539604
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2539605
    invoke-static {v1}, LX/3Cm;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2539606
    if-eqz v2, :cond_8

    .line 2539607
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 2539608
    :goto_6
    move-object v1, v2

    .line 2539609
    if-eqz v1, :cond_1

    .line 2539610
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->cD()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->cN()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2539611
    invoke-static {v1}, LX/Bm1;->b(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/events/model/Event;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    .line 2539612
    :cond_9
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539613
    const-string v9, "action_ref"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 2539614
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539615
    const-string v9, "event_ref_mechanism"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2539616
    :cond_a
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539617
    const-string v9, "action_ref"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/facebook/events/common/ActionSource;

    .line 2539618
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2539619
    const-string v9, "event_ref_mechanism"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/facebook/events/common/ActionMechanism;

    .line 2539620
    new-instance v8, Lcom/facebook/events/common/EventActionContext;

    sget-object v9, Lcom/facebook/events/common/ActionSource;->PERMALINK:Lcom/facebook/events/common/ActionSource;

    const/4 v10, 0x0

    invoke-direct/range {v8 .. v13}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto/16 :goto_3

    .line 2539621
    :cond_b
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->X(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2539622
    new-instance v8, Lcom/facebook/events/common/EventActionContext;

    sget-object v9, Lcom/facebook/events/common/ActionSource;->PERMALINK:Lcom/facebook/events/common/ActionSource;

    sget-object v10, Lcom/facebook/events/common/ActionSource;->MOBILE_SYSTEM_NOTIFICATION:Lcom/facebook/events/common/ActionSource;

    invoke-direct {v8, v9, v10, v13}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    goto/16 :goto_3

    .line 2539623
    :cond_c
    sget-object v8, Lcom/facebook/events/common/EventActionContext;->c:Lcom/facebook/events/common/EventActionContext;

    goto/16 :goto_3

    .line 2539624
    :cond_d
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2539625
    const-string v4, "event_ref_mechanism"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2539626
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v3, "unknown"

    goto/16 :goto_4

    .line 2539627
    :cond_e
    iget-object v1, v0, LX/I7i;->c:LX/1Kf;

    const/16 v2, 0x6dc

    iget-object v3, v0, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v1, v2, v3}, LX/1Kf;->a(ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_5
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2539484
    if-eqz p1, :cond_3

    .line 2539485
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2539486
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2539487
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v1, v0}, LX/I8k;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2539488
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2539489
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v2, v2

    .line 2539490
    invoke-virtual {v1, v0, v2}, LX/Bkv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2539491
    :goto_0
    if-eqz p2, :cond_0

    .line 2539492
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2539493
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2539494
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 2539495
    if-eqz p1, :cond_1

    .line 2539496
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {v0, p1}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 2539497
    :cond_1
    if-eqz p2, :cond_2

    .line 2539498
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {v0, p2}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 2539499
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->G()V

    .line 2539500
    return-void

    .line 2539501
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I8k;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2539502
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    .line 2539503
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v1, v1

    .line 2539504
    invoke-virtual {v0, v1}, LX/Bkv;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2539441
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2539483
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v0, p1, p2}, LX/E8m;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2539072
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v0, p1}, LX/E8m;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2539017
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2539015
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v0, v0

    .line 2539016
    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 2539013
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2539014
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2539012
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->mK_()LX/63R;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 2539010
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->mK_()LX/63R;

    move-result-object v0

    .line 2539011
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2539008
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aw:Z

    .line 2539009
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2538973
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    .line 2539002
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 2539003
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2539004
    :goto_0
    return-object v0

    .line 2539005
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539006
    iget-object v1, v0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v1

    .line 2539007
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->EVENT:LX/103;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()V
    .locals 4

    .prologue
    .line 2538993
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->L()V

    .line 2538994
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    .line 2538995
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/I5f;->q:Z

    .line 2538996
    const/4 v1, 0x1

    iput v1, v0, LX/I5f;->r:I

    .line 2538997
    iget-object v1, v0, LX/I5f;->e:LX/0Xw;

    iget-object v2, v0, LX/I5f;->h:LX/I5d;

    new-instance v3, Landroid/content/IntentFilter;

    const-string p0, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v3, p0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2538998
    iget-object v1, v0, LX/I5f;->f:LX/1Ck;

    sget-object v2, LX/I5e;->LOAD_INITIAL_NEWER_FEED:LX/I5e;

    invoke-static {v0}, LX/I5f;->j(LX/I5f;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iget-object p0, v0, LX/I5f;->l:LX/0Vd;

    invoke-virtual {v1, v2, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2538999
    iget-object v1, v0, LX/I5f;->f:LX/1Ck;

    sget-object v2, LX/I5e;->LOAD_INITIAL_OLDER_FEED:LX/I5e;

    invoke-virtual {v0}, LX/I5f;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iget-object p0, v0, LX/I5f;->j:LX/0Vd;

    invoke-virtual {v1, v2, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2539000
    const/4 v1, 0x2

    iput v1, v0, LX/I5f;->r:I

    .line 2539001
    return-void
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2538992
    const/4 v0, 0x0

    return-object v0
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2538991
    iget-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    return v0
.end method

.method public final kK_()V
    .locals 2

    .prologue
    .line 2538988
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I8k;->b(Z)V

    .line 2538989
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->c(LX/IBP;)V

    .line 2538990
    return-void
.end method

.method public final kL_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2538975
    iput-boolean v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    .line 2538976
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->d(LX/IBP;)V

    .line 2538977
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    invoke-virtual {v0}, LX/I5f;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538978
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->k()V

    .line 2538979
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    invoke-virtual {v0, v2}, LX/I8k;->b(Z)V

    .line 2538980
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    invoke-virtual {v0}, LX/2jY;->z()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2538981
    :cond_1
    :goto_0
    return-void

    .line 2538982
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    invoke-virtual {v0, v1}, LX/E8m;->a(LX/2jY;)Z

    .line 2538983
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    .line 2538984
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/I8k;->E:Z

    .line 2538985
    invoke-virtual {v0}, LX/I8k;->j()V

    .line 2538986
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2538987
    goto :goto_0
.end method

.method public final kU_()V
    .locals 0

    .prologue
    .line 2538974
    return-void
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2539275
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    return-object v0
.end method

.method public final mK_()LX/63R;
    .locals 2

    .prologue
    .line 2539350
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v0

    .line 2539351
    instance-of v1, v0, LX/63R;

    if-eqz v1, :cond_0

    check-cast v0, LX/63R;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2539349
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2539346
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    if-nez v0, :cond_0

    const-string v0, "NO_SESSION_ID"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539347
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2539348
    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2539343
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v0, :cond_0

    .line 2539344
    const/4 v0, 0x0

    .line 2539345
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v1, :cond_1

    const-string v0, "ANDROID_EVENT_PERMALINK_PRIVATE"

    goto :goto_0

    :cond_1
    const-string v0, "ANDROID_EVENT_PERMALINK"

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/16 v2, 0x1f6

    const/4 v1, -0x1

    .line 2539285
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->O(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_0

    if-ne p1, v2, :cond_0

    .line 2539286
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    invoke-virtual {v0}, LX/I8k;->e()V

    .line 2539287
    :cond_0
    const/16 v0, 0x1f5

    if-ne p1, v0, :cond_2

    .line 2539288
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->d:LX/DBC;

    invoke-virtual {v0, p1, p2, p3}, LX/DBC;->a(IILandroid/content/Intent;)V

    .line 2539289
    :cond_1
    :goto_0
    return-void

    .line 2539290
    :cond_2
    if-eq p1, v2, :cond_3

    const/16 v0, 0x1f7

    if-ne p1, v0, :cond_6

    .line 2539291
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->e:LX/I7i;

    const/16 v5, 0x1f6

    .line 2539292
    const/4 v3, -0x1

    if-ne p2, v3, :cond_4

    if-eq p1, v5, :cond_10

    const/16 v3, 0x1f7

    if-eq p1, v3, :cond_10

    .line 2539293
    :cond_4
    :goto_1
    if-ne p1, v2, :cond_1

    if-ne p2, v1, :cond_1

    .line 2539294
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539295
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->j:Z

    move v0, v1

    .line 2539296
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/events/model/Event;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2539297
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081ee9

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081ee8

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/I7E;

    invoke-direct {v2, p0}, LX/I7E;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2539298
    :cond_5
    goto :goto_0

    .line 2539299
    :cond_6
    const/16 v0, 0x1f8

    if-ne p1, v0, :cond_8

    if-ne p2, v1, :cond_8

    .line 2539300
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->i:LX/I8x;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    const/4 v6, 0x0

    .line 2539301
    if-nez p3, :cond_14

    .line 2539302
    :cond_7
    :goto_2
    goto :goto_0

    .line 2539303
    :cond_8
    const/16 v0, 0x1fb

    if-ne p1, v0, :cond_a

    if-ne p2, v1, :cond_a

    .line 2539304
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->j:LX/I94;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    const/4 v6, 0x0

    .line 2539305
    if-nez p3, :cond_16

    .line 2539306
    :cond_9
    :goto_3
    goto :goto_0

    .line 2539307
    :cond_a
    const/16 v0, 0x6dc

    if-eq p1, v0, :cond_b

    const/16 v0, 0x6de

    if-ne p1, v0, :cond_c

    :cond_b
    if-ne p2, v1, :cond_c

    .line 2539308
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2539309
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->V:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2539310
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0

    .line 2539311
    :cond_c
    const/16 v0, 0x138a

    if-ne p1, v0, :cond_d

    if-ne p2, v1, :cond_d

    .line 2539312
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2539313
    :cond_d
    if-ne p2, v1, :cond_f

    const/16 v0, 0x6f

    if-eq p1, v0, :cond_e

    const/16 v0, 0x6b

    if-ne p1, v0, :cond_f

    .line 2539314
    :cond_e
    if-eqz p3, :cond_1

    const-string v0, "extra_event_cancel_state"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2539315
    const-string v0, "extra_event_cancel_state"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2539316
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 2539317
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    new-instance v1, LX/BlC;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539318
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2539319
    invoke-direct {v1, v2}, LX/BlC;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2539320
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->b:LX/0TD;

    new-instance v1, Lcom/facebook/events/permalink/EventPermalinkFragment$17;

    invoke-direct {v1, p0}, Lcom/facebook/events/permalink/EventPermalinkFragment$17;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    const v2, -0x769bcb0e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_0

    .line 2539321
    :cond_f
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2539322
    :cond_10
    if-eqz p3, :cond_4

    .line 2539323
    const-string v3, "is_uploading_media"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2539324
    if-eqz v3, :cond_11

    .line 2539325
    const-string v3, "extra_composer_has_published"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2539326
    iget-object v3, v0, LX/I7i;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v3, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_1

    .line 2539327
    :cond_11
    new-instance v4, LX/I7h;

    invoke-direct {v4, v0, p1}, LX/I7h;-><init>(LX/I7i;I)V

    .line 2539328
    if-ne p1, v5, :cond_12

    .line 2539329
    iget-object v3, v0, LX/I7i;->i:LX/Bl6;

    new-instance v5, LX/BlN;

    sget-object v6, LX/BlI;->SENDING:LX/BlI;

    invoke-direct {v5, v6}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v3, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2539330
    :cond_12
    iget-object v5, v0, LX/I7i;->h:LX/1Ck;

    const-string v6, "EVENT_POST_TASK"

    const-string v3, "extra_composer_has_published"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 2539331
    sget-object v3, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v3, v3

    .line 2539332
    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    :goto_4
    invoke-virtual {v5, v6, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_1

    :cond_13
    iget-object v3, v0, LX/I7i;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v3, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_4

    .line 2539333
    :cond_14
    const-string v2, "is_uploading_media"

    invoke-virtual {p3, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2539334
    iget-object v2, v0, LX/I8x;->c:LX/Bl6;

    new-instance v3, LX/BlN;

    sget-object v4, LX/BlI;->SENDING:LX/BlI;

    invoke-direct {v3, v4}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2539335
    iget-object v3, v0, LX/I8x;->a:LX/0Sh;

    const-string v2, "extra_composer_has_published"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2539336
    sget-object v2, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v2, v2

    .line 2539337
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    :goto_5
    new-instance v4, LX/I8v;

    invoke-direct {v4, v0}, LX/I8v;-><init>(LX/I8x;)V

    invoke-virtual {v3, v2, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_2

    :cond_15
    iget-object v2, v0, LX/I8x;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    new-instance v4, LX/I8w;

    iget-object v5, v0, LX/I8x;->d:LX/1nQ;

    invoke-direct {v4, v1, v5}, LX/I8w;-><init>(Ljava/lang/String;LX/1nQ;)V

    invoke-virtual {v2, p3, v4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_5

    .line 2539338
    :cond_16
    const-string v2, "is_uploading_media"

    invoke-virtual {p3, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2539339
    iget-object v2, v0, LX/I94;->c:LX/Bl6;

    new-instance v3, LX/BlN;

    sget-object v4, LX/BlI;->SENDING:LX/BlI;

    invoke-direct {v3, v4}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2539340
    iget-object v3, v0, LX/I94;->a:LX/0Sh;

    const-string v2, "extra_composer_has_published"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2539341
    sget-object v2, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v2, v2

    .line 2539342
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    :goto_6
    new-instance v4, LX/I92;

    invoke-direct {v4, v0}, LX/I92;-><init>(LX/I94;)V

    invoke-virtual {v3, v2, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_3

    :cond_17
    iget-object v2, v0, LX/I94;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    new-instance v4, LX/I93;

    iget-object v5, v0, LX/I94;->d:LX/1nQ;

    invoke-direct {v4, v1, v5}, LX/I93;-><init>(Ljava/lang/String;LX/1nQ;)V

    invoke-virtual {v2, p3, v4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2539279
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2539280
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    if-eqz v0, :cond_0

    .line 2539281
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    .line 2539282
    iget-object p0, v0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz p0, :cond_0

    .line 2539283
    iget-object p0, v0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    invoke-virtual {v0}, LX/IBq;->c()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setTranslationY(F)V

    .line 2539284
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x17cc8826

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539276
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aI:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2539277
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v3, LX/IBP;->CREATE_VIEW:LX/IBP;

    invoke-virtual {v2, v3}, LX/IBQ;->a(LX/IBP;)J

    .line 2539278
    const v2, 0x7f0304f6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x22e3e3ac

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7808bee8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539073
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v2, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v1, v2}, LX/IBQ;->b(LX/IBP;)V

    .line 2539074
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    if-eqz v1, :cond_0

    .line 2539075
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->af:LX/2iz;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539076
    iget-object v4, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2539077
    invoke-virtual {v1, v2}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2539078
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539079
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    if-eqz v1, :cond_1

    .line 2539080
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v1}, LX/E8m;->n()V

    .line 2539081
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v1}, LX/E93;->dispose()V

    .line 2539082
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v2}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2539083
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->O:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2539084
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    if-eqz v1, :cond_2

    .line 2539085
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aD:LX/I7U;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539086
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aC:LX/I7P;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539087
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aA:LX/I7S;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539088
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aB:LX/I7T;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539089
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aE:LX/I7O;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539090
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->as:LX/0Yb;

    if-eqz v1, :cond_3

    .line 2539091
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->as:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2539092
    :cond_3
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->C:LX/DCI;

    if-eqz v1, :cond_4

    .line 2539093
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->C:LX/DCI;

    invoke-virtual {v1}, LX/DCI;->a()V

    .line 2539094
    :cond_4
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->E:LX/DCL;

    if-eqz v1, :cond_5

    .line 2539095
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->E:LX/DCL;

    invoke-virtual {v1}, LX/DCL;->a()V

    .line 2539096
    :cond_5
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->N()V

    .line 2539097
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->g:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2539098
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2539099
    iget-object v2, v1, LX/I8k;->g:LX/1Qq;

    invoke-interface {v2}, LX/0Vf;->dispose()V

    .line 2539100
    iget-object v2, v1, LX/I8k;->H:LX/2ja;

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/I8k;->F:LX/IBq;

    sget-object v4, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v2, v4}, LX/IBq;->b(LX/IBr;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2539101
    iget-object v2, v1, LX/I8k;->H:LX/2ja;

    invoke-virtual {v2}, LX/2ja;->e()V

    .line 2539102
    :cond_6
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2539103
    const/16 v1, 0x2b

    const v2, 0x1d737f8a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x325043fc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539259
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    .line 2539260
    iget-object v2, v1, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x60015

    const-string v6, "EventPermalinkFragment"

    invoke-interface {v2, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2539261
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->X:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x60016

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2539262
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->f:LX/BiW;

    .line 2539263
    iget-object v2, v1, LX/BiW;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v5, v1, LX/BiW;->a:LX/0Yj;

    invoke-interface {v2, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 2539264
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    invoke-virtual {v1}, LX/IBQ;->c()V

    .line 2539265
    iput-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    .line 2539266
    iput-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->an:Landroid/view/View;

    .line 2539267
    iput-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aq:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2539268
    iput-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ar:Lcom/facebook/resources/ui/FbTextView;

    .line 2539269
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_0

    .line 2539270
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2539271
    iput-object v3, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2539272
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2539273
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Z:LX/8wR;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->bc:LX/8wT;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2539274
    const/16 v1, 0x2b

    const v2, -0x577faff5

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x66d8bc1b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539257
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2539258
    const/16 v1, 0x2b

    const v2, -0x459ccdf9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x63aec757

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539252
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2539253
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->V()V

    .line 2539254
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->I:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->d()V

    .line 2539255
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v1}, LX/I8k;->f()V

    .line 2539256
    const/16 v1, 0x2b

    const v2, -0x1e58680

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2539247
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2539248
    const-string v0, "has_logged_first_scroll"

    iget-boolean v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aR:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2539249
    const-string v0, "show_reaction_units"

    iget-boolean v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aS:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2539250
    const-string v0, "on_demand_waiting_for_load"

    iget-boolean v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2539251
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2f3b87b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2539227
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2539228
    invoke-direct {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->C()V

    .line 2539229
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v1, :cond_1

    .line 2539230
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->K:LX/0Uh;

    const/16 v2, 0x338

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2539231
    :goto_1
    const/16 v1, 0x2b

    const v2, -0x2f5ff867

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2539232
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    sget-object v2, LX/I7V;->FETCH_PERMALINK_STORIES_GRAPHQL:LX/I7V;

    invoke-virtual {v1, v2}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2539233
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2539234
    iget-object v2, v1, LX/I8k;->d:LX/I8W;

    invoke-virtual {v2}, LX/I8W;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object v1, v2

    .line 2539235
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/Bkv;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2539236
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->J(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    goto :goto_0

    .line 2539237
    :cond_2
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->h:LX/Bkv;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-virtual {v2, v1, v4}, LX/Bkv;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2539238
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->J(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    goto :goto_0

    .line 2539239
    :cond_3
    new-instance v1, LX/4ET;

    invoke-direct {v1}, LX/4ET;-><init>()V

    .line 2539240
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v2, v2

    .line 2539241
    const-string v4, "event_id"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2539242
    move-object v1, v1

    .line 2539243
    new-instance v2, LX/7uJ;

    invoke-direct {v2}, LX/7uJ;-><init>()V

    move-object v2, v2

    .line 2539244
    const-string v4, "input"

    invoke-virtual {v2, v4, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2539245
    :try_start_0
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->N:LX/0gX;

    invoke-virtual {v1, v2}, LX/0gX;->a(LX/0gV;)LX/0gM;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ax:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2539246
    :catch_0
    goto :goto_1
.end method

.method public final onStop()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x722ee104

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2539219
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2539220
    invoke-static {}, LX/I7V;->values()[LX/I7V;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2539221
    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    invoke-virtual {v5, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2539222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2539223
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ax:LX/0gM;

    if-eqz v0, :cond_1

    .line 2539224
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->N:LX/0gX;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ax:LX/0gM;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 2539225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ax:LX/0gM;

    .line 2539226
    :cond_1
    const v0, 0x574a701

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 2539149
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2539150
    const v0, 0x7f0d0e3c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->am:Landroid/view/ViewGroup;

    .line 2539151
    const v4, 0x7f0d04e7

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2539152
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aM:LX/1P0;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2539153
    new-instance v4, LX/0g7;

    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v4, v5}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aO:LX/0g7;

    .line 2539154
    const v4, 0x7f0d0e3d

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewStub;

    .line 2539155
    sget-boolean v4, Lcom/facebook/events/permalink/EventPermalinkFragment;->al:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ah:LX/0hI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v9

    .line 2539156
    :goto_0
    new-instance v4, LX/IBq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    iget-object v8, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->c:Ljava/lang/Boolean;

    invoke-direct/range {v4 .. v9}, LX/IBq;-><init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;Ljava/lang/Boolean;I)V

    iput-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    .line 2539157
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    .line 2539158
    iput-object v5, v4, LX/I8k;->F:LX/IBq;

    .line 2539159
    iget-object v6, v4, LX/I8k;->i:LX/I8m;

    iget-object v7, v4, LX/I8k;->F:LX/IBq;

    .line 2539160
    iput-object v7, v6, LX/I8m;->b:LX/IBq;

    .line 2539161
    iget-object v6, v4, LX/I8k;->F:LX/IBq;

    new-instance v7, LX/I8e;

    invoke-direct {v7, v4}, LX/I8e;-><init>(LX/I8k;)V

    .line 2539162
    iput-object v7, v6, LX/IBq;->d:LX/I8d;

    .line 2539163
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aP:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v5, LX/I7M;

    invoke-direct {v5, p0}, LX/I7M;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2539164
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aO:LX/0g7;

    move-object v0, v4

    .line 2539165
    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    .line 2539166
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2539167
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    .line 2539168
    iput-object v1, v0, LX/I8k;->D:LX/0g8;

    .line 2539169
    iget-object v2, v0, LX/I8k;->i:LX/I8m;

    iget-object v4, v0, LX/I8k;->D:LX/0g8;

    .line 2539170
    iput-object v4, v2, LX/I8m;->a:LX/0g8;

    .line 2539171
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v0, v3}, LX/0g7;->d(Z)V

    .line 2539172
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->k()V

    .line 2539173
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ap:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/I7N;

    invoke-direct {v1, p0}, LX/I7N;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2539174
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    new-instance v1, LX/I78;

    invoke-direct {v1, p0}, LX/I78;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fu;)V

    .line 2539175
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->au:I

    .line 2539176
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    new-instance v1, LX/I79;

    invoke-direct {v1, p0}, LX/I79;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v0, v1}, LX/0g7;->b(LX/0fx;)V

    .line 2539177
    new-instance v0, LX/I7K;

    invoke-direct {v0, p0}, LX/I7K;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2539178
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->C:LX/DCI;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    .line 2539179
    iget-object v4, v2, LX/0fz;->a:LX/0qq;

    move-object v2, v4

    .line 2539180
    invoke-virtual {v1, v2, v0}, LX/DCI;->a(LX/0qq;LX/0g4;)V

    .line 2539181
    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->E:LX/DCL;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    .line 2539182
    iget-object v4, v2, LX/0fz;->a:LX/0qq;

    move-object v2, v4

    .line 2539183
    invoke-virtual {v1, v2, v0}, LX/DCL;->a(LX/0qq;LX/0g4;)V

    .line 2539184
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aA:LX/I7S;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539185
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aB:LX/I7T;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539186
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aC:LX/I7P;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539187
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aD:LX/I7U;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539188
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->x:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aE:LX/I7O;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539189
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->CREATE_VIEW:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->d(LX/IBP;)V

    .line 2539190
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v1, LX/IBP;->RENDERING:LX/IBP;

    invoke-virtual {v0, v1}, LX/IBQ;->a(LX/IBP;)J

    .line 2539191
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aY:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_3

    .line 2539192
    sget-object v0, LX/5O9;->NOTIFICATION_PREFETCH:LX/5O9;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(LX/5O9;)V

    .line 2539193
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aY:Lcom/facebook/events/model/Event;

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2539194
    invoke-static {p0, v3}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    .line 2539195
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    .line 2539196
    iget-object v2, v1, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2539197
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2539198
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aw:Z

    if-nez v0, :cond_2

    .line 2539199
    const/4 v10, 0x0

    .line 2539200
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->z:LX/0zG;

    invoke-interface {v4}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, LX/0h5;

    .line 2539201
    instance-of v4, v5, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    if-eqz v4, :cond_1

    move-object v4, v5

    .line 2539202
    check-cast v4, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v4, v10}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2539203
    :cond_1
    const-class v4, LX/63U;

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/63U;

    .line 2539204
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ba:LX/63Q;

    if-nez v4, :cond_2

    if-nez v8, :cond_7

    .line 2539205
    :cond_2
    :goto_2
    new-instance v0, LX/I7L;

    invoke-direct {v0, p0}, LX/I7L;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->bc:LX/8wT;

    .line 2539206
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Z:LX/8wR;

    iget-object v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->bc:LX/8wT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2539207
    return-void

    .line 2539208
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2539209
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2539210
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    invoke-static {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;Lcom/facebook/events/model/Event;)V

    .line 2539211
    :goto_3
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_5

    .line 2539212
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2539213
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2539214
    goto :goto_1

    .line 2539215
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 2539216
    :cond_7
    new-instance v4, LX/63Q;

    invoke-direct {v4}, LX/63Q;-><init>()V

    iput-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ba:LX/63Q;

    .line 2539217
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ba:LX/63Q;

    move-object v6, v5

    check-cast v6, LX/63T;

    iget-object v7, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ao:LX/0g7;

    const/4 v9, 0x1

    move-object v5, p0

    invoke-virtual/range {v4 .. v10}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    goto :goto_2

    .line 2539218
    :cond_8
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->Y:LX/1Ck;

    sget-object v1, LX/I7V;->FETCH_EVENT_FROM_DB:LX/I7V;

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aV:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I7A;

    invoke-direct {v3, p0}, LX/I7A;-><init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_3
.end method

.method public final p()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2539104
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->n:LX/I5f;

    invoke-virtual {v2}, LX/I5f;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2539105
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v2, v3, :cond_1

    .line 2539106
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->k()V

    .line 2539107
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    invoke-virtual {v2}, LX/I8k;->m()V

    .line 2539108
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v2, :cond_4

    .line 2539109
    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->W:LX/0ad;

    sget-short v3, LX/347;->O:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    iput-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aS:Z

    .line 2539110
    iget-boolean v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aS:Z

    if-eqz v0, :cond_6

    .line 2539111
    iput-boolean v1, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aT:Z

    .line 2539112
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    if-nez v4, :cond_7

    .line 2539113
    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->o()Ljava/lang/String;

    move-result-object v5

    .line 2539114
    if-nez v5, :cond_8

    .line 2539115
    :cond_4
    :goto_1
    return-void

    .line 2539116
    :cond_5
    invoke-static {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->J(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    goto :goto_0

    .line 2539117
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    .line 2539118
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/IBQ;->f:Z

    .line 2539119
    invoke-static {v0}, LX/IBQ;->f(LX/IBQ;)V

    .line 2539120
    goto :goto_1

    .line 2539121
    :cond_7
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    if-nez v4, :cond_9

    .line 2539122
    :goto_2
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v5, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v4, v5}, LX/IBQ;->b(LX/IBP;)V

    .line 2539123
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v5, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v4, v5}, LX/IBQ;->a(LX/IBP;)J

    .line 2539124
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->af:LX/2iz;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2iz;->f(Ljava/lang/String;)V

    .line 2539125
    goto :goto_1

    .line 2539126
    :cond_8
    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v7, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    invoke-virtual {v6, v7}, LX/IBQ;->a(LX/IBP;)J

    .line 2539127
    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ae:LX/CfW;

    new-instance v7, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v7}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    const-wide/16 v9, 0x2

    .line 2539128
    iput-wide v9, v7, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2539129
    move-object v7, v7

    .line 2539130
    iget-object v8, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v8, v8

    .line 2539131
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2539132
    iput-object v8, v7, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2539133
    move-object v7, v7

    .line 2539134
    invoke-virtual {v6, v5, v7}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539135
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2539136
    const-string v6, "source_entity_id"

    .line 2539137
    iget-object v7, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v7, v7

    .line 2539138
    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2539139
    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539140
    iput-object v5, v6, LX/2jY;->x:Landroid/os/Bundle;

    .line 2539141
    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ac:LX/Cfr;

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    .line 2539142
    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    .line 2539143
    iput-object v6, v5, LX/I8k;->H:LX/2ja;

    .line 2539144
    iget-object v5, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->af:LX/2iz;

    iget-object v6, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aJ:LX/2jY;

    .line 2539145
    iget-object v7, v6, LX/2jY;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2539146
    invoke-virtual {v5, v6, p0}, LX/2iz;->a(Ljava/lang/String;LX/CfG;)V

    goto :goto_1

    .line 2539147
    :cond_9
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    invoke-virtual {v4}, LX/2ja;->g()V

    .line 2539148
    iget-object v4, p0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aN:LX/2ja;

    invoke-virtual {v4}, LX/2ja;->i()V

    goto :goto_2
.end method
