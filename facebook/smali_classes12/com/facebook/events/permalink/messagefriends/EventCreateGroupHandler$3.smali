.class public final Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/IAd;


# direct methods
.method public constructor <init>(LX/IAd;LX/0Pz;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2545925
    iput-object p1, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->c:LX/IAd;

    iput-object p2, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->a:LX/0Pz;

    iput-object p3, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2545926
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->c:LX/IAd;

    iget-object v0, v0, LX/IAd;->d:LX/11H;

    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->c:LX/IAd;

    iget-object v1, v1, LX/IAd;->c:LX/IAe;

    .line 2545927
    new-instance v2, LX/6he;

    invoke-direct {v2}, LX/6he;-><init>()V

    move-object v2, v2

    .line 2545928
    iget-object v3, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->a:LX/0Pz;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2545929
    iput-object v3, v2, LX/6he;->c:Ljava/util/List;

    .line 2545930
    move-object v2, v2

    .line 2545931
    new-instance v3, Lcom/facebook/messaging/service/model/CreateGroupParams;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/service/model/CreateGroupParams;-><init>(LX/6he;)V

    move-object v2, v3

    .line 2545932
    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2545933
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    const v2, -0x682da0df

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2545934
    :goto_0
    return-void

    .line 2545935
    :catch_0
    move-exception v0

    .line 2545936
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
