.class public Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IAk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IAd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/7nK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Landroid/widget/ProgressBar;

.field public j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public k:Lcom/facebook/widget/error/GenericErrorViewStub;

.field public l:LX/IAh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1ZF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private o:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private p:LX/63W;

.field public q:Lcom/facebook/widget/error/GenericErrorView;

.field public r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2546219
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v3

    check-cast v3, LX/B9y;

    new-instance p1, LX/IAk;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {p1, v4, v5, p0}, LX/IAk;-><init>(LX/0tX;Landroid/content/res/Resources;LX/1Ck;)V

    move-object v4, p1

    check-cast v4, LX/IAk;

    const/16 v5, 0x1b7b

    invoke-static {v6, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x2ca

    invoke-static {v6, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    iput-object v2, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b:LX/B9y;

    iput-object v4, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->c:LX/IAk;

    iput-object v5, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->d:LX/0Ot;

    iput-object v6, v1, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->e:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;Z)V
    .locals 2

    .prologue
    .line 2546212
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    if-nez v0, :cond_0

    .line 2546213
    :goto_0
    return-void

    .line 2546214
    :cond_0
    if-eqz p1, :cond_1

    .line 2546215
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->n:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2546216
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->p:LX/63W;

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0

    .line 2546217
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->o:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2546218
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2546140
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081f7a

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/IAn;

    invoke-direct {v2, p0}, LX/IAn;-><init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2546141
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2546188
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2546189
    const-class v0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2546190
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546191
    const-string v1, "extra_message_as_group"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->f:Z

    .line 2546192
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2546193
    const-string v1, "event_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->g:Ljava/lang/String;

    .line 2546194
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2546195
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->c:LX/IAk;

    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/IAk;->a(Ljava/lang/String;Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V

    .line 2546196
    :goto_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f081f73

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2546197
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2546198
    move-object v0, v0

    .line 2546199
    const/4 v1, -0x2

    .line 2546200
    iput v1, v0, LX/108;->h:I

    .line 2546201
    move-object v0, v0

    .line 2546202
    const/4 v1, 0x1

    .line 2546203
    iput-boolean v1, v0, LX/108;->d:Z

    .line 2546204
    move-object v1, v0

    .line 2546205
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->n:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2546206
    iput-boolean v3, v0, LX/108;->d:Z

    .line 2546207
    move-object v0, v0

    .line 2546208
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->o:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2546209
    new-instance v0, LX/IAl;

    invoke-direct {v0, p0}, LX/IAl;-><init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->p:LX/63W;

    .line 2546210
    return-void

    .line 2546211
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b()V

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2546179
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546180
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b:LX/B9y;

    invoke-virtual {v0}, LX/B9y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2546181
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, LX/0ax;->aj:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2546182
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2546183
    :goto_0
    return-void

    .line 2546184
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b:LX/B9y;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v2}, LX/7nK;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v3}, LX/7nK;->eN_()Ljava/lang/String;

    move-result-object v3

    .line 2546185
    iget-object v4, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v4}, LX/7nK;->b()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v4}, LX/7nK;->b()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object v4, v4

    .line 2546186
    iget-object v5, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v5}, LX/7nK;->c()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v5}, LX/7nK;->c()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;->a()Ljava/lang/String;

    move-result-object v5

    :goto_2
    move-object v5, v5

    .line 2546187
    iget-object v6, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    invoke-interface {v6}, LX/7nK;->e()Ljava/lang/String;

    move-result-object v6

    const-string v8, "event"

    const/16 v9, 0x64

    move-object v7, p1

    invoke-virtual/range {v0 .. v9}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2546168
    const/16 v1, 0x8

    .line 2546169
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    if-nez v0, :cond_0

    .line 2546170
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->k:Lcom/facebook/widget/error/GenericErrorViewStub;

    invoke-virtual {v0}, LX/4nv;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    .line 2546171
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2546172
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2546173
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2546174
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2546175
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    .line 2546176
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    new-instance v1, LX/IAo;

    invoke-direct {v1, p0}, LX/IAo;-><init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2546177
    return-void

    .line 2546178
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->q:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2546162
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2546163
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2546164
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2546165
    if-eqz v0, :cond_0

    .line 2546166
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2546167
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6bf84844

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546160
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2546161
    const v1, 0x7f0304db

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5cf249be

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x625eb61b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546155
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->c:LX/IAk;

    if-eqz v1, :cond_0

    .line 2546156
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->c:LX/IAk;

    .line 2546157
    iget-object v2, v1, LX/IAk;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2546158
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2546159
    const/16 v1, 0x2b

    const v2, 0x69eb69b8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x56a922a7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546147
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2546148
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    .line 2546149
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    if-nez v1, :cond_0

    .line 2546150
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x73fe42a2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2546151
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    iget-boolean v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->f:Z

    if-eqz v1, :cond_1

    const v1, 0x7f081f6a

    :goto_1
    invoke-interface {v2, v1}, LX/1ZF;->x_(I)V

    .line 2546152
    iget-object v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->m:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2546153
    iget-boolean v1, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->r:Z

    invoke-static {p0, v1}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;Z)V

    goto :goto_0

    .line 2546154
    :cond_1
    const v1, 0x7f081f69

    goto :goto_1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2546142
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2546143
    const v0, 0x7f0d05b0

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->i:Landroid/widget/ProgressBar;

    .line 2546144
    const v0, 0x7f0d0e24

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2546145
    const v0, 0x7f0d0a4a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorViewStub;

    iput-object v0, p0, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->k:Lcom/facebook/widget/error/GenericErrorViewStub;

    .line 2546146
    return-void
.end method
