.class public Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Landroid/view/View;

.field private e:LX/5O6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542680
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2542681
    invoke-direct {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a()V

    .line 2542682
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542677
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542678
    invoke-direct {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a()V

    .line 2542679
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2542674
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2542675
    invoke-direct {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a()V

    .line 2542676
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2542667
    const v0, 0x7f0304cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2542668
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->e:LX/5O6;

    .line 2542669
    const v0, 0x7f0d0df8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->d:Landroid/view/View;

    .line 2542670
    const v0, 0x7f0d0df9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2542671
    const v0, 0x7f0d0dfa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2542672
    const v0, 0x7f0d0dfb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2542673
    return-void
.end method


# virtual methods
.method public getComposerContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 2542683
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->d:Landroid/view/View;

    return-object v0
.end method

.method public getComposerPostPhoto()Lcom/facebook/fbui/glyph/GlyphView;
    .locals 1

    .prologue
    .line 2542666
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->c:Lcom/facebook/fbui/glyph/GlyphView;

    return-object v0
.end method

.method public getComposerPrompt()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 2542665
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->b:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getComposerVoice()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 2542664
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2542660
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2542661
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->e:LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, p1, v1}, LX/5O6;->a(Landroid/graphics/Canvas;F)V

    .line 2542662
    iget-object v0, p0, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->e:LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, p1, v1}, LX/5O6;->b(Landroid/graphics/Canvas;F)V

    .line 2542663
    return-void
.end method
