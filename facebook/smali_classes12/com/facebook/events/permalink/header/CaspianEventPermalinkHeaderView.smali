.class public Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final G:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventArtist;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcom/facebook/events/common/EventAnalyticsParams;

.field private C:Z

.field private D:Landroid/content/Context;

.field private E:I

.field public F:[Ljava/lang/String;

.field private final H:Landroid/view/View$OnClickListener;

.field private I:LX/Bar;

.field public m:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/events/model/Event;

.field private v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field private w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/IAN;

.field public z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2545617
    const-class v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    const-string v1, "event_permalink"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->G:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2545613
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545614
    new-instance v0, LX/IAJ;

    invoke-direct {v0, p0}, LX/IAJ;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2545615
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/content/Context;)V

    .line 2545616
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2545609
    invoke-direct {p0, p1, p2, p3}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545610
    new-instance v0, LX/IAJ;

    invoke-direct {v0, p0}, LX/IAJ;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2545611
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/content/Context;)V

    .line 2545612
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 2545604
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2545605
    new-instance v0, LX/IAJ;

    invoke-direct {v0, p0}, LX/IAJ;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2545606
    iput-boolean p2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    .line 2545607
    invoke-direct {p0, p1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/content/Context;)V

    .line 2545608
    return-void
.end method

.method private a(LX/0Px;Z)Landroid/text/Spannable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;Z)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2545590
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2545591
    :cond_0
    :goto_0
    return-object v1

    .line 2545592
    :cond_1
    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545593
    iget-object v2, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2545594
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2545595
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2545596
    if-nez v0, :cond_3

    .line 2545597
    :goto_1
    if-eqz p2, :cond_4

    .line 2545598
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2545599
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081f13

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "{sentence}"

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2545600
    const-string v1, "{sentence}"

    const/16 v3, 0x11

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 2545601
    :goto_2
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->k()Landroid/text/style/ClickableSpan;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v3, v4}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    if-nez v0, :cond_2

    move-object v0, v2

    :cond_2
    invoke-virtual {v1, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    .line 2545602
    invoke-virtual {v0}, LX/47x;->a()LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    goto :goto_0

    .line 2545603
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00da

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/text/SpannableString;
    .locals 3

    .prologue
    .line 2545588
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2545589
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, LX/IAK;

    invoke-direct {v2, p0, v0}, LX/IAK;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;Ljava/lang/String;)V

    const/16 v0, 0x21

    invoke-virtual {v1, v2, v0}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->a()LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/text/Spannable;)Landroid/text/SpannableStringBuilder;
    .locals 3

    .prologue
    .line 2545584
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v0

    .line 2545585
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2545586
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0

    .line 2545587
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static varargs a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2545583
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(ZLjava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(ZLjava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2545569
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const-string v0, " \u00b7 "

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2545570
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2545571
    if-eqz p1, :cond_4

    .line 2545572
    invoke-virtual {v4, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2545573
    if-eqz p0, :cond_2

    .line 2545574
    const-string v0, "\n"

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move v0, v1

    .line 2545575
    :goto_0
    array-length v5, p2

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, p2, v1

    .line 2545576
    if-eqz v6, :cond_1

    .line 2545577
    if-eqz v0, :cond_0

    .line 2545578
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2545579
    :cond_0
    invoke-virtual {v4, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move v0, v2

    .line 2545580
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2545581
    goto :goto_0

    .line 2545582
    :cond_3
    return-object v4

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Ljava/lang/CharSequence;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2545558
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2545559
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f12

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2545560
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2545561
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2545562
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 2545563
    :cond_0
    :goto_0
    return-object v0

    .line 2545564
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v0, v1, :cond_2

    .line 2545565
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082af2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2545566
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne v0, v1, :cond_3

    .line 2545567
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082af3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2545568
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 2545374
    const-class v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2545375
    iput-object p1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->D:Landroid/content/Context;

    .line 2545376
    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->d:LX/Bam;

    .line 2545377
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2545378
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545379
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2545380
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_1

    .line 2545381
    iget-object v0, p0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setVisibility(I)V

    .line 2545382
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0886

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setMinimumHeight(I)V

    .line 2545383
    :goto_0
    return-void

    .line 2545384
    :cond_1
    invoke-virtual {p0}, LX/Ban;->e()V

    goto :goto_0
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V
    .locals 4
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2545550
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2545551
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 2545552
    iget-object v1, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2545553
    aput-object v1, v0, v2

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->F:[Ljava/lang/String;

    .line 2545554
    :goto_0
    return-void

    .line 2545555
    :cond_0
    new-array v0, v3, [Ljava/lang/String;

    .line 2545556
    iget-object v1, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2545557
    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->F:[Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 3
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2545526
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2545527
    iput-object p1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545528
    iput-object p2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2545529
    iput-object p3, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2545530
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V

    .line 2545531
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0e0838

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2545532
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0e0834

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleTextAppearance(I)V

    .line 2545533
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_0

    .line 2545534
    new-instance v0, LX/Bar;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->D:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Bar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->I:LX/Bar;

    .line 2545535
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->I:LX/Bar;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 2545536
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->I:LX/Bar;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v1

    .line 2545537
    iget-object p1, v0, LX/Bar;->b:Landroid/widget/TextView;

    const-string p2, "dd"

    invoke-static {p2, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2545538
    iget-object p1, v0, LX/Bar;->c:Landroid/widget/TextView;

    const-string p2, "MMM"

    invoke-static {p2, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    iget-object p3, v0, LX/Bar;->a:LX/0W9;

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2545539
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x7f010299

    aput v1, v0, v2

    .line 2545540
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->D:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2545541
    const/4 v1, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->E:I

    .line 2545542
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2545543
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->h()V

    .line 2545544
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_1

    .line 2545545
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b()V

    .line 2545546
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->g()V

    .line 2545547
    return-void

    .line 2545548
    :cond_2
    const v0, 0x7f0e0837

    goto :goto_0

    .line 2545549
    :cond_3
    const v0, 0x7f0e0833

    goto :goto_1
.end method

.method private static a(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;LX/I76;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Boolean;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1nQ;LX/0hB;LX/0ad;)V
    .locals 0

    .prologue
    .line 2545525
    iput-object p1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->m:LX/I76;

    iput-object p2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->n:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p3, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->o:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->p:Ljava/lang/Boolean;

    iput-object p5, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->q:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p6, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->r:LX/1nQ;

    iput-object p7, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->s:LX/0hB;

    iput-object p8, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->t:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    invoke-static {v8}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v1

    check-cast v1, LX/I76;

    invoke-static {v8}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v2

    check-cast v2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-static {v8}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v5

    check-cast v5, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v8}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-static {v8}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static {v8}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static/range {v0 .. v8}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;LX/I76;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Boolean;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1nQ;LX/0hB;LX/0ad;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2545366
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->s:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b088a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0881

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2545367
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2545368
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v3, 0x7f0e0838

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2545369
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v2}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getTitleTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/text/TextPaint;ILandroid/text/SpannableStringBuilder;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2545370
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v3, 0x7f0e0839

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2545371
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v2}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getTitleTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/text/TextPaint;ILandroid/text/SpannableStringBuilder;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2545372
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v1, 0x7f0e083a

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2545373
    :cond_0
    return-void
.end method

.method private a(Landroid/text/TextPaint;ILandroid/text/SpannableStringBuilder;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2545385
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v1, p3

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2545386
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->E:I

    if-gt v0, v1, :cond_0

    const/4 v7, 0x1

    :cond_0
    return v7
.end method

.method private b(LX/0Px;Z)Landroid/text/Spannable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventArtist;",
            ">;Z)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2545387
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2545388
    :cond_0
    :goto_0
    return-object v1

    .line 2545389
    :cond_1
    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventArtist;

    .line 2545390
    iget-object v2, v0, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2545391
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2545392
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2545393
    if-nez v0, :cond_3

    .line 2545394
    :goto_1
    if-eqz p2, :cond_4

    .line 2545395
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2545396
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081f14

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "{sentence}"

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2545397
    const-string v1, "{sentence}"

    const/16 v3, 0x11

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 2545398
    :goto_2
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->k()Landroid/text/style/ClickableSpan;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v3, v4}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    if-nez v0, :cond_2

    move-object v0, v2

    :cond_2
    invoke-virtual {v1, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    .line 2545399
    invoke-virtual {v0}, LX/47x;->a()LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    goto :goto_0

    .line 2545400
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00da

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2545401
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2545402
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 2545403
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2545404
    const v0, 0x7f0d2d73

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2545405
    if-eqz v0, :cond_0

    .line 2545406
    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 2545407
    iget-object v1, p0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;I)V

    .line 2545408
    :cond_0
    return-void
.end method

.method private g()V
    .locals 15

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2545409
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v1, :cond_b

    .line 2545410
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v1

    .line 2545411
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 2545412
    new-instance v6, Landroid/graphics/PointF;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a()D

    move-result-wide v4

    double-to-float v2, v4

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->b()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-direct {v6, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2545413
    :goto_0
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2545414
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v4

    .line 2545415
    :goto_1
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 2545416
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    sget-object v2, LX/1bc;->LOW:LX/1bc;

    .line 2545417
    iput-object v2, v1, LX/1bX;->i:LX/1bc;

    .line 2545418
    move-object v1, v1

    .line 2545419
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2545420
    :goto_2
    iget-boolean v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v1, :cond_0

    .line 2545421
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545422
    iget-object v2, v1, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v1, v2

    .line 2545423
    if-eqz v1, :cond_2

    move v1, v8

    .line 2545424
    :goto_3
    if-eqz v1, :cond_3

    sget-object v1, LX/Bam;->NARROW:LX/Bam;

    :goto_4
    iput-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->d:LX/Bam;

    .line 2545425
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2545426
    :cond_0
    if-nez v4, :cond_1

    if-eqz v5, :cond_7

    .line 2545427
    :cond_1
    new-instance v12, LX/IAM;

    invoke-direct {v12, p0}, LX/IAM;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V

    .line 2545428
    :goto_5
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->t:LX/0ad;

    sget-short v1, LX/Bjt;->a:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    move v7, v8

    .line 2545429
    :goto_6
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    .line 2545430
    iget-object v9, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->F:[Ljava/lang/String;

    move-object v9, v9

    .line 2545431
    sget-object v10, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->G:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v11, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->H:Landroid/view/View$OnClickListener;

    iget-boolean v13, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-nez v13, :cond_5

    move v13, v8

    :goto_7
    iget-object v14, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v14, :cond_6

    move v14, v8

    :goto_8
    move v8, v3

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2545432
    return-void

    :cond_2
    move v1, v3

    .line 2545433
    goto :goto_3

    .line 2545434
    :cond_3
    sget-object v1, LX/Bam;->CUSTOM:LX/Bam;

    goto :goto_4

    :cond_4
    move v7, v3

    .line 2545435
    goto :goto_6

    :cond_5
    move v13, v3

    .line 2545436
    goto :goto_7

    :cond_6
    move v14, v3

    goto :goto_8

    :cond_7
    move-object v12, v0

    goto :goto_5

    :cond_8
    move-object v5, v0

    goto :goto_2

    :cond_9
    move-object v4, v0

    goto/16 :goto_1

    :cond_a
    move-object v6, v0

    goto/16 :goto_0

    :cond_b
    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    goto :goto_2
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2545437
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545438
    iget-object v1, v0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2545439
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2545440
    iget-boolean v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v1, :cond_0

    .line 2545441
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/String;)V

    .line 2545442
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v0, :cond_1

    .line 2545443
    :goto_0
    return-void

    .line 2545444
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_2

    .line 2545445
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->j()V

    goto :goto_0

    .line 2545446
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->i()V

    goto :goto_0
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 2545447
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    .line 2545448
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2545449
    sget-object v1, LX/IAN;->WITH_ARTISTS:LX/IAN;

    iput-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    .line 2545450
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-direct {p0, v1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2545451
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v3, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    if-ne v2, v3, :cond_3

    .line 2545452
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v2}, LX/Bm1;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    .line 2545453
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-direct {p0, v2, v4}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(LX/0Px;Z)Landroid/text/Spannable;

    move-result-object v2

    .line 2545454
    iget-object v3, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v3

    .line 2545455
    if-nez v3, :cond_2

    .line 2545456
    :goto_1
    new-array v3, v6, [Ljava/lang/CharSequence;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2545457
    :cond_0
    :goto_2
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2545458
    return-void

    .line 2545459
    :cond_1
    sget-object v1, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    iput-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    goto :goto_0

    .line 2545460
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2545461
    :cond_3
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v3, LX/IAN;->WITH_ARTISTS:LX/IAN;

    if-ne v2, v3, :cond_0

    .line 2545462
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-direct {p0, v2, v4}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b(LX/0Px;Z)Landroid/text/Spannable;

    move-result-object v2

    .line 2545463
    iget-object v3, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v3

    .line 2545464
    if-nez v3, :cond_4

    .line 2545465
    :goto_3
    new-array v3, v6, [Ljava/lang/CharSequence;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_2

    .line 2545466
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2545467
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    .line 2545468
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2545469
    sget-object v0, LX/IAN;->WITH_ARTISTS:LX/IAN;

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    .line 2545470
    :goto_0
    const/4 v0, 0x0

    .line 2545471
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v2, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    if-ne v1, v2, :cond_3

    .line 2545472
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v0}, LX/Bm1;->b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    .line 2545473
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545474
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v0, v1

    .line 2545475
    sget-object v1, LX/7vJ;->PAGE:LX/7vJ;

    if-ne v0, v1, :cond_2

    .line 2545476
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-direct {p0, v0, v3}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(LX/0Px;Z)Landroid/text/Spannable;

    move-result-object v0

    .line 2545477
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/text/Spannable;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2545478
    :cond_0
    :goto_1
    iget-object v1, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2545479
    return-void

    .line 2545480
    :cond_1
    sget-object v0, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    goto :goto_0

    .line 2545481
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-direct {p0, v0, v4}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(LX/0Px;Z)Landroid/text/Spannable;

    move-result-object v0

    .line 2545482
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-direct {p0, v1}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Ljava/lang/CharSequence;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/CharSequence;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1

    .line 2545483
    :cond_3
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v2, LX/IAN;->WITH_ARTISTS:LX/IAN;

    if-ne v1, v2, :cond_0

    .line 2545484
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-direct {p0, v0, v3}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b(LX/0Px;Z)Landroid/text/Spannable;

    move-result-object v0

    .line 2545485
    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Landroid/text/Spannable;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1
.end method

.method private k()Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2545486
    new-instance v0, LX/IAL;

    invoke-direct {v0, p0}, LX/IAL;-><init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V

    return-object v0
.end method

.method public static l(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2545487
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->n:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2545488
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v5

    .line 2545489
    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2545490
    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/data/model/PhotoSet;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 2545491
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->n:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    sget-object v3, LX/74S;->EVENT_COVER_PHOTO:LX/74S;

    invoke-interface {v1, v6, v7, v0, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;

    move-result-object v0

    .line 2545492
    if-eqz v0, :cond_0

    .line 2545493
    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->o:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2545494
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_2

    .line 2545495
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->r:LX/1nQ;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545496
    iget-object v3, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2545497
    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2545498
    :cond_1
    :goto_1
    return-void

    .line 2545499
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->r:LX/1nQ;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545500
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2545501
    iget-object v2, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 2545502
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, LX/Ban;->a(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0429

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;ZZ)V
    .locals 2
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2545503
    iput-boolean p4, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    .line 2545504
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2545505
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->y:Z

    move v0, v0

    .line 2545506
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    if-nez v0, :cond_0

    .line 2545507
    new-instance v0, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    .line 2545508
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b(Landroid/view/View;)V

    .line 2545509
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    if-eqz v0, :cond_1

    .line 2545510
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->x:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    iget-object v1, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->B:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2545511
    :cond_1
    if-eqz p5, :cond_2

    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    if-nez v0, :cond_2

    .line 2545512
    new-instance v0, Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    .line 2545513
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b(Landroid/view/View;)V

    .line 2545514
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    if-eqz v0, :cond_3

    .line 2545515
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->w:Lcom/facebook/events/permalink/draft/DraftEventBannerView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/permalink/draft/DraftEventBannerView;->a(Lcom/facebook/events/model/Event;)V

    .line 2545516
    :cond_3
    return-void
.end method

.method public getCoverPhotoDescription()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 2545517
    iget-object v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->F:[Ljava/lang/String;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2545518
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2545519
    invoke-virtual {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->b:I

    .line 2545520
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2545521
    iget-boolean v0, p0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->C:Z

    if-eqz v0, :cond_0

    .line 2545522
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->h()V

    .line 2545523
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->g()V

    .line 2545524
    return-void
.end method
