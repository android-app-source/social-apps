.class public Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/14x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/B9v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IAW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/events/model/Event;

.field public i:Lcom/facebook/events/common/EventAnalyticsParams;

.field private j:LX/5O6;

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545839
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2545840
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a()V

    .line 2545841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2545836
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2545837
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a()V

    .line 2545838
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2545833
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545834
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a()V

    .line 2545835
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2545827
    const-class v0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2545828
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->j:LX/5O6;

    .line 2545829
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->k:I

    .line 2545830
    invoke-virtual {p0, p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2545831
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2545832
    return-void
.end method

.method private a(LX/7TY;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2545825
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f81

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    invoke-static {}, LX/10A;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IAY;

    invoke-direct {v1, p0}, LX/IAY;-><init>(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2545826
    return-void
.end method

.method private a(LX/7TY;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2545823
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081f85

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v1, 0x7f02089a

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IAX;

    invoke-direct {v1, p0, p3}, LX/IAX;-><init>(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2545824
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;LX/I76;LX/DB4;LX/14x;LX/1nQ;LX/B9v;LX/IAW;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 2545822
    iput-object p1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a:LX/I76;

    iput-object p2, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->b:LX/DB4;

    iput-object p3, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->c:LX/14x;

    iput-object p4, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->d:LX/1nQ;

    iput-object p5, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->e:LX/B9v;

    iput-object p6, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->f:LX/IAW;

    iput-object p7, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->g:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    invoke-static {v7}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v1

    check-cast v1, LX/I76;

    invoke-static {v7}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v2

    check-cast v2, LX/DB4;

    invoke-static {v7}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v3

    check-cast v3, LX/14x;

    invoke-static {v7}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    invoke-static {v7}, LX/B9v;->b(LX/0QB;)LX/B9v;

    move-result-object v5

    check-cast v5, LX/B9v;

    new-instance p1, LX/IAW;

    invoke-static {v7}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v7}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v7}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-direct {p1, v6, v8, v9, p0}, LX/IAW;-><init>(LX/0kL;LX/0tX;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V

    move-object v6, p1

    check-cast v6, LX/IAW;

    invoke-static {v7}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static/range {v0 .. v7}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;LX/I76;LX/DB4;LX/14x;LX/1nQ;LX/B9v;LX/IAW;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2545812
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2545813
    new-instance v1, LX/3Af;

    invoke-direct {v1, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2545814
    new-instance v2, LX/7TY;

    invoke-direct {v2, v0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2545815
    invoke-direct {p0, v2, p1}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(LX/7TY;Ljava/lang/String;)V

    .line 2545816
    invoke-direct {p0, v2, p1, v0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(LX/7TY;Ljava/lang/String;Landroid/content/Context;)V

    .line 2545817
    iget-object v3, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2545818
    invoke-direct {p0, v2, p1, v0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->b(LX/7TY;Ljava/lang/String;Landroid/content/Context;)V

    .line 2545819
    :cond_0
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2545820
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2545821
    return-void
.end method

.method private b(LX/7TY;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2545810
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ef6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v1, 0x7f020888

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IAZ;

    invoke-direct {v1, p0, p2, p3}, LX/IAZ;-><init>(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2545811
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 4

    .prologue
    .line 2545795
    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->l:Z

    .line 2545796
    iput-object p1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    .line 2545797
    iput-object p2, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2545798
    iget-object v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    .line 2545799
    iget-object v1, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v1

    .line 2545800
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2545801
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->setText(Ljava/lang/CharSequence;)V

    .line 2545802
    return-void

    .line 2545803
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2545804
    :cond_1
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2545805
    goto :goto_1

    .line 2545806
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->b:LX/DB4;

    .line 2545807
    iget-object v2, p1, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v2, v2

    .line 2545808
    iget-object v3, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v3, v3

    .line 2545809
    invoke-virtual {v1, v0, v2, v3}, LX/DB4;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventActionStyle;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x7d794c4f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2545783
    iget-object v1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    .line 2545784
    iget-object p1, v1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v1, p1

    .line 2545785
    if-nez v1, :cond_1

    .line 2545786
    :cond_0
    const v1, -0xd9be7a7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2545787
    :goto_0
    return-void

    .line 2545788
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    .line 2545789
    iget-object v2, v1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v1, v2

    .line 2545790
    iget-object v2, v1, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2545791
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2545792
    const v1, -0x1e32ebdd

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2545793
    :cond_2
    invoke-direct {p0, v1}, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->a(Ljava/lang/String;)V

    .line 2545794
    const v1, -0x59a3606d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2545779
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2545780
    iget-boolean v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->l:Z

    if-eqz v0, :cond_0

    .line 2545781
    iget-object v0, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->j:LX/5O6;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->k:I

    invoke-virtual {v0, p1, v1, v2}, LX/5O6;->a(Landroid/graphics/Canvas;FI)V

    .line 2545782
    :cond_0
    return-void
.end method
