.class public Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/I6a;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition",
            "<",
            "LX/I6a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition",
            "<",
            "LX/I6a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547116
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2547117
    iput-object p6, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2547118
    iput-object p5, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2547119
    iput-object p4, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2547120
    iput-object p3, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 2547121
    iput-object p2, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;

    .line 2547122
    iput-object p1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->f:Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    .line 2547123
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;
    .locals 10

    .prologue
    .line 2547124
    const-class v1, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;

    monitor-enter v1

    .line 2547125
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2547126
    sput-object v2, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2547127
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2547128
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2547129
    new-instance v3, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;->a(LX/0QB;)Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;-><init>(Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;)V

    .line 2547130
    move-object v0, v3

    .line 2547131
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2547132
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547133
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2547134
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2547135
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2547136
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2547137
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->f:Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547138
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/components/HeaderNoMenuComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547139
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547140
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547141
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547142
    iget-object v1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2547143
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2547144
    const/4 v0, 0x1

    return v0
.end method
