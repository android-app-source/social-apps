.class public Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/2ee;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547113
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2547114
    iput-object p1, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    .line 2547115
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;
    .locals 4

    .prologue
    .line 2547102
    const-class v1, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    monitor-enter v1

    .line 2547103
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2547104
    sput-object v2, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2547105
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2547106
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2547107
    new-instance p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;)V

    .line 2547108
    move-object v0, p0

    .line 2547109
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2547110
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547111
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2547112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/2ee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2547101
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2547093
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 2547094
    iget-object v0, p0, Lcom/facebook/events/permalink/pinnedpost/PinnedPostExplanationPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;

    new-instance v1, LX/Brz;

    sget-object v2, LX/2ei;->STORY_HEADER:LX/2ei;

    invoke-direct {v1, p2, v2}, LX/Brz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2ei;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2547095
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Void;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2547097
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2547098
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2547099
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2547100
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 2547096
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    return-object v0
.end method
