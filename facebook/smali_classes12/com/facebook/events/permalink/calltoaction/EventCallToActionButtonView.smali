.class public Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2542110
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2542111
    invoke-direct {p0}, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a()V

    .line 2542112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2542113
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2542114
    invoke-direct {p0}, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a()V

    .line 2542115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2542116
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2542117
    invoke-direct {p0}, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a()V

    .line 2542118
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2542119
    const v0, 0x7f0304e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2542120
    const v0, 0x7f0d0e2e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a:Landroid/widget/TextView;

    .line 2542121
    return-void
.end method


# virtual methods
.method public getCallToActionButtonView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2542122
    iget-object v0, p0, Lcom/facebook/events/permalink/calltoaction/EventCallToActionButtonView;->a:Landroid/widget/TextView;

    return-object v0
.end method
