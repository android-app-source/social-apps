.class public Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/events/permalink/about/DetailsTextView;

.field private c:LX/5O6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2539956
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2539957
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a()V

    .line 2539958
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2539959
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2539960
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a()V

    .line 2539961
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2539945
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2539946
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a()V

    .line 2539947
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2539948
    const-class v0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2539949
    const v0, 0x7f0304ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2539950
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->c:LX/5O6;

    .line 2539951
    const v0, 0x7f0d0e34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/about/DetailsTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->b:Lcom/facebook/events/permalink/about/DetailsTextView;

    .line 2539952
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->b:Lcom/facebook/events/permalink/about/DetailsTextView;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->a()V

    .line 2539953
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->b:Lcom/facebook/events/permalink/about/DetailsTextView;

    new-instance v1, LX/I7c;

    invoke-direct {v1, p0}, LX/I7c;-><init>(Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;)V

    .line 2539954
    iput-object v1, v0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    .line 2539955
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a:LX/1Uf;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 4

    .prologue
    .line 2539938
    iget-object v0, p1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v0

    .line 2539939
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2539940
    iget-object v1, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->a:LX/1Uf;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    .line 2539941
    iget-object v1, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->b:Lcom/facebook/events/permalink/about/DetailsTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2539942
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->setVisibility(I)V

    .line 2539943
    :goto_0
    return-void

    .line 2539944
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2539935
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2539936
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsToxicleView;->c:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->b(Landroid/graphics/Canvas;)V

    .line 2539937
    return-void
.end method
