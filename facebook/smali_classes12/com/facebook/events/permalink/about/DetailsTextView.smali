.class public Lcom/facebook/events/permalink/about/DetailsTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:Z

.field private d:Z

.field public e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/graphics/Rect;

.field public l:LX/I7a;

.field private m:Landroid/util/SparseIntArray;

.field private n:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2539834
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2539835
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->a:F

    .line 2539836
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->b:F

    .line 2539837
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2539838
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2539839
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->a:F

    .line 2539840
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->b:F

    .line 2539841
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->b()V

    .line 2539842
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2539843
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2539844
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->a:F

    .line 2539845
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->b:F

    .line 2539846
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->b()V

    .line 2539847
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2539848
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2539849
    const v1, 0x7f0a0696

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->i:I

    .line 2539850
    const v1, 0x7f0b1448

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->h:I

    .line 2539851
    const/16 v0, 0xc

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    .line 2539852
    iget v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setMaxLines(I)V

    .line 2539853
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->k:Landroid/graphics/Rect;

    .line 2539854
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    .line 2539855
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    .line 2539856
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    .line 2539857
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setFocusable(Z)V

    .line 2539858
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2539859
    iget-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    if-nez v0, :cond_0

    .line 2539860
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->e()V

    .line 2539861
    :goto_0
    return-void

    .line 2539862
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->d()V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2539863
    iget-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->e:Z

    move v0, v0

    .line 2539864
    if-nez v0, :cond_0

    .line 2539865
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->k:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2539866
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    .line 2539867
    iget v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setMaxLines(I)V

    .line 2539868
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCollapsedStateHeight()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setFadingGradient(I)V

    .line 2539869
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    if-eqz v0, :cond_0

    .line 2539870
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    invoke-interface {v0}, LX/I7a;->b()V

    .line 2539871
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2539872
    iget v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->f:I

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    if-ge v0, v1, :cond_0

    .line 2539873
    iget v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->f:I

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    .line 2539874
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    .line 2539875
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setMaxLines(I)V

    .line 2539876
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->f()V

    .line 2539877
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    if-eqz v0, :cond_1

    .line 2539878
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    invoke-interface {v0}, LX/I7a;->a()V

    .line 2539879
    :cond_1
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2539880
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2539881
    return-void
.end method

.method private getCollapsedStateHeight()I
    .locals 2

    .prologue
    .line 2539882
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->h()V

    .line 2539883
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method private getExpandedStateHeight()I
    .locals 2

    .prologue
    .line 2539884
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->h()V

    .line 2539885
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method private h()V
    .locals 8

    .prologue
    .line 2539829
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 2539830
    :goto_0
    return-void

    .line 2539831
    :cond_0
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->a:F

    iget v6, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->b:F

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2539832
    iget-object v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 2539833
    iget-object v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    iget v3, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->g:I

    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCompoundPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method private setFadingGradient(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2539779
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->h:I

    sub-int v2, p1, v2

    int-to-float v2, v2

    int-to-float v4, p1

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getCurrentTextColor()I

    move-result v5

    iget v6, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->i:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 2539780
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2539781
    return-void
.end method

.method private setIsRigid(Z)V
    .locals 0

    .prologue
    .line 2539827
    iput-boolean p1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->e:Z

    .line 2539828
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2539824
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2539825
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setFocusable(Z)V

    .line 2539826
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2539820
    iget v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 2539821
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->j:I

    .line 2539822
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2539823
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2539808
    iget-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->d:Z

    if-eqz v0, :cond_1

    .line 2539809
    iget-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->f:I

    if-gt v0, v1, :cond_2

    .line 2539810
    iput-boolean v3, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    .line 2539811
    iput-boolean v3, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->e:Z

    .line 2539812
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->f()V

    .line 2539813
    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->d:Z

    .line 2539814
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 2539815
    return-void

    .line 2539816
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->f:I

    if-le v0, v1, :cond_0

    .line 2539817
    iput-boolean v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    .line 2539818
    iput-boolean v2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->e:Z

    .line 2539819
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getHeight()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setFadingGradient(I)V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2539799
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->d:Z

    .line 2539800
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2539801
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2539802
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_1

    .line 2539803
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 2539804
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_2

    .line 2539805
    iget-object v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 2539806
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 2539807
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const v0, 0x65486515

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2539791
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 2539792
    const v0, 0x611e085

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2539793
    :goto_0
    return v3

    .line 2539794
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_1

    .line 2539795
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v2, p0, v0, p1}, Landroid/text/method/MovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2539796
    const v0, -0x20a16482

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2539797
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/DetailsTextView;->c()V

    .line 2539798
    const v0, -0x150cc357

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 2539787
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setLineSpacing(FF)V

    .line 2539788
    iput p2, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->a:F

    .line 2539789
    iput p1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->b:F

    .line 2539790
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .prologue
    .line 2539784
    iput p1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->f:I

    .line 2539785
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 2539786
    return-void
.end method

.method public setOnExpandCollapseListener(LX/I7a;)V
    .locals 0

    .prologue
    .line 2539782
    iput-object p1, p0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    .line 2539783
    return-void
.end method
