.class public Lcom/facebook/events/permalink/about/EventArtistRowView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/events/model/EventArtist;

.field public i:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2539892
    const-class v0, Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/about/EventArtistRowView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2539893
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2539894
    const-class p1, Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-static {p1, p0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2539895
    const p1, 0x7f03049a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2539896
    const p1, 0x7f0d0340

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2539897
    const p1, 0x7f0d02c4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2539898
    const p1, 0x7f0d02c3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2539899
    new-instance p1, LX/I7b;

    invoke-direct {p1, p0}, LX/I7b;-><init>(Lcom/facebook/events/permalink/about/EventArtistRowView;)V

    iput-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->i:Landroid/view/View$OnClickListener;

    .line 2539900
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-static {v2}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    const/16 p0, 0xbc6

    invoke-static {v2, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/events/permalink/about/EventArtistRowView;->a:LX/154;

    iput-object p0, p1, Lcom/facebook/events/permalink/about/EventArtistRowView;->b:LX/0Or;

    iput-object v2, p1, Lcom/facebook/events/permalink/about/EventArtistRowView;->c:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/EventArtist;)V
    .locals 8

    .prologue
    .line 2539901
    iput-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->h:Lcom/facebook/events/model/EventArtist;

    .line 2539902
    iget-object v0, p1, Lcom/facebook/events/model/EventArtist;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2539903
    if-eqz v0, :cond_0

    .line 2539904
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2539905
    iget-object v1, p1, Lcom/facebook/events/model/EventArtist;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2539906
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/events/permalink/about/EventArtistRowView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2539907
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2539908
    iget-object v1, p1, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2539909
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2539910
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2539911
    const/4 v3, 0x0

    .line 2539912
    iget-object v1, p1, Lcom/facebook/events/model/EventArtist;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2539913
    if-eqz v1, :cond_4

    const-string v2, ", "

    invoke-static {v2}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, LX/2Cb;->limit(I)LX/2Cb;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    move-object v1, v1

    .line 2539914
    iget-boolean v2, p1, Lcom/facebook/events/model/EventArtist;->d:Z

    move v2, v2

    .line 2539915
    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f081edc

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 2539916
    iget-object v7, p1, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2539917
    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 2539918
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2539919
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081edb

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2539920
    :cond_1
    :goto_2
    move-object v1, v1

    .line 2539921
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2539922
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2539923
    return-void

    .line 2539924
    :cond_2
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2539925
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v2

    .line 2539926
    goto :goto_2

    :cond_3
    move-object v1, v3

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 2539927
    :cond_5
    iget v2, p1, Lcom/facebook/events/model/EventArtist;->e:I

    move v2, v2

    .line 2539928
    if-nez v2, :cond_6

    const/4 v4, 0x0

    :goto_3
    move-object v2, v4

    .line 2539929
    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00e4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object p1, p0, Lcom/facebook/events/permalink/about/EventArtistRowView;->a:LX/154;

    invoke-virtual {p1, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method
