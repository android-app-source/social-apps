.class public Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/events/permalink/about/DetailsTextView;

.field private e:Landroid/view/ViewGroup;

.field private f:Ljava/lang/String;

.field private g:LX/5O6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2540020
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2540021
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a()V

    .line 2540022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2540017
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2540018
    invoke-direct {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a()V

    .line 2540019
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2540015
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2540016
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2540001
    const-class v0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2540002
    const v0, 0x7f0304ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2540003
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->setOrientation(I)V

    .line 2540004
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2540005
    invoke-virtual {p0, v2, v0, v2, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->setPadding(IIII)V

    .line 2540006
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->g:LX/5O6;

    .line 2540007
    const v0, 0x7f0d0e36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2540008
    const v0, 0x7f0d0e35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->b:Landroid/view/View;

    .line 2540009
    const v0, 0x7f0d0e34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/about/DetailsTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->d:Lcom/facebook/events/permalink/about/DetailsTextView;

    .line 2540010
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->d:Lcom/facebook/events/permalink/about/DetailsTextView;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->a()V

    .line 2540011
    const v0, 0x7f0d0e37

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    .line 2540012
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->d:Lcom/facebook/events/permalink/about/DetailsTextView;

    new-instance v1, LX/I7d;

    invoke-direct {v1, p0}, LX/I7d;-><init>(Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;)V

    .line 2540013
    iput-object v1, v0, Lcom/facebook/events/permalink/about/DetailsTextView;->l:LX/I7a;

    .line 2540014
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a:LX/1Uf;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V
    .locals 10
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 2539967
    iget-object v0, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2539968
    iput-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->f:Ljava/lang/String;

    .line 2539969
    iget-object v0, p1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v0

    .line 2539970
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2539971
    iget-object v3, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->a:LX/1Uf;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v1, v4}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    .line 2539972
    iget-object v3, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->d:Lcom/facebook/events/permalink/about/DetailsTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2539973
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2539974
    :goto_0
    if-eqz p2, :cond_0

    .line 2539975
    invoke-static {p2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v4

    .line 2539976
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2539977
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2539978
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->d:Lcom/facebook/events/permalink/about/DetailsTextView;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/about/DetailsTextView;->getVisibility()I

    move-result v0

    if-ne v0, v9, :cond_4

    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v9, :cond_4

    .line 2539979
    invoke-virtual {p0, v9}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->setVisibility(I)V

    .line 2539980
    :goto_1
    return-void

    .line 2539981
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->b:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2539982
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2539983
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2539984
    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 2539985
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventArtist;

    .line 2539986
    if-eqz v1, :cond_3

    .line 2539987
    iget-object v1, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_5

    .line 2539988
    const v1, 0x7f03049c

    iget-object v7, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move v1, v2

    .line 2539989
    :goto_3
    new-instance v7, Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/facebook/events/permalink/about/EventArtistRowView;-><init>(Landroid/content/Context;)V

    .line 2539990
    invoke-virtual {v7, v0}, Lcom/facebook/events/permalink/about/EventArtistRowView;->a(Lcom/facebook/events/model/EventArtist;)V

    .line 2539991
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2539992
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2539993
    :cond_3
    const v7, 0x7f03049b

    iget-object v8, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_3

    .line 2539994
    :cond_4
    invoke-virtual {p0, v2}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_3
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2539997
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2539998
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->g:LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, p1, v1}, LX/5O6;->a(Landroid/graphics/Canvas;F)V

    .line 2539999
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->g:LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, p1, v1}, LX/5O6;->b(Landroid/graphics/Canvas;F)V

    .line 2540000
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2539995
    iget-object v0, p0, Lcom/facebook/events/permalink/about/EventPermalinkDetailsView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2539996
    return-void
.end method
