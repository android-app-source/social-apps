.class public Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field public a:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2545694
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2545695
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2545693
    const-string v0, "event_host_list"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2545690
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2545691
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;

    invoke-static {v0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object p1

    check-cast p1, LX/I76;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    iput-object p1, p0, Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;->a:LX/I76;

    iput-object v0, p0, Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;->b:LX/0zG;

    .line 2545692
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2545687
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2545688
    const-string v1, "EVENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2545689
    invoke-static {v0}, LX/1nQ;->a(Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xec28b70

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2545686
    const v1, 0x7f0304d5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x18018fc9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x58c7107e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2545683
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2545684
    iget-object v0, p0, Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;->b:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f081f78

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2545685
    const/16 v0, 0x2b

    const v2, 0x4beb3ed

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2545668
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2545669
    const v0, 0x7f0d0e0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2545670
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2545671
    const-string v2, "ARTISTS"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2545672
    const-string v3, "HOSTS"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2545673
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2545674
    new-instance v1, LX/IAQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v2}, LX/IAQ;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2545675
    new-instance v2, LX/IAO;

    invoke-direct {v2, p0, v1}, LX/IAO;-><init>(Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;LX/IAQ;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2545676
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2545677
    :goto_0
    return-void

    .line 2545678
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2545679
    new-instance v2, LX/IAR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v1}, LX/IAR;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2545680
    new-instance v1, LX/IAP;

    invoke-direct {v1, p0, v2}, LX/IAP;-><init>(Lcom/facebook/events/permalink/hostsinfo/EventHostsFragment;LX/IAR;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2545681
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 2545682
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not be starting hosts when no artist or hosts are provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
