.class public Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2545703
    const-class v0, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545704
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2545705
    const p1, 0x7f03089b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2545706
    const p1, 0x7f0207fa

    invoke-virtual {p0, p1}, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->setBackgroundResource(I)V

    .line 2545707
    const p1, 0x7f0d1641

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2545708
    const p1, 0x7f0d1642

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->c:Landroid/widget/TextView;

    .line 2545709
    return-void
.end method
