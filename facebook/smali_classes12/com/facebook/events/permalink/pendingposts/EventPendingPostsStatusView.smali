.class public Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Landroid/content/res/Resources;

.field public h:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2546978
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2546979
    invoke-direct {p0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a()V

    .line 2546980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2546975
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2546976
    invoke-direct {p0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a()V

    .line 2546977
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2546951
    const-class v0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2546952
    const v0, 0x7f0304df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2546953
    invoke-virtual {p0, v3}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->setOrientation(I)V

    .line 2546954
    invoke-virtual {p0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->g:Landroid/content/res/Resources;

    .line 2546955
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->g:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2546956
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2546957
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2546958
    iget-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->g:Landroid/content/res/Resources;

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2546959
    const v0, 0x7f0d0e29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2546960
    const v0, 0x7f0d0e2a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2546961
    const v0, 0x7f0d0e2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2546962
    iget-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/IBM;

    invoke-direct {v1, p0}, LX/IBM;-><init>(Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2546963
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2546974
    iput-object p1, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->c:LX/17Y;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    const/16 v0, 0x15e7

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a(Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V
    .locals 8

    .prologue
    .line 2546964
    iput-object p1, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->h:Lcom/facebook/events/model/Event;

    .line 2546965
    iget-object v0, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/events/model/Event;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2546966
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aW()I

    move-result v2

    .line 2546967
    iget-object v3, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->g:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    const v0, 0x7f0f00e2

    :goto_0
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2546968
    iget-object v2, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->e:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v1, :cond_1

    const v0, 0x7f081eda

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2546969
    iget-object v2, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->g:Landroid/content/res/Resources;

    if-eqz v1, :cond_2

    const v0, 0x7f081ed9

    :goto_2
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2546970
    return-void

    .line 2546971
    :cond_0
    const v0, 0x7f0f00e3

    goto :goto_0

    .line 2546972
    :cond_1
    const v0, 0x7f081ede

    goto :goto_1

    .line 2546973
    :cond_2
    const v0, 0x7f081edd

    goto :goto_2
.end method
