.class public Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/events/model/Event;

.field public m:Ljava/lang/String;

.field public n:LX/DBC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545853
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2545854
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->d()V

    .line 2545855
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2545870
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2545871
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->d()V

    .line 2545872
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2545873
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545874
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->d()V

    .line 2545875
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;Ljava/lang/Boolean;LX/0wM;)V
    .locals 0

    .prologue
    .line 2545869
    iput-object p1, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->j:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->k:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->a(Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;Ljava/lang/Boolean;LX/0wM;)V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2545860
    const-class v0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2545861
    const v0, 0x7f0304ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2545862
    const v0, 0x7f0d0e33

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2545863
    new-instance v1, LX/IAa;

    invoke-direct {v1, p0}, LX/IAa;-><init>(Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2545864
    const v0, 0x7f0d0e32

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2545865
    invoke-virtual {p0}, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f081ed4

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2545866
    iget-object v0, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->k:LX/0wM;

    const v1, 0x7f020886

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2545867
    return-void

    .line 2545868
    :cond_0
    const v1, 0x7f081ed3

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Ljava/lang/String;LX/DBC;)V
    .locals 0

    .prologue
    .line 2545856
    iput-object p1, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->l:Lcom/facebook/events/model/Event;

    .line 2545857
    iput-object p2, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->m:Ljava/lang/String;

    .line 2545858
    iput-object p3, p0, Lcom/facebook/events/permalink/invitefriends/CopyEventInvitesView;->n:LX/DBC;

    .line 2545859
    return-void
.end method
