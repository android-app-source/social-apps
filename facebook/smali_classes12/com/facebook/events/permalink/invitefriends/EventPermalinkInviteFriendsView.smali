.class public Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/events/model/Event;

.field private e:LX/DBC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545876
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2545877
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a()V

    .line 2545878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2545900
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2545901
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a()V

    .line 2545902
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2545897
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2545898
    invoke-direct {p0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a()V

    .line 2545899
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2545886
    const-class v0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;

    invoke-static {v0, p0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2545887
    const v0, 0x7f0304fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2545888
    const v0, 0x7f0d0e3f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2545889
    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->b:LX/0ad;

    sget-short v2, LX/347;->N:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2545890
    if-eqz v1, :cond_0

    .line 2545891
    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->c:LX/0wM;

    const v2, 0x7f020887

    const v3, -0xa76f01

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2545892
    invoke-virtual {v0, v1, v4, v4, v4}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2545893
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2545894
    const v1, 0x7f081f79

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2545895
    :cond_1
    invoke-virtual {p0, p0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2545896
    return-void
.end method

.method private static a(Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;Ljava/lang/Boolean;LX/0ad;LX/0wM;)V
    .locals 0

    .prologue
    .line 2545885
    iput-object p1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->b:LX/0ad;

    iput-object p3, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->c:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;

    invoke-static {v2}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v2

    check-cast v2, LX/0wM;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->a(Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;Ljava/lang/Boolean;LX/0ad;LX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;LX/DBC;)V
    .locals 0

    .prologue
    .line 2545882
    iput-object p1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->d:Lcom/facebook/events/model/Event;

    .line 2545883
    iput-object p2, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->e:LX/DBC;

    .line 2545884
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x537a94d4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2545879
    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->e:LX/DBC;

    if-eqz v1, :cond_0

    .line 2545880
    iget-object v1, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->e:LX/DBC;

    iget-object v2, p0, Lcom/facebook/events/permalink/invitefriends/EventPermalinkInviteFriendsView;->d:Lcom/facebook/events/model/Event;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/DBC;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)V

    .line 2545881
    :cond_0
    const v1, -0x4a767e89

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
