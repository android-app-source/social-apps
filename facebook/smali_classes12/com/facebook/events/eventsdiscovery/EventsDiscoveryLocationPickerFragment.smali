.class public Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/I5E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FOx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

.field public h:LX/H4G;

.field public i:Landroid/widget/ProgressBar;

.field public j:Landroid/location/Location;

.field public k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

.field public l:Ljava/lang/String;

.field private final n:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/6oO;

.field private final p:Landroid/widget/AdapterView$OnItemClickListener;

.field private final q:LX/I57;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2535104
    const-class v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2535174
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2535175
    new-instance v0, LX/I54;

    invoke-direct {v0, p0}, LX/I54;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->n:LX/0Vd;

    .line 2535176
    new-instance v0, LX/I55;

    invoke-direct {v0, p0}, LX/I55;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->o:LX/6oO;

    .line 2535177
    new-instance v0, LX/I56;

    invoke-direct {v0, p0}, LX/I56;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2535178
    new-instance v0, LX/I57;

    invoke-direct {v0, p0}, LX/I57;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->q:LX/I57;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;LX/H4F;)V
    .locals 3

    .prologue
    .line 2535166
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2535167
    const-string v1, "extra_location_data"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2535168
    const-string v1, "extra_location_range"

    invoke-virtual {p2}, LX/H4F;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2535169
    invoke-static {p0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->e(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    .line 2535170
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2535171
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2535172
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2535173
    :cond_0
    return-void
.end method

.method public static c$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V
    .locals 3

    .prologue
    .line 2535159
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b()V

    .line 2535160
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    const v1, 0x6c5f0a1

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2535161
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2535162
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v1

    .line 2535163
    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    move-object v0, v0

    .line 2535164
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a:LX/I5E;

    new-instance v2, LX/I5A;

    invoke-direct {v2, p0}, LX/I5A;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    invoke-virtual {v1, v0, v2}, LX/I5E;->a(Ljava/lang/String;LX/0TF;)V

    .line 2535165
    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->g:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V
    .locals 3

    .prologue
    .line 2535156
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2535157
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->g:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2535158
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2535139
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2535140
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-static {v0}, LX/I5E;->b(LX/0QB;)LX/I5E;

    move-result-object v3

    check-cast v3, LX/I5E;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v5

    check-cast v5, LX/0zG;

    invoke-static {v0}, LX/FOx;->a(LX/0QB;)LX/FOx;

    move-result-object p1

    check-cast p1, LX/FOx;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v3, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a:LX/I5E;

    iput-object v4, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->b:LX/1nQ;

    iput-object v5, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->c:LX/0zG;

    iput-object p1, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->d:LX/FOx;

    iput-object v0, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->e:LX/1Ck;

    .line 2535141
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535142
    const-string v1, "extra_events_discovery_suggestion_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->l:Ljava/lang/String;

    .line 2535143
    new-instance v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    new-instance v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->d:LX/FOx;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/FOx;->a(Landroid/app/Activity;)LX/FOw;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;-><init>(LX/FOw;)V

    invoke-direct {v0, v1}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;-><init>(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2535144
    new-instance v0, LX/H4G;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LX/H4G;-><init>(Landroid/content/Context;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;Z)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    .line 2535145
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->q:LX/I57;

    .line 2535146
    iput-object v1, v0, LX/H4G;->f:LX/I57;

    .line 2535147
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535148
    const-string v1, "extra_is_current_location_selected"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2535149
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2535150
    const-string v2, "extra_location_range"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2535151
    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v2

    aget-object v1, v2, v1

    .line 2535152
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    .line 2535153
    iput-boolean v0, v2, LX/H4G;->b:Z

    .line 2535154
    iput-object v1, v2, LX/H4G;->d:LX/H4F;

    .line 2535155
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x56936bcd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2535138
    const v1, 0x7f03056f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2c3938be

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1cc9859a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2535133
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a:LX/I5E;

    .line 2535134
    iget-object v2, v1, LX/I5E;->f:LX/H4I;

    invoke-virtual {v2}, LX/H4I;->a()V

    .line 2535135
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2535136
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2535137
    const/16 v1, 0x2b

    const v2, -0x4b28cf20

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a541808    # 3474946.0f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2535112
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2535113
    const/4 v6, 0x0

    .line 2535114
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->c:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2535115
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030570

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2535116
    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setCustomTitleView(Landroid/view/View;)V

    .line 2535117
    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2535118
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f020818

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2535119
    iput-object v4, v3, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2535120
    move-object v3, v3

    .line 2535121
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0837f9

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2535122
    iput-object v4, v3, LX/108;->j:Ljava/lang/String;

    .line 2535123
    move-object v3, v3

    .line 2535124
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2535125
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2535126
    const v3, 0x7f0d0f1c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iput-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->g:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    .line 2535127
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->g:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->o:LX/6oO;

    invoke-virtual {v2, v3}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setInputTextListener(Landroid/text/TextWatcher;)V

    .line 2535128
    new-instance v2, LX/I58;

    invoke-direct {v2, p0}, LX/I58;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2535129
    new-instance v2, LX/I59;

    invoke-direct {v2, p0}, LX/I59;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnBackPressedListener(LX/63J;)V

    .line 2535130
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a:LX/I5E;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->n:LX/0Vd;

    sget-object v4, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3, v4}, LX/I5E;->a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2535131
    invoke-static {p0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->c$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V

    .line 2535132
    const/16 v1, 0x2b

    const v2, 0x30eaf914

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2535105
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2535106
    const v0, 0x7f0d0f1a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2535107
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2535108
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2535109
    const v0, 0x7f0d0f1b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->i:Landroid/widget/ProgressBar;

    .line 2535110
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->i:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2535111
    return-void
.end method
