.class public Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""


# instance fields
.field public i:LX/I5M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:LX/I5L;

.field private final k:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public l:LX/2jY;

.field public m:LX/E8m;

.field public n:LX/0m9;

.field public o:Ljava/lang/String;

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

.field public u:I

.field public v:Z

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2535319
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    .line 2535320
    new-instance v0, LX/I5L;

    invoke-direct {v0, p0}, LX/I5L;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->j:LX/I5L;

    .line 2535321
    const-string v0, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->k:Ljava/lang/String;

    .line 2535322
    return-void
.end method

.method public static Q(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;)V
    .locals 1

    .prologue
    .line 2535398
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->j()V

    .line 2535399
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    invoke-virtual {v0}, LX/E8m;->k()V

    .line 2535400
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f_(Z)V

    .line 2535401
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;I)V

    .line 2535402
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;ILjava/util/List;)LX/0m9;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0m9;"
        }
    .end annotation

    .prologue
    .line 2535374
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2535375
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2535376
    const-string v0, "suggestion_token"

    invoke-virtual {v1, v0, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535377
    :cond_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2535378
    const-string v0, "time"

    invoke-virtual {v1, v0, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535379
    :cond_1
    if-eqz p2, :cond_2

    .line 2535380
    iget-boolean v0, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v0

    .line 2535381
    if-nez v0, :cond_3

    .line 2535382
    const-string v0, "city"

    .line 2535383
    iget-object v2, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2535384
    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535385
    :cond_2
    :goto_0
    if-eqz p4, :cond_5

    .line 2535386
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2535387
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2535388
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 2535389
    :cond_3
    iget-object v0, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v0, v0

    .line 2535390
    if-eqz v0, :cond_2

    .line 2535391
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2535392
    const-string v3, "latitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2535393
    const-string v3, "longitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2535394
    const-string v0, "lat_lon"

    invoke-virtual {v1, v0, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2535395
    const-string v0, "range"

    invoke-virtual {v1, v0, p3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_0

    .line 2535396
    :cond_4
    const-string v0, "event_categories"

    invoke-virtual {v1, v0, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2535397
    :cond_5
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-static {p0}, LX/I5M;->b(LX/0QB;)LX/I5M;

    move-result-object p0

    check-cast p0, LX/I5M;

    iput-object p0, p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;I)V
    .locals 1

    .prologue
    .line 2535369
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2535370
    if-eqz v0, :cond_0

    .line 2535371
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2535372
    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2535373
    :cond_0
    return-void
.end method


# virtual methods
.method public final N()V
    .locals 14

    .prologue
    .line 2535363
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->j:LX/I5L;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->q:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->l:LX/2jY;

    .line 2535364
    iget-boolean v6, v5, LX/2jY;->o:Z

    move v5, v6

    .line 2535365
    iget-object v6, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->o:Ljava/lang/String;

    .line 2535366
    if-nez v5, :cond_0

    .line 2535367
    :goto_0
    return-void

    .line 2535368
    :cond_0
    iget-object v7, v0, LX/I5M;->b:Lcom/facebook/reaction/ReactionUtil;

    const/4 v9, 0x5

    move-object v8, v2

    move-object v10, v6

    move-object v11, v1

    move-object v12, v3

    move-object v13, v4

    invoke-virtual/range {v7 .. v13}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0Ve;ILjava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final P()V
    .locals 4

    .prologue
    .line 2535403
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535404
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->I()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2ja;->d(J)V

    .line 2535405
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v0

    invoke-virtual {v0}, LX/2ja;->e()V

    .line 2535406
    :cond_0
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2535362
    const-string v0, "event_discovery"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2535340
    const-class v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2535341
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535342
    const-string v1, "extra_events_discovery_suggestion_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->r:Ljava/lang/String;

    .line 2535343
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535344
    const-string v1, "extra_events_discovery_filter_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->s:Ljava/lang/String;

    .line 2535345
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535346
    const-string v1, "extra_events_discovery_filter_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->t:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2535347
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535348
    const-string v1, "extra_events_discovery_filter_location_range"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->u:I

    .line 2535349
    if-eqz p1, :cond_0

    .line 2535350
    const-string v0, "extra_events_discovery_fragment_waiting_for_location_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->v:Z

    .line 2535351
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535352
    const-string v1, "extra_events_discovery_fragment_selected_category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->w:Ljava/util/List;

    .line 2535353
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->s:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->t:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->u:I

    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->w:Ljava/util/List;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;ILjava/util/List;)LX/0m9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    .line 2535354
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535355
    const-string v1, "extra_events_discovery_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->p:Ljava/lang/String;

    .line 2535356
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2535357
    const-string v1, "extra_fragment_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->q:Ljava/lang/String;

    .line 2535358
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2535359
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2535360
    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->l:LX/2jY;

    .line 2535361
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2535337
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2535338
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->p:Ljava/lang/String;

    invoke-super {p0, v0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->c(Ljava/lang/String;)V

    .line 2535339
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x25629bbd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2535332
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v1, v1

    .line 2535333
    invoke-virtual {p0, p2, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/view/ViewGroup;LX/2jY;)Landroid/view/View;

    move-result-object v1

    .line 2535334
    iget-object v2, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v2, v2

    .line 2535335
    iput-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    .line 2535336
    const/16 v2, 0x2b

    const v3, -0x59861ed3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2535329
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2535330
    const-string v0, "extra_events_discovery_fragment_waiting_for_location_result"

    iget-boolean v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2535331
    return-void
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2535323
    iget-boolean v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->v:Z

    if-nez v0, :cond_0

    .line 2535324
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->j:LX/I5L;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->q:Ljava/lang/String;

    .line 2535325
    invoke-virtual {v0, v1}, LX/I5M;->a(Ljava/lang/String;)LX/2jY;

    move-result-object p0

    .line 2535326
    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2535327
    move-object v0, p0

    .line 2535328
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    invoke-virtual {v0, v1}, LX/I5M;->a(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    goto :goto_0
.end method
