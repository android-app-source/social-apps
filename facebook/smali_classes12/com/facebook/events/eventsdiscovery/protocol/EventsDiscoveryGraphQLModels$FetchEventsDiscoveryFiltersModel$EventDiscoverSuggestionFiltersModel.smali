.class public final Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1ce3be76
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2535690
    const-class v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2535693
    const-class v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2535691
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2535692
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2535680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2535681
    invoke-virtual {p0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2535682
    invoke-virtual {p0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2535683
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2535684
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2535685
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2535686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2535687
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2535688
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->e:Ljava/util/List;

    .line 2535689
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2535672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2535673
    invoke-virtual {p0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2535674
    invoke-virtual {p0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2535675
    if-eqz v1, :cond_0

    .line 2535676
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;

    .line 2535677
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->e:Ljava/util/List;

    .line 2535678
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2535679
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2535669
    new-instance v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;

    invoke-direct {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;-><init>()V

    .line 2535670
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2535671
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2535668
    const v0, 0x5a65265d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2535667
    const v0, -0x734aeec1

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2535665
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->f:Ljava/lang/String;

    .line 2535666
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->f:Ljava/lang/String;

    return-object v0
.end method
