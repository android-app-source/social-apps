.class public final Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2535430
    const-class v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;

    new-instance v1, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2535431
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2535432
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2535433
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2535434
    const/4 v2, 0x0

    .line 2535435
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2535436
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2535437
    :goto_0
    move v1, v2

    .line 2535438
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2535439
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2535440
    new-instance v1, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;

    invoke-direct {v1}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;-><init>()V

    .line 2535441
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2535442
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2535443
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2535444
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2535445
    :cond_0
    return-object v1

    .line 2535446
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2535447
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_4

    .line 2535448
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2535449
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2535450
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 2535451
    const-string p0, "event_category_list"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2535452
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2535453
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, p0, :cond_3

    .line 2535454
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, p0, :cond_3

    .line 2535455
    invoke-static {p1, v0}, LX/I5S;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2535456
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2535457
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2535458
    goto :goto_1

    .line 2535459
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2535460
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2535461
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method
