.class public Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:I

.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field private D:Z

.field private E:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;",
            ">;"
        }
    .end annotation
.end field

.field public G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;"
        }
    .end annotation
.end field

.field public H:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public I:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public J:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field private M:Ljava/util/Calendar;

.field private N:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I5J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/I5E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:LX/I4u;

.field private final m:LX/I4v;

.field private final n:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/6Uh;

.field public final p:LX/I4y;

.field public q:LX/I5K;

.field public r:Landroid/support/v4/view/ViewPager;

.field public s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public x:Lcom/facebook/resources/ui/FbTextView;

.field public y:Lcom/facebook/resources/ui/FbTextView;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2534974
    sget-object v0, LX/H4F;->SEARCH_RADIUS_5:LX/H4F;

    invoke-virtual {v0}, LX/H4F;->ordinal()I

    move-result v0

    sput v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a:I

    .line 2534975
    const-class v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    sput-object v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->j:Ljava/lang/Class;

    .line 2534976
    const-class v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2534962
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2534963
    new-instance v0, LX/I4u;

    invoke-direct {v0, p0}, LX/I4u;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->l:LX/I4u;

    .line 2534964
    new-instance v0, LX/I4v;

    invoke-direct {v0, p0}, LX/I4v;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->m:LX/I4v;

    .line 2534965
    new-instance v0, LX/I4w;

    invoke-direct {v0, p0}, LX/I4w;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->n:LX/0TF;

    .line 2534966
    new-instance v0, LX/I4x;

    invoke-direct {v0, p0}, LX/I4x;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->o:LX/6Uh;

    .line 2534967
    new-instance v0, LX/I4y;

    invoke-direct {v0, p0}, LX/I4y;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->p:LX/I4y;

    .line 2534968
    sget v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a:I

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    .line 2534969
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    .line 2534970
    iget v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->C:I

    .line 2534971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    .line 2534972
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->G:Ljava/util/List;

    .line 2534973
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->H:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2534958
    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, LX/H4F;->getValue()I

    move-result v0

    .line 2534959
    const v1, 0x7f0f0179

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2534960
    const v1, 0x7f0837ec

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2534961
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u2022 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 9
    .param p2    # Ljava/util/Calendar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2534948
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 2534949
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2534950
    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11S;

    sget-object v6, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    invoke-interface {v4, v6, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2534951
    const-wide/16 v6, -0x1

    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    .line 2534952
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2534953
    :goto_1
    move-object v0, v4

    .line 2534954
    return-object v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2534955
    :cond_1
    const-string v4, " - "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2534956
    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11S;

    sget-object v6, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    invoke-interface {v4, v6, v0, v1}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2534957
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;Z)V
    .locals 4

    .prologue
    .line 2534921
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I5K;->a(Ljava/util/List;)V

    .line 2534922
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2534923
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    .line 2534924
    if-eqz p1, :cond_1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2534925
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2534926
    return-void

    .line 2534927
    :catch_0
    move-exception v0

    .line 2534928
    sget-object v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->j:Ljava/lang/Class;

    const-string v2, "Error parse suggestionToken"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2534929
    :cond_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2534930
    const-string v3, "time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2534931
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2534932
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2534933
    :goto_1
    goto :goto_0

    .line 2534934
    :cond_2
    const/4 v3, 0x0

    move v0, v3

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 2534935
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    .line 2534936
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;->j()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-virtual {v3}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2534937
    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    goto :goto_1

    .line 2534938
    :cond_3
    add-int/lit8 v3, v0, 0x1

    move v0, v3

    goto :goto_2

    .line 2534939
    :cond_4
    new-instance v3, LX/I5Q;

    invoke-direct {v3}, LX/I5Q;-><init>()V

    .line 2534940
    iput-object v2, v3, LX/I5Q;->a:Ljava/lang/String;

    .line 2534941
    move-object v3, v3

    .line 2534942
    invoke-virtual {v3}, LX/I5Q;->a()Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    move-result-object v3

    .line 2534943
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    .line 2534944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->L:Z

    .line 2534945
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    iget-object p1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v0, p1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2534946
    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-virtual {v3, v0}, LX/I5K;->a(Ljava/util/List;)V

    .line 2534947
    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    goto :goto_1
.end method

.method private static b(Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/util/Calendar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2534908
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 2534909
    :goto_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2534910
    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2534911
    const-wide/16 v5, -0x1

    cmp-long v5, v0, v5

    if-nez v5, :cond_1

    .line 2534912
    :goto_1
    move-object v0, v3

    .line 2534913
    return-object v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2534914
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2534915
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2534916
    :try_start_0
    const-string v6, "start"

    invoke-virtual {v5, v6, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2534917
    const-string v3, "end"

    invoke-virtual {v5, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2534918
    :goto_2
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2534919
    :catch_0
    move-exception v3

    .line 2534920
    sget-object v4, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->j:Ljava/lang/Class;

    const-string v6, "Error build time range"

    invoke-static {v4, v6, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static d$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2534977
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 2534978
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->N:Ljava/util/Calendar;

    invoke-static {v0, v1, v2, v4}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;Z)Landroid/content/Intent;

    move-result-object v1

    .line 2534979
    const-string v0, "extra_calendar_picker_title"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0837ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2534980
    const-string v0, "extra_enable_time_picker"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2534981
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->g:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x66

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2534982
    return-void
.end method

.method public static m(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 4

    .prologue
    .line 2534886
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-eqz v0, :cond_3

    .line 2534887
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534888
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v1

    .line 2534889
    if-eqz v0, :cond_2

    .line 2534890
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    invoke-static {v0, v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2534891
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 2534892
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-static {v0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2534893
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->H:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2534894
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534895
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-static {v0, v2}, LX/I4m;->a(Landroid/content/Context;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v0

    .line 2534896
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2534897
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u2022 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2534898
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534899
    :cond_1
    return-void

    .line 2534900
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534901
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2534902
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2534903
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534904
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2534905
    move-object v1, v0

    goto :goto_0

    .line 2534906
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->u:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 2534907
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0837ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2534837
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2534838
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    const/16 v3, 0x2e4

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xc

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    new-instance v7, LX/I5J;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct {v7, v5, v6}, LX/I5J;-><init>(LX/0tX;LX/1Ck;)V

    move-object v5, v7

    check-cast v5, LX/I5J;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v7

    check-cast v7, LX/0zG;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/I5E;->b(LX/0QB;)LX/I5E;

    move-result-object v0

    check-cast v0, LX/I5E;

    iput-object v3, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->b:LX/0Or;

    iput-object v4, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->c:LX/0Or;

    iput-object v5, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->d:LX/I5J;

    iput-object v6, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->e:LX/1nQ;

    iput-object v7, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->f:LX/0zG;

    iput-object v8, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->h:LX/0Uh;

    iput-object v0, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->i:LX/I5E;

    .line 2534839
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534840
    const-string v1, "extra_events_discovery_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2534841
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0837ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->t:Ljava/lang/String;

    .line 2534842
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534843
    const-string v1, "extra_events_discovery_single_tab_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->v:Ljava/lang/String;

    .line 2534844
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534845
    const-string v1, "extra_events_discovery_subtitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2534846
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0837ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->u:Ljava/lang/String;

    .line 2534847
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534848
    const-string v1, "extra_events_discovery_suggestion_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2534849
    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534850
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534851
    const-string v1, "extra_reaction_analytics_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->E:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2534852
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2534853
    const-string v1, "extra_need_fetch_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2534854
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->i:LX/I5E;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, LX/I4z;

    invoke-direct {v2, p0}, LX/I4z;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    sget-object v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/I5E;->a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2534855
    :cond_3
    new-instance v0, LX/I5K;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->E:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    invoke-direct {v0, v1, v2, v3}, LX/I5K;-><init>(LX/0gc;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    .line 2534856
    if-eqz p1, :cond_4

    .line 2534857
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->D:Z

    .line 2534858
    const-string v0, "extra_events_discovery_fragment_current_tab_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    .line 2534859
    const-string v0, "extra_events_discovery_fragment_previous_tab_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->C:I

    .line 2534860
    const-string v0, "extra_events_discovery_fragment_subtitle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->u:Ljava/lang/String;

    .line 2534861
    const-string v0, "extra_events_discovery_fragment_range_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    .line 2534862
    const-string v0, "extra_events_discovery_fragment_location_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534863
    const-string v0, "extra_events_discovery_fragment_category_filters"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->G:Ljava/util/List;

    .line 2534864
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "extra_events_discovery_fragment_selected_category"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    .line 2534865
    const-string v0, "extra_events_discovery_fragment_location_filters"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->J:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2534866
    :goto_0
    return-void

    .line 2534867
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->d:LX/I5J;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->m:LX/I4v;

    .line 2534868
    new-instance v2, LX/I5H;

    invoke-direct {v2, v0}, LX/I5H;-><init>(LX/I5J;)V

    .line 2534869
    new-instance v3, LX/I5I;

    invoke-direct {v3, v0, v1}, LX/I5I;-><init>(LX/I5J;LX/I4v;)V

    .line 2534870
    iget-object v4, v0, LX/I5J;->b:LX/1Ck;

    const-string v5, "fetchEventsDiscoveryCategoryFilters"

    invoke-virtual {v4, v5, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2534871
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->i:LX/I5E;

    const-string v1, ""

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->n:LX/0TF;

    invoke-virtual {v0, v1, v2}, LX/I5E;->a(Ljava/lang/String;LX/0TF;)V

    .line 2534872
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    .line 2534873
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534874
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2534875
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2534876
    const-string v2, "event_categories"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2534877
    const-string v2, "event_categories"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    .line 2534878
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_5

    .line 2534879
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2534880
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2534881
    :cond_5
    move-object v0, v3

    .line 2534882
    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2534883
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    goto :goto_0

    .line 2534884
    :catch_0
    move-exception v0

    .line 2534885
    sget-object v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->j:Ljava/lang/Class;

    const-string v2, "error parse categories"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v4, 0x66

    const/4 v3, -0x1

    .line 2534810
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->h:LX/0Uh;

    const/16 v1, 0x3a2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x65

    if-ne p1, v0, :cond_2

    .line 2534811
    if-eq p2, v3, :cond_1

    iget-boolean v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->D:Z

    if-eqz v0, :cond_1

    .line 2534812
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v2

    iget v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/H4F;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/I5K;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;I)V

    .line 2534813
    :cond_0
    :goto_0
    return-void

    .line 2534814
    :cond_1
    if-ne p2, v3, :cond_0

    .line 2534815
    const-string v0, "extra_location_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534816
    const-string v0, "extra_location_range"

    sget v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a:I

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    .line 2534817
    invoke-static {p0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->m(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    .line 2534818
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {}, LX/H4F;->values()[LX/H4F;

    move-result-object v2

    iget v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/H4F;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/I5K;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;I)V

    goto :goto_0

    .line 2534819
    :cond_2
    if-ne p2, v3, :cond_3

    if-ne p1, v4, :cond_3

    .line 2534820
    const-string v0, "extra_start_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    .line 2534821
    const-string v0, "extra_end_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->N:Ljava/util/Calendar;

    .line 2534822
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->N:Ljava/util/Calendar;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 2534823
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->M:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->N:Ljava/util/Calendar;

    invoke-static {v1, v2}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    .line 2534824
    new-instance v2, LX/I5Q;

    invoke-direct {v2}, LX/I5Q;-><init>()V

    .line 2534825
    iput-object v0, v2, LX/I5Q;->a:Ljava/lang/String;

    .line 2534826
    move-object v0, v2

    .line 2534827
    iput-object v1, v0, LX/I5Q;->b:Ljava/lang/String;

    .line 2534828
    move-object v0, v0

    .line 2534829
    invoke-virtual {v0}, LX/I5Q;->a()Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    move-result-object v0

    .line 2534830
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2534831
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I5K;->a(Ljava/util/List;)V

    .line 2534832
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    goto :goto_0

    .line 2534833
    :cond_3
    if-nez p2, :cond_0

    if-ne p1, v4, :cond_0

    .line 2534834
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->C:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2534835
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2534836
    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x60a4dcdd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2534809
    const v1, 0x7f03056e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x449ba8fe

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7144c3ee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2534804
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    invoke-virtual {v0, v2}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2534805
    if-eqz v0, :cond_0

    .line 2534806
    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->P()V

    .line 2534807
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2534808
    const/16 v0, 0x2b

    const v2, -0x171378ce

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2534793
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2534794
    const-string v0, "extra_events_discovery_fragment_current_tab_position"

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->B:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2534795
    const-string v0, "extra_events_discovery_fragment_previous_tab_position"

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->C:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2534796
    const-string v0, "extra_events_discovery_fragment_subtitle"

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2534797
    const-string v0, "extra_events_discovery_fragment_range_index"

    iget v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2534798
    const-string v0, "extra_events_discovery_fragment_location_data"

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2534799
    const-string v0, "extra_events_discovery_fragment_time_filters"

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2534800
    const-string v0, "extra_events_discovery_fragment_category_filters"

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->G:Ljava/util/List;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2534801
    const-string v0, "extra_events_discovery_fragment_selected_category"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2534802
    const-string v0, "extra_events_discovery_fragment_location_filters"

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->J:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2534803
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2534742
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2534743
    const v0, 0x7f0d0f19

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    .line 2534744
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2534745
    const v0, 0x7f0d0f18

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2534746
    const/4 v3, 0x0

    .line 2534747
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->f:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2534748
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030571

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2534749
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setCustomTitleView(Landroid/view/View;)V

    .line 2534750
    const v0, 0x7f0d0f1d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 2534751
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->y:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534752
    const v0, 0x7f0d0f1e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    .line 2534753
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->x:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534754
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2534755
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2534756
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->h:LX/0Uh;

    const/16 v3, 0x3a2

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2534757
    const v2, 0x7f0209d1

    .line 2534758
    :goto_0
    move v2, v2

    .line 2534759
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2534760
    iput-object v1, v0, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2534761
    move-object v0, v0

    .line 2534762
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->h:LX/0Uh;

    const/16 v2, 0x3a2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2534763
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2534764
    :goto_1
    move-object v1, v1

    .line 2534765
    iput-object v1, v0, LX/108;->j:Ljava/lang/String;

    .line 2534766
    move-object v0, v0

    .line 2534767
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2534768
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2534769
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/I51;

    invoke-direct {v1, p0}, LX/I51;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2534770
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->v:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2534771
    if-eqz p2, :cond_1

    .line 2534772
    const-string v0, "extra_events_discovery_fragment_time_filters"

    invoke-static {p2, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    .line 2534773
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;Z)V

    .line 2534774
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/I50;

    invoke-direct {v1, p0}, LX/I50;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    .line 2534775
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2534776
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->o:LX/6Uh;

    .line 2534777
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 2534778
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->e:LX/1nQ;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->E:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->E:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534779
    iget-object p0, v0, LX/1nQ;->i:LX/0Zb;

    const-string p1, "event_discovery_open_filter_surface"

    const/4 p2, 0x0

    invoke-interface {p0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2534780
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2534781
    const-string p1, "event_discovery"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "ref_module"

    invoke-virtual {p0, p1, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "ref_mechanism"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "event_suggestion_token"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2534782
    :cond_0
    return-void

    .line 2534783
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->d:LX/I5J;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->l:LX/I4u;

    .line 2534784
    new-instance v2, LX/I5F;

    invoke-direct {v2, v0}, LX/I5F;-><init>(LX/I5J;)V

    .line 2534785
    new-instance v3, LX/I5G;

    invoke-direct {v3, v0, v1}, LX/I5G;-><init>(LX/I5J;LX/I4u;)V

    .line 2534786
    iget-object p1, v0, LX/I5J;->b:LX/1Ck;

    const-string p2, "fetchEventsDiscoveryTimeFilters"

    invoke-virtual {p1, p2, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2534787
    goto :goto_2

    .line 2534788
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->v:Ljava/lang/String;

    .line 2534789
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/I5K;->f:Z

    .line 2534790
    iput-object v1, v0, LX/I5K;->e:Ljava/lang/String;

    .line 2534791
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2534792
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->s:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    goto :goto_2

    :cond_3
    const v2, 0x7f020912

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method
