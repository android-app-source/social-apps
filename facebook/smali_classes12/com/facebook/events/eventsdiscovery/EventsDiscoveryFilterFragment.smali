.class public Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field private A:Lcom/facebook/widget/text/BetterButton;

.field private B:Lcom/facebook/widget/text/BetterButton;

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/I4n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:LX/I4o;

.field public q:LX/I4m;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field private w:Landroid/content/Context;

.field public x:LX/I4y;

.field private y:Landroid/widget/ExpandableListView;

.field private z:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2534567
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2534568
    new-instance v0, LX/I4o;

    invoke-direct {v0, p0}, LX/I4o;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->p:LX/I4o;

    .line 2534569
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->r:Ljava/util/List;

    .line 2534570
    sget v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a:I

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    const/16 v1, 0xc

    invoke-static {v2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const-class p0, LX/I4n;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/I4n;

    iput-object v3, p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->m:LX/0Or;

    iput-object v1, p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object v2, p1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->o:LX/I4n;

    return-void
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 2534571
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 2534572
    const-string v0, "extra_location_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534573
    const-string v0, "extra_location_range"

    sget v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a:I

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    .line 2534574
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    const v1, 0x7f0d020f

    .line 2534575
    iget-object v2, v0, LX/I4m;->g:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v6, v2

    .line 2534576
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-nez v0, :cond_1

    .line 2534577
    :cond_0
    :goto_0
    return-void

    .line 2534578
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534579
    iget-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v0, v1

    .line 2534580
    if-eqz v0, :cond_2

    .line 2534581
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    invoke-static {v1, v2}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    .line 2534582
    iput-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2534583
    :cond_2
    new-instance v0, LX/I4k;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534584
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2534585
    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534586
    iget-object v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2534587
    const/4 v3, 0x1

    sget-object v4, LX/I4j;->SINGLE_SELECTION:LX/I4j;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/I4k;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/I4j;Ljava/lang/String;)V

    .line 2534588
    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    invoke-virtual {v1, v6, v0}, LX/I4m;->a(ILX/I4k;)V

    .line 2534589
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534590
    iput-object v1, v0, LX/I4m;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534591
    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x2a

    const v1, -0xad93f6e

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2534592
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2534593
    const-class v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2534594
    const v0, 0x7f0e0bcc

    invoke-virtual {p0, v7, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2534595
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2534596
    if-eqz v1, :cond_0

    .line 2534597
    const-string v0, "extra_events_discovery_fragment_category_filters"

    invoke-static {v1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->r:Ljava/util/List;

    .line 2534598
    new-instance v0, Ljava/util/HashSet;

    const-string v2, "extra_events_discovery_fragment_selected_category"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->s:Ljava/util/HashSet;

    .line 2534599
    const-string v0, "extra_events_discovery_fragment_location_filters"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->t:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2534600
    const-string v0, "extra_events_discovery_fragment_selected_location"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534601
    const-string v0, "extra_events_discovery_fragment_range_index"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    .line 2534602
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0241

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->w:Landroid/content/Context;

    .line 2534603
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->o:LX/I4n;

    iget-object v1, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->r:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->s:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->t:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iget-object v4, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget-object v5, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->p:LX/I4o;

    invoke-virtual/range {v0 .. v5}, LX/I4n;->a(Ljava/util/List;Ljava/util/HashSet;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;LX/I4o;)LX/I4m;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    .line 2534604
    const/16 v0, 0x2b

    const v1, 0x1c665bc4

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/16 v0, 0x2a

    const v1, -0x21e05b9

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2534605
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2534606
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2534607
    const/16 v2, 0x50

    invoke-virtual {v0, v2}, Landroid/view/Window;->setGravity(I)V

    .line 2534608
    const/4 v2, -0x2

    invoke-virtual {v0, v6, v2}, Landroid/view/Window;->setLayout(II)V

    .line 2534609
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->w:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2534610
    const v2, 0x7f03056b

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2534611
    const v0, 0x7f0d0f12

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->y:Landroid/widget/ExpandableListView;

    .line 2534612
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->y:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->q:LX/I4m;

    invoke-virtual {v0, v3}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 2534613
    new-instance v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->w:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    .line 2534614
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a009e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundColor(I)V

    .line 2534615
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b23ce    # 1.849486E38f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v3, v6, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2534616
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2534617
    iget-object v3, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->y:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListView;->addFooterView(Landroid/view/View;)V

    .line 2534618
    const v0, 0x7f0d0f11

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    .line 2534619
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->z:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/I4p;

    invoke-direct {v3, p0}, LX/I4p;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534620
    const v0, 0x7f0d09a9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->A:Lcom/facebook/widget/text/BetterButton;

    .line 2534621
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->A:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/I4q;

    invoke-direct {v3, p0}, LX/I4q;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534622
    const v0, 0x7f0d0f14

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->B:Lcom/facebook/widget/text/BetterButton;

    .line 2534623
    iget-object v0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->B:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/I4r;

    invoke-direct {v3, p0}, LX/I4r;-><init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534624
    const/16 v0, 0x2b

    const v3, -0x6150fde3

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
