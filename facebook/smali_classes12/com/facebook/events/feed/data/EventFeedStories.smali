.class public Lcom/facebook/events/feed/data/EventFeedStories;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/feed/data/EventFeedStories;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final c:LX/0ta;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2536047
    new-instance v0, LX/I5i;

    invoke-direct {v0}, LX/I5i;-><init>()V

    sput-object v0, Lcom/facebook/events/feed/data/EventFeedStories;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0ta;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2536041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2536042
    iput-object p1, p0, Lcom/facebook/events/feed/data/EventFeedStories;->a:LX/0Px;

    .line 2536043
    iput-object p2, p0, Lcom/facebook/events/feed/data/EventFeedStories;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2536044
    iput-object p3, p0, Lcom/facebook/events/feed/data/EventFeedStories;->c:LX/0ta;

    .line 2536045
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2536048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2536049
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2536050
    if-nez v0, :cond_0

    .line 2536051
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2536052
    :goto_0
    iput-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->a:LX/0Px;

    .line 2536053
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2536054
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0ta;

    iput-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->c:LX/0ta;

    .line 2536055
    return-void

    .line 2536056
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2536046
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2536037
    iget-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2536038
    iget-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2536039
    iget-object v0, p0, Lcom/facebook/events/feed/data/EventFeedStories;->c:LX/0ta;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2536040
    return-void
.end method
