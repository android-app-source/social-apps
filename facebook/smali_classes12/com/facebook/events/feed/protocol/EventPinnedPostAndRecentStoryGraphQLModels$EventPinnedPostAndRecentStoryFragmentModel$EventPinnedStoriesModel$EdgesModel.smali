.class public final Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1e88472f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536584
    const-class v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536583
    const-class v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2536581
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2536582
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2536571
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536572
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2536573
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2536574
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2536575
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2536576
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2536577
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2536578
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2536579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536580
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2536563
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536564
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2536565
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2536566
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2536567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;

    .line 2536568
    iput-object v0, v1, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2536569
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536570
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536561
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->e:Ljava/lang/String;

    .line 2536562
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2536558
    new-instance v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;-><init>()V

    .line 2536559
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2536560
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2536552
    const v0, 0x5775b2ed

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2536557
    const v0, 0x4be6de5e    # 3.0260412E7f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536555
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2536556
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536553
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->g:Ljava/lang/String;

    .line 2536554
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$EventPinnedStoriesModel$EdgesModel;->g:Ljava/lang/String;

    return-object v0
.end method
