.class public final Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2205b9b8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536417
    const-class v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536416
    const-class v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2536443
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2536444
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536441
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->f:Ljava/lang/String;

    .line 2536442
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2536433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536434
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2536435
    invoke-direct {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2536436
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2536437
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2536438
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2536439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2536430
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536432
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2536429
    new-instance v0, LX/I5t;

    invoke-direct {v0, p1}, LX/I5t;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536428
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2536426
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2536427
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2536425
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2536422
    new-instance v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;

    invoke-direct {v0}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;-><init>()V

    .line 2536423
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2536424
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536420
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->e:Ljava/lang/String;

    .line 2536421
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2536419
    const v0, -0x1e4191ea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2536418
    const v0, 0x4c808d5

    return v0
.end method
