.class public final Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5753896e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2537342
    const-class v0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2537341
    const-class v0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2537339
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2537340
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2537331
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2537332
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2537333
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2537334
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2537335
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2537336
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2537337
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2537338
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2537314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2537315
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2537316
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    .line 2537317
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2537318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;

    .line 2537319
    iput-object v0, v1, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->e:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    .line 2537320
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2537321
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEvent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2537329
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->e:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->e:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    .line 2537330
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->e:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2537326
    new-instance v0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;

    invoke-direct {v0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;-><init>()V

    .line 2537327
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2537328
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2537325
    const v0, -0x26c5abc2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2537324
    const v0, -0x1bd5e76f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2537322
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->f:Ljava/lang/String;

    .line 2537323
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method
