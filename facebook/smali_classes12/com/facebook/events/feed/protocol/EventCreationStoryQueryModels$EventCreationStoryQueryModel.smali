.class public final Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f566888
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536159
    const-class v0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2536187
    const-class v0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2536185
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2536186
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2536179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536180
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2536181
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2536182
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2536183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536184
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2536171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2536172
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2536173
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    .line 2536174
    invoke-virtual {p0}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2536175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;

    .line 2536176
    iput-object v0, v1, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->e:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    .line 2536177
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2536178
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2536170
    new-instance v0, LX/I5m;

    invoke-direct {v0, p1}, LX/I5m;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2536168
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->e:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->e:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    .line 2536169
    iget-object v0, p0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->e:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2536166
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2536167
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2536165
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2536162
    new-instance v0, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;-><init>()V

    .line 2536163
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2536164
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2536161
    const v0, 0x91b073e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2536160
    const v0, 0x403827a

    return v0
.end method
