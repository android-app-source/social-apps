.class public Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2524161
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2524162
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a()V

    .line 2524163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2524164
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2524165
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a()V

    .line 2524166
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2524167
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2524168
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a()V

    .line 2524169
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2524170
    const v0, 0x7f030562

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2524171
    const v0, 0x7f0d0f0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2524172
    return-void
.end method


# virtual methods
.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2524173
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2524174
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2524175
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524176
    return-void
.end method
