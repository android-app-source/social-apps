.class public final Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

.field public final synthetic b:LX/Hyx;


# direct methods
.method public constructor <init>(LX/Hyx;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 0

    .prologue
    .line 2524947
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->b:LX/Hyx;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2524948
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->b:LX/Hyx;

    iget-object v0, v0, LX/Hyx;->k:LX/HyO;

    sget-object v1, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->a(LX/HyN;)V

    .line 2524949
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->b:LX/Hyx;

    iget-object v0, v0, LX/Hyx;->i:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->b:LX/Hyx;

    iget-object v1, v1, LX/Hyx;->j:LX/Bky;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Bky;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    const-wide/16 v8, 0x3e8

    .line 2524950
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2524951
    sget-object v5, LX/Bkw;->c:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524952
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/5O7;->a(J)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2524953
    sget-object v5, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2524954
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/5O7;->a(J)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2524955
    sget-object v5, LX/Bkw;->J:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->b()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2524956
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2524957
    sget-object v5, LX/Bkw;->K:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524958
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2524959
    sget-object v5, LX/Bkw;->B:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524960
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2524961
    sget-object v5, LX/Bkw;->C:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524962
    :cond_2
    sget-object v5, LX/Bkw;->G:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2524963
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2524964
    sget-object v5, LX/Bkw;->Q:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524965
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2524966
    sget-object v5, LX/Bkw;->M:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524967
    :cond_3
    move-object v2, v4

    .line 2524968
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2524969
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$2;->b:LX/Hyx;

    iget-object v0, v0, LX/Hyx;->k:LX/HyO;

    sget-object v1, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2524970
    return-void

    .line 2524971
    :cond_4
    sget-object v5, LX/Bkw;->J:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2524972
    :cond_5
    sget-object v5, LX/Bkw;->K:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
