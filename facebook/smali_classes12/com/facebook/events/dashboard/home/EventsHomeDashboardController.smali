.class public Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/HzD;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/Activity;

.field private final d:Landroid/content/Context;

.field private final e:LX/I1G;

.field private final f:LX/I1g;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/I5E;

.field public final i:LX/1Ck;

.field private final j:LX/1nQ;

.field public k:LX/I1f;

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:LX/HzL;

.field public n:LX/0m9;

.field public o:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/HzH;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field private s:Lcom/facebook/events/common/EventAnalyticsParams;

.field public t:LX/HzI;

.field private u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2528740
    const-class v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/HzL;Landroid/content/Context;LX/I1G;LX/I1g;LX/0Or;LX/I5E;LX/1Ck;LX/1nQ;)V
    .locals 1
    .param p1    # LX/HzL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardEventsTodayAdapter$OnSeeAllClickListener;",
            "Landroid/content/Context;",
            "LX/I1G;",
            "LX/I1g;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "LX/I5E;",
            "LX/1Ck;",
            "LX/1nQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2528728
    new-instance v0, LX/I15;

    invoke-direct {v0, p0}, LX/I15;-><init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->b:LX/0Vd;

    .line 2528729
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->q:Ljava/util/List;

    .line 2528730
    const-class v0, Landroid/app/Activity;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->c:Landroid/app/Activity;

    .line 2528731
    iput-object p1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->m:LX/HzL;

    .line 2528732
    iput-object p2, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->d:Landroid/content/Context;

    .line 2528733
    iput-object p3, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    .line 2528734
    iput-object p4, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->f:LX/I1g;

    .line 2528735
    iput-object p5, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->g:LX/0Or;

    .line 2528736
    iput-object p6, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->h:LX/I5E;

    .line 2528737
    iput-object p7, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->i:LX/1Ck;

    .line 2528738
    iput-object p8, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->j:LX/1nQ;

    .line 2528739
    return-void
.end method

.method private a(J)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2528720
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2528721
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 2528722
    const/16 v3, 0xb

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2528723
    const/16 v3, 0xc

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2528724
    const/16 v3, 0xd

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2528725
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2528726
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v3, 0x0

    .line 2528685
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2528686
    const/4 v1, 0x0

    .line 2528687
    if-eqz p1, :cond_1

    .line 2528688
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528689
    if-eqz v0, :cond_1

    .line 2528690
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528691
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2528692
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528693
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2528694
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528695
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2528696
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528697
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2528698
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528699
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v1

    .line 2528700
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2528701
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchTodaysEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v7, :cond_2

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2528702
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a(J)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2528703
    if-ge v2, v10, :cond_0

    .line 2528704
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2528705
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .line 2528706
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    move v2, v3

    .line 2528707
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2528708
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 2528709
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    if-le v2, v10, :cond_3

    const/4 v3, 0x1

    .line 2528710
    :cond_3
    iget-object v2, v0, LX/I1f;->i:LX/I1N;

    .line 2528711
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2528712
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2528713
    invoke-static {v2}, LX/I1N;->f(LX/I1N;)V

    .line 2528714
    iput-boolean v3, v2, LX/I1N;->f:Z

    .line 2528715
    invoke-static {v2}, LX/I1N;->e(LX/I1N;)V

    .line 2528716
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2528717
    invoke-static {v0}, LX/I1f;->i(LX/I1f;)V

    .line 2528718
    :goto_2
    return-void

    .line 2528719
    :cond_4
    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v1, v0}, LX/I1f;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V
    .locals 7

    .prologue
    .line 2528671
    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->n:LX/0m9;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->n:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, LX/I18;

    invoke-direct {v2, p0, p1}, LX/I18;-><init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V

    iget-object v3, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->o:Ljava/lang/String;

    .line 2528672
    new-instance v4, LX/7nX;

    invoke-direct {v4}, LX/7nX;-><init>()V

    move-object v4, v4

    .line 2528673
    const-string v5, "enable_download"

    iget-object v6, v1, LX/I1G;->g:LX/0tQ;

    invoke-virtual {v6}, LX/0tQ;->c()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2528674
    const-string v5, "firstCount"

    const/4 v6, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2528675
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2528676
    const-string v5, "location"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2528677
    :cond_0
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2528678
    const-string v5, "afterCursor"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2528679
    :cond_1
    iget-object v5, v1, LX/I1G;->f:LX/I6X;

    invoke-virtual {v5, v4}, LX/I6X;->a(LX/0gW;)V

    .line 2528680
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->d:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2528681
    new-instance v5, LX/I1C;

    invoke-direct {v5, v1, v2}, LX/I1C;-><init>(LX/I1G;LX/I18;)V

    .line 2528682
    iget-object v6, v1, LX/I1G;->e:LX/1Ck;

    sget-object p0, LX/Hy9;->FETCH_EVENTS_UPDATE:LX/Hy9;

    new-instance p1, LX/I1D;

    invoke-direct {p1, v1, v4}, LX/I1D;-><init>(LX/I1G;LX/0zO;)V

    invoke-virtual {v6, p0, p1, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2528683
    return-void

    .line 2528684
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 10

    .prologue
    .line 2528637
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->o:Ljava/lang/String;

    .line 2528638
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->r:Z

    .line 2528639
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v8, 0x0

    .line 2528640
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 2528641
    iget-object v4, v0, LX/I1G;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528642
    const/16 v4, 0xb

    invoke-virtual {v5, v4, v8}, Ljava/util/Calendar;->set(II)V

    .line 2528643
    const/16 v4, 0xc

    invoke-virtual {v5, v4, v8}, Ljava/util/Calendar;->set(II)V

    .line 2528644
    const/16 v4, 0xd

    invoke-virtual {v5, v4, v8}, Ljava/util/Calendar;->set(II)V

    .line 2528645
    const/16 v4, 0xe

    invoke-virtual {v5, v4, v8}, Ljava/util/Calendar;->set(II)V

    .line 2528646
    new-instance v4, LX/7n1;

    invoke-direct {v4}, LX/7n1;-><init>()V

    const-string v6, "profile_image_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "cover_image_portrait_size"

    iget-object v7, v0, LX/I1G;->h:LX/0hB;

    invoke-virtual {v7}, LX/0hB;->f()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v6, "cover_image_landscape_size"

    iget-object v7, v0, LX/I1G;->h:LX/0hB;

    invoke-virtual {v7}, LX/0hB;->g()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v6, "first"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "timezone"

    iget-object v4, v0, LX/I1G;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/TimeZone;

    invoke-virtual {v4}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "ends_after"

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    .line 2528647
    new-instance v5, LX/7n1;

    invoke-direct {v5}, LX/7n1;-><init>()V

    move-object v5, v5

    .line 2528648
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2528649
    iget-object v6, v4, LX/0gW;->e:LX/0w7;

    move-object v4, v6

    .line 2528650
    invoke-virtual {v5, v4}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->d:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    const/4 v5, 0x1

    .line 2528651
    iput-boolean v5, v4, LX/0zO;->p:Z

    .line 2528652
    move-object v4, v4

    .line 2528653
    iget-object v5, v0, LX/I1G;->b:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v0, v4

    .line 2528654
    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2528655
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->h:LX/I5E;

    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->b:LX/0Vd;

    sget-object v3, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/I5E;->a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2528656
    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/I16;

    invoke-direct {v2, p0}, LX/I16;-><init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V

    .line 2528657
    new-instance v3, LX/7mz;

    invoke-direct {v3}, LX/7mz;-><init>()V

    move-object v3, v3

    .line 2528658
    const-string v4, "timezone"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2528659
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2528660
    iget-object v4, v1, LX/I1G;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2528661
    new-instance v4, LX/I1F;

    invoke-direct {v4, v1, v2}, LX/I1F;-><init>(LX/I1G;LX/I16;)V

    .line 2528662
    iget-object v5, v1, LX/I1G;->e:LX/1Ck;

    sget-object v6, LX/Hy9;->FETCH_TIME_FILTERS:LX/Hy9;

    invoke-virtual {v5, v6, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2528663
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    new-instance v1, LX/I17;

    invoke-direct {v1, p0}, LX/I17;-><init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V

    .line 2528664
    new-instance v2, LX/7my;

    invoke-direct {v2}, LX/7my;-><init>()V

    .line 2528665
    const-string v3, "color"

    const-string v4, "white"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "size"

    const-string v5, "24"

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2528666
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2528667
    iget-object v3, v0, LX/I1G;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2528668
    new-instance v3, LX/I1E;

    invoke-direct {v3, v0, v1}, LX/I1E;-><init>(LX/I1G;LX/I17;)V

    .line 2528669
    iget-object v4, v0, LX/I1G;->e:LX/1Ck;

    sget-object v5, LX/Hy9;->FETCH_DISCOVERY_FILTERS:LX/Hy9;

    invoke-virtual {v4, v5, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2528670
    return-void
.end method


# virtual methods
.method public final a()LX/1OM;
    .locals 1

    .prologue
    .line 2528636
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    return-object v0
.end method

.method public final a(LX/HzH;)V
    .locals 0

    .prologue
    .line 2528634
    iput-object p1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->p:LX/HzH;

    .line 2528635
    return-void
.end method

.method public final a(LX/HzI;)V
    .locals 0

    .prologue
    .line 2528557
    iput-object p1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->t:LX/HzI;

    .line 2528558
    return-void
.end method

.method public final a(Lcom/facebook/events/common/EventAnalyticsParams;LX/HzV;)V
    .locals 2

    .prologue
    .line 2528627
    iput-object p1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->s:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528628
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->f:LX/I1g;

    invoke-virtual {v0, p1}, LX/I1g;->a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I1f;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    .line 2528629
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->m:LX/HzL;

    .line 2528630
    iget-object p1, v0, LX/I1f;->i:LX/I1N;

    .line 2528631
    iput-object v1, p1, LX/I1N;->h:LX/HzL;

    .line 2528632
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e()V

    .line 2528633
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 12

    .prologue
    .line 2528603
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    .line 2528604
    iget-object v1, v0, LX/I1f;->i:LX/I1N;

    .line 2528605
    iget-boolean v0, v1, LX/I1N;->f:Z

    move v1, v0

    .line 2528606
    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2528607
    if-eqz v0, :cond_0

    .line 2528608
    new-instance v0, LX/7n4;

    invoke-direct {v0}, LX/7n4;-><init>()V

    .line 2528609
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7n4;->f:Ljava/lang/String;

    .line 2528610
    iput-object p1, v0, LX/7n4;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2528611
    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0}, LX/7n4;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-result-object v0

    .line 2528612
    iget-object v2, v1, LX/I1f;->j:LX/I1R;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/I1R;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    .line 2528613
    iget-object v2, v1, LX/I1f;->i:LX/I1N;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2528614
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 2528615
    iput-boolean v7, v2, LX/I1N;->f:Z

    .line 2528616
    :goto_1
    invoke-static {v2}, LX/I1N;->e(LX/I1N;)V

    .line 2528617
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2528618
    invoke-static {v1}, LX/I1f;->i(LX/I1f;)V

    .line 2528619
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v5, v6

    .line 2528620
    :goto_2
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 2528621
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v4

    invoke-interface {v4}, LX/7oa;->j()J

    move-result-wide v10

    cmp-long v4, v8, v10

    if-lez v4, :cond_5

    .line 2528622
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v6, v7

    .line 2528623
    :cond_3
    if-nez v6, :cond_4

    .line 2528624
    iget-object v4, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2528625
    :cond_4
    invoke-static {v2}, LX/I1N;->f(LX/I1N;)V

    goto :goto_1

    .line 2528626
    :cond_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2528601
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0, p1}, LX/I1f;->a(Ljava/lang/String;)V

    .line 2528602
    return-void
.end method

.method public final a(ZII)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2528594
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2528595
    :goto_0
    return-void

    .line 2528596
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->u:Z

    if-nez v0, :cond_1

    .line 2528597
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->j:LX/1nQ;

    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->s:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->s:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2528598
    iput-boolean v4, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->u:Z

    .line 2528599
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0, v4}, LX/I1f;->b(Z)V

    .line 2528600
    invoke-static {p0, v4}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2528584
    const/4 v4, 0x0

    .line 2528585
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 2528586
    :cond_0
    :goto_0
    return-void

    .line 2528587
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2528588
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2528589
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2528590
    iput-object v4, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2528591
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2528592
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->i:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_EVENTS_TODAY:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I19;

    invoke-direct {v3, p0}, LX/I19;-><init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2528593
    iput-object v4, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 3

    .prologue
    .line 2528569
    if-eqz p1, :cond_1

    .line 2528570
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2528571
    new-instance v0, LX/7n4;

    invoke-direct {v0}, LX/7n4;-><init>()V

    .line 2528572
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7n4;->f:Ljava/lang/String;

    .line 2528573
    iput-object p1, v0, LX/7n4;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2528574
    iget-object v1, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0}, LX/7n4;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-result-object v0

    .line 2528575
    iget-object v2, v1, LX/I1f;->i:LX/I1N;

    .line 2528576
    iget-object p0, v2, LX/I1N;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2528577
    iget-object p0, v2, LX/I1N;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 2528578
    iget-object p1, v2, LX/I1N;->d:Ljava/util/List;

    invoke-interface {p1, p0, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2528579
    invoke-static {v2}, LX/I1N;->e(LX/I1N;)V

    .line 2528580
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2528581
    :cond_0
    invoke-static {v1}, LX/I1f;->i(LX/I1f;)V

    .line 2528582
    :cond_1
    :goto_0
    return-void

    .line 2528583
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/I1f;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2528565
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->i:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2528566
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e:LX/I1G;

    .line 2528567
    iget-object p0, v0, LX/I1G;->e:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 2528568
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2528559
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1f;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    .line 2528560
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0}, LX/I1f;->d()V

    .line 2528561
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, LX/I1f;->b(Ljava/util/List;)V

    .line 2528562
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->e()V

    .line 2528563
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->b()V

    .line 2528564
    return-void
.end method
