.class public Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2529016
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2529017
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->a()V

    .line 2529018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529019
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2529020
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->a()V

    .line 2529021
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529022
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2529023
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->a()V

    .line 2529024
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2529025
    const v0, 0x7f030582

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2529026
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->setLayoutDirection(I)V

    .line 2529027
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2529028
    return-void
.end method
