.class public Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:Lcom/facebook/fig/listitem/FigListItem;

.field public n:LX/1aX;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/0m9;

.field public r:Landroid/view/View$OnClickListener;

.field private final s:Landroid/content/Context;

.field public final t:LX/1Ad;

.field public final u:LX/0wM;

.field public final v:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final w:LX/I53;

.field public final x:LX/1nQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2528868
    const-class v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    const-string v1, "event_dashboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/fig/listitem/FigListItem;Landroid/content/Context;LX/1Ad;LX/0wM;LX/I53;LX/1nQ;)V
    .locals 2
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/fig/listitem/FigListItem;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2528856
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2528857
    iput-object p3, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->s:Landroid/content/Context;

    .line 2528858
    iput-object p1, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->v:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528859
    iput-object p7, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->x:LX/1nQ;

    .line 2528860
    iput-object p2, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->m:Lcom/facebook/fig/listitem/FigListItem;

    .line 2528861
    iput-object p4, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->t:LX/1Ad;

    .line 2528862
    iput-object p5, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->u:LX/0wM;

    .line 2528863
    iput-object p6, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->w:LX/I53;

    .line 2528864
    new-instance v0, LX/1Uo;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-static {v0, p3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->n:LX/1aX;

    .line 2528865
    new-instance v0, LX/I1J;

    invoke-direct {v0, p0}, LX/I1J;-><init>(Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;)V

    move-object v0, v0

    .line 2528866
    iput-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->r:Landroid/view/View$OnClickListener;

    .line 2528867
    return-void
.end method
