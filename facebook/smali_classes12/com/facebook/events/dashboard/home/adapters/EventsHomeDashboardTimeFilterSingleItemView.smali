.class public Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2529013
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2529014
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a()V

    .line 2529015
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2528998
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2528999
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a()V

    .line 2529000
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529010
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2529011
    invoke-direct {p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a()V

    .line 2529012
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2529006
    const v0, 0x7f030581

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2529007
    const v0, 0x7f0d0f45

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2529008
    const v0, 0x7f0d0f46

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2529009
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2529001
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2529002
    iget-object v0, p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2529003
    invoke-virtual {p0, p3}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2529004
    invoke-virtual {p0, p4}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2529005
    return-void
.end method
