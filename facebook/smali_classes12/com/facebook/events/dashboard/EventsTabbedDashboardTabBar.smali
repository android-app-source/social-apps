.class public Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:I


# instance fields
.field public b:I

.field private c:[Lcom/facebook/resources/ui/FbTextView;

.field public d:LX/HzM;

.field private e:LX/5O6;

.field private f:Lcom/facebook/fbui/widget/text/BadgeTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2526202
    const v0, 0x7f0d0f53

    sput v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2526203
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2526204
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    .line 2526205
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a()V

    .line 2526206
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526207
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2526208
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    .line 2526209
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a()V

    .line 2526210
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526211
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2526212
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    .line 2526213
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a()V

    .line 2526214
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2526215
    const v0, 0x7f03058e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2526216
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->e:LX/5O6;

    .line 2526217
    const v0, 0x7f0d0f54

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2526218
    const v1, 0x7f0d0f55

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->f:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2526219
    const v1, 0x7f0d0f56

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2526220
    sget v3, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a:I

    sget-object v4, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTag(ILjava/lang/Object;)V

    .line 2526221
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->f:Lcom/facebook/fbui/widget/text/BadgeTextView;

    sget v4, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a:I

    sget-object v5, LX/Hx7;->CALENDAR:LX/Hx7;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTag(ILjava/lang/Object;)V

    .line 2526222
    sget v3, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a:I

    sget-object v4, LX/Hx7;->HOSTING:LX/Hx7;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTag(ILjava/lang/Object;)V

    .line 2526223
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/facebook/resources/ui/FbTextView;

    aput-object v0, v3, v2

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->f:Lcom/facebook/fbui/widget/text/BadgeTextView;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    aput-object v1, v3, v0

    iput-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->c:[Lcom/facebook/resources/ui/FbTextView;

    .line 2526224
    sget-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setSelectedTabType(LX/Hx7;)V

    .line 2526225
    new-instance v1, LX/Hzq;

    invoke-direct {v1, p0}, LX/Hzq;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;)V

    .line 2526226
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->c:[Lcom/facebook/resources/ui/FbTextView;

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v2, v3, v0

    .line 2526227
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2526228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2526229
    :cond_0
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2526230
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2526231
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->e:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->a(Landroid/graphics/Canvas;)V

    .line 2526232
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->e:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->b(Landroid/graphics/Canvas;)V

    .line 2526233
    return-void
.end method

.method public getCalendarBadgeCount()I
    .locals 1

    .prologue
    .line 2526234
    iget v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    return v0
.end method

.method public setCalendarBadgeCount(I)V
    .locals 2

    .prologue
    .line 2526235
    iput p1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    .line 2526236
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->f:Lcom/facebook/fbui/widget/text/BadgeTextView;

    if-lez p1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2526237
    return-void

    .line 2526238
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public setOnTabChangeListener(LX/HzM;)V
    .locals 0

    .prologue
    .line 2526239
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->d:LX/HzM;

    .line 2526240
    return-void
.end method

.method public setSelectedTabType(LX/Hx7;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2526241
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->c:[Lcom/facebook/resources/ui/FbTextView;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2526242
    sget v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->a:I

    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbTextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbTextView;->setSelected(Z)V

    .line 2526243
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2526244
    goto :goto_1

    .line 2526245
    :cond_1
    return-void
.end method
