.class public Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

.field public b:LX/I0b;

.field public c:LX/I0a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2527443
    new-instance v0, LX/I0Z;

    invoke-direct {v0}, LX/I0Z;-><init>()V

    sput-object v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2527441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527442
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2527436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/I0b;->valueOf(Ljava/lang/String;)LX/I0b;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    .line 2527438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/I0a;->valueOf(Ljava/lang/String;)LX/I0a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    .line 2527439
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527440
    return-void
.end method

.method public static a()Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;
    .locals 1

    .prologue
    .line 2527424
    new-instance v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    invoke-direct {v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/I0a;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;
    .locals 0

    .prologue
    .line 2527434
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    .line 2527435
    return-object p0
.end method

.method public final a(LX/I0b;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;
    .locals 0

    .prologue
    .line 2527432
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    .line 2527433
    return-object p0
.end method

.method public final a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;
    .locals 0

    .prologue
    .line 2527430
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527431
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2527429
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2527425
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    invoke-virtual {v0}, LX/I0b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2527426
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    invoke-virtual {v0}, LX/I0a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2527427
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2527428
    return-void
.end method
