.class public Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/I7w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/view/View$OnClickListener;

.field public k:LX/7oa;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2528298
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2528299
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a()V

    .line 2528300
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2528295
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2528296
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a()V

    .line 2528297
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2528248
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2528249
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a()V

    .line 2528250
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2528302
    new-instance v0, LX/I0w;

    invoke-direct {v0, p0, p1}, LX/I0w;-><init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2528301
    new-instance v0, LX/I0x;

    invoke-direct {v0, p0, p1}, LX/I0x;-><init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2528303
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2528304
    const v0, 0x7f03054e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2528305
    const v0, 0x7f0d0eec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2528306
    const v0, 0x7f0d0eed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2528307
    const v0, 0x7f0d0eee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2528308
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    invoke-static {v0}, LX/I7w;->b(LX/0QB;)LX/I7w;

    move-result-object v0

    check-cast v0, LX/I7w;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a:LX/I7w;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2528288
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->e:Landroid/view/View$OnClickListener;

    .line 2528289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->f:Landroid/view/View$OnClickListener;

    .line 2528290
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->g:Landroid/view/View$OnClickListener;

    .line 2528291
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528292
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528293
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528294
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2528281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->h:Landroid/view/View$OnClickListener;

    .line 2528282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->i:Landroid/view/View$OnClickListener;

    .line 2528283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->j:Landroid/view/View$OnClickListener;

    .line 2528284
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528285
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528286
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528287
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2528277
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2528278
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2528279
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2528280
    return-void
.end method

.method public static setPrivateRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 3

    .prologue
    .line 2528271
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d()V

    .line 2528272
    sget-object v0, LX/I0y;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2528273
    :goto_0
    return-void

    .line 2528274
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 2528275
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 2528276
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static setPublicRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 3

    .prologue
    .line 2528265
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d()V

    .line 2528266
    sget-object v0, LX/I0y;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2528267
    :goto_0
    return-void

    .line 2528268
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 2528269
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 2528270
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 2

    .prologue
    .line 2528251
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->k:LX/7oa;

    .line 2528252
    invoke-interface {p1}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v1, :cond_0

    .line 2528253
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821ee

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528254
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821eb

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528255
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821ef

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528256
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c()V

    .line 2528257
    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setPublicRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 2528258
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a:LX/I7w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, LX/I7w;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2528259
    return-void

    .line 2528260
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821eb

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528261
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821ec

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528262
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821ed

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2528263
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->b()V

    .line 2528264
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setPrivateRsvpButtonColor(Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0
.end method
