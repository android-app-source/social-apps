.class public Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/Blh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/I0r;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Landroid/graphics/Paint;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

.field private l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

.field public n:LX/7oa;

.field public o:Lcom/facebook/events/common/EventAnalyticsParams;

.field public p:Lcom/facebook/events/common/ActionMechanism;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2527375
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2527376
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    .line 2527377
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a()V

    .line 2527378
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527371
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2527372
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    .line 2527373
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a()V

    .line 2527374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527292
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2527293
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    .line 2527294
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a()V

    .line 2527295
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527357
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527358
    const v0, 0x7f03054b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2527359
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->setWillNotDraw(Z)V

    .line 2527360
    const v0, 0x7f0d0ed4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2527361
    const v0, 0x7f0d0ed5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2527362
    const v0, 0x7f0d0edf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2527363
    const v0, 0x7f0d0ee0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->k:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    .line 2527364
    const v0, 0x7f0d0ede

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2527365
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2527366
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2527367
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->d:I

    .line 2527368
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->e:I

    .line 2527369
    new-instance v0, LX/I0V;

    invoke-direct {v0, p0}, LX/I0V;-><init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2527370
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;LX/Blh;LX/I0r;)V
    .locals 0

    .prologue
    .line 2527356
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a:LX/Blh;

    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->b:LX/I0r;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;

    invoke-static {v1}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v0

    check-cast v0, LX/Blh;

    invoke-static {v1}, LX/I0r;->b(LX/0QB;)LX/I0r;

    move-result-object v1

    check-cast v1, LX/I0r;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;LX/Blh;LX/I0r;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2527352
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2527353
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->b:LX/I0r;

    iget-object v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/I0r;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Landroid/text/SpannableStringBuilder;Z)V

    .line 2527354
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527355
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2527337
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    invoke-static {v1}, LX/I0r;->a(LX/7oa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527338
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527339
    iget-object v3, v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    move-object v0, v3

    .line 2527340
    sget-object v3, LX/I0a;->PAST:LX/I0a;

    if-ne v0, v3, :cond_0

    const v0, 0x7f0a00a4

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2527341
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    invoke-static {v0}, LX/I0r;->b(LX/7oa;)Ljava/lang/String;

    move-result-object v0

    .line 2527342
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2527343
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2527344
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527345
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2527346
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->k:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    iget-object v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527347
    iget-object v3, v2, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    move-object v2, v3

    .line 2527348
    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a(LX/7oa;LX/I0a;)V

    .line 2527349
    return-void

    .line 2527350
    :cond_0
    const v0, 0x7f0a00ab

    goto :goto_0

    .line 2527351
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2527325
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    invoke-interface {v0}, LX/7oa;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527326
    iget-object v1, v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    move-object v0, v1

    .line 2527327
    sget-object v1, LX/I0a;->FUTURE:LX/I0a;

    if-ne v0, v1, :cond_1

    .line 2527328
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    if-nez v0, :cond_0

    .line 2527329
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->f()V

    .line 2527330
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    iget-object v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2527331
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    invoke-virtual {v0, v3}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setVisibility(I)V

    .line 2527332
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    invoke-virtual {v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->f:I

    .line 2527333
    :goto_0
    return-void

    .line 2527334
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    if-eqz v0, :cond_2

    .line 2527335
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->setVisibility(I)V

    .line 2527336
    :cond_2
    iput v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->f:I

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2527317
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    invoke-interface {v0}, LX/7oa;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v0

    .line 2527318
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    invoke-interface {v1}, LX/7oa;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v1

    .line 2527319
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 2527320
    if-eqz v1, :cond_1

    invoke-static {v1}, LX/5O7;->a(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2527321
    if-eqz v0, :cond_0

    .line 2527322
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2527323
    :goto_1
    return-void

    .line 2527324
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2527314
    const v0, 0x7f0d0ee1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2527315
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->l:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    .line 2527316
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 4

    .prologue
    .line 2527300
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527301
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v0, v0

    .line 2527302
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->n:LX/7oa;

    .line 2527303
    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527304
    iput-object p3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->p:Lcom/facebook/events/common/ActionMechanism;

    .line 2527305
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->m:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527306
    iget-object v3, v0, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    move-object v0, v3

    .line 2527307
    sget-object v3, LX/I0a;->PAST:LX/I0a;

    if-ne v0, v3, :cond_0

    const v0, 0x7f0a011a

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2527308
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->b()V

    .line 2527309
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c()V

    .line 2527310
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->d()V

    .line 2527311
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->e()V

    .line 2527312
    return-void

    .line 2527313
    :cond_0
    const v0, 0x7f0a008a

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2527296
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2527297
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    .line 2527298
    iget v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->e:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->d:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->e:I

    sub-int/2addr v0, v4

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->f:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2527299
    return-void
.end method
