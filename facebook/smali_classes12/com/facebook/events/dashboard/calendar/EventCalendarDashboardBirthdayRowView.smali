.class public Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I76;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I0r;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/view/View$OnClickListener;

.field private final e:Landroid/graphics/Paint;

.field private f:I

.field private g:I

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2526989
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2526990
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526991
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a:LX/0Ot;

    .line 2526992
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526993
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->b:LX/0Ot;

    .line 2526994
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526995
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->c:LX/0Ot;

    .line 2526996
    new-instance v0, LX/I0Q;

    invoke-direct {v0, p0}, LX/I0Q;-><init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->d:Landroid/view/View$OnClickListener;

    .line 2526997
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    .line 2526998
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a()V

    .line 2526999
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526978
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2526979
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526980
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a:LX/0Ot;

    .line 2526981
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526982
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->b:LX/0Ot;

    .line 2526983
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526984
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->c:LX/0Ot;

    .line 2526985
    new-instance v0, LX/I0Q;

    invoke-direct {v0, p0}, LX/I0Q;-><init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->d:Landroid/view/View$OnClickListener;

    .line 2526986
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    .line 2526987
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a()V

    .line 2526988
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526967
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2526968
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526969
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a:LX/0Ot;

    .line 2526970
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526971
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->b:LX/0Ot;

    .line 2526972
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2526973
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->c:LX/0Ot;

    .line 2526974
    new-instance v0, LX/I0Q;

    invoke-direct {v0, p0}, LX/I0Q;-><init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->d:Landroid/view/View$OnClickListener;

    .line 2526975
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    .line 2526976
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a()V

    .line 2526977
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527000
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527001
    const v0, 0x7f030542

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2527002
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->setOrientation(I)V

    .line 2527003
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2527004
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2527005
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->f:I

    .line 2527006
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->g:I

    .line 2527007
    const v0, 0x7f0d0ed2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2527008
    const v0, 0x7f0d0ed3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 2527009
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;",
            "LX/0Ot",
            "<",
            "LX/I76;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/I0r;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2526966
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->c:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;

    const/16 v1, 0x1b62

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1b10

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x5c8

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 2526934
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526935
    :cond_0
    return-void

    .line 2526936
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2526937
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2526938
    invoke-static {v1}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2526939
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0r;

    invoke-virtual {v0}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v0

    .line 2526940
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    cmp-long v0, v4, v0

    if-gez v0, :cond_2

    const/4 v0, 0x1

    .line 2526941
    :goto_0
    iget-object v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v0, :cond_3

    const v1, 0x7f0a011a

    :goto_1
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2526942
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v0, :cond_4

    const v0, 0x7f0a00a4

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 2526943
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2526944
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 2526945
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v1, 0x7f0207aa

    invoke-virtual {v0, v1, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 2526946
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v4, v2

    move v3, v2

    .line 2526947
    :goto_3
    if-ge v4, v9, :cond_6

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;

    .line 2526948
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 2526949
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2526950
    :goto_4
    invoke-virtual {v1, v7, v11, v11, v11}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2526951
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/facebook/widget/text/BetterTextView;->setTag(Ljava/lang/Object;)V

    .line 2526952
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2526953
    invoke-virtual {v1, v5}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2526954
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2526955
    add-int/lit8 v3, v3, 0x1

    .line 2526956
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_2
    move v0, v2

    .line 2526957
    goto :goto_0

    .line 2526958
    :cond_3
    const v1, 0x7f0a008a

    goto :goto_1

    .line 2526959
    :cond_4
    const v0, 0x7f0a00ab

    goto :goto_2

    .line 2526960
    :cond_5
    const v1, 0x7f03054c

    invoke-virtual {v6, v1, v11, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2526961
    iget-object v10, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v10}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2526962
    iget-object v10, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v10, v1}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_4

    :cond_6
    move v0, v3

    .line 2526963
    :goto_5
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2526964
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2526965
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2526930
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2526931
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    .line 2526932
    iget v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->f:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->g:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->f:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->g:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardBirthdayRowView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2526933
    return-void
.end method
