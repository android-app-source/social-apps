.class public Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/I0r;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Blh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/1aX;

.field private f:I

.field private g:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field private j:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

.field private k:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

.field private l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public m:LX/7oa;

.field public n:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final o:Landroid/graphics/Paint;

.field private p:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2527902
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    const-string v1, "event_profile_pic"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2527898
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2527899
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    .line 2527900
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a()V

    .line 2527901
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527894
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2527895
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    .line 2527896
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a()V

    .line 2527897
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527890
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2527891
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    .line 2527892
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a()V

    .line 2527893
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527871
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527872
    const v0, 0x7f030546

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2527873
    const v0, 0x7f0d0ed7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2527874
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2527875
    const v0, 0x7f0d0ed8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->g:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    .line 2527876
    const v0, 0x7f0d0ed9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2527877
    const v0, 0x7f0d0eda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2527878
    const v0, 0x7f0d0edb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->j:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    .line 2527879
    const v0, 0x7f0d0edc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->k:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    .line 2527880
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2527881
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2527882
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2527883
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->p:I

    .line 2527884
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2527885
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    .line 2527886
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 2527887
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->f:I

    .line 2527888
    new-instance v0, LX/I0n;

    invoke-direct {v0, p0}, LX/I0n;-><init>(Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2527889
    return-void
.end method

.method private a(LX/7oa;)V
    .locals 2

    .prologue
    .line 2527846
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a:LX/1Ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 2527847
    invoke-interface {p1}, LX/7oa;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v0

    .line 2527848
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->g:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/util/Date;)Z

    .line 2527849
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;LX/1Ad;LX/I0r;LX/Blh;)V
    .locals 0

    .prologue
    .line 2527870
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->b:LX/I0r;

    iput-object p3, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->c:LX/Blh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/I0r;->b(LX/0QB;)LX/I0r;

    move-result-object v1

    check-cast v1, LX/I0r;

    invoke-static {v2}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v2

    check-cast v2, LX/Blh;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a(Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;LX/1Ad;LX/I0r;LX/Blh;)V

    return-void
.end method


# virtual methods
.method public final a(LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 3

    .prologue
    .line 2527858
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    .line 2527859
    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527860
    invoke-virtual {p0, p1}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->setCoverPhotoView(LX/7oa;)V

    .line 2527861
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    invoke-static {v1}, LX/I0r;->a(LX/7oa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527862
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    invoke-static {v0}, LX/I0r;->b(LX/7oa;)Ljava/lang/String;

    move-result-object v0

    .line 2527863
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2527864
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527865
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2527866
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->j:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    sget-object v2, LX/I0a;->FUTURE:LX/I0a;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a(LX/7oa;LX/I0a;)V

    .line 2527867
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->k:Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->m:LX/7oa;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarRsvpView;->a(LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2527868
    return-void

    .line 2527869
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2527853
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2527854
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getBottom()I

    move-result v0

    .line 2527855
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    .line 2527856
    iget v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->p:I

    int-to-float v1, v1

    int-to-float v2, v0

    iget-object v4, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    sub-float/2addr v2, v4

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->p:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v0, v0

    iget-object v4, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2527857
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x23512793

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527850
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2527851
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2527852
    const/16 v1, 0x2d

    const v2, 0x4fc31766    # 6.5461811E9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x56356f2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527843
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2527844
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2527845
    const/16 v1, 0x2d

    const v2, -0x60273c3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2527840
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 2527841
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2527842
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2527837
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 2527838
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2527839
    return-void
.end method

.method public setCoverPhotoView(LX/7oa;)V
    .locals 4

    .prologue
    .line 2527826
    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eT_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eT_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2527827
    invoke-interface {p1}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eT_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 2527828
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    iget v2, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->f:I

    iget v3, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->f:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 2527829
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 2527830
    move-object v0, v0

    .line 2527831
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2527832
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a:LX/1Ad;

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 2527833
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 2527834
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->g:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2527835
    :goto_0
    return-void

    .line 2527836
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a(LX/7oa;)V

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2527825
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
