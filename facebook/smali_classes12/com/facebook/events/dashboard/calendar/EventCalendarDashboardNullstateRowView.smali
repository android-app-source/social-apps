.class public Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/I53;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/graphics/Paint;

.field private c:I

.field private d:I

.field private e:Ljava/lang/Long;

.field private f:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2527080
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2527081
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    .line 2527082
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a()V

    .line 2527083
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527095
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2527096
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    .line 2527097
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a()V

    .line 2527098
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527108
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2527109
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    .line 2527110
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a()V

    .line 2527111
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527099
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527100
    const v0, 0x7f030549

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2527101
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2527102
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2527103
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2527104
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->c:I

    .line 2527105
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->d:I

    .line 2527106
    invoke-virtual {p0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2527107
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/Long;)V
    .locals 7

    .prologue
    .line 2527093
    iget-object v6, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a:LX/I53;

    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v6, p1, v0}, LX/I53;->a(Ljava/lang/Long;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2527094
    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;

    invoke-static {v0}, LX/I53;->b(LX/0QB;)LX/I53;

    move-result-object v0

    check-cast v0, LX/I53;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a:LX/I53;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 2527090
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->e:Ljava/lang/Long;

    .line 2527091
    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527092
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2527086
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2527087
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    .line 2527088
    iget v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->c:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->c:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->d:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2527089
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x505451dc

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527084
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->e:Ljava/lang/Long;

    invoke-direct {p0, v1}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardNullstateRowView;->a(Ljava/lang/Long;)V

    .line 2527085
    const v1, 0x4a2a8e73    # 2794396.8f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
