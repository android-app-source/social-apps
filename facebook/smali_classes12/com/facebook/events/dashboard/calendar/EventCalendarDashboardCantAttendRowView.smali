.class public Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/Blh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/I0r;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Landroid/graphics/Paint;

.field private d:I

.field private e:I

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

.field public j:LX/7oa;

.field public k:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2527057
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2527058
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    .line 2527059
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a()V

    .line 2527060
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527053
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2527054
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    .line 2527055
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a()V

    .line 2527056
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527049
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2527050
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    .line 2527051
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a()V

    .line 2527052
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527036
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527037
    const v0, 0x7f030544

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2527038
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->setWillNotDraw(Z)V

    .line 2527039
    const v0, 0x7f0d0ed4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2527040
    const v0, 0x7f0d0ed5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2527041
    const v0, 0x7f0d0ed6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2527042
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2527043
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2527044
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2527045
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->d:I

    .line 2527046
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->e:I

    .line 2527047
    new-instance v0, LX/I0R;

    invoke-direct {v0, p0}, LX/I0R;-><init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2527048
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;LX/Blh;LX/I0r;)V
    .locals 0

    .prologue
    .line 2527035
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a:LX/Blh;

    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->b:LX/I0r;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    invoke-static {v1}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v0

    check-cast v0, LX/Blh;

    invoke-static {v1}, LX/I0r;->b(LX/0QB;)LX/I0r;

    move-result-object v1

    check-cast v1, LX/I0r;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;LX/Blh;LX/I0r;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2527031
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->j:LX/7oa;

    invoke-static {v1}, LX/I0r;->a(LX/7oa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527032
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->j:LX/7oa;

    invoke-interface {v0}, LX/7oa;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0821f2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527033
    return-void

    .line 2527034
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0821ed

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2527027
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2527028
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->b:LX/I0r;

    iget-object v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->i:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/I0r;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Landroid/text/SpannableStringBuilder;Z)V

    .line 2527029
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527030
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1

    .prologue
    .line 2527020
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->i:Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2527021
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v0, v0

    .line 2527022
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->j:LX/7oa;

    .line 2527023
    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2527024
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c()V

    .line 2527025
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->b()V

    .line 2527026
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2527016
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2527017
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    .line 2527018
    iget v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->e:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->d:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->e:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2527019
    return-void
.end method
