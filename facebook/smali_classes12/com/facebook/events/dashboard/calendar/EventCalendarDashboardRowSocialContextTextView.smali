.class public Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/I10;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/I11;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0hL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/1Uo;

.field private final f:LX/4Ac;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2527285
    const-class v0, LX/I11;

    const-string v1, "event_dashboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2527281
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2527282
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    .line 2527283
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a()V

    .line 2527284
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527277
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2527278
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    .line 2527279
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a()V

    .line 2527280
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2527273
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2527274
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    .line 2527275
    invoke-direct {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a()V

    .line 2527276
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2527264
    const-class v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2527265
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 2527266
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2527267
    move-object v0, v0

    .line 2527268
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2527269
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2527270
    move-object v0, v0

    .line 2527271
    iput-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->e:LX/1Uo;

    .line 2527272
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;LX/I10;LX/I11;LX/0hL;)V
    .locals 0

    .prologue
    .line 2527263
    iput-object p1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a:LX/I10;

    iput-object p2, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->b:LX/I11;

    iput-object p3, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->c:LX/0hL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;

    new-instance p1, LX/I10;

    const-class v0, Landroid/content/Context;

    invoke-interface {v2, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p1, v0, v1, v3}, LX/I10;-><init>(Landroid/content/Context;LX/1Ad;LX/0wM;)V

    move-object v0, p1

    check-cast v0, LX/I10;

    new-instance p1, LX/I11;

    invoke-static {v2}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v1

    check-cast v1, LX/0hL;

    invoke-static {v2}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p1, v1, v3}, LX/I11;-><init>(LX/0hL;Landroid/content/res/Resources;)V

    move-object v1, p1

    check-cast v1, LX/I11;

    invoke-static {v2}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v2

    check-cast v2, LX/0hL;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;LX/I10;LX/I11;LX/0hL;)V

    return-void
.end method


# virtual methods
.method public final a(LX/7oa;LX/I0a;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2527125
    invoke-interface {p1}, LX/7oa;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2527126
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a:LX/I10;

    .line 2527127
    invoke-interface {p1}, LX/7oa;->o()J

    move-result-wide v8

    invoke-static {v8, v9}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v8

    .line 2527128
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 2527129
    invoke-static {v8}, LX/5O7;->a(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-virtual {v8, v9}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2527130
    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v8, v10, v11}, LX/5O7;->a(Ljava/util/Date;J)Ljava/lang/String;

    move-result-object v8

    .line 2527131
    iget-object v9, v0, LX/I10;->a:Landroid/content/Context;

    const v10, 0x7f0837de

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2527132
    :goto_0
    move-object v0, v8

    .line 2527133
    invoke-virtual {p0, v5, v5, v5, v5}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2527134
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2527135
    invoke-virtual {p0, v3}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->setVisibility(I)V

    .line 2527136
    return-void

    .line 2527137
    :cond_0
    sget-object v0, LX/I0a;->PAST:LX/I0a;

    invoke-virtual {p2, v0}, LX/I0a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2527138
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a:LX/I10;

    .line 2527139
    invoke-interface {p1}, LX/7oa;->R()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2527140
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2527141
    :goto_2
    move-object v0, v1

    .line 2527142
    invoke-virtual {p0, v5, v5, v5, v5}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2527143
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->a:LX/I10;

    sget-object v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2527144
    new-instance v2, LX/I12;

    invoke-direct {v2}, LX/I12;-><init>()V

    .line 2527145
    if-nez p1, :cond_17

    .line 2527146
    invoke-virtual {v2}, LX/I12;->a()LX/I13;

    move-result-object v2

    .line 2527147
    :goto_3
    move-object v6, v2

    .line 2527148
    iget-object v0, v6, LX/I13;->a:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v0

    .line 2527149
    :goto_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 2527150
    :goto_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 2527151
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    if-ne v2, v0, :cond_4

    .line 2527152
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->e:LX/1Uo;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v0, v4}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 2527153
    iget-object v4, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v4, v0}, LX/4Ac;->a(LX/1aX;)V

    move-object v4, v0

    .line 2527154
    :goto_6
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    invoke-virtual {v4, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 2527155
    invoke-virtual {v4}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2527156
    invoke-virtual {v4}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2527157
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2527158
    :cond_3
    iget-object v0, v6, LX/I13;->a:Ljava/util/List;

    move-object v1, v0

    goto :goto_4

    .line 2527159
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v0, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    move-object v4, v0

    goto :goto_6

    .line 2527160
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->b:LX/I11;

    iget-object v1, v6, LX/I13;->d:Landroid/graphics/drawable/Drawable;

    iget v2, v6, LX/I13;->c:I

    const/4 v11, 0x0

    .line 2527161
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 2527162
    iput-object v7, v0, LX/I11;->f:Ljava/util/List;

    .line 2527163
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/Drawable;

    .line 2527164
    const/4 v7, 0x0

    .line 2527165
    iget v10, v0, LX/I11;->c:I

    iget v12, v0, LX/I11;->c:I

    invoke-virtual {v4, v7, v7, v10, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2527166
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2527167
    goto :goto_7

    .line 2527168
    :cond_6
    iput-object v1, v0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    .line 2527169
    iget-object v4, v0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    iget-object v9, v0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    iget-object v10, v0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    invoke-virtual {v4, v11, v11, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2527170
    add-int/lit8 v4, v8, 0x1

    .line 2527171
    iget-object v8, v0, LX/I11;->i:Landroid/graphics/Paint;

    iget-object v9, v0, LX/I11;->b:Landroid/content/res/Resources;

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 2527172
    iget v8, v0, LX/I11;->c:I

    mul-int/2addr v8, v4

    add-int/lit8 v4, v4, -0x1

    iget v9, v0, LX/I11;->d:I

    mul-int/2addr v4, v9

    sub-int v4, v8, v4

    iput v4, v0, LX/I11;->g:I

    .line 2527173
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->c:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v5

    :goto_8
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->c:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->b:LX/I11;

    :goto_9
    invoke-virtual {p0, v0, v5, v1, v5}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2527174
    iget-object v0, v6, LX/I13;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 2527175
    :cond_7
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->b:LX/I11;

    goto :goto_8

    :cond_8
    move-object v1, v5

    goto :goto_9

    :cond_9
    iget-object v8, v0, LX/I10;->a:Landroid/content/Context;

    const v9, 0x7f0837df

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 2527176
    :cond_a
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2527177
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2527178
    :cond_b
    invoke-static {p1}, LX/I10;->s(LX/7oa;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2527179
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2527180
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2527181
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0176

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v1, v2, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2527182
    :goto_a
    move-object v1, v1

    .line 2527183
    goto/16 :goto_2

    .line 2527184
    :cond_c
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2527185
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0175

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v4

    invoke-interface {v4}, LX/7oe;->a()I

    move-result v4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v8

    invoke-interface {v8}, LX/7oe;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v2, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2527186
    :goto_b
    move-object v1, v1

    .line 2527187
    goto/16 :goto_2

    .line 2527188
    :cond_d
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2527189
    invoke-static {v0, p1}, LX/I10;->r(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    .line 2527190
    :cond_e
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {p1}, LX/7oa;->Y()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    .line 2527191
    :cond_f
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2527192
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0176

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v1, v2, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    .line 2527193
    :cond_10
    invoke-static {v0, p1}, LX/I10;->o(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    .line 2527194
    :cond_11
    invoke-static {v0, p1}, LX/I10;->q(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    .line 2527195
    :cond_12
    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2527196
    invoke-static {v0, p1}, LX/I10;->r(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    .line 2527197
    :cond_13
    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2527198
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    .line 2527199
    :cond_14
    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {p1}, LX/7oa;->Y()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    .line 2527200
    invoke-static {v0, p1}, LX/I10;->o(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    .line 2527201
    :cond_15
    invoke-static {p1}, LX/I10;->v(LX/7oa;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2527202
    iget-object v1, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    .line 2527203
    :cond_16
    invoke-static {v0, p1}, LX/I10;->q(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    .line 2527204
    :cond_17
    invoke-static {p1}, LX/I10;->s(LX/7oa;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 2527205
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2527206
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 2527207
    iget-object v4, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0837d9

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v8

    invoke-interface {v8}, LX/7oe;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-interface {p1}, LX/7oa;->H()LX/7od;

    move-result-object v8

    invoke-interface {v8}, LX/7od;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x2

    invoke-interface {p1}, LX/7oa;->D()LX/7oc;

    move-result-object v9

    invoke-interface {v9}, LX/7oc;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2527208
    :goto_c
    move-object v4, v4

    .line 2527209
    iput-object v4, v2, LX/I12;->b:Ljava/lang/String;

    .line 2527210
    move-object v4, v2

    .line 2527211
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v6

    if-nez v6, :cond_18

    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v6, v7, :cond_1f

    .line 2527212
    :cond_18
    const v6, 0x7f0a0092

    .line 2527213
    :goto_d
    move v6, v6

    .line 2527214
    iput v6, v4, LX/I12;->c:I

    .line 2527215
    move-object v4, v4

    .line 2527216
    const/4 v8, -0x1

    .line 2527217
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 2527218
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0208d6

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 2527219
    :goto_e
    move-object v6, v6

    .line 2527220
    iput-object v6, v4, LX/I12;->d:Landroid/graphics/drawable/Drawable;

    .line 2527221
    move-object v4, v4

    .line 2527222
    invoke-static {v0, p1, v1}, LX/I10;->b(LX/I10;LX/7oa;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;

    move-result-object v6

    .line 2527223
    iput-object v6, v4, LX/I12;->a:Ljava/util/List;

    .line 2527224
    invoke-virtual {v2}, LX/I12;->a()LX/I13;

    move-result-object v2

    goto/16 :goto_3

    .line 2527225
    :cond_19
    invoke-static {v0, p1}, LX/I10;->j(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v4

    .line 2527226
    iput-object v4, v2, LX/I12;->b:Ljava/lang/String;

    .line 2527227
    move-object v4, v2

    .line 2527228
    invoke-static {p1}, LX/I10;->u(LX/7oa;)Z

    move-result v6

    if-nez v6, :cond_1a

    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v6

    if-nez v6, :cond_1a

    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v6, v7, :cond_21

    .line 2527229
    :cond_1a
    const v6, 0x7f0a0092

    .line 2527230
    :goto_f
    move v6, v6

    .line 2527231
    iput v6, v4, LX/I12;->c:I

    .line 2527232
    move-object v4, v4

    .line 2527233
    const/4 v8, -0x1

    .line 2527234
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 2527235
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0208d6

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 2527236
    :goto_10
    move-object v6, v6

    .line 2527237
    iput-object v6, v4, LX/I12;->d:Landroid/graphics/drawable/Drawable;

    .line 2527238
    move-object v4, v4

    .line 2527239
    invoke-static {v0, p1, v1}, LX/I10;->b(LX/I10;LX/7oa;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;

    move-result-object v6

    .line 2527240
    iput-object v6, v4, LX/I12;->a:Ljava/util/List;

    .line 2527241
    invoke-virtual {v2}, LX/I12;->a()LX/I13;

    move-result-object v2

    goto/16 :goto_3

    .line 2527242
    :cond_1b
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 2527243
    invoke-static {v0, p1}, LX/I10;->n(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 2527244
    :cond_1c
    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v4

    if-nez v4, :cond_1d

    invoke-static {p1}, LX/I10;->y(LX/7oa;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 2527245
    iget-object v4, v0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0f0173

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v7

    invoke-interface {v7}, LX/7oe;->a()I

    move-result v7

    new-array v8, v9, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v9

    invoke-interface {v9}, LX/7oe;->a()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 2527246
    :cond_1d
    invoke-interface {p1}, LX/7oa;->Y()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1e

    .line 2527247
    invoke-static {v0, p1}, LX/I10;->o(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 2527248
    :cond_1e
    invoke-static {v0, p1}, LX/I10;->q(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    :cond_1f
    const v6, 0x7f0a008a

    goto/16 :goto_d

    .line 2527249
    :cond_20
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    .line 2527250
    sget-object v7, LX/I0z;->a:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    .line 2527251
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f020854

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_e

    .line 2527252
    :pswitch_0
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0207de

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_e

    .line 2527253
    :pswitch_1
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f02099e

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_e

    :cond_21
    const v6, 0x7f0a008a

    goto/16 :goto_f

    .line 2527254
    :cond_22
    invoke-static {p1}, LX/I10;->u(LX/7oa;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 2527255
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0209fb

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_10

    .line 2527256
    :cond_23
    invoke-static {p1}, LX/I10;->v(LX/7oa;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 2527257
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f020785

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_10

    .line 2527258
    :cond_24
    invoke-interface {p1}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v6

    .line 2527259
    sget-object v7, LX/I0z;->b:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_1

    .line 2527260
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f020854

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_10

    .line 2527261
    :pswitch_2
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0207de

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_10

    .line 2527262
    :pswitch_3
    iget-object v6, v0, LX/I10;->c:LX/0wM;

    const v7, 0x7f0209e3

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/16 :goto_10

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x25108fa6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527122
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onAttachedToWindow()V

    .line 2527123
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 2527124
    const/16 v1, 0x2d

    const v2, 0x27171d0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xa1200e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527112
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onDetachedFromWindow()V

    .line 2527113
    iget-object v1, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 2527114
    const/16 v1, 0x2d

    const v2, -0x39aa2851

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2527119
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onFinishTemporaryDetach()V

    .line 2527120
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 2527121
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2527116
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onStartTemporaryDetach()V

    .line 2527117
    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 2527118
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2527115
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowSocialContextTextView;->f:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
