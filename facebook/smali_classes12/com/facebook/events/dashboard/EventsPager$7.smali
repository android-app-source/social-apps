.class public final Lcom/facebook/events/dashboard/EventsPager$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/Hze;


# direct methods
.method public constructor <init>(LX/Hze;LX/0Px;)V
    .locals 0

    .prologue
    .line 2525795
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsPager$7;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 2525796
    const-wide v2, 0x7fffffffffffffffL

    .line 2525797
    const-wide/16 v4, 0x0

    .line 2525798
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2525799
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v7, v0

    move v1, v0

    :goto_0
    if-ge v7, v10, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsPager$7;->a:LX/0Px;

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oa;

    .line 2525800
    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v11

    .line 2525801
    invoke-virtual {v11}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v8

    .line 2525802
    cmp-long v0, v8, v2

    if-gez v0, :cond_0

    move-wide v2, v8

    .line 2525803
    :cond_0
    cmp-long v0, v8, v4

    if-lez v0, :cond_3

    .line 2525804
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move-wide v4, v8

    .line 2525805
    :goto_1
    iget-object v1, v11, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2525806
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2525807
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_1

    .line 2525808
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->d:LX/HyO;

    sget-object v8, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v1, v8}, LX/HyO;->a(LX/HyN;)V

    .line 2525809
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->b:Landroid/content/ContentResolver;

    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v8, v8, LX/Hze;->c:LX/Bky;

    invoke-static {v1, v8, v11}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V

    .line 2525810
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->d:LX/HyO;

    sget-object v8, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v1, v8}, LX/HyO;->b(LX/HyN;)V

    .line 2525811
    :goto_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v1, v0

    goto :goto_0

    .line 2525812
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->b:Landroid/content/ContentResolver;

    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v8, v8, LX/Hze;->c:LX/Bky;

    invoke-static {v1, v8, v11}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V

    goto :goto_2

    .line 2525813
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v7, p0, Lcom/facebook/events/dashboard/EventsPager$7;->a:LX/0Px;

    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsPager$7;->a:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    invoke-virtual {v7, v1, v8}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    .line 2525814
    iput-object v1, v0, LX/Hze;->p:LX/0Px;

    .line 2525815
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v0, v0, LX/Hze;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsPager$7;->b:LX/Hze;

    iget-object v1, v1, LX/Hze;->c:LX/Bky;

    invoke-static/range {v0 .. v6}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;JJLjava/util/List;)V

    .line 2525816
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method
