.class public Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Hws;
.implements LX/0o8;


# static fields
.field public static final A:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public C:Landroid/view/View;

.field public D:LX/Hx6;

.field public E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

.field public F:LX/Hz2;

.field public G:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public H:Ljava/lang/String;

.field private I:Ljava/util/GregorianCalendar;

.field private J:Ljava/text/SimpleDateFormat;

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public O:Z

.field public P:Ljava/lang/String;

.field public Q:Z

.field public R:Z

.field private S:Landroid/content/Context;

.field public T:Lcom/facebook/events/common/EventAnalyticsParams;

.field public U:LX/I3b;

.field private V:I

.field private W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public X:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field public Y:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public Z:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/2ja;

.field private ab:LX/2jY;

.field public ac:Z

.field private ad:LX/1P0;

.field private final ae:LX/Hyo;

.field private final af:LX/Hyp;

.field private final ag:LX/Hyq;

.field private final ah:LX/Hys;

.field private final ai:LX/Hyr;

.field public final aj:LX/0m9;

.field private final ak:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final al:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final am:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final an:LX/Hyh;

.field public final ao:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final ap:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final aq:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/DBC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7v8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I3c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Hz3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HyT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/I5E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/I5M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HyO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/HyA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Bie;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Hx9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2524787
    const-class v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->A:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2524772
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2524773
    new-instance v0, LX/Hyo;

    invoke-direct {v0, p0}, LX/Hyo;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ae:LX/Hyo;

    .line 2524774
    new-instance v0, LX/Hyp;

    invoke-direct {v0, p0}, LX/Hyp;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->af:LX/Hyp;

    .line 2524775
    new-instance v0, LX/Hyq;

    invoke-direct {v0, p0}, LX/Hyq;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ag:LX/Hyq;

    .line 2524776
    new-instance v0, LX/Hys;

    invoke-direct {v0, p0}, LX/Hys;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ah:LX/Hys;

    .line 2524777
    new-instance v0, LX/Hyr;

    invoke-direct {v0, p0}, LX/Hyr;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ai:LX/Hyr;

    .line 2524778
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aj:LX/0m9;

    .line 2524779
    new-instance v0, LX/Hye;

    invoke-direct {v0, p0}, LX/Hye;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ak:LX/0TF;

    .line 2524780
    new-instance v0, LX/Hyf;

    invoke-direct {v0, p0}, LX/Hyf;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->al:LX/0TF;

    .line 2524781
    new-instance v0, LX/Hyg;

    invoke-direct {v0, p0}, LX/Hyg;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->am:LX/0Vd;

    .line 2524782
    new-instance v0, LX/Hyi;

    invoke-direct {v0, p0}, LX/Hyi;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->an:LX/Hyh;

    .line 2524783
    new-instance v0, LX/Hyj;

    invoke-direct {v0, p0}, LX/Hyj;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ao:LX/0Vd;

    .line 2524784
    new-instance v0, LX/Hyk;

    invoke-direct {v0, p0}, LX/Hyk;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ap:LX/0Vd;

    .line 2524785
    new-instance v0, LX/Hyl;

    invoke-direct {v0, p0}, LX/Hyl;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aq:LX/0Vd;

    .line 2524786
    return-void
.end method

.method public static A(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2524788
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2524789
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v3, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v2, v3, :cond_2

    .line 2524790
    :cond_0
    :goto_0
    move v0, v0

    .line 2524791
    if-eqz v0, :cond_1

    .line 2524792
    iput-boolean v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    .line 2524793
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->O:Z

    .line 2524794
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->b(Z)V

    .line 2524795
    invoke-static {p0, v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V

    .line 2524796
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b()V

    .line 2524797
    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    .line 2524798
    sget-object v4, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v3, v4, :cond_3

    sget-object v4, LX/Hx6;->PAST:LX/Hx6;

    if-ne v3, v4, :cond_3

    iget-object v4, v2, LX/Hyx;->b:Ljava/lang/String;

    if-nez v4, :cond_4

    :cond_3
    sget-object v4, LX/Hx6;->UPCOMING:LX/Hx6;

    if-eq v3, v4, :cond_5

    sget-object v4, LX/Hx6;->PAST:LX/Hx6;

    if-eq v3, v4, :cond_5

    iget-object v4, v2, LX/Hyx;->a:Ljava/lang/String;

    if-eqz v4, :cond_5

    :cond_4
    const/4 v4, 0x1

    :goto_1
    move v2, v4

    .line 2524799
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLastVisiblePosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-le v2, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static D(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 7

    .prologue
    .line 2524800
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    new-instance v2, LX/HyY;

    invoke-direct {v2, p0}, LX/HyY;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    const/4 v6, 0x1

    .line 2524801
    if-eqz v2, :cond_0

    sget-object v3, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v1, v3, :cond_0

    iget-object v3, v0, LX/Hyx;->o:LX/0ad;

    sget-short v4, LX/347;->l:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2524802
    :cond_0
    :goto_0
    return-void

    .line 2524803
    :cond_1
    invoke-static {}, LX/7oV;->j()LX/7oM;

    move-result-object v3

    .line 2524804
    const-string v4, "event_prompt_styles"

    const-string v5, "BANNER_IMAGE"

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2524805
    const-string v4, "event_prompt_surface"

    .line 2524806
    sget-object v5, LX/Hyw;->a:[I

    invoke-virtual {v1}, LX/Hx6;->ordinal()I

    move-result p0

    aget v5, v5, p0

    packed-switch v5, :pswitch_data_0

    .line 2524807
    const/4 v5, 0x0

    :goto_1
    move-object v5, v5

    .line 2524808
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2524809
    const-string v4, "theme_height"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2524810
    const-string v4, "theme_width"

    iget-object v5, v0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2524811
    const-string v4, "prompt_height"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2524812
    const-string v4, "prompt_width"

    iget-object v5, v0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2524813
    invoke-static {}, LX/7oV;->j()LX/7oM;

    move-result-object v4

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2524814
    iget-object v5, v3, LX/0gW;->e:LX/0w7;

    move-object v3, v5

    .line 2524815
    invoke-virtual {v4, v3}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2524816
    iget-object v3, v0, LX/Hyx;->l:LX/0tX;

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2524817
    new-instance v4, LX/Hyv;

    invoke-direct {v4, v0, v2}, LX/Hyv;-><init>(LX/Hyx;LX/HyY;)V

    .line 2524818
    iget-object v5, v0, LX/Hyx;->q:LX/1Ck;

    sget-object v6, LX/Hy9;->FETCH_PROMPTS:LX/Hy9;

    invoke-virtual {v5, v6, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2524819
    :pswitch_0
    const-string v5, "BIRTHDAYS_DASHBOARD"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public static H(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 3

    .prologue
    .line 2524820
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2524821
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment$19;

    invoke-direct {v1, p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment$19;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a:LX/0TD;

    invoke-interface {v0, v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2524822
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/location/ImmutableLocation;)LX/0m9;
    .locals 6

    .prologue
    .line 2524823
    invoke-virtual {p0}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 2524824
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2524825
    const-string v2, "latitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2524826
    const-string v2, "longitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2524827
    return-object v1
.end method

.method private a(Landroid/os/Bundle;Landroid/content/Intent;)Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v10, 0x6

    const/4 v6, 0x0

    .line 2524828
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 2524829
    if-eqz p2, :cond_6

    .line 2524830
    const-string v0, "birthday_view_referrer_param"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2524831
    const-string v0, "birthday_view_start_date"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2524832
    :goto_0
    :try_start_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2524833
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->J:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2524834
    :goto_1
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2524835
    new-instance v8, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->I:Ljava/util/GregorianCalendar;

    .line 2524836
    cmp-long v4, v2, v4

    if-eqz v4, :cond_5

    .line 2524837
    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->I:Ljava/util/GregorianCalendar;

    invoke-virtual {v4, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 2524838
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->I:Ljava/util/GregorianCalendar;

    invoke-virtual {v2, v10}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v10}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    if-eq v2, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2524839
    :goto_3
    if-eqz p1, :cond_0

    const-string v2, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 2524840
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c:LX/7v8;

    if-nez v1, :cond_1

    const-string v1, ""

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/7v8;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 2524841
    :goto_4
    return-object v0

    .line 2524842
    :catch_0
    move-exception v0

    .line 2524843
    const-class v2, LX/I0B;

    const-string v3, "Invalid date format"

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    move-wide v2, v4

    goto :goto_1

    :cond_3
    move v0, v6

    .line 2524844
    goto :goto_2

    .line 2524845
    :cond_4
    const-string v0, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    move-object v0, v7

    goto :goto_3

    :cond_6
    move-object v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;LX/0TD;LX/DBC;LX/7v8;LX/I3c;LX/Hyx;LX/Hz3;LX/HyT;LX/I5E;LX/I5M;LX/HyO;LX/Bl6;LX/1nQ;LX/HyA;LX/Bie;LX/Hx9;LX/03V;LX/0zG;LX/0Uh;LX/0tX;LX/0Or;LX/Cfr;LX/3Fx;LX/1Ck;LX/0So;LX/0ad;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;",
            "LX/0TD;",
            "LX/DBC;",
            "LX/7v8;",
            "LX/I3c;",
            "LX/Hyx;",
            "LX/Hz3;",
            "LX/HyT;",
            "LX/I5E;",
            "LX/I5M;",
            "LX/HyO;",
            "LX/Bl6;",
            "LX/1nQ;",
            "LX/HyA;",
            "LX/Bie;",
            "LX/Hx9;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0zG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/Cfr;",
            "LX/3Fx;",
            "LX/1Ck;",
            "LX/0So;",
            "LX/0ad;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2524846
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a:LX/0TD;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b:LX/DBC;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c:LX/7v8;

    iput-object p4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->d:LX/I3c;

    iput-object p5, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iput-object p6, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->f:LX/Hz3;

    iput-object p7, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->g:LX/HyT;

    iput-object p8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->h:LX/I5E;

    iput-object p9, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->i:LX/I5M;

    iput-object p10, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    iput-object p11, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iput-object p12, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->l:LX/1nQ;

    iput-object p13, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->m:LX/HyA;

    iput-object p14, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->n:LX/Bie;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->o:LX/Hx9;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->p:LX/03V;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->q:LX/0zG;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->r:LX/0Uh;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->s:LX/0tX;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->t:LX/0Or;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->u:LX/Cfr;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->v:LX/3Fx;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w:LX/1Ck;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->x:LX/0So;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->y:LX/0ad;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->z:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V
    .locals 2

    .prologue
    .line 2524938
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2524939
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b(Z)V

    .line 2524940
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 29

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-static/range {v28 .. v28}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static/range {v28 .. v28}, LX/DBC;->a(LX/0QB;)LX/DBC;

    move-result-object v4

    check-cast v4, LX/DBC;

    invoke-static/range {v28 .. v28}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v5

    check-cast v5, LX/7v8;

    const-class v6, LX/I3c;

    move-object/from16 v0, v28

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/I3c;

    invoke-static/range {v28 .. v28}, LX/Hyx;->a(LX/0QB;)LX/Hyx;

    move-result-object v7

    check-cast v7, LX/Hyx;

    const-class v8, LX/Hz3;

    move-object/from16 v0, v28

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Hz3;

    invoke-static/range {v28 .. v28}, LX/HyT;->a(LX/0QB;)LX/HyT;

    move-result-object v9

    check-cast v9, LX/HyT;

    invoke-static/range {v28 .. v28}, LX/I5E;->a(LX/0QB;)LX/I5E;

    move-result-object v10

    check-cast v10, LX/I5E;

    invoke-static/range {v28 .. v28}, LX/I5M;->a(LX/0QB;)LX/I5M;

    move-result-object v11

    check-cast v11, LX/I5M;

    invoke-static/range {v28 .. v28}, LX/HyO;->a(LX/0QB;)LX/HyO;

    move-result-object v12

    check-cast v12, LX/HyO;

    invoke-static/range {v28 .. v28}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v13

    check-cast v13, LX/Bl6;

    invoke-static/range {v28 .. v28}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v14

    check-cast v14, LX/1nQ;

    invoke-static/range {v28 .. v28}, LX/HyA;->a(LX/0QB;)LX/HyA;

    move-result-object v15

    check-cast v15, LX/HyA;

    invoke-static/range {v28 .. v28}, LX/Bie;->a(LX/0QB;)LX/Bie;

    move-result-object v16

    check-cast v16, LX/Bie;

    invoke-static/range {v28 .. v28}, LX/Hx9;->a(LX/0QB;)LX/Hx9;

    move-result-object v17

    check-cast v17, LX/Hx9;

    invoke-static/range {v28 .. v28}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    invoke-static/range {v28 .. v28}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v19

    check-cast v19, LX/0zG;

    invoke-static/range {v28 .. v28}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v20

    check-cast v20, LX/0Uh;

    invoke-static/range {v28 .. v28}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v21

    check-cast v21, LX/0tX;

    const/16 v22, 0x122d

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    const-class v23, LX/Cfr;

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/Cfr;

    invoke-static/range {v28 .. v28}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v24

    check-cast v24, LX/3Fx;

    invoke-static/range {v28 .. v28}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v25

    check-cast v25, LX/1Ck;

    invoke-static/range {v28 .. v28}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v26

    check-cast v26, LX/0So;

    invoke-static/range {v28 .. v28}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v27

    check-cast v27, LX/0ad;

    invoke-static/range {v28 .. v28}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v28

    check-cast v28, Ljava/lang/Boolean;

    invoke-static/range {v2 .. v28}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;LX/0TD;LX/DBC;LX/7v8;LX/I3c;LX/Hyx;LX/Hz3;LX/HyT;LX/I5E;LX/I5M;LX/HyO;LX/Bl6;LX/1nQ;LX/HyA;LX/Bie;LX/Hx9;LX/03V;LX/0zG;LX/0Uh;LX/0tX;LX/0Or;LX/Cfr;LX/3Fx;LX/1Ck;LX/0So;LX/0ad;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Landroid/database/Cursor;)V
    .locals 13

    .prologue
    const/4 v3, 0x1

    .line 2524847
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v1, LX/HyN;->DB_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2524848
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->o:LX/Hx9;

    invoke-virtual {v0}, LX/Hx9;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2524849
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->g:LX/HyT;

    const/16 v1, 0xe

    invoke-virtual {v0, p1, v1}, LX/HyT;->a(Landroid/database/Cursor;I)LX/0Px;

    move-result-object v0

    .line 2524850
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 2524851
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->o:LX/Hx9;

    invoke-virtual {v1}, LX/Hx9;->b()V

    .line 2524852
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->L:Z

    if-eqz v1, :cond_2

    .line 2524853
    iput-boolean v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->K:Z

    .line 2524854
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v1, v0}, LX/Hz2;->a(LX/0Px;)V

    .line 2524855
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->al:LX/0TF;

    .line 2524856
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v12

    const/4 v4, 0x0

    move v11, v4

    :goto_0
    if-ge v11, v12, :cond_3

    invoke-virtual {v0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/model/Event;

    .line 2524857
    invoke-static {v4}, LX/Bm1;->a(Lcom/facebook/events/model/Event;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v6

    .line 2524858
    new-instance v5, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v7, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    iget-object v8, v1, LX/Hyx;->h:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 2524859
    iget-object v10, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2524860
    invoke-static {v10}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    invoke-direct/range {v5 .. v10}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2524861
    iget-object v6, v1, LX/Hyx;->m:LX/1My;

    .line 2524862
    iget-object v7, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v7

    .line 2524863
    invoke-virtual {v6, v2, v4, v5}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2524864
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto :goto_0

    .line 2524865
    :cond_3
    iput-boolean v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->O:Z

    .line 2524866
    const-string v0, "warm"

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->P:Ljava/lang/String;

    .line 2524867
    :goto_1
    return-void

    .line 2524868
    :cond_4
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p0    # Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2524869
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v1, LX/HyN;->NETWORK_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2524870
    iput-boolean v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->O:Z

    .line 2524871
    const-string v0, "cold"

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->P:Ljava/lang/String;

    .line 2524872
    if-eqz p1, :cond_0

    .line 2524873
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524874
    if-eqz v0, :cond_0

    .line 2524875
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524876
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2524877
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v2}, LX/Hz2;->c(Z)V

    .line 2524878
    :goto_0
    return-void

    .line 2524879
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524880
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2524881
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2524882
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2524883
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->o:LX/Hx9;

    invoke-virtual {v2}, LX/Hx9;->a()V

    .line 2524884
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2524885
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2524886
    iput-object v0, v2, LX/Hyx;->a:Ljava/lang/String;

    .line 2524887
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v2, :cond_6

    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->R:Z

    if-nez v0, :cond_6

    .line 2524888
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/Hz2;->b(Z)V

    .line 2524889
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    if-eqz v0, :cond_4

    .line 2524890
    iput-boolean v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    .line 2524891
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v3}, LX/Hz2;->b(Z)V

    .line 2524892
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->c(LX/0Px;)V

    .line 2524893
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0, v1}, LX/Hyx;->a(LX/0Px;)V

    goto :goto_0

    .line 2524894
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->K:Z

    if-eqz v0, :cond_5

    .line 2524895
    iput-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->N:LX/0Px;

    goto :goto_2

    .line 2524896
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->a(LX/0Px;)V

    goto :goto_2

    .line 2524897
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/Hz2;->b(Z)V

    goto :goto_1
.end method

.method private b(Z)V
    .locals 12

    .prologue
    .line 2524898
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v1, :cond_0

    .line 2524899
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->V:I

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->W:Ljava/util/List;

    .line 2524900
    const/16 v8, 0xe

    const/4 v11, 0x0

    move-object v6, v0

    move v7, p1

    move v9, v1

    move-object v10, v2

    invoke-virtual/range {v6 .. v11}, LX/Hyx;->a(ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2524901
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2524902
    :goto_0
    return-void

    .line 2524903
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    const/16 v3, 0xe

    iget v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->V:I

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->W:Ljava/util/List;

    move v2, p1

    invoke-virtual/range {v0 .. v5}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2524904
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v1, LX/HyN;->NETWORK_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->a(LX/HyN;)V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p0    # Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2524905
    iput-boolean v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->O:Z

    .line 2524906
    const-string v0, "cold"

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->P:Ljava/lang/String;

    .line 2524907
    if-eqz p1, :cond_0

    .line 2524908
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524909
    if-eqz v0, :cond_0

    .line 2524910
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524911
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2524912
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->c(Z)V

    .line 2524913
    :cond_1
    :goto_0
    return-void

    .line 2524914
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524915
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2524916
    if-eqz v0, :cond_1

    .line 2524917
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2524918
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2524919
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2524920
    iput-object v0, v2, LX/Hyx;->b:Ljava/lang/String;

    .line 2524921
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    if-eqz v0, :cond_4

    .line 2524922
    iput-boolean v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    .line 2524923
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v3}, LX/Hz2;->b(Z)V

    .line 2524924
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->c(LX/0Px;)V

    .line 2524925
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    invoke-virtual {v0, v1}, LX/Hyx;->a(LX/0Px;)V

    goto :goto_0

    .line 2524926
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, LX/Hz2;->a(LX/0Px;)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V
    .locals 2

    .prologue
    .line 2524927
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2524928
    :goto_0
    return-void

    .line 2524929
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->an:LX/Hyh;

    invoke-virtual {v0, p1, v1}, LX/Hyx;->a(ZLX/Hyh;)V

    goto :goto_0
.end method

.method public static u$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 2

    .prologue
    .line 2524930
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2524931
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->g:LX/HyT;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    invoke-virtual {v0, v1}, LX/HyT;->a(LX/Hx6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2524932
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b(Z)V

    .line 2524933
    :cond_0
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2524934
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2524935
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2524936
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2524937
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2524724
    const-string v0, "event_dashboard"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2524725
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2524726
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2524727
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2524728
    if-eqz p1, :cond_4

    const-string v1, "extra_dashboard_filter_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2524729
    const-string v0, "extra_dashboard_filter_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx6;->valueOf(Ljava/lang/String;)LX/Hx6;

    move-result-object v0

    .line 2524730
    :goto_0
    move-object v0, v0

    .line 2524731
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    .line 2524732
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2524733
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->m:LX/HyA;

    invoke-virtual {v0}, LX/HyA;->a()V

    .line 2524734
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    invoke-virtual {v0}, LX/HyO;->a()V

    .line 2524735
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->r:LX/0Uh;

    const/16 v1, 0x3a1

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->M:Z

    .line 2524736
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524737
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2524738
    if-eqz v1, :cond_1

    const-string v2, "action_ref"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/common/ActionSource;

    if-nez v2, :cond_6

    .line 2524739
    :cond_1
    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2524740
    :goto_1
    move-object v1, v2

    .line 2524741
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a()Ljava/lang/String;

    move-result-object v3

    .line 2524742
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2524743
    const-string v5, "tracking_codes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->T:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524744
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ak:LX/0TF;

    .line 2524745
    iput-object v1, v0, LX/Hyx;->c:LX/0TF;

    .line 2524746
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->V:I

    .line 2524747
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "PUBLISHED"

    aput-object v1, v0, v6

    const-string v1, "DRAFT"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string v2, "SCHEDULED_DRAFT_FOR_PUBLICATION"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CANCELED"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->W:Ljava/util/List;

    .line 2524748
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->u$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524749
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->J:Ljava/text/SimpleDateFormat;

    .line 2524750
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Landroid/os/Bundle;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H:Ljava/lang/String;

    .line 2524751
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b:LX/DBC;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->T:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524752
    iput-object v1, v0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524753
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c()V

    .line 2524754
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ae:LX/Hyo;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2524755
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->af:LX/Hyp;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2524756
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ag:LX/Hyq;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2524757
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ah:LX/Hys;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2524758
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ai:LX/Hyr;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2524759
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->S:Landroid/content/Context;

    .line 2524760
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2524761
    iput-boolean v7, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->R:Z

    .line 2524762
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    invoke-virtual {v0, v1}, LX/I5M;->a(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ab:LX/2jY;

    .line 2524763
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->u:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ab:LX/2jY;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    .line 2524764
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->f:LX/Hz3;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->S:Landroid/content/Context;

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->T:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v6, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ab:LX/2jY;

    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    move-object v5, p0

    invoke-virtual/range {v0 .. v8}, LX/Hz3;->a(LX/Hx6;Ljava/lang/Boolean;Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;LX/2jY;Ljava/lang/String;LX/2ja;)LX/Hz2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524765
    return-void

    .line 2524766
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->r:LX/0Uh;

    const/16 v1, 0x3a3

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2524767
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->h:LX/I5E;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aq:LX/0Vd;

    sget-object v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->A:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/I5E;->a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2

    .line 2524768
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->i:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->am:LX/0Vd;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aj:LX/0m9;

    const-string v4, "event_discovery_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    goto :goto_2

    .line 2524769
    :cond_4
    if-eqz v0, :cond_5

    const-string v1, "extra_dashboard_filter_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2524770
    const-string v1, "extra_dashboard_filter_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx6;->valueOf(Ljava/lang/String;)LX/Hx6;

    move-result-object v0

    goto/16 :goto_0

    .line 2524771
    :cond_5
    sget-object v0, LX/Hx6;->UPCOMING:LX/Hx6;

    goto/16 :goto_0

    :cond_6
    new-instance v3, Lcom/facebook/events/common/EventActionContext;

    sget-object v4, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v2, v5}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    move-object v2, v3

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2524573
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2524581
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2524582
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2524583
    const/4 v4, 0x0

    .line 2524584
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v1, :cond_3

    .line 2524585
    :cond_0
    :goto_0
    const/4 v4, 0x0

    .line 2524586
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    if-eq v0, v1, :cond_5

    .line 2524587
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2524588
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 2524589
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2524590
    :cond_2
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2524591
    return-void

    .line 2524592
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2524593
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524594
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2524595
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v1, LX/HyN;->DB_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2524596
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524597
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2524598
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2524599
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/HyZ;

    invoke-direct {v3, p0}, LX/HyZ;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2524600
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2524601
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2524602
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524603
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2524604
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524605
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2524606
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2524607
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Hya;

    invoke-direct {v3, p0}, LX/Hya;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2524608
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Z:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_1

    .line 2524609
    :catch_0
    move-exception v0

    .line 2524610
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->p:LX/03V;

    const-class v2, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2524611
    :cond_7
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2524612
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w:LX/1Ck;

    sget-object v1, LX/Hyt;->DB_FETCH:LX/Hyt;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->X:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Hyb;

    invoke-direct {v3, p0}, LX/Hyb;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_2
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2524613
    invoke-static {}, LX/7oV;->a()LX/7oJ;

    move-result-object v0

    .line 2524614
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2524615
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->s:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2524616
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->w:LX/1Ck;

    sget-object v2, LX/Hyt;->FETCH_EVENT_COUNTS:LX/Hyt;

    new-instance v3, LX/Hyd;

    invoke-direct {v3, p0}, LX/Hyd;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2524617
    return-void
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2524618
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2524619
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2524620
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2524621
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ab:LX/2jY;

    .line 2524622
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2524623
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2524624
    const-string v0, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2524625
    sparse-switch p1, :sswitch_data_0

    .line 2524626
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2524627
    :cond_1
    :goto_1
    return-void

    .line 2524628
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b:LX/DBC;

    invoke-virtual {v0, p1, p2, p3}, LX/DBC;->a(IILandroid/content/Intent;)V

    goto :goto_1

    .line 2524629
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2524630
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2524631
    if-eqz v0, :cond_1

    .line 2524632
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-wide v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2524633
    if-nez v0, :cond_2

    .line 2524634
    :goto_2
    goto :goto_1

    .line 2524635
    :sswitch_2
    if-nez p2, :cond_0

    .line 2524636
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1556604017999292"

    .line 2524637
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2524638
    move-object v0, v0

    .line 2524639
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 2524640
    :cond_2
    iget-object v2, v1, LX/Hz2;->j:LX/I2Z;

    .line 2524641
    iget-object v3, v2, LX/I2Z;->j:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2524642
    iget-object p0, v2, LX/I2Z;->f:Ljava/util/List;

    iget-object v3, v2, LX/I2Z;->j:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HyQ;

    const/4 p0, 0x1

    .line 2524643
    iput-boolean p0, v3, LX/HyQ;->c:Z

    .line 2524644
    :cond_3
    invoke-static {v1}, LX/Hz2;->m(LX/Hz2;)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x6a -> :sswitch_2
        0x1f5 -> :sswitch_0
        0x6dc -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x47752acb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524645
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v2, LX/HyN;->CREATE_VIEW:LX/HyN;

    invoke-virtual {v1, v2}, LX/HyO;->a(LX/HyN;)V

    .line 2524646
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->S:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030567

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x57259918

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x46b2c888

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524647
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->m:LX/HyA;

    invoke-virtual {v1}, LX/HyA;->b()V

    .line 2524648
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ae:LX/Hyo;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2524649
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->af:LX/Hyp;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2524650
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ag:LX/Hyq;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2524651
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ah:LX/Hys;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2524652
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ai:LX/Hyr;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2524653
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    if-eqz v1, :cond_0

    .line 2524654
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    invoke-virtual {v1}, LX/2ja;->e()V

    .line 2524655
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    if-eqz v1, :cond_1

    .line 2524656
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v1}, LX/Hz2;->f()V

    .line 2524657
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->U:LX/I3b;

    if-eqz v1, :cond_2

    .line 2524658
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->U:LX/I3b;

    .line 2524659
    iget-object v2, v1, LX/I3b;->d:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2524660
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2524661
    const/16 v1, 0x2b

    const v2, 0x348abdb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6c6e9298

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524662
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2524663
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    if-eqz v1, :cond_0

    .line 2524664
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524665
    iget-object v2, v1, LX/Hz2;->o:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 2524666
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    if-eqz v1, :cond_1

    .line 2524667
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->x:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2ja;->d(J)V

    .line 2524668
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x36bb544b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x641901e1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524669
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2524670
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    if-eqz v1, :cond_0

    .line 2524671
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524672
    iget-object v2, v1, LX/Hz2;->o:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 2524673
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    if-eqz v1, :cond_1

    .line 2524674
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->x:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2ja;->e(J)V

    .line 2524675
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x77bae2fa

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2524676
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2524677
    const-string v0, "extra_dashboard_filter_type"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524678
    const-string v0, "birthday_view_waterfall_id_param"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524679
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x528e1a97

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2524574
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2524575
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2524576
    if-nez v1, :cond_1

    .line 2524577
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x291c851f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2524578
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->q:LX/0zG;

    invoke-interface {v1}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2524579
    if-eqz v1, :cond_0

    .line 2524580
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f082193

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2524680
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2524681
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->y:LX/0ad;

    sget-short v1, LX/347;->d:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2524682
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->d:LX/I3c;

    new-instance v1, LX/Hym;

    invoke-direct {v1, p0}, LX/Hym;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524683
    new-instance p2, LX/I3b;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-direct {p2, v2, v3, p1, v1}, LX/I3b;-><init>(LX/0tX;LX/0hB;LX/1Ck;LX/Hym;)V

    .line 2524684
    move-object v0, p2

    .line 2524685
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->U:LX/I3b;

    .line 2524686
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->U:LX/I3b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2524687
    new-instance v2, LX/I3Z;

    invoke-direct {v2, v0, v1}, LX/I3Z;-><init>(LX/I3b;Landroid/content/Context;)V

    .line 2524688
    new-instance v3, LX/I3a;

    invoke-direct {v3, v0}, LX/I3a;-><init>(LX/I3b;)V

    .line 2524689
    iget-object p1, v0, LX/I3b;->d:LX/1Ck;

    const-string p2, "event_collections_data_fetch"

    invoke-virtual {p1, p2, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2524690
    :cond_0
    const v0, 0x7f0d0f0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2524691
    new-instance v0, LX/1P0;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->S:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ad:LX/1P0;

    .line 2524692
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ad:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2524693
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2524694
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2524695
    new-instance v1, LX/HyX;

    invoke-direct {v1, p0}, LX/HyX;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    move-object v1, v1

    .line 2524696
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2524697
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->aa:LX/2ja;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->ad:LX/1P0;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->x:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2ja;->a(LX/1P1;JZ)V

    .line 2524698
    const v0, 0x7f0d0f0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    .line 2524699
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setShouldShowBirthdays(Z)V

    .line 2524700
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    new-instance v1, LX/HyW;

    invoke-direct {v1, p0}, LX/HyW;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524701
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->k:LX/Hww;

    .line 2524702
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    invoke-virtual {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setDashboardFilterType(LX/Hx6;)V

    .line 2524703
    const v0, 0x7f0d0efe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2524704
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0a00d1

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2524705
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2524706
    new-instance v1, LX/HyU;

    invoke-direct {v1, p0}, LX/HyU;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    move-object v1, v1

    .line 2524707
    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2524708
    const v0, 0x7f0d08df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->C:Landroid/view/View;

    .line 2524709
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->C:Landroid/view/View;

    new-instance v1, LX/HyV;

    invoke-direct {v1, p0}, LX/HyV;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2524710
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->C:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2524711
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_1

    .line 2524712
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->B:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2524713
    new-instance v1, LX/Hyn;

    invoke-direct {v1, p0}, LX/Hyn;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    move-object v1, v1

    .line 2524714
    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2524715
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->j:LX/HyO;

    sget-object v1, LX/HyN;->CREATE_VIEW:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2524716
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v0, v1, :cond_3

    .line 2524717
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Hz2;->b(Z)V

    .line 2524718
    invoke-static {p0, v4}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->c(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;Z)V

    .line 2524719
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->M:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D:LX/Hx6;

    invoke-virtual {v0, v1}, LX/Hz2;->d(LX/Hx6;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2524720
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->D(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V

    .line 2524721
    :cond_2
    return-void

    .line 2524722
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->b()V

    goto :goto_1

    .line 2524723
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
