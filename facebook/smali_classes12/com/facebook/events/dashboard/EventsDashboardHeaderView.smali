.class public Lcom/facebook/events/dashboard/EventsDashboardHeaderView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/HxQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Bie;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HyA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

.field private e:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public f:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2523510
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2523511
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a()V

    .line 2523512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2523513
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2523514
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a()V

    .line 2523515
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2523516
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2523517
    const v0, 0x7f030558

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2523518
    const v0, 0x7f0d0efa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->d:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    .line 2523519
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->d:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a:LX/HxQ;

    .line 2523520
    iget-object v2, v1, LX/HxQ;->c:LX/Hx6;

    move-object v1, v2

    .line 2523521
    invoke-virtual {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->setDashboardFilterType(LX/Hx6;)V

    .line 2523522
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->d:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    new-instance v1, LX/Hy2;

    invoke-direct {v1, p0}, LX/Hy2;-><init>(Lcom/facebook/events/dashboard/EventsDashboardHeaderView;)V

    .line 2523523
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->l:LX/HxU;

    .line 2523524
    const v0, 0x7f0d0efb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->e:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2523525
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->e:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v1, LX/Hy3;

    invoke-direct {v1, p0}, LX/Hy3;-><init>(Lcom/facebook/events/dashboard/EventsDashboardHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523526
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardHeaderView;LX/HxQ;LX/Bie;LX/HyA;)V
    .locals 0

    .prologue
    .line 2523527
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a:LX/HxQ;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->b:LX/Bie;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->c:LX/HyA;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;

    invoke-static {v2}, LX/HxQ;->a(LX/0QB;)LX/HxQ;

    move-result-object v0

    check-cast v0, LX/HxQ;

    invoke-static {v2}, LX/Bie;->b(LX/0QB;)LX/Bie;

    move-result-object v1

    check-cast v1, LX/Bie;

    invoke-static {v2}, LX/HyA;->a(LX/0QB;)LX/HyA;

    move-result-object v2

    check-cast v2, LX/HyA;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a(Lcom/facebook/events/dashboard/EventsDashboardHeaderView;LX/HxQ;LX/Bie;LX/HyA;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0QR;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1
    .param p1    # LX/0QR;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QR",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;",
            ">;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2523528
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->d:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    .line 2523529
    iput-object p1, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->m:LX/0QR;

    .line 2523530
    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523531
    return-void
.end method
