.class public Lcom/facebook/events/dashboard/EventProfilePictureView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/1aX;

.field private e:Landroid/text/style/MetricAffectingSpan;

.field private f:Landroid/text/style/MetricAffectingSpan;

.field private g:Landroid/net/Uri;

.field private h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2522131
    const-class v0, Lcom/facebook/events/dashboard/EventProfilePictureView;

    const-string v1, "event_profile_pic"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/EventProfilePictureView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2522132
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2522133
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a()V

    .line 2522134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2522135
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2522136
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a()V

    .line 2522137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2522138
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2522139
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a()V

    .line 2522140
    return-void
.end method

.method private a(ILX/0xr;)LX/Hx8;
    .locals 4

    .prologue
    .line 2522141
    new-instance v0, LX/Hx8;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v1, v2, p2, v3}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, LX/Hx8;-><init>(Landroid/graphics/Typeface;II)V

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2522145
    const-class v0, Lcom/facebook/events/dashboard/EventProfilePictureView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2522146
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2522147
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2522148
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b15e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2522149
    sget-object v3, LX/0xr;->MEDIUM:LX/0xr;

    invoke-direct {p0, v1, v3}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(ILX/0xr;)LX/Hx8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->e:Landroid/text/style/MetricAffectingSpan;

    .line 2522150
    sget-object v1, LX/0xr;->REGULAR:LX/0xr;

    invoke-direct {p0, v2, v1}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(ILX/0xr;)LX/Hx8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->f:Landroid/text/style/MetricAffectingSpan;

    .line 2522151
    const v1, 0x7f0a06cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2522152
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2522153
    iput-object v1, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2522154
    move-object v0, v2

    .line 2522155
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020659

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2522156
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    .line 2522157
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V

    .line 2522158
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 2522142
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 2522143
    invoke-direct {p0, p2}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setStartDate(Ljava/util/Date;)V

    .line 2522144
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventProfilePictureView;LX/0Or;LX/6RZ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/dashboard/EventProfilePictureView;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/6RZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2522159
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->b:LX/6RZ;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/EventProfilePictureView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    invoke-static {p0, v1, v0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(Lcom/facebook/events/dashboard/EventProfilePictureView;LX/0Or;LX/6RZ;)V

    return-void
.end method

.method private setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 2522093
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getPaddingLeft()I

    move-result v0

    .line 2522094
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getPaddingTop()I

    move-result v1

    .line 2522095
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getPaddingRight()I

    move-result v2

    .line 2522096
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventProfilePictureView;->getPaddingBottom()I

    move-result v3

    .line 2522097
    invoke-virtual {p0, p1}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2522098
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setPadding(IIII)V

    .line 2522099
    return-void
.end method

.method private setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2522126
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->g:Landroid/net/Uri;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2522127
    :goto_0
    return-void

    .line 2522128
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->g:Landroid/net/Uri;

    .line 2522129
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/events/dashboard/EventProfilePictureView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2522130
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method private setStartDate(Ljava/util/Date;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 2522116
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->b:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2522117
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->b:LX/6RZ;

    invoke-virtual {v1, p1}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2522118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2522119
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2522120
    :goto_0
    return-void

    .line 2522121
    :cond_0
    iput-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->h:Ljava/lang/String;

    .line 2522122
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2522123
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->e:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2522124
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->f:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2522125
    invoke-virtual {p0, v2}, Lcom/facebook/events/dashboard/EventProfilePictureView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2522113
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 2522114
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(Landroid/net/Uri;Ljava/util/Date;)V

    .line 2522115
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6b156d0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522110
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onAttachedToWindow()V

    .line 2522111
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2522112
    const/16 v1, 0x2d

    const v2, -0x409532ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xc2c0190

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522107
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onDetachedFromWindow()V

    .line 2522108
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2522109
    const/16 v1, 0x2d

    const v2, 0x28d919df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2522104
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onFinishTemporaryDetach()V

    .line 2522105
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2522106
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2522101
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onStartTemporaryDetach()V

    .line 2522102
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2522103
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2522100
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventProfilePictureView;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
