.class public Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HyT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hz3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public j:LX/Hz2;

.field private k:Lcom/facebook/events/common/EventAnalyticsParams;

.field private l:I

.field public m:Z

.field public n:Z

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r:LX/Hz9;

.field private final s:LX/HzA;

.field private final t:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final u:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2525444
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2525445
    new-instance v0, LX/Hz9;

    invoke-direct {v0, p0}, LX/Hz9;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->r:LX/Hz9;

    .line 2525446
    new-instance v0, LX/HzA;

    invoke-direct {v0, p0}, LX/HzA;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->s:LX/HzA;

    .line 2525447
    new-instance v0, LX/Hz4;

    invoke-direct {v0, p0}, LX/Hz4;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->t:LX/0TF;

    .line 2525448
    new-instance v0, LX/Hz5;

    invoke-direct {v0, p0}, LX/Hz5;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->u:LX/0Vd;

    .line 2525449
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 2525438
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2525439
    :goto_0
    if-lez v0, :cond_1

    .line 2525440
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->b:LX/HyT;

    const/16 v2, 0xe

    invoke-virtual {v1, p1, v2}, LX/HyT;->a(Landroid/database/Cursor;I)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Hz2;->a(LX/0Px;)V

    .line 2525441
    :goto_1
    return-void

    .line 2525442
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 2525443
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d()V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2525363
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525364
    if-nez v0, :cond_1

    .line 2525365
    :cond_0
    :goto_0
    return-void

    .line 2525366
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525367
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2525368
    if-eqz v0, :cond_0

    .line 2525369
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2525370
    iget-boolean v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->n:Z

    if-eqz v2, :cond_2

    .line 2525371
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v2, v1}, LX/Hz2;->a(LX/0Px;)V

    .line 2525372
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2525373
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2525374
    iput-object v3, v2, LX/Hyx;->a:Ljava/lang/String;

    .line 2525375
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->m:Z

    .line 2525376
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    invoke-virtual {v0, v1}, LX/Hyx;->a(LX/0Px;)V

    goto :goto_0

    .line 2525377
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v2, v3}, LX/Hz2;->b(Z)V

    .line 2525378
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v2, v1}, LX/Hz2;->c(LX/0Px;)V

    goto :goto_1

    .line 2525379
    :cond_3
    iput-boolean v3, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->m:Z

    goto :goto_2
.end method

.method public static c(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2525429
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2525430
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525431
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2525432
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d()V

    .line 2525433
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525434
    :cond_0
    :goto_0
    return-void

    .line 2525435
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2525436
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->g:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_EVENTS_FOR_DISCOVERY_SURFACE:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Hz7;

    invoke-direct {v3, p0}, LX/Hz7;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2525437
    iput-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2525426
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2525427
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment$5;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->a:LX/0TD;

    invoke-interface {v0, v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2525428
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V
    .locals 6

    .prologue
    .line 2525424
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    iget-boolean v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->m:Z

    const/16 v3, 0xe

    iget v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->l:I

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->o:Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525425
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2525407
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2525408
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/HyT;->b(LX/0QB;)LX/HyT;

    move-result-object v6

    check-cast v6, LX/HyT;

    const-class v7, LX/Hz3;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Hz3;

    invoke-static {v0}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v8

    check-cast v8, LX/Hyx;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v9

    check-cast v9, LX/Bl6;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v10

    check-cast v10, LX/0zG;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v4, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->a:LX/0TD;

    iput-object v6, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->b:LX/HyT;

    iput-object v7, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->c:LX/Hz3;

    iput-object v8, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    iput-object v9, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e:LX/Bl6;

    iput-object v10, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->f:LX/0zG;

    iput-object p1, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->g:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->h:LX/0ad;

    .line 2525409
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2525410
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2525411
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2525412
    const-string v2, "event_dashboard_all_upcoming"

    move-object v2, v2

    .line 2525413
    invoke-direct {v0, v1, v7, v2, v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2525414
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->l:I

    .line 2525415
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "PUBLISHED"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "CANCELED"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->o:Ljava/util/List;

    .line 2525416
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->b:LX/HyT;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    invoke-virtual {v0, v1}, LX/HyT;->a(LX/Hx6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525417
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    .line 2525418
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->d:LX/Hyx;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->t:LX/0TF;

    .line 2525419
    iput-object v1, v0, LX/Hyx;->c:LX/0TF;

    .line 2525420
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->c:LX/Hz3;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, LX/Hz3;->a(LX/Hx6;Ljava/lang/Boolean;Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;LX/2jY;Ljava/lang/String;LX/2ja;)LX/Hz2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    .line 2525421
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->r:LX/Hz9;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525422
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->s:LX/HzA;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525423
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7515a6d2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2525406
    const v1, 0x7f030572

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x246813cd

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47ecca73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2525400
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->r:LX/Hz9;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2525401
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->e:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->s:LX/HzA;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2525402
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    if-eqz v1, :cond_0

    .line 2525403
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v1}, LX/Hz2;->f()V

    .line 2525404
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2525405
    const/16 v1, 0x2b

    const v2, -0x77713e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x20d7e2ab

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2525397
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2525398
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->f:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2525399
    const/16 v0, 0x2b

    const v2, -0x863db21

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2525380
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2525381
    const v0, 0x7f0d0f20

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525382
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2525383
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->j:LX/Hz2;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2525384
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525385
    new-instance v1, LX/Hz8;

    invoke-direct {v1, p0}, LX/Hz8;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    move-object v1, v1

    .line 2525386
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2525387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->n:Z

    .line 2525388
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->c(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    .line 2525389
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->n:Z

    if-eqz v0, :cond_1

    .line 2525390
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2525391
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 2525392
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;Landroid/database/Cursor;)V

    .line 2525393
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525394
    :cond_1
    return-void

    .line 2525395
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2525396
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->g:LX/1Ck;

    sget-object v1, LX/HzB;->DB_FETCH:LX/HzB;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p1, LX/Hz6;

    invoke-direct {p1, p0}, LX/Hz6;-><init>(Lcom/facebook/events/dashboard/EventsDiscoveryUpcomingEventsFragment;)V

    invoke-virtual {v0, v1, v2, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
