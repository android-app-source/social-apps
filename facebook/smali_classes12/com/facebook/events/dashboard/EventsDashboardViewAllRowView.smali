.class public Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hx5;

.field public c:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2524188
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2524189
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a()V

    .line 2524190
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2524191
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2524192
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a()V

    .line 2524193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2524194
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2524195
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a()V

    .line 2524196
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2524197
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2524198
    new-instance v0, LX/HyP;

    invoke-direct {v0, p0}, LX/HyP;-><init>(Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2524199
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v0

    check-cast v0, LX/1nQ;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->a:LX/1nQ;

    return-void
.end method


# virtual methods
.method public setEventsDashboardBucketAdapter(LX/Hx5;)V
    .locals 0

    .prologue
    .line 2524200
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->b:LX/Hx5;

    .line 2524201
    return-void
.end method
