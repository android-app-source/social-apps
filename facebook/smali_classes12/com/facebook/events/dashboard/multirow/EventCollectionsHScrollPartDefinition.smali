.class public Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/I2L;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

.field private final c:LX/0hB;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530171
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2530172
    iput-object p1, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->a:Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    .line 2530173
    iput-object p2, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2530174
    iput-object p3, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->c:LX/0hB;

    .line 2530175
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;
    .locals 6

    .prologue
    .line 2530160
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    monitor-enter v1

    .line 2530161
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2530162
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2530163
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530164
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2530165
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;-><init>(Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0hB;)V

    .line 2530166
    move-object v0, p0

    .line 2530167
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2530168
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530169
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2530170
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2530159
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2530154
    check-cast p2, LX/I2L;

    check-cast p3, LX/I2o;

    const/4 v3, 0x1

    .line 2530155
    iget-object v6, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->c:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    invoke-static {v1, v3, v3}, LX/2eF;->a(IZZ)LX/2eF;

    move-result-object v1

    const/4 v2, 0x0

    .line 2530156
    new-instance v3, LX/I2K;

    invoke-direct {v3, p0, p2}, LX/I2K;-><init>(Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;LX/I2L;)V

    move-object v3, v3

    .line 2530157
    const-string v4, "event_collection_carousel_key"

    new-instance v5, LX/I2J;

    invoke-direct {v5, p0}, LX/I2J;-><init>(Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;)V

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2530158
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2530152
    check-cast p1, LX/I2L;

    .line 2530153
    iget-object v0, p1, LX/I2L;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/I2L;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
