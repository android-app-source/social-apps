.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/I2b;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardLoadingRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardNullStateRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/ReactionRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardErrorMessageRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530531
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2530532
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    .line 2530533
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->j:LX/I2b;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530534
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->a:LX/I2b;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530535
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->b:LX/I2b;

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530536
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->c:LX/I2b;

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530537
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->d:LX/I2b;

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530538
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->f:LX/I2b;

    invoke-interface {v0, v1, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530539
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->g:LX/I2b;

    invoke-interface {v0, v1, p11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530540
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->e:LX/I2b;

    invoke-interface {v0, v1, p8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530541
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->k:LX/I2b;

    invoke-interface {v0, v1, p9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530542
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->h:LX/I2b;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530543
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->i:LX/I2b;

    invoke-interface {v0, v1, p10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530544
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->l:LX/I2b;

    invoke-interface {v0, v1, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530545
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    sget-object v1, LX/I2b;->m:LX/I2b;

    invoke-interface {v0, v1, p13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530546
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;
    .locals 14

    .prologue
    .line 2530547
    new-instance v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;

    const/16 v1, 0x513

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x512

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x514

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x516

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x517

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x518

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x51c

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x51d

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x519

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x30a2

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x515

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1b1b

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x51b

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2530548
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2530549
    const/4 v4, 0x0

    .line 2530550
    if-nez p2, :cond_0

    .line 2530551
    :goto_0
    return-object v4

    :cond_0
    move-object v0, p2

    .line 2530552
    check-cast v0, LX/I2V;

    .line 2530553
    check-cast p2, LX/I2V;

    .line 2530554
    iget-object v1, p2, LX/I2V;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2530555
    iget-object v2, v0, LX/I2V;->b:LX/I2b;

    move-object v0, v2

    .line 2530556
    iget-object v2, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardRootPartSelector;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    .line 2530557
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Part definition is not specified for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530558
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2530559
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2530560
    const/4 v0, 0x1

    return v0
.end method
