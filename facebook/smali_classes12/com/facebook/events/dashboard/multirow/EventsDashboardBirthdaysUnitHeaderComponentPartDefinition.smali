.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/I2P;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/I2P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530265
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2530266
    iput-object p2, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;->d:LX/I2P;

    .line 2530267
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 2530268
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2530269
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2530270
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2530271
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530272
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2530273
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/I2P;->a(LX/0QB;)LX/I2P;

    move-result-object v4

    check-cast v4, LX/I2P;

    invoke-direct {p0, v3, v4}, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/I2P;)V

    .line 2530274
    move-object v0, p0

    .line 2530275
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2530276
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530277
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2530278
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2530279
    check-cast p3, LX/1Pn;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1Pn;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2530280
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardBirthdaysUnitHeaderComponentPartDefinition;->d:LX/I2P;

    const/4 p0, 0x0

    .line 2530281
    new-instance p2, LX/I2O;

    invoke-direct {p2, v0}, LX/I2O;-><init>(LX/I2P;)V

    .line 2530282
    sget-object p3, LX/I2P;->a:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/I2N;

    .line 2530283
    if-nez p3, :cond_0

    .line 2530284
    new-instance p3, LX/I2N;

    invoke-direct {p3}, LX/I2N;-><init>()V

    .line 2530285
    :cond_0
    invoke-static {p3, p1, p0, p0, p2}, LX/I2N;->a$redex0(LX/I2N;LX/1De;IILX/I2O;)V

    .line 2530286
    move-object p2, p3

    .line 2530287
    move-object p0, p2

    .line 2530288
    move-object v0, p0

    .line 2530289
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2530290
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2530291
    const/4 v0, 0x0

    return-object v0
.end method
