.class public Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/I3K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I3F;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Bm1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:LX/I3J;

.field public j:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2531133
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2531134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->h:Z

    return-void
.end method

.method public static b(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V
    .locals 5

    .prologue
    .line 2531135
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2531136
    iget-boolean v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->h:Z

    if-nez v1, :cond_0

    .line 2531137
    :goto_0
    return-void

    .line 2531138
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->d:LX/I3F;

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->g:Ljava/lang/String;

    new-instance v4, LX/I3A;

    invoke-direct {v4, p0}, LX/I3A;-><init>(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V

    invoke-virtual {v1, v0, v2, v3, v4}, LX/I3F;->a(IILjava/lang/String;LX/Hxd;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531139
    const-string v0, "event_subscriptions"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2531101
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2531102
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    const-class v5, LX/I3K;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/I3K;

    invoke-static {v0}, LX/I3F;->b(LX/0QB;)LX/I3F;

    move-result-object p1

    check-cast p1, LX/I3F;

    invoke-static {v0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v0

    check-cast v0, LX/Bm1;

    iput-object v3, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->b:LX/1nQ;

    iput-object v5, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->c:LX/I3K;

    iput-object p1, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->d:LX/I3F;

    iput-object v0, v2, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->e:LX/Bm1;

    .line 2531103
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531104
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2531105
    const-string v2, "extras_event_action_context"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/common/EventActionContext;

    .line 2531106
    sget-object v2, Lcom/facebook/events/common/ActionSource;->MOBILE_SUBSCRIPTIONS_LIST:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v1, v2}, Lcom/facebook/events/common/EventActionContext;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventActionContext;

    move-result-object v1

    move-object v1, v1

    .line 2531107
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2531108
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2531109
    if-eqz v2, :cond_0

    .line 2531110
    :goto_0
    move-object v2, v2

    .line 2531111
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531112
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->c:LX/I3K;

    iget-object v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1}, LX/I3K;->a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I3J;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    .line 2531113
    invoke-static {p0}, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->b(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V

    .line 2531114
    return-void

    :cond_0
    const-string v2, "unknown"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2fd5f49b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2531127
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08eb

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2531128
    const v2, 0x7f030584

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2531129
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2531130
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I37;

    invoke-direct {v2, p0}, LX/I37;-><init>(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2531131
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I38;

    invoke-direct {v2, p0}, LX/I38;-><init>(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2531132
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    const/16 v2, 0x2b

    const v3, -0x7be73db2

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x52cf9673

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2531122
    iget-object v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->b:LX/1nQ;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    invoke-virtual {v3}, LX/I3J;->a()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531123
    iget-object v6, v4, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v4, v6

    .line 2531124
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, LX/1nQ;->a(Ljava/lang/String;II)V

    .line 2531125
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2531126
    const/16 v1, 0x2b

    const v2, 0x30ea4919

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x67c1bde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2531118
    iget-object v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->d:LX/I3F;

    invoke-virtual {v1}, LX/I3F;->a()V

    .line 2531119
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2531120
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2531121
    const/16 v1, 0x2b

    const v2, 0x75847c95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5e1bcb0a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2531115
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2531116
    iget-object v0, p0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0821ca

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2531117
    const/16 v0, 0x2b

    const v2, 0xa51f359

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
