.class public Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/I08;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I0H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/I09;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7v8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Lcom/facebook/widget/listview/BetterListView;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/util/GregorianCalendar;

.field public m:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2526861
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2526862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->i:Z

    return-void
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V
    .locals 6

    .prologue
    .line 2526864
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->a:LX/I08;

    invoke-virtual {v0}, LX/I08;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526865
    :cond_0
    :goto_0
    return-void

    .line 2526866
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->a:LX/I08;

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->l:Ljava/util/GregorianCalendar;

    new-instance v5, LX/I0K;

    invoke-direct {v5, p0}, LX/I0K;-><init>(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V

    invoke-virtual/range {v0 .. v5}, LX/I08;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2526863
    const-string v0, "event_birthdays"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2526821
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2526822
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->m:Ljava/text/SimpleDateFormat;

    .line 2526823
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;

    invoke-static {v0}, LX/I08;->b(LX/0QB;)LX/I08;

    move-result-object v3

    check-cast v3, LX/I08;

    invoke-static {v0}, LX/I0H;->b(LX/0QB;)LX/I0H;

    move-result-object v4

    check-cast v4, LX/I0H;

    invoke-static {v0}, LX/I09;->a(LX/0QB;)LX/I09;

    move-result-object v5

    check-cast v5, LX/I09;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    invoke-static {v0}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v7

    check-cast v7, LX/7v8;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    iput-object v3, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->a:LX/I08;

    iput-object v4, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    iput-object v5, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->c:LX/I09;

    iput-object v6, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->d:LX/1nQ;

    iput-object v7, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->e:LX/7v8;

    iput-object v0, v2, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->f:LX/0zG;

    .line 2526824
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->c:LX/I09;

    .line 2526825
    iget-object v1, v0, LX/I09;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, v0, LX/I09;->a:LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2526826
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-wide/16 v7, -0x1

    const/4 v13, 0x6

    const/4 v4, 0x0

    .line 2526827
    const/4 v3, 0x0

    .line 2526828
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 2526829
    if-eqz v0, :cond_0

    .line 2526830
    const-string v3, "birthday_view_referrer_param"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2526831
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "birthday_view_start_date"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2526832
    :try_start_0
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 2526833
    iget-object v6, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->m:Ljava/text/SimpleDateFormat;

    invoke-virtual {v6, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    .line 2526834
    :goto_0
    new-instance v10, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2526835
    new-instance v11, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v11, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->l:Ljava/util/GregorianCalendar;

    .line 2526836
    cmp-long v7, v5, v7

    if-eqz v7, :cond_7

    .line 2526837
    iget-object v7, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->l:Ljava/util/GregorianCalendar;

    invoke-virtual {v7, v5, v6}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 2526838
    iget-object v5, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->l:Ljava/util/GregorianCalendar;

    invoke-virtual {v5, v13}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v5

    invoke-virtual {v10, v13}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v6

    if-eq v5, v6, :cond_1

    const/4 v4, 0x1

    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2526839
    :goto_1
    if-eqz p1, :cond_2

    const-string v5, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    .line 2526840
    :cond_2
    iget-object v5, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->e:LX/7v8;

    if-nez v3, :cond_3

    const-string v3, ""

    :cond_3
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v3, v4}, LX/7v8;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 2526841
    :goto_2
    move-object v0, v3

    .line 2526842
    iput-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->k:Ljava/lang/String;

    .line 2526843
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    iget-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->k:Ljava/lang/String;

    .line 2526844
    iput-object v1, v0, LX/I0H;->j:Ljava/lang/String;

    .line 2526845
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    .line 2526846
    iput-object p0, v0, LX/I0H;->f:Lcom/facebook/base/fragment/FbFragment;

    .line 2526847
    if-eqz p1, :cond_4

    const-string v0, "posted_birthdays_fbids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2526848
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    const-string v1, "posted_birthdays_fbids"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2526849
    iput-object v1, v0, LX/I0H;->k:Landroid/os/Bundle;

    .line 2526850
    :cond_4
    invoke-static {p0}, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b$redex0(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V

    .line 2526851
    return-void

    .line 2526852
    :catch_0
    move-exception v5

    .line 2526853
    const-class v6, LX/I0B;

    const-string v10, "Invalid date format"

    new-array v11, v4, [Ljava/lang/Object;

    invoke-static {v6, v5, v10, v11}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    move-wide v5, v7

    goto :goto_0

    .line 2526854
    :cond_6
    const-string v3, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_7
    move-object v4, v9

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2526855
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2526856
    if-eqz p3, :cond_0

    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    .line 2526857
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2526858
    if-eqz v0, :cond_0

    .line 2526859
    iget-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    iget-wide v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-wide v4, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    invoke-virtual {v1, v2, v4, v5}, LX/I0H;->a(Ljava/lang/String;J)V

    .line 2526860
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x3e9c6f58

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2526814
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08eb

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2526815
    const v2, 0x7f030595

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 2526816
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I0I;

    invoke-direct {v2, p0}, LX/I0I;-><init>(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2526817
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2526818
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2526819
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I0J;

    invoke-direct {v2, p0}, LX/I0J;-><init>(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2526820
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    const/16 v2, 0x2b

    const v3, -0x261937d8

    invoke-static {v5, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x409698fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2526809
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 2526810
    iget-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->c:LX/I09;

    .line 2526811
    iget-object v2, v1, LX/I09;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v4, v1, LX/I09;->a:LX/0Yj;

    invoke-interface {v2, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 2526812
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2526813
    const/16 v1, 0x2b

    const v2, 0x35615120

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2526803
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2526804
    const-string v0, "birthday_view_waterfall_id_param"

    iget-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2526805
    const-string v0, "posted_birthdays_fbids"

    iget-object v1, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b:LX/I0H;

    .line 2526806
    iget-object p0, v1, LX/I0H;->k:Landroid/os/Bundle;

    move-object v1, p0

    .line 2526807
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2526808
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x66d38670

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2526800
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2526801
    iget-object v0, p0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->f:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f0821ad

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2526802
    const/16 v0, 0x2b

    const v2, 0xa8bc29c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
