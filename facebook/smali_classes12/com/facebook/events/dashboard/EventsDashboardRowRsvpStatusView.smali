.class public Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field public a:LX/I7w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DBA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DBC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DB9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HxQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/events/model/Event;

.field private k:Lcom/facebook/events/common/EventAnalyticsParams;

.field private l:LX/BnW;

.field private m:Landroid/view/View$OnClickListener;

.field private n:LX/Bni;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2523905
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2523906
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a()V

    .line 2523907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2523908
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2523909
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a()V

    .line 2523910
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2523911
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2523912
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a()V

    .line 2523913
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;)LX/BnW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2523914
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;Z)LX/BnW;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2523915
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->h:LX/0wM;

    const v1, -0xa76f01

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_0

    .line 2523917
    const v0, 0x7f020642

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2523918
    :goto_0
    return-object v0

    .line 2523919
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_1

    .line 2523920
    const v0, 0x7f020644

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2523921
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_2

    .line 2523922
    const v0, 0x7f020641

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2523923
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523924
    if-eqz p1, :cond_0

    const v0, 0x7f020661

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f02064f

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523925
    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p2}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2523803
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2523804
    new-instance v0, LX/HyE;

    invoke-direct {v0, p0}, LX/HyE;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->m:Landroid/view/View$OnClickListener;

    .line 2523805
    new-instance v0, LX/HyF;

    invoke-direct {v0, p0}, LX/HyF;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    .line 2523806
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;LX/I7w;LX/DBA;LX/DBC;LX/DB9;LX/BiT;LX/38v;LX/HxQ;LX/0wM;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2523926
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->b:LX/DBA;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->c:LX/DBC;

    iput-object p4, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->d:LX/DB9;

    iput-object p5, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->e:LX/BiT;

    iput-object p6, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->f:LX/38v;

    iput-object p7, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->g:LX/HxQ;

    iput-object p8, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->h:LX/0wM;

    iput-object p9, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->i:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-static {v9}, LX/I7w;->b(LX/0QB;)LX/I7w;

    move-result-object v1

    check-cast v1, LX/I7w;

    invoke-static {v9}, LX/DBA;->b(LX/0QB;)LX/DBA;

    move-result-object v2

    check-cast v2, LX/DBA;

    invoke-static {v9}, LX/DBC;->a(LX/0QB;)LX/DBC;

    move-result-object v3

    check-cast v3, LX/DBC;

    invoke-static {v9}, LX/DB9;->b(LX/0QB;)LX/DB9;

    move-result-object v4

    check-cast v4, LX/DB9;

    invoke-static {v9}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v5

    check-cast v5, LX/BiT;

    const-class v6, LX/38v;

    invoke-interface {v9, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/38v;

    invoke-static {v9}, LX/HxQ;->a(LX/0QB;)LX/HxQ;

    move-result-object v7

    check-cast v7, LX/HxQ;

    invoke-static {v9}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v0 .. v9}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;LX/I7w;LX/DBA;LX/DBC;LX/DB9;LX/BiT;LX/38v;LX/HxQ;LX/0wM;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523897
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_0

    .line 2523898
    const v0, 0x7f02067d

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2523899
    :goto_0
    return-object v0

    .line 2523900
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_1

    .line 2523901
    const v0, 0x7f020681

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2523902
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_2

    .line 2523903
    const v0, 0x7f020679

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2523904
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523927
    if-eqz p1, :cond_0

    const v0, 0x7f020649

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f02039c

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2523801
    new-instance v0, LX/HyG;

    invoke-direct {v0, p0}, LX/HyG;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    .line 2523802
    return-void
.end method

.method private c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2523807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_0

    .line 2523808
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0812a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2523809
    :goto_0
    return-object v0

    .line 2523810
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_1

    .line 2523811
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0812aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2523812
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_2

    .line 2523813
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0812ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2523814
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V
    .locals 4

    .prologue
    .line 2523815
    new-instance v0, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2523816
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2523817
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    .line 2523818
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->y:Z

    move v2, v3

    .line 2523819
    if-nez v2, :cond_1

    .line 2523820
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v2, :cond_0

    .line 2523821
    const v2, 0x7f082f34    # 1.810201E38f

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2523822
    const v3, 0x7f020651

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2523823
    new-instance v3, LX/HyI;

    invoke-direct {v3, p0}, LX/HyI;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2523824
    :cond_0
    const v2, 0x7f082f35

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2523825
    const v3, 0x7f02064c

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2523826
    new-instance v3, LX/HyJ;

    invoke-direct {v3, p0}, LX/HyJ;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2523827
    :cond_1
    const v2, 0x7f082f36

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    .line 2523828
    const v2, 0x7f02064b

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2523829
    new-instance v2, LX/HyK;

    invoke-direct {v2, p0}, LX/HyK;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2523830
    invoke-virtual {v0, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2523831
    return-void
.end method

.method private getAdminContentDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2523832
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSavedContentDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2523833
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Z)LX/BnW;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2523834
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->g:LX/HxQ;

    .line 2523835
    iget-object v2, v1, LX/HxQ;->c:LX/Hx6;

    move-object v1, v2

    .line 2523836
    sget-object v2, LX/Hx6;->PAST:LX/Hx6;

    if-eq v1, v2, :cond_0

    .line 2523837
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->H:Z

    move v1, v1

    .line 2523838
    if-eqz v1, :cond_1

    .line 2523839
    :cond_0
    :goto_0
    return-object v0

    .line 2523840
    :cond_1
    if-eqz p2, :cond_2

    .line 2523841
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->b()V

    .line 2523842
    :cond_2
    const/4 v1, 0x1

    move v1, v1

    .line 2523843
    sget-object v2, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {p1, v2}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2523844
    new-instance v0, LX/BnW;

    invoke-direct {p0, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getAdminContentDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->m:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2523845
    :cond_3
    iget v2, p1, Lcom/facebook/events/model/Event;->aq:I

    move v2, v2

    .line 2523846
    if-lez v2, :cond_4

    .line 2523847
    new-instance v0, LX/BnW;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getTicketedEventDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getTicketedEventContentDescription()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/HyH;

    invoke-direct {v3, p0}, LX/HyH;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2523848
    :cond_4
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v2

    .line 2523849
    if-eqz v1, :cond_7

    if-eqz v2, :cond_7

    .line 2523850
    iget-object v2, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v2

    .line 2523851
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v2, v3, :cond_5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v2, v3, :cond_a

    .line 2523852
    :cond_5
    if-eqz p2, :cond_6

    .line 2523853
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->f:LX/38v;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    invoke-virtual {v0, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    .line 2523854
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2523855
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, LX/Bne;->b(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    goto :goto_0

    .line 2523856
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->f:LX/38v;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    invoke-virtual {v0, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    .line 2523857
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2523858
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    goto :goto_0

    .line 2523859
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 2523860
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v2, v3, :cond_8

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v2, v3, :cond_8

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v3, :cond_a

    .line 2523861
    :cond_8
    if-eqz p2, :cond_9

    .line 2523862
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->f:LX/38v;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    invoke-virtual {v0, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    .line 2523863
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2523864
    iget-object v3, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v3, v3

    .line 2523865
    invoke-virtual {v0, v1, v2, v3}, LX/Bne;->b(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    goto/16 :goto_0

    .line 2523866
    :cond_9
    new-instance v0, LX/BnW;

    invoke-direct {p0, v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v2}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->c(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->m:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2523867
    :cond_a
    iget-boolean v2, p1, Lcom/facebook/events/model/Event;->E:Z

    move v2, v2

    .line 2523868
    if-eqz v2, :cond_0

    .line 2523869
    new-instance v0, LX/BnW;

    invoke-direct {p0, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->b(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getSavedContentDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->f:LX/38v;

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->n:LX/Bni;

    invoke-virtual {v3, v4}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v3

    .line 2523870
    iget-object v4, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v4, v4

    .line 2523871
    const/4 p1, 0x0

    .line 2523872
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v4, p0, :cond_b

    .line 2523873
    new-instance p0, LX/Bnd;

    invoke-static {}, LX/BiT;->b()LX/0Px;

    move-result-object v4

    invoke-direct {p0, v3, v4, p1}, LX/Bnd;-><init>(LX/Bne;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    move-object p0, p0

    .line 2523874
    :goto_1
    move-object v3, p0

    .line 2523875
    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_b
    new-instance p0, LX/Bna;

    invoke-direct {p0, v3, p1}, LX/Bna;-><init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_1
.end method

.method public final a(LX/HyL;)V
    .locals 3

    .prologue
    .line 2523876
    iget-object v0, p1, LX/HyL;->a:Lcom/facebook/events/model/Event;

    iget-object v1, p1, LX/HyL;->b:LX/BnW;

    iget-object v2, p1, LX/HyL;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;LX/BnW;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2523877
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;LX/BnW;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1
    .param p2    # LX/BnW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2523878
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    .line 2523879
    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->l:LX/BnW;

    .line 2523880
    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523881
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 2523882
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523883
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setVisibility(I)V

    .line 2523884
    :goto_0
    return-void

    .line 2523885
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    invoke-virtual {v0, p1, p3}, LX/I7w;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2523886
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->b:LX/DBA;

    invoke-virtual {v0, p1, p3}, LX/DBA;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2523887
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->d:LX/DB9;

    invoke-virtual {v0, p1, p3}, LX/DB9;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2523888
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setVisibility(I)V

    .line 2523889
    iget-object v0, p2, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2523890
    iget-object v0, p2, LX/BnW;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2523891
    iget-object v0, p2, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public getBoundModelAndState()LX/HyL;
    .locals 4

    .prologue
    .line 2523892
    new-instance v0, LX/HyL;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->l:LX/BnW;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v0, v1, v2, v3}, LX/HyL;-><init>(Lcom/facebook/events/model/Event;LX/BnW;Lcom/facebook/events/common/EventAnalyticsParams;)V

    return-object v0
.end method

.method public getTicketedEventContentDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2523893
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTicketedEventDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2523894
    const v0, 0x7f020646

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public setOptimisticEvent(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2523895
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;)LX/BnW;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;LX/BnW;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2523896
    return-void
.end method
