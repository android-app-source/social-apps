.class public final Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/Hyx;


# direct methods
.method public constructor <init>(LX/Hyx;LX/0Px;)V
    .locals 0

    .prologue
    .line 2524973
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 2524974
    const-wide v2, 0x7fffffffffffffffL

    .line 2524975
    const-wide/16 v4, 0x0

    .line 2524976
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2524977
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_3

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2524978
    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v8

    .line 2524979
    cmp-long v10, v8, v2

    if-gez v10, :cond_0

    move-wide v2, v8

    .line 2524980
    :cond_0
    cmp-long v10, v8, v4

    if-lez v10, :cond_1

    move-wide v4, v8

    .line 2524981
    :cond_1
    iget-object v8, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2524982
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2524983
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 2524984
    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v8, v8, LX/Hyx;->k:LX/HyO;

    sget-object v9, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v8, v9}, LX/HyO;->a(LX/HyN;)V

    .line 2524985
    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v8, v8, LX/Hyx;->i:Landroid/content/ContentResolver;

    iget-object v9, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v9, v9, LX/Hyx;->j:LX/Bky;

    invoke-static {v8, v9, v0}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V

    .line 2524986
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v0, v0, LX/Hyx;->k:LX/HyO;

    sget-object v8, LX/HyN;->DB_UPDATE:LX/HyN;

    invoke-virtual {v0, v8}, LX/HyO;->b(LX/HyN;)V

    .line 2524987
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2524988
    :cond_2
    iget-object v8, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v8, v8, LX/Hyx;->i:Landroid/content/ContentResolver;

    iget-object v9, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v9, v9, LX/Hyx;->j:LX/Bky;

    invoke-static {v8, v9, v0}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;)V

    goto :goto_1

    .line 2524989
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v0, v0, LX/Hyx;->i:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;->b:LX/Hyx;

    iget-object v1, v1, LX/Hyx;->j:LX/Bky;

    invoke-static/range {v0 .. v6}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;JJLjava/util/List;)V

    .line 2524990
    return-void
.end method
