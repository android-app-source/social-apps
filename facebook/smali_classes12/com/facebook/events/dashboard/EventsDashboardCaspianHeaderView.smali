.class public Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;
.super Lcom/facebook/widget/CustomHorizontalScrollView;
.source ""


# instance fields
.field public a:LX/HyA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field public g:LX/Hx6;

.field private h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

.field private i:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field public k:LX/Hww;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2522482
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomHorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 2522483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    .line 2522484
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    .line 2522485
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a()V

    .line 2522486
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2522477
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomHorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2522478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    .line 2522479
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    .line 2522480
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a()V

    .line 2522481
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2522472
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomHorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2522473
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    .line 2522474
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    .line 2522475
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a()V

    .line 2522476
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2522467
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2522468
    const v0, 0x7f030555

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->setContentView(I)V

    .line 2522469
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setHorizontalScrollBarEnabled(Z)V

    .line 2522470
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->b()V

    .line 2522471
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;LX/HyA;LX/1nQ;LX/0W9;LX/154;)V
    .locals 0

    .prologue
    .line 2522466
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a:LX/HyA;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->b:LX/1nQ;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->c:LX/0W9;

    iput-object p4, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->d:LX/154;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    invoke-static {v3}, LX/HyA;->a(LX/0QB;)LX/HyA;

    move-result-object v0

    check-cast v0, LX/HyA;

    invoke-static {v3}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {v3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {v3}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v3

    check-cast v3, LX/154;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;LX/HyA;LX/1nQ;LX/0W9;LX/154;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2522402
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2522403
    const v0, 0x7f0d0ef5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2522404
    const v0, 0x7f0d0ef6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2522405
    const v0, 0x7f0d0ef7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2522406
    const v0, 0x7f0d0ef8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2522407
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    if-eqz v0, :cond_0

    .line 2522408
    const v0, 0x7f0d0ef9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2522409
    const v0, 0x7f0d0ef9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2522410
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    .line 2522411
    invoke-static {}, LX/Hx6;->values()[LX/Hx6;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2522412
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 2522413
    :cond_1
    return-void

    .line 2522414
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    if-nez v0, :cond_3

    sget-object v0, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v5, v0, :cond_5

    .line 2522415
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    .line 2522416
    sget-object v6, LX/Hx6;->INVITED:LX/Hx6;

    if-ne v5, v6, :cond_4

    .line 2522417
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    .line 2522418
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, v5, LX/Hx6;->menuStringResId:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->c:LX/0W9;

    invoke-virtual {v7}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2522419
    iput-object v5, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->a:LX/Hx6;

    .line 2522420
    new-instance v6, LX/HxO;

    invoke-direct {v6, p0, v1, v5}, LX/HxO;-><init>(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;ILX/Hx6;)V

    invoke-virtual {v0, v6}, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522421
    add-int/lit8 v0, v1, 0x1

    .line 2522422
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2522456
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2522457
    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 2522458
    :cond_0
    :goto_0
    return-void

    .line 2522459
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2522460
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 2522461
    if-lez p1, :cond_2

    .line 2522462
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2522463
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 2522464
    :goto_1
    sub-int v0, v2, v0

    .line 2522465
    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->scrollTo(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2522447
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    .line 2522448
    iget-object v3, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->a:LX/Hx6;

    move-object v3, v3

    .line 2522449
    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->g:LX/Hx6;

    if-ne v3, v4, :cond_0

    .line 2522450
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->setTextColor(I)V

    .line 2522451
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2522452
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a010e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;->setTextColor(I)V

    goto :goto_1

    .line 2522453
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    if-eqz v0, :cond_2

    .line 2522454
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e()V

    .line 2522455
    :cond_2
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2522443
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->i:LX/0QR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->i:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2522444
    :cond_0
    :goto_0
    return-void

    .line 2522445
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->i:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->a()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    .line 2522446
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e()V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2522438
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    if-eqz v0, :cond_0

    .line 2522439
    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    if-lez v0, :cond_1

    .line 2522440
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->d:LX/154;

    iget v2, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    invoke-virtual {v1, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2522441
    :cond_0
    :goto_0
    return-void

    .line 2522442
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->h:Lcom/facebook/events/dashboard/EventsDashboardFilterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getBadgeCount()I
    .locals 1

    .prologue
    .line 2522437
    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    return v0
.end method

.method public setBadge(I)V
    .locals 0

    .prologue
    .line 2522434
    iput p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    .line 2522435
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->e()V

    .line 2522436
    return-void
.end method

.method public setCountsSummarySupplier(LX/0QR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QR",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2522431
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->i:LX/0QR;

    .line 2522432
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->d()V

    .line 2522433
    return-void
.end method

.method public setDashboardFilterType(LX/Hx6;)V
    .locals 0

    .prologue
    .line 2522428
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->g:LX/Hx6;

    .line 2522429
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->c()V

    .line 2522430
    return-void
.end method

.method public setOnFilterClickedListener(LX/Hww;)V
    .locals 0

    .prologue
    .line 2522426
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->k:LX/Hww;

    .line 2522427
    return-void
.end method

.method public setShouldShowBirthdays(Z)V
    .locals 0

    .prologue
    .line 2522423
    iput-boolean p1, p0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->f:Z

    .line 2522424
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->b()V

    .line 2522425
    return-void
.end method
