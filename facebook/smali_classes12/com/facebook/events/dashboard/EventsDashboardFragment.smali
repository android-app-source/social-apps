.class public Lcom/facebook/events/dashboard/EventsDashboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public A:LX/Hxz;

.field public B:LX/Hze;

.field public C:Lcom/facebook/widget/listview/BetterListView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private E:Lcom/facebook/events/common/EventAnalyticsParams;

.field public F:LX/195;

.field public G:I

.field public H:Z

.field public I:Ljava/lang/String;

.field public J:Ljava/lang/String;

.field public K:LX/Hx6;

.field private L:LX/Hx6;

.field public M:I

.field private N:LX/Hxq;

.field public O:Ljava/lang/Long;

.field public P:Ljava/lang/Long;

.field private Q:LX/Hxd;

.field private R:LX/Hxb;

.field public S:LX/1PH;

.field public T:Landroid/widget/AbsListView$OnScrollListener;

.field private U:Z

.field private V:Z

.field public W:LX/5O9;

.field public final X:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/Hxr;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Landroid/database/Cursor;

.field public a:LX/HxP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:LX/Hxg;

.field private ab:LX/Hxg;

.field public b:LX/DBC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Bky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HxS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Hze;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/I08;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HxQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Hx9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Hx5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/HyA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/HyO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I3F;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I3Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2iz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/7v8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Hy0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Hy8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/I0P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2523182
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2523183
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/Hxr;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    .line 2523184
    new-instance v0, LX/Hxh;

    invoke-direct {v0, p0}, LX/Hxh;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->aa:LX/Hxg;

    .line 2523185
    new-instance v0, LX/Hxi;

    invoke-direct {v0, p0}, LX/Hxi;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->ab:LX/Hxg;

    return-void
.end method

.method private a(LX/Hx6;Z)V
    .locals 6
    .param p1    # LX/Hx6;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2523196
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->L:LX/Hx6;

    if-eq v0, p1, :cond_2

    if-eqz p2, :cond_2

    .line 2523197
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->L:LX/Hx6;

    .line 2523198
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2523199
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m:LX/1nQ;

    invoke-virtual {p1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->M:I

    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->v()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2523200
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2523201
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 2523202
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2523203
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n:LX/HyA;

    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v1

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HyA;->c(Ljava/lang/String;)V

    .line 2523204
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/HxP;LX/DBC;LX/Bky;LX/HxS;LX/0Or;LX/I08;LX/HxQ;LX/Hx9;LX/0kb;LX/193;LX/0TD;LX/Hx5;LX/1nQ;LX/HyA;LX/HyO;LX/0tX;LX/1Ck;LX/0Ot;LX/0Ot;LX/2iz;LX/0Uh;LX/7v8;LX/Hy0;LX/Hy8;LX/I0P;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/dashboard/EventsDashboardFragment;",
            "LX/HxP;",
            "LX/DBC;",
            "LX/Bky;",
            "LX/HxS;",
            "LX/0Or",
            "<",
            "LX/Hze;",
            ">;",
            "LX/I08;",
            "LX/HxQ;",
            "LX/Hx9;",
            "LX/0kb;",
            "LX/193;",
            "LX/0TD;",
            "LX/Hx5;",
            "LX/1nQ;",
            "LX/HyA;",
            "LX/HyO;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/I3F;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/I3Y;",
            ">;",
            "LX/2iz;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/7v8;",
            "LX/Hy0;",
            "LX/Hy8;",
            "LX/I0P;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2523205
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a:LX/HxP;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b:LX/DBC;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c:LX/Bky;

    iput-object p4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->d:LX/HxS;

    iput-object p5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->f:LX/I08;

    iput-object p7, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    iput-object p8, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->h:LX/Hx9;

    iput-object p9, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->i:LX/0kb;

    iput-object p10, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->j:LX/193;

    iput-object p11, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k:LX/0TD;

    iput-object p12, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    iput-object p13, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m:LX/1nQ;

    iput-object p14, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n:LX/HyA;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->p:LX/0tX;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->q:LX/1Ck;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t:LX/2iz;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->u:LX/0Uh;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->v:LX/7v8;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->w:LX/Hy0;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x:LX/Hy8;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y:LX/I0P;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->z:LX/0Or;

    return-void
.end method

.method public static a(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/widget/listview/BetterListView;)V
    .locals 2

    .prologue
    .line 2523206
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->S:LX/1PH;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2523207
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->G:I

    .line 2523208
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->T:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p1, v0}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2523209
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {p1, v0}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2523210
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 30

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static/range {v28 .. v28}, LX/HxP;->a(LX/0QB;)LX/HxP;

    move-result-object v3

    check-cast v3, LX/HxP;

    invoke-static/range {v28 .. v28}, LX/DBC;->a(LX/0QB;)LX/DBC;

    move-result-object v4

    check-cast v4, LX/DBC;

    invoke-static/range {v28 .. v28}, LX/Bky;->a(LX/0QB;)LX/Bky;

    move-result-object v5

    check-cast v5, LX/Bky;

    invoke-static/range {v28 .. v28}, LX/HxS;->a(LX/0QB;)LX/HxS;

    move-result-object v6

    check-cast v6, LX/HxS;

    const/16 v7, 0x1b05

    move-object/from16 v0, v28

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {v28 .. v28}, LX/I08;->a(LX/0QB;)LX/I08;

    move-result-object v8

    check-cast v8, LX/I08;

    invoke-static/range {v28 .. v28}, LX/HxQ;->a(LX/0QB;)LX/HxQ;

    move-result-object v9

    check-cast v9, LX/HxQ;

    invoke-static/range {v28 .. v28}, LX/Hx9;->a(LX/0QB;)LX/Hx9;

    move-result-object v10

    check-cast v10, LX/Hx9;

    invoke-static/range {v28 .. v28}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v11

    check-cast v11, LX/0kb;

    const-class v12, LX/193;

    move-object/from16 v0, v28

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/193;

    invoke-static/range {v28 .. v28}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, LX/0TD;

    invoke-static/range {v28 .. v28}, LX/Hx5;->a(LX/0QB;)LX/Hx5;

    move-result-object v14

    check-cast v14, LX/Hx5;

    invoke-static/range {v28 .. v28}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v15

    check-cast v15, LX/1nQ;

    invoke-static/range {v28 .. v28}, LX/HyA;->a(LX/0QB;)LX/HyA;

    move-result-object v16

    check-cast v16, LX/HyA;

    invoke-static/range {v28 .. v28}, LX/HyO;->a(LX/0QB;)LX/HyO;

    move-result-object v17

    check-cast v17, LX/HyO;

    invoke-static/range {v28 .. v28}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v18

    check-cast v18, LX/0tX;

    invoke-static/range {v28 .. v28}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v19

    check-cast v19, LX/1Ck;

    const/16 v20, 0x1b27

    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x1b2a

    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {v28 .. v28}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v22

    check-cast v22, LX/2iz;

    invoke-static/range {v28 .. v28}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v23

    check-cast v23, LX/0Uh;

    invoke-static/range {v28 .. v28}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v24

    check-cast v24, LX/7v8;

    const-class v25, LX/Hy0;

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/Hy0;

    invoke-static/range {v28 .. v28}, LX/Hy8;->a(LX/0QB;)LX/Hy8;

    move-result-object v26

    check-cast v26, LX/Hy8;

    invoke-static/range {v28 .. v28}, LX/I0P;->a(LX/0QB;)LX/I0P;

    move-result-object v27

    check-cast v27, LX/I0P;

    const/16 v29, 0x122d

    invoke-static/range {v28 .. v29}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    invoke-static/range {v2 .. v28}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/HxP;LX/DBC;LX/Bky;LX/HxS;LX/0Or;LX/I08;LX/HxQ;LX/Hx9;LX/0kb;LX/193;LX/0TD;LX/Hx5;LX/1nQ;LX/HyA;LX/HyO;LX/0tX;LX/1Ck;LX/0Ot;LX/0Ot;LX/2iz;LX/0Uh;LX/7v8;LX/Hy0;LX/Hy8;LX/I0P;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 2523211
    sget-object v0, LX/Hxf;->b:[I

    invoke-virtual {p1}, LX/Hx6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2523212
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c:LX/Bky;

    iget-object v0, v0, LX/Bky;->d:Landroid/net/Uri;

    .line 2523213
    :goto_0
    return-object v0

    .line 2523214
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c:LX/Bky;

    iget-object v0, v0, LX/Bky;->e:Landroid/net/Uri;

    goto :goto_0

    .line 2523215
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c:LX/Bky;

    iget-object v0, v0, LX/Bky;->f:Landroid/net/Uri;

    goto :goto_0

    .line 2523216
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c:LX/Bky;

    iget-object v0, v0, LX/Bky;->g:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hxr;)V
    .locals 1

    .prologue
    .line 2523217
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2523218
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2523219
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2523220
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Landroid/database/Cursor;LX/Hx6;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2523221
    if-nez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->M:I

    .line 2523222
    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->M:I

    if-lez v0, :cond_4

    .line 2523223
    const/4 v0, 0x1

    .line 2523224
    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne p2, v1, :cond_0

    .line 2523225
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->h:LX/Hx9;

    invoke-virtual {v1}, LX/Hx9;->b()V

    .line 2523226
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 2523227
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v2, LX/HyN;->RENDERING:LX/HyN;

    invoke-virtual {v1, v2}, LX/HyO;->a(LX/HyN;)V

    .line 2523228
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2523229
    iget-object v4, v2, LX/Hxz;->p:Ljava/util/Set;

    invoke-static {v4}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v4

    move-object v2, v4

    .line 2523230
    invoke-virtual {v1, p1, v2, p2, v0}, LX/Hx5;->a(Landroid/database/Cursor;LX/0Rf;LX/Hx6;Z)V

    .line 2523231
    invoke-direct {p0, p2, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(LX/Hx6;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2523232
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    if-eq v0, p1, :cond_2

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 2523233
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2523234
    iput-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    .line 2523235
    :cond_2
    return-void

    .line 2523236
    :cond_3
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 2523237
    :cond_4
    sget-object v0, LX/Hx6;->PAST:LX/Hx6;

    if-ne p2, v0, :cond_5

    .line 2523238
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->V:Z

    goto :goto_1

    .line 2523239
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->U:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2523240
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    if-eq v1, p1, :cond_6

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    if-eqz v1, :cond_6

    .line 2523241
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2523242
    iput-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    :cond_6
    throw v0
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V
    .locals 3

    .prologue
    .line 2523243
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->W:LX/5O9;

    if-nez v0, :cond_0

    .line 2523244
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v0, p1}, LX/Hx5;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V

    .line 2523245
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3Y;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2523246
    iget-object v2, v1, LX/Hx5;->p:LX/Gcz;

    move-object v1, v2

    .line 2523247
    iput-object v1, v0, LX/I3Y;->e:LX/Gcz;

    .line 2523248
    :goto_0
    return-void

    .line 2523249
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    sget-object v1, LX/Hxr;->ON_SUGGESTIONS:LX/Hxr;

    new-instance v2, Lcom/facebook/events/dashboard/EventsDashboardFragment$16;

    invoke-direct {v2, p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFragment$16;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Z)V
    .locals 5

    .prologue
    .line 2523250
    if-eqz p1, :cond_0

    .line 2523251
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2523252
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2523253
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v1

    sget-object v2, LX/Hx6;->PAST:LX/Hx6;

    if-ne v1, v2, :cond_2

    .line 2523254
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->ab:LX/Hxg;

    const/4 p1, 0x0

    const/4 p0, 0x1

    .line 2523255
    iget-object v3, v1, LX/Hze;->h:LX/1Ck;

    sget-object v4, LX/Hzc;->FETCH_PAST_EVENTS:LX/Hzc;

    invoke-virtual {v3, v4}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2523256
    :cond_1
    :goto_0
    return-void

    .line 2523257
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->aa:LX/Hxg;

    invoke-virtual {v1, v0, v2}, LX/Hze;->b(ILX/Hxg;)V

    goto :goto_0

    .line 2523258
    :cond_3
    iget-boolean v3, v1, LX/Hze;->n:Z

    if-nez v3, :cond_5

    .line 2523259
    iput-boolean p0, v1, LX/Hze;->n:Z

    .line 2523260
    iput-object p1, v1, LX/Hze;->m:Ljava/lang/String;

    .line 2523261
    iput-boolean p0, v1, LX/Hze;->o:Z

    .line 2523262
    :cond_4
    new-instance v3, LX/Hza;

    invoke-direct {v3, v1, v0}, LX/Hza;-><init>(LX/Hze;I)V

    .line 2523263
    new-instance v4, LX/Hzb;

    invoke-direct {v4, v1, v2}, LX/Hzb;-><init>(LX/Hze;LX/Hxg;)V

    .line 2523264
    iget-object p0, v1, LX/Hze;->h:LX/1Ck;

    sget-object p1, LX/Hzc;->FETCH_PAST_EVENTS:LX/Hzc;

    invoke-virtual {p0, p1, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    .line 2523265
    :cond_5
    iget-boolean v3, v1, LX/Hze;->o:Z

    if-nez v3, :cond_4

    .line 2523266
    if-eqz v2, :cond_1

    .line 2523267
    iget-boolean v3, v1, LX/Hze;->o:Z

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, p1}, LX/Hxg;->a(ZILjava/lang/Long;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;ZILjava/lang/Long;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2523349
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2523350
    :cond_0
    :goto_0
    return-void

    .line 2523351
    :cond_1
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v2}, LX/Hxz;->h()V

    .line 2523352
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2523353
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v2, p1}, LX/Hx5;->b(Z)V

    .line 2523354
    if-eqz p1, :cond_4

    .line 2523355
    if-lez p2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->U:Z

    .line 2523356
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523357
    :cond_2
    :goto_2
    if-eqz p3, :cond_0

    .line 2523358
    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->O:Ljava/lang/Long;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2523359
    goto :goto_1

    .line 2523360
    :cond_4
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->U:Z

    .line 2523361
    if-nez p2, :cond_2

    .line 2523362
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v1, v0}, LX/Hx5;->a(Z)V

    .line 2523363
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v2, LX/HyN;->RENDERING:LX/HyN;

    invoke-virtual {v1, v2}, LX/HyO;->a(LX/HyN;)V

    .line 2523364
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(LX/Hx6;Z)V

    .line 2523365
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->h:LX/Hx9;

    invoke-virtual {v0}, LX/Hx9;->a()V

    goto :goto_2
.end method

.method private b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2523268
    if-eqz p1, :cond_0

    const-string v0, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2523269
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->v:LX/7v8;

    const-string v1, ""

    .line 2523270
    sget-object p0, LX/7v7;->BIRTHDAYS_EVENTS_DASHBOARD_ENTRY:LX/7v7;

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {v0, p0, v1, p1}, LX/7v8;->a(LX/7v8;LX/7v7;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 2523271
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2523345
    sget-object v0, LX/Hx6;->PAST:LX/Hx6;

    if-ne p1, v0, :cond_0

    .line 2523346
    sget-object v0, LX/Bkw;->I:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v0

    .line 2523347
    :goto_0
    move-object v0, v0

    .line 2523348
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Landroid/database/Cursor;LX/Hx6;)V
    .locals 2

    .prologue
    .line 2523330
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v1, LX/HyN;->DB_FETCH:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2523331
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 2523332
    :goto_0
    if-nez v0, :cond_0

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne p2, v1, :cond_0

    .line 2523333
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->h:LX/Hx9;

    invoke-virtual {v1}, LX/Hx9;->c()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->U:Z

    .line 2523334
    :cond_0
    if-gtz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->U:Z

    if-eqz v0, :cond_3

    .line 2523335
    :cond_1
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Z:Landroid/database/Cursor;

    .line 2523336
    invoke-static {p0, p1, p2}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Landroid/database/Cursor;LX/Hx6;)V

    .line 2523337
    :goto_1
    return-void

    .line 2523338
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 2523339
    :cond_3
    const-string v0, "cold"

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->I:Ljava/lang/String;

    .line 2523340
    if-eqz p1, :cond_4

    .line 2523341
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 2523342
    :cond_4
    sget-object v0, LX/5O9;->GRAPHQL:LX/5O9;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->W:LX/5O9;

    .line 2523343
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523344
    sget-object v0, LX/Hxr;->ON_EVENTS_FROM_GRAPHQL:LX/Hxr;

    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hxr;)V

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Ljava/util/List;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2523302
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2523303
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    move-object v0, v0

    .line 2523304
    const/4 v3, 0x0

    .line 2523305
    iget-object v1, v0, LX/Hxz;->k:LX/3kp;

    iget-object v2, v0, LX/Hxz;->d:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 2523306
    iget-object v1, v0, LX/Hxz;->k:LX/3kp;

    const/4 v2, 0x1

    .line 2523307
    iput v2, v1, LX/3kp;->b:I

    .line 2523308
    iget-object v1, v0, LX/Hxz;->k:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2523309
    iget-object v1, v0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    const v2, 0x7f0d08dc

    invoke-virtual {v1, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 2523310
    instance-of v2, v1, Landroid/view/ViewStub;

    if-nez v2, :cond_3

    .line 2523311
    :cond_0
    :goto_0
    if-gtz p2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2523312
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2523313
    iput-object p1, v0, LX/Hx5;->t:Ljava/util/List;

    .line 2523314
    iput p2, v0, LX/Hx5;->u:I

    .line 2523315
    new-instance v2, LX/Hx0;

    invoke-direct {v2, v0}, LX/Hx0;-><init>(LX/Hx5;)V

    invoke-static {p1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v5

    .line 2523316
    iget-object v2, v0, LX/Hx5;->I:LX/Gd0;

    iget-object v3, v0, LX/Hx5;->G:Landroid/content/Context;

    iget-object v4, v0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v6, Lcom/facebook/events/common/ActionSource;->MOBILE_SUBSCRIPTIONS_DASHBOARD:Lcom/facebook/events/common/ActionSource;

    sget-object v7, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUBSCRIPTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, LX/Gd0;->a(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)LX/Gcz;

    move-result-object v2

    iput-object v2, v0, LX/Hx5;->q:LX/Gcz;

    .line 2523317
    const v2, 0x57f624eb

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2523318
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3F;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2523319
    iget-object v2, v1, LX/Hx5;->q:LX/Gcz;

    move-object v1, v2

    .line 2523320
    iput-object v1, v0, LX/I3F;->f:LX/Gcz;

    .line 2523321
    :cond_2
    return-void

    .line 2523322
    :cond_3
    check-cast v1, Landroid/view/ViewStub;

    .line 2523323
    const v2, 0x7f03058a

    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2523324
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2523325
    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2523326
    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2523327
    new-instance v2, LX/Hxt;

    invoke-direct {v2, v0, v1}, LX/Hxt;-><init>(LX/Hxz;Lcom/facebook/fbui/widget/megaphone/Megaphone;)V

    .line 2523328
    iput-object v2, v1, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 2523329
    new-instance v2, LX/Hxu;

    invoke-direct {v2, v0, v1}, LX/Hxu;-><init>(LX/Hxz;Lcom/facebook/fbui/widget/megaphone/Megaphone;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;ZILjava/lang/Long;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2523287
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2523288
    :cond_0
    :goto_0
    return-void

    .line 2523289
    :cond_1
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v2}, LX/Hxz;->h()V

    .line 2523290
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2523291
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v2, p1}, LX/Hx5;->b(Z)V

    .line 2523292
    if-eqz p1, :cond_4

    .line 2523293
    if-lez p2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->V:Z

    .line 2523294
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523295
    :cond_2
    :goto_2
    if-eqz p3, :cond_0

    .line 2523296
    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->P:Ljava/lang/Long;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2523297
    goto :goto_1

    .line 2523298
    :cond_4
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->V:Z

    .line 2523299
    if-nez p2, :cond_2

    .line 2523300
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v1, v0}, LX/Hx5;->a(Z)V

    .line 2523301
    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    invoke-direct {p0, v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(LX/Hx6;Z)V

    goto :goto_2
.end method

.method public static e$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 2

    .prologue
    .line 2523281
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2523282
    :goto_0
    return-void

    .line 2523283
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2523284
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->i:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2523285
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    sget-object v1, LX/DBa;->FAILURE_LOADING_EVENTS:LX/DBa;

    invoke-virtual {v0, v1}, LX/Hxz;->a(LX/DBa;)V

    goto :goto_0

    .line 2523286
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v0, v1}, LX/Hxz;->a(LX/DBa;)V

    goto :goto_0
.end method

.method public static k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z
    .locals 1

    .prologue
    .line 2523280
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;
    .locals 1

    .prologue
    .line 2523277
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    .line 2523278
    iget-object p0, v0, LX/HxQ;->c:LX/Hx6;

    move-object v0, p0

    .line 2523279
    return-object v0
.end method

.method public static n(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 4

    .prologue
    .line 2523272
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    .line 2523273
    iget-boolean v1, v0, LX/HxQ;->e:Z

    if-nez v1, :cond_0

    invoke-static {v0}, LX/HxQ;->c(LX/HxQ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2523274
    iget-object v1, v0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v1

    iget-object v2, v0, LX/HxQ;->c:LX/Hx6;

    invoke-virtual {v2}, LX/Hx6;->ordinal()I

    move-result v2

    const/4 v3, 0x0

    iget-object p0, v0, LX/HxQ;->b:LX/1jv;

    invoke-virtual {v1, v2, v3, p0}, LX/0k5;->a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    .line 2523275
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/HxQ;->e:Z

    .line 2523276
    :cond_0
    return-void
.end method

.method public static p(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 4

    .prologue
    .line 2523186
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v1, LX/HyN;->CREATE_VIEW:LX/HyN;

    invoke-virtual {v0, v1}, LX/HyO;->b(LX/HyN;)V

    .line 2523187
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v0, v1, :cond_1

    .line 2523188
    :cond_0
    :goto_0
    return-void

    .line 2523189
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2523190
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2523191
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 2523192
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Landroid/database/Cursor;LX/Hx6;)V

    .line 2523193
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2523194
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2523195
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->q:LX/1Ck;

    sget-object v1, LX/Hxs;->FIRST_DB_FETCH:LX/Hxs;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/HxX;

    invoke-direct {v3, p0}, LX/HxX;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1
.end method

.method public static q(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 6

    .prologue
    .line 2522951
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y:LX/I0P;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2522952
    iget-boolean v3, v0, LX/I0P;->f:Z

    if-nez v3, :cond_0

    .line 2522953
    iput-object p0, v0, LX/I0P;->h:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2522954
    iput-object v1, v0, LX/I0P;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 2522955
    iput-object v2, v0, LX/I0P;->l:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2522956
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy/MM/dd"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v3, v0, LX/I0P;->n:Ljava/text/SimpleDateFormat;

    .line 2522957
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v0, v3}, LX/I0P;->a(LX/I0P;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 2522958
    iget-object v4, v0, LX/I0P;->i:LX/I0H;

    .line 2522959
    iput-object v3, v4, LX/I0H;->j:Ljava/lang/String;

    .line 2522960
    iget-object v3, v0, LX/I0P;->i:LX/I0H;

    .line 2522961
    iput-object p0, v3, LX/I0H;->f:Lcom/facebook/base/fragment/FbFragment;

    .line 2522962
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0f74

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, LX/I0P;->k:I

    .line 2522963
    new-instance v3, LX/I0M;

    invoke-direct {v3, v0}, LX/I0M;-><init>(LX/I0P;)V

    iput-object v3, v0, LX/I0P;->d:Landroid/widget/AbsListView$OnScrollListener;

    .line 2522964
    new-instance v3, LX/I0N;

    invoke-direct {v3, v0}, LX/I0N;-><init>(LX/I0P;)V

    iput-object v3, v0, LX/I0P;->m:LX/1PH;

    .line 2522965
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/I0P;->f:Z

    .line 2522966
    :cond_0
    iget-object v3, v0, LX/I0P;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v4, v0, LX/I0P;->d:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2522967
    iget-object v3, v0, LX/I0P;->l:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget-object v4, v0, LX/I0P;->m:LX/1PH;

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2522968
    iget-object v3, v0, LX/I0P;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v4, v0, LX/I0P;->i:LX/I0H;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2522969
    invoke-static {v0}, LX/I0P;->c(LX/I0P;)V

    .line 2522970
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m:LX/1nQ;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->v()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2522971
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2522972
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/1nQ;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 2522973
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b()V

    .line 2522974
    return-void
.end method

.method public static r(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/1PH;
    .locals 1

    .prologue
    .line 2522941
    new-instance v0, LX/HxY;

    invoke-direct {v0, p0}, LX/HxY;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    return-object v0
.end method

.method public static s(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 2522940
    new-instance v0, LX/Hxa;

    invoke-direct {v0, p0}, LX/Hxa;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    return-object v0
.end method

.method public static t(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 8

    .prologue
    .line 2522915
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2522916
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v3

    sget-object v4, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v3, v4, :cond_3

    :cond_0
    move v1, v2

    .line 2522917
    :cond_1
    :goto_0
    move v0, v1

    .line 2522918
    if-eqz v0, :cond_2

    .line 2522919
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Z)V

    .line 2522920
    :cond_2
    return-void

    .line 2522921
    :cond_3
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v3

    .line 2522922
    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v4

    .line 2522923
    if-ltz v3, :cond_4

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v5}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_4

    if-ltz v4, :cond_4

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v5}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result v5

    if-lt v4, v5, :cond_5

    :cond_4
    move v1, v2

    .line 2522924
    goto :goto_0

    .line 2522925
    :cond_5
    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2522926
    invoke-static {v5, v3, v4}, LX/Hx5;->i(LX/Hx5;II)V

    .line 2522927
    iget-object v6, v5, LX/Hx5;->A:Ljava/lang/Object;

    sget-object v7, LX/Hx5;->d:Ljava/lang/Object;

    if-ne v6, v7, :cond_8

    const/4 v6, 0x1

    :goto_1
    move v5, v6

    .line 2522928
    if-nez v5, :cond_1

    .line 2522929
    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2522930
    invoke-static {v5, v3, v4}, LX/Hx5;->i(LX/Hx5;II)V

    .line 2522931
    iget-object v6, v5, LX/Hx5;->A:Ljava/lang/Object;

    instance-of v6, v6, Lcom/facebook/events/model/Event;

    if-eqz v6, :cond_9

    .line 2522932
    iget-object v6, v5, LX/Hx5;->A:Ljava/lang/Object;

    check-cast v6, Lcom/facebook/events/model/Event;

    .line 2522933
    :goto_2
    move-object v3, v6

    .line 2522934
    if-eqz v3, :cond_7

    .line 2522935
    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v3

    .line 2522936
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v5

    sget-object v6, LX/Hx6;->PAST:LX/Hx6;

    if-ne v5, v6, :cond_6

    .line 2522937
    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->P:Ljava/lang/Long;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->P:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v3, v5, v3

    if-gtz v3, :cond_1

    move v1, v2

    goto :goto_0

    .line 2522938
    :cond_6
    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->O:Ljava/lang/Long;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->O:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v3, v5, v3

    if-ltz v3, :cond_1

    move v1, v2

    goto :goto_0

    :cond_7
    move v1, v2

    .line 2522939
    goto :goto_0

    :cond_8
    const/4 v6, 0x0

    goto :goto_1

    :cond_9
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private v()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2522912
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2522913
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2522914
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private w()V
    .locals 7

    .prologue
    .line 2522905
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->R:LX/Hxb;

    if-nez v0, :cond_0

    .line 2522906
    new-instance v0, LX/Hxc;

    invoke-direct {v0, p0}, LX/Hxc;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->R:LX/Hxb;

    .line 2522907
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3Y;

    const/4 v1, 0x5

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->R:LX/Hxb;

    .line 2522908
    new-instance v4, LX/I3T;

    invoke-direct {v4, v0, v1, v2}, LX/I3T;-><init>(LX/I3Y;II)V

    .line 2522909
    new-instance v5, LX/I3U;

    invoke-direct {v5, v0, v3}, LX/I3U;-><init>(LX/I3Y;LX/Hxb;)V

    .line 2522910
    iget-object v6, v0, LX/I3Y;->b:LX/1Ck;

    sget-object p0, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_CUTS:LX/I3X;

    invoke-virtual {v6, p0, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2522911
    return-void
.end method

.method private x()V
    .locals 4

    .prologue
    .line 2522900
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Q:LX/Hxd;

    if-nez v0, :cond_0

    .line 2522901
    new-instance v0, LX/Hxe;

    invoke-direct {v0, p0}, LX/Hxe;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Q:LX/Hxd;

    .line 2522902
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3F;

    const/16 v1, 0xf

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Q:LX/Hxd;

    .line 2522903
    const/4 p0, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, LX/I3F;->a(IILjava/lang/String;LX/Hxd;)V

    .line 2522904
    return-void
.end method

.method private y()Z
    .locals 2

    .prologue
    .line 2522899
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->N:LX/Hxq;

    sget-object v1, LX/Hxq;->STANDALONE:LX/Hxq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2522898
    const-string v0, "event_dashboard"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2522975
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2522976
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2522977
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2522978
    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a:LX/HxP;

    if-eqz v0, :cond_3

    const-string v5, "force_tabbed_dashboard"

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2522979
    :goto_0
    iput-boolean v0, v4, LX/HxP;->b:Z

    .line 2522980
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2522981
    if-eqz p1, :cond_5

    const-string v4, "extra_key_fragment_mode"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2522982
    const-string v0, "extra_key_fragment_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hxq;->valueOf(Ljava/lang/String;)LX/Hxq;

    move-result-object v0

    .line 2522983
    :goto_1
    move-object v0, v0

    .line 2522984
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->N:LX/Hxq;

    .line 2522985
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2522986
    if-eqz p1, :cond_7

    const-string v4, "extra_dashboard_filter_type"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2522987
    const-string v0, "extra_dashboard_filter_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx6;->valueOf(Ljava/lang/String;)LX/Hx6;

    move-result-object v0

    .line 2522988
    :goto_2
    move-object v0, v0

    .line 2522989
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    .line 2522990
    sget-object v0, LX/Hxf;->a:[I

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->N:LX/Hxq;

    invoke-virtual {v4}, LX/Hxq;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2522991
    :goto_3
    const/4 v3, 0x0

    .line 2522992
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->u:LX/0Uh;

    const/16 v1, 0x3a0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->H:Z

    .line 2522993
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2522994
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->j:LX/193;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v3, "events_dashboard_scroll"

    invoke-virtual {v0, v1, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->F:LX/195;

    .line 2522995
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_1

    .line 2522996
    sget-object v0, LX/5O9;->DB_FETCH:LX/5O9;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->W:LX/5O9;

    .line 2522997
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2522998
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->d:LX/HxS;

    sget-object v1, LX/HxS;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hzd;

    .line 2522999
    if-eqz v0, :cond_4

    .line 2523000
    iget-object v1, v0, LX/Hzd;->b:LX/Hze;

    iput-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    .line 2523001
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    .line 2523002
    iput-object v3, v1, LX/HxQ;->d:LX/Hze;

    .line 2523003
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->aa:LX/Hxg;

    const/4 v7, 0x1

    .line 2523004
    iget-boolean v6, v1, LX/Hze;->k:Z

    if-nez v6, :cond_9

    move v6, v7

    :goto_4
    const-string v8, "query session exists before first fetch"

    invoke-static {v6, v8}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2523005
    iget-wide v8, v0, LX/Hzd;->e:J

    .line 2523006
    iget v6, v0, LX/Hzd;->c:I

    .line 2523007
    iput-boolean v7, v1, LX/Hze;->k:Z

    .line 2523008
    const/4 v10, 0x0

    iput-object v10, v1, LX/Hze;->j:Ljava/lang/String;

    .line 2523009
    iput-boolean v7, v1, LX/Hze;->l:Z

    .line 2523010
    iget-object v7, v0, LX/Hzd;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2523011
    iget-object v10, v1, LX/Hze;->d:LX/HyO;

    sget-object v11, LX/HyN;->NETWORK_FETCH:LX/HyN;

    invoke-virtual {v10, v11}, LX/HyO;->a(LX/HyN;)V

    .line 2523012
    invoke-static {v1, v6, v3, v8, v9}, LX/Hze;->a(LX/Hze;ILX/Hxg;J)LX/0Vd;

    move-result-object v6

    .line 2523013
    new-instance v8, Lcom/facebook/events/dashboard/EventsPager$2;

    invoke-direct {v8, v1, v7, v6}, Lcom/facebook/events/dashboard/EventsPager$2;-><init>(LX/Hze;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    move-object v1, v8

    .line 2523014
    :goto_5
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    sget-object v4, LX/Hxr;->ON_EVENTS_FROM_GRAPHQL:LX/Hxr;

    invoke-virtual {v3, v4, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2523015
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    sget-object v3, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v1, v3, :cond_2

    .line 2523016
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    .line 2523017
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v4, LX/HyN;->DB_FETCH:LX/HyN;

    invoke-virtual {v3, v4}, LX/HyO;->a(LX/HyN;)V

    .line 2523018
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    sget-object v4, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v3, v4, :cond_a

    if-eqz v0, :cond_a

    iget-object v3, v0, LX/Hzd;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v3, :cond_a

    .line 2523019
    iget-object v3, v0, LX/Hzd;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2523020
    :goto_6
    move-object v0, v3

    .line 2523021
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->Y:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2523022
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    .line 2523023
    iput-object p0, v0, LX/Hx5;->k:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523024
    new-instance v0, LX/Hxn;

    invoke-direct {v0, p0}, LX/Hxn;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    move-object v0, v0

    .line 2523025
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    .line 2523026
    iput-object p0, v1, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2523027
    iput-object v0, v1, LX/HxQ;->b:LX/1jv;

    .line 2523028
    iput-object v3, v1, LX/HxQ;->c:LX/Hx6;

    .line 2523029
    const/4 v4, 0x0

    iput-boolean v4, v1, LX/HxQ;->e:Z

    .line 2523030
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->J:Ljava/lang/String;

    .line 2523031
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->f:LX/I08;

    const/4 v1, 0x3

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    new-instance v5, LX/Hxl;

    invoke-direct {v5, p0}, LX/Hxl;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/I08;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V

    .line 2523032
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->w()V

    .line 2523033
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x()V

    .line 2523034
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2523035
    iget-object v1, v0, LX/Hxz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object v0, v1

    .line 2523036
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523037
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523038
    iput-object v1, v0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523039
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b:LX/DBC;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->E:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523040
    iput-object v1, v0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523041
    return-void

    :cond_3
    move v0, v3

    .line 2523042
    goto/16 :goto_0

    .line 2523043
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->w:LX/Hy0;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v3, v1}, LX/Hy0;->a(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;Ljava/lang/Boolean;)LX/Hxz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2523044
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v0}, LX/Hxz;->a()V

    goto/16 :goto_3

    .line 2523045
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->w:LX/Hy0;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v3}, LX/Hy0;->a(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;Ljava/lang/Boolean;)LX/Hxz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2523046
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v0}, LX/Hxz;->a()V

    .line 2523047
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    invoke-virtual {v0}, LX/HyO;->a()V

    goto/16 :goto_3

    .line 2523048
    :cond_4
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hze;

    iput-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    .line 2523049
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    .line 2523050
    iput-object v4, v1, LX/HxQ;->d:LX/Hze;

    .line 2523051
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->aa:LX/Hxg;

    const/4 v7, 0x1

    .line 2523052
    iget-boolean v6, v1, LX/Hze;->k:Z

    if-nez v6, :cond_b

    move v6, v7

    :goto_7
    const-string v8, "query session exists before first fetch"

    invoke-static {v6, v8}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2523053
    iget-object v6, v1, LX/Hze;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v8

    .line 2523054
    iput-boolean v7, v1, LX/Hze;->k:Z

    .line 2523055
    const/4 v6, 0x0

    iput-object v6, v1, LX/Hze;->j:Ljava/lang/String;

    .line 2523056
    iput-boolean v7, v1, LX/Hze;->l:Z

    .line 2523057
    invoke-static {v1, v3}, LX/Hze;->a$redex0(LX/Hze;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2523058
    iget-object v7, v1, LX/Hze;->d:LX/HyO;

    sget-object v10, LX/HyN;->NETWORK_FETCH:LX/HyN;

    invoke-virtual {v7, v10}, LX/HyO;->a(LX/HyN;)V

    .line 2523059
    invoke-static {v1, v3, v4, v8, v9}, LX/Hze;->a(LX/Hze;ILX/Hxg;J)LX/0Vd;

    move-result-object v7

    .line 2523060
    new-instance v8, Lcom/facebook/events/dashboard/EventsPager$1;

    invoke-direct {v8, v1, v6, v7}, Lcom/facebook/events/dashboard/EventsPager$1;-><init>(LX/Hze;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    move-object v1, v8

    .line 2523061
    goto/16 :goto_5

    .line 2523062
    :cond_5
    if-eqz v0, :cond_6

    const-string v4, "extra_key_fragment_mode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2523063
    const-string v4, "extra_key_fragment_mode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hxq;->valueOf(Ljava/lang/String;)LX/Hxq;

    move-result-object v0

    goto/16 :goto_1

    .line 2523064
    :cond_6
    sget-object v0, LX/Hxq;->STANDALONE:LX/Hxq;

    goto/16 :goto_1

    .line 2523065
    :cond_7
    if-eqz v0, :cond_8

    const-string v4, "extra_dashboard_filter_type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2523066
    const-string v4, "extra_dashboard_filter_type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx6;->valueOf(Ljava/lang/String;)LX/Hx6;

    move-result-object v0

    goto/16 :goto_2

    .line 2523067
    :cond_8
    sget-object v0, LX/Hx6;->UPCOMING:LX/Hx6;

    goto/16 :goto_2

    .line 2523068
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_a
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k:LX/0TD;

    new-instance v4, LX/Hxm;

    invoke-direct {v4, p0, v1}, LX/Hxm;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto/16 :goto_6

    .line 2523069
    :cond_b
    const/4 v6, 0x0

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2523070
    invoke-static {}, LX/7oV;->a()LX/7oJ;

    move-result-object v0

    .line 2523071
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2523072
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->p:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2523073
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->q:LX/1Ck;

    sget-object v2, LX/Hxs;->FETCH_EVENT_COUNTS:LX/Hxs;

    new-instance v3, LX/Hxj;

    invoke-direct {v3, p0}, LX/Hxj;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2523074
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2523075
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2523076
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2523077
    new-instance v2, LX/HxZ;

    invoke-direct {v2, p0, v1}, LX/HxZ;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;Lcom/facebook/widget/listview/BetterListView;)V

    move-object v1, v2

    .line 2523078
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2523079
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2523080
    sparse-switch p1, :sswitch_data_0

    .line 2523081
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2523082
    :cond_1
    :goto_1
    return-void

    .line 2523083
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b:LX/DBC;

    invoke-virtual {v0, p1, p2, p3}, LX/DBC;->a(IILandroid/content/Intent;)V

    goto :goto_1

    .line 2523084
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2523085
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->g:LX/HxQ;

    .line 2523086
    iget-object v1, v0, LX/HxQ;->c:LX/Hx6;

    move-object v0, v1

    .line 2523087
    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v0, v1, :cond_3

    .line 2523088
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y:LX/I0P;

    .line 2523089
    iget-object v2, v0, LX/I0P;->i:LX/I0H;

    if-eqz v2, :cond_2

    if-eqz p3, :cond_2

    const/16 v2, 0x6dc

    if-ne p1, v2, :cond_2

    .line 2523090
    const-string v2, "publishPostParams"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2523091
    if-eqz v2, :cond_2

    .line 2523092
    iget-object v3, v0, LX/I0P;->i:LX/I0H;

    iget-wide v4, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget-wide v6, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->originalPostTime:J

    invoke-virtual {v3, v4, v6, v7}, LX/I0H;->a(Ljava/lang/String;J)V

    .line 2523093
    :cond_2
    goto :goto_1

    .line 2523094
    :cond_3
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2523095
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t:LX/2iz;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, LX/2iz;->b(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    goto :goto_1

    .line 2523096
    :sswitch_2
    if-nez p2, :cond_0

    .line 2523097
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1556604017999292"

    .line 2523098
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2523099
    move-object v0, v0

    .line 2523100
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6a -> :sswitch_2
        0x1f5 -> :sswitch_0
        0x6dc -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x725fbae4

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2523101
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->o:LX/HyO;

    sget-object v2, LX/HyN;->CREATE_VIEW:LX/HyN;

    invoke-virtual {v0, v2}, LX/HyO;->a(LX/HyN;)V

    .line 2523102
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08eb

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2523103
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->y()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2523104
    const v2, 0x7f030561

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x2b

    const v3, -0x5e1fbd30

    invoke-static {v5, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2523105
    :goto_0
    return-object v0

    :cond_0
    const v2, 0x7f03024f

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x2a080314

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x11163ca6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2523106
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->B:LX/Hze;

    .line 2523107
    iget-object v2, v0, LX/Hze;->b:Landroid/content/ContentResolver;

    iget-object v4, v0, LX/Hze;->c:LX/Bky;

    const/16 v5, 0x30

    iget-object v6, v0, LX/Hze;->f:LX/0TD;

    invoke-static {v2, v4, v5, v6}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;ILX/0TD;)V

    .line 2523108
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3F;

    invoke-virtual {v0}, LX/I3F;->a()V

    .line 2523109
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I3Y;

    invoke-virtual {v0}, LX/I3Y;->a()V

    .line 2523110
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2523111
    const/16 v0, 0x2b

    const v2, 0x6fb93277

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x558cb29c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522942
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->n:LX/HyA;

    invoke-virtual {v1}, LX/HyA;->b()V

    .line 2522943
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2522944
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2522945
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2522946
    iget-object v2, v1, LX/Hxz;->l:LX/Bl6;

    iget-object v4, v1, LX/Hxz;->m:LX/Hxx;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2522947
    iget-object v2, v1, LX/Hxz;->l:LX/Bl6;

    iget-object v4, v1, LX/Hxz;->o:LX/Hxy;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 2522948
    const/4 v2, 0x0

    iput-object v2, v1, LX/Hxz;->a:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2522949
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2522950
    const/16 v1, 0x2b

    const v2, 0x145ee6be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x926456c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2523112
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    .line 2523113
    iget-object v2, v1, LX/Hxz;->e:LX/0zG;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0h5;

    .line 2523114
    if-nez v2, :cond_0

    .line 2523115
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2523116
    const/16 v1, 0x2b

    const v2, -0x1551ee90

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2523117
    :cond_0
    const v4, 0x7f082193

    invoke-interface {v2, v4}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2523118
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2523119
    const-string v0, "extra_dashboard_filter_type"

    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->m(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/Hx6;

    move-result-object v1

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523120
    const-string v0, "birthday_view_waterfall_id_param"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523121
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x57ee29bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2523122
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2523123
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    const/4 p0, 0x0

    .line 2523124
    iget-boolean v2, v1, LX/Hxz;->h:Z

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2523125
    if-eqz v2, :cond_3

    .line 2523126
    iget-object v2, v1, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    const-class v4, LX/1ZF;

    invoke-virtual {v2, v4}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2523127
    if-nez v2, :cond_5

    .line 2523128
    :cond_0
    :goto_1
    iget-object v2, v1, LX/Hxz;->n:LX/0ad;

    sget-short v4, LX/347;->r:S

    invoke-interface {v2, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v1, LX/Hxz;->n:LX/0ad;

    sget-short v4, LX/347;->s:S

    invoke-interface {v2, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2523129
    :cond_1
    invoke-static {v1}, LX/Hxz;->o(LX/Hxz;)V

    .line 2523130
    :cond_2
    :goto_2
    iget-object v2, v1, LX/Hxz;->p:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 2523131
    const/16 v1, 0x2b

    const v2, 0x4dd71731    # 4.51077664E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2523132
    :cond_3
    invoke-static {v1}, LX/Hxz;->o(LX/Hxz;)V

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 2523133
    :cond_5
    iget-object v2, v1, LX/Hxz;->e:LX/0zG;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0h5;

    .line 2523134
    if-eqz v2, :cond_0

    .line 2523135
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2523136
    invoke-interface {v2, v4}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2523137
    new-instance v4, LX/Hxv;

    invoke-direct {v4, v1}, LX/Hxv;-><init>(LX/Hxz;)V

    invoke-interface {v2, v4}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2523138
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2523139
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->N:LX/Hxq;

    sget-object v1, LX/Hxq;->SECONDARY_NAVIGATION:LX/Hxq;

    if-ne v0, v1, :cond_1

    .line 2523140
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x:LX/Hy8;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a:LX/HxP;

    .line 2523141
    invoke-virtual {v1}, LX/HxP;->a()Z

    move-result v2

    .line 2523142
    if-eqz v2, :cond_2

    .line 2523143
    sget-object v2, LX/Hy8;->b:[LX/Hx6;

    iput-object v2, v0, LX/Hy8;->d:[LX/Hx6;

    .line 2523144
    :goto_0
    iget-object v2, v0, LX/Hy8;->d:[LX/Hx6;

    array-length v2, v2

    new-array v2, v2, [LX/Hy4;

    iput-object v2, v0, LX/Hy8;->e:[LX/Hy4;

    .line 2523145
    new-instance v2, LX/Hy7;

    invoke-direct {v2, v0}, LX/Hy7;-><init>(LX/Hy8;)V

    iput-object v2, v0, LX/Hy8;->f:LX/Hy7;

    .line 2523146
    const v0, 0x7f0d08de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2523147
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x:LX/Hy8;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2523148
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x:LX/Hy8;

    .line 2523149
    iput-object p0, v1, LX/Hy8;->g:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523150
    const v1, 0x7f0d08dd

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2523151
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2523152
    new-instance v2, LX/Hxo;

    invoke-direct {v2, p0}, LX/Hxo;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523153
    iput-object v2, v1, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2523154
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    if-eq v1, v2, :cond_0

    .line 2523155
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->x:LX/Hy8;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->K:LX/Hx6;

    .line 2523156
    iget-object p1, v1, LX/Hy8;->d:[LX/Hx6;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    move v1, p1

    .line 2523157
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2523158
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v0}, LX/Hxz;->b()V

    .line 2523159
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    new-instance v1, LX/Hxp;

    invoke-direct {v1, p0}, LX/Hxp;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523160
    iput-object v1, v0, LX/Hxz;->c:LX/HxV;

    .line 2523161
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->T:Landroid/widget/AbsListView$OnScrollListener;

    .line 2523162
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/1PH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->S:LX/1PH;

    .line 2523163
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->p(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523164
    :goto_1
    return-void

    .line 2523165
    :cond_1
    const/4 v3, 0x0

    .line 2523166
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    invoke-virtual {v0}, LX/Hxz;->b()V

    .line 2523167
    const v0, 0x7f0d0efe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2523168
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x7f0a00d1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2523169
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->D:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->r(Lcom/facebook/events/dashboard/EventsDashboardFragment;)LX/1PH;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2523170
    const v0, 0x7f0d0eff

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2523171
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2523172
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    .line 2523173
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->G:I

    .line 2523174
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->s(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Landroid/widget/AbsListView$OnScrollListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2523175
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->c()V

    .line 2523176
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2523177
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->A:LX/Hxz;

    new-instance v1, LX/HxW;

    invoke-direct {v1, p0}, LX/HxW;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523178
    iput-object v1, v0, LX/Hxz;->c:LX/HxV;

    .line 2523179
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->p(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2523180
    goto :goto_1

    .line 2523181
    :cond_2
    sget-object v2, LX/Hy8;->a:[LX/Hx6;

    iput-object v2, v0, LX/Hy8;->d:[LX/Hx6;

    goto/16 :goto_0
.end method
