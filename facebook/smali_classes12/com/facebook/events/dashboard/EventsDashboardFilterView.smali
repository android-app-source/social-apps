.class public Lcom/facebook/events/dashboard/EventsDashboardFilterView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/HxP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Path;

.field private j:Landroid/graphics/Paint;

.field public k:LX/Hx6;

.field public l:LX/HxU;

.field public m:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2522600
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2522601
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Landroid/content/Context;)V

    .line 2522602
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2522653
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2522654
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Landroid/content/Context;)V

    .line 2522655
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2522656
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2522657
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Landroid/content/Context;)V

    .line 2522658
    return-void
.end method

.method private static a(II)Landroid/graphics/Path;
    .locals 3

    .prologue
    .line 2522659
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 2522660
    int-to-float v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2522661
    div-int/lit8 v1, p1, 0x2

    int-to-float v1, v1

    int-to-float v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2522662
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 2522663
    return-object v0
.end method

.method private a(LX/Hx6;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2522664
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->m:LX/0QR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->m:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2522665
    :goto_0
    return-object v0

    .line 2522666
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->m:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->a()I

    move-result v0

    .line 2522667
    sget-object v2, LX/Hx6;->INVITED:LX/Hx6;

    if-ne p1, v2, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->c:LX/154;

    invoke-virtual {v1, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2522668
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2522669
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a:LX/HxP;

    invoke-virtual {v0}, LX/HxP;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->d:Z

    .line 2522670
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b(Landroid/content/Context;)V

    .line 2522671
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b()V

    .line 2522672
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2522675
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2522676
    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->f:I

    .line 2522677
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getWidth()I

    move-result v1

    .line 2522678
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getHeight()I

    move-result v2

    .line 2522679
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v3

    .line 2522680
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v4

    .line 2522681
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getScrollX()I

    move-result v5

    .line 2522682
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getScrollY()I

    move-result v6

    .line 2522683
    int-to-float v5, v5

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2522684
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    float-to-int v5, v5

    .line 2522685
    invoke-static {}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    add-int v0, v5, v3

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->e:I

    add-int/2addr v0, v1

    .line 2522686
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getTotalPaddingTop()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getTotalPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->g:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->h:I

    add-int/2addr v1, v2

    .line 2522687
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2522688
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->i:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2522689
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2522690
    return-void

    .line 2522691
    :cond_0
    sub-int/2addr v1, v5

    sub-int/2addr v1, v4

    sub-int v0, v1, v0

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->e:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardFilterView;LX/HxP;LX/1nQ;LX/154;)V
    .locals 0

    .prologue
    .line 2522673
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a:LX/HxP;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b:LX/1nQ;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->c:LX/154;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    invoke-static {v2}, LX/HxP;->a(LX/0QB;)LX/HxP;

    move-result-object v0

    check-cast v0, LX/HxP;

    invoke-static {v2}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {v2}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v2

    check-cast v2, LX/154;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Lcom/facebook/events/dashboard/EventsDashboardFilterView;LX/HxP;LX/1nQ;LX/154;)V

    return-void
.end method

.method private static a()Z
    .locals 1

    .prologue
    .line 2522652
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/Hx6;)I
    .locals 1
    .param p1    # LX/Hx6;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2522674
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    if-ne v0, p1, :cond_0

    iget v0, p1, LX/Hx6;->selectedIconResId:I

    :goto_0
    return v0

    :cond_0
    iget v0, p1, LX/Hx6;->iconResId:I

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2522649
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    if-eqz v0, :cond_0

    .line 2522650
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    iget v0, v0, LX/Hx6;->menuStringResId:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->setText(I)V

    .line 2522651
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2522640
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2522641
    const v1, 0x7f0b0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->e:I

    .line 2522642
    const v1, 0x7f0b1570

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->f:I

    .line 2522643
    const v1, 0x7f0b1571

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->g:I

    .line 2522644
    const v1, 0x7f0b1572

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->h:I

    .line 2522645
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->j:Landroid/graphics/Paint;

    .line 2522646
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->j:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2522647
    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->g:I

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->f:I

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(II)Landroid/graphics/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->i:Landroid/graphics/Path;

    .line 2522648
    return-void
.end method


# virtual methods
.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 2522636
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v0

    .line 2522637
    invoke-static {}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2522638
    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->e:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->f:I

    add-int/2addr v0, v1

    .line 2522639
    :cond_0
    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 2522632
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v0

    .line 2522633
    invoke-static {}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2522634
    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->e:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->f:I

    add-int/2addr v0, v1

    .line 2522635
    :cond_0
    return v0
.end method

.method public getDashboardFilterType()LX/Hx6;
    .locals 1

    .prologue
    .line 2522631
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x188aa39e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522628
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onAttachedToWindow()V

    .line 2522629
    invoke-virtual {p0, p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522630
    const/16 v1, 0x2d

    const v2, 0x1615c411

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    const v1, 0x6e14e03a

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 2522612
    new-instance v6, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2522613
    invoke-virtual {v6}, LX/5OM;->c()LX/5OG;

    move-result-object v7

    .line 2522614
    invoke-static {}, LX/Hx6;->values()[LX/Hx6;

    move-result-object v8

    array-length v9, v8

    move v4, v3

    :goto_0
    if-ge v4, v9, :cond_3

    aget-object v10, v8, v4

    .line 2522615
    iget-boolean v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->d:Z

    if-nez v0, :cond_0

    sget-object v0, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v10, v0, :cond_1

    .line 2522616
    :cond_0
    iget v0, v10, LX/Hx6;->menuStringResId:I

    invoke-virtual {v7, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    .line 2522617
    invoke-direct {p0, v10}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(LX/Hx6;)Ljava/lang/String;

    move-result-object v11

    move-object v0, v1

    .line 2522618
    check-cast v0, LX/3Ai;

    invoke-virtual {v0, v11}, LX/3Ai;->b(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2522619
    invoke-direct {p0, v10}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b(LX/Hx6;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2522620
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2522621
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    if-ne v10, v0, :cond_2

    move v0, v2

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2522622
    new-instance v0, LX/HxT;

    invoke-direct {v0, p0, v10}, LX/HxT;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFilterView;LX/Hx6;)V

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2522623
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v3

    .line 2522624
    goto :goto_1

    .line 2522625
    :cond_3
    iput-boolean v3, v6, LX/0ht;->e:Z

    .line 2522626
    invoke-virtual {v6, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2522627
    const v0, -0x25c26e51

    invoke-static {v0, v5}, LX/02F;->a(II)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6d1c6215

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522609
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522610
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onDetachedFromWindow()V

    .line 2522611
    const/16 v1, 0x2d

    const v2, 0x29321555

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2522606
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2522607
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->a(Landroid/graphics/Canvas;)V

    .line 2522608
    return-void
.end method

.method public setDashboardFilterType(LX/Hx6;)V
    .locals 0

    .prologue
    .line 2522603
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    .line 2522604
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b()V

    .line 2522605
    return-void
.end method

.method public setOnFilterTypeChangedListener(LX/HxU;)V
    .locals 0

    .prologue
    .line 2522598
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->l:LX/HxU;

    .line 2522599
    return-void
.end method
