.class public Lcom/facebook/events/dashboard/EventsDashboardRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I04;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/events/dashboard/EventProfilePictureView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

.field private k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private l:I

.field public m:Lcom/facebook/events/model/Event;

.field private n:LX/Hx6;

.field private o:LX/I03;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2524054
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2524055
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->b()V

    .line 2524056
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2524051
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2524052
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->b()V

    .line 2524053
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2524057
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2524058
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->b()V

    .line 2524059
    return-void
.end method

.method private static a(Lcom/facebook/events/dashboard/EventsDashboardRowView;LX/6RZ;LX/I04;LX/DB4;)V
    .locals 0

    .prologue
    .line 2524113
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a:LX/6RZ;

    iput-object p2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->b:LX/I04;

    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->c:LX/DB4;

    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;ZZ)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2524060
    iget v0, p1, Lcom/facebook/events/model/Event;->aq:I

    move v0, v0

    .line 2524061
    if-lez v0, :cond_0

    .line 2524062
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2524063
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->c:LX/DB4;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2524064
    iget v2, p1, Lcom/facebook/events/model/Event;->aq:I

    move v5, v2

    .line 2524065
    if-lez v5, :cond_4

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2524066
    iget-object v2, v1, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p2, 0x7f0f0152

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v3, v4

    invoke-virtual {v2, p2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2524067
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524068
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2524069
    :goto_1
    return-void

    .line 2524070
    :cond_0
    if-nez p3, :cond_3

    .line 2524071
    if-eqz p2, :cond_1

    .line 2524072
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2524073
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524074
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2524075
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->c:LX/DB4;

    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2524076
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->y:Z

    move v1, v1

    .line 2524077
    if-eqz v1, :cond_5

    .line 2524078
    iget-object v1, v0, LX/DB4;->a:Landroid/content/Context;

    const v3, 0x7f082f4e

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2524079
    :goto_2
    move-object v0, v1

    .line 2524080
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2524081
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2524082
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524083
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2524084
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2524085
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->z:Z

    move v0, v0

    .line 2524086
    if-eqz v0, :cond_2

    const v0, 0x7f0a00d2

    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_1

    :cond_2
    const v0, 0x7f0a010e

    goto :goto_3

    .line 2524087
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v2, v4

    .line 2524088
    goto/16 :goto_0

    .line 2524089
    :cond_5
    iget-boolean v1, v0, LX/DB4;->c:Z

    if-eqz v1, :cond_a

    .line 2524090
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->H:Z

    move v1, v1

    .line 2524091
    if-eqz v1, :cond_a

    .line 2524092
    iget-object v1, p1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v3, v1

    .line 2524093
    if-eqz v3, :cond_a

    .line 2524094
    iget-object v1, v3, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2524095
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 2524096
    iget-object v1, p1, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v1, v1

    .line 2524097
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-eq v1, v4, :cond_6

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-ne v1, v4, :cond_8

    .line 2524098
    :cond_6
    iget-object v4, v0, LX/DB4;->a:Landroid/content/Context;

    .line 2524099
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2524100
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v1, v5, :cond_7

    const v1, 0x7f082f43

    :goto_4
    new-array v5, p3, [Ljava/lang/Object;

    .line 2524101
    iget-object p3, v3, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v3, p3

    .line 2524102
    aput-object v3, v5, p2

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_7
    const v1, 0x7f082f42

    goto :goto_4

    .line 2524103
    :cond_8
    iget-object v4, v0, LX/DB4;->a:Landroid/content/Context;

    .line 2524104
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2524105
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v1, v5, :cond_9

    .line 2524106
    iget-boolean v1, v0, LX/DB4;->d:Z

    if-eqz v1, :cond_b

    .line 2524107
    const v1, 0x7f082f46

    .line 2524108
    :goto_5
    move v1, v1

    .line 2524109
    :goto_6
    new-array v5, p3, [Ljava/lang/Object;

    .line 2524110
    iget-object p3, v3, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v3, p3

    .line 2524111
    aput-object v3, v5, p2

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_9
    const v1, 0x7f082f44

    goto :goto_6

    .line 2524112
    :cond_a
    invoke-virtual {v0, p1}, LX/DB4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_b
    const v1, 0x7f082f45

    goto :goto_5
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    invoke-static {v2}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    const-class v1, LX/I04;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/I04;

    invoke-static {v2}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v2

    check-cast v2, LX/DB4;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/dashboard/EventsDashboardRowView;LX/6RZ;LX/I04;LX/DB4;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2524035
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2524036
    iget v2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    move v0, v2

    .line 2524037
    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 2524038
    :goto_0
    if-eq v0, p1, :cond_0

    .line 2524039
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->l:I

    :goto_1
    invoke-virtual {v2, v1, v0, v1, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2524040
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2524041
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2524042
    goto :goto_1
.end method

.method private a(Lcom/facebook/events/model/Event;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2524043
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->n:LX/Hx6;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->n:LX/Hx6;

    sget-object v3, LX/Hx6;->PAST:LX/Hx6;

    if-ne v2, v3, :cond_0

    move v2, v0

    .line 2524044
    :goto_0
    if-nez v2, :cond_1

    sget-object v2, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {p1, v2}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2524045
    iget-boolean v2, p1, Lcom/facebook/events/model/Event;->H:Z

    move v2, v2

    .line 2524046
    if-eqz v2, :cond_1

    .line 2524047
    iget-boolean v2, p1, Lcom/facebook/events/model/Event;->y:Z

    move v2, v2

    .line 2524048
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 2524049
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2524050
    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2523928
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2523929
    const v0, 0x7f03055e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2523930
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setOrientation(I)V

    .line 2523931
    const v0, 0x7f021903

    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setBackgroundResource(I)V

    .line 2523932
    const v0, 0x7f0d0f04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2523933
    const v0, 0x7f0d0f05

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventProfilePictureView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->d:Lcom/facebook/events/dashboard/EventProfilePictureView;

    .line 2523934
    const v0, 0x7f0d0f06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2523935
    const v0, 0x7f0d0f07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2523936
    const v0, 0x7f0d0f08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2523937
    const v0, 0x7f0d0f09

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2523938
    const v0, 0x7f0d0f0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    .line 2523939
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2523940
    iget v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    move v0, v1

    .line 2523941
    iput v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->l:I

    .line 2523942
    return-void
.end method

.method private c()V
    .locals 11

    .prologue
    .line 2523943
    const v0, 0x7f0d0f0b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2523944
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    .line 2523945
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->b:LX/I04;

    .line 2523946
    new-instance v2, LX/I03;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/I7w;->b(LX/0QB;)LX/I7w;

    move-result-object v5

    check-cast v5, LX/I7w;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v6

    check-cast v6, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v7

    check-cast v7, LX/Bky;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v8

    check-cast v8, LX/7vZ;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v9

    check-cast v9, LX/Bl6;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    move-object v3, p0

    invoke-direct/range {v2 .. v10}, LX/I03;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowView;LX/0TD;LX/I7w;Landroid/content/ContentResolver;LX/Bky;LX/7vZ;LX/Bl6;LX/1Ck;)V

    .line 2523947
    move-object v0, v2

    .line 2523948
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    .line 2523949
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    .line 2523950
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->l:LX/HyD;

    .line 2523951
    return-void
.end method

.method private d()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.System.currentTimeMillis"
        }
    .end annotation

    .prologue
    .line 2523952
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2523953
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->n:LX/Hx6;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v1, :cond_1

    .line 2523954
    :cond_0
    :goto_0
    return-void

    .line 2523955
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2523956
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a:LX/6RZ;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5, v0, v1}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v0

    .line 2523957
    sget-object v1, LX/6RY;->PAST:LX/6RY;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/6RY;->YESTERDAY:LX/6RY;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/6RY;->TODAY:LX/6RY;

    if-ne v0, v1, :cond_0

    .line 2523958
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2523959
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2523960
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2523961
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;ZLcom/facebook/events/common/EventAnalyticsParams;LX/Hx6;Z)V
    .locals 7

    .prologue
    .line 2523962
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/model/Event;ZLcom/facebook/events/common/EventAnalyticsParams;LX/Hx6;ZZ)V

    .line 2523963
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;ZLcom/facebook/events/common/EventAnalyticsParams;LX/Hx6;ZZ)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2523964
    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setFocusable(Z)V

    .line 2523965
    invoke-virtual {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setClickable(Z)V

    .line 2523966
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    .line 2523967
    iget-object v3, v2, LX/I03;->i:Landroid/animation/Animator;

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 2523968
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523969
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2523970
    iget-object v3, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2523971
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2523972
    :cond_0
    :goto_1
    return-void

    .line 2523973
    :cond_1
    iput-object p4, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->n:LX/Hx6;

    .line 2523974
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    if-eqz v2, :cond_2

    .line 2523975
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    invoke-virtual {v2}, LX/I03;->b()V

    .line 2523976
    :cond_2
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523977
    invoke-direct {p0, p1}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/model/Event;)Z

    move-result v2

    .line 2523978
    if-nez v2, :cond_6

    .line 2523979
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    if-eqz v2, :cond_3

    .line 2523980
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    invoke-virtual {v2, v6}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->setVisibility(I)V

    .line 2523981
    :cond_3
    :goto_2
    if-eqz p4, :cond_8

    sget-object v2, LX/Hx6;->PAST:LX/Hx6;

    if-ne p4, v2, :cond_8

    .line 2523982
    :goto_3
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523983
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->y:Z

    move v2, v3

    .line 2523984
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    sget-object v3, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v2, v3}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    if-eqz v0, :cond_9

    .line 2523985
    :cond_5
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-virtual {v2, v6}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setVisibility(I)V

    .line 2523986
    :goto_4
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->d:Lcom/facebook/events/dashboard/EventProfilePictureView;

    invoke-virtual {v2, p1}, Lcom/facebook/events/dashboard/EventProfilePictureView;->a(Lcom/facebook/events/model/Event;)V

    .line 2523987
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523988
    iget-object v4, v3, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2523989
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2523990
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523991
    iget-object v4, v3, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2523992
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2523993
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a:LX/6RZ;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    .line 2523994
    iget-boolean v4, v3, Lcom/facebook/events/model/Event;->N:Z

    move v3, v4

    .line 2523995
    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v4}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v5}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5, v1}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v2

    .line 2523996
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2523997
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2523998
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->d()V

    .line 2523999
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    invoke-direct {p0, v2, p2, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/model/Event;ZZ)V

    .line 2524000
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2524001
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v0

    .line 2524002
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2524003
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2524004
    iget-object v1, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, v1

    .line 2524005
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524006
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2524007
    iget-object v1, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, v1

    .line 2524008
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2524009
    :goto_5
    invoke-direct {p0, p5}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Z)V

    .line 2524010
    iput-object p3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524011
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    if-eqz v0, :cond_0

    .line 2524012
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->o:LX/I03;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524013
    iput-object v1, v0, LX/I03;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2524014
    goto/16 :goto_1

    .line 2524015
    :cond_6
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    if-nez v2, :cond_7

    .line 2524016
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->c()V

    .line 2524017
    :cond_7
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->setVisibility(I)V

    .line 2524018
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    invoke-virtual {v2, v3}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/events/model/Event;)V

    goto/16 :goto_2

    :cond_8
    move v0, v1

    .line 2524019
    goto/16 :goto_3

    .line 2524020
    :cond_9
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->setVisibility(I)V

    .line 2524021
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-virtual {v2, p1, p6}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;Z)LX/BnW;

    move-result-object v2

    .line 2524022
    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-virtual {v3, p1, v2, p3}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a(Lcom/facebook/events/model/Event;LX/BnW;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto/16 :goto_4

    .line 2524023
    :cond_a
    iget-object v0, p1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v0

    .line 2524024
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2524025
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2524026
    iget-object v1, p1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, v1

    .line 2524027
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2524028
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2524029
    iget-object v1, p1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, v1

    .line 2524030
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 2524031
    :cond_b
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_5

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public getEvent()Lcom/facebook/events/model/Event;
    .locals 1

    .prologue
    .line 2524032
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    return-object v0
.end method

.method public getInlineRsvpView()Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2524034
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->i:Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    return-object v0
.end method

.method public getRsvpStatusView()Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;
    .locals 1

    .prologue
    .line 2524033
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->j:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    return-object v0
.end method
