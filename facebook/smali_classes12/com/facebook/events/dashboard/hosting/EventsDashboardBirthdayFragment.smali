.class public Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/I1j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/content/Context;

.field private e:Lcom/facebook/events/common/EventAnalyticsParams;

.field public f:Z

.field private g:Landroid/support/v7/widget/RecyclerView;

.field public h:LX/I1i;

.field public final i:LX/Hyh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2529313
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2529314
    new-instance v0, LX/I1k;

    invoke-direct {v0, p0}, LX/I1k;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->i:LX/Hyh;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;

    invoke-static {v3}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    invoke-static {v3}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v2

    check-cast v2, LX/Hyx;

    const-class p0, LX/I1j;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/I1j;

    iput-object v1, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->a:LX/0zG;

    iput-object v2, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->b:LX/Hyx;

    iput-object v3, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->c:LX/I1j;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2529327
    const-string v0, "event_birthdays"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2529328
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2529329
    const-class v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2529330
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->d:Landroid/content/Context;

    .line 2529331
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2529332
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2529333
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2529334
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2529335
    :goto_0
    move-object v2, v3

    .line 2529336
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->a()Ljava/lang/String;

    move-result-object v3

    .line 2529337
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2529338
    const-string v5, "tracking_codes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529339
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->b:LX/Hyx;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->i:LX/Hyh;

    invoke-virtual {v0, v1, v2}, LX/Hyx;->a(ZLX/Hyh;)V

    .line 2529340
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->c:LX/I1j;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529341
    new-instance v4, LX/I1i;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v0}, LX/I21;->b(LX/0QB;)LX/I21;

    move-result-object v3

    check-cast v3, LX/I21;

    invoke-direct {v4, v1, v2, v3}, LX/I1i;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I21;)V

    .line 2529342
    move-object v0, v4

    .line 2529343
    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->h:LX/I1i;

    .line 2529344
    return-void

    :cond_0
    const-string v3, "unknown"

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1a141005

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2529326
    const v1, 0x7f03055a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5abe5de4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e58514

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2529323
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2529324
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08219c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2529325
    const/16 v0, 0x2b

    const v2, -0x6c65236c

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2529315
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2529316
    const v0, 0x7f0d0efd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->g:Landroid/support/v7/widget/RecyclerView;

    .line 2529317
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2529318
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->g:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->h:LX/I1i;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2529319
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;->g:Landroid/support/v7/widget/RecyclerView;

    .line 2529320
    new-instance v1, LX/I1l;

    invoke-direct {v1, p0}, LX/I1l;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardBirthdayFragment;)V

    move-object v1, v1

    .line 2529321
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2529322
    return-void
.end method
