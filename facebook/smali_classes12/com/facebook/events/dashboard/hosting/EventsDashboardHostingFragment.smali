.class public Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I1p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/content/Context;

.field private g:Lcom/facebook/events/common/EventAnalyticsParams;

.field private h:LX/I2H;

.field private i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:LX/I1o;

.field private l:Landroid/support/v7/widget/RecyclerView;

.field public m:Landroid/widget/ProgressBar;

.field private final n:LX/I1u;

.field private final o:LX/I1v;

.field public final p:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2529527
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2529528
    new-instance v0, LX/I1u;

    invoke-direct {v0, p0}, LX/I1u;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->n:LX/I1u;

    .line 2529529
    new-instance v0, LX/I1v;

    invoke-direct {v0, p0}, LX/I1v;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->o:LX/I1v;

    .line 2529530
    new-instance v0, LX/I1q;

    invoke-direct {v0, p0}, LX/I1q;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->p:LX/0Vd;

    .line 2529531
    return-void
.end method

.method private static a(LX/I2H;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2529523
    sget-object v0, LX/I1t;->a:[I

    invoke-virtual {p0}, LX/I2H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2529524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported sectionType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2529525
    :pswitch_0
    const-string v0, "PUBLISHED"

    .line 2529526
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "DRAFT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p0    # Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529511
    if-nez p1, :cond_1

    .line 2529512
    :cond_0
    :goto_0
    return-void

    .line 2529513
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529514
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v1

    .line 2529515
    if-eqz v1, :cond_0

    .line 2529516
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v2

    .line 2529517
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2529518
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->j:Z

    .line 2529519
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2529520
    iput-object v1, v0, LX/Hyx;->a:Ljava/lang/String;

    .line 2529521
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    invoke-virtual {v0, v2}, LX/I1o;->a(Ljava/util/List;)V

    goto :goto_0

    .line 2529522
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2529502
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 2529503
    :cond_0
    :goto_0
    return-void

    .line 2529504
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2529505
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529506
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529507
    iput-object v4, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2529508
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2529509
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->c:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_HOSTING_EVENTS:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I1r;

    invoke-direct {v3, p0}, LX/I1r;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2529510
    iput-object v4, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static e(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V
    .locals 7

    .prologue
    .line 2529460
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b:LX/Hyx;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    iget-boolean v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->j:Z

    const/16 v3, 0xe

    iget-object v4, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0f74

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->h:LX/I2H;

    invoke-static {v5}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a(LX/I2H;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    const-string v6, "HOST"

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2529461
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2529501
    const-string v0, "event_dashboard_all_hosting"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2529481
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2529482
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v0}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v4

    check-cast v4, LX/Hyx;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    const-class p1, LX/I1p;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/I1p;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v0

    check-cast v0, LX/Bl6;

    iput-object v3, v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b:LX/Hyx;

    iput-object v5, v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->c:LX/1Ck;

    iput-object p1, v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->d:LX/I1p;

    iput-object v0, v2, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e:LX/Bl6;

    .line 2529483
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->f:Landroid/content/Context;

    .line 2529484
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2529485
    const-string v1, "extra_events_hosting_dashboard_section_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2H;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->h:LX/I2H;

    .line 2529486
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2529487
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2529488
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2529489
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2529490
    :goto_0
    move-object v2, v3

    .line 2529491
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a()Ljava/lang/String;

    move-result-object v3

    .line 2529492
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2529493
    const-string v5, "tracking_codes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529494
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->d:LX/I1p;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->g:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1}, LX/I1p;->a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I1o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    .line 2529495
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b:LX/Hyx;

    invoke-virtual {v0}, LX/Hyx;->a()V

    .line 2529496
    invoke-static {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    .line 2529497
    invoke-static {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->b(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    .line 2529498
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->n:LX/I1u;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2529499
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->o:LX/I1v;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2529500
    return-void

    :cond_0
    const-string v3, "unknown"

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x32ff8e09

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2529480
    const v1, 0x7f03055a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x68bf515b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4013d079

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2529474
    iput-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->l:Landroid/support/v7/widget/RecyclerView;

    .line 2529475
    iput-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    .line 2529476
    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->n:LX/I1u;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2529477
    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->e:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->o:LX/I1v;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2529478
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2529479
    const/16 v1, 0x2b

    const v2, 0xcff345e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x21ba81d7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2529471
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2529472
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->h:LX/I2H;

    invoke-static {v2, v3}, LX/I2F;->a(Landroid/content/Context;LX/I2H;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2529473
    const/16 v0, 0x2b

    const v2, 0x26a53e6f

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2529462
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2529463
    const v0, 0x7f0d0efd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->l:Landroid/support/v7/widget/RecyclerView;

    .line 2529464
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->l:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2529465
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->l:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->k:LX/I1o;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2529466
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->l:Landroid/support/v7/widget/RecyclerView;

    .line 2529467
    new-instance v1, LX/I1s;

    invoke-direct {v1, p0}, LX/I1s;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;)V

    move-object v1, v1

    .line 2529468
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2529469
    const v0, 0x7f0d0efc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;->m:Landroid/widget/ProgressBar;

    .line 2529470
    return-void
.end method
