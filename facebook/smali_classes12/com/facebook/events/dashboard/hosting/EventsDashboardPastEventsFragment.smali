.class public Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I1p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:Lcom/facebook/events/common/EventAnalyticsParams;

.field private g:LX/I2H;

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:LX/I1o;

.field private k:Landroid/support/v7/widget/RecyclerView;

.field public l:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2529631
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-static {v4}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    invoke-static {v4}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v2

    check-cast v2, LX/Hyx;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const-class p0, LX/I1p;

    invoke-interface {v4, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/I1p;

    iput-object v1, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a:LX/0zG;

    iput-object v2, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b:LX/Hyx;

    iput-object v3, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->c:LX/1Ck;

    iput-object v4, p1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->d:LX/I1p;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p0    # Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529619
    if-nez p1, :cond_1

    .line 2529620
    :cond_0
    :goto_0
    return-void

    .line 2529621
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529622
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;

    move-result-object v1

    .line 2529623
    if-eqz v1, :cond_0

    .line 2529624
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v2

    .line 2529625
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2529626
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->i:Z

    .line 2529627
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2529628
    iput-object v1, v0, LX/Hyx;->b:Ljava/lang/String;

    .line 2529629
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    invoke-virtual {v0, v2}, LX/I1o;->a(Ljava/util/List;)V

    goto :goto_0

    .line 2529630
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2529610
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    .line 2529611
    :cond_0
    :goto_0
    return-void

    .line 2529612
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2529613
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529614
    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a$redex0(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529615
    iput-object v4, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2529616
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2529617
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->c:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_PAST_EVENTS:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I1x;

    invoke-direct {v3, p0}, LX/I1x;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2529618
    iput-object v4, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static e(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V
    .locals 6

    .prologue
    .line 2529608
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b:LX/Hyx;

    iget-boolean v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->i:Z

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0f74

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const-string v4, "PUBLISHED"

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "HOST"

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Hyx;->a(ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2529609
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2529607
    const-string v0, "event_dashboard_all_past"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2529589
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2529590
    const-class v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2529591
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->e:Landroid/content/Context;

    .line 2529592
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2529593
    const-string v1, "extra_events_hosting_dashboard_section_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2H;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->g:LX/I2H;

    .line 2529594
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2529595
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2529596
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2529597
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2529598
    :goto_0
    move-object v2, v3

    .line 2529599
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a()Ljava/lang/String;

    move-result-object v3

    .line 2529600
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2529601
    const-string v5, "tracking_codes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529602
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->d:LX/I1p;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1}, LX/I1p;->a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I1o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    .line 2529603
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b:LX/Hyx;

    invoke-virtual {v0}, LX/Hyx;->a()V

    .line 2529604
    invoke-static {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->e(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    .line 2529605
    invoke-static {p0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    .line 2529606
    return-void

    :cond_0
    const-string v3, "unknown"

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x70de49d7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2529572
    const v1, 0x7f03055a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1e505564

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2647cc7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2529585
    iput-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->k:Landroid/support/v7/widget/RecyclerView;

    .line 2529586
    iput-object v2, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    .line 2529587
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2529588
    const/16 v1, 0x2b

    const v2, 0x4fce0938

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x23f084a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2529582
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2529583
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->g:LX/I2H;

    invoke-static {v2, v3}, LX/I2F;->a(Landroid/content/Context;LX/I2H;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2529584
    const/16 v0, 0x2b

    const v2, -0x59680d38

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2529573
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2529574
    const v0, 0x7f0d0efd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->k:Landroid/support/v7/widget/RecyclerView;

    .line 2529575
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->k:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2529576
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->k:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2529577
    iget-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->k:Landroid/support/v7/widget/RecyclerView;

    .line 2529578
    new-instance v1, LX/I1y;

    invoke-direct {v1, p0}, LX/I1y;-><init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    move-object v1, v1

    .line 2529579
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2529580
    const v0, 0x7f0d0efc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->l:Landroid/widget/ProgressBar;

    .line 2529581
    return-void
.end method
