.class public Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public A:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final B:LX/HzV;

.field private final C:LX/HzS;

.field private final D:LX/HzT;

.field private final E:LX/HzU;

.field private final F:LX/HzH;

.field private final G:LX/HzI;

.field public final H:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final I:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public e:Landroid/widget/ProgressBar;

.field public f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

.field public g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

.field public h:Landroid/view/View;

.field public i:LX/1P0;

.field public j:LX/HzD;

.field private k:LX/1OM;

.field public l:Lcom/facebook/events/common/EventAnalyticsParams;

.field public m:LX/Hx7;

.field private n:LX/HzL;

.field private o:LX/HzW;

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I0m;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bie;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/I26;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/I1A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/HzX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2525681
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2525682
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2525683
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->p:LX/0Ot;

    .line 2525684
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2525685
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->q:LX/0Ot;

    .line 2525686
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2525687
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->r:LX/0Ot;

    .line 2525688
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2525689
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->s:LX/0Ot;

    .line 2525690
    new-instance v0, LX/HzV;

    invoke-direct {v0, p0}, LX/HzV;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->B:LX/HzV;

    .line 2525691
    new-instance v0, LX/HzS;

    invoke-direct {v0, p0}, LX/HzS;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->C:LX/HzS;

    .line 2525692
    new-instance v0, LX/HzT;

    invoke-direct {v0, p0}, LX/HzT;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->D:LX/HzT;

    .line 2525693
    new-instance v0, LX/HzU;

    invoke-direct {v0, p0}, LX/HzU;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->E:LX/HzU;

    .line 2525694
    new-instance v0, LX/HzH;

    invoke-direct {v0, p0}, LX/HzH;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->F:LX/HzH;

    .line 2525695
    new-instance v0, LX/HzI;

    invoke-direct {v0, p0}, LX/HzI;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->G:LX/HzI;

    .line 2525696
    new-instance v0, LX/HzJ;

    invoke-direct {v0, p0}, LX/HzJ;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->H:LX/0Vd;

    .line 2525697
    new-instance v0, LX/HzK;

    invoke-direct {v0, p0}, LX/HzK;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->I:LX/0Vd;

    .line 2525698
    return-void
.end method

.method public static b$redex0(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;LX/Hx7;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2525633
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->m:LX/Hx7;

    if-ne v0, p1, :cond_0

    .line 2525634
    :goto_0
    return-void

    .line 2525635
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2525636
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->m:LX/Hx7;

    .line 2525637
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setSelectedTabType(LX/Hx7;)V

    .line 2525638
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_1

    .line 2525639
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2525640
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    if-eqz v0, :cond_2

    .line 2525641
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    invoke-interface {v0}, LX/HzD;->c()V

    .line 2525642
    :cond_2
    sget-object v0, LX/HzG;->a:[I

    invoke-virtual {p1}, LX/Hx7;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2525643
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->w:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2525644
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    sget-object v0, LX/Hx7;->CALENDAR:LX/Hx7;

    if-eq p1, v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;->setEnabled(Z)V

    .line 2525645
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    .line 2525646
    new-instance v2, LX/HzE;

    invoke-direct {v2, p0}, LX/HzE;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    move-object v2, v2

    .line 2525647
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2525648
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    new-instance v2, LX/HzP;

    invoke-direct {v2, p0}, LX/HzP;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    .line 2525649
    iput-object v2, v0, Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;->c:LX/HzO;

    .line 2525650
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->i:LX/1P0;

    .line 2525651
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->i:LX/1P0;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2525652
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525653
    new-instance v2, LX/HzR;

    invoke-direct {v2, p0}, LX/HzR;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    move-object v2, v2

    .line 2525654
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2525655
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2525656
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->B:LX/HzV;

    invoke-interface {v0, v1, v2}, LX/HzD;->a(Lcom/facebook/events/common/EventAnalyticsParams;LX/HzV;)V

    .line 2525657
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    invoke-interface {v0}, LX/HzD;->a()LX/1OM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->k:LX/1OM;

    .line 2525658
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->k:LX/1OM;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2525659
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->F:LX/HzH;

    invoke-interface {v0, v1}, LX/HzD;->a(LX/HzH;)V

    .line 2525660
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->G:LX/HzI;

    invoke-interface {v0, v1}, LX/HzD;->a(LX/HzI;)V

    .line 2525661
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    invoke-interface {v0}, LX/HzD;->b()V

    goto/16 :goto_0

    .line 2525662
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->t:LX/I1A;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->n:LX/HzL;

    .line 2525663
    new-instance v3, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/I1G;->b(LX/0QB;)LX/I1G;

    move-result-object v6

    check-cast v6, LX/I1G;

    const-class v4, LX/I1g;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/I1g;

    const/16 v4, 0x161a

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/I5E;->b(LX/0QB;)LX/I5E;

    move-result-object v9

    check-cast v9, LX/I5E;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v11

    check-cast v11, LX/1nQ;

    move-object v4, v2

    invoke-direct/range {v3 .. v11}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;-><init>(LX/HzL;Landroid/content/Context;LX/I1G;LX/I1g;LX/0Or;LX/I5E;LX/1Ck;LX/1nQ;)V

    .line 2525664
    move-object v0, v3

    .line 2525665
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    .line 2525666
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_3

    .line 2525667
    const v0, 0x7f0d0f3e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2525668
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525669
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    goto/16 :goto_1

    .line 2525670
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HzD;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    .line 2525671
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_4

    .line 2525672
    const v0, 0x7f0d0f40

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2525673
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525674
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    goto/16 :goto_1

    .line 2525675
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HzD;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    .line 2525676
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_5

    .line 2525677
    const v0, 0x7f0d0f42

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2525678
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525679
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 2525680
    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2525632
    const-string v0, "event_dashboard"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2525606
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2525607
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    const/16 v4, 0x1b0e

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1ae8

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1b18

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x19c6

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, LX/I1A;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/I1A;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v9

    check-cast v9, LX/Bl6;

    new-instance v12, LX/HzX;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    invoke-direct {v12, v10, v11}, LX/HzX;-><init>(LX/0tX;LX/0hB;)V

    move-object v10, v12

    check-cast v10, LX/HzX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v13

    check-cast v13, LX/1nQ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    iput-object v4, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->p:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->q:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->r:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->s:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->t:LX/I1A;

    iput-object v9, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iput-object v10, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->v:LX/HzX;

    iput-object v11, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->w:LX/1Ck;

    iput-object v12, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->x:LX/0tX;

    iput-object v13, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->y:LX/1nQ;

    iput-object p1, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->z:LX/0ad;

    iput-object v0, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->A:LX/0zG;

    .line 2525608
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2525609
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2525610
    if-eqz v1, :cond_0

    const-string v3, "action_ref"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/common/ActionSource;

    if-nez v3, :cond_2

    .line 2525611
    :cond_0
    sget-object v3, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2525612
    :goto_0
    move-object v1, v3

    .line 2525613
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2525614
    const-string v4, "extra_ref_module"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2525615
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2525616
    :goto_1
    move-object v3, v3

    .line 2525617
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a()Ljava/lang/String;

    move-result-object v4

    .line 2525618
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2525619
    const-string v6, "tracking_codes"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2525620
    new-instance v0, LX/HzL;

    invoke-direct {v0, p0}, LX/HzL;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->n:LX/HzL;

    .line 2525621
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->C:LX/HzS;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525622
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->D:LX/HzT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525623
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->E:LX/HzU;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525624
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->z:LX/0ad;

    sget-short v1, LX/347;->m:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2525625
    new-instance v0, LX/HzW;

    invoke-direct {v0, p0}, LX/HzW;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->o:LX/HzW;

    .line 2525626
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->o:LX/HzW;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2525627
    new-instance v0, LX/7oP;

    invoke-direct {v0}, LX/7oP;-><init>()V

    move-object v0, v0

    .line 2525628
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2525629
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->x:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2525630
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->w:LX/1Ck;

    sget-object v2, LX/Hy9;->FETCH_EVENT_COUNTS:LX/Hy9;

    new-instance v3, LX/HzF;

    invoke-direct {v3, p0}, LX/HzF;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2525631
    :cond_1
    return-void

    :cond_2
    new-instance v4, Lcom/facebook/events/common/EventActionContext;

    sget-object v5, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v3, v6}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    move-object v3, v4

    goto :goto_0

    :cond_3
    const-string v3, "unknown"

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2525602
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2525603
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2525604
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525605
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x688b36e6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2525601
    const v1, 0x7f03057e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4354c579

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x12ec2a7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2525585
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->w:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2525586
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->h:Landroid/view/View;

    .line 2525587
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    .line 2525588
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525589
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525590
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525591
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2525592
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->e:Landroid/widget/ProgressBar;

    .line 2525593
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->i:LX/1P0;

    .line 2525594
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    .line 2525595
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->j:LX/HzD;

    invoke-interface {v1}, LX/HzD;->c()V

    .line 2525596
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->C:LX/HzS;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2525597
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->D:LX/HzT;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2525598
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->u:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->E:LX/HzU;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2525599
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2525600
    const/16 v1, 0x2b

    const v2, 0x7eb1311

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2525555
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2525556
    const-string v0, "extra_key_dashboard_tab_type"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->m:LX/Hx7;

    invoke-virtual {v1}, LX/Hx7;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2525557
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x34e0da37

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2525582
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2525583
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->A:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f082193

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2525584
    const/16 v0, 0x2b

    const v2, -0x53df4537

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2525558
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2525559
    const v0, 0x7f0d0f3d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->e:Landroid/widget/ProgressBar;

    .line 2525560
    const v0, 0x7f0d0f3c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->f:Lcom/facebook/events/dashboard/EventsHeroDashboardSwipeRefreshLayout;

    .line 2525561
    const v0, 0x7f0d0f3b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    .line 2525562
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    new-instance v1, LX/HzN;

    invoke-direct {v1, p0}, LX/HzN;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    .line 2525563
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->d:LX/HzM;

    .line 2525564
    const v0, 0x7f0d0f44

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->h:Landroid/view/View;

    .line 2525565
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->h:Landroid/view/View;

    new-instance v1, LX/HzQ;

    invoke-direct {v1, p0}, LX/HzQ;-><init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2525566
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2525567
    if-eqz p2, :cond_2

    .line 2525568
    const-string v0, "extra_key_dashboard_tab_type"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2525569
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2525570
    invoke-static {v0}, LX/Hx7;->valueOf(Ljava/lang/String;)LX/Hx7;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;LX/Hx7;)V

    .line 2525571
    :goto_1
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->y:LX/1nQ;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    .line 2525572
    iget-object p0, v1, LX/1nQ;->i:LX/0Zb;

    const-string p1, "view"

    const/4 p2, 0x0

    invoke-interface {p0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2525573
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2525574
    const-string p1, "tracking"

    invoke-virtual {p0, p1, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2525575
    :cond_0
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2525576
    const-string p1, "event_dashboard"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    iget-object p1, v1, LX/1nQ;->j:LX/0kv;

    iget-object p2, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p1, p2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "ref_module"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "ref_mechanism"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    const-string p1, "events_dashboard_tab_type"

    invoke-virtual {p0, p1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2525577
    :cond_1
    return-void

    .line 2525578
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2525579
    const-string v1, "extra_key_dashboard_tab_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2525580
    :cond_3
    sget-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-static {p0, v0}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;LX/Hx7;)V

    .line 2525581
    sget-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {v0}, LX/Hx7;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
