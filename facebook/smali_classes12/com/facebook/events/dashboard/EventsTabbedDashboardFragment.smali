.class public Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Hws;
.implements LX/0o8;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/events/common/EventAnalyticsParams;

.field public B:LX/Hx7;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:LX/1P0;

.field public F:Landroid/os/Parcelable;

.field public G:Landroid/os/Parcelable;

.field public final H:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final I:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final J:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final K:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final L:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final M:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final N:LX/Hxk;

.field public a:LX/7v8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I08;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Hyx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/I5E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/I5M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Bie;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Hzu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Cfr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3Fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final o:LX/0m9;

.field public p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public q:Landroid/view/View;

.field public r:LX/Hzt;

.field public s:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

.field public t:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private u:Landroid/content/Context;

.field private v:LX/2jY;

.field public w:LX/I05;

.field public x:LX/I05;

.field public y:Z

.field public z:LX/2ja;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2526088
    const-class v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2526089
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2526090
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->o:LX/0m9;

    .line 2526091
    sget-object v0, LX/I05;->INITIAL:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    .line 2526092
    sget-object v0, LX/I05;->INITIAL:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    .line 2526093
    new-instance v0, LX/Hzh;

    invoke-direct {v0, p0}, LX/Hzh;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->H:LX/0Vd;

    .line 2526094
    new-instance v0, LX/Hzi;

    invoke-direct {v0, p0}, LX/Hzi;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->I:LX/0Vd;

    .line 2526095
    new-instance v0, LX/Hzj;

    invoke-direct {v0, p0}, LX/Hzj;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->J:Ljava/util/concurrent/Callable;

    .line 2526096
    new-instance v0, LX/Hzk;

    invoke-direct {v0, p0}, LX/Hzk;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->K:LX/0Vd;

    .line 2526097
    new-instance v0, LX/Hzl;

    invoke-direct {v0, p0}, LX/Hzl;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->L:Ljava/util/concurrent/Callable;

    .line 2526098
    new-instance v0, LX/Hzm;

    invoke-direct {v0, p0}, LX/Hzm;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->M:LX/0Vd;

    .line 2526099
    new-instance v0, LX/Hzn;

    invoke-direct {v0, p0}, LX/Hzn;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->N:LX/Hxk;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-static {p0}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v2

    check-cast v2, LX/7v8;

    invoke-static {p0}, LX/I08;->b(LX/0QB;)LX/I08;

    move-result-object v3

    check-cast v3, LX/I08;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v5

    check-cast v5, LX/Hyx;

    invoke-static {p0}, LX/I5E;->b(LX/0QB;)LX/I5E;

    move-result-object v6

    check-cast v6, LX/I5E;

    invoke-static {p0}, LX/I5M;->b(LX/0QB;)LX/I5M;

    move-result-object v7

    check-cast v7, LX/I5M;

    invoke-static {p0}, LX/Bie;->b(LX/0QB;)LX/Bie;

    move-result-object v8

    check-cast v8, LX/Bie;

    const-class v9, LX/Hzu;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Hzu;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    const-class v12, LX/Cfr;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/Cfr;

    invoke-static {p0}, LX/3Fx;->a(LX/0QB;)LX/3Fx;

    move-result-object v13

    check-cast v13, LX/3Fx;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v2, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a:LX/7v8;

    iput-object v3, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->b:LX/I08;

    iput-object v4, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->c:LX/0SG;

    iput-object v5, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    iput-object v6, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->e:LX/I5E;

    iput-object v7, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->f:LX/I5M;

    iput-object v8, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->g:LX/Bie;

    iput-object v9, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->h:LX/Hzu;

    iput-object v10, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->i:LX/0Uh;

    iput-object v11, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->j:LX/0So;

    iput-object v12, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->k:LX/Cfr;

    iput-object v13, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->l:LX/3Fx;

    iput-object p0, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->m:LX/1Ck;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2526100
    invoke-static {p0, v1}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;Z)V

    .line 2526101
    if-eqz p1, :cond_6

    .line 2526102
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526103
    if-eqz v0, :cond_6

    .line 2526104
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526105
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2526106
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526107
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2526108
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2526109
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526110
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2526111
    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2526112
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2526113
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2526114
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2526115
    :cond_1
    sget-object v0, LX/I05;->LOADED:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    .line 2526116
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2526117
    iget-object v6, v0, LX/Hzt;->f:LX/I2i;

    .line 2526118
    if-nez v2, :cond_7

    .line 2526119
    iget-object v7, v6, LX/I2i;->d:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 2526120
    iget-object v7, v6, LX/I2i;->g:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 2526121
    :cond_2
    iget-object v7, v6, LX/I2i;->j:LX/Hx7;

    sget-object v8, LX/Hx7;->DISCOVER:LX/Hx7;

    if-ne v7, v8, :cond_3

    .line 2526122
    invoke-static {v6}, LX/I2i;->a(LX/I2i;)V

    .line 2526123
    :cond_3
    if-eqz v2, :cond_4

    .line 2526124
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2526125
    invoke-static {v9}, LX/CfY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/Set;

    move-result-object v13

    .line 2526126
    new-instance v8, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v10, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v11, 0x0

    invoke-direct/range {v8 .. v13}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2526127
    iget-object v10, v0, LX/Hzt;->h:LX/1My;

    iget-object v11, v0, LX/Hzt;->g:LX/0TF;

    invoke-virtual {v9}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v11, v9, v8}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1

    .line 2526128
    :cond_4
    invoke-static {v0}, LX/Hzt;->k(LX/Hzt;)V

    .line 2526129
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    sget-object v1, LX/Hx7;->DISCOVER:LX/Hx7;

    if-ne v0, v1, :cond_5

    .line 2526130
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    invoke-virtual {v0, v1}, LX/Hzt;->a(LX/I05;)V

    .line 2526131
    :cond_5
    return-void

    .line 2526132
    :cond_6
    sget-object v0, LX/I05;->ERROR:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    goto :goto_2

    .line 2526133
    :cond_7
    iput-object v2, v6, LX/I2i;->d:Ljava/util/List;

    .line 2526134
    const/4 v7, 0x0

    move v8, v7

    :goto_3
    iget-object v7, v6, LX/I2i;->d:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v8, v7, :cond_2

    .line 2526135
    iget-object v9, v6, LX/I2i;->g:Ljava/util/HashMap;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2526136
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;Z)V
    .locals 2

    .prologue
    .line 2526137
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2526138
    iget-boolean v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    move v0, v1

    .line 2526139
    if-eq v0, p1, :cond_0

    .line 2526140
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2526141
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 5

    .prologue
    .line 2526146
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    sget-object v1, LX/I05;->INITIAL:LX/I05;

    if-ne v0, v1, :cond_0

    .line 2526147
    sget-object v0, LX/I05;->FIRST_LOAD:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    .line 2526148
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->i:LX/0Uh;

    const/16 v1, 0x3a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526149
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->e:LX/I5E;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->I:LX/0Vd;

    sget-object v3, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/I5E;->a(Landroid/app/Activity;LX/0Vd;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2526150
    :goto_0
    return-void

    .line 2526151
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->f:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->H:LX/0Vd;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->o:LX/0m9;

    const-string v4, "events_tabbed_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 4

    .prologue
    .line 2526142
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    sget-object v1, LX/I05;->INITIAL:LX/I05;

    if-ne v0, v1, :cond_0

    .line 2526143
    sget-object v0, LX/I05;->FIRST_LOAD:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    .line 2526144
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->m:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_UPCOMING_EVENTS:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->J:Ljava/util/concurrent/Callable;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->K:LX/0Vd;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2526145
    return-void
.end method

.method public static p(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 2

    .prologue
    .line 2526192
    sget-object v0, LX/I05;->ERROR:LX/I05;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    .line 2526193
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    sget-object v1, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne v0, v1, :cond_0

    .line 2526194
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    invoke-virtual {v0, v1}, LX/Hzt;->a(LX/I05;)V

    .line 2526195
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2526196
    const-string v0, "event_dashboard"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2526152
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2526153
    const-class v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2526154
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2526155
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2526156
    if-eqz v1, :cond_0

    const-string v2, "action_ref"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/common/ActionSource;

    if-nez v2, :cond_1

    .line 2526157
    :cond_0
    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2526158
    :goto_0
    move-object v1, v2

    .line 2526159
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2526160
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2526161
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2526162
    :goto_1
    move-object v2, v2

    .line 2526163
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->A:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2526164
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2526165
    if-eqz p1, :cond_3

    const-string v1, "extra_dashboard_tab_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2526166
    const-string v0, "extra_dashboard_tab_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx7;->valueOf(Ljava/lang/String;)LX/Hx7;

    move-result-object v0

    .line 2526167
    :goto_2
    move-object v0, v0

    .line 2526168
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    .line 2526169
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2526170
    if-eqz p1, :cond_5

    const-string v1, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2526171
    const-string v1, "birthday_view_waterfall_id_param"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2526172
    :goto_3
    move-object v0, v1

    .line 2526173
    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->D:Ljava/lang/String;

    .line 2526174
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    invoke-static {v0, v1}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->u:Landroid/content/Context;

    .line 2526175
    new-instance v0, LX/1P0;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->u:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    .line 2526176
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->m:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_UPCOMING_EVENTS:LX/Hy9;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->L:Ljava/util/concurrent/Callable;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->M:LX/0Vd;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2526177
    invoke-static {p0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->b(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2526178
    new-instance v11, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    invoke-direct {v11, v7}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2526179
    iget-object v7, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->c:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v11, v7, v8}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 2526180
    iget-object v7, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->b:LX/I08;

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->C:Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v12, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->N:LX/Hxk;

    invoke-virtual/range {v7 .. v12}, LX/I08;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V

    .line 2526181
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->f:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    invoke-virtual {v0, v1}, LX/I5M;->a(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->v:LX/2jY;

    .line 2526182
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->k:LX/Cfr;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->v:LX/2jY;

    invoke-virtual {v0, v1, v4}, LX/Cfr;->a(LX/2jY;LX/1P1;)LX/2ja;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    .line 2526183
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->h:LX/Hzu;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->u:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->A:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->v:LX/2jY;

    iget-object v6, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, LX/Hzu;->a(LX/Hx7;Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;LX/2jY;LX/2ja;)LX/Hzt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2526184
    return-void

    :cond_1
    new-instance v3, Lcom/facebook/events/common/EventActionContext;

    sget-object v5, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    const/4 v6, 0x0

    invoke-direct {v3, v5, v2, v6}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_2
    const-string v2, "unknown"

    goto/16 :goto_1

    .line 2526185
    :cond_3
    if-eqz v0, :cond_4

    const-string v1, "extra_dashboard_tab_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2526186
    const-string v1, "extra_dashboard_tab_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Hx7;->valueOf(Ljava/lang/String;)LX/Hx7;

    move-result-object v0

    goto/16 :goto_2

    .line 2526187
    :cond_4
    sget-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    goto/16 :goto_2

    .line 2526188
    :cond_5
    const/4 v1, 0x0

    .line 2526189
    if-eqz v0, :cond_6

    .line 2526190
    const-string v1, "birthday_view_referrer_param"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2526191
    :cond_6
    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a:LX/7v8;

    if-nez v1, :cond_7

    const-string v1, ""

    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/7v8;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2526086
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2526087
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2526028
    const/4 v0, 0x0

    return-object v0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2526029
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2526030
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 0

    .prologue
    .line 2526031
    return-object p0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2526032
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->v:LX/2jY;

    .line 2526033
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2526034
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2526035
    const-string v0, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6484760e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2526036
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->u:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03058d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5bf11599

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0xd3a7c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2526037
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2526038
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    invoke-virtual {v1}, LX/2ja;->e()V

    .line 2526039
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    if-eqz v1, :cond_0

    .line 2526040
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2526041
    iget-object v4, v1, LX/Hzt;->h:LX/1My;

    invoke-virtual {v4}, LX/1My;->a()V

    .line 2526042
    :cond_0
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2526043
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2526044
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->s:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    .line 2526045
    iput-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->q:Landroid/view/View;

    .line 2526046
    const/16 v1, 0x2b

    const v2, -0x1f958114

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6620d25f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2526047
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2526048
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    if-eqz v1, :cond_0

    .line 2526049
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2526050
    iget-object v2, v1, LX/Hzt;->h:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 2526051
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    if-eqz v1, :cond_1

    .line 2526052
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->j:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2ja;->d(J)V

    .line 2526053
    :cond_1
    const/16 v1, 0x2b

    const v2, 0xf9152d7

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xbe13c2b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2526054
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2526055
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    if-eqz v1, :cond_0

    .line 2526056
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2526057
    iget-object v2, v1, LX/Hzt;->h:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 2526058
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    if-eqz v1, :cond_1

    .line 2526059
    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->j:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2ja;->e(J)V

    .line 2526060
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x36df950d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2526061
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2526062
    const-string v0, "extra_dashboard_tab_type"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    invoke-virtual {v1}, LX/Hx7;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2526063
    const-string v0, "birthday_view_waterfall_id_param"

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2526064
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526065
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2526066
    const v0, 0x7f0d0f0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2526067
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2526068
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2526069
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2526070
    new-instance v1, LX/Hzf;

    invoke-direct {v1, p0}, LX/Hzf;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    move-object v1, v1

    .line 2526071
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2526072
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    iget-object v2, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->j:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2ja;->a(LX/1P1;JZ)V

    .line 2526073
    const v0, 0x7f0d0efe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2526074
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0a00d1

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2526075
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->t:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2526076
    new-instance v1, LX/Hzp;

    invoke-direct {v1, p0}, LX/Hzp;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    move-object v1, v1

    .line 2526077
    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2526078
    const v0, 0x7f0d0f53

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->s:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    .line 2526079
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->s:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    new-instance v1, LX/Hzo;

    invoke-direct {v1, p0}, LX/Hzo;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2526080
    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->d:LX/HzM;

    .line 2526081
    const v0, 0x7f0d08df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->q:Landroid/view/View;

    .line 2526082
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->q:Landroid/view/View;

    new-instance v1, LX/Hzg;

    invoke-direct {v1, p0}, LX/Hzg;-><init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2526083
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2526084
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Hzt;->a(Ljava/lang/Boolean;)V

    .line 2526085
    return-void
.end method
