.class public Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/I3K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I3Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Bm1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/widget/listview/BetterListView;

.field public h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

.field public i:LX/I3J;

.field public j:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2531415
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2531409
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2531410
    const-string v1, "events_suggestions_cut_diplay_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531411
    const-string v1, "events_suggestions_cut_type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531412
    const-string v1, "extras_event_action_context"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2531413
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531414
    return-object v0
.end method

.method public static b(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V
    .locals 12

    .prologue
    .line 2531401
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2531402
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->b()LX/0ut;

    move-result-object v0

    invoke-interface {v0}, LX/0ut;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2531403
    :goto_0
    return-void

    .line 2531404
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->d:LX/I3Y;

    const/16 v2, 0xa

    invoke-direct {p0}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    new-instance v5, LX/I3Q;

    invoke-direct {v5, p0}, LX/I3Q;-><init>(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V

    .line 2531405
    new-instance v6, LX/I3V;

    move-object v7, v0

    move-object v8, v3

    move-object v9, v4

    move v10, v2

    move v11, v1

    invoke-direct/range {v6 .. v11}, LX/I3V;-><init>(LX/I3Y;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2531406
    new-instance v7, LX/I3W;

    invoke-direct {v7, v0, v5}, LX/I3W;-><init>(LX/I3Y;LX/Hxb;)V

    .line 2531407
    iget-object v8, v0, LX/I3Y;->b:LX/1Ck;

    sget-object v9, LX/I3X;->FETCH_EVENTS_SUGGESTIONS_FOR_CUT:LX/I3X;

    invoke-virtual {v8, v9, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2531408
    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->b()LX/0ut;

    move-result-object v4

    invoke-interface {v4}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2531394
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531395
    if-eqz v0, :cond_0

    .line 2531396
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531397
    const-string v1, "events_suggestions_cut_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2531398
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531399
    const-string v1, "events_suggestions_cut_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2531400
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot load suggestions fragment without cut type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531416
    const-string v0, "event_suggestions"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2531379
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2531380
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v4

    check-cast v4, LX/1nQ;

    const-class v5, LX/I3K;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/I3K;

    invoke-static {v0}, LX/I3Y;->b(LX/0QB;)LX/I3Y;

    move-result-object v6

    check-cast v6, LX/I3Y;

    invoke-static {v0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object p1

    check-cast p1, LX/Bm1;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v3, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b:LX/1nQ;

    iput-object v5, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->c:LX/I3K;

    iput-object v6, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->d:LX/I3Y;

    iput-object p1, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->e:LX/Bm1;

    iput-object v0, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2531381
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531382
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2531383
    const-string v2, "extras_event_action_context"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/common/EventActionContext;

    .line 2531384
    sget-object v2, Lcom/facebook/events/common/ActionSource;->MOBILE_SUGGESTIONS_LIST:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v1, v2}, Lcom/facebook/events/common/EventActionContext;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventActionContext;

    move-result-object v1

    move-object v1, v1

    .line 2531385
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2531386
    const-string v3, "extra_ref_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2531387
    if-eqz v2, :cond_0

    .line 2531388
    :goto_0
    move-object v2, v2

    .line 2531389
    invoke-virtual {p0}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531390
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->c:LX/I3K;

    iget-object v1, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1}, LX/I3K;->a(Lcom/facebook/events/common/EventAnalyticsParams;)LX/I3J;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    .line 2531391
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000f

    const-string v2, "EventsSuggestionsFragment"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2531392
    invoke-static {p0}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V

    .line 2531393
    return-void

    :cond_0
    const-string v2, "unknown"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x302974ab

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2531373
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08eb

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2531374
    const v2, 0x7f030584

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 2531375
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2531376
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I3N;

    invoke-direct {v2, p0}, LX/I3N;-><init>(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2531377
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/I3O;

    invoke-direct {v2, p0}, LX/I3O;-><init>(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2531378
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    const/16 v2, 0x2b

    const v3, 0x2f08db03

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x62c9afde

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2531368
    iget-object v1, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b:LX/1nQ;

    invoke-virtual {p0}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    invoke-virtual {v3}, LX/I3J;->a()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531369
    iget-object v6, v4, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v4, v6

    .line 2531370
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, LX/1nQ;->a(Ljava/lang/String;II)V

    .line 2531371
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2531372
    const/16 v1, 0x2b

    const v2, -0x3b8a9512

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4fd293aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2531354
    iget-object v1, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->d:LX/I3Y;

    invoke-virtual {v1}, LX/I3Y;->a()V

    .line 2531355
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    .line 2531356
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2531357
    const/16 v1, 0x2b

    const v2, -0x39f0a1b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5d905b1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2531358
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2531359
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531360
    if-eqz v0, :cond_0

    .line 2531361
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2531362
    const-string v2, "events_suggestions_cut_diplay_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2531363
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2531364
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2531365
    const-string v3, "events_suggestions_cut_diplay_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2531366
    :goto_0
    const v0, 0x4a502964    # 3410521.0f

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2531367
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0821b5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method
