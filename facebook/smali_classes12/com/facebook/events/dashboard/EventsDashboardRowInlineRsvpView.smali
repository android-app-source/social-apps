.class public Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/events/model/Event;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/view/View$OnClickListener;

.field private k:Landroid/view/View$OnClickListener;

.field public l:LX/HyD;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2523733
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2523734
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a()V

    .line 2523735
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2523730
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2523731
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a()V

    .line 2523732
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2523693
    new-instance v0, LX/HyB;

    invoke-direct {v0, p0, p1}, LX/HyB;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2523729
    new-instance v0, LX/HyC;

    invoke-direct {v0, p0, p1}, LX/HyC;-><init>(Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2523717
    const-class v0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    invoke-static {v0, p0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2523718
    const v0, 0x7f03055d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2523719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->f:Landroid/view/View$OnClickListener;

    .line 2523720
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->g:Landroid/view/View$OnClickListener;

    .line 2523721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->h:Landroid/view/View$OnClickListener;

    .line 2523722
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->i:Landroid/view/View$OnClickListener;

    .line 2523723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->j:Landroid/view/View$OnClickListener;

    .line 2523724
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->k:Landroid/view/View$OnClickListener;

    .line 2523725
    const v0, 0x7f0d0f01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2523726
    const v0, 0x7f0d0f02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2523727
    const v0, 0x7f0d0f03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2523728
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;

    invoke-static {v0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v0

    check-cast v0, LX/BiT;

    iput-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->a:LX/BiT;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2523713
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523714
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523715
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523716
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2523709
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523710
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523711
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523712
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2523696
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->b:Lcom/facebook/events/model/Event;

    .line 2523697
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2523698
    const/4 v0, 0x1

    move v0, v0

    .line 2523699
    if-eqz v0, :cond_0

    .line 2523700
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f6

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523701
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523702
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f7

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523703
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c()V

    .line 2523704
    :goto_0
    return-void

    .line 2523705
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523706
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f5

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523707
    iget-object v0, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0821f9

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2523708
    invoke-direct {p0}, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->b()V

    goto :goto_0
.end method

.method public setRsvpActionListener(LX/HyD;)V
    .locals 0

    .prologue
    .line 2523694
    iput-object p1, p0, Lcom/facebook/events/dashboard/EventsDashboardRowInlineRsvpView;->l:LX/HyD;

    .line 2523695
    return-void
.end method
