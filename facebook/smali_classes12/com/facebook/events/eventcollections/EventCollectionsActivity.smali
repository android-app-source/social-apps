.class public Lcom/facebook/events/eventcollections/EventCollectionsActivity;
.super Lcom/facebook/richdocument/BaseRichDocumentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2531509
    invoke-direct {p0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/richdocument/RichDocumentFragment;
    .locals 3

    .prologue
    .line 2531510
    new-instance v1, Lcom/facebook/events/eventcollections/EventCollectionsFragment;

    invoke-direct {v1}, Lcom/facebook/events/eventcollections/EventCollectionsFragment;-><init>()V

    .line 2531511
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/EventCollectionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extras_event_analytics_params"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531512
    iput-object v0, v1, Lcom/facebook/events/eventcollections/EventCollectionsFragment;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531513
    return-object v1
.end method
