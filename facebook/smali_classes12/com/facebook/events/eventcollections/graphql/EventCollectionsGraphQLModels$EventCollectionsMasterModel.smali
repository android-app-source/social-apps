.class public final Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6244cabf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2532887
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2532834
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2532885
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2532886
    return-void
.end method

.method private k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532883
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532884
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2532871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2532872
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x1b00318f

    invoke-static {v1, v0, v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2532873
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2532874
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2532875
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2532876
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2532877
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2532878
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2532879
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2532880
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2532881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2532882
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2532851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2532852
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2532853
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1b00318f

    invoke-static {v2, v0, v3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2532854
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->d()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2532855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    .line 2532856
    iput v3, v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->e:I

    move-object v1, v0

    .line 2532857
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2532858
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    .line 2532859
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2532860
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    .line 2532861
    iput-object v0, v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    .line 2532862
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2532863
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532864
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2532865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    .line 2532866
    iput-object v0, v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532867
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2532868
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2532869
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2532870
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532849
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->f:Ljava/lang/String;

    .line 2532850
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2532846
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2532847
    const/4 v0, 0x0

    const v1, 0x1b00318f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->e:I

    .line 2532848
    return-void
.end method

.method public final synthetic b()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532845
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2532842
    new-instance v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-direct {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;-><init>()V

    .line 2532843
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2532844
    return-object v0
.end method

.method public final synthetic c()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532841
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCollectionPivot"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2532839
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2532840
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2532838
    const v0, 0x43d3c9cf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2532837
    const v0, 0x430dbcb8

    return v0
.end method

.method public final j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532835
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    .line 2532836
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    return-object v0
.end method
