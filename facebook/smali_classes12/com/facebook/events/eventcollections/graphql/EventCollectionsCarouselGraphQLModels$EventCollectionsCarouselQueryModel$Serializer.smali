.class public final Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2532129
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;

    new-instance v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2532130
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2532132
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2532133
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2532134
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2532135
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2532136
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2532137
    if-eqz v2, :cond_3

    .line 2532138
    const-string v3, "event_collections"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532139
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2532140
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2532141
    if-eqz v3, :cond_2

    .line 2532142
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532143
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2532144
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 2532145
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2532146
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2532147
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2532148
    if-eqz v0, :cond_0

    .line 2532149
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532150
    invoke-static {v1, v0, p1, p2}, LX/I3w;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2532151
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2532152
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2532153
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2532154
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2532155
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2532156
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2532131
    check-cast p1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$Serializer;->a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
