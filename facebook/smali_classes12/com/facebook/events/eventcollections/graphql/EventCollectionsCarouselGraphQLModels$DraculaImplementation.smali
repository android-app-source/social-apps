.class public final Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2531853
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2531854
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2531855
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2531856
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2531857
    if-nez p1, :cond_0

    move v0, v1

    .line 2531858
    :goto_0
    return v0

    .line 2531859
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2531860
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2531861
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2531862
    const v2, 0x3949fc71

    const/4 v5, 0x0

    .line 2531863
    if-nez v0, :cond_1

    move v4, v5

    .line 2531864
    :goto_1
    move v0, v4

    .line 2531865
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2531866
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2531867
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2531868
    :sswitch_1
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 2531869
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2531870
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2531871
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2531872
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2531873
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2531874
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2531875
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2531876
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2531877
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2531878
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2531879
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2531880
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2531881
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2531882
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2531883
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2531884
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2531885
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7456af04 -> :sswitch_2
        0x1bce7bb -> :sswitch_0
        0x3949fc71 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2531910
    new-instance v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2531886
    if-eqz p0, :cond_0

    .line 2531887
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2531888
    if-eq v0, p0, :cond_0

    .line 2531889
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2531890
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2531891
    sparse-switch p2, :sswitch_data_0

    .line 2531892
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2531893
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2531894
    const v1, 0x3949fc71

    .line 2531895
    if-eqz v0, :cond_0

    .line 2531896
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2531897
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2531898
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2531899
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2531900
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2531901
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2531902
    :sswitch_2
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 2531903
    invoke-static {v0, p3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7456af04 -> :sswitch_1
        0x1bce7bb -> :sswitch_0
        0x3949fc71 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2531904
    if-eqz p1, :cond_0

    .line 2531905
    invoke-static {p0, p1, p2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2531906
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;

    .line 2531907
    if-eq v0, v1, :cond_0

    .line 2531908
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2531909
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2531850
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2531851
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2531852
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2531826
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2531827
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2531828
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2531829
    iput p2, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->b:I

    .line 2531830
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2531849
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2531825
    new-instance v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2531819
    iget v0, p0, LX/1vt;->c:I

    .line 2531820
    move v0, v0

    .line 2531821
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2531822
    iget v0, p0, LX/1vt;->c:I

    .line 2531823
    move v0, v0

    .line 2531824
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2531831
    iget v0, p0, LX/1vt;->b:I

    .line 2531832
    move v0, v0

    .line 2531833
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531834
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2531835
    move-object v0, v0

    .line 2531836
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2531837
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2531838
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2531839
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2531840
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2531841
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2531842
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2531843
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2531844
    invoke-static {v3, v9, v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2531845
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2531846
    iget v0, p0, LX/1vt;->c:I

    .line 2531847
    move v0, v0

    .line 2531848
    return v0
.end method
