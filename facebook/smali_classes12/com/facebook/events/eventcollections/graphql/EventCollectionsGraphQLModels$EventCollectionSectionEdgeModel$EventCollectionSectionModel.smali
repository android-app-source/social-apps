.class public final Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/8Yz;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43c9d70f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2532654
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2532653
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2532651
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2532652
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532648
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2532649
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2532650
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532646
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2532647
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    return-object v0
.end method

.method private p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532644
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532645
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 2532618
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2532619
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2532620
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2532621
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->c()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2532622
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2532623
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2532624
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->mE_()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2532625
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2532626
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2532627
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->k()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2532628
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2532629
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->m()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2532630
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2532631
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 2532632
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2532633
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2532634
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2532635
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2532636
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2532637
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2532638
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2532639
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2532640
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2532641
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2532642
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2532643
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2532600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2532601
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2532602
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2532603
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2532604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    .line 2532605
    iput-object v0, v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2532606
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2532607
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2532608
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2532609
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    .line 2532610
    iput-object v0, v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2532611
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2532612
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532613
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2532614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    .line 2532615
    iput-object v0, v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2532616
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2532617
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532599
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2532596
    new-instance v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-direct {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;-><init>()V

    .line 2532597
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2532598
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532655
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->f:Ljava/lang/String;

    .line 2532656
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532578
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->g:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->g:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 2532579
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->g:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final synthetic d()LX/7oa;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532580
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2532581
    const v0, 0x1927229f

    return v0
.end method

.method public final e()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532582
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2532583
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method

.method public final synthetic em_()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532584
    invoke-direct {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2532585
    const v0, 0x1c343941

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532586
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->k:Ljava/lang/String;

    .line 2532587
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532588
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->m:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->m:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2532589
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->m:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532590
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->n:Ljava/lang/String;

    .line 2532591
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532592
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o:Ljava/lang/String;

    .line 2532593
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final mE_()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2532594
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2532595
    iget-object v0, p0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->j:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method
