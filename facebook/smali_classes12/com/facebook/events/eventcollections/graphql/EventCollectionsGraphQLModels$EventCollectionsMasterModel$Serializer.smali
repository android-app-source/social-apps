.class public final Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2532832
    const-class v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    new-instance v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2532833
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2532831
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2532800
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2532801
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2532802
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2532803
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2532804
    if-eqz v2, :cond_2

    .line 2532805
    const-string v3, "collection_pivot"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532806
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2532807
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2532808
    if-eqz v3, :cond_1

    .line 2532809
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532810
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2532811
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v2

    if-ge p0, v2, :cond_0

    .line 2532812
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1, p2}, LX/I46;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2532813
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2532814
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2532815
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2532816
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2532817
    if-eqz v2, :cond_3

    .line 2532818
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532819
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2532820
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2532821
    if-eqz v2, :cond_4

    .line 2532822
    const-string v3, "event_collection_document"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532823
    invoke-static {v1, v2, p1, p2}, LX/I43;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2532824
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2532825
    if-eqz v2, :cond_5

    .line 2532826
    const-string v3, "header_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532827
    invoke-static {v1, v2, p1, p2}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2532828
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2532829
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2532830
    check-cast p1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$Serializer;->a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;LX/0nX;LX/0my;)V

    return-void
.end method
