.class public Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/events/common/EventAnalyticsParams;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2534166
    const-class v0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    const-string v1, "event_dashboard"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2534167
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2534168
    const-class v0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    invoke-static {v0, p0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2534169
    const v0, 0x7f0304b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2534170
    const v0, 0x7f0d0dbf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2534171
    const v0, 0x7f0d0db7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2534172
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2534173
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const p1, 0x7f0a00e3

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2534174
    iput-object p1, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2534175
    move-object v1, v1

    .line 2534176
    const p1, 0x7f0a00b4

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2534177
    iget-object v1, p0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2534178
    new-instance v0, LX/I4W;

    invoke-direct {v0, p0}, LX/I4W;-><init>(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534179
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setMinimumHeight(I)V

    .line 2534180
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p0

    check-cast p0, LX/17Y;

    iput-object v1, p1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->b:LX/17Y;

    return-void
.end method

.method public static setEventCollectionPhoto(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V
    .locals 3
    .param p0    # Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2534181
    iget-object v0, p0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2534182
    return-void
.end method

.method public static setEventCollectionTitle(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2534183
    iget-object v0, p0, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534184
    return-void
.end method
