.class public Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/I4V;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/events/eventcollections/view/impl/block/RelatedEventCollectionsBlockView;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Lcom/facebook/widget/CustomFrameLayout;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final f:Lcom/facebook/widget/text/BetterTextView;

.field public final g:Lcom/facebook/widget/CustomFrameLayout;

.field public final h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final i:Lcom/facebook/widget/text/BetterTextView;

.field public j:Lcom/facebook/events/common/EventAnalyticsParams;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2534295
    const-class v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    const-string v1, "event_collection"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2534296
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2534297
    const-class v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 2534298
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2534299
    const v0, 0x7f0d29b2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->d:Lcom/facebook/widget/CustomFrameLayout;

    .line 2534300
    const v0, 0x7f0d29b3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2534301
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->a(Landroid/content/res/Resources;)LX/1af;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2534302
    const v0, 0x7f0d29b4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2534303
    const v0, 0x7f0d29b5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->g:Lcom/facebook/widget/CustomFrameLayout;

    .line 2534304
    const v0, 0x7f0d29b6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2534305
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->a(Landroid/content/res/Resources;)LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2534306
    const v0, 0x7f0d29b7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2534307
    new-instance v0, LX/I4e;

    invoke-direct {v0, p0}, LX/I4e;-><init>(Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;)V

    .line 2534308
    iget-object v1, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->d:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534309
    iget-object v1, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->g:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534310
    const v0, 0x7f0d29b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/I4f;

    invoke-direct {v1, p0}, LX/I4f;-><init>(Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534311
    return-void
.end method

.method private static a(Landroid/content/res/Resources;)LX/1af;
    .locals 2

    .prologue
    .line 2534312
    new-instance v0, LX/1Uo;

    invoke-direct {v0, p0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v1, 0x7f0a00e3

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2534313
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2534314
    move-object v0, v0

    .line 2534315
    const v1, 0x7f0a061b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p0

    check-cast p0, LX/17Y;

    iput-object v1, p1, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->b:LX/17Y;

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2534316
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2534317
    :cond_0
    const/4 v2, 0x0

    .line 2534318
    iput-object v2, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->l:Ljava/lang/String;

    .line 2534319
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534320
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2534321
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->g:Lcom/facebook/widget/CustomFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2534322
    :goto_0
    return-void

    .line 2534323
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->g:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2534324
    iput-object p1, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->l:Ljava/lang/String;

    .line 2534325
    iget-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534326
    iget-object v1, p0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p3, :cond_2

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    sget-object v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
