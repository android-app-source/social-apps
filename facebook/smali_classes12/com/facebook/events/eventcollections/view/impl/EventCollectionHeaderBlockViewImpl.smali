.class public Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/I4T;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/events/eventcollections/view/impl/block/EventCollectionHeaderBlockView;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2534244
    const-class v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;

    const-string v1, "event_collection"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2534245
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2534246
    const v0, 0x7f0d0db7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->b:Landroid/widget/TextView;

    .line 2534247
    const v0, 0x7f0d0db9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->c:Landroid/widget/TextView;

    .line 2534248
    const v0, 0x7f0d0db6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2534249
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2534250
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2534251
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2534252
    move-object v1, v1

    .line 2534253
    const v2, 0x7f0a0619

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2534254
    iget-object v1, p0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2534255
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2534256
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2534257
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534258
    return-void

    .line 2534259
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
