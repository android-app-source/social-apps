.class public Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;
.super LX/CnT;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/events/eventcollections/view/impl/block/EventBlockView;",
        "Lcom/facebook/events/eventcollections/model/data/EventBlockData;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public d:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7vW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/7vZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:LX/Bne;

.field public k:LX/I4K;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2534058
    const-class v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/I4b;)V
    .locals 6

    .prologue
    .line 2534059
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2534060
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;

    const-class v3, LX/38v;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/38v;

    invoke-static {v0}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v4

    check-cast v4, LX/DB4;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object p1

    check-cast p1, LX/7vW;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v0

    check-cast v0, LX/7vZ;

    iput-object v3, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->d:LX/38v;

    iput-object v4, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->e:LX/DB4;

    iput-object v5, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->f:LX/1Ad;

    iput-object p1, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->g:LX/7vW;

    iput-object v0, v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->h:LX/7vZ;

    .line 2534061
    iget-object v0, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->d:LX/38v;

    new-instance v1, LX/I4S;

    invoke-direct {v1, p0}, LX/I4S;-><init>(Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;)V

    invoke-virtual {v0, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->j:LX/Bne;

    .line 2534062
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 5

    .prologue
    .line 2534063
    check-cast p1, LX/I4K;

    .line 2534064
    iput-object p1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534065
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534066
    check-cast v0, LX/I4b;

    .line 2534067
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-interface {v1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v1

    .line 2534068
    iput-object v1, v0, LX/I4b;->c:Ljava/lang/String;

    .line 2534069
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534070
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534071
    iget-object v1, v2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v1

    .line 2534072
    move-object v1, v2

    .line 2534073
    iget-object v2, v0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2534074
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->f:LX/1Ad;

    sget-object v2, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534075
    iget-object v3, v2, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534076
    iget-object v2, v3, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v3, v2

    .line 2534077
    move-object v2, v3

    .line 2534078
    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2534079
    iget-object v2, v0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2534080
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534081
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    move-object v1, v2

    .line 2534082
    iget-object v2, v0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 2534083
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->e:LX/DB4;

    iget-object v2, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534084
    iget-object v3, v2, LX/I4K;->b:Lcom/facebook/events/model/Event;

    move-object v2, v3

    .line 2534085
    invoke-virtual {v1, v2}, LX/DB4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v1

    .line 2534086
    iget-object v2, v0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 2534087
    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534088
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534089
    iget-object v3, v2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v3

    .line 2534090
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2534091
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534092
    iget-object v3, v2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v3

    .line 2534093
    :goto_0
    move-object v1, v2

    .line 2534094
    iget-object v2, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    .line 2534095
    iget-object v3, v2, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534096
    iget-object v4, v3, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    move-object v3, v4

    .line 2534097
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2534098
    iget-object v3, v2, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534099
    iget-object v4, v3, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    move-object v3, v4

    .line 2534100
    :goto_1
    move-object v2, v3

    .line 2534101
    iget-object v3, v0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2534102
    invoke-virtual {v0}, LX/I4b;->a()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->j:LX/Bne;

    iget-object v2, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v2}, LX/I4K;->g()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v3}, LX/I4K;->h()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;->k:LX/I4K;

    invoke-virtual {v4}, LX/I4K;->i()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2534103
    return-void

    .line 2534104
    :cond_0
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534105
    iget-object v3, v2, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v2, v3

    .line 2534106
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2534107
    iget-object v2, v1, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2534108
    iget-object v3, v2, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v2, v3

    .line 2534109
    goto :goto_0

    .line 2534110
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
