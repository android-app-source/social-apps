.class public Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;
.super LX/0te;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JJL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2679365
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2679366
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2679367
    iput-object v0, p0, Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;->a:LX/0Ot;

    .line 2679368
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;

    const/16 v1, 0x1b98

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2679369
    iget-object v0, p0, Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJL;

    invoke-virtual {v0}, LX/JJL;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x26fe7bd0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679370
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2679371
    invoke-static {p0, p0}, Lcom/facebook/events/sync/adapter/FacebookCalendarSyncAdapterService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2679372
    const/16 v1, 0x25

    const v2, -0x7d103c57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
