.class public Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/J1m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/J1c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/widget/text/BetterEditTextView;

.field public f:LX/Iox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2639064
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2639065
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639062
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2639063
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639044
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2639045
    const-class v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    invoke-static {v0, p0}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2639046
    const v0, 0x7f030dff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2639047
    const v0, 0x7f0d1b7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2639048
    return-void
.end method

.method private static a(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;LX/J1m;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;",
            "LX/J1m;",
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/J1c;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2639061
    iput-object p1, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a:LX/J1m;

    iput-object p2, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->d:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    invoke-static {v1}, LX/J1m;->a(LX/0QB;)LX/J1m;

    move-result-object v0

    check-cast v0, LX/J1m;

    const/16 v2, 0x34

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2d73

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x5c8

    invoke-static {v1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v2, v3, v1}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;LX/J1m;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x112e9a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2639055
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2639056
    iget-object v1, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a:LX/J1m;

    new-instance v2, LX/J1a;

    invoke-direct {v2, p0}, LX/J1a;-><init>(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;)V

    .line 2639057
    iput-object v2, v1, LX/J1m;->b:LX/IoE;

    .line 2639058
    iget-object v1, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->a:LX/J1m;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2639059
    iget-object v1, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v2, LX/J1b;

    invoke-direct {v2, p0}, LX/J1b;-><init>(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2639060
    const/16 v1, 0x2d

    const v2, 0x7d2c2ec9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/Iox;)V
    .locals 1

    .prologue
    .line 2639052
    iput-object p1, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->f:LX/Iox;

    .line 2639053
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->f:LX/Iox;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2639054
    return-void
.end method

.method public setMemoText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2639049
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2639050
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2639051
    :cond_0
    return-void
.end method
