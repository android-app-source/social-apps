.class public Lcom/facebook/payments/p2p/ui/DollarIconEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2639023
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 2639024
    const-string v0, "USD"

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    .line 2639025
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    .line 2639026
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    .line 2639027
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    .line 2639028
    invoke-direct {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c()V

    .line 2639029
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2639016
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2639017
    const-string v0, "USD"

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    .line 2639018
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    .line 2639019
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    .line 2639020
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    .line 2639021
    invoke-direct {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c()V

    .line 2639022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2639009
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2639010
    const-string v0, "USD"

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    .line 2639011
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    .line 2639012
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    .line 2639013
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    .line 2639014
    invoke-direct {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c()V

    .line 2639015
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2638999
    invoke-virtual {p0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setIncludeFontPadding(Z)V

    .line 2639000
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getGravity()I

    move-result v0

    or-int/lit8 v0, v0, 0x30

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setGravity(I)V

    .line 2639001
    invoke-virtual {p0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setWillNotDraw(Z)V

    .line 2639002
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1e73

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setCompoundDrawablePadding(I)V

    .line 2639003
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2639004
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2639005
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2639006
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2639007
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2639008
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2638992
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2638993
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 2638994
    const v2, 0x101042a

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    .line 2638995
    if-eqz v1, :cond_0

    .line 2638996
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setColor(I)V

    .line 2638997
    :goto_0
    return-void

    .line 2638998
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setColor(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2638989
    invoke-virtual {p0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setCurrencyCode(Ljava/lang/String;)V

    .line 2638990
    invoke-virtual {p0, p2}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setAmount(Ljava/lang/String;)V

    .line 2638991
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2638987
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0673

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setColor(I)V

    .line 2638988
    return-void
.end method

.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 2639030
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getCompoundDrawablePadding()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getTextSize()F

    move-result v1

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 1

    .prologue
    .line 2638986
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getCompoundPaddingLeft()I

    move-result v0

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2638979
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 2638980
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getTextSize()F

    move-result v0

    .line 2638981
    const v1, 0x3e2e147b    # 0.17f

    mul-float/2addr v1, v0

    .line 2638982
    iget-object v2, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    const/high16 v3, 0x3e800000    # 0.25f

    mul-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2638983
    iget v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b:I

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget-object v2, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2638984
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    iget v2, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b:I

    int-to-float v2, v2

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2638985
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    .prologue
    .line 2638975
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 2638976
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2638977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 2638978
    :cond_0
    return-void
.end method

.method public final onSelectionChanged(II)V
    .locals 1

    .prologue
    .line 2638973
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setSelection(I)V

    .line 2638974
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x130285f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2638971
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getTextSize()F

    move-result v1

    float-to-int v1, v1

    sub-int v1, p2, v1

    iput v1, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b:I

    .line 2638972
    const/16 v1, 0x2d

    const v2, -0x649a3601

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAmount(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2638968
    invoke-virtual {p0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2638969
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->invalidate()V

    .line 2638970
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 2638964
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2638965
    invoke-virtual {p0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setTextColor(I)V

    .line 2638966
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->invalidate()V

    .line 2638967
    return-void
.end method

.method public setCurrencyCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2638961
    iput-object p1, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    .line 2638962
    iget-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->c:Ljava/lang/String;

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->d:Ljava/lang/String;

    .line 2638963
    return-void
.end method
