.class public Lcom/facebook/payments/p2p/P2pPaymentActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/Iyv;


# instance fields
.field private p:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/Iyr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/63L;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2634043
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/Iyx;LX/Iyw;)V
    .locals 4

    .prologue
    .line 2634032
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2634033
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->q:LX/Iyr;

    .line 2634034
    iget-object v2, p2, LX/Iyw;->a:LX/Iyu;

    move-object v2, v2

    .line 2634035
    iget-object v3, v1, LX/Iyr;->a:LX/0P1;

    invoke-virtual {v3, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/Iyr;->a:LX/0P1;

    invoke-virtual {v3, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Iys;

    iget-object v3, v3, LX/Iys;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    :goto_0
    check-cast v3, LX/Iyq;

    check-cast v3, LX/Iyq;

    .line 2634036
    const/4 v2, 0x2

    .line 2634037
    invoke-virtual {v0, v2, v2}, LX/3u1;->a(II)V

    .line 2634038
    const v2, 0x7f082c04

    invoke-virtual {v0, v2}, LX/3u1;->b(I)V

    .line 2634039
    iget-object v2, p1, LX/Iyx;->c:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 2634040
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/3u1;->b(Ljava/lang/CharSequence;)V

    .line 2634041
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->r:LX/63L;

    .line 2634042
    return-void

    :cond_0
    iget-object v3, v1, LX/Iyr;->a:LX/0P1;

    sget-object p2, LX/Iyu;->DEFAULT:LX/Iyu;

    invoke-virtual {v3, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Iys;

    iget-object v3, v3, LX/Iys;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method private static a(Lcom/facebook/payments/p2p/P2pPaymentActivity;LX/67X;LX/Iyr;)V
    .locals 0

    .prologue
    .line 2634031
    iput-object p1, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->p:LX/67X;

    iput-object p2, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->q:LX/Iyr;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;

    invoke-static {v1}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v0

    check-cast v0, LX/67X;

    invoke-static {v1}, LX/Iyr;->a(LX/0QB;)LX/Iyr;

    move-result-object v1

    check-cast v1, LX/Iyr;

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->a(Lcom/facebook/payments/p2p/P2pPaymentActivity;LX/67X;LX/Iyr;)V

    return-void
.end method

.method private m()LX/Iyx;
    .locals 5

    .prologue
    .line 2634026
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_payment_amount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    .line 2634027
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_memo"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2634028
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_recipient_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/Name;

    .line 2634029
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "extra_recipient_userkey"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserKey;

    .line 2634030
    new-instance v4, LX/Iyx;

    invoke-direct {v4, v0, v3, v1, v2}, LX/Iyx;-><init>(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/facebook/user/model/Name;Lcom/facebook/user/model/UserKey;)V

    return-object v4
.end method

.method private n()LX/Iyw;
    .locals 3

    .prologue
    .line 2633999
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_flow_style"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Iyu;->valueOf(Ljava/lang/String;)LX/Iyu;

    move-result-object v0

    .line 2634000
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_currency_code"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2634001
    new-instance v2, LX/Iyw;

    invoke-direct {v2, v0, v1}, LX/Iyw;-><init>(LX/Iyu;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2634025
    const-string v0, "payment_tray_popup"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2634022
    invoke-static {p0, p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2634023
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2634024
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2634020
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->finish()V

    .line 2634021
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634012
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2634013
    const v0, 0x7f030dfd

    invoke-virtual {p0, v0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->setContentView(I)V

    .line 2634014
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d222d

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;

    .line 2634015
    invoke-direct {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->m()LX/Iyx;

    move-result-object v1

    .line 2634016
    invoke-direct {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->n()LX/Iyw;

    move-result-object v2

    .line 2634017
    invoke-virtual {v0, v2, v1}, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a(LX/Iyw;LX/Iyx;)V

    .line 2634018
    invoke-direct {p0, v1, v2}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->a(LX/Iyx;LX/Iyw;)V

    .line 2634019
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2634010
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->finish()V

    .line 2634011
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2634006
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2634007
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2634008
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentActivity;->r:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2634009
    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2634002
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2634003
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/P2pPaymentActivity;->finish()V

    .line 2634004
    const/4 v0, 0x1

    .line 2634005
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
