.class public Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638678
    new-instance v0, LX/J1P;

    invoke-direct {v0}, LX/J1P;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2638679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638680
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->a:LX/0Px;

    .line 2638681
    iput-boolean p2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->b:Z

    .line 2638682
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638684
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->a:LX/0Px;

    .line 2638685
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->b:Z

    .line 2638686
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638687
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638688
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2638689
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2638690
    return-void
.end method
