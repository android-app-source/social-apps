.class public Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638176
    const-string v0, "validatePaymentCardBinParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->a:Ljava/lang/String;

    .line 2638177
    new-instance v0, LX/J12;

    invoke-direct {v0}, LX/J12;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638174
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->b:Ljava/lang/String;

    .line 2638175
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2638178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638179
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2638180
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2638181
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->b:Ljava/lang/String;

    .line 2638182
    return-void

    :cond_0
    move v0, v2

    .line 2638183
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2638184
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638172
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638171
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "paymentCardBin"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638169
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638170
    return-void
.end method
