.class public Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field public final b:LX/DtK;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638607
    new-instance v0, LX/J1L;

    invoke-direct {v0}, LX/J1L;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2638608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638609
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->a:I

    .line 2638610
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DtK;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    .line 2638611
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->c:J

    .line 2638612
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638613
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2638614
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "transactionsQueryType"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "endTimeSeconds"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2638615
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638616
    iget-wide v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2638617
    return-void
.end method
