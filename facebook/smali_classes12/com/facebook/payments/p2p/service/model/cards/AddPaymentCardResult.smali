.class public Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final credentialId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "credential_id"
    .end annotation
.end field

.field public final encodedCredentialId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "encoded_credential_id"
    .end annotation
.end field

.field public final followUpActionButtonText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "follow_up_action_button_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final followUpActionText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "follow_up_action_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final followUpActionType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "follow_up_action_type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final followUpActionUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "follow_up_action_url"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2637904
    const-class v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637905
    new-instance v0, LX/J0n;

    invoke-direct {v0}, LX/J0n;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2637888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637889
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->credentialId:Ljava/lang/String;

    .line 2637890
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->encodedCredentialId:Ljava/lang/String;

    .line 2637891
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionType:Ljava/lang/String;

    .line 2637892
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionText:Ljava/lang/String;

    .line 2637893
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionUrl:Ljava/lang/String;

    .line 2637894
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionButtonText:Ljava/lang/String;

    .line 2637895
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2637896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->credentialId:Ljava/lang/String;

    .line 2637898
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->encodedCredentialId:Ljava/lang/String;

    .line 2637899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionType:Ljava/lang/String;

    .line 2637900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionText:Ljava/lang/String;

    .line 2637901
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionUrl:Ljava/lang/String;

    .line 2637902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionButtonText:Ljava/lang/String;

    .line 2637903
    return-void
.end method

.method public static newBuilder()LX/J0o;
    .locals 1

    .prologue
    .line 2637887
    new-instance v0, LX/J0o;

    invoke-direct {v0}, LX/J0o;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2637886
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2637879
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->credentialId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637880
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->encodedCredentialId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637881
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637882
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637883
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637884
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637885
    return-void
.end method
