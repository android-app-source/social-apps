.class public Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638494
    const-string v0, "FetchPaymentRequestParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->a:Ljava/lang/String;

    .line 2638495
    new-instance v0, LX/J1E;

    invoke-direct {v0}, LX/J1E;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    .line 2638489
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2638490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638491
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638492
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    .line 2638493
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638483
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638486
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "requestId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638484
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638485
    return-void
.end method
