.class public Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mFallbackMSite:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fallback_msite"
    .end annotation
.end field

.field private final mFallbackUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fallback_uri"
    .end annotation
.end field

.field private final mScreen:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "screen"
    .end annotation
.end field

.field private final mScreenData:Lcom/facebook/payments/p2p/model/verification/ScreenData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "screen_data"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638935
    const-class v0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638934
    new-instance v0, LX/J1Z;

    invoke-direct {v0}, LX/J1Z;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2638928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638929
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackMSite:Z

    .line 2638930
    iput-object v1, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackUri:Ljava/lang/String;

    .line 2638931
    iput-object v1, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreen:Ljava/lang/String;

    .line 2638932
    iput-object v1, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreenData:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2638933
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638923
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackMSite:Z

    .line 2638924
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackUri:Ljava/lang/String;

    .line 2638925
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreen:Ljava/lang/String;

    .line 2638926
    const-class v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreenData:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2638927
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2638936
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackMSite:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638921
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackUri:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638920
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreen:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/payments/p2p/model/verification/ScreenData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638919
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreenData:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638918
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2638913
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackMSite:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2638914
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mFallbackUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638915
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreen:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638916
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->mScreenData:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2638917
    return-void
.end method
