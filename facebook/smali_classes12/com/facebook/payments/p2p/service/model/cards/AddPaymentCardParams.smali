.class public Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637875
    new-instance v0, LX/J0m;

    invoke-direct {v0}, LX/J0m;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2637844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->a:Ljava/lang/String;

    .line 2637846
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->b:I

    .line 2637847
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->c:I

    .line 2637848
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->d:Ljava/lang/String;

    .line 2637849
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->e:Ljava/lang/String;

    .line 2637850
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->f:Ljava/lang/String;

    .line 2637851
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->g:Ljava/lang/String;

    .line 2637852
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    .line 2637853
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2637865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637866
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->a:Ljava/lang/String;

    .line 2637867
    iput p2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->b:I

    .line 2637868
    iput p3, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->c:I

    .line 2637869
    iput-object p4, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->d:Ljava/lang/String;

    .line 2637870
    iput-object p5, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->e:Ljava/lang/String;

    .line 2637871
    iput-object p6, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->f:Ljava/lang/String;

    .line 2637872
    iput-object p7, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->g:Ljava/lang/String;

    .line 2637873
    iput-object p8, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    .line 2637874
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2637864
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2637863
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "creditCardNumber"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "month"

    iget v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "year"

    iget v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "csc"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "zip"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "userid"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "productType"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "productId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2637854
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637855
    iget v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2637856
    iget v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2637857
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637858
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637859
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637860
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637861
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637862
    return-void
.end method
