.class public Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638164
    const-string v0, "setPrimaryCardParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->a:Ljava/lang/String;

    .line 2638165
    new-instance v0, LX/J11;

    invoke-direct {v0}, LX/J11;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 2638151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638152
    iput-wide p1, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    .line 2638153
    iput-object p3, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->c:Ljava/lang/String;

    .line 2638154
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2638160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638161
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    .line 2638162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->c:Ljava/lang/String;

    .line 2638163
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638159
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2638158
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "credentialId"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "userId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2638155
    iget-wide v0, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2638156
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638157
    return-void
.end method
