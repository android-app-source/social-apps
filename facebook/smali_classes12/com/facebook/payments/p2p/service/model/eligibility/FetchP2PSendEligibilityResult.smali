.class public Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCanSend:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_viewer_send_money"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638243
    const-class v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638244
    new-instance v0, LX/J15;

    invoke-direct {v0}, LX/J15;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2638245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->mCanSend:Z

    .line 2638247
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638249
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->mCanSend:Z

    .line 2638250
    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 2638251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638252
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->mCanSend:Z

    .line 2638253
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2638254
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->mCanSend:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638255
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638256
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->mCanSend:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2638257
    return-void
.end method
