.class public Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638116
    new-instance v0, LX/J0y;

    invoke-direct {v0}, LX/J0y;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638117
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;->a:Ljava/lang/String;

    .line 2638119
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2638120
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638121
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;->a:Ljava/lang/String;

    .line 2638122
    return-void
.end method


# virtual methods
.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 2638123
    sget-object v0, LX/6zQ;->NEW_PAY_OVER_COUNTER:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638124
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638125
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638126
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewPayOverCounterOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638127
    return-void
.end method
