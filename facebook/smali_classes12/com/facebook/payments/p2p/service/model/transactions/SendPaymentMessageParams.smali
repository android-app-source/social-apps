.class public Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/5g0;

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638758
    const-string v0, "SendPaymentMessageParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->a:Ljava/lang/String;

    .line 2638759
    new-instance v0, LX/J1V;

    invoke-direct {v0}, LX/J1V;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/J1W;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2638760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638761
    iget-object v0, p1, LX/J1W;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2638762
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638763
    iget-object v0, p1, LX/J1W;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2638764
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2638765
    iget-object v0, p1, LX/J1W;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2638766
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2638767
    iget-object v0, p1, LX/J1W;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2638768
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2638769
    iget-object v0, p1, LX/J1W;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2638770
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2638771
    iget-object v0, p1, LX/J1W;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2638772
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->c:Ljava/lang/String;

    .line 2638773
    iget-object v0, p1, LX/J1W;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2638774
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->d:Ljava/lang/String;

    .line 2638775
    iget-object v0, p1, LX/J1W;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2638776
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->e:Ljava/lang/String;

    .line 2638777
    iget-object v0, p1, LX/J1W;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2638778
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->f:Ljava/lang/String;

    .line 2638779
    iget-object v0, p1, LX/J1W;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2638780
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->g:Ljava/lang/String;

    .line 2638781
    iget-object v0, p1, LX/J1W;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2638782
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->h:Ljava/lang/String;

    .line 2638783
    iget-object v0, p1, LX/J1W;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2638784
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->i:Ljava/lang/String;

    .line 2638785
    iget-object v0, p1, LX/J1W;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2638786
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->j:Ljava/lang/String;

    .line 2638787
    iget-object v0, p1, LX/J1W;->j:LX/5g0;

    move-object v0, v0

    .line 2638788
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    .line 2638789
    iget-object v0, p1, LX/J1W;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2638790
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->l:Ljava/lang/String;

    .line 2638791
    iget-object v0, p1, LX/J1W;->l:Ljava/lang/String;

    move-object v0, v0

    .line 2638792
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->m:Ljava/lang/String;

    .line 2638793
    iget-object v0, p1, LX/J1W;->m:Ljava/lang/String;

    move-object v0, v0

    .line 2638794
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->n:Ljava/lang/String;

    .line 2638795
    iget-object v0, p1, LX/J1W;->n:Ljava/lang/String;

    move-object v0, v0

    .line 2638796
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->o:Ljava/lang/String;

    .line 2638797
    return-void

    :cond_0
    move v0, v2

    .line 2638798
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2638799
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2638800
    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638802
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2638803
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->c:Ljava/lang/String;

    .line 2638804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->d:Ljava/lang/String;

    .line 2638805
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->e:Ljava/lang/String;

    .line 2638806
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->f:Ljava/lang/String;

    .line 2638807
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->g:Ljava/lang/String;

    .line 2638808
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->h:Ljava/lang/String;

    .line 2638809
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->i:Ljava/lang/String;

    .line 2638810
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->j:Ljava/lang/String;

    .line 2638811
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    .line 2638812
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->l:Ljava/lang/String;

    .line 2638813
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->m:Ljava/lang/String;

    .line 2638814
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->n:Ljava/lang/String;

    .line 2638815
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->o:Ljava/lang/String;

    .line 2638816
    return-void
.end method

.method public static newBuilder()LX/J1W;
    .locals 1

    .prologue
    .line 2638817
    new-instance v0, LX/J1W;

    invoke-direct {v0}, LX/J1W;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638818
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638819
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2638820
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638821
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638822
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638823
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638824
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638825
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638826
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638827
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638828
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638829
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638830
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638831
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638832
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638833
    return-void
.end method
