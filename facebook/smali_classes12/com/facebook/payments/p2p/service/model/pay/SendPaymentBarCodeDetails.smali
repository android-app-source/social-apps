.class public Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638392
    new-instance v0, LX/J1A;

    invoke-direct {v0}, LX/J1A;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2638393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638394
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->a:Ljava/lang/String;

    .line 2638395
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->b:Ljava/lang/String;

    .line 2638396
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->c:Ljava/lang/String;

    .line 2638397
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->d:Ljava/lang/String;

    .line 2638398
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->e:Ljava/lang/String;

    .line 2638399
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->f:Ljava/lang/String;

    .line 2638400
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->g:Ljava/lang/String;

    .line 2638401
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->a:Ljava/lang/String;

    .line 2638404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->b:Ljava/lang/String;

    .line 2638405
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->c:Ljava/lang/String;

    .line 2638406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->d:Ljava/lang/String;

    .line 2638407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->e:Ljava/lang/String;

    .line 2638408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->f:Ljava/lang/String;

    .line 2638409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->g:Ljava/lang/String;

    .line 2638410
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638411
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638412
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638413
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638414
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638415
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638416
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638417
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638418
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBarCodeDetails;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638419
    return-void
.end method
