.class public Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638530
    new-instance v0, LX/J1H;

    invoke-direct {v0}, LX/J1H;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638528
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a:Ljava/util/ArrayList;

    .line 2638529
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2638524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638525
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a:Ljava/util/ArrayList;

    .line 2638526
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2638531
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638523
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638522
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "paymentRequests"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638520
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a:Ljava/util/ArrayList;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2638521
    return-void
.end method
