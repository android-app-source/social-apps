.class public Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638091
    new-instance v0, LX/J0w;

    invoke-direct {v0}, LX/J0w;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638085
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638086
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->a:Ljava/lang/String;

    .line 2638087
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->b:Ljava/lang/String;

    .line 2638088
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->c:Ljava/lang/String;

    .line 2638089
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->d:Ljava/lang/String;

    .line 2638090
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2638079
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638080
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->a:Ljava/lang/String;

    .line 2638081
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->b:Ljava/lang/String;

    .line 2638082
    iput-object p3, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->c:Ljava/lang/String;

    .line 2638083
    iput-object p4, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->d:Ljava/lang/String;

    .line 2638084
    return-void
.end method


# virtual methods
.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 2638092
    sget-object v0, LX/6zQ;->NEW_MANUAL_TRANSFER:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638078
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638072
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638073
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638074
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638075
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638076
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewManualTransferOption;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638077
    return-void
.end method
