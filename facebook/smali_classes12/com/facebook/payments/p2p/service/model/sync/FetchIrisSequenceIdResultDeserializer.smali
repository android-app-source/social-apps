.class public Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2638550
    const-class v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    new-instance v1, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;

    invoke-direct {v1}, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2638551
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2638552
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 2638553
    const-class v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 2638554
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 2638555
    const-class v1, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;

    monitor-enter v1

    .line 2638556
    :try_start_0
    sget-object v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2638557
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2638558
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 2638559
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 2638560
    :goto_1
    return-object v0

    .line 2638561
    :cond_2
    sget-object v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 2638562
    if-eqz v0, :cond_0

    .line 2638563
    monitor-exit v1

    goto :goto_1

    .line 2638564
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2638565
    :pswitch_0
    :try_start_3
    const-string v2, "iris_sequence_id"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2638566
    :pswitch_1
    const-class v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;

    const-string v2, "mIrisSequenceId"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2638567
    sget-object v2, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638568
    monitor-exit v1

    goto :goto_1

    .line 2638569
    :catch_0
    move-exception v0

    .line 2638570
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch -0x3b55bd3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
