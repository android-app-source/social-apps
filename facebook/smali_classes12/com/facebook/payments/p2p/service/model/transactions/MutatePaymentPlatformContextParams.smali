.class public Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/J1S;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638722
    new-instance v0, LX/J1R;

    invoke-direct {v0}, LX/J1R;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638715
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/J1S;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->a:LX/J1S;

    .line 2638716
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->b:Ljava/lang/String;

    .line 2638717
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638723
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638721
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mutation"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->a:LX/J1S;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "platformContextId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638718
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->a:LX/J1S;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638719
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638720
    return-void
.end method
