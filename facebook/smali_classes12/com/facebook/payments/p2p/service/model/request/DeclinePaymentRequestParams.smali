.class public Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638468
    const-string v0, "DeclinePaymentRequestParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->a:Ljava/lang/String;

    .line 2638469
    new-instance v0, LX/J1D;

    invoke-direct {v0}, LX/J1D;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638478
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->b:Ljava/lang/String;

    .line 2638479
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2638474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638475
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->b:Ljava/lang/String;

    .line 2638476
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638473
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638472
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "requestId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638470
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638471
    return-void
.end method
