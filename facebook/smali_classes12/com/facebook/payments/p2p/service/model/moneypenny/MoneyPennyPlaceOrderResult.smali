.class public Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mTransactionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "payment_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638308
    const-class v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638316
    new-instance v0, LX/J17;

    invoke-direct {v0}, LX/J17;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2638313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;->mTransactionId:Ljava/lang/String;

    .line 2638315
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;->mTransactionId:Ljava/lang/String;

    .line 2638319
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638312
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;->mTransactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638311
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638309
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;->mTransactionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638310
    return-void
.end method
