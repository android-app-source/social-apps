.class public Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCardBinId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638188
    const-class v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638189
    new-instance v0, LX/J13;

    invoke-direct {v0}, LX/J13;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2638190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;->mCardBinId:Ljava/lang/String;

    .line 2638192
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;->mCardBinId:Ljava/lang/String;

    .line 2638195
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638196
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638197
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;->mCardBinId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638198
    return-void
.end method
