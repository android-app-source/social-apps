.class public Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637968
    new-instance v0, LX/J0q;

    invoke-direct {v0}, LX/J0q;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2637960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637961
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->a:Ljava/lang/String;

    .line 2637962
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->b:I

    .line 2637963
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->c:I

    .line 2637964
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->d:Ljava/lang/String;

    .line 2637965
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    .line 2637966
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->f:Z

    .line 2637967
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2637969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637970
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->a:Ljava/lang/String;

    .line 2637971
    iput p2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->b:I

    .line 2637972
    iput p3, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->c:I

    .line 2637973
    iput-object p4, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->d:Ljava/lang/String;

    .line 2637974
    iput-object p5, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    .line 2637975
    iput-boolean p6, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->f:Z

    .line 2637976
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2637959
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2637958
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "credentialId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "month"

    iget v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "year"

    iget v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->c:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "csc"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "zip"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "shouldVerifyMobileEligibility"

    iget-boolean v2, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2637951
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637952
    iget v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2637953
    iget v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2637954
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637955
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2637956
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2637957
    return-void
.end method
