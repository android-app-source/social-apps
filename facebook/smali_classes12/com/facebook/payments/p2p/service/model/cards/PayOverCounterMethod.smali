.class public Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/PaymentMethod;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638131
    new-instance v0, LX/J0z;

    invoke-direct {v0}, LX/J0z;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2638139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638140
    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 2638137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638138
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638136
    const-string v0, "0"

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638141
    const v0, 0x7f083a1a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 2638135
    sget-object v0, LX/6zU;->PAY_OVER_COUNTER:LX/6zU;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638134
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638133
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/service/model/cards/PayOverCounterMethod;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 2638132
    return-void
.end method
