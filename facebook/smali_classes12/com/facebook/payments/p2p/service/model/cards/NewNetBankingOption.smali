.class public Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638112
    new-instance v0, LX/J0x;

    invoke-direct {v0}, LX/J0x;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638106
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->a:Ljava/lang/String;

    .line 2638108
    const-class v0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2638109
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->b:LX/0Px;

    .line 2638110
    return-void

    .line 2638111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2638102
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 2638103
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->a:Ljava/lang/String;

    .line 2638104
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->b:LX/0Px;

    .line 2638105
    return-void
.end method


# virtual methods
.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 2638101
    sget-object v0, LX/6zQ;->NEW_NET_BANKING:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638100
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638096
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638097
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638098
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NewNetBankingOption;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2638099
    return-void
.end method
