.class public Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638354
    new-instance v0, LX/J18;

    invoke-direct {v0}, LX/J18;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638345
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->a:Ljava/lang/String;

    .line 2638346
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->b:Ljava/lang/String;

    .line 2638347
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->c:Ljava/lang/String;

    .line 2638348
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638353
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638349
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638350
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638351
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPayment3dsDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638352
    return-void
.end method
