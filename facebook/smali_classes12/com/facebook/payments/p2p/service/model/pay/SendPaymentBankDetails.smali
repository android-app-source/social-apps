.class public Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638388
    new-instance v0, LX/J19;

    invoke-direct {v0}, LX/J19;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2638358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638359
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->a:Ljava/lang/String;

    .line 2638360
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->b:Ljava/lang/String;

    .line 2638361
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->c:Ljava/lang/String;

    .line 2638362
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->d:Ljava/lang/String;

    .line 2638363
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->e:Ljava/lang/String;

    .line 2638364
    return-void
.end method

.method public constructor <init>(LX/0lF;)V
    .locals 6

    .prologue
    .line 2638379
    const-string v0, "code"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "image"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "redirect_url"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "dismiss_url"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "name"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2638380
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638382
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->a:Ljava/lang/String;

    .line 2638383
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->b:Ljava/lang/String;

    .line 2638384
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->c:Ljava/lang/String;

    .line 2638385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->d:Ljava/lang/String;

    .line 2638386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->e:Ljava/lang/String;

    .line 2638387
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2638372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638373
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->a:Ljava/lang/String;

    .line 2638374
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->b:Ljava/lang/String;

    .line 2638375
    iput-object p3, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->c:Ljava/lang/String;

    .line 2638376
    iput-object p4, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->d:Ljava/lang/String;

    .line 2638377
    iput-object p5, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->e:Ljava/lang/String;

    .line 2638378
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638371
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638365
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638366
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638367
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638368
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638369
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/pay/SendPaymentBankDetails;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638370
    return-void
.end method
