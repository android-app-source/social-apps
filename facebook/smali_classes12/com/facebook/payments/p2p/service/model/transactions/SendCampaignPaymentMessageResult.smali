.class public Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638745
    new-instance v0, LX/J1U;

    invoke-direct {v0}, LX/J1U;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2638746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638747
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;->a:LX/0Px;

    .line 2638748
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638750
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;->a:LX/0Px;

    .line 2638751
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638752
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638753
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2638754
    return-void
.end method
