.class public Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637946
    const-string v0, "deletePaymentCardParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->a:Ljava/lang/String;

    .line 2637947
    new-instance v0, LX/J0p;

    invoke-direct {v0}, LX/J0p;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 2637943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637944
    iput-wide p1, p0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    .line 2637945
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2637936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637937
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    .line 2637938
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2637942
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2637941
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "credentialId"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2637939
    iget-wide v0, p0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2637940
    return-void
.end method
