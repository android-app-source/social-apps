.class public Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final riskResult:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "risk_result"
    .end annotation
.end field

.field private final verificationUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "verification_url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638858
    const-class v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638857
    new-instance v0, LX/J1X;

    invoke-direct {v0}, LX/J1X;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2638852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638853
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->id:Ljava/lang/String;

    .line 2638854
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->riskResult:Ljava/lang/String;

    .line 2638855
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->verificationUrl:Ljava/lang/String;

    .line 2638856
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638848
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->id:Ljava/lang/String;

    .line 2638849
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->riskResult:Ljava/lang/String;

    .line 2638850
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->verificationUrl:Ljava/lang/String;

    .line 2638851
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638859
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638846
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638845
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->riskResult:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2638844
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->verificationUrl:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638843
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638839
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638840
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->riskResult:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638841
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->verificationUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638842
    return-void
.end method
