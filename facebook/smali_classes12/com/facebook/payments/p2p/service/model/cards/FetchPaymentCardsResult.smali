.class public Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;


# instance fields
.field public final b:Lcom/facebook/payments/p2p/model/PaymentCard;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2637980
    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    const/4 v1, 0x0

    .line 2637981
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2637982
    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;Ljava/util/List;)V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->a:Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    .line 2637983
    new-instance v0, LX/J0r;

    invoke-direct {v0}, LX/J0r;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2637984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637985
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2637986
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->c:LX/0Px;

    .line 2637987
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/p2p/model/PaymentCard;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/facebook/payments/p2p/model/PaymentCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2637988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637989
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2637990
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->c:LX/0Px;

    .line 2637991
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2637992
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2637993
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2637994
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2637995
    return-void
.end method
