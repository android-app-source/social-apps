.class public Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:LX/J1G;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638515
    const-string v0, "FetchPaymentRequestsParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->a:Ljava/lang/String;

    .line 2638516
    new-instance v0, LX/J1F;

    invoke-direct {v0}, LX/J1F;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/J1G;)V
    .locals 0

    .prologue
    .line 2638512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638513
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    .line 2638514
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638506
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/J1G;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    .line 2638507
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638511
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638510
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "queryType"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638508
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638509
    return-void
.end method
