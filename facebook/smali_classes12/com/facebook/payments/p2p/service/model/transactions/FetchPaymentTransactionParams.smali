.class public Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638637
    new-instance v0, LX/J1N;

    invoke-direct {v0}, LX/J1N;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638639
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    .line 2638640
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0rS;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    .line 2638641
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0rS;)V
    .locals 0

    .prologue
    .line 2638642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638643
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638644
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    .line 2638645
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    .line 2638646
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638647
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638648
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "transactionId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dataFreshnessParam"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    invoke-virtual {v2}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638649
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638650
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638651
    return-void
.end method
