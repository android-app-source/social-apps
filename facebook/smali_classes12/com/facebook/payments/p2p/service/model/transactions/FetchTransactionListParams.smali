.class public Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/DtK;


# instance fields
.field private final b:I

.field public final c:LX/DtK;

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638655
    sget-object v0, LX/DtK;->ALL:LX/DtK;

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->a:LX/DtK;

    .line 2638656
    new-instance v0, LX/J1O;

    invoke-direct {v0}, LX/J1O;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/DtK;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2638657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638658
    iput v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->b:I

    .line 2638659
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638660
    if-lez p2, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2638661
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    .line 2638662
    iput p2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->d:I

    .line 2638663
    return-void

    .line 2638664
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638666
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->b:I

    .line 2638667
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DtK;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    .line 2638668
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->d:I

    .line 2638669
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638670
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638671
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "transactionsQueryType"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "maxTransactionsToFetch"

    iget v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638672
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638673
    iget v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2638674
    return-void
.end method
