.class public Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638633
    new-instance v0, LX/J1M;

    invoke-direct {v0}, LX/J1M;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2638621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638622
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->a:LX/0Px;

    .line 2638623
    iput-boolean p2, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->b:Z

    .line 2638624
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638630
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->a:LX/0Px;

    .line 2638631
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->b:Z

    .line 2638632
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638628
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638625
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2638626
    iget-boolean v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2638627
    return-void
.end method
