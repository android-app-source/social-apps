.class public Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638727
    new-instance v0, LX/J1T;

    invoke-direct {v0}, LX/J1T;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2638728
    const-string v0, "sendCampaignPaymentMessageParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638730
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->b:LX/0Px;

    .line 2638731
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->c:Ljava/lang/String;

    .line 2638732
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->d:Ljava/lang/String;

    .line 2638733
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->e:Ljava/lang/String;

    .line 2638734
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638735
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638736
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "recipientIds"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "campaignName"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "externalRequestId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "message"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638737
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2638738
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638739
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638740
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638741
    return-void
.end method
