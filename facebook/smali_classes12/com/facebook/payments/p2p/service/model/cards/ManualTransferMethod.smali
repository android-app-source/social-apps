.class public Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/PaymentMethod;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2637999
    new-instance v0, LX/J0s;

    invoke-direct {v0}, LX/J0s;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638001
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->a:Ljava/lang/String;

    .line 2638002
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b:Ljava/lang/String;

    .line 2638003
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2638004
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->c:Landroid/net/Uri;

    .line 2638005
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->d:Ljava/lang/String;

    .line 2638006
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->e:Ljava/lang/String;

    .line 2638007
    return-void

    .line 2638008
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638009
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2638010
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 2638011
    sget-object v0, LX/6zU;->MANUAL_TRANSFER:LX/6zU;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638012
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638013
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638014
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638015
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638016
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638017
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638018
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638019
    return-void

    .line 2638020
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
