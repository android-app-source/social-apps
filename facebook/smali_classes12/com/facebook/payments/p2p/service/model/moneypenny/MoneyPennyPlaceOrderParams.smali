.class public Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public final f:J

.field public final g:J

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638282
    const-string v0, "moneyPennyPlaceOrderParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->a:Ljava/lang/String;

    .line 2638283
    new-instance v0, LX/J16;

    invoke-direct {v0}, LX/J16;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2638284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638285
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2638286
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->c:Ljava/lang/String;

    .line 2638287
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->d:Ljava/lang/String;

    .line 2638288
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->e:Ljava/lang/String;

    .line 2638289
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->f:J

    .line 2638290
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->g:J

    .line 2638291
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->h:Ljava/lang/String;

    .line 2638292
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638293
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2638294
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "amount"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2638295
    iget-object v3, v2, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v3

    .line 2638296
    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "userCredential"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "fingerprintNonce"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "requestId"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->f:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "itemId"

    iget-wide v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "userId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2638297
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2638298
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638299
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638300
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638301
    iget-wide v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2638302
    iget-wide v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2638303
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638304
    return-void
.end method
