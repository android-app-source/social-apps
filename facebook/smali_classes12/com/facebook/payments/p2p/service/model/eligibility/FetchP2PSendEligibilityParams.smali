.class public Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:LX/0rS;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638223
    const-string v0, "P2PSendEligibilityParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->a:Ljava/lang/String;

    .line 2638224
    new-instance v0, LX/J14;

    invoke-direct {v0}, LX/J14;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0rS;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2638225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638226
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2638227
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    .line 2638228
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    .line 2638229
    return-void

    .line 2638230
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638232
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0rS;

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    .line 2638233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    .line 2638234
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638235
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638236
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dataFreshnessParam"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "receiver"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638237
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2638238
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638239
    return-void
.end method
