.class public Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mIrisSequenceId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "iris_sequence_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2638549
    const-class v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638548
    new-instance v0, LX/J1I;

    invoke-direct {v0}, LX/J1I;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2638545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->mIrisSequenceId:Ljava/lang/String;

    .line 2638547
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638543
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->mIrisSequenceId:Ljava/lang/String;

    .line 2638544
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2638539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638540
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->mIrisSequenceId:Ljava/lang/String;

    .line 2638541
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638535
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->mIrisSequenceId:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638538
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638536
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/sync/FetchIrisSequenceIdResult;->mIrisSequenceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638537
    return-void
.end method
