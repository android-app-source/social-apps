.class public Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/PaymentMethod;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638057
    new-instance v0, LX/J0u;

    invoke-direct {v0}, LX/J0u;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->a:Ljava/lang/String;

    .line 2638053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->b:Ljava/lang/String;

    .line 2638054
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->c:Ljava/lang/String;

    .line 2638055
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->d:Ljava/lang/String;

    .line 2638056
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2638050
    const-string v0, "0"

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2638049
    const v0, 0x7f083a1b

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 2638048
    sget-object v0, LX/6zU;->NET_BANKING:LX/6zU;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638047
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 2638041
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638042
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638043
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638044
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638045
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638046
    return-void
.end method
