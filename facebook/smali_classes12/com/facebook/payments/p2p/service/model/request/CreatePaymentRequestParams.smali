.class public Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2638463
    const-string v0, "CreatePaymentRequestParams"

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->a:Ljava/lang/String;

    .line 2638464
    new-instance v0, LX/J1C;

    invoke-direct {v0}, LX/J1C;-><init>()V

    sput-object v0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2638435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->b:Ljava/lang/String;

    .line 2638437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->c:Ljava/lang/String;

    .line 2638438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->d:Ljava/lang/String;

    .line 2638439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->e:Ljava/lang/String;

    .line 2638440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->f:Ljava/lang/String;

    .line 2638441
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->g:Ljava/lang/String;

    .line 2638442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->h:Ljava/lang/String;

    .line 2638443
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2638444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638445
    iput-object p1, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->b:Ljava/lang/String;

    .line 2638446
    iput-object p2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->c:Ljava/lang/String;

    .line 2638447
    iput-object p3, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->d:Ljava/lang/String;

    .line 2638448
    iput-object p4, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->e:Ljava/lang/String;

    .line 2638449
    iput-object p5, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->f:Ljava/lang/String;

    .line 2638450
    iput-object p6, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->g:Ljava/lang/String;

    .line 2638451
    iput-object p7, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->h:Ljava/lang/String;

    .line 2638452
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2638453
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2638454
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "amount"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "offlineThreadingId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "requesteeId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "memoText"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "groupThreadId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "themeId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "platformContextId"

    iget-object v2, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2638455
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638456
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638457
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638458
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638459
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638460
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638461
    iget-object v0, p0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2638462
    return-void
.end method
