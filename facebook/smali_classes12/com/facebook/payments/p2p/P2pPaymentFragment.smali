.class public Lcom/facebook/payments/p2p/P2pPaymentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Iyy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/Iyy;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Iz5;

.field private f:LX/J1j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/Iz2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/J1i;

.field public i:LX/Iz1;

.field private j:LX/Iyw;

.field private k:LX/Iyx;

.field public l:LX/Iyv;

.field private m:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public n:Landroid/widget/ProgressBar;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/resources/ui/FbButton;

.field public q:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2634191
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2634192
    new-instance v0, LX/Iz3;

    invoke-direct {v0, p0}, LX/Iz3;-><init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->c:LX/0TF;

    .line 2634193
    new-instance v0, LX/Iz4;

    invoke-direct {v0, p0}, LX/Iz4;-><init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->d:LX/0TF;

    .line 2634194
    new-instance v0, LX/Iz5;

    invoke-direct {v0, p0}, LX/Iz5;-><init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->e:LX/Iz5;

    return-void
.end method

.method private static a(Lcom/facebook/payments/p2p/P2pPaymentFragment;LX/J1j;LX/Iz2;)V
    .locals 0

    .prologue
    .line 2634195
    iput-object p1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->f:LX/J1j;

    iput-object p2, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->g:LX/Iz2;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;

    const-class v0, LX/J1j;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/J1j;

    const-class v2, LX/Iz2;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Iz2;

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a(Lcom/facebook/payments/p2p/P2pPaymentFragment;LX/J1j;LX/Iz2;)V

    return-void
.end method


# virtual methods
.method public final a(LX/Iyw;LX/Iyx;)V
    .locals 5

    .prologue
    .line 2634157
    iput-object p2, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->k:LX/Iyx;

    .line 2634158
    iput-object p1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->j:LX/Iyw;

    .line 2634159
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->g:LX/Iz2;

    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->j:LX/Iyw;

    .line 2634160
    iget-object v2, v1, LX/Iyw;->a:LX/Iyu;

    move-object v1, v2

    .line 2634161
    new-instance v3, LX/Iz1;

    invoke-static {v0}, LX/Iyr;->a(LX/0QB;)LX/Iyr;

    move-result-object v2

    check-cast v2, LX/Iyr;

    invoke-direct {v3, v1, v2}, LX/Iz1;-><init>(LX/Iyu;LX/Iyr;)V

    .line 2634162
    move-object v0, v3

    .line 2634163
    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->i:LX/Iz1;

    .line 2634164
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->i:LX/Iz1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->k:LX/Iyx;

    iget-object v3, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->e:LX/Iz5;

    .line 2634165
    iget-object v4, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p2

    const/4 v4, 0x0

    move p1, v4

    :goto_0
    if-ge p1, p2, :cond_0

    iget-object v4, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Iz9;

    .line 2634166
    iput-object v2, v4, LX/Iz9;->b:LX/Iyx;

    .line 2634167
    iput-object v3, v4, LX/Iz9;->c:LX/Iz5;

    .line 2634168
    add-int/lit8 v4, p1, 0x1

    move p1, v4

    goto :goto_0

    .line 2634169
    :cond_0
    invoke-virtual {v3}, LX/Iz5;->a()V

    .line 2634170
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->i:LX/Iz1;

    const/4 v2, 0x1

    .line 2634171
    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2634172
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2634173
    :cond_1
    move-object v0, v3

    .line 2634174
    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2634175
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->c:LX/0TF;

    .line 2634176
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2634177
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2634178
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->f:LX/J1j;

    new-instance v1, LX/Iz6;

    invoke-direct {v1, p0}, LX/Iz6;-><init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->j:LX/Iyw;

    .line 2634179
    iget-object v4, v3, LX/Iyw;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2634180
    invoke-virtual {v0, v1, v2, v3}, LX/J1j;->a(LX/Ioo;ZLjava/lang/String;)LX/J1i;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->h:LX/J1i;

    .line 2634181
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->h:LX/J1i;

    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->m:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, v1}, LX/J1i;->a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V

    .line 2634182
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->h:LX/J1i;

    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->k:LX/Iyx;

    .line 2634183
    iget-object v2, v1, LX/Iyx;->a:Ljava/math/BigDecimal;

    move-object v1, v2

    .line 2634184
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J1i;->a(Ljava/lang/String;)V

    .line 2634185
    return-void

    .line 2634186
    :cond_2
    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iz9;

    invoke-virtual {v1}, LX/Iz9;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v3, v1

    .line 2634187
    :goto_1
    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2634188
    iget-object v1, v0, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iz9;

    .line 2634189
    new-instance v4, LX/Iyz;

    invoke-direct {v4, v0, v1}, LX/Iyz;-><init>(LX/Iz1;LX/Iz9;)V

    invoke-static {v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2634190
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634196
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2634197
    const-class v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;

    invoke-static {v0, p0}, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2634198
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2634153
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2634154
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/Iyv;

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->l:LX/Iyv;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2634155
    return-void

    .line 2634156
    :catch_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement P2pPaymentFragment.Listener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x22bdd7a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2634136
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0d2231

    if-ne v1, v2, :cond_1

    .line 2634137
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->n:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2634138
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->o:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2634139
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->i:LX/Iz1;

    .line 2634140
    iget-object v2, v1, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2634141
    sget-object v2, LX/Iyy;->SUCCESS:LX/Iyy;

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2634142
    :cond_0
    move-object v1, v5

    .line 2634143
    iput-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2634144
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->d:LX/0TF;

    .line 2634145
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 2634146
    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2634147
    :cond_1
    const v1, -0x7a7b87c3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2634148
    :cond_2
    iget-object v2, v1, LX/Iz1;->a:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iz9;

    invoke-virtual {v2}, LX/Iz9;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2634149
    const/4 v2, 0x1

    move-object v5, v3

    move v3, v2

    :goto_0
    iget-object v2, v1, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2634150
    iget-object v2, v1, LX/Iz1;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iz9;

    .line 2634151
    new-instance p1, LX/Iz0;

    invoke-direct {p1, v1, v2}, LX/Iz0;-><init>(LX/Iz1;LX/Iz9;)V

    invoke-static {v5, p1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2634152
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x19ce5798

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2634135
    const v1, 0x7f030dfe

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x25bd6a23

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, 0x2b09704e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2634129
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2634130
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2634131
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2634132
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2634133
    iget-object v1, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2634134
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x17b03d45    # 1.13892E-24f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634121
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2634122
    const v0, 0x7f0d2202

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->m:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2634123
    const v0, 0x7f0d222e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->n:Landroid/widget/ProgressBar;

    .line 2634124
    const v0, 0x7f0d222f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->o:Landroid/view/View;

    .line 2634125
    const v0, 0x7f0d2231

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->p:Lcom/facebook/resources/ui/FbButton;

    .line 2634126
    const v0, 0x7f0d2230

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->q:Landroid/widget/LinearLayout;

    .line 2634127
    iget-object v0, p0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2634128
    return-void
.end method
