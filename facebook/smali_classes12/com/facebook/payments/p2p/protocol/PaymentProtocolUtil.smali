.class public Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;


# instance fields
.field public final a:LX/0aG;

.field private final b:LX/0Sh;

.field private final c:LX/0tX;

.field public final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0aG;LX/0Sh;LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636387
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->e:Ljava/util/HashMap;

    .line 2636388
    iput-object p1, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    .line 2636389
    iput-object p2, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->b:LX/0Sh;

    .line 2636390
    iput-object p3, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->c:LX/0tX;

    .line 2636391
    iput-object p4, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->d:Ljava/util/concurrent/Executor;

    .line 2636392
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .locals 7

    .prologue
    .line 2636373
    sget-object v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    if-nez v0, :cond_1

    .line 2636374
    const-class v1, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    monitor-enter v1

    .line 2636375
    :try_start_0
    sget-object v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636376
    if-eqz v2, :cond_0

    .line 2636377
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2636378
    new-instance p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;-><init>(LX/0aG;LX/0Sh;LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 2636379
    move-object v0, p0

    .line 2636380
    sput-object v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636381
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636382
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636383
    :cond_1
    sget-object v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    return-object v0

    .line 2636384
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636385
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636372
    iget-object v0, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x3c7a4689

    move-object v1, p2

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636369
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2636370
    sget-object v1, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->a:Ljava/lang/String;

    new-instance v2, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;

    invoke-direct {v2, p1, p2, p3}, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;-><init>(JLjava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636371
    const-string v1, "set_primary_payment_card"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0rS;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rS;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636347
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2636348
    sget-object v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->a:Ljava/lang/String;

    new-instance v2, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    invoke-direct {v2, p1, p2}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;-><init>(LX/0rS;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636349
    const-string v1, "fetch_p2p_send_eligibility"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2636350
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq p1, v1, :cond_0

    .line 2636351
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {p0, v1, p2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0rS;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2636352
    :cond_0
    new-instance v1, LX/J06;

    invoke-direct {v1, p0}, LX/J06;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636393
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2636394
    const-string v8, "editPaymentCardParams"

    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636395
    iget-object v0, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v1, "edit_payment_card"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x56e266a9

    move-object v2, v7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    new-instance v1, LX/4At;

    invoke-direct {v1, p1, p6}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2636396
    new-instance v1, LX/J08;

    invoke-direct {v1, p0}, LX/J08;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636365
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2636366
    const-string v10, "addPaymentCardParams"

    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636367
    const-string v0, "add_payment_card"

    invoke-static {p0, v9, v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2636368
    new-instance v1, LX/J07;

    invoke-direct {v1, p0}, LX/J07;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636361
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2636362
    const-string v8, "editPaymentCardParams"

    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636363
    const-string v0, "edit_payment_card"

    invoke-static {p0, v7, v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2636364
    new-instance v1, LX/J09;

    invoke-direct {v1, p0}, LX/J09;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2636358
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2636359
    sget-object v9, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->a:Ljava/lang/String;

    new-instance v0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2636360
    iget-object v0, p0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v1, "create_payment_request"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x2f3f821c

    move-object v2, v8

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2636353
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2636354
    const-string v1, "fetch_theme_list"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2636355
    new-instance v1, LX/J05;

    invoke-direct {v1, p0}, LX/J05;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    .line 2636356
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2636357
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
