.class public Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639897
    return-void
.end method

.method private static a(LX/J20;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2639889
    if-eqz p1, :cond_0

    .line 2639890
    :goto_0
    return-object p1

    .line 2639891
    :cond_0
    invoke-interface {p0}, LX/J20;->a()LX/J2w;

    move-result-object v0

    sget-object v1, LX/J2w;->SPACED_DOUBLE_DIVIDER:LX/J2w;

    if-ne v0, v1, :cond_1

    .line 2639892
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031392

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 2639893
    :cond_1
    invoke-interface {p0}, LX/J20;->a()LX/J2w;

    move-result-object v0

    sget-object v1, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    if-ne v0, v1, :cond_2

    .line 2639894
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031341

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 2639895
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, LX/J20;->a()LX/J2w;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/J29;LX/J20;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2639802
    sget-object v0, LX/J2C;->a:[I

    invoke-interface {p2}, LX/J20;->a()LX/J2w;

    move-result-object v1

    invoke-virtual {v1}, LX/J2w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2639803
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/J20;->a()LX/J2w;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2639804
    :pswitch_0
    check-cast p2, LX/J2c;

    .line 2639805
    if-nez p3, :cond_5

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300a7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2639806
    :goto_0
    iget-object v1, p2, LX/J2c;->a:LX/3Ab;

    new-instance v2, LX/J2B;

    invoke-direct {v2, p0, p1}, LX/J2B;-><init>(Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;LX/J29;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 2639807
    move-object v0, v0

    .line 2639808
    :goto_1
    return-object v0

    .line 2639809
    :pswitch_1
    check-cast p2, LX/J2j;

    .line 2639810
    if-nez p3, :cond_6

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030df2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/ImageDetailView;

    .line 2639811
    :goto_2
    iget-object v1, p2, LX/J2j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2639812
    iget-object v1, p2, LX/J2j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2639813
    iget-object v1, p2, LX/J2j;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setImageUrl(Landroid/net/Uri;)V

    .line 2639814
    move-object v0, v0

    .line 2639815
    goto :goto_1

    .line 2639816
    :pswitch_2
    invoke-static {p2, p3, p4}, Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;->a(LX/J20;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 2639817
    :pswitch_3
    check-cast p2, LX/J2r;

    .line 2639818
    if-nez p3, :cond_8

    new-instance p3, LX/J1x;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/J1x;-><init>(Landroid/content/Context;)V

    .line 2639819
    :goto_4
    iget-object v0, p2, LX/J2r;->c:LX/J2e;

    move-object v0, v0

    .line 2639820
    check-cast v0, LX/J2f;

    .line 2639821
    const v1, 0x7f0308d1

    invoke-virtual {p3, v1}, LX/J1x;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2639822
    iget-object v2, v0, LX/J2f;->a:LX/J2d;

    if-eqz v2, :cond_9

    iget-object v2, v0, LX/J2f;->a:LX/J2d;

    iget-object v2, v2, LX/J2d;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 2639823
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2639824
    iget-object p0, v0, LX/J2f;->a:LX/J2d;

    iget p0, p0, LX/J2d;->a:I

    iput p0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2639825
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2639826
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2639827
    iget-object v0, v0, LX/J2f;->a:LX/J2d;

    iget-object v0, v0, LX/J2d;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v2, LX/J1x;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2639828
    :goto_5
    invoke-virtual {p3, p2, p1}, LX/J1x;->a(LX/J2r;LX/J29;)V

    .line 2639829
    move-object v0, p3

    .line 2639830
    goto :goto_1

    .line 2639831
    :pswitch_4
    check-cast p2, LX/J2r;

    .line 2639832
    if-nez p3, :cond_a

    new-instance p3, LX/J1x;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/J1x;-><init>(Landroid/content/Context;)V

    .line 2639833
    :goto_6
    invoke-virtual {p3, p2, p1}, LX/J1x;->a(LX/J2r;LX/J29;)V

    .line 2639834
    move-object v0, p3

    .line 2639835
    goto/16 :goto_1

    .line 2639836
    :pswitch_5
    check-cast p2, LX/J2r;

    const/4 v5, 0x0

    .line 2639837
    if-nez p3, :cond_0

    new-instance p3, LX/J1x;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/J1x;-><init>(Landroid/content/Context;)V

    .line 2639838
    :goto_7
    const v0, 0x7f030dde

    invoke-virtual {p3, v0}, LX/J1x;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2639839
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2639840
    iget-object v1, p2, LX/J2r;->c:LX/J2e;

    move-object v1, v1

    .line 2639841
    check-cast v1, LX/J2i;

    iget-object v6, v1, LX/J2i;->a:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v5

    :goto_8
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/J2h;

    .line 2639842
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03119b

    invoke-virtual {v2, v3, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/ui/ImageDetailView;

    .line 2639843
    iget-object v3, v1, LX/J2h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/ui/ImageDetailView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2639844
    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->REGULAR:LX/0xr;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/payments/ui/ImageDetailView;->a(LX/0xq;LX/0xr;)V

    .line 2639845
    iget-object v3, v1, LX/J2h;->c:LX/0Px;

    invoke-static {v3}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v3

    invoke-virtual {v3}, LX/0wv;->a()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/facebook/payments/ui/ImageDetailView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2639846
    iget-object v1, v1, LX/J2h;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/facebook/payments/ui/ImageDetailView;->setAuxTextView(Ljava/lang/CharSequence;)V

    .line 2639847
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2639848
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8

    .line 2639849
    :cond_0
    check-cast p3, LX/J1x;

    goto :goto_7

    .line 2639850
    :cond_1
    invoke-virtual {p3, p2, p1}, LX/J1x;->a(LX/J2r;LX/J29;)V

    .line 2639851
    move-object v0, p3

    .line 2639852
    goto/16 :goto_1

    .line 2639853
    :pswitch_6
    check-cast p2, LX/J2r;

    .line 2639854
    if-nez p3, :cond_b

    new-instance p3, LX/J1x;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/J1x;-><init>(Landroid/content/Context;)V

    .line 2639855
    :goto_9
    const v0, 0x7f03100c

    invoke-virtual {p3, v0}, LX/J1x;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PriceTableView;

    .line 2639856
    iget-object v1, p2, LX/J2r;->c:LX/J2e;

    move-object v1, v1

    .line 2639857
    check-cast v1, LX/J2k;

    iget-object v1, v1, LX/J2k;->a:LX/0Px;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PriceTableView;->setRowDatas(LX/0Px;)V

    .line 2639858
    invoke-virtual {p3, p2, p1}, LX/J1x;->a(LX/J2r;LX/J29;)V

    .line 2639859
    move-object v0, p3

    .line 2639860
    goto/16 :goto_1

    .line 2639861
    :pswitch_7
    check-cast p2, LX/J2r;

    const/4 v5, 0x0

    .line 2639862
    if-nez p3, :cond_3

    new-instance p3, LX/J1x;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/J1x;-><init>(Landroid/content/Context;)V

    .line 2639863
    :goto_a
    const v0, 0x7f031094

    invoke-virtual {p3, v0}, LX/J1x;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/ProgressLayout;

    .line 2639864
    invoke-virtual {v0}, Lcom/facebook/payments/ui/ProgressLayout;->removeAllViews()V

    .line 2639865
    iget-object v1, p2, LX/J2r;->c:LX/J2e;

    move-object v1, v1

    .line 2639866
    check-cast v1, LX/J2m;

    move v4, v5

    move v6, v5

    .line 2639867
    :goto_b
    iget-object v2, v1, LX/J2m;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_4

    .line 2639868
    iget-object v2, v1, LX/J2m;->b:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J2l;

    .line 2639869
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v7, 0x7f031095

    invoke-virtual {v3, v7, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2639870
    iget-object v7, v2, LX/J2l;->a:Ljava/lang/String;

    iget-object p0, v1, LX/J2m;->a:LX/J2l;

    iget-object p0, p0, LX/J2l;->a:Ljava/lang/String;

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2639871
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0a00aa

    invoke-static {v6, v7}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTextColor(I)V

    .line 2639872
    add-int/lit8 v6, v4, 0x1

    .line 2639873
    :cond_2
    iget-object v7, v2, LX/J2l;->a:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2639874
    iget-object v2, v2, LX/J2l;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2639875
    invoke-virtual {v0, v3}, Lcom/facebook/payments/ui/ProgressLayout;->addView(Landroid/view/View;)V

    .line 2639876
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_b

    .line 2639877
    :cond_3
    check-cast p3, LX/J1x;

    goto :goto_a

    .line 2639878
    :cond_4
    invoke-virtual {v0, v6}, Lcom/facebook/payments/ui/ProgressLayout;->setProgress(I)V

    .line 2639879
    invoke-virtual {p3, p2, p1}, LX/J1x;->a(LX/J2r;LX/J29;)V

    .line 2639880
    move-object v0, p3

    .line 2639881
    goto/16 :goto_1

    .line 2639882
    :cond_5
    check-cast p3, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    move-object v0, p3

    goto/16 :goto_0

    .line 2639883
    :cond_6
    check-cast p3, Lcom/facebook/payments/ui/ImageDetailView;

    move-object v0, p3

    goto/16 :goto_2

    .line 2639884
    :cond_7
    iget-object v1, p2, LX/J2j;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_3

    .line 2639885
    :cond_8
    check-cast p3, LX/J1x;

    goto/16 :goto_4

    .line 2639886
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_5

    .line 2639887
    :cond_a
    check-cast p3, LX/J1x;

    goto/16 :goto_6

    .line 2639888
    :cond_b
    check-cast p3, LX/J1x;

    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
