.class public Lcom/facebook/payments/receipt/components/ReceiptListView;
.super LX/73U;
.source ""

# interfaces
.implements LX/J1v;


# instance fields
.field public a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

.field private b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private c:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2639637
    invoke-direct {p0, p1}, LX/73U;-><init>(Landroid/content/Context;)V

    .line 2639638
    invoke-direct {p0}, Lcom/facebook/payments/receipt/components/ReceiptListView;->e()V

    .line 2639639
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639668
    invoke-direct {p0, p1, p2}, LX/73U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2639669
    invoke-direct {p0}, Lcom/facebook/payments/receipt/components/ReceiptListView;->e()V

    .line 2639670
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639665
    invoke-direct {p0, p1, p2, p3}, LX/73U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2639666
    invoke-direct {p0}, Lcom/facebook/payments/receipt/components/ReceiptListView;->e()V

    .line 2639667
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2639661
    const v0, 0x7f03119c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2639662
    const v0, 0x7f0d0a95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2639663
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->c:Landroid/widget/ListView;

    .line 2639664
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2639658
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->c:Landroid/widget/ListView;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 2639659
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2639660
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2639655
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->c:Landroid/widget/ListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 2639656
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2639657
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2639653
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Lcom/facebook/payments/receipt/components/ReceiptListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J1y;

    invoke-direct {v2, p0}, LX/J1y;-><init>(Lcom/facebook/payments/receipt/components/ReceiptListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2639654
    return-void
.end method

.method public setData(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/J20;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2639644
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639645
    iget-object v1, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->c:LX/J1z;

    move-object v0, v1

    .line 2639646
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->c:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2639647
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2639648
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/J1z;->setNotifyOnChange(Z)V

    .line 2639649
    invoke-virtual {v0}, LX/J1z;->clear()V

    .line 2639650
    invoke-virtual {v0, p1}, LX/J1z;->addAll(Ljava/util/Collection;)V

    .line 2639651
    const v1, -0x2f66e820

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2639652
    return-void
.end method

.method public setReceiptComponentController(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V
    .locals 1

    .prologue
    .line 2639640
    iput-object p1, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639641
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptListView;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639642
    iput-object p0, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    .line 2639643
    return-void
.end method
