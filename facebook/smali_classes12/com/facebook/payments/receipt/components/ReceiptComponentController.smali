.class public Lcom/facebook/payments/receipt/components/ReceiptComponentController;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J25;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/J1z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/J2D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/J22;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/6qh;

.field public i:LX/J1v;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/J29;

.field public k:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2639534
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2639535
    new-instance v0, LX/J1r;

    invoke-direct {v0, p0}, LX/J1r;-><init>(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V

    iput-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->h:LX/6qh;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/J25;->a(LX/0QB;)LX/J25;

    move-result-object v3

    check-cast v3, LX/J25;

    new-instance v5, LX/J1z;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v4}, LX/J1z;-><init>(Landroid/content/Context;)V

    move-object v4, v5

    check-cast v4, LX/J1z;

    new-instance v7, LX/J2D;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-direct {v7, v5, v6}, LX/J2D;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0tX;)V

    move-object v5, v7

    check-cast v5, LX/J2D;

    new-instance p1, LX/J22;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v7

    check-cast v7, LX/5fv;

    invoke-direct {p1, v6, v7}, LX/J22;-><init>(Landroid/content/res/Resources;LX/5fv;)V

    move-object v6, p1

    check-cast v6, LX/J22;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v2, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->b:LX/J25;

    iput-object v4, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->c:LX/J1z;

    iput-object v5, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->d:LX/J2D;

    iput-object v6, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->e:LX/J22;

    iput-object v7, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->g:LX/1Ck;

    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 2639536
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    if-eqz v0, :cond_0

    .line 2639537
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    invoke-interface {v0}, LX/J1v;->a()V

    .line 2639538
    :cond_0
    new-instance v0, LX/J1t;

    invoke-direct {v0, p0}, LX/J1t;-><init>(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V

    .line 2639539
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->g:LX/1Ck;

    const-string v2, "fetch_receipt_details_key"

    iget-object v3, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->d:LX/J2D;

    iget-object v4, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->k:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2639540
    iget-object p0, v4, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    move-object v4, p0

    .line 2639541
    new-instance v5, LX/J2E;

    invoke-direct {v5}, LX/J2E;-><init>()V

    move-object v5, v5

    .line 2639542
    const-string p0, "0"

    invoke-virtual {v5, p0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2639543
    iget-object p0, v3, LX/J2D;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2639544
    iput-object p0, v5, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2639545
    iget-object p0, v3, LX/J2D;->b:LX/0tX;

    invoke-virtual {p0, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 2639546
    move-object v3, v5

    .line 2639547
    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2639548
    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 2

    .prologue
    .line 2639549
    sget-object v0, LX/J1u;->a:[I

    .line 2639550
    iget-object v1, p1, LX/73T;->a:LX/73S;

    move-object v1, v1

    .line 2639551
    invoke-virtual {v1}, LX/73S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2639552
    :goto_0
    return-void

    .line 2639553
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->d()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639554
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2639555
    const-class v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-static {v0, p0}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a(Ljava/lang/Class;LX/02k;)V

    .line 2639556
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2639557
    const-string v1, "extra_controller_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->k:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2639558
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->k:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2639559
    iget-object v1, v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    move-object v0, v1

    .line 2639560
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->b:LX/J25;

    .line 2639561
    iget-object v2, v1, LX/J25;->a:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J23;

    iget-object v2, v2, LX/J23;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J29;

    move-object v1, v2

    .line 2639562
    iput-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->j:LX/J29;

    .line 2639563
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->j:LX/J29;

    iget-object v2, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->h:LX/6qh;

    .line 2639564
    iput-object v2, v1, LX/J29;->e:LX/6qh;

    .line 2639565
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->c:LX/J1z;

    iget-object v2, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->b:LX/J25;

    .line 2639566
    iget-object p1, v2, LX/J25;->a:LX/0P1;

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/J23;

    iget-object p1, p1, LX/J23;->c:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;

    move-object v0, p1

    .line 2639567
    iget-object v2, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->j:LX/J29;

    .line 2639568
    iput-object v0, v1, LX/J1z;->b:Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;

    .line 2639569
    iput-object v2, v1, LX/J1z;->a:LX/J29;

    .line 2639570
    invoke-direct {p0}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->d()V

    .line 2639571
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639572
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    if-eqz v0, :cond_0

    .line 2639573
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    invoke-interface {v0}, LX/J1v;->b()V

    .line 2639574
    iget-object v0, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    invoke-interface {v0}, LX/J1v;->c()V

    .line 2639575
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x27f48f25

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2639576
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2639577
    iget-object v1, p0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2639578
    const/16 v1, 0x2b

    const v2, 0x5ff04b7a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
