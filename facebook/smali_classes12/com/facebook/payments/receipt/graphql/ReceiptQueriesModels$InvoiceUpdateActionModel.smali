.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/J2G;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x312eb538
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640641
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640640
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640638
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640639
    return-void
.end method

.method private a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640617
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2640618
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2640632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640633
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2640634
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2640635
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2640636
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640637
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2640624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640625
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2640626
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2640627
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2640628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;

    .line 2640629
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2640630
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640631
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640621
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel;-><init>()V

    .line 2640622
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640623
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640620
    const v0, -0x58ad1447

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640619
    const v0, -0x17bca39a

    return v0
.end method
