.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/J2H;
.implements LX/J2I;
.implements LX/J2J;
.implements LX/J2K;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2c7f1280
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640099
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640127
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640125
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640126
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640122
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2640123
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2640124
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640120
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    .line 2640121
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2640100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640101
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2640102
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2640103
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->mL_()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 2640104
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2640105
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2640106
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->c()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2640107
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->d()LX/0Px;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 2640108
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e()LX/0Px;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 2640109
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2640110
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2640111
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2640112
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2640113
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2640114
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2640115
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2640116
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2640117
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2640118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640119
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640097
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->f:Ljava/util/List;

    .line 2640098
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2640064
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640065
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2640066
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2640067
    if-eqz v1, :cond_0

    .line 2640068
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640069
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->f:Ljava/util/List;

    .line 2640070
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->mL_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2640071
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->mL_()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2640072
    if-eqz v1, :cond_1

    .line 2640073
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640074
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->g:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 2640075
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2640076
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    .line 2640077
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2640078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640079
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    .line 2640080
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2640081
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2640082
    if-eqz v2, :cond_3

    .line 2640083
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640084
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 2640085
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->d()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2640086
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2640087
    if-eqz v2, :cond_4

    .line 2640088
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640089
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 2640090
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2640091
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->e()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2640092
    if-eqz v2, :cond_5

    .line 2640093
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640094
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->l:Ljava/util/List;

    move-object v1, v0

    .line 2640095
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640096
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640128
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;-><init>()V

    .line 2640129
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640130
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640062
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->i:Ljava/lang/String;

    .line 2640063
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640060
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->j:Ljava/util/List;

    .line 2640061
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640051
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k:Ljava/util/List;

    .line 2640052
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640059
    const v0, 0x63e4e92b

    return v0
.end method

.method public final e()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640057
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$PriceListModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->l:Ljava/util/List;

    .line 2640058
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640056
    const v0, 0xeddea05

    return v0
.end method

.method public final mL_()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640054
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->g:Ljava/util/List;

    .line 2640055
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic mM_()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640053
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$CurrentStepModel;

    move-result-object v0

    return-object v0
.end method
