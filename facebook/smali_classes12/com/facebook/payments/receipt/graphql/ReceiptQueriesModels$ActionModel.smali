.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/J2G;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x510c9653
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2639974
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2639973
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2639971
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2639972
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639968
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2639969
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2639970
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639934
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->g:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->g:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2639935
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->g:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2639954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2639955
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2639956
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->a()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2639957
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2639958
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->c()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2639959
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2639960
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2639961
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2639962
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2639963
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2639964
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2639965
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2639966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2639967
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2639946
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2639947
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2639948
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2639949
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2639950
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;

    .line 2639951
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->g:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    .line 2639952
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2639953
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639975
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    .line 2639976
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->f:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2639943
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;-><init>()V

    .line 2639944
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2639945
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639942
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->k()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$InvoiceUpdateActionModel$InvoiceModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639940
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->h:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->h:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    .line 2639941
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->h:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionStyle;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639938
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->i:Ljava/lang/String;

    .line 2639939
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ActionModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2639937
    const v0, 0x4bf6c9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2639936
    const v0, 0x2884e1ab

    return v0
.end method
