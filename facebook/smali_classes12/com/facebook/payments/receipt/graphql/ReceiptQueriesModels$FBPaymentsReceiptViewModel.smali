.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1fc80a21
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640365
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640364
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640362
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640363
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640311
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2640312
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2640313
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640360
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2640361
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640358
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->g:Ljava/lang/String;

    .line 2640359
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640356
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    .line 2640357
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2640342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640343
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2640344
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2640345
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2640346
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2640347
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->d()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 2640348
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2640349
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2640350
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2640351
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2640352
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2640353
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2640354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640355
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2640324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640325
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2640326
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2640327
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2640328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    .line 2640329
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2640330
    :cond_0
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2640331
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    .line 2640332
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2640333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    .line 2640334
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->h:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    .line 2640335
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->d()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2640336
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2640337
    if-eqz v2, :cond_2

    .line 2640338
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    .line 2640339
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->i:Ljava/util/List;

    move-object v1, v0

    .line 2640340
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640341
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640323
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/3Ab;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640322
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640319
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;-><init>()V

    .line 2640320
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640321
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640318
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->m()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640316
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->i:Ljava/util/List;

    .line 2640317
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640315
    const v0, 0x7feb7395

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640314
    const v0, -0x14f708a6

    return v0
.end method
