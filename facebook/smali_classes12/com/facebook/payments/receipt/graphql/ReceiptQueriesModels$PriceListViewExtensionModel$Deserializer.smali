.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2640828
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel;

    new-instance v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2640829
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2640804
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2640805
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2640806
    const/4 v2, 0x0

    .line 2640807
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2640808
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2640809
    :goto_0
    move v1, v2

    .line 2640810
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2640811
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2640812
    new-instance v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel;

    invoke-direct {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$PriceListViewExtensionModel;-><init>()V

    .line 2640813
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2640814
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2640815
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2640816
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2640817
    :cond_0
    return-object v1

    .line 2640818
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2640819
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_3

    .line 2640820
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2640821
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2640822
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 2640823
    const-string p0, "price_list"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2640824
    invoke-static {p1, v0}, LX/J2Y;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2640825
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2640826
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2640827
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
