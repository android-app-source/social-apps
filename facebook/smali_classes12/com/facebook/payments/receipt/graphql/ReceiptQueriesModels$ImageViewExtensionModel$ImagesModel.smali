.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x473d1b46
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640487
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640486
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640484
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640485
    return-void
.end method

.method private j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640482
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    .line 2640483
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2640476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640477
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2640478
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2640479
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2640480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640481
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2640468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640469
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2640470
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    .line 2640471
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2640472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;

    .line 2640473
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    .line 2640474
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640475
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640467
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640464
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ImageViewExtensionModel$ImagesModel;-><init>()V

    .line 2640465
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640466
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640462
    const v0, 0x14969ca6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640463
    const v0, 0x4984e12

    return v0
.end method
