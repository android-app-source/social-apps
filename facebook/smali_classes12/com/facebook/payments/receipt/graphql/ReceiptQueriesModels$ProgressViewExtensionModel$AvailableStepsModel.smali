.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x759fa8bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2641005
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640982
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2641003
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2641004
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2640995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640996
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2640997
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2640998
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2640999
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2641000
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2641001
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2641002
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2640992
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640994
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640990
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->e:Ljava/lang/String;

    .line 2640991
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640987
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;-><init>()V

    .line 2640988
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640989
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640985
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->f:Ljava/lang/String;

    .line 2640986
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$AvailableStepsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640984
    const v0, -0xa128f7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640983
    const v0, -0x14f83c7f

    return v0
.end method
