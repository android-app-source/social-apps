.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2640131
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    new-instance v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2640132
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2640133
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2640134
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2640135
    const/4 v2, 0x0

    .line 2640136
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 2640137
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2640138
    :goto_0
    move v1, v2

    .line 2640139
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2640140
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2640141
    new-instance v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    invoke-direct {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;-><init>()V

    .line 2640142
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2640143
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2640144
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2640145
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2640146
    :cond_0
    return-object v1

    .line 2640147
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2640148
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_9

    .line 2640149
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2640150
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2640151
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 2640152
    const-string p0, "__type__"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2640153
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 2640154
    :cond_4
    const-string p0, "additional_instruction"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2640155
    invoke-static {p1, v0}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2640156
    :cond_5
    const-string p0, "id"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2640157
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2640158
    :cond_6
    const-string p0, "other_participant"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2640159
    invoke-static {p1, v0}, LX/J2Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2640160
    :cond_7
    const-string p0, "sections"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2640161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2640162
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, p0, :cond_8

    .line 2640163
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, p0, :cond_8

    .line 2640164
    invoke-static {p1, v0}, LX/J2R;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2640165
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2640166
    :cond_8
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2640167
    goto/16 :goto_1

    .line 2640168
    :cond_9
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2640169
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2640170
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2640171
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2640172
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2640173
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2640174
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1
.end method
