.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2641079
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel;

    new-instance v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2641080
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2641081
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2641082
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2641083
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2641084
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2641085
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2641086
    if-eqz v2, :cond_0

    .line 2641087
    const-string p0, "available_steps"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641088
    invoke-static {v1, v2, p1, p2}, LX/J2Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641089
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2641090
    if-eqz v2, :cond_1

    .line 2641091
    const-string p0, "current_step"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641092
    invoke-static {v1, v2, p1}, LX/J2a;->a(LX/15i;ILX/0nX;)V

    .line 2641093
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2641094
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2641095
    check-cast p1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel$Serializer;->a(Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ProgressViewExtensionModel;LX/0nX;LX/0my;)V

    return-void
.end method
