.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7156d24e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640025
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640026
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640029
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640030
    return-void
.end method

.method private j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640027
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640028
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2640003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640004
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2640005
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2640006
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2640007
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2640008
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2640009
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2640010
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2640011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640012
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2640016
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640017
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2640018
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640019
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2640020
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;

    .line 2640021
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->e:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    .line 2640022
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640023
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640024
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640013
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;-><init>()V

    .line 2640014
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640015
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640001
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->f:Ljava/lang/String;

    .line 2640002
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2639999
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->g:Ljava/lang/String;

    .line 2640000
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2639998
    const v0, 0x5bbb43b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2639997
    const v0, -0x2010153d

    return v0
.end method
