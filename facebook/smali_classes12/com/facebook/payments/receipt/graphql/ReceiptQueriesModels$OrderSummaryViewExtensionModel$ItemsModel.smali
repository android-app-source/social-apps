.class public final Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x62ac5af9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640758
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2640763
    const-class v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2640761
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2640762
    return-void
.end method

.method private j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640759
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->f:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->f:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    .line 2640760
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->f:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2640732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640733
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2640734
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2640735
    invoke-virtual {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 2640736
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2640737
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2640738
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2640739
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2640740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640741
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2640750
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2640751
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2640752
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    .line 2640753
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2640754
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;

    .line 2640755
    iput-object v0, v1, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->f:Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    .line 2640756
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2640757
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640764
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->e:Ljava/lang/String;

    .line 2640765
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2640747
    new-instance v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;

    invoke-direct {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;-><init>()V

    .line 2640748
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2640749
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2640746
    invoke-direct {p0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->j()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel$PriceModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2640744
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->g:Ljava/util/List;

    .line 2640745
    iget-object v0, p0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$OrderSummaryViewExtensionModel$ItemsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2640743
    const v0, -0x49c0bf49

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2640742
    const v0, -0x14fcc838

    return v0
.end method
