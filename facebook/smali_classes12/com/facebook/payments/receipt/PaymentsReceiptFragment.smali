.class public Lcom/facebook/payments/receipt/PaymentsReceiptFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field private a:Landroid/content/Context;

.field public b:LX/0h5;

.field public c:Lcom/facebook/payments/receipt/components/ReceiptListView;

.field public d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2639420
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2639421
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639422
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2639423
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->a:Landroid/content/Context;

    .line 2639424
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2639425
    const-string v1, "extra_receipt_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639426
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x79343e8e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2639427
    iget-object v1, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f1e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6a810d2d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2639428
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2639429
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 2639430
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2639431
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, LX/J1q;

    invoke-direct {v2, p0}, LX/J1q;-><init>(Lcom/facebook/payments/receipt/PaymentsReceiptFragment;)V

    iget-object p1, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639432
    move-object p1, p1

    .line 2639433
    iget-object p1, p1, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p2, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639434
    move-object p2, p2

    .line 2639435
    iget-object p2, p2, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p2, p2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p2}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p2

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 2639436
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 2639437
    iput-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->b:LX/0h5;

    .line 2639438
    iget-object v1, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->b:LX/0h5;

    iget-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639439
    move-object v0, v0

    .line 2639440
    iget-object v0, v0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const v0, 0x7f0838d4

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2639441
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2639442
    const-string v1, "receipt_component_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639443
    if-nez v0, :cond_1

    .line 2639444
    iget-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639445
    move-object v0, v0

    .line 2639446
    iget-object v0, v0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2639447
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2639448
    const-string v2, "extra_controller_params"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2639449
    new-instance v2, Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-direct {v2}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;-><init>()V

    .line 2639450
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2639451
    move-object v0, v2

    .line 2639452
    move-object v1, v0

    .line 2639453
    :goto_1
    const v0, 0x7f0d24b4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/components/ReceiptListView;

    iput-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->c:Lcom/facebook/payments/receipt/components/ReceiptListView;

    .line 2639454
    iget-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->c:Lcom/facebook/payments/receipt/components/ReceiptListView;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/receipt/components/ReceiptListView;->setReceiptComponentController(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V

    .line 2639455
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2639456
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const-string v2, "receipt_component_fragment_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2639457
    return-void

    .line 2639458
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/receipt/PaymentsReceiptFragment;->d:Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    .line 2639459
    move-object v0, v0

    .line 2639460
    iget-object v0, v0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->c:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method
