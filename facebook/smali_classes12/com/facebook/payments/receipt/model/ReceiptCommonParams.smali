.class public Lcom/facebook/payments/receipt/model/ReceiptCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/receipt/model/ReceiptCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

.field public final b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final c:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2641754
    new-instance v0, LX/J2o;

    invoke-direct {v0}, LX/J2o;-><init>()V

    sput-object v0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/J2p;)V
    .locals 1

    .prologue
    .line 2641755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2641756
    iget-object v0, p1, LX/J2p;->a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2641757
    iget-object v0, p1, LX/J2p;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2641758
    iget-object v0, p1, LX/J2p;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->c:Ljava/lang/String;

    .line 2641759
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2641760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2641761
    const-class v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2641762
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2641763
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->c:Ljava/lang/String;

    .line 2641764
    return-void
.end method

.method public static a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;)LX/J2p;
    .locals 1

    .prologue
    .line 2641765
    new-instance v0, LX/J2p;

    invoke-direct {v0, p0}, LX/J2p;-><init>(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2641766
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2641767
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2641768
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2641769
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2641770
    return-void
.end method
