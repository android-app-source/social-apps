.class public Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/J2v;


# instance fields
.field public final b:LX/6xh;

.field public final c:Ljava/lang/String;

.field public final d:LX/J2x;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2641827
    new-instance v0, LX/J2s;

    invoke-direct {v0}, LX/J2s;-><init>()V

    sput-object v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 2641828
    new-instance v0, LX/J2v;

    invoke-direct {v0}, LX/J2v;-><init>()V

    sput-object v0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->a:LX/J2v;

    return-void
.end method

.method public constructor <init>(LX/J2t;)V
    .locals 3

    .prologue
    .line 2641815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2641816
    iget-object v0, p1, LX/J2t;->c:LX/6xh;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xh;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    .line 2641817
    iget-object v0, p1, LX/J2t;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    .line 2641818
    iget-object v0, p1, LX/J2t;->e:LX/J2x;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J2x;

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    .line 2641819
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2641820
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    move-object v0, v0

    .line 2641821
    sget-object p1, LX/6xh;->UNKNOWN:LX/6xh;

    if-eq v0, p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2641822
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2641823
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2641824
    return-void

    :cond_0
    move v0, v2

    .line 2641825
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2641826
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2641810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2641811
    invoke-static {}, LX/6xh;->values()[LX/6xh;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    .line 2641812
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    .line 2641813
    invoke-static {}, LX/J2x;->values()[LX/J2x;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    .line 2641814
    return-void
.end method

.method public static newBuilder()LX/J2t;
    .locals 2

    .prologue
    .line 2641829
    new-instance v0, LX/J2t;

    invoke-direct {v0}, LX/J2t;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2641809
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2641798
    if-ne p0, p1, :cond_1

    .line 2641799
    :cond_0
    :goto_0
    return v0

    .line 2641800
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 2641801
    goto :goto_0

    .line 2641802
    :cond_2
    check-cast p1, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2641803
    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    iget-object v3, p1, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2641804
    goto :goto_0

    .line 2641805
    :cond_3
    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2641806
    goto :goto_0

    .line 2641807
    :cond_4
    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    iget-object v3, p1, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2641808
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2641797
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2641793
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->b:LX/6xh;

    invoke-virtual {v0}, LX/6xh;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2641794
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2641795
    iget-object v0, p0, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->d:LX/J2x;

    invoke-virtual {v0}, LX/J2x;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2641796
    return-void
.end method
