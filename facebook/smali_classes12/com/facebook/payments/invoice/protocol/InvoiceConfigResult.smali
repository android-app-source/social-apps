.class public Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633629
    new-instance v0, LX/Iyb;

    invoke-direct {v0}, LX/Iyb;-><init>()V

    sput-object v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2633630
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;-><init>(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Landroid/net/Uri;)V

    .line 2633631
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633633
    const-class v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    iput-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633634
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->b:Landroid/net/Uri;

    .line 2633635
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V
    .locals 1

    .prologue
    .line 2633636
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;-><init>(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Landroid/net/Uri;)V

    .line 2633637
    return-void
.end method

.method private constructor <init>(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2633638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633639
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2633640
    iput-object p1, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633641
    iput-object p2, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->b:Landroid/net/Uri;

    .line 2633642
    return-void

    .line 2633643
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633644
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633645
    iget-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633646
    iget-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633647
    return-void
.end method
