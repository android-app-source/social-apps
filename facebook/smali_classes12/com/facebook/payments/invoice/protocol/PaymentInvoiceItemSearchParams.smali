.class public Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/6xh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633728
    new-instance v0, LX/Iye;

    invoke-direct {v0}, LX/Iye;-><init>()V

    sput-object v0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633717
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->a:Ljava/lang/String;

    .line 2633718
    const-class v0, LX/6xh;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xh;

    iput-object v0, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->b:LX/6xh;

    .line 2633719
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6xh;)V
    .locals 0

    .prologue
    .line 2633724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633725
    iput-object p1, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->a:Ljava/lang/String;

    .line 2633726
    iput-object p2, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->b:LX/6xh;

    .line 2633727
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633723
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633720
    iget-object v0, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633721
    iget-object v0, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->b:LX/6xh;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2633722
    return-void
.end method
