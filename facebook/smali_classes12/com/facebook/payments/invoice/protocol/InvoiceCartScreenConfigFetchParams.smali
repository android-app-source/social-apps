.class public Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633565
    new-instance v0, LX/IyY;

    invoke-direct {v0}, LX/IyY;-><init>()V

    sput-object v0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 2633566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633567
    iput-wide p1, p0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;->a:J

    .line 2633568
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2633569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633570
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;->a:J

    .line 2633571
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633572
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2633573
    iget-wide v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2633574
    return-void
.end method
