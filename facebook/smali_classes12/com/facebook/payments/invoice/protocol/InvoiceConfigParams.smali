.class public Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:LX/6xh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633613
    new-instance v0, LX/Iya;

    invoke-direct {v0}, LX/Iya;-><init>()V

    sput-object v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLX/6xh;)V
    .locals 1

    .prologue
    .line 2633614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633615
    iput-wide p1, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->a:J

    .line 2633616
    iput-object p3, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->b:LX/6xh;

    .line 2633617
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2633618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633619
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->a:J

    .line 2633620
    const-class v0, LX/6xh;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xh;

    iput-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->b:LX/6xh;

    .line 2633621
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633622
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2633623
    iget-wide v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2633624
    iget-object v0, p0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->b:LX/6xh;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2633625
    return-void
.end method
