.class public Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;
.super Lcom/facebook/payments/cart/PaymentsCartFragment;
.source ""


# instance fields
.field private final v:LX/6qh;

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/cart/model/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2632937
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;-><init>()V

    .line 2632938
    new-instance v0, LX/Iy8;

    invoke-direct {v0, p0}, LX/Iy8;-><init>(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->v:LX/6qh;

    return-void
.end method

.method public static l(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V
    .locals 3

    .prologue
    .line 2632934
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/Ixz;->a(Lcom/facebook/payments/cart/model/PaymentsCartParams;Ljava/lang/String;)V

    .line 2632935
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2632936
    return-void
.end method

.method public static m(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V
    .locals 6

    .prologue
    .line 2632911
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Iy2;->setNotifyOnChange(Z)V

    .line 2632912
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    invoke-virtual {v0}, LX/Iy2;->clear()V

    .line 2632913
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    .line 2632914
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2632915
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-nez v2, :cond_0

    .line 2632916
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2632917
    :goto_0
    move-object v1, v1

    .line 2632918
    invoke-virtual {v0, v1}, LX/Iy2;->addAll(Ljava/util/Collection;)V

    .line 2632919
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    const v1, 0x60b17fda

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2632920
    return-void

    .line 2632921
    :cond_0
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2632922
    iget-object v3, v2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    move-object v2, v3

    .line 2632923
    if-eqz v2, :cond_1

    .line 2632924
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2632925
    iget-object v4, v3, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2632926
    invoke-static {v3}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    .line 2632927
    sget-object v4, LX/IyK;->SEARCH_ADD_ITEM:LX/IyK;

    invoke-static {v4, v2, v3}, Lcom/facebook/payments/cart/model/SimpleCartItem;->a(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)LX/IyS;

    move-result-object v4

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2632928
    iput-object v5, v4, LX/IyS;->d:Ljava/lang/String;

    .line 2632929
    move-object v4, v4

    .line 2632930
    invoke-virtual {v4}, LX/IyS;->a()Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v4

    move-object v2, v4

    .line 2632931
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2632932
    :cond_1
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->w:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2632933
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 2632910
    const v0, 0x7f0306ae

    return v0
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/cart/model/CartItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2632907
    iput-object p1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->w:LX/0Px;

    .line 2632908
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->m(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    .line 2632909
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 2632905
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->m:LX/IyJ;

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    invoke-virtual {v0, p2}, LX/Iy2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    invoke-virtual {v1, v0, v2}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V

    .line 2632906
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2632939
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2632940
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 2632941
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2632942
    check-cast v2, Landroid/view/ViewGroup;

    new-instance v3, LX/Iy9;

    invoke-direct {v3, p0, v0}, LX/Iy9;-><init>(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    sget-object v4, LX/73h;->BACK_ARROW:LX/73h;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 2632943
    invoke-virtual {v1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    .line 2632944
    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2632945
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    .line 2632946
    new-instance v1, LX/IyA;

    invoke-direct {v1, p0}, LX/IyA;-><init>(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    .line 2632947
    iput-object v1, v0, Landroid/support/v7/widget/SearchView;->mOnQueryChangeListener:LX/3xK;

    .line 2632948
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2632903
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->m(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    .line 2632904
    return-void
.end method

.method public final i()LX/6qh;
    .locals 1

    .prologue
    .line 2632902
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->v:LX/6qh;

    return-object v0
.end method

.method public final j()LX/6xZ;
    .locals 1

    .prologue
    .line 2632901
    sget-object v0, LX/6xZ;->CART_ITEM_SEARCH:LX/6xZ;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 2632893
    packed-switch p1, :pswitch_data_0

    .line 2632894
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2632895
    :cond_0
    :goto_0
    return-void

    .line 2632896
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not supported RC "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2632897
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 2632898
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->t:LX/Iy1;

    invoke-virtual {p0, p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/Iy1;->a(Lcom/facebook/payments/cart/PaymentsCartFragment;Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    goto :goto_0

    .line 2632899
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 2632900
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->t:LX/Iy1;

    invoke-static {p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->b(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/Iy1;->a(Lcom/facebook/payments/cart/PaymentsCartFragment;Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x3e31b631

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632884
    invoke-super {p0, p1}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2632885
    if-eqz p1, :cond_0

    .line 2632886
    const-string v1, "extra_cart_items"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2632887
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->w:LX/0Px;

    .line 2632888
    const-string v1, "extra_search_query"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->x:Ljava/lang/String;

    .line 2632889
    :goto_0
    const v1, 0x7defcd88

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2632890
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2632891
    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->w:LX/0Px;

    .line 2632892
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2632880
    const-string v0, "extra_cart_items"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->w:LX/0Px;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2632881
    const-string v0, "extra_search_query"

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2632882
    invoke-super {p0, p1}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2632883
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632872
    invoke-super {p0, p1, p2}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2632873
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->q:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2632874
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-nez v0, :cond_0

    .line 2632875
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    invoke-interface {v0, v1}, LX/Ixz;->a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)V

    .line 2632876
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2632877
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->l(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    .line 2632878
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;->m(Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;)V

    .line 2632879
    return-void
.end method
