.class public Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;
.super Lcom/facebook/payments/cart/PaymentsCartFragment;
.source ""


# instance fields
.field public v:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final w:LX/6qh;

.field public x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/cart/model/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

.field private z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2633076
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;-><init>()V

    .line 2633077
    new-instance v0, LX/IyB;

    invoke-direct {v0, p0}, LX/IyB;-><init>(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->w:LX/6qh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object p0

    check-cast p0, LX/5fv;

    iput-object p0, p1, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->v:LX/5fv;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;LX/73T;)V
    .locals 3

    .prologue
    .line 2633068
    sget-object v0, LX/IyG;->a:[I

    .line 2633069
    iget-object v1, p1, LX/73T;->a:LX/73S;

    move-object v1, v1

    .line 2633070
    invoke-virtual {v1}, LX/73S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2633071
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not supported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2633072
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 2633073
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2633074
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->b(LX/73T;)V

    .line 2633075
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(LX/73T;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2633053
    const-string v0, "extra_user_action"

    invoke-virtual {p1, v0, v1}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2633054
    const-string v0, "view_name"

    invoke-virtual {p1, v0, v1}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2633055
    const-string v1, "edit_item_button_view"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2633056
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2633057
    iget-object v4, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2633058
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2633059
    iget-object v4, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->m:LX/IyJ;

    iget-object v5, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    invoke-virtual {v4, v0, v5}, LX/IyJ;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V

    .line 2633060
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2633061
    :cond_1
    const-string v1, "remove_item_button_view"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2633062
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    new-instance v1, LX/IyD;

    invoke-direct {v1, p0, v2}, LX/IyD;-><init>(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    .line 2633063
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->l(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    .line 2633064
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2633065
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->t:LX/Iy1;

    invoke-virtual {v0}, LX/Iy1;->a()V

    .line 2633066
    :cond_2
    return-void

    .line 2633067
    :cond_3
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not supported click action on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private b(Lcom/facebook/payments/cart/model/SimpleCartItem;)V
    .locals 3

    .prologue
    .line 2633044
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2633045
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2633046
    iget-object v2, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2633047
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2633048
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2633049
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2633050
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->l(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    .line 2633051
    return-void

    .line 2633052
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V
    .locals 3

    .prologue
    .line 2633000
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->m()V

    .line 2633001
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Iy2;->setNotifyOnChange(Z)V

    .line 2633002
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    invoke-virtual {v0}, LX/Iy2;->clear()V

    .line 2633003
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Iy2;->addAll(Ljava/util/Collection;)V

    .line 2633004
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    const v1, -0xa4a9de6

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2633005
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->p()V

    .line 2633006
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2633007
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->y:Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const v0, 0x7f0838cc

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a(Ljava/lang/String;)V

    .line 2633008
    :goto_1
    return-void

    .line 2633009
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->g:Ljava/lang/String;

    goto :goto_0

    .line 2633010
    :cond_1
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->y:Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    const v0, 0x7f0838cd

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v2, LX/IyF;

    invoke-direct {v2, p0}, LX/IyF;-><init>(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->f:Ljava/lang/String;

    goto :goto_2
.end method

.method private m()V
    .locals 3

    .prologue
    .line 2633031
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633032
    iget v2, v1, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->c:I

    move v1, v2

    .line 2633033
    if-ne v0, v1, :cond_0

    .line 2633034
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b()V

    .line 2633035
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633036
    iget-object v2, v1, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2633037
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, v2, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/String;LX/73i;)V

    .line 2633038
    :goto_0
    return-void

    .line 2633039
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a()Landroid/support/v7/widget/SearchView;

    move-result-object v0

    .line 2633040
    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 2633041
    new-instance v1, LX/IyE;

    invoke-direct {v1, p0}, LX/IyE;-><init>(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    .line 2633042
    iput-object v1, v0, Landroid/support/v7/widget/SearchView;->mOnQueryTextFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 2633043
    goto :goto_0
.end method

.method private p()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2633022
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-nez v0, :cond_0

    .line 2633023
    :goto_0
    return-void

    .line 2633024
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2633025
    iget-object v1, v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2633026
    invoke-static {v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 2633027
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    move-object v3, v0

    :goto_1
    if-ge v1, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2633028
    invoke-virtual {v0}, Lcom/facebook/payments/cart/model/SimpleCartItem;->f()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    .line 2633029
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2633030
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->y:Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

    new-instance v1, LX/73b;

    const v4, 0x7f081d42

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->v:LX/5fv;

    invoke-virtual {v5, v3}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v4, v3, v2}, LX/73b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->setSubtotal(LX/73b;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 2633021
    const v0, 0x7f0306b0

    return v0
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/cart/model/CartItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2633078
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2633014
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2633015
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 2633016
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->z:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 2633017
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2633018
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v3, LX/IyC;

    invoke-direct {v3, p0, v0}, LX/IyC;-><init>(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object v4, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v4, v4, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v4, v4, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {v4}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v0, v4}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 2633019
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->m()V

    .line 2633020
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2633012
    invoke-direct {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->p()V

    .line 2633013
    return-void
.end method

.method public final i()LX/6qh;
    .locals 1

    .prologue
    .line 2633011
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->w:LX/6qh;

    return-object v0
.end method

.method public final j()LX/6xZ;
    .locals 1

    .prologue
    .line 2632999
    sget-object v0, LX/6xZ;->VIEW_CART:LX/6xZ;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 2632984
    packed-switch p1, :pswitch_data_0

    .line 2632985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2632986
    :cond_0
    :goto_0
    return-void

    .line 2632987
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not supported RC "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2632988
    :pswitch_1
    const/4 v2, -0x1

    .line 2632989
    if-eq p2, v2, :cond_2

    .line 2632990
    :cond_1
    :goto_1
    goto :goto_0

    .line 2632991
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 2632992
    invoke-virtual {p0, p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->b(Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    goto :goto_0

    .line 2632993
    :pswitch_3
    if-ne p2, v0, :cond_0

    .line 2632994
    invoke-static {p3}, Lcom/facebook/payments/cart/PaymentsCartFragment;->b(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->b(Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    goto :goto_0

    .line 2632995
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2632996
    if-eqz v0, :cond_1

    .line 2632997
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 2632998
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4fd923c2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632978
    invoke-super {p0, p1}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2632979
    const-class v1, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->p:Landroid/content/Context;

    invoke-static {v1, p0, v2}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2632980
    if-nez p1, :cond_0

    .line 2632981
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    .line 2632982
    :goto_0
    const v1, -0x355c77d6    # -5358613.0f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2632983
    :cond_0
    const-string v1, "cart_items"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2632975
    const-string v0, "cart_items"

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2632976
    invoke-super {p0, p1}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2632977
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632969
    invoke-super {p0, p1, p2}, Lcom/facebook/payments/cart/PaymentsCartFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2632970
    const v0, 0x7f0d0aba

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->y:Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;

    .line 2632971
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    if-nez v0, :cond_0

    .line 2632972
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    invoke-interface {v0, v1}, LX/Ixz;->a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)V

    .line 2632973
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->l(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    .line 2632974
    return-void
.end method
