.class public Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Lcom/facebook/payments/cart/model/CustomItemsConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633396
    new-instance v0, LX/IyT;

    invoke-direct {v0}, LX/IyT;-><init>()V

    sput-object v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->a:Ljava/lang/String;

    .line 2633390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    .line 2633391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->c:I

    .line 2633392
    const-class v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    .line 2633393
    const-class v0, Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    .line 2633394
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->f:Landroid/os/Parcelable;

    .line 2633395
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/payments/cart/model/CustomItemsConfig;Lcom/facebook/payments/cart/model/CatalogItemsConfig;Landroid/os/Parcelable;)V
    .locals 0
    .param p4    # Lcom/facebook/payments/cart/model/CustomItemsConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/payments/cart/model/CatalogItemsConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2633397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633398
    iput-object p1, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->a:Ljava/lang/String;

    .line 2633399
    iput-object p2, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    .line 2633400
    iput p3, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->c:I

    .line 2633401
    iput-object p4, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    .line 2633402
    iput-object p5, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    .line 2633403
    iput-object p6, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->f:Landroid/os/Parcelable;

    .line 2633404
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633387
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633380
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633381
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633382
    iget v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2633383
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->d:Lcom/facebook/payments/cart/model/CustomItemsConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633384
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->e:Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633385
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->f:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633386
    return-void
.end method
