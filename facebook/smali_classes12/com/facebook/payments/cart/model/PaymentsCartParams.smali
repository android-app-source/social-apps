.class public Lcom/facebook/payments/cart/model/PaymentsCartParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/cart/model/PaymentsCartParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xg;

.field public final b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final c:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633265
    new-instance v0, LX/IyN;

    invoke-direct {v0}, LX/IyN;-><init>()V

    sput-object v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IyO;)V
    .locals 1

    .prologue
    .line 2633266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633267
    iget-object v0, p1, LX/IyO;->a:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    .line 2633268
    iget-object v0, p1, LX/IyO;->g:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2633269
    iget-object v0, p1, LX/IyO;->b:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->c:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    .line 2633270
    iget-object v0, p1, LX/IyO;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->d:Ljava/lang/String;

    .line 2633271
    iget-object v0, p1, LX/IyO;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2633272
    iget-object v0, p1, LX/IyO;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->f:Ljava/lang/String;

    .line 2633273
    iget-object v0, p1, LX/IyO;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->g:Ljava/lang/String;

    .line 2633274
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633276
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    .line 2633277
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2633278
    const-class v0, Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->c:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    .line 2633279
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->d:Ljava/lang/String;

    .line 2633280
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2633281
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->f:Ljava/lang/String;

    .line 2633282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->g:Ljava/lang/String;

    .line 2633283
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633284
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633285
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2633286
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633287
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->c:Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633288
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633289
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633290
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633291
    iget-object v0, p0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633292
    return-void
.end method
