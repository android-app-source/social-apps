.class public Lcom/facebook/payments/cart/model/SimpleCartItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/cart/model/SimpleCartItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/IyK;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final g:I

.field public final h:I

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633376
    new-instance v0, LX/IyR;

    invoke-direct {v0}, LX/IyR;-><init>()V

    sput-object v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IyS;)V
    .locals 1

    .prologue
    .line 2633365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633366
    iget-object v0, p1, LX/IyS;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    .line 2633367
    iget-object v0, p1, LX/IyS;->a:LX/IyK;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    .line 2633368
    iget-object v0, p1, LX/IyS;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    .line 2633369
    iget-object v0, p1, LX/IyS;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    .line 2633370
    iget-object v0, p1, LX/IyS;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->e:Ljava/lang/String;

    .line 2633371
    iget-object v0, p1, LX/IyS;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2633372
    iget v0, p1, LX/IyS;->g:I

    iput v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    .line 2633373
    iget v0, p1, LX/IyS;->h:I

    iput v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->h:I

    .line 2633374
    iget-object v0, p1, LX/IyS;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->i:LX/0Px;

    .line 2633375
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633339
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    .line 2633340
    const-class v0, LX/IyK;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IyK;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    .line 2633341
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    .line 2633342
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    .line 2633343
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->e:Ljava/lang/String;

    .line 2633344
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2633345
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    .line 2633346
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->h:I

    .line 2633347
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->i:LX/0Px;

    .line 2633348
    return-void
.end method

.method public static a(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)LX/IyS;
    .locals 1

    .prologue
    .line 2633364
    new-instance v0, LX/IyS;

    invoke-direct {v0, p0, p1, p2}, LX/IyS;-><init>(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633363
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 2

    .prologue
    .line 2633360
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 2633361
    iget v1, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    move v1, v1

    .line 2633362
    invoke-virtual {v0, v1}, Lcom/facebook/payments/currency/CurrencyAmount;->a(I)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2633359
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->i:LX/0Px;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633349
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633350
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2633351
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633352
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633353
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633354
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2633355
    iget v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2633356
    iget v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2633357
    iget-object v0, p0, Lcom/facebook/payments/cart/model/SimpleCartItem;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2633358
    return-void
.end method
