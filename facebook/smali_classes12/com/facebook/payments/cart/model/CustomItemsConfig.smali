.class public Lcom/facebook/payments/cart/model/CustomItemsConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/cart/model/CustomItemsConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6xN;",
            "Lcom/facebook/payments/form/model/FormFieldAttributes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633244
    new-instance v0, LX/IyM;

    invoke-direct {v0}, LX/IyM;-><init>()V

    sput-object v0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633246
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->a:Ljava/lang/String;

    .line 2633247
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->b:I

    .line 2633248
    invoke-static {p1}, LX/46R;->h(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    .line 2633249
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0P1",
            "<",
            "LX/6xN;",
            "Lcom/facebook/payments/form/model/FormFieldAttributes;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2633239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633240
    iput-object p1, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->a:Ljava/lang/String;

    .line 2633241
    iput p2, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->b:I

    .line 2633242
    iput-object p3, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    .line 2633243
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633234
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633235
    iget-object v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2633236
    iget v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2633237
    iget-object v0, p0, Lcom/facebook/payments/cart/model/CustomItemsConfig;->c:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 2633238
    return-void
.end method
