.class public Lcom/facebook/payments/cart/model/QuantityConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/cart/model/QuantityConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/IyQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2633308
    new-instance v0, LX/IyP;

    invoke-direct {v0}, LX/IyP;-><init>()V

    sput-object v0, Lcom/facebook/payments/cart/model/QuantityConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2633309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633310
    const-class v0, LX/IyQ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IyQ;

    iput-object v0, p0, Lcom/facebook/payments/cart/model/QuantityConfig;->a:LX/IyQ;

    .line 2633311
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2633305
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2633306
    iget-object v0, p0, Lcom/facebook/payments/cart/model/QuantityConfig;->a:LX/IyQ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2633307
    return-void
.end method
