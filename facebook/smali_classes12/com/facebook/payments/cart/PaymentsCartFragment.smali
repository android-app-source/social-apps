.class public abstract Lcom/facebook/payments/cart/PaymentsCartFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public final i:LX/Iy3;

.field public j:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Iy2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Ixz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/IyJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/content/Context;

.field public q:Landroid/widget/ListView;

.field public r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

.field public s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

.field public t:LX/Iy1;

.field public u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2632740
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 2632741
    new-instance v0, LX/Iy3;

    invoke-direct {v0, p0}, LX/Iy3;-><init>(Lcom/facebook/payments/cart/PaymentsCartFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->i:LX/Iy3;

    return-void
.end method

.method public static a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2632797
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2632798
    const-string v1, "payments_cart_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2632799
    return-object v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v0, p1

    check-cast v0, Lcom/facebook/payments/cart/PaymentsCartFragment;

    invoke-static {p0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v1

    check-cast v1, LX/6wr;

    new-instance v4, LX/Iy2;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    new-instance p1, LX/Iy7;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v3

    check-cast v3, LX/5fv;

    invoke-static {p0}, LX/IyJ;->b(LX/0QB;)LX/IyJ;

    move-result-object v5

    check-cast v5, LX/IyJ;

    invoke-direct {p1, v3, v5}, LX/Iy7;-><init>(LX/5fv;LX/IyJ;)V

    move-object v3, p1

    check-cast v3, LX/Iy7;

    invoke-direct {v4, v2, v3}, LX/Iy2;-><init>(Landroid/content/Context;LX/Iy7;)V

    move-object v2, v4

    check-cast v2, LX/Iy2;

    invoke-static {p0}, LX/IyX;->a(LX/0QB;)LX/IyX;

    move-result-object v3

    check-cast v3, LX/Ixz;

    invoke-static {p0}, LX/IyJ;->b(LX/0QB;)LX/IyJ;

    move-result-object v4

    check-cast v4, LX/IyJ;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p0

    check-cast p0, LX/6xb;

    iput-object v1, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->j:LX/6wr;

    iput-object v2, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    iput-object v3, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iput-object v4, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->m:LX/IyJ;

    iput-object v5, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->o:LX/6xb;

    return-void
.end method

.method public static b(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;
    .locals 5

    .prologue
    .line 2632786
    const-string v0, "extra_parcelable"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2632787
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2632788
    iget-object v2, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 2632789
    iget-object v3, v2, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2632790
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "extra_numeric"

    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 2632791
    sget-object v2, LX/IyK;->CART_ITEM:LX/IyK;

    .line 2632792
    iget-object v3, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2632793
    invoke-static {v2, v3, v1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->a(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)LX/IyS;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/IyS;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;)LX/IyS;

    move-result-object v0

    const-string v1, "extra_quantity"

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2632794
    iput v1, v0, LX/IyS;->g:I

    .line 2632795
    move-object v0, v0

    .line 2632796
    invoke-virtual {v0}, LX/IyS;->a()Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2632784
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->o:LX/6xb;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v1, v1, Lcom/facebook/payments/cart/model/PaymentsCartParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->j()LX/6xZ;

    move-result-object v2

    const-string v3, "payflows_back_click"

    invoke-virtual {v0, v1, v2, v3}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 2632785
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a()I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end method

.method public final a(Landroid/content/Intent;)Lcom/facebook/payments/cart/model/SimpleCartItem;
    .locals 5

    .prologue
    .line 2632773
    const-string v0, "extra_parcelable"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2632774
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2632775
    iget-object v3, v2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2632776
    new-instance v3, Ljava/math/BigDecimal;

    const-string v4, "extra_numeric"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 2632777
    sget-object v2, LX/IyK;->CART_CUSTOM_ITEM:LX/IyK;

    const-string v3, "extra_title"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->a(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)LX/IyS;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/IyS;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;)LX/IyS;

    move-result-object v0

    const-string v1, "extra_quantity"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2632778
    iput v1, v0, LX/IyS;->g:I

    .line 2632779
    move-object v0, v0

    .line 2632780
    const-string v1, "extra_subtitle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2632781
    iput-object v1, v0, LX/IyS;->e:Ljava/lang/String;

    .line 2632782
    move-object v0, v0

    .line 2632783
    invoke-virtual {v0}, LX/IyS;->a()Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/cart/model/CartItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b()V
.end method

.method public abstract h()V
.end method

.method public abstract i()LX/6qh;
.end method

.method public abstract j()LX/6xZ;
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2632772
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const v0, 0x7f0838ce

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x1b30b121

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2632763
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2632764
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0103f1

    const v3, 0x7f0e0326

    invoke-static {v0, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->p:Landroid/content/Context;

    .line 2632765
    const-class v0, Lcom/facebook/payments/cart/PaymentsCartFragment;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->p:Landroid/content/Context;

    invoke-static {v0, p0, v2}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2632766
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2632767
    const-string v2, "payments_cart_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2632768
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->o:LX/6xb;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, v2, Lcom/facebook/payments/cart/model/PaymentsCartParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v3, v3, Lcom/facebook/payments/cart/model/PaymentsCartParams;->a:LX/6xg;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->j()LX/6xZ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 2632769
    if-eqz p1, :cond_0

    .line 2632770
    const-string v0, "cart_screen_config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2632771
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x59f1e7a4

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x477f04c0    # 65284.75f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632760
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->p:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2632761
    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v2, v2, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    iget-object v3, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v3, v3, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v1, v2, v3}, LX/6wr;->a(Landroid/view/View;LX/0am;Z)V

    .line 2632762
    const/16 v2, 0x2b

    const v3, -0x8eb34a3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3e36b034

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632757
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 2632758
    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->i:LX/Iy3;

    invoke-interface {v1, v2}, LX/Ixz;->b(LX/Iy3;)V

    .line 2632759
    const/16 v1, 0x2b

    const v2, 0x519214a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2632754
    const-string v0, "cart_screen_config"

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->s:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2632755
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2632756
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632742
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2632743
    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->b()V

    .line 2632744
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2632745
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->q:Landroid/widget/ListView;

    .line 2632746
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->q:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2632747
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->k:LX/Iy2;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->i()LX/6qh;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2632748
    iget-object p1, v0, LX/Iy2;->a:LX/Iy7;

    .line 2632749
    iget-object v0, p1, LX/Iy7;->b:LX/IyJ;

    invoke-virtual {v0, v1, v2}, LX/IyJ;->a(LX/6qh;Lcom/facebook/payments/cart/model/PaymentsCartParams;)V

    .line 2632750
    iput-object v1, p1, LX/Iy7;->c:LX/6qh;

    .line 2632751
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->i:LX/Iy3;

    invoke-interface {v0, v1}, LX/Ixz;->a(LX/Iy3;)V

    .line 2632752
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->m:LX/IyJ;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->i()LX/6qh;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartFragment;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    invoke-virtual {v0, v1, v2}, LX/IyJ;->a(LX/6qh;Lcom/facebook/payments/cart/model/PaymentsCartParams;)V

    .line 2632753
    return-void
.end method
