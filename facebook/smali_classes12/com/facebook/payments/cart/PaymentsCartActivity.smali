.class public Lcom/facebook/payments/cart/PaymentsCartActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:LX/Iy1;

.field private r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

.field private s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

.field private t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2632667
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2632668
    new-instance v0, LX/Iy1;

    invoke-direct {v0, p0}, LX/Iy1;-><init>(Lcom/facebook/payments/cart/PaymentsCartActivity;)V

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->q:LX/Iy1;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/cart/model/PaymentsCartParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2632717
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/cart/PaymentsCartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2632718
    const-string v1, "payments_cart_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2632719
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2632720
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/cart/PaymentsCartActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->p:LX/6wr;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/cart/PaymentsCartActivity;)V
    .locals 3

    .prologue
    .line 2632714
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2632715
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2632716
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/cart/PaymentsCartActivity;Lcom/facebook/payments/cart/PaymentsCartFragment;Lcom/facebook/payments/cart/model/SimpleCartItem;)V
    .locals 1
    .param p1    # Lcom/facebook/payments/cart/PaymentsCartFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632707
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632708
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 2632709
    :cond_0
    if-eqz p2, :cond_1

    .line 2632710
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    .line 2632711
    iget-object p0, v0, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->x:Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2632712
    invoke-static {v0}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;->l(Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;)V

    .line 2632713
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2632702
    instance-of v0, p1, Lcom/facebook/payments/cart/PaymentsCartFragment;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2632703
    check-cast v0, Lcom/facebook/payments/cart/PaymentsCartFragment;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->q:LX/Iy1;

    .line 2632704
    iput-object v1, v0, Lcom/facebook/payments/cart/PaymentsCartFragment;->t:LX/Iy1;

    .line 2632705
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2632706
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632695
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2632696
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/cart/PaymentsCartActivity;->setContentView(I)V

    .line 2632697
    if-nez p1, :cond_0

    .line 2632698
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2632699
    invoke-static {p0}, Lcom/facebook/payments/cart/PaymentsCartActivity;->a$redex0(Lcom/facebook/payments/cart/PaymentsCartActivity;)V

    .line 2632700
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 2632701
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632678
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 2632679
    invoke-static {p0, p0}, Lcom/facebook/payments/cart/PaymentsCartActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2632680
    invoke-virtual {p0}, Lcom/facebook/payments/cart/PaymentsCartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "payments_cart_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2632681
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    if-nez v0, :cond_0

    .line 2632682
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2632683
    new-instance v1, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    invoke-direct {v1}, Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;-><init>()V

    .line 2632684
    invoke-static {v0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2632685
    move-object v0, v1

    .line 2632686
    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    .line 2632687
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    if-nez v0, :cond_1

    .line 2632688
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    .line 2632689
    new-instance v1, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    invoke-direct {v1}, Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;-><init>()V

    .line 2632690
    invoke-static {v0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2632691
    move-object v0, v1

    .line 2632692
    iput-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    .line 2632693
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v1, v1, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 2632694
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 2632675
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2632676
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->r:Lcom/facebook/payments/cart/model/PaymentsCartParams;

    iget-object v0, v0, Lcom/facebook/payments/cart/model/PaymentsCartParams;->e:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 2632677
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2632669
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2632670
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->s:Lcom/facebook/payments/cart/PaymentsSearchCartItemFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->S_()Z

    .line 2632671
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2632672
    return-void

    .line 2632673
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632674
    iget-object v0, p0, Lcom/facebook/payments/cart/PaymentsCartActivity;->t:Lcom/facebook/payments/cart/PaymentsShowCartItemsFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/cart/PaymentsCartFragment;->S_()Z

    goto :goto_0
.end method
