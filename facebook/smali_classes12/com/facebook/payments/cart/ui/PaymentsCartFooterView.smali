.class public Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;
.super LX/6E7;
.source ""


# instance fields
.field private a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

.field private b:Lcom/facebook/payments/ui/PriceTableRowView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2633461
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 2633462
    invoke-direct {p0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a()V

    .line 2633463
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2633458
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2633459
    invoke-direct {p0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a()V

    .line 2633460
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2633455
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2633456
    invoke-direct {p0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a()V

    .line 2633457
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2633447
    const v0, 0x7f030f16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2633448
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->setOrientation(I)V

    .line 2633449
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2633450
    const v0, 0x7f0d0551

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    .line 2633451
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b()V

    .line 2633452
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->f()V

    .line 2633453
    const v0, 0x7f0d24ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PriceTableRowView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->b:Lcom/facebook/payments/ui/PriceTableRowView;

    .line 2633454
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2633443
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 2633444
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 2633445
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2633446
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2633439
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setEnabled(Z)V

    .line 2633440
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 2633441
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->a:Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-virtual {v0, p2}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2633442
    return-void
.end method

.method public setSubtotal(LX/73b;)V
    .locals 1

    .prologue
    .line 2633437
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/PaymentsCartFooterView;->b:Lcom/facebook/payments/ui/PriceTableRowView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PriceTableRowView;->setRowData(LX/73b;)V

    .line 2633438
    return-void
.end method
