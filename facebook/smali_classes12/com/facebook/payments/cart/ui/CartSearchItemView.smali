.class public Lcom/facebook/payments/cart/ui/CartSearchItemView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/ImageView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2633405
    const-class v0, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2633406
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 2633407
    const-class v0, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    invoke-static {v0, p0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2633408
    const v0, 0x7f03024b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2633409
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-static {v1, p1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2633410
    const v0, 0x7f0d08d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->c:Landroid/widget/ImageView;

    .line 2633411
    const v0, 0x7f0d08d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2633412
    const v0, 0x7f0d08d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->e:Landroid/widget/TextView;

    .line 2633413
    const v0, 0x7f0d08d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->f:Landroid/widget/TextView;

    .line 2633414
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object p0

    check-cast p0, LX/5fv;

    iput-object p0, p1, Lcom/facebook/payments/cart/ui/CartSearchItemView;->a:LX/5fv;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/cart/model/SimpleCartItem;Ljava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2633415
    if-nez p2, :cond_1

    .line 2633416
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->e:Landroid/widget/TextView;

    .line 2633417
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2633418
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2633419
    :goto_0
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    move-object v0, v0

    .line 2633420
    sget-object v2, LX/IyK;->SEARCH_ADD_ITEM:LX/IyK;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 2633421
    :goto_1
    iget-object v4, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2633422
    iget-object v4, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_4

    move v2, v3

    :goto_3
    invoke-virtual {v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2633423
    iget-object v2, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2633424
    if-nez v0, :cond_0

    .line 2633425
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00aa

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2633426
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->a:LX/5fv;

    .line 2633427
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 2633428
    invoke-virtual {v1, v2}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2633429
    invoke-virtual {p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2633430
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/payments/cart/ui/CartSearchItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2633431
    :cond_0
    return-void

    .line 2633432
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/cart/ui/CartSearchItemView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2633433
    goto :goto_1

    :cond_3
    move v2, v3

    .line 2633434
    goto :goto_2

    :cond_4
    move v2, v1

    .line 2633435
    goto :goto_3

    :cond_5
    move v3, v1

    .line 2633436
    goto :goto_4
.end method
