.class public Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/CheckoutParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

.field public final b:Lcom/facebook/payments/checkout/CheckoutCommonParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2643032
    new-instance v0, LX/J3Y;

    invoke-direct {v0}, LX/J3Y;-><init>()V

    sput-object v0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2643033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643034
    const-class v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iput-object v0, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2643035
    const-class v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    iput-object v0, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->b:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 2643036
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/sample/PaymentsFlowSampleData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 0

    .prologue
    .line 2643037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643038
    iput-object p1, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2643039
    iput-object p2, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->b:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 2643040
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 2643041
    iget-object v0, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->b:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 2

    .prologue
    .line 2643042
    new-instance v0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;

    iget-object v1, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-direct {v0, v1, p1}, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2643043
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2643044
    iget-object v0, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2643045
    iget-object v0, p0, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;->b:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2643046
    return-void
.end method
