.class public Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/J31;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Iyi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Iyo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Landroid/widget/ListView;

.field private m:Landroid/content/Context;

.field public n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

.field private o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/J3T;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2642388
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "Pikachu Image"

    const-string v2, "http://cdn8.staztic.com/app/a/740/740596/cute-pikachu-wallpaper-849170-1-s-307x512.jpg"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Charmander Image"

    const-string v2, "http://vignette1.wikia.nocookie.net/pokemon/images/9/96/004Charmander_OS_anime.png"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Bulbasaur Image"

    const-string v2, "http://pm1.narvii.com/5724/af7a68ce5328256950f402f0c77e3ed850c2a21a_hq.jpg"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a:LX/0P1;

    .line 2642389
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "<empty>"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "The quick brown fox jumps over the lazy dog."

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog."

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2642938
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2642939
    return-void
.end method

.method private static P(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2642927
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    if-nez v0, :cond_0

    .line 2642928
    sget-object v0, LX/6xK;->SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;

    const-string v1, "Custom Shipping Method"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v0

    new-instance v1, Lcom/facebook/payments/form/model/ShippingMethodFormData;

    const-string v2, "USD"

    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/payments/form/model/ShippingMethodFormData;-><init>(Ljava/util/Currency;)V

    .line 2642929
    iput-object v1, v0, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2642930
    move-object v0, v0

    .line 2642931
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    .line 2642932
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v13

    .line 2642933
    new-instance v0, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    const-string v1, "Free Shipping"

    const-string v2, "Free Shipping"

    new-instance v3, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v6, "USD"

    const-wide/16 v8, 0x0

    invoke-direct {v3, v6, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    new-instance v6, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    const-string v7, "FedEx Priority"

    const-string v8, "FedEx Priority"

    new-instance v9, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "USD"

    const-wide/16 v2, 0x7d0

    invoke-direct {v9, v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    move v10, v4

    move v11, v4

    invoke-direct/range {v6 .. v11}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    new-instance v7, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    const-string v8, "UPS Same Day"

    const-string v9, "UPS Same Day"

    new-instance v10, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "USD"

    const-wide/16 v2, 0xbb8

    invoke-direct {v10, v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    move v11, v4

    move v12, v5

    invoke-direct/range {v7 .. v12}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    new-instance v11, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;

    const-string v1, "Add Custom Shipping Address"

    const/16 v2, 0x64

    invoke-direct {v11, v1, v13, v2}, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;-><init>(Ljava/lang/String;Landroid/content/Intent;I)V

    new-instance v12, Lcom/facebook/payments/selector/model/DividerSelectorRow;

    invoke-direct {v12}, Lcom/facebook/payments/selector/model/DividerSelectorRow;-><init>()V

    new-instance v13, Lcom/facebook/payments/selector/model/FooterSelectorRow;

    const-string v1, "Changes saved here will NEVER be saved, please start using pen and paper :P"

    const-string v2, "Shop Settings"

    const-string v3, "https://m.facebook.com"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v13, v1, v2, v3}, Lcom/facebook/payments/selector/model/FooterSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v8, v0

    move-object v9, v6

    move-object v10, v7

    invoke-static/range {v8 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2642934
    new-instance v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    const-string v2, "Title of Selector screen"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    const-string v4, "shipping_option"

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 2642935
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    invoke-static {v0, v1}, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642936
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, v5, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2642937
    return-void
.end method

.method public static a(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2642921
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 2642922
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v1

    new-array v1, v1, [B

    .line 2642923
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    .line 2642924
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 2642925
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2642926
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to read json config."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;LX/J32;)V
    .locals 14

    .prologue
    .line 2642717
    sget-object v0, LX/J3K;->a:[I

    invoke-virtual {p1}, LX/J32;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2642718
    :goto_0
    return-void

    .line 2642719
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "sample_checkout_configuration.json"

    invoke-static {p0, v1}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2642720
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642721
    goto :goto_0

    .line 2642722
    :pswitch_1
    sget-object v2, LX/6xg;->NMOR_PAGES_COMMERCE:LX/6xg;

    sget-object v3, LX/6xY;->INVOICE:LX/6xY;

    invoke-static {v3}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v3

    invoke-virtual {v3}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v3

    new-instance v4, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;

    const-wide/16 v6, 0x6f

    invoke-direct {v4, v6, v7}, Lcom/facebook/payments/invoice/protocol/InvoiceCartScreenConfigFetchParams;-><init>(J)V

    .line 2642723
    new-instance v5, LX/IyO;

    invoke-direct {v5, v2, v3, v4}, LX/IyO;-><init>(LX/6xg;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/cart/model/CartScreenConfigFetchParams;)V

    move-object v2, v5

    .line 2642724
    const-string v3, "Search To Add Products"

    .line 2642725
    iput-object v3, v2, LX/IyO;->c:Ljava/lang/String;

    .line 2642726
    move-object v2, v2

    .line 2642727
    new-instance v3, Lcom/facebook/payments/cart/model/PaymentsCartParams;

    invoke-direct {v3, v2}, Lcom/facebook/payments/cart/model/PaymentsCartParams;-><init>(LX/IyO;)V

    move-object v2, v3

    .line 2642728
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/facebook/payments/cart/PaymentsCartActivity;->a(Landroid/content/Context;Lcom/facebook/payments/cart/model/PaymentsCartParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v2

    .line 2642729
    iget-object v3, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642730
    goto :goto_0

    .line 2642731
    :pswitch_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/6xK;->NOTE_FORM_CONTROLLER:LX/6xK;

    const-string v2, "Add Note"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v1

    invoke-virtual {v1}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642732
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642733
    goto :goto_0

    .line 2642734
    :pswitch_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/6xK;->SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;

    const-string v2, "Custom Shipping Method"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v1

    new-instance v2, Lcom/facebook/payments/form/model/ShippingMethodFormData;

    const-string v3, "USD"

    invoke-static {v3}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/payments/form/model/ShippingMethodFormData;-><init>(Ljava/util/Currency;)V

    .line 2642735
    iput-object v2, v1, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2642736
    move-object v1, v1

    .line 2642737
    invoke-virtual {v1}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642738
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642739
    goto/16 :goto_0

    .line 2642740
    :pswitch_4
    invoke-static {}, Lcom/facebook/payments/form/model/ItemFormData;->newBuilder()LX/6xR;

    move-result-object v0

    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->aa()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v1

    .line 2642741
    iput-object v1, v0, LX/6xR;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 2642742
    move-object v0, v0

    .line 2642743
    sget-object v1, LX/6xN;->TITLE:LX/6xN;

    const-string v2, "Enter Title"

    sget-object v3, LX/6xe;->REQUIRED:LX/6xe;

    sget-object v4, LX/6xO;->TEXT:LX/6xO;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v1

    const/16 v2, 0x14

    .line 2642744
    iput v2, v1, LX/6xM;->f:I

    .line 2642745
    move-object v1, v1

    .line 2642746
    invoke-virtual {v1}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v1

    move-object v1, v1

    .line 2642747
    iput-object v1, v0, LX/6xR;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 2642748
    move-object v0, v0

    .line 2642749
    const/16 v1, 0xa

    .line 2642750
    iput v1, v0, LX/6xR;->a:I

    .line 2642751
    move-object v0, v0

    .line 2642752
    invoke-virtual {v0}, LX/6xR;->a()Lcom/facebook/payments/form/model/ItemFormData;

    move-result-object v0

    .line 2642753
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    const-string v3, "Custom Item"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v2

    .line 2642754
    iput-object v0, v2, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2642755
    move-object v0, v2

    .line 2642756
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642757
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642758
    goto/16 :goto_0

    .line 2642759
    :pswitch_5
    const-string v0, "Title"

    invoke-static {v0}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a(Ljava/lang/String;)LX/73R;

    move-result-object v0

    const-string v1, "http://cdn8.staztic.com/app/a/740/740596/cute-pikachu-wallpaper-849170-1-s-307x512.jpg"

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2642760
    iput-object v1, v0, LX/73R;->c:LX/0Px;

    .line 2642761
    move-object v0, v0

    .line 2642762
    const-string v1, "Sub Title"

    .line 2642763
    iput-object v1, v0, LX/73R;->d:Ljava/lang/String;

    .line 2642764
    move-object v0, v0

    .line 2642765
    const-string v1, "Sub Sub Title"

    .line 2642766
    iput-object v1, v0, LX/73R;->e:Ljava/lang/String;

    .line 2642767
    move-object v0, v0

    .line 2642768
    invoke-virtual {v0}, LX/73R;->a()Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-result-object v0

    .line 2642769
    invoke-static {}, Lcom/facebook/payments/form/model/ItemFormData;->newBuilder()LX/6xR;

    move-result-object v1

    .line 2642770
    iput-object v0, v1, LX/6xR;->d:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    .line 2642771
    move-object v0, v1

    .line 2642772
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->aa()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v1

    .line 2642773
    iput-object v1, v0, LX/6xR;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 2642774
    move-object v0, v0

    .line 2642775
    const/4 v1, 0x1

    .line 2642776
    iput v1, v0, LX/6xR;->a:I

    .line 2642777
    move-object v0, v0

    .line 2642778
    invoke-virtual {v0}, LX/6xR;->a()Lcom/facebook/payments/form/model/ItemFormData;

    move-result-object v0

    .line 2642779
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    const-string v3, "Custom Item"

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/payments/form/model/PaymentsFormParams;->a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;

    move-result-object v2

    .line 2642780
    iput-object v0, v2, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 2642781
    move-object v0, v2

    .line 2642782
    invoke-virtual {v0}, LX/6xW;->a()Lcom/facebook/payments/form/model/PaymentsFormParams;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642783
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642784
    goto/16 :goto_0

    .line 2642785
    :pswitch_6
    invoke-static {p0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->P(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    goto/16 :goto_0

    .line 2642786
    :pswitch_7
    :try_start_0
    const-string v2, "sample_invoice_configuration.json"

    invoke-static {p0, v2}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2642787
    iget-object v3, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->i:LX/Iyo;

    iget-object v4, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->j:LX/0lC;

    invoke-virtual {v4, v2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/Iyo;->a(LX/0lF;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    move-result-object v4

    .line 2642788
    iget-object v2, v4, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2642789
    iget-object v3, v2, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->f:Landroid/os/Parcelable;

    move-object v2, v3

    .line 2642790
    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 2642791
    iget-object v3, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    invoke-static {v3}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v3

    const-class v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    invoke-virtual {v3, v5}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v3

    invoke-virtual {v3}, LX/0wv;->b()LX/0Px;

    move-result-object v5

    .line 2642792
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2642793
    sget-object v3, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v3}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v3

    invoke-virtual {v3}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v3

    invoke-virtual {v3}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v3

    .line 2642794
    const-string v6, "Cute Cats"

    invoke-static {v6}, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a(Ljava/lang/String;)LX/6rU;

    move-result-object v6

    const-string v7, "This is subtitle"

    .line 2642795
    iput-object v7, v6, LX/6rU;->c:Ljava/lang/String;

    .line 2642796
    move-object v6, v6

    .line 2642797
    const-string v7, "This is subsubtitle"

    .line 2642798
    iput-object v7, v6, LX/6rU;->c:Ljava/lang/String;

    .line 2642799
    move-object v6, v6

    .line 2642800
    const-string v7, "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Indian_Cat_pic.jpg/220px-Indian_Cat_pic.jpg"

    .line 2642801
    iput-object v7, v6, LX/6rU;->e:Ljava/lang/String;

    .line 2642802
    move-object v6, v6

    .line 2642803
    invoke-virtual {v6}, LX/6rU;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    move-result-object v6

    .line 2642804
    const-string v7, "Long Long Long Long Long Long title Lovely whale"

    invoke-static {v7}, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a(Ljava/lang/String;)LX/6rU;

    move-result-object v7

    const-string v8, "This is subtitle"

    .line 2642805
    iput-object v8, v7, LX/6rU;->c:Ljava/lang/String;

    .line 2642806
    move-object v7, v7

    .line 2642807
    const-string v8, "This is subsubtitle"

    .line 2642808
    iput-object v8, v7, LX/6rU;->c:Ljava/lang/String;

    .line 2642809
    move-object v7, v7

    .line 2642810
    const-string v8, "http://media-channel.nationalgeographic.com/media/uploads/photos/content/photo/2013/01/30/KillerWhale_01_BuiltForTheKillV.jpg"

    .line 2642811
    iput-object v8, v7, LX/6rU;->e:Ljava/lang/String;

    .line 2642812
    move-object v7, v7

    .line 2642813
    invoke-virtual {v7}, LX/6rU;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    move-result-object v7

    .line 2642814
    const-string v8, "Subtotal"

    new-instance v9, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v10, "USD"

    const-wide/16 v12, 0xbe

    invoke-direct {v9, v10, v12, v13}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-static {v8, v9}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v8

    .line 2642815
    const-string v9, "Tax"

    new-instance v10, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v11, "USD"

    const-wide/16 v12, 0x32

    invoke-direct {v10, v11, v12, v13}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-static {v9, v10}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v9

    .line 2642816
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2642817
    invoke-virtual {v10, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642818
    invoke-virtual {v10, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642819
    iget-object v8, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    invoke-static {v8}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2642820
    iget-object v8, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    invoke-virtual {v10, v8}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2642821
    :cond_0
    sget-object v8, LX/6qw;->INVOICE_CREATION:LX/6qw;

    sget-object v9, LX/6xg;->MOR_NONE:LX/6xg;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    invoke-static {v2}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/0Px;)LX/0Rf;

    move-result-object v2

    invoke-static {v8, v9, v2, v3}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v2

    sget-object v3, LX/6re;->FIXED_AMOUNT:LX/6re;

    .line 2642822
    iput-object v3, v2, LX/6qZ;->e:LX/6re;

    .line 2642823
    move-object v2, v2

    .line 2642824
    iget-object v3, v4, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;->a:Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    .line 2642825
    iget-object v4, v3, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2642826
    invoke-static {v3}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    .line 2642827
    iput-object v3, v2, LX/6qZ;->f:Ljava/util/Currency;

    .line 2642828
    move-object v2, v2

    .line 2642829
    invoke-static {v6, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2642830
    iput-object v3, v2, LX/6qZ;->i:LX/0Px;

    .line 2642831
    move-object v2, v2

    .line 2642832
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2642833
    iput-object v3, v2, LX/6qZ;->h:LX/0Px;

    .line 2642834
    move-object v2, v2

    .line 2642835
    iput-object v5, v2, LX/6qZ;->w:LX/0Px;

    .line 2642836
    move-object v2, v2

    .line 2642837
    sget-object v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->b:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 2642838
    iput-object v3, v2, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 2642839
    move-object v2, v2

    .line 2642840
    invoke-virtual {v2}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    .line 2642841
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2642842
    iget-object v3, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2642843
    :goto_2
    goto/16 :goto_0

    .line 2642844
    :pswitch_8
    new-instance v0, LX/4IA;

    invoke-direct {v0}, LX/4IA;-><init>()V

    const-string v1, "1005695256167261"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2642845
    const-string v2, "id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642846
    move-object v0, v0

    .line 2642847
    const-string v1, "2"

    .line 2642848
    const-string v2, "quantity"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642849
    move-object v0, v0

    .line 2642850
    const-string v1, "Pikachu"

    .line 2642851
    const-string v2, "title"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642852
    move-object v0, v0

    .line 2642853
    new-instance v1, LX/4I9;

    invoke-direct {v1}, LX/4I9;-><init>()V

    const-string v2, "1.23"

    .line 2642854
    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642855
    move-object v1, v1

    .line 2642856
    const-string v2, "USD"

    .line 2642857
    const-string v3, "currency"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642858
    move-object v1, v1

    .line 2642859
    const-string v2, "currency_amount"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2642860
    move-object v1, v0

    .line 2642861
    new-instance v0, LX/4IB;

    invoke-direct {v0}, LX/4IB;-><init>()V

    const-string v2, "1125483910839940"

    const-string v3, "1125483610839970"

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2642862
    const-string v3, "shipping_options"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2642863
    move-object v2, v0

    .line 2642864
    new-instance v0, LX/4I8;

    invoke-direct {v0}, LX/4I8;-><init>()V

    const-string v3, "PAGES_COMMERCE"

    .line 2642865
    const-string v4, "client"

    invoke-virtual {v0, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642866
    move-object v0, v0

    .line 2642867
    const-string v3, "219546581433682"

    .line 2642868
    const-string v4, "actor_id"

    invoke-virtual {v0, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642869
    move-object v0, v0

    .line 2642870
    const-string v3, "219546581433682"

    .line 2642871
    const-string v4, "seller_id"

    invoke-virtual {v0, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642872
    move-object v3, v0

    .line 2642873
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2642874
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2642875
    const-string v4, "buyer_id"

    invoke-virtual {v3, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642876
    move-object v0, v3

    .line 2642877
    const-string v3, "Thanks for you purchase!"

    .line 2642878
    const-string v4, "notes"

    invoke-virtual {v0, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642879
    move-object v0, v0

    .line 2642880
    const-string v3, "selected_options"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2642881
    move-object v0, v0

    .line 2642882
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2642883
    const-string v2, "items"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2642884
    move-object v0, v0

    .line 2642885
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->h:LX/Iyi;

    .line 2642886
    new-instance v2, LX/Iyj;

    invoke-direct {v2}, LX/Iyj;-><init>()V

    move-object v2, v2

    .line 2642887
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2642888
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2642889
    iget-object v3, v1, LX/Iyi;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2642890
    new-instance v3, LX/Iyh;

    invoke-direct {v3, v1}, LX/Iyh;-><init>(LX/Iyi;)V

    iget-object v4, v1, LX/Iyi;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2642891
    new-instance v1, LX/J3J;

    invoke-direct {v1, p0}, LX/J3J;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    iget-object v2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2642892
    goto/16 :goto_0

    .line 2642893
    :pswitch_9
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2642894
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642895
    goto/16 :goto_0

    .line 2642896
    :pswitch_a
    new-instance v0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    const-string v1, "id"

    const-string v2, "abc@fb.com"

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a(Ljava/lang/String;Ljava/lang/String;)LX/6zS;

    move-result-object v1

    const-string v2, "Blah blah blah"

    .line 2642897
    iput-object v2, v1, LX/6zS;->d:Ljava/lang/String;

    .line 2642898
    move-object v1, v1

    .line 2642899
    const-string v2, "facebook.com"

    .line 2642900
    iput-object v2, v1, LX/6zS;->e:Ljava/lang/String;

    .line 2642901
    move-object v1, v1

    .line 2642902
    sget-object v2, LX/6zT;->MIB:LX/6zT;

    .line 2642903
    iput-object v2, v1, LX/6zS;->c:LX/6zT;

    .line 2642904
    move-object v1, v1

    .line 2642905
    invoke-virtual {v1}, LX/6zS;->a()Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;-><init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;)V

    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6v0;

    move-result-object v0

    invoke-virtual {v0}, LX/6v0;->a()Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    move-result-object v0

    .line 2642906
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642907
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642908
    goto/16 :goto_0

    .line 2642909
    :pswitch_b
    invoke-static {}, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->newBuilder()LX/J2t;

    move-result-object v0

    sget-object v1, LX/6xh;->MOCK:LX/6xh;

    .line 2642910
    iput-object v1, v0, LX/J2t;->c:LX/6xh;

    .line 2642911
    move-object v0, v0

    .line 2642912
    const-string v1, "242242"

    .line 2642913
    iput-object v1, v0, LX/J2t;->d:Ljava/lang/String;

    .line 2642914
    move-object v0, v0

    .line 2642915
    invoke-virtual {v0}, LX/J2t;->a()Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;)LX/J2p;

    move-result-object v0

    invoke-virtual {v0}, LX/J2p;->a()Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    move-result-object v0

    .line 2642916
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v1, v2, v0}, Lcom/facebook/payments/receipt/PaymentsReceiptActivity;->a(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/receipt/model/ReceiptCommonParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2642917
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642918
    goto/16 :goto_0

    .line 2642919
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2642920
    :catch_0
    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;LX/J3T;)V
    .locals 6

    .prologue
    .line 2642512
    sget-object v0, LX/J3K;->b:[I

    invoke-virtual {p1}, LX/J3T;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2642513
    :goto_0
    return-void

    .line 2642514
    :pswitch_0
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 2642515
    :goto_1
    iput-boolean v0, v1, LX/J34;->a:Z

    .line 2642516
    move-object v0, v1

    .line 2642517
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642518
    goto :goto_0

    .line 2642519
    :pswitch_1
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 2642520
    :goto_2
    iput-boolean v0, v1, LX/J34;->b:Z

    .line 2642521
    move-object v0, v1

    .line 2642522
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642523
    goto :goto_0

    .line 2642524
    :pswitch_2
    new-instance v1, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2642525
    const/4 v0, 0x1

    .line 2642526
    iput-boolean v0, v1, LX/34b;->d:Z

    .line 2642527
    sget-object v0, LX/J3T;->ITEM_IMAGE:LX/J3T;

    invoke-virtual {v0}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 2642528
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2642529
    invoke-virtual {v1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    .line 2642530
    new-instance p1, LX/J3O;

    invoke-direct {p1, p0, v0}, LX/J3O;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Ljava/lang/String;)V

    invoke-virtual {v3, p1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 2642531
    :cond_0
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2642532
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2642533
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2642534
    goto :goto_0

    .line 2642535
    :pswitch_3
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642536
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642537
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642538
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3P;

    invoke-direct {v3, p0, v0}, LX/J3P;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642539
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642540
    goto/16 :goto_0

    .line 2642541
    :pswitch_4
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642542
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642543
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642544
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3Q;

    invoke-direct {v3, p0, v0}, LX/J3Q;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642545
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642546
    goto/16 :goto_0

    .line 2642547
    :pswitch_5
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642548
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642549
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642550
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3R;

    invoke-direct {v3, p0, v0}, LX/J3R;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642551
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642552
    goto/16 :goto_0

    .line 2642553
    :pswitch_6
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 2642554
    :goto_4
    iput-boolean v0, v1, LX/J34;->g:Z

    .line 2642555
    move-object v0, v1

    .line 2642556
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642557
    goto/16 :goto_0

    .line 2642558
    :pswitch_7
    new-instance v1, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2642559
    const/4 v0, 0x1

    .line 2642560
    iput-boolean v0, v1, LX/34b;->d:Z

    .line 2642561
    sget-object v0, LX/J3T;->ITEM_IMAGE:LX/J3T;

    invoke-virtual {v0}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 2642562
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2642563
    invoke-virtual {v1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    .line 2642564
    new-instance p1, LX/J3S;

    invoke-direct {p1, p0, v0}, LX/J3S;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Ljava/lang/String;)V

    invoke-virtual {v3, p1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_5

    .line 2642565
    :cond_1
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2642566
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2642567
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2642568
    goto/16 :goto_0

    .line 2642569
    :pswitch_8
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642570
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642571
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642572
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J35;

    invoke-direct {v3, p0, v0}, LX/J35;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642573
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642574
    goto/16 :goto_0

    .line 2642575
    :pswitch_9
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642576
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642577
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642578
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J36;

    invoke-direct {v3, p0, v0}, LX/J36;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642579
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642580
    goto/16 :goto_0

    .line 2642581
    :pswitch_a
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642582
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642583
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642584
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J37;

    invoke-direct {v3, p0, v0}, LX/J37;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642585
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642586
    goto/16 :goto_0

    .line 2642587
    :pswitch_b
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642588
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642589
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642590
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J38;

    invoke-direct {v3, p0, v0}, LX/J38;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642591
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642592
    goto/16 :goto_0

    .line 2642593
    :pswitch_c
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    if-nez v0, :cond_7

    const/4 v0, 0x1

    .line 2642594
    :goto_6
    iput-boolean v0, v1, LX/J34;->m:Z

    .line 2642595
    move-object v0, v1

    .line 2642596
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642597
    goto/16 :goto_0

    .line 2642598
    :pswitch_d
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642599
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642600
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642601
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J39;

    invoke-direct {v3, p0, v0}, LX/J39;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642602
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642603
    goto/16 :goto_0

    .line 2642604
    :pswitch_e
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642605
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642606
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642607
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3A;

    invoke-direct {v3, p0, v0}, LX/J3A;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642608
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642609
    goto/16 :goto_0

    .line 2642610
    :pswitch_f
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642611
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642612
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642613
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3B;

    invoke-direct {v3, p0, v0}, LX/J3B;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642614
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642615
    goto/16 :goto_0

    .line 2642616
    :pswitch_10
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642617
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642618
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642619
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3C;

    invoke-direct {v3, p0, v0}, LX/J3C;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642620
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642621
    goto/16 :goto_0

    .line 2642622
    :pswitch_11
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642623
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642624
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642625
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3D;

    invoke-direct {v3, p0, v0}, LX/J3D;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642626
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642627
    goto/16 :goto_0

    .line 2642628
    :pswitch_12
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    if-nez v0, :cond_8

    const/4 v0, 0x1

    .line 2642629
    :goto_7
    iput-boolean v0, v1, LX/J34;->s:Z

    .line 2642630
    move-object v0, v1

    .line 2642631
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642632
    goto/16 :goto_0

    .line 2642633
    :pswitch_13
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    .line 2642634
    :goto_8
    iput-boolean v0, v1, LX/J34;->t:Z

    .line 2642635
    move-object v0, v1

    .line 2642636
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642637
    goto/16 :goto_0

    .line 2642638
    :pswitch_14
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    if-nez v0, :cond_a

    const/4 v0, 0x1

    .line 2642639
    :goto_9
    iput-boolean v0, v1, LX/J34;->u:Z

    .line 2642640
    move-object v0, v1

    .line 2642641
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642642
    goto/16 :goto_0

    .line 2642643
    :pswitch_15
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    if-nez v0, :cond_b

    const/4 v0, 0x1

    .line 2642644
    :goto_a
    iput-boolean v0, v1, LX/J34;->v:Z

    .line 2642645
    move-object v0, v1

    .line 2642646
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642647
    goto/16 :goto_0

    .line 2642648
    :pswitch_16
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    .line 2642649
    :goto_b
    iput-boolean v0, v1, LX/J34;->w:Z

    .line 2642650
    move-object v0, v1

    .line 2642651
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642652
    goto/16 :goto_0

    .line 2642653
    :pswitch_17
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    .line 2642654
    :goto_c
    iput-boolean v0, v1, LX/J34;->x:Z

    .line 2642655
    move-object v0, v1

    .line 2642656
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642657
    goto/16 :goto_0

    .line 2642658
    :pswitch_18
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    if-nez v0, :cond_e

    const/4 v0, 0x1

    .line 2642659
    :goto_d
    iput-boolean v0, v1, LX/J34;->y:Z

    .line 2642660
    move-object v0, v1

    .line 2642661
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642662
    goto/16 :goto_0

    .line 2642663
    :pswitch_19
    new-instance v1, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2642664
    const/4 v0, 0x1

    .line 2642665
    iput-boolean v0, v1, LX/34b;->d:Z

    .line 2642666
    sget-object v0, LX/J3T;->PLAIN_TEXT:LX/J3T;

    invoke-virtual {v0}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 2642667
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2642668
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2642669
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b:LX/0P1;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 2642670
    new-instance p1, LX/J3E;

    invoke-direct {p1, p0, v3}, LX/J3E;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;I)V

    invoke-virtual {v0, p1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_e

    .line 2642671
    :cond_2
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2642672
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2642673
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2642674
    goto/16 :goto_0

    .line 2642675
    :pswitch_1a
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642676
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642677
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642678
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3G;

    invoke-direct {v3, p0, v0}, LX/J3G;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642679
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642680
    goto/16 :goto_0

    .line 2642681
    :pswitch_1b
    new-instance v0, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 2642682
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2642683
    invoke-virtual {v0}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->setSelection(I)V

    .line 2642684
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/J3H;

    invoke-direct {v3, p0, v0}, LX/J3H;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2642685
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2642686
    goto/16 :goto_0

    .line 2642687
    :pswitch_1c
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v0, v1}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    if-nez v0, :cond_f

    const/4 v0, 0x1

    .line 2642688
    :goto_f
    iput-boolean v0, v1, LX/J34;->C:Z

    .line 2642689
    move-object v0, v1

    .line 2642690
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642691
    goto/16 :goto_0

    .line 2642692
    :pswitch_1d
    const/4 p1, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2642693
    new-instance v1, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2642694
    iput-boolean v4, v1, LX/34b;->d:Z

    .line 2642695
    sget-object v2, LX/J3T;->PAY_BUTTON_TEXT:LX/J3T;

    invoke-virtual {v2}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/34b;->a(Ljava/lang/String;)V

    .line 2642696
    new-array v2, p1, [I

    const v3, 0x7f081d47

    aput v3, v2, v0

    const v3, 0x7f08002a

    aput v3, v2, v4

    .line 2642697
    :goto_10
    if-ge v0, p1, :cond_3

    aget v3, v2, v0

    .line 2642698
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    .line 2642699
    new-instance v5, LX/J3I;

    invoke-direct {v5, p0, v3}, LX/J3I;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;I)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2642700
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 2642701
    :cond_3
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2642702
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2642703
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2642704
    goto/16 :goto_0

    .line 2642705
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2642706
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2642707
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 2642708
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 2642709
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 2642710
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 2642711
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 2642712
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 2642713
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_b

    .line 2642714
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_c

    .line 2642715
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_d

    .line 2642716
    :cond_f
    const/4 v0, 0x0

    goto :goto_f

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public static aa()Lcom/facebook/payments/form/model/FormFieldAttributes;
    .locals 4

    .prologue
    .line 2642508
    sget-object v0, LX/6xN;->PRICE:LX/6xN;

    const-string v1, "Enter Price"

    sget-object v2, LX/6xe;->REQUIRED:LX/6xe;

    sget-object v3, LX/6xO;->PRICE:LX/6xO;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v0

    const-string v1, "USD"

    .line 2642509
    iput-object v1, v0, LX/6xM;->g:Ljava/lang/String;

    .line 2642510
    move-object v0, v0

    .line 2642511
    invoke-virtual {v0}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V
    .locals 2

    .prologue
    .line 2642503
    iput-object p1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642504
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642505
    iput-object v1, v0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642506
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    const v1, -0x2164aca4

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2642507
    return-void
.end method

.method public static k(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2642494
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2642495
    :try_start_0
    sget-object v1, LX/J3T;->AMOUNT_CUSTOM_LABEL:LX/J3T;

    invoke-virtual {v1}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->d:LX/5fv;

    iget-object v3, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v3, v3, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v4, v4, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v1

    .line 2642496
    const v2, 0x7f081d43

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->d:LX/5fv;

    iget-object v4, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v4, v4, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v5, v5, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v2

    .line 2642497
    const v3, 0x7f081d44

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->d:LX/5fv;

    iget-object v5, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v5, v5, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v6, v6, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v3

    .line 2642498
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642499
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642500
    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2642501
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2642502
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to parse the amounts provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2642490
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2642491
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->m:Landroid/content/Context;

    .line 2642492
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->m:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    new-instance v3, LX/J31;

    invoke-direct {v3}, LX/J31;-><init>()V

    move-object v3, v3

    move-object v3, v3

    check-cast v3, LX/J31;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v4

    check-cast v4, LX/5fv;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x12cb

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/Iyi;->b(LX/0QB;)LX/Iyi;

    move-result-object v8

    check-cast v8, LX/Iyi;

    invoke-static {v0}, LX/Iyo;->b(LX/0QB;)LX/Iyo;

    move-result-object v9

    check-cast v9, LX/Iyo;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object p1

    check-cast p1, LX/0lC;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v3, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    iput-object v4, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->d:LX/5fv;

    iput-object v5, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->f:LX/0Or;

    iput-object v7, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->g:Ljava/util/concurrent/Executor;

    iput-object v8, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->h:LX/Iyi;

    iput-object v9, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->i:LX/Iyo;

    iput-object p1, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->j:LX/0lC;

    iput-object v0, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->k:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2642493
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2642485
    packed-switch p1, :pswitch_data_0

    .line 2642486
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2642487
    :cond_0
    :goto_0
    return-void

    .line 2642488
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2642489
    const-string v0, "selector_params"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3e466a7a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2642484
    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->m:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f18

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x631061f6

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2642390
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2642391
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->l:Landroid/widget/ListView;

    .line 2642392
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2642393
    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v0

    .line 2642394
    iput-boolean p1, v0, LX/J34;->a:Z

    .line 2642395
    move-object v0, v0

    .line 2642396
    iput-boolean p2, v0, LX/J34;->b:Z

    .line 2642397
    move-object v0, v0

    .line 2642398
    const-string v1, "http://cdn8.staztic.com/app/a/740/740596/cute-pikachu-wallpaper-849170-1-s-307x512.jpg"

    .line 2642399
    iput-object v1, v0, LX/J34;->c:Ljava/lang/String;

    .line 2642400
    move-object v0, v0

    .line 2642401
    const-string v1, "Pokemon Page"

    .line 2642402
    iput-object v1, v0, LX/J34;->d:Ljava/lang/String;

    .line 2642403
    move-object v0, v0

    .line 2642404
    const-string v1, "Sold by Ticketmaster"

    .line 2642405
    iput-object v1, v0, LX/J34;->e:Ljava/lang/String;

    .line 2642406
    move-object v0, v0

    .line 2642407
    iput-boolean p1, v0, LX/J34;->g:Z

    .line 2642408
    move-object v0, v0

    .line 2642409
    const-string v1, "http://cdn8.staztic.com/app/a/740/740596/cute-pikachu-wallpaper-849170-1-s-307x512.jpg"

    .line 2642410
    iput-object v1, v0, LX/J34;->h:Ljava/lang/String;

    .line 2642411
    move-object v0, v0

    .line 2642412
    const-string v1, "Pikachu"

    .line 2642413
    iput-object v1, v0, LX/J34;->i:Ljava/lang/String;

    .line 2642414
    move-object v0, v0

    .line 2642415
    const-string v1, "Yellow, Medium"

    .line 2642416
    iput-object v1, v0, LX/J34;->j:Ljava/lang/String;

    .line 2642417
    move-object v0, v0

    .line 2642418
    const-string v1, "Best selling pokemon!"

    .line 2642419
    iput-object v1, v0, LX/J34;->k:Ljava/lang/String;

    .line 2642420
    move-object v0, v0

    .line 2642421
    const-string v1, "Facebook"

    .line 2642422
    iput-object v1, v0, LX/J34;->l:Ljava/lang/String;

    .line 2642423
    move-object v0, v0

    .line 2642424
    iput-boolean p2, v0, LX/J34;->m:Z

    .line 2642425
    move-object v0, v0

    .line 2642426
    const-string v1, "USD"

    .line 2642427
    iput-object v1, v0, LX/J34;->n:Ljava/lang/String;

    .line 2642428
    move-object v0, v0

    .line 2642429
    const-string v1, "12.56"

    .line 2642430
    iput-object v1, v0, LX/J34;->o:Ljava/lang/String;

    .line 2642431
    move-object v0, v0

    .line 2642432
    const-string v1, "1.23"

    .line 2642433
    iput-object v1, v0, LX/J34;->p:Ljava/lang/String;

    .line 2642434
    move-object v0, v0

    .line 2642435
    const-string v1, "4.97"

    .line 2642436
    iput-object v1, v0, LX/J34;->q:Ljava/lang/String;

    .line 2642437
    move-object v0, v0

    .line 2642438
    const-string v1, "18.76"

    .line 2642439
    iput-object v1, v0, LX/J34;->r:Ljava/lang/String;

    .line 2642440
    move-object v0, v0

    .line 2642441
    iput-boolean p2, v0, LX/J34;->v:Z

    .line 2642442
    move-object v0, v0

    .line 2642443
    iput-boolean p1, v0, LX/J34;->w:Z

    .line 2642444
    move-object v0, v0

    .line 2642445
    iput-boolean p2, v0, LX/J34;->x:Z

    .line 2642446
    move-object v0, v0

    .line 2642447
    iput-boolean p1, v0, LX/J34;->y:Z

    .line 2642448
    move-object v0, v0

    .line 2642449
    iput p1, v0, LX/J34;->z:I

    .line 2642450
    move-object v0, v0

    .line 2642451
    const-string v1, "2C2P"

    .line 2642452
    iput-object v1, v0, LX/J34;->A:Ljava/lang/String;

    .line 2642453
    move-object v0, v0

    .line 2642454
    const-string v1, "Walmart"

    .line 2642455
    iput-object v1, v0, LX/J34;->B:Ljava/lang/String;

    .line 2642456
    move-object v0, v0

    .line 2642457
    iput-boolean p1, v0, LX/J34;->C:Z

    .line 2642458
    move-object v0, v0

    .line 2642459
    const v1, 0x7f081d47

    .line 2642460
    iput v1, v0, LX/J34;->D:I

    .line 2642461
    move-object v0, v0

    .line 2642462
    invoke-virtual {v0}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642463
    invoke-static {}, LX/J3T;->values()[LX/J3T;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->p:LX/0Px;

    .line 2642464
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 2642465
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2642466
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, LX/J3M;

    invoke-direct {v2, p0}, LX/J3M;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    sget-object p1, LX/73i;->DEFAULT:LX/73i;

    sget-object p2, LX/73h;->BACK_ARROW:LX/73h;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 2642467
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 2642468
    const-string v1, "Sample Payments Flows"

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2642469
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const-string v2, "BUY"

    .line 2642470
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2642471
    move-object v1, v1

    .line 2642472
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2642473
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    const/4 p1, 0x0

    aput-object v1, v2, p1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2642474
    new-instance v1, LX/J3N;

    invoke-direct {v1, p0}, LX/J3N;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2642475
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->p:LX/0Px;

    .line 2642476
    iput-object v1, v0, LX/J31;->b:LX/0Px;

    .line 2642477
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642478
    iput-object v1, v0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642479
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    new-instance v1, LX/J3F;

    invoke-direct {v1, p0}, LX/J3F;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    .line 2642480
    iput-object v1, v0, LX/J31;->a:LX/J3F;

    .line 2642481
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->c:LX/J31;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2642482
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->l:Landroid/widget/ListView;

    new-instance v1, LX/J3L;

    invoke-direct {v1, p0}, LX/J3L;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2642483
    return-void
.end method
