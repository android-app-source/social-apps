.class public Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2643104
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643105
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2643106
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;

    const/16 v0, 0x12cb

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;->a:LX/0Or;

    .line 2643107
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x362b40d4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2643108
    const v1, 0x7f030f0f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x33e989a8    # -3.9442784E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643109
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2643110
    const v0, 0x7f0d1055

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    .line 2643111
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string p1, "payment_providers_view_controller_tag"

    invoke-virtual {v1, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 2643112
    if-nez v1, :cond_0

    .line 2643113
    invoke-static {}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->newBuilder()LX/BF5;

    move-result-object p1

    iget-object v1, p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2643114
    iget-object p2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p2

    .line 2643115
    iput-object v1, p1, LX/BF5;->c:Ljava/lang/String;

    .line 2643116
    move-object v1, p1

    .line 2643117
    sget-object p1, LX/6xg;->NMOR_TIP_JAR:LX/6xg;

    .line 2643118
    iput-object p1, v1, LX/BF5;->b:LX/6xg;

    .line 2643119
    move-object v1, v1

    .line 2643120
    invoke-virtual {v1}, LX/BF5;->a()Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    move-result-object v1

    .line 2643121
    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;)Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    move-result-object v1

    .line 2643122
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object p1

    invoke-virtual {p1}, LX/0gc;->a()LX/0hH;

    move-result-object p1

    const-string p2, "payment_providers_view_controller_tag"

    invoke-virtual {p1, v1, p2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object p1

    invoke-virtual {p1}, LX/0hH;->b()I

    .line 2643123
    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->setComponentController(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 2643124
    return-void
.end method
