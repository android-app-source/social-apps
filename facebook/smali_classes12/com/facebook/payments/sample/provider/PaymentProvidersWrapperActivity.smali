.class public Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2643089
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2643090
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643091
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2643092
    const v0, 0x7f03068c

    invoke-virtual {p0, v0}, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;->setContentView(I)V

    .line 2643093
    if-nez p1, :cond_0

    .line 2643094
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    new-instance v2, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;

    invoke-direct {v2}, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2643095
    :cond_0
    sget-object v0, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 2643096
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643097
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 2643098
    invoke-static {p0, p0}, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2643099
    iget-object v0, p0, Lcom/facebook/payments/sample/provider/PaymentProvidersWrapperActivity;->p:LX/6wr;

    sget-object v1, LX/73i;->DEFAULT:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 2643100
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 2643101
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2643102
    sget-object v0, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 2643103
    return-void
.end method
