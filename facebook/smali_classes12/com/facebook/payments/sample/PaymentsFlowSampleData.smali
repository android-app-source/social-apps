.class public Lcom/facebook/payments/sample/PaymentsFlowSampleData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/sample/PaymentsFlowSampleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Ljava/lang/String;

.field public final C:Z

.field public final D:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public final a:Z

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Z

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:Ljava/lang/String;

.field public final s:Z

.field public final t:Z

.field public final u:Z

.field public final v:Z

.field public final w:Z

.field public final x:Z

.field public final y:Z

.field public final z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2642152
    new-instance v0, LX/J33;

    invoke-direct {v0}, LX/J33;-><init>()V

    sput-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/J34;)V
    .locals 1

    .prologue
    .line 2642153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2642154
    iget-boolean v0, p1, LX/J34;->a:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    .line 2642155
    iget-boolean v0, p1, LX/J34;->b:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    .line 2642156
    iget-object v0, p1, LX/J34;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    .line 2642157
    iget-object v0, p1, LX/J34;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    .line 2642158
    iget-object v0, p1, LX/J34;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    .line 2642159
    iget-object v0, p1, LX/J34;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    .line 2642160
    iget-boolean v0, p1, LX/J34;->g:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    .line 2642161
    iget-object v0, p1, LX/J34;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    .line 2642162
    iget-object v0, p1, LX/J34;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    .line 2642163
    iget-object v0, p1, LX/J34;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    .line 2642164
    iget-object v0, p1, LX/J34;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    .line 2642165
    iget-object v0, p1, LX/J34;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    .line 2642166
    iget-boolean v0, p1, LX/J34;->m:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    .line 2642167
    iget-object v0, p1, LX/J34;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    .line 2642168
    iget-object v0, p1, LX/J34;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    .line 2642169
    iget-object v0, p1, LX/J34;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    .line 2642170
    iget-object v0, p1, LX/J34;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    .line 2642171
    iget-object v0, p1, LX/J34;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    .line 2642172
    iget-boolean v0, p1, LX/J34;->s:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    .line 2642173
    iget-boolean v0, p1, LX/J34;->t:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    .line 2642174
    iget-boolean v0, p1, LX/J34;->u:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    .line 2642175
    iget-boolean v0, p1, LX/J34;->v:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    .line 2642176
    iget-boolean v0, p1, LX/J34;->w:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    .line 2642177
    iget-boolean v0, p1, LX/J34;->x:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    .line 2642178
    iget-boolean v0, p1, LX/J34;->y:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    .line 2642179
    iget v0, p1, LX/J34;->z:I

    iput v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    .line 2642180
    iget-object v0, p1, LX/J34;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    .line 2642181
    iget-object v0, p1, LX/J34;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    .line 2642182
    iget-boolean v0, p1, LX/J34;->C:Z

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    .line 2642183
    iget v0, p1, LX/J34;->D:I

    iput v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    .line 2642184
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2642119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2642120
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    .line 2642121
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    .line 2642122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    .line 2642123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    .line 2642124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    .line 2642125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    .line 2642126
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    .line 2642127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    .line 2642128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    .line 2642129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    .line 2642130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    .line 2642131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    .line 2642132
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    .line 2642133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    .line 2642134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    .line 2642135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    .line 2642136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    .line 2642137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    .line 2642138
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    .line 2642139
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    .line 2642140
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    .line 2642141
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    .line 2642142
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    .line 2642143
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    .line 2642144
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    .line 2642145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    .line 2642146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    .line 2642147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    .line 2642148
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    .line 2642149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    .line 2642150
    return-void
.end method

.method public static newBuilder()LX/J34;
    .locals 1

    .prologue
    .line 2642151
    new-instance v0, LX/J34;

    invoke-direct {v0}, LX/J34;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2642105
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    .line 2642106
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    if-eqz v1, :cond_0

    .line 2642107
    sget-object v1, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642108
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    if-eqz v1, :cond_2

    .line 2642109
    :cond_1
    sget-object v1, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642110
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    if-eqz v1, :cond_3

    .line 2642111
    sget-object v1, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642112
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    if-eqz v1, :cond_4

    .line 2642113
    sget-object v1, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642114
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    if-eqz v1, :cond_5

    .line 2642115
    sget-object v1, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642116
    :cond_5
    iget-boolean v1, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    if-eqz v1, :cond_6

    .line 2642117
    sget-object v1, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642118
    :cond_6
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2642104
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2642073
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642074
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642075
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642076
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642077
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642078
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642079
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642080
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642081
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642082
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642083
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642084
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642085
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642086
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642087
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642088
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642089
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642090
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642091
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642092
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642093
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642094
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642095
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642096
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642097
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642098
    iget v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2642099
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642100
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2642101
    iget-boolean v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2642102
    iget v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2642103
    return-void
.end method
