.class public Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2641851
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2641860
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2641856
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2641857
    const v0, 0x7f030f17

    invoke-virtual {p0, v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;->setContentView(I)V

    .line 2641858
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    new-instance v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    invoke-direct {v2}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;-><init>()V

    const-string v3, "fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2641859
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2641852
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 2641853
    invoke-static {p0, p0}, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2641854
    iget-object v0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleActivity;->p:LX/6wr;

    sget-object v1, LX/73i;->DEFAULT:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 2641855
    return-void
.end method
