.class public Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IEn;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/IEt;

.field public final c:I

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;ILX/IEt;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;I",
            "Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter$BirthdayStickerItemListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2553369
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2553370
    iput-object p2, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->a:Ljava/util/List;

    .line 2553371
    iput-object p4, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->b:LX/IEt;

    .line 2553372
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->d:Landroid/content/res/Resources;

    .line 2553373
    add-int/lit8 v0, p3, 0x1

    iget-object v1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->d:Landroid/content/res/Resources;

    const v2, 0x7f0b1ca3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/2addr v0, v1

    .line 2553374
    iget-object v1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->d:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v0, v1, v0

    div-int/2addr v0, p3

    iput v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->c:I

    .line 2553375
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2553376
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2553377
    const v1, 0x7f03019c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2553378
    new-instance v1, LX/IEn;

    invoke-direct {v1, p0, v0}, LX/IEn;-><init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2553379
    check-cast p1, LX/IEn;

    .line 2553380
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 2553381
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2553382
    iget-object v1, p1, LX/IEn;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    const-class p2, LX/IEn;

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p2

    invoke-virtual {v1, p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2553383
    iget-object v1, p1, LX/IEn;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance p0, LX/IEm;

    invoke-direct {p0, p1, v0}, LX/IEm;-><init>(LX/IEn;Lcom/facebook/stickers/model/Sticker;)V

    invoke-virtual {v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2553384
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2553385
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
