.class public Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IEs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IEx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:LX/IEt;

.field public f:Landroid/support/v7/widget/RecyclerView;

.field public g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public i:Z

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2553455
    const-class v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2553503
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2553504
    new-instance v0, LX/IEt;

    invoke-direct {v0, p0}, LX/IEt;-><init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->e:LX/IEt;

    return-void
.end method

.method public static c$redex0(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V
    .locals 4

    .prologue
    .line 2553487
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2553488
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2553489
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2553490
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->c:LX/IEs;

    iget-object v1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->k:Ljava/lang/String;

    .line 2553491
    new-instance v3, LX/IEr;

    invoke-static {v0}, Lcom/facebook/stickers/search/StickerSearchLoader;->b(LX/0QB;)Lcom/facebook/stickers/search/StickerSearchLoader;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/search/StickerSearchLoader;

    invoke-direct {v3, v2, v1}, LX/IEr;-><init>(Lcom/facebook/stickers/search/StickerSearchLoader;Ljava/lang/String;)V

    .line 2553492
    move-object v0, v3

    .line 2553493
    new-instance v1, LX/IEw;

    invoke-direct {v1, p0}, LX/IEw;-><init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V

    .line 2553494
    sget-object v2, LX/IEp;->a:[I

    iget-object v3, v0, LX/IEr;->c:LX/IEq;

    invoke-virtual {v3}, LX/IEq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2553495
    :goto_0
    return-void

    .line 2553496
    :pswitch_0
    iget-object v2, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2553497
    iget-object v2, v0, LX/IEr;->c:LX/IEq;

    sget-object v3, LX/IEq;->INIT:LX/IEq;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, LX/IEr;->c:LX/IEq;

    sget-object v3, LX/IEq;->FAILED:LX/IEq;

    if-eq v2, v3, :cond_0

    .line 2553498
    :goto_1
    goto :goto_0

    .line 2553499
    :pswitch_1
    iget-object v2, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2553500
    :pswitch_2
    iget-object v2, v0, LX/IEr;->e:LX/0Px;

    invoke-virtual {v1, v2}, LX/IEw;->a(LX/0Px;)V

    goto :goto_0

    .line 2553501
    :cond_0
    iget-object v2, v0, LX/IEr;->b:Lcom/facebook/stickers/search/StickerSearchLoader;

    new-instance v3, LX/IEo;

    invoke-direct {v3, v0}, LX/IEo;-><init>(LX/IEr;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(LX/3Mb;)V

    .line 2553502
    iget-object v2, v0, LX/IEr;->b:Lcom/facebook/stickers/search/StickerSearchLoader;

    new-instance v3, LX/8li;

    iget-object p0, v0, LX/IEr;->a:Ljava/lang/String;

    sget-object v1, LX/4m4;->POSTS:LX/4m4;

    invoke-direct {v3, p0, v1}, LX/8li;-><init>(Ljava/lang/String;LX/4m4;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2553484
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2553485
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-static {p1}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    const-class v3, LX/IEs;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/IEs;

    new-instance v1, LX/IEx;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/IEx;-><init>(LX/0Zb;)V

    move-object p1, v1

    check-cast p1, LX/IEx;

    iput-object v2, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->b:LX/1Kf;

    iput-object v3, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->c:LX/IEs;

    iput-object p1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->d:LX/IEx;

    .line 2553486
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2553479
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2553480
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2553481
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2553482
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2553483
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x45a7359a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2553478
    const v1, 0x7f03019b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x45363148

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2553456
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2553457
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2553458
    const-string v1, "extra_birthday_sticker_composer_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2553459
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2553460
    const-string v1, "extra_birthday_sticker_from_composer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->i:Z

    .line 2553461
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2553462
    const-string v1, "extra_birthday_composer_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    .line 2553463
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2553464
    const-string v1, "extra_sticker_search_query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->k:Ljava/lang/String;

    .line 2553465
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2553466
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    .line 2553467
    :cond_0
    const v0, 0x7f0d06f7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2553468
    const v0, 0x7f0d06f6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    .line 2553469
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->d:LX/IEx;

    iget-object v1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    .line 2553470
    iget-object v2, v0, LX/IEx;->a:LX/0Zb;

    const-string v3, "open_sticker_picker"

    invoke-static {v0, v3}, LX/IEx;->c(LX/IEx;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p2, "composer_session_id"

    invoke-virtual {v3, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2553471
    iget-object v0, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->d:LX/IEx;

    iget-object v1, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->k:Ljava/lang/String;

    .line 2553472
    iget-object v3, v0, LX/IEx;->a:LX/0Zb;

    const-string v4, "sticker_search_query"

    invoke-static {v0, v4}, LX/IEx;->c(LX/IEx;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p2, "composer_session_id"

    invoke-virtual {v4, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p2, "sticker_search_query"

    invoke-virtual {v4, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2553473
    const v0, 0x7f0d06f5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2553474
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082972

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 2553475
    new-instance v1, LX/IEu;

    invoke-direct {v1, p0}, LX/IEu;-><init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2553476
    invoke-static {p0}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->c$redex0(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V

    .line 2553477
    return-void
.end method
