.class public Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2553421
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2553422
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "extra_birthday_sticker_composer_config"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_birthday_sticker_from_composer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_birthday_composer_session_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_sticker_search_query"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2553423
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2553424
    const v0, 0x7f03019a

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerActivity;->setContentView(I)V

    .line 2553425
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    .line 2553426
    if-nez v0, :cond_1

    .line 2553427
    invoke-virtual {p0}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2553428
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2553429
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2553430
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2553431
    :cond_0
    new-instance v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-direct {v0}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;-><init>()V

    move-object v0, v0

    .line 2553432
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2553433
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    sget-object v3, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2553434
    :cond_1
    return-void
.end method
