.class public Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;
.super LX/1Qj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;


# instance fields
.field public final n:LX/1HI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1HI;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480529
    new-instance v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment$NoOpRunnable;

    invoke-direct {v0}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment$NoOpRunnable;-><init>()V

    sget-object v1, LX/1PU;->b:LX/1PY;

    invoke-direct {p0, p1, v0, v1}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2480530
    iput-object p2, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->n:LX/1HI;

    .line 2480531
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;
    .locals 5

    .prologue
    .line 2480532
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->o:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    if-nez v0, :cond_1

    .line 2480533
    const-class v1, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    monitor-enter v1

    .line 2480534
    :try_start_0
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->o:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480535
    if-eqz v2, :cond_0

    .line 2480536
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480537
    new-instance p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-direct {p0, v3, v4}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;-><init>(Landroid/content/Context;LX/1HI;)V

    .line 2480538
    move-object v0, p0

    .line 2480539
    sput-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->o:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480540
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480541
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480542
    :cond_1
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->o:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    return-object v0

    .line 2480543
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 2480545
    iget-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->n:LX/1HI;

    const-string v1, "notification_story_images_prefetch_environemnt"

    invoke-static {p2, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 2480546
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2480547
    sget-object v0, LX/HYN;->a:LX/HYN;

    move-object v0, v0

    .line 2480548
    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2480549
    const/4 v0, 0x0

    return-object v0
.end method
