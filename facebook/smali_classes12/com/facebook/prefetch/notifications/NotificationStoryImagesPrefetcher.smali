.class public Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;


# instance fields
.field private final a:LX/Ajr;

.field public final b:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

.field private final c:LX/1Da;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/permalink/PermalinkRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1R0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R0",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Z


# direct methods
.method public constructor <init>(LX/Ajl;LX/22q;Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;LX/1Da;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0ad;)V
    .locals 2
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ajl;",
            "LX/22q;",
            "Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;",
            "LX/1Da;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/permalink/PermalinkRootPartDefinition;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480582
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "prefetch_notification_image_in_bkg"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 2480583
    invoke-virtual {p2, v0, p1}, LX/22q;->a(Lcom/facebook/common/callercontext/CallerContext;LX/Ajk;)LX/Ajr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->a:LX/Ajr;

    .line 2480584
    iput-object p3, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->b:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    .line 2480585
    iput-object p4, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->c:LX/1Da;

    .line 2480586
    iput-object p5, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->d:LX/0Ot;

    .line 2480587
    iput-object p6, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->f:Ljava/util/concurrent/ExecutorService;

    .line 2480588
    sget-short v0, LX/HYP;->a:S

    const/4 v1, 0x0

    invoke-interface {p7, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->g:Z

    .line 2480589
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;
    .locals 11

    .prologue
    .line 2480561
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->h:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    if-nez v0, :cond_1

    .line 2480562
    const-class v1, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    monitor-enter v1

    .line 2480563
    :try_start_0
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->h:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480564
    if-eqz v2, :cond_0

    .line 2480565
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480566
    new-instance v3, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    invoke-static {v0}, LX/Ajl;->a(LX/0QB;)LX/Ajl;

    move-result-object v4

    check-cast v4, LX/Ajl;

    const-class v5, LX/22q;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/22q;

    invoke-static {v0}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;->a(LX/0QB;)Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;

    invoke-static {v0}, LX/1Da;->b(LX/0QB;)LX/1Da;

    move-result-object v7

    check-cast v7, LX/1Da;

    const/16 v8, 0x1cd3

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;-><init>(LX/Ajl;LX/22q;Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetchEnvironment;LX/1Da;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0ad;)V

    .line 2480567
    move-object v0, v3

    .line 2480568
    sput-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->h:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480569
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480570
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480571
    :cond_1
    sget-object v0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->h:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    return-object v0

    .line 2480572
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 4

    .prologue
    .line 2480574
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->g:Z

    if-nez v0, :cond_0

    .line 2480575
    iget-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->a:LX/Ajr;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/5g1;->OFF:LX/5g1;

    invoke-virtual {v0, v1, v2}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2480576
    :goto_0
    monitor-exit p0

    return-void

    .line 2480577
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->e:LX/1R0;

    if-nez v0, :cond_1

    .line 2480578
    iget-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->c:LX/1Da;

    iget-object v1, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->d:LX/0Ot;

    new-instance v2, LX/HYM;

    invoke-direct {v2}, LX/HYM;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1Da;->a(LX/0Ot;LX/1Qx;Z)LX/1R0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->e:LX/1R0;

    .line 2480579
    :cond_1
    iget-object v0, p0, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher$1;-><init>(Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;Lcom/facebook/graphql/model/FeedUnit;)V

    const v2, -0x24292b1a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2480580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
