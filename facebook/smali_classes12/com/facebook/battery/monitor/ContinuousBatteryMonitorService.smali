.class public Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;
.super LX/1ZN;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field private static final h:Ljava/lang/String;

.field private static final i:LX/0Tn;

.field private static final j:LX/0Tn;


# instance fields
.field public b:LX/0So;
    .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/UUID;
    .annotation runtime Lcom/facebook/device/annotations/BootId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2MH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499738
    const-class v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->h:Ljava/lang/String;

    .line 2499739
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "continuous_battery_monitor/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2499740
    sput-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a:LX/0Tn;

    const-string v1, "service_trigger_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->i:LX/0Tn;

    .line 2499741
    sget-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a:LX/0Tn;

    const-string v1, "last_cleanup_since_boot_time_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->j:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2499742
    sget-object v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2499743
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2499745
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action_enable_broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "trigger_type"

    const-string v2, "com.facebook.intent.action.ENABLE_BROADCAST_RECEIVER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2499744
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action_report_battery_status"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "trigger_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(IIJLjava/lang/String;)V
    .locals 3

    .prologue
    .line 2499730
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->d:LX/0Zb;

    const-string v1, "android_continuous_battery_drain"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2499731
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2499732
    const-string v1, "current_battery_level"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "max_battery_level"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "trigger_type"

    invoke-virtual {v0, v1, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "elapsed_time_since_boot_ms"

    invoke-virtual {v0, v1, p3, p4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v0

    const-string v1, "boot_id"

    iget-object v2, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->f:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2499733
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 2499734
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MH;

    const-class v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;

    invoke-virtual {v0, v1}, LX/2MH;->b(Ljava/lang/Class;)V

    .line 2499735
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2499736
    const/4 v1, 0x3

    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, p1

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    invoke-static {p0, v4, v5, v6}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 2499737
    return-void
.end method

.method private static a(Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;LX/0So;LX/03V;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/UUID;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;",
            "LX/0So;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/UUID;",
            "LX/0Ot",
            "<",
            "LX/2MH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2499729
    iput-object p1, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->b:LX/0So;

    iput-object p2, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->c:LX/03V;

    iput-object p3, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->d:LX/0Zb;

    iput-object p4, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->f:Ljava/util/UUID;

    iput-object p6, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->g:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;

    invoke-static {v6}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v6}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v6}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v6}, LX/2G9;->a(LX/0QB;)Ljava/util/UUID;

    move-result-object v5

    check-cast v5, Ljava/util/UUID;

    const/16 v7, 0x1cf

    invoke-static {v6, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;LX/0So;LX/03V;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/UUID;LX/0Ot;)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v12, -0x1

    .line 2499708
    const-string v0, "action_enable_broadcast_receiver"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2499709
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MH;

    const-class v2, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService$BroadcastReceiver;

    invoke-virtual {v0, v2}, LX/2MH;->a(Ljava/lang/Class;)V

    .line 2499710
    :cond_0
    const/4 v0, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 2499711
    if-nez v0, :cond_1

    .line 2499712
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->c:LX/03V;

    sget-object v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->h:Ljava/lang/String;

    const-string v2, "the intent to get battery status is unavailable"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2499713
    :goto_0
    return-void

    .line 2499714
    :cond_1
    const-string v2, "level"

    invoke-virtual {v0, v2, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2499715
    if-ne v2, v12, :cond_2

    .line 2499716
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->c:LX/03V;

    sget-object v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->h:Ljava/lang/String;

    const-string v2, "battery level is unavailable"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2499717
    :cond_2
    const-string v3, "scale"

    invoke-virtual {v0, v3, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 2499718
    if-ne v3, v12, :cond_3

    .line 2499719
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->c:LX/03V;

    sget-object v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->h:Ljava/lang/String;

    const-string v2, "battery scale is unavailable"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2499720
    :cond_3
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->j:LX/0Tn;

    invoke-interface {v0, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 2499721
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 2499722
    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    sub-long v8, v4, v6

    const-wide/32 v10, 0x5265c00

    cmp-long v0, v8, v10

    if-gez v0, :cond_4

    sub-long v6, v4, v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-gez v0, :cond_5

    .line 2499723
    :cond_4
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v6, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->j:LX/0Tn;

    invoke-interface {v0, v6, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v6, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->i:LX/0Tn;

    invoke-interface {v0, v6, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    move v0, v1

    .line 2499724
    :goto_1
    const/16 v1, 0xf0

    if-le v0, v1, :cond_6

    .line 2499725
    invoke-direct {p0, v4, v5}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(J)V

    goto :goto_0

    .line 2499726
    :cond_5
    iget-object v0, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->i:LX/0Tn;

    invoke-interface {v0, v1, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2499727
    iget-object v1, p0, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v6, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->i:LX/0Tn;

    invoke-interface {v1, v6, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_1

    .line 2499728
    :cond_6
    const-string v0, "trigger_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(IIJLjava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x6dfe89e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499704
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->b(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499705
    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    .line 2499706
    const v1, 0x384c952e

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 2499707
    :catchall_0
    move-exception v1

    invoke-static {p1}, LX/37V;->a(Landroid/content/Intent;)Z

    const v2, 0x7b38c29

    invoke-static {v2, v0}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x47f70a72

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499701
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2499702
    invoke-static {p0, p0}, Lcom/facebook/battery/monitor/ContinuousBatteryMonitorService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2499703
    const/16 v1, 0x25

    const v2, 0x727920f3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
