.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2507284
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2507285
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2507286
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2507287
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2507288
    const/4 v2, 0x0

    .line 2507289
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 2507290
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2507291
    :goto_0
    move v1, v2

    .line 2507292
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2507293
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2507294
    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;-><init>()V

    .line 2507295
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2507296
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2507297
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2507298
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2507299
    :cond_0
    return-object v1

    .line 2507300
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2507301
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_a

    .line 2507302
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2507303
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2507304
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 2507305
    const-string p0, "current_tag_expansion"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2507306
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 2507307
    :cond_3
    const-string p0, "excluded_members"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2507308
    invoke-static {p1, v0}, LX/Hp7;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2507309
    :cond_4
    const-string p0, "icon_image"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2507310
    invoke-static {p1, v0}, LX/Hp5;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2507311
    :cond_5
    const-string p0, "id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2507312
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2507313
    :cond_6
    const-string p0, "included_members"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2507314
    invoke-static {p1, v0}, LX/Hp8;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2507315
    :cond_7
    const-string p0, "legacy_graph_api_privacy_json"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2507316
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2507317
    :cond_8
    const-string p0, "name"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2507318
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2507319
    :cond_9
    const-string p0, "tag_expansion_options"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2507320
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 2507321
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2507322
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2507323
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2507324
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2507325
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2507326
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2507327
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2507328
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2507329
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2507330
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
