.class public final Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2508636
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2508637
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2508638
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2508639
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2508640
    const/4 v2, 0x0

    .line 2508641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 2508642
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2508643
    :goto_0
    move v1, v2

    .line 2508644
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2508645
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2508646
    new-instance v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;-><init>()V

    .line 2508647
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2508648
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2508649
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2508650
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2508651
    :cond_0
    return-object v1

    .line 2508652
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2508653
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_7

    .line 2508654
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2508655
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2508656
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 2508657
    const-string p0, "__type__"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2508658
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 2508659
    :cond_4
    const-string p0, "albums"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2508660
    invoke-static {p1, v0}, LX/HpK;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2508661
    :cond_5
    const-string p0, "group_albums"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2508662
    invoke-static {p1, v0}, LX/HpM;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2508663
    :cond_6
    const-string p0, "id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2508664
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2508665
    :cond_7
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2508666
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2508667
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2508668
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2508669
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2508670
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
