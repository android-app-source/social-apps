.class public final Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xf06c76f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508726
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508725
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2508723
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2508724
    return-void
.end method

.method private a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508727
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508728
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2508717
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508718
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2508719
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2508720
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2508721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508722
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2508709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508710
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2508711
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508712
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2508713
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;

    .line 2508714
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508715
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508716
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2508706
    new-instance v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel$EdgesModel;-><init>()V

    .line 2508707
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2508708
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2508705
    const v0, 0x66439fce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2508704
    const v0, -0x26eca880

    return v0
.end method
