.class public final Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xf06c76f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508594
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508593
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2508591
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2508592
    return-void
.end method

.method private a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508589
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508590
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2508583
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508584
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2508585
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2508586
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2508587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508588
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2508575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508576
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2508577
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508578
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2508579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;

    .line 2508580
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPicker_AlbumRelayQLModels$CSComposerAlbumPickerAlbumRelayQLModel;

    .line 2508581
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508582
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2508570
    new-instance v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel$EdgesModel;-><init>()V

    .line 2508571
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2508572
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2508574
    const v0, 0x38149d88

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2508573
    const v0, 0x45667be1

    return v0
.end method
