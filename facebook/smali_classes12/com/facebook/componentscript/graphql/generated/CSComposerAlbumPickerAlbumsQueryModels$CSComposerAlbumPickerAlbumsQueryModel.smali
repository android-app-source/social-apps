.class public final Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7778787e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508798
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2508844
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2508842
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2508843
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508839
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2508840
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2508841
    :cond_0
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508837
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->f:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->f:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    .line 2508838
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->f:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508835
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->g:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->g:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    .line 2508836
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->g:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508833
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->h:Ljava/lang/String;

    .line 2508834
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2508821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508822
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2508823
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->k()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2508824
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->l()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2508825
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2508826
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2508827
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2508828
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2508829
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2508830
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2508831
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508832
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2508808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2508809
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->k()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2508810
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->k()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    .line 2508811
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->k()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2508812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    .line 2508813
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->f:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$AlbumsModel;

    .line 2508814
    :cond_0
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->l()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2508815
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->l()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    .line 2508816
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->l()Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2508817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    .line 2508818
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->g:Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$GroupAlbumsModel;

    .line 2508819
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2508820
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2508807
    new-instance v0, LX/HpH;

    invoke-direct {v0, p1}, LX/HpH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2508806
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2508804
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2508805
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2508797
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2508801
    new-instance v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;-><init>()V

    .line 2508802
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2508803
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2508800
    const v0, -0x410f0914

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2508799
    const v0, 0x252222

    return v0
.end method
