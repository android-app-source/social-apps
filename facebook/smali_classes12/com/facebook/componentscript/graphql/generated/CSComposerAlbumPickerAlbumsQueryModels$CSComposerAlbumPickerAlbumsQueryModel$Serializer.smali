.class public final Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2508795
    const-class v0, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2508796
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2508794
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2508773
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2508774
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2508775
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2508776
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2508777
    if-eqz v2, :cond_0

    .line 2508778
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508779
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2508780
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2508781
    if-eqz v2, :cond_1

    .line 2508782
    const-string p0, "albums"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508783
    invoke-static {v1, v2, p1, p2}, LX/HpK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2508784
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2508785
    if-eqz v2, :cond_2

    .line 2508786
    const-string p0, "group_albums"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508787
    invoke-static {v1, v2, p1, p2}, LX/HpM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2508788
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2508789
    if-eqz v2, :cond_3

    .line 2508790
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508791
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2508792
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2508793
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2508772
    check-cast p1, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel$Serializer;->a(Lcom/facebook/componentscript/graphql/generated/CSComposerAlbumPickerAlbumsQueryModels$CSComposerAlbumPickerAlbumsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
