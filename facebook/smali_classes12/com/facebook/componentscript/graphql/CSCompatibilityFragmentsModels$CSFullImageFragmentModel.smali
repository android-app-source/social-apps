.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41789764
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:D

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507222
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507202
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2507220
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2507221
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507218
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->f:Ljava/lang/String;

    .line 2507219
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507216
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->h:Ljava/lang/String;

    .line 2507217
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2507223
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507224
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2507225
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2507226
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2507227
    iget v1, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->e:I

    invoke-virtual {p1, v7, v1, v7}, LX/186;->a(III)V

    .line 2507228
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2507229
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->g:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2507230
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2507231
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->i:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 2507232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507233
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2507213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507215
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2507208
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2507209
    invoke-virtual {p1, p2, v4, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->e:I

    .line 2507210
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->g:D

    .line 2507211
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;->i:I

    .line 2507212
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2507205
    new-instance v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;-><init>()V

    .line 2507206
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2507207
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2507204
    const v0, -0x592ecf05

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2507203
    const v0, 0x437b93b

    return v0
.end method
