.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2507536
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2507537
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2507574
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2507538
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2507539
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2507540
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2507541
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2507542
    :goto_0
    move v1, v2

    .line 2507543
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2507544
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2507545
    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;-><init>()V

    .line 2507546
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2507547
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2507548
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2507549
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2507550
    :cond_0
    return-object v1

    .line 2507551
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_7

    .line 2507552
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2507553
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2507554
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v9, :cond_1

    .line 2507555
    const-string p0, "can_viewer_edit"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2507556
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v3

    goto :goto_1

    .line 2507557
    :cond_2
    const-string p0, "icon_image"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2507558
    invoke-static {p1, v0}, LX/Hp9;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2507559
    :cond_3
    const-string p0, "label"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2507560
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2507561
    :cond_4
    const-string p0, "privacy_options"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2507562
    invoke-static {p1, v0}, LX/HpC;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2507563
    :cond_5
    const-string p0, "selectedPrivacyOption"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2507564
    invoke-static {p1, v0}, LX/HpF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2507565
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2507566
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2507567
    if-eqz v1, :cond_8

    .line 2507568
    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 2507569
    :cond_8
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2507570
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2507571
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2507572
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2507573
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
