.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6615acc9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507736
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507737
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2507738
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2507739
    return-void
.end method

.method private a()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507740
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507741
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2507742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507743
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2507744
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2507745
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2507746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507747
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2507748
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507749
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2507750
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507751
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->a()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2507752
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;

    .line 2507753
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;->e:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507754
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507755
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2507756
    new-instance v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel;-><init>()V

    .line 2507757
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2507758
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2507759
    const v0, 0x750e14b7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2507760
    const v0, -0x4844ee20

    return v0
.end method
