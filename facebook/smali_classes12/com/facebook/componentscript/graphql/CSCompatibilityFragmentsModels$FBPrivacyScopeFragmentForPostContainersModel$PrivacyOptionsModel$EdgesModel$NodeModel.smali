.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd959429
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$ExcludedMembersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$IncludedMembersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507726
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2507725
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2507723
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2507724
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507721
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 2507722
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$ExcludedMembersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2507719
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$ExcludedMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    .line 2507720
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507717
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    .line 2507718
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507715
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2507716
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$IncludedMembersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2507713
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$IncludedMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    .line 2507714
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507727
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 2507728
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507711
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    .line 2507712
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2507665
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    .line 2507666
    iget-object v0, p0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2507691
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507692
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2507693
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2507694
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2507695
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2507696
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 2507697
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2507698
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2507699
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->q()LX/0Px;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->c(Ljava/util/List;)I

    move-result v7

    .line 2507700
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2507701
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2507702
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2507703
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2507704
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2507705
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2507706
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2507707
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2507708
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2507709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507710
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2507673
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2507674
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2507675
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2507676
    if-eqz v1, :cond_3

    .line 2507677
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507678
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 2507679
    :goto_0
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2507680
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    .line 2507681
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->l()Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2507682
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507683
    iput-object v0, v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->g:Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$CSFullImageFragmentModel;

    .line 2507684
    :cond_0
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2507685
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2507686
    if-eqz v2, :cond_1

    .line 2507687
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    .line 2507688
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    move-object v1, v0

    .line 2507689
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2507690
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2507672
    invoke-direct {p0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2507669
    new-instance v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$PrivacyOptionsModel$EdgesModel$NodeModel;-><init>()V

    .line 2507670
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2507671
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2507668
    const v0, 0xb9d0e2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2507667
    const v0, -0x7646fe03    # -4.4539E-33f

    return v0
.end method
