.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2507470
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2507471
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2507431
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2507432
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2507433
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x7

    const/4 v3, 0x0

    .line 2507434
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2507435
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2507436
    if-eqz v2, :cond_0

    .line 2507437
    const-string v2, "current_tag_expansion"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507438
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2507439
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507440
    if-eqz v2, :cond_1

    .line 2507441
    const-string v3, "excluded_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507442
    invoke-static {v1, v2, p1, p2}, LX/Hp7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2507443
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507444
    if-eqz v2, :cond_2

    .line 2507445
    const-string v3, "icon_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507446
    invoke-static {v1, v2, p1}, LX/Hp5;->a(LX/15i;ILX/0nX;)V

    .line 2507447
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2507448
    if-eqz v2, :cond_3

    .line 2507449
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507450
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2507451
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507452
    if-eqz v2, :cond_4

    .line 2507453
    const-string v3, "included_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507454
    invoke-static {v1, v2, p1, p2}, LX/Hp8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2507455
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2507456
    if-eqz v2, :cond_5

    .line 2507457
    const-string v3, "legacy_graph_api_privacy_json"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507458
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2507459
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2507460
    if-eqz v2, :cond_6

    .line 2507461
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507462
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2507463
    :cond_6
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2507464
    if-eqz v2, :cond_7

    .line 2507465
    const-string v2, "tag_expansion_options"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507466
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2507467
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2507468
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2507469
    check-cast p1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel$Serializer;->a(Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyOptionFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
