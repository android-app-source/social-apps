.class public final Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2508001
    const-class v0, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;

    new-instance v1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2508002
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2508003
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2507976
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2507977
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2507978
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2507979
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2507980
    if-eqz v2, :cond_0

    .line 2507981
    const-string p0, "can_viewer_edit"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507982
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2507983
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507984
    if-eqz v2, :cond_1

    .line 2507985
    const-string p0, "icon_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507986
    invoke-static {v1, v2, p1}, LX/Hp9;->a(LX/15i;ILX/0nX;)V

    .line 2507987
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2507988
    if-eqz v2, :cond_2

    .line 2507989
    const-string p0, "label"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507990
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2507991
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507992
    if-eqz v2, :cond_3

    .line 2507993
    const-string p0, "privacy_options"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507994
    invoke-static {v1, v2, p1, p2}, LX/HpC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2507995
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2507996
    if-eqz v2, :cond_4

    .line 2507997
    const-string p0, "selectedPrivacyOption"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2507998
    invoke-static {v1, v2, p1, p2}, LX/HpF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2507999
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2508000
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2507975
    check-cast p1, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel$Serializer;->a(Lcom/facebook/componentscript/graphql/CSCompatibilityFragmentsModels$FBPrivacyScopeFragmentForPostContainersModel;LX/0nX;LX/0my;)V

    return-void
.end method
