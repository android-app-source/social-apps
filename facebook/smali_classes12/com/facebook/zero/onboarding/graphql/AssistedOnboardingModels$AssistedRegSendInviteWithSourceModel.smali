.class public final Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaee9268
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2496611
    const-class v0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2496610
    const-class v0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2496608
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2496609
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2496588
    iget-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->e:Ljava/lang/String;

    .line 2496589
    iget-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2496606
    iget-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->f:Ljava/lang/String;

    .line 2496607
    iget-object v0, p0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2496598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2496599
    invoke-direct {p0}, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2496600
    invoke-direct {p0}, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2496601
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2496602
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2496603
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2496604
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2496605
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2496595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2496596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2496597
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2496592
    new-instance v0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;

    invoke-direct {v0}, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;-><init>()V

    .line 2496593
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2496594
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2496591
    const v0, -0x1b8e9810

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2496590
    const v0, -0x35677291    # -4998839.5f

    return v0
.end method
