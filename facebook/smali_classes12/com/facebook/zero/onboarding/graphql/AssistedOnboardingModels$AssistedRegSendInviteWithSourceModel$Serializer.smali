.class public final Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2496571
    const-class v0, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;

    new-instance v1, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2496572
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496573
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2496574
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2496575
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2496576
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2496577
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2496578
    if-eqz p0, :cond_0

    .line 2496579
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496580
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496581
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2496582
    if-eqz p0, :cond_1

    .line 2496583
    const-string p2, "client_subscription_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496584
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496585
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2496586
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2496587
    check-cast p1, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel$Serializer;->a(Lcom/facebook/zero/onboarding/graphql/AssistedOnboardingModels$AssistedRegSendInviteWithSourceModel;LX/0nX;LX/0my;)V

    return-void
.end method
