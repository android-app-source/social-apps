.class public Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496335
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->p:LX/0if;

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2496329
    invoke-virtual {p0}, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2496330
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2496331
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_launch_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2496332
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "amount"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->r:Ljava/lang/String;

    .line 2496333
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->p:LX/0if;

    sget-object v1, LX/0ig;->Y:LX/0ih;

    const-string v2, "top_up_amount"

    iget-object v3, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2496334
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2496312
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2496313
    const v0, 0x7f030123

    invoke-virtual {p0, v0}, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->setContentView(I)V

    .line 2496314
    invoke-static {p0, p0}, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2496315
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->p:LX/0if;

    sget-object v1, LX/0ig;->Y:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2496316
    invoke-direct {p0}, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->b()V

    .line 2496317
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2496318
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f08374e

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2496319
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->r:Ljava/lang/String;

    .line 2496320
    new-instance v1, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;

    invoke-direct {v1}, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;-><init>()V

    .line 2496321
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2496322
    const-string p1, "amount"

    invoke-virtual {v2, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496323
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2496324
    move-object v0, v1

    .line 2496325
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2496326
    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2496327
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2496328
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1e2e2473

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496309
    iget-object v1, p0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->p:LX/0if;

    sget-object v2, LX/0ig;->Y:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2496310
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2496311
    const/16 v1, 0x23

    const v2, -0x2ea88895

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
