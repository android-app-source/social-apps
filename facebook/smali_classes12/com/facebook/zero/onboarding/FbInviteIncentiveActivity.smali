.class public Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Gm2;
.implements LX/F7d;


# instance fields
.field public p:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hi4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496291
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;LX/0tX;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Ot;LX/0if;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;",
            "LX/0tX;",
            "Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;",
            "LX/0Ot",
            "<",
            "LX/Hi4;",
            ">;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2496290
    iput-object p1, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->p:LX/0tX;

    iput-object p2, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->q:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iput-object p3, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    const/16 v3, 0x3915

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-static {p0, v0, v1, v3, v2}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->a(Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;LX/0tX;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Ot;LX/0if;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2496286
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fb_incentive_activity_open"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2496287
    const-string v0, "ccu_enabled"

    iget-object v2, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->q:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2496288
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hi4;

    invoke-virtual {v0, v1}, LX/Hi4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2496289
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/GmD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2496273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->u:Z

    .line 2496274
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->X:LX/0ih;

    const-string v2, "send_invites"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2496275
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GmD;

    .line 2496276
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2496277
    :try_start_0
    const-string v3, "hash"

    invoke-virtual {v0}, LX/GmD;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2496278
    :goto_1
    new-instance v3, LX/4Cz;

    invoke-direct {v3}, LX/4Cz;-><init>()V

    iget-object v4, v0, LX/GmD;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4Cz;->a(Ljava/lang/String;)LX/4Cz;

    move-result-object v3

    iget-object v4, v0, LX/GmD;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4Cz;->b(Ljava/lang/String;)LX/4Cz;

    move-result-object v3

    iget-object v0, v0, LX/GmD;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/4Cz;->c(Ljava/lang/String;)LX/4Cz;

    move-result-object v0

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/4Cz;->a(Ljava/lang/Integer;)LX/4Cz;

    move-result-object v0

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2496279
    const-string v3, "invite_details"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496280
    move-object v0, v0

    .line 2496281
    invoke-static {}, LX/Hi0;->a()LX/Hhz;

    move-result-object v2

    .line 2496282
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2496283
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2496284
    iget-object v2, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->p:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2496285
    :cond_0
    return-void

    :catch_0
    goto :goto_1
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 2496292
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->c(Z)V

    .line 2496293
    iget-object v1, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v2, LX/0ig;->X:LX/0ih;

    const-string v3, "contact_invite_list_open"

    if-eqz p1, :cond_0

    const-string v0, "from_legal"

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2496294
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fb_incentive_start_contact_inviter_fragment"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2496295
    const-string v0, "ccu_enabled"

    iget-object v2, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->q:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v2}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2496296
    const-string v0, "accepted_legal"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2496297
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hi4;

    invoke-virtual {v0, v1}, LX/Hi4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2496298
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2496299
    new-instance v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    invoke-direct {v1}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;-><init>()V

    .line 2496300
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2496301
    const-string v3, "has_title_bar"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2496302
    const-string v3, "title_bar_title_string_id"

    const v4, 0x7f08374e

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2496303
    const-string v3, "analytics_tag"

    const-string v4, "assisted_onboarding"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496304
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2496305
    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2496306
    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2496307
    return-void

    .line 2496308
    :cond_0
    const-string v0, "ccu_enabled"

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 2496267
    if-nez p1, :cond_0

    .line 2496268
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2496269
    :goto_0
    return-void

    .line 2496270
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2496271
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f08374e

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2496272
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Hhp;

    invoke-direct {v1, p0}, LX/Hhp;-><init>(Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2496259
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setVisibility(I)V

    .line 2496260
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f08374e

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2496261
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Hhq;

    invoke-direct {v1, p0}, LX/Hhq;-><init>(Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 2496262
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2496263
    new-instance v1, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    invoke-direct {v1}, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;-><init>()V

    .line 2496264
    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2496265
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2496266
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2496256
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->X:LX/0ih;

    const-string v2, "cc_legal_accepted"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496257
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->b(Z)V

    .line 2496258
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/GmD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2496248
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fb_incentive_finish_contact_inviter_fragment"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2496249
    const-string v0, "num_invites_sent"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2496250
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hi4;

    invoke-virtual {v0, v1}, LX/Hi4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2496251
    invoke-direct {p0, p1}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->b(Ljava/util/List;)V

    .line 2496252
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2496253
    invoke-direct {p0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->l()V

    .line 2496254
    :goto_0
    return-void

    .line 2496255
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->finish()V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2496231
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2496232
    invoke-static {p0, p0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2496233
    const v0, 0x7f030123

    invoke-virtual {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->setContentView(I)V

    .line 2496234
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->X:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2496235
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->t:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2496236
    invoke-direct {p0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->b()V

    .line 2496237
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->q:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a()Z

    move-result v0

    .line 2496238
    iget-object v1, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v2, LX/0ig;->X:LX/0ih;

    const-string v3, "start_funnel"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2496239
    if-nez v0, :cond_0

    .line 2496240
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v1, LX/0ig;->X:LX/0ih;

    const-string v2, "ccu_legal_open"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496241
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->c(Z)V

    .line 2496242
    sget-object v0, LX/89v;->IORG_INCENTIVE_INVITE:LX/89v;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    .line 2496243
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2496244
    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2496245
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2496246
    :goto_0
    return-void

    .line 2496247
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->b(Z)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 2496224
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2496225
    instance-of v1, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    if-eqz v1, :cond_0

    .line 2496226
    check-cast v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2496227
    iget-object v1, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->X:LX/0ih;

    const-string v3, "contact_invite_list_close"

    const-string p0, "device_back"

    invoke-virtual {v1, v2, v3, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2496228
    invoke-static {v0}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->k(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    .line 2496229
    :goto_0
    return-void

    .line 2496230
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x3182fe6b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2496216
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2496217
    instance-of v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->u:Z

    if-nez v2, :cond_0

    .line 2496218
    check-cast v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2496219
    iget-object v2, v0, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->o:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2496220
    invoke-direct {p0, v0}, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->b(Ljava/util/List;)V

    .line 2496221
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;->s:LX/0if;

    sget-object v2, LX/0ig;->X:LX/0ih;

    invoke-virtual {v0, v2}, LX/0if;->c(LX/0ih;)V

    .line 2496222
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2496223
    const/16 v0, 0x23

    const v2, -0x6eb89eac

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
