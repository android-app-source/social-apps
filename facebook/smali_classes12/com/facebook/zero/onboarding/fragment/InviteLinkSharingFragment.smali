.class public Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/34b;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496443
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496444
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2496445
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object p1

    check-cast p1, LX/0if;

    invoke-static {v0}, LX/Hht;->b(LX/0QB;)LX/Hht;

    move-result-object v0

    check-cast v0, LX/Hht;

    iput-object p1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->a:LX/0if;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->b:LX/Hht;

    .line 2496446
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x49c8f24b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496447
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2496448
    if-eqz v1, :cond_0

    .line 2496449
    const-string v2, "amount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->d:Ljava/lang/String;

    .line 2496450
    :goto_0
    const v1, 0x7f030909

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, -0x1a306b10

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 2496451
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->d:Ljava/lang/String;

    .line 2496452
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->a:LX/0if;

    sget-object v2, LX/0ig;->Y:LX/0ih;

    const-string v3, "top_up_value_null"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496453
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2496454
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2496455
    instance-of v1, v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    if-eqz v1, :cond_0

    .line 2496456
    check-cast v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    .line 2496457
    iget-object v1, v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 2496458
    new-instance v1, LX/Hhv;

    invoke-direct {v1, p0}, LX/Hhv;-><init>(Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2496459
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2496460
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2496461
    move-object v1, v1

    .line 2496462
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2496463
    new-instance v1, LX/Hhw;

    invoke-direct {v1, p0}, LX/Hhw;-><init>(Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2496464
    :cond_0
    const v0, 0x7f0d1731

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2496465
    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2496466
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->b:LX/Hht;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->d:Ljava/lang/String;

    const/4 p2, 0x1

    const/4 v6, 0x0

    .line 2496467
    new-instance v7, LX/34b;

    invoke-direct {v7, v2}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2496468
    iput-boolean p2, v7, LX/34b;->d:Z

    .line 2496469
    iput-boolean p2, v7, LX/34b;->i:Z

    .line 2496470
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300fd

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 2496471
    const v4, 0x7f0d058b

    invoke-virtual {v8, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/text/BetterTextView;

    .line 2496472
    const v5, 0x7f0d058c

    invoke-virtual {v8, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/text/BetterTextView;

    .line 2496473
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f083757

    new-array v11, p2, [Ljava/lang/Object;

    invoke-virtual {v1}, LX/Hht;->a()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v11, v6

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 2496474
    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496475
    invoke-virtual {v4, v9}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2496476
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f083758

    new-array v10, p2, [Ljava/lang/Object;

    aput-object v3, v10, v6

    invoke-virtual {v4, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2496477
    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496478
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2496479
    invoke-virtual {v5, v4}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2496480
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b2381

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v7, v8, v4}, LX/34b;->a(Landroid/view/View;F)V

    .line 2496481
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08375f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    .line 2496482
    const v5, 0x7f0208fd

    invoke-virtual {v4, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2496483
    new-instance v5, LX/Hhr;

    invoke-direct {v5, v1, v2}, LX/Hhr;-><init>(LX/Hht;Landroid/content/Context;)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2496484
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 2496485
    sget-object v4, LX/Hht;->a:Landroid/content/Intent;

    invoke-virtual {v8, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    move v5, v6

    .line 2496486
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_3

    .line 2496487
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 2496488
    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v6, :cond_1

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2496489
    :cond_1
    invoke-virtual {v4, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 2496490
    invoke-virtual {v4, v8}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v6, v10}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2496491
    new-instance v10, LX/Hhs;

    invoke-direct {v10, v1, v4, v2, v3}, LX/Hhs;-><init>(LX/Hht;Landroid/content/pm/ResolveInfo;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v6, v10}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2496492
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2496493
    :cond_3
    move-object v1, v7

    .line 2496494
    iput-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->c:LX/34b;

    .line 2496495
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/InviteLinkSharingFragment;->c:LX/34b;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2496496
    return-void
.end method
