.class public Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hi4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Gm9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Hht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbButton;

.field public j:Lcom/facebook/fig/textinput/FigEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496388
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496428
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2496429
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    const/16 v3, 0x3915

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p1}, LX/Gm9;->a(LX/0QB;)LX/Gm9;

    move-result-object v4

    check-cast v4, LX/Gm9;

    invoke-static {p1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-static {p1}, LX/Hht;->b(LX/0QB;)LX/Hht;

    move-result-object v6

    check-cast v6, LX/Hht;

    const/16 v0, 0x12cb

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iput-object v5, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->c:LX/0if;

    iput-object v6, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->d:LX/Hht;

    iput-object p1, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->e:LX/0Or;

    .line 2496430
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2f2285a3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496431
    const v1, 0x7f0306ce

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x35f57bc5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2496389
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2496390
    iget-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->c:LX/0if;

    sget-object v1, LX/0ig;->X:LX/0ih;

    const-string v2, "confirmation_open"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496391
    const v0, 0x7f0d04f7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2496392
    const v0, 0x7f0d0841

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2496393
    const v0, 0x7f0d0842

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2496394
    const v0, 0x7f0d05f0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->i:Lcom/facebook/resources/ui/FbButton;

    .line 2496395
    const v0, 0x7f0d1262

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->j:Lcom/facebook/fig/textinput/FigEditText;

    .line 2496396
    const v0, 0x7f0d1261

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2496397
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object v1, v1, LX/Gm9;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2496398
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2496399
    const-string v1, ""

    .line 2496400
    :goto_0
    move-object v1, v1

    .line 2496401
    iget-object v2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object v2, v2, LX/Gm9;->d:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2496402
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f08374f

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2496403
    :goto_1
    move-object v2, v2

    .line 2496404
    iget-object p1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object p1, p1, LX/Gm9;->e:Ljava/lang/String;

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2496405
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f083750

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2496406
    :goto_2
    move-object p1, p1

    .line 2496407
    iget-object p2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496408
    iget-object p2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p2, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2496409
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496410
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2496411
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496412
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2496413
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/Hhu;

    invoke-direct {v2, p0}, LX/Hhu;-><init>(Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2496414
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    .line 2496415
    iget-boolean v2, v1, LX/Gm9;->f:Z

    move v1, v2

    .line 2496416
    if-eqz v1, :cond_0

    .line 2496417
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setGravity(I)V

    .line 2496418
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 2496419
    iget-object v2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->j:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2, v3}, Lcom/facebook/fig/textinput/FigEditText;->setVisibility(I)V

    .line 2496420
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2496421
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2496422
    iget-object v2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->j:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserPhoneNumber;

    .line 2496423
    iget-object v1, v0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2496424
    invoke-virtual {v2, v0}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2496425
    :cond_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fb_incentive_start_confirmation_fragment"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2496426
    iget-object v0, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hi4;

    invoke-virtual {v0, v1}, LX/Hi4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2496427
    return-void

    :cond_1
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object v1, v1, LX/Gm9;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object v2, v2, LX/Gm9;->d:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    iget-object p1, p0, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    iget-object p1, p1, LX/Gm9;->e:Ljava/lang/String;

    goto/16 :goto_2
.end method
