.class public Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Hht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbButton;

.field public e:Lcom/facebook/fig/textinput/FigEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2496510
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496511
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2496512
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;

    invoke-static {p1}, LX/Hht;->b(LX/0QB;)LX/Hht;

    move-result-object v2

    check-cast v2, LX/Hht;

    invoke-static {p1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    const/16 v0, 0x12cb

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->a:LX/Hht;

    iput-object v3, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->b:LX/0if;

    iput-object p1, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->c:LX/0Or;

    .line 2496513
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x477c4251

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496514
    const v1, 0x7f031515

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x643318bb

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2496515
    const v0, 0x7f0d1262

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->e:Lcom/facebook/fig/textinput/FigEditText;

    .line 2496516
    const v0, 0x7f0d2f99

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2496517
    iget-object v0, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->b:LX/0if;

    sget-object v1, LX/0ig;->Y:LX/0ih;

    const-string v2, "phone_confirmation_view_loaded"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496518
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2496519
    instance-of v1, v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    if-eqz v1, :cond_0

    .line 2496520
    check-cast v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    .line 2496521
    iget-object v1, v0, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 2496522
    new-instance v1, LX/Hhy;

    invoke-direct {v1, p0}, LX/Hhy;-><init>(Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2496523
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2496524
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2496525
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2496526
    iget-object v1, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->e:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserPhoneNumber;

    .line 2496527
    iget-object v2, v0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, v2

    .line 2496528
    invoke-virtual {v1, v0}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2496529
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;->d:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Hhx;

    invoke-direct {v1, p0}, LX/Hhx;-><init>(Lcom/facebook/zero/onboarding/fragment/TopUpPhoneConfirmationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2496530
    return-void
.end method
