.class public Lcom/facebook/zero/iptest/ZeroIPTestBroadcastReceiver;
.super LX/25h;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/HhT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2495569
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ZERO_IP_TEST_ACTION"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, LX/25h;-><init>([Ljava/lang/String;)V

    .line 2495570
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/iptest/ZeroIPTestBroadcastReceiver;

    new-instance v4, LX/HhT;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v1

    check-cast v1, LX/0Sg;

    invoke-static {v0}, LX/HhW;->a(LX/0QB;)LX/HhW;

    move-result-object v2

    check-cast v2, LX/HhW;

    invoke-static {v0}, LX/2Tr;->a(LX/0QB;)LX/2Tr;

    move-result-object v3

    check-cast v3, LX/2Tr;

    const/16 p1, 0x15b2

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {v4, v1, v2, v3, p1}, LX/HhT;-><init>(LX/0Sg;LX/HhW;LX/2Tr;LX/0Or;)V

    move-object v0, v4

    check-cast v0, LX/HhT;

    iput-object v0, p0, Lcom/facebook/zero/iptest/ZeroIPTestBroadcastReceiver;->a:LX/HhT;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2495571
    invoke-static {p0, p1}, Lcom/facebook/zero/iptest/ZeroIPTestBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2495572
    invoke-static {p1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2495573
    iget-object v0, p0, Lcom/facebook/zero/iptest/ZeroIPTestBroadcastReceiver;->a:LX/HhT;

    .line 2495574
    iget-object v1, v0, LX/HhT;->b:LX/0Sg;

    const-string p0, "ZeroIPTestInvoker-invoke"

    new-instance p1, Lcom/facebook/zero/iptest/ZeroIPTestInvoker$1;

    invoke-direct {p1, v0}, Lcom/facebook/zero/iptest/ZeroIPTestInvoker$1;-><init>(LX/HhT;)V

    sget-object p2, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    sget-object p3, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v1, p0, p1, p2, p3}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 2495575
    return-void
.end method
