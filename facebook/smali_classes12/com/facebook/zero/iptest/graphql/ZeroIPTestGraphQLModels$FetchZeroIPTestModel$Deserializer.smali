.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495800
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;

    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2495801
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495802
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2495803
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2495804
    const/4 v2, 0x0

    .line 2495805
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2495806
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495807
    :goto_0
    move v1, v2

    .line 2495808
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2495809
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2495810
    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;-><init>()V

    .line 2495811
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2495812
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2495813
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2495814
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2495815
    :cond_0
    return-object v1

    .line 2495816
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495817
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2495818
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2495819
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495820
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2495821
    const-string v4, "zero_carrier"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2495822
    const/4 v3, 0x0

    .line 2495823
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2495824
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495825
    :goto_2
    move v1, v3

    .line 2495826
    goto :goto_1

    .line 2495827
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2495828
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2495829
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2495830
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495831
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2495832
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2495833
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495834
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2495835
    const-string v5, "ip_tests"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2495836
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2495837
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_e

    .line 2495838
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495839
    :goto_4
    move v1, v4

    .line 2495840
    goto :goto_3

    .line 2495841
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2495842
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2495843
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 2495844
    :cond_9
    const-string p0, "timeout"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2495845
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    .line 2495846
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_c

    .line 2495847
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2495848
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495849
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v8, :cond_a

    .line 2495850
    const-string p0, "nodes"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2495851
    invoke-static {p1, v0}, LX/Hhe;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_5

    .line 2495852
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 2495853
    :cond_c
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2495854
    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 2495855
    if-eqz v1, :cond_d

    .line 2495856
    invoke-virtual {v0, v5, v6, v4}, LX/186;->a(III)V

    .line 2495857
    :cond_d
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_e
    move v1, v4

    move v6, v4

    move v7, v4

    goto :goto_5
.end method
