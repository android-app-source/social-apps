.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2495720
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2495721
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2495722
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2495723
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 2495724
    if-nez p1, :cond_0

    .line 2495725
    :goto_0
    return v0

    .line 2495726
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2495727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2495728
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2495729
    const v2, 0x6273600b

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2495730
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2495731
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2495732
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2495733
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2495734
    const v2, -0x448b7ff

    const/4 v4, 0x0

    .line 2495735
    if-nez v1, :cond_1

    move v3, v4

    .line 2495736
    :goto_1
    move v1, v3

    .line 2495737
    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(III)I

    move-result v2

    .line 2495738
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2495739
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2495740
    invoke-virtual {p3, v7, v2, v0}, LX/186;->a(III)V

    .line 2495741
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2495742
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2495743
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2495744
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2495745
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2495746
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2495747
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2495748
    invoke-virtual {p0, p1, v9, v0}, LX/15i;->a(III)I

    move-result v4

    .line 2495749
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2495750
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2495751
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2495752
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2495753
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2495754
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 2495755
    invoke-virtual {p3, v9, v4, v0}, LX/186;->a(III)V

    .line 2495756
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 2495757
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2495758
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2495759
    const v2, 0xff7ab44

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2495760
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2495761
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2495762
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2495763
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2495764
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2495765
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2495766
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2495767
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2495768
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 2495769
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2495770
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2495771
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2495772
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 2495773
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2495774
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2495775
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x448b7ff -> :sswitch_2
        0xff7ab44 -> :sswitch_4
        0x270cdccb -> :sswitch_3
        0x6273600b -> :sswitch_1
        0x7a5dc145 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2495792
    new-instance v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2495776
    sparse-switch p2, :sswitch_data_0

    .line 2495777
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2495778
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2495779
    const v1, 0x6273600b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2495780
    :goto_0
    :sswitch_1
    return-void

    .line 2495781
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2495782
    const v1, -0x448b7ff

    .line 2495783
    if-eqz v0, :cond_0

    .line 2495784
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2495785
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2495786
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2495787
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2495788
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2495789
    :cond_0
    goto :goto_0

    .line 2495790
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2495791
    const v1, 0xff7ab44

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x448b7ff -> :sswitch_1
        0xff7ab44 -> :sswitch_1
        0x270cdccb -> :sswitch_3
        0x6273600b -> :sswitch_2
        0x7a5dc145 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2495794
    if-eqz p1, :cond_0

    .line 2495795
    invoke-static {p0, p1, p2}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2495796
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    .line 2495797
    if-eq v0, v1, :cond_0

    .line 2495798
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2495799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2495793
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2495713
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2495714
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2495715
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2495716
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2495717
    :cond_0
    iput-object p1, p0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2495718
    iput p2, p0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->b:I

    .line 2495719
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2495694
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2495687
    new-instance v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2495688
    iget v0, p0, LX/1vt;->c:I

    .line 2495689
    move v0, v0

    .line 2495690
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2495691
    iget v0, p0, LX/1vt;->c:I

    .line 2495692
    move v0, v0

    .line 2495693
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2495710
    iget v0, p0, LX/1vt;->b:I

    .line 2495711
    move v0, v0

    .line 2495712
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2495695
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2495696
    move-object v0, v0

    .line 2495697
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2495698
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2495699
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2495700
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2495701
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2495702
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2495703
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2495704
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2495705
    invoke-static {v3, v9, v2}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2495706
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2495707
    iget v0, p0, LX/1vt;->c:I

    .line 2495708
    move v0, v0

    .line 2495709
    return v0
.end method
