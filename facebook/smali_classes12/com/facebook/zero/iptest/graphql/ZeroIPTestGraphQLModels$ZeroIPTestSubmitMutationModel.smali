.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x42eda609
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2496048
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2496047
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2496045
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2496046
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2496043
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2496044
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2496037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2496038
    invoke-direct {p0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x270cdccb

    invoke-static {v1, v0, v2}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2496039
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2496040
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2496041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2496042
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2496027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2496028
    invoke-direct {p0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2496029
    invoke-direct {p0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x270cdccb

    invoke-static {v2, v0, v3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2496030
    invoke-direct {p0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2496031
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    .line 2496032
    iput v3, v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->e:I

    .line 2496033
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2496034
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2496035
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2496036
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2496024
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2496025
    const/4 v0, 0x0

    const v1, 0x270cdccb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;->e:I

    .line 2496026
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2496019
    new-instance v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    invoke-direct {v0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;-><init>()V

    .line 2496020
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2496021
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2496023
    const v0, 0x7542ddbc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2496022
    const v0, 0xf5d1e14

    return v0
.end method
