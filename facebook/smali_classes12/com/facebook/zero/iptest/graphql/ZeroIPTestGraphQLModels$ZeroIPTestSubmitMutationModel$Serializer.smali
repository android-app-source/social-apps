.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495996
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2495997
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495999
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2496000
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2496001
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2496002
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2496003
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2496004
    if-eqz v2, :cond_2

    .line 2496005
    const-string p0, "viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496006
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2496007
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2496008
    if-eqz p0, :cond_1

    .line 2496009
    const-string v0, "zero_carrier"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496010
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2496011
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2496012
    if-eqz v0, :cond_0

    .line 2496013
    const-string v2, "carrier_id"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2496014
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2496015
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2496016
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2496017
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2496018
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2495998
    check-cast p1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Serializer;->a(Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
