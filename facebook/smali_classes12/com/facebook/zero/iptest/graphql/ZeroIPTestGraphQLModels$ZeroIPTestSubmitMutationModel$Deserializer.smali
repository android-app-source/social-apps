.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495942
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2495943
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495944
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2495945
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2495946
    const/4 v2, 0x0

    .line 2495947
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2495948
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495949
    :goto_0
    move v1, v2

    .line 2495950
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2495951
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2495952
    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$ZeroIPTestSubmitMutationModel;-><init>()V

    .line 2495953
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2495954
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2495955
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2495956
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2495957
    :cond_0
    return-object v1

    .line 2495958
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495959
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2495960
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2495961
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495962
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2495963
    const-string v4, "viewer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2495964
    const/4 v3, 0x0

    .line 2495965
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2495966
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495967
    :goto_2
    move v1, v3

    .line 2495968
    goto :goto_1

    .line 2495969
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2495970
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2495971
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2495972
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495973
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2495974
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2495975
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495976
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_6

    if-eqz v4, :cond_6

    .line 2495977
    const-string v5, "zero_carrier"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2495978
    const/4 v4, 0x0

    .line 2495979
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 2495980
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495981
    :goto_4
    move v1, v4

    .line 2495982
    goto :goto_3

    .line 2495983
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2495984
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2495985
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 2495986
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2495987
    :cond_a
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_b

    .line 2495988
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2495989
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2495990
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v5, :cond_a

    .line 2495991
    const-string p0, "carrier_id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2495992
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 2495993
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2495994
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2495995
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_c
    move v1, v4

    goto :goto_5
.end method
