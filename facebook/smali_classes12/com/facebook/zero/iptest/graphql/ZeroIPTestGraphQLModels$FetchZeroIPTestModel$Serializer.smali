.class public final Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495858
    const-class v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;

    new-instance v1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2495859
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495860
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 2495861
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2495862
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2495863
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2495864
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2495865
    if-eqz v2, :cond_9

    .line 2495866
    const-string v3, "zero_carrier"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495867
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2495868
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2495869
    if-eqz v3, :cond_8

    .line 2495870
    const-string v4, "ip_tests"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495871
    const/4 v6, 0x0

    .line 2495872
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2495873
    invoke-virtual {v1, v3, v6}, LX/15i;->g(II)I

    move-result v4

    .line 2495874
    if-eqz v4, :cond_6

    .line 2495875
    const-string v5, "nodes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495876
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2495877
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result p0

    if-ge v5, p0, :cond_5

    .line 2495878
    invoke-virtual {v1, v4, v5}, LX/15i;->q(II)I

    move-result p0

    const/4 p2, 0x0

    .line 2495879
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2495880
    invoke-virtual {v1, p0, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2495881
    if-eqz v0, :cond_0

    .line 2495882
    const-string v2, "expected_rc"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495883
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2495884
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2495885
    if-eqz v0, :cond_1

    .line 2495886
    const-string v2, "ip"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495887
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2495888
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2495889
    if-eqz v0, :cond_2

    .line 2495890
    const-string v2, "path"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495891
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2495892
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {v1, p0, v0, p2}, LX/15i;->a(III)I

    move-result v0

    .line 2495893
    if-eqz v0, :cond_3

    .line 2495894
    const-string v2, "port"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495895
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 2495896
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2495897
    if-eqz v0, :cond_4

    .line 2495898
    const-string v2, "test_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495899
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2495900
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2495901
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2495902
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2495903
    :cond_6
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4, v6}, LX/15i;->a(III)I

    move-result v4

    .line 2495904
    if-eqz v4, :cond_7

    .line 2495905
    const-string v5, "timeout"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2495906
    invoke-virtual {p1, v4}, LX/0nX;->b(I)V

    .line 2495907
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2495908
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2495909
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2495910
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2495911
    check-cast p1, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel$Serializer;->a(Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;LX/0nX;LX/0my;)V

    return-void
.end method
