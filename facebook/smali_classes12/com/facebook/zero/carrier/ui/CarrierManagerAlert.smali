.class public Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/view/View;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2495106
    const-class v0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2495107
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2495108
    const v0, 0x7f030246

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2495109
    const v0, 0x7f0d08c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2495110
    const v0, 0x7f0d08c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->d:Landroid/widget/TextView;

    .line 2495111
    const v0, 0x7f0d08c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->b:Landroid/view/View;

    .line 2495112
    iget-object v0, p0, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->b:Landroid/view/View;

    new-instance v1, LX/HhB;

    invoke-direct {v1, p0}, LX/HhB;-><init>(Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2495113
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b237e

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2495114
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2495115
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a094c

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2495116
    invoke-virtual {p0, v0}, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2495117
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/zero/carrier/ui/CarrierManagerAlert;->setVisibility(I)V

    .line 2495118
    return-void
.end method
