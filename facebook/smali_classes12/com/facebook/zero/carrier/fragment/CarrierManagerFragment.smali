.class public Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:I

.field public B:LX/Hh0;

.field public C:LX/Hh0;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field public d:LX/0Sh;

.field public e:LX/0Xl;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/11S;

.field public h:LX/Hgy;

.field public i:LX/03V;

.field public j:LX/0Zb;

.field public k:LX/7YP;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private p:Landroid/view/View;

.field public q:Landroid/view/ViewGroup;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field public t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field public w:Landroid/widget/LinearLayout;

.field public x:Landroid/view/View;

.field public y:Landroid/view/View;

.field public z:Landroid/widget/ProgressBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2495068
    const-class v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a:Ljava/lang/String;

    .line 2495069
    const-class v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    const-string v1, "zero_carrier_manager"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2495064
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2495065
    new-instance v0, LX/Hh1;

    invoke-direct {v0, p0}, LX/Hh1;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->B:LX/Hh0;

    .line 2495066
    new-instance v0, LX/Hh2;

    invoke-direct {v0, p0}, LX/Hh2;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->C:LX/Hh0;

    .line 2495067
    return-void
.end method

.method private static a(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2494868
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->g:LX/11S;

    sget-object v1, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0x14d1

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11S;

    new-instance v0, LX/Hgy;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v8

    check-cast v8, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object p1

    check-cast p1, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {v0, v7, v8, v9, p1}, LX/Hgy;-><init>(LX/0TD;Lcom/facebook/common/shortcuts/InstallShortcutHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    move-object v7, v0

    check-cast v7, LX/Hgy;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {p0}, LX/7YP;->a(LX/0QB;)LX/7YP;

    move-result-object p0

    check-cast p0, LX/7YP;

    iput-object v2, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->d:LX/0Sh;

    iput-object v4, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->e:LX/0Xl;

    iput-object v5, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->f:LX/0Or;

    iput-object v6, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->g:LX/11S;

    iput-object v7, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->h:LX/Hgy;

    iput-object v8, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->i:LX/03V;

    iput-object v9, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->j:LX/0Zb;

    iput-object p0, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->k:LX/7YP;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;LX/Hh0;Z)V
    .locals 7

    .prologue
    .line 2495060
    invoke-interface {p1}, LX/Hh0;->a()V

    .line 2495061
    iget-object v6, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->k:LX/7YP;

    new-instance v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;

    const/16 v1, 0x19

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/6Y4;->CARRIER_MANAGER:LX/6Y4;

    sget-object v5, LX/0yY;->CARRIER_MANAGER:LX/0yY;

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;-><init>(ILjava/lang/String;ZLX/6Y4;LX/0yY;)V

    invoke-virtual {v6, v0}, LX/7YP;->a(Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2495062
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->d:LX/0Sh;

    new-instance v2, LX/Hh4;

    invoke-direct {v2, p0, p1}, LX/Hh4;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;LX/Hh0;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2495063
    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;)V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v7, 0x2

    const/16 v5, 0x8

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 2494910
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2494911
    if-eqz v0, :cond_0

    .line 2494912
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->w:Ljava/lang/String;

    move-object v1, v1

    .line 2494913
    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2494914
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->y:Z

    move v0, v0

    .line 2494915
    if-eqz v0, :cond_1

    .line 2494916
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->h:LX/Hgy;

    .line 2494917
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->A:Ljava/lang/String;

    move-object v1, v1

    .line 2494918
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->z:Ljava/lang/String;

    move-object v2, v2

    .line 2494919
    new-instance v3, LX/Hh5;

    invoke-direct {v3, p0, p1}, LX/Hh5;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;)V

    .line 2494920
    iget-object v6, v0, LX/Hgy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/0df;->E:LX/0Tn;

    const/4 v12, 0x1

    invoke-interface {v6, v8, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    move v6, v6

    .line 2494921
    if-nez v6, :cond_c

    .line 2494922
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2494923
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v7, [I

    .line 2494924
    iget v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->t:I

    move v3, v3

    .line 2494925
    aput v3, v2, v4

    .line 2494926
    iget v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->u:I

    move v3, v3

    .line 2494927
    aput v3, v2, v9

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 2494928
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 2494929
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2494930
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->s:Ljava/lang/String;

    move-object v0, v0

    .line 2494931
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2494932
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2494933
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->s:Ljava/lang/String;

    move-object v1, v1

    .line 2494934
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2494935
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2494936
    :goto_1
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    move-object v0, v0

    .line 2494937
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2494938
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2494939
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->r:Landroid/widget/TextView;

    .line 2494940
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    move-object v1, v1

    .line 2494941
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2494942
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->r:Landroid/widget/TextView;

    .line 2494943
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->m:Ljava/lang/String;

    move-object v1, v1

    .line 2494944
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2494945
    :goto_2
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2494946
    const v0, 0x7f083743

    new-array v1, v9, [Ljava/lang/Object;

    .line 2494947
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->n:Ljava/lang/String;

    move-object v2, v2

    .line 2494948
    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2494949
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2494950
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2494951
    iget-wide v12, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->o:J

    move-wide v0, v12

    .line 2494952
    cmp-long v0, v0, v10

    if-eqz v0, :cond_5

    .line 2494953
    const v0, 0x7f083744

    new-array v1, v9, [Ljava/lang/Object;

    .line 2494954
    iget-wide v12, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->o:J

    move-wide v2, v12

    .line 2494955
    invoke-static {p0, v2, v3}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2494956
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2494957
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2494958
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2494959
    :goto_3
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->u:Landroid/widget/TextView;

    .line 2494960
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->r:Ljava/lang/String;

    move-object v1, v1

    .line 2494961
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2494962
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->u:Landroid/widget/TextView;

    .line 2494963
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->r:Ljava/lang/String;

    move-object v1, v1

    .line 2494964
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2494965
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2494966
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    move-object v0, v0

    .line 2494967
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2494968
    new-instance v6, LX/HhE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, LX/HhE;-><init>(Landroid/content/Context;)V

    .line 2494969
    iget-wide v12, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->q:J

    move-wide v0, v12

    .line 2494970
    cmp-long v0, v0, v10

    if-gtz v0, :cond_6

    .line 2494971
    new-instance v0, LX/Hh9;

    const v1, 0x7f02039d

    .line 2494972
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    move-object v2, v2

    .line 2494973
    const v3, 0x7f083745

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v9, [LX/HhA;

    new-instance v7, LX/HhA;

    const v8, 0x7f083746

    invoke-virtual {p0, v8}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2494974
    iget-object v10, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->p:Ljava/lang/String;

    move-object v10, v10

    .line 2494975
    invoke-direct {v7, v8, v10}, LX/HhA;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v7, v5, v4

    invoke-direct/range {v0 .. v5}, LX/Hh9;-><init>(ILjava/lang/String;Ljava/lang/String;Z[LX/HhA;)V

    invoke-virtual {v6, v0}, LX/HhE;->a(LX/Hh9;)V

    .line 2494976
    :goto_4
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2494977
    :cond_2
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->v:LX/0Px;

    move-object v2, v0

    .line 2494978
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v4

    :goto_5
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 2494979
    new-instance v11, LX/HhE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v11, v5}, LX/HhE;-><init>(Landroid/content/Context;)V

    .line 2494980
    new-instance v5, LX/Hh9;

    invoke-static {v0, v9}, LX/HhD;->a(Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Z)I

    move-result v6

    .line 2494981
    iget-object v7, v0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2494982
    const v0, 0x7f083748

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v10, v4, [LX/HhA;

    invoke-direct/range {v5 .. v10}, LX/Hh9;-><init>(ILjava/lang/String;Ljava/lang/String;Z[LX/HhA;)V

    invoke-virtual {v11, v5}, LX/HhE;->a(LX/Hh9;)V

    .line 2494983
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2494984
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2494985
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 2494986
    :cond_4
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2494987
    :cond_5
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2494988
    :cond_6
    new-instance v0, LX/Hh9;

    const v1, 0x7f02039d

    .line 2494989
    iget-object v2, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    move-object v2, v2

    .line 2494990
    const v3, 0x7f083745

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v7, [LX/HhA;

    new-instance v7, LX/HhA;

    const v8, 0x7f083746

    invoke-virtual {p0, v8}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2494991
    iget-object v10, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->p:Ljava/lang/String;

    move-object v10, v10

    .line 2494992
    invoke-direct {v7, v8, v10}, LX/HhA;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v7, v5, v4

    new-instance v7, LX/HhA;

    const v8, 0x7f083747

    invoke-virtual {p0, v8}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2494993
    iget-wide v12, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->q:J

    move-wide v10, v12

    .line 2494994
    invoke-static {p0, v10, v11}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v10}, LX/HhA;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v7, v5, v9

    invoke-direct/range {v0 .. v5}, LX/Hh9;-><init>(ILjava/lang/String;Ljava/lang/String;Z[LX/HhA;)V

    invoke-virtual {v6, v0}, LX/HhE;->a(LX/Hh9;)V

    goto/16 :goto_4

    .line 2494995
    :cond_7
    new-instance v6, LX/HhE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, LX/HhE;-><init>(Landroid/content/Context;)V

    .line 2494996
    new-instance v0, LX/Hh9;

    const v1, 0x7f020704

    const v2, 0x7f083749

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2494997
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->x:Ljava/lang/String;

    move-object v3, v3

    .line 2494998
    new-array v5, v4, [LX/HhA;

    invoke-direct/range {v0 .. v5}, LX/Hh9;-><init>(ILjava/lang/String;Ljava/lang/String;Z[LX/HhA;)V

    invoke-virtual {v6, v0}, LX/HhE;->a(LX/Hh9;)V

    .line 2494999
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2495000
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v0, v0

    .line 2495001
    const/4 v2, 0x0

    .line 2495002
    if-eqz v0, :cond_8

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_8
    move v1, v2

    .line 2495003
    :goto_6
    move v0, v1

    .line 2495004
    if-eqz v0, :cond_b

    .line 2495005
    new-instance v0, LX/HhG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HhG;-><init>(Landroid/content/Context;)V

    .line 2495006
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v1, v1

    .line 2495007
    new-instance v2, LX/Hh6;

    invoke-direct {v2, p0}, LX/Hh6;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V

    .line 2495008
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 2495009
    iget-boolean v5, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    move v5, v5

    .line 2495010
    if-nez v5, :cond_9

    .line 2495011
    new-instance v5, LX/HhF;

    invoke-virtual {v0}, LX/HhG;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, LX/HhF;-><init>(Landroid/content/Context;)V

    .line 2495012
    const/16 v9, 0x64

    const/4 v8, 0x0

    .line 2495013
    iget-object v6, v5, LX/HhF;->d:Landroid/widget/ImageView;

    invoke-static {v3, v8}, LX/HhD;->a(Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Z)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2495014
    iget-boolean v6, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->g:Z

    move v6, v6

    .line 2495015
    if-eqz v6, :cond_11

    .line 2495016
    iget-object v6, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2495017
    :goto_8
    move-object v6, v6

    .line 2495018
    iget-object v7, v5, LX/HhF;->a:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2495019
    iget-object v7, v5, LX/HhF;->a:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495020
    iget-object v6, v5, LX/HhF;->c:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2495021
    iget-object v6, v5, LX/HhF;->c:Landroid/view/View;

    invoke-virtual {v6, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2495022
    iget-boolean v6, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->j:Z

    move v6, v6

    .line 2495023
    if-nez v6, :cond_10

    .line 2495024
    iget-object v6, v5, LX/HhF;->c:Landroid/view/View;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2495025
    iget-object v6, v5, LX/HhF;->d:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 2495026
    iget-object v6, v5, LX/HhF;->a:Landroid/widget/TextView;

    invoke-static {v9, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2495027
    iget-object v6, v5, LX/HhF;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2495028
    iget-object v6, v5, LX/HhF;->b:Landroid/widget/TextView;

    invoke-static {v9, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2495029
    iget-object v6, v5, LX/HhF;->b:Landroid/widget/TextView;

    const v7, 0x7f08374b

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 2495030
    iget-object v6, v5, LX/HhF;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, LX/HhF;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f08374b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495031
    :goto_9
    iget-object v3, v0, LX/HhG;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, LX/HhG;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, LX/HhD;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2495032
    iget-object v3, v0, LX/HhG;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_7

    .line 2495033
    :cond_a
    iget-object v1, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2495034
    :cond_b
    return-void

    .line 2495035
    :cond_c
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v6

    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v8, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2495036
    iput-object v8, v6, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2495037
    move-object v6, v6

    .line 2495038
    new-instance v8, LX/Hgw;

    invoke-direct {v8, v0}, LX/Hgw;-><init>(LX/Hgy;)V

    .line 2495039
    iput-object v8, v6, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2495040
    move-object v6, v6

    .line 2495041
    invoke-virtual {v6}, LX/15E;->a()LX/15D;

    move-result-object v6

    .line 2495042
    iget-object v8, v0, LX/Hgy;->d:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v8, v6}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v6

    .line 2495043
    iget-object v8, v6, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v6, v8

    .line 2495044
    new-instance v8, LX/Hgx;

    invoke-direct {v8, v0, v1}, LX/Hgx;-><init>(LX/Hgy;Ljava/lang/String;)V

    iget-object v12, v0, LX/Hgy;->a:LX/0TD;

    invoke-static {v6, v8, v12}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2495045
    iget-object v8, v0, LX/Hgy;->a:LX/0TD;

    invoke-static {v6, v3, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 2495046
    :cond_d
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_a
    if-ge v3, v4, :cond_f

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 2495047
    iget-boolean v5, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    move v1, v5

    .line 2495048
    if-nez v1, :cond_e

    .line 2495049
    const/4 v1, 0x1

    goto/16 :goto_6

    .line 2495050
    :cond_e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_a

    :cond_f
    move v1, v2

    .line 2495051
    goto/16 :goto_6

    .line 2495052
    :cond_10
    iget-object v6, v5, LX/HhF;->c:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2495053
    iget-object v6, v5, LX/HhF;->d:Landroid/widget/ImageView;

    const/16 v7, 0xff

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 2495054
    iget-object v6, v5, LX/HhF;->a:Landroid/widget/TextView;

    const/high16 v7, -0x1000000

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2495055
    iget-object v6, v5, LX/HhF;->b:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9

    :cond_11
    invoke-virtual {v5}, LX/HhF;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f083740

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 2495056
    iget-object v1, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2495057
    aput-object v1, v10, v11

    const/4 v11, 0x1

    .line 2495058
    iget-object v1, v3, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2495059
    aput-object v1, v10, v11

    invoke-virtual {v6, v7, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_8
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2494909
    const-string v0, "zero_carrier_manager"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2494896
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2494897
    const-class v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-static {v0, p0}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2494898
    if-eqz p1, :cond_0

    .line 2494899
    :goto_0
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->j:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "carrier_manager_impression"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a()Ljava/lang/String;

    move-result-object v2

    .line 2494900
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2494901
    move-object v1, v1

    .line 2494902
    const-string v2, "ref"

    const-string v3, "ref"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2494903
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2494904
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->e:LX/0Xl;

    const-string v1, "com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2494905
    :goto_1
    return-void

    .line 2494906
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 2494907
    goto :goto_0

    .line 2494908
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->e:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->CARRIER_MANAGER:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2494892
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2494893
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->e:LX/0Xl;

    const-string v1, "com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2494894
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->C:LX/Hh0;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a$redex0(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;LX/Hh0;Z)V

    .line 2494895
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x6da090e7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2494874
    const v0, 0x7f030245

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    .line 2494875
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08b7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->l:Landroid/view/View;

    .line 2494876
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d04de

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->m:Landroid/view/View;

    .line 2494877
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08bb

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->n:Landroid/view/View;

    .line 2494878
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08bc

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2494879
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08bd

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->r:Landroid/widget/TextView;

    .line 2494880
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08be

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->p:Landroid/view/View;

    .line 2494881
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08bf

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->s:Landroid/widget/TextView;

    .line 2494882
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08c2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    .line 2494883
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08c1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->u:Landroid/widget/TextView;

    .line 2494884
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08c3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->y:Landroid/view/View;

    .line 2494885
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08c4

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->z:Landroid/widget/ProgressBar;

    .line 2494886
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08c5

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->v:Landroid/view/View;

    .line 2494887
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d08ba

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    .line 2494888
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->A:I

    .line 2494889
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->y:Landroid/view/View;

    new-instance v2, LX/Hh3;

    invoke-direct {v2, p0}, LX/Hh3;-><init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2494890
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->B:LX/Hh0;

    invoke-static {p0, v0, v3}, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->a$redex0(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;LX/Hh0;Z)V

    .line 2494891
    iget-object v0, p0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    const/16 v2, 0x2b

    const v3, 0x14118748

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c4a794b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2494869
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2494870
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2494871
    if-eqz v0, :cond_0

    .line 2494872
    const v2, 0x7f08373f

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2494873
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x4ff759eb

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
