.class public final Lcom/facebook/zero/settings/FlexSettingsActivity$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/zero/settings/FlexSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/settings/FlexSettingsActivity;Z)V
    .locals 0

    .prologue
    .line 2666128
    iput-object p1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iput-boolean p2, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2666129
    iget-boolean v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->a:Z

    if-nez v0, :cond_1

    .line 2666130
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v0

    .line 2666131
    if-eqz v0, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 2666132
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2666133
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    invoke-virtual {v0}, LX/120;->c()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b()V

    .line 2666134
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity$6;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->Q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2666135
    return-void
.end method
