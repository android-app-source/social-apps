.class public Lcom/facebook/zero/settings/FlexSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0yL;


# instance fields
.field public A:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private D:LX/12I;

.field public E:Z

.field public F:Landroid/widget/LinearLayout;

.field private G:Lcom/facebook/resources/ui/FbTextView;

.field private H:Lcom/facebook/fbui/glyph/GlyphView;

.field public I:Landroid/view/View;

.field private J:Lcom/facebook/resources/ui/FbTextView;

.field private K:Lcom/facebook/resources/ui/FbTextView;

.field private L:Landroid/widget/ImageView;

.field private M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

.field private N:Lcom/facebook/resources/ui/FbTextView;

.field private O:Lcom/facebook/resources/ui/FbTextView;

.field private P:Lcom/facebook/resources/ui/FbButton;

.field public Q:Landroid/view/View;

.field private R:Ljava/lang/String;

.field public p:LX/64L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11u;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/120;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2666337
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/settings/FlexSettingsActivity;LX/64L;LX/0yP;LX/0yI;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Ot;LX/0Sh;LX/0Ot;LX/0Ot;LX/0Ot;LX/120;LX/0yH;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/settings/FlexSettingsActivity;",
            "LX/64L;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/0yI;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11u;",
            ">;",
            "LX/120;",
            "LX/0yH;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2666180
    iput-object p1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->p:LX/64L;

    iput-object p2, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->q:LX/0yP;

    iput-object p3, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->r:LX/0yI;

    iput-object p4, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->t:LX/17Y;

    iput-object p6, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->v:LX/0Sh;

    iput-object p8, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->w:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->x:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->y:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    iput-object p12, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->A:LX/0yH;

    iput-object p13, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->B:LX/0Or;

    iput-object p14, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/settings/FlexSettingsActivity;

    invoke-static {v14}, LX/64L;->b(LX/0QB;)LX/64L;

    move-result-object v1

    check-cast v1, LX/64L;

    invoke-static {v14}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v2

    check-cast v2, LX/0yP;

    invoke-static {v14}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v3

    check-cast v3, LX/0yI;

    invoke-static {v14}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v14}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    const/16 v6, 0x4e0

    invoke-static {v14, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v14}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    const/16 v8, 0x1646

    invoke-static {v14, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1c6

    invoke-static {v14, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2e8

    invoke-static {v14, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v14}, LX/120;->a(LX/0QB;)LX/120;

    move-result-object v11

    check-cast v11, LX/120;

    invoke-static {v14}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v12

    check-cast v12, LX/0yH;

    const/16 v13, 0x38b

    invoke-static {v14, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v14}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v14

    check-cast v14, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v14}, Lcom/facebook/zero/settings/FlexSettingsActivity;->a(Lcom/facebook/zero/settings/FlexSettingsActivity;LX/64L;LX/0yP;LX/0yI;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Ot;LX/0Sh;LX/0Ot;LX/0Ot;LX/0Ot;LX/120;LX/0yH;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/settings/FlexSettingsActivity;LX/0yY;)V
    .locals 2

    .prologue
    .line 2666330
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    .line 2666331
    iget-boolean v1, v0, LX/120;->y:Z

    move v0, v1

    .line 2666332
    if-eqz v0, :cond_1

    .line 2666333
    :cond_0
    :goto_0
    return-void

    .line 2666334
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/zero/settings/FlexSettingsActivity;->b(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2666335
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2666336
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    invoke-virtual {v0, v1}, LX/11u;->a(LX/120;)V

    goto :goto_0
.end method

.method private b(LX/0yY;)Z
    .locals 2

    .prologue
    .line 2666326
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->A:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    .line 2666327
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2666328
    :cond_0
    const/4 v0, 0x1

    .line 2666329
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2666304
    const v0, 0x7f030670

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->setContentView(I)V

    .line 2666305
    const v0, 0x7f0d11b6

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2666306
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->r:LX/0yI;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v1

    sget-object v2, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->E:Z

    .line 2666307
    const v1, 0x7f0d11b7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->F:Landroid/widget/LinearLayout;

    .line 2666308
    const v1, 0x7f0d11b8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->G:Lcom/facebook/resources/ui/FbTextView;

    .line 2666309
    const v1, 0x7f0d11b9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->H:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2666310
    const v1, 0x7f0d11ba

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->I:Landroid/view/View;

    .line 2666311
    const v1, 0x7f0d11bb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    .line 2666312
    const v1, 0x7f0d11bc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    .line 2666313
    const v1, 0x7f0d11bd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->L:Landroid/widget/ImageView;

    .line 2666314
    const v1, 0x7f0d11be

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    .line 2666315
    const v1, 0x7f0d11bf

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    .line 2666316
    const v1, 0x7f0d11c0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->O:Lcom/facebook/resources/ui/FbTextView;

    .line 2666317
    const v1, 0x7f0d11c1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    .line 2666318
    const v0, 0x7f0d051d

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->Q:Landroid/view/View;

    .line 2666319
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->H:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/JEZ;

    invoke-direct {v1, p0}, LX/JEZ;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2666320
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->O:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/JEa;

    invoke-direct {v1, p0}, LX/JEa;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2666321
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/JEb;

    invoke-direct {v1, p0}, LX/JEb;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2666322
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->j:LX/0Tn;

    invoke-virtual {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080e8c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->R:Ljava/lang/String;

    .line 2666323
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    const/4 v1, 0x1

    .line 2666324
    iput-boolean v1, v0, LX/120;->a:Z

    .line 2666325
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2666297
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2666298
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2666299
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2666300
    new-instance v1, LX/JEc;

    invoke-direct {v1, p0}, LX/JEc;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2666301
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-virtual {v1}, LX/0yc;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f080e70

    :goto_0
    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2666302
    return-void

    .line 2666303
    :cond_0
    const v1, 0x7f080e6f

    goto :goto_0
.end method

.method public static n(Lcom/facebook/zero/settings/FlexSettingsActivity;)V
    .locals 3

    .prologue
    .line 2666286
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2666287
    const-string v0, "ref"

    const-string v1, "dialtone_settings_screen"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2666288
    iget-boolean v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->E:Z

    if-eqz v0, :cond_0

    const-string v0, "dialtone://switch_to_dialtone"

    .line 2666289
    :goto_0
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->t:LX/17Y;

    invoke-interface {v1, p0, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2666290
    if-nez v1, :cond_1

    .line 2666291
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2666292
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-object v0, v1

    .line 2666293
    :goto_1
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2666294
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2666295
    return-void

    .line 2666296
    :cond_0
    const-string v0, "dialtone://switch_to_full_fb"

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static o(Lcom/facebook/zero/settings/FlexSettingsActivity;)V
    .locals 5

    .prologue
    .line 2666284
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/zero/settings/FlexSettingsActivity$8;

    invoke-direct {v1, p0}, Lcom/facebook/zero/settings/FlexSettingsActivity$8;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    const-wide/16 v2, 0x3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2666285
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 2666280
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2666281
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    const v0, 0x7f0d0c8b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v1, v0}, LX/120;->a(Landroid/view/ViewStub;)V

    .line 2666282
    :cond_0
    new-instance v0, LX/JEf;

    invoke-direct {v0, p0}, LX/JEf;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->D:LX/12I;

    .line 2666283
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 2666215
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->Q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2666216
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->v:LX/0Sh;

    new-instance v1, Lcom/facebook/zero/settings/FlexSettingsActivity$5;

    invoke-direct {v1, p0}, Lcom/facebook/zero/settings/FlexSettingsActivity$5;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2666217
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 2666218
    const v0, 0x7f080e6e

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2666219
    const v0, 0x7f080e73

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2666220
    const v0, 0x7f080e6d

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2666221
    const v0, 0x7f080e76

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 2666222
    const v0, 0x7f080e77

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 2666223
    const v0, 0x7f080e75

    new-array v1, v13, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->R:Ljava/lang/String;

    aput-object v2, v1, v11

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 2666224
    const v0, 0x7f080e78

    new-array v1, v13, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->R:Ljava/lang/String;

    aput-object v9, v1, v11

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    .line 2666225
    const v0, 0x7f080e79

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    .line 2666226
    const v0, 0x7f080e7e

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2666227
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2666228
    const v0, 0x7f080e71

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2666229
    const v0, 0x7f080e74

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2666230
    const v0, 0x7f080e72

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2666231
    const v0, 0x7f080e7a

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 2666232
    const v0, 0x7f080e7c

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 2666233
    const v0, 0x7f080e7b

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 2666234
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666235
    const v0, 0x7f080e7f

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2666236
    :goto_0
    iget-boolean v7, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->E:Z

    if-eqz v7, :cond_3

    .line 2666237
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2666238
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666239
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666240
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666241
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666242
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666243
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v9}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666244
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    .line 2666245
    iput-boolean v13, v0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    .line 2666246
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    const v1, 0x7f020b57

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2666247
    :goto_1
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2666248
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2666249
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    invoke-virtual {v0, v12}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->setVisibility(I)V

    .line 2666250
    :goto_2
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2666251
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->O:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v11}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666252
    :goto_3
    return-void

    .line 2666253
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666254
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666255
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666256
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666257
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v10}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666258
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v10}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666259
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    .line 2666260
    iput-boolean v11, v0, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->b:Z

    .line 2666261
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    const v1, 0x7f02039e

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2666262
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2666263
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    invoke-virtual {v0, v11}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->setVisibility(I)V

    goto :goto_2

    .line 2666264
    :cond_3
    const v1, 0x7f080e6c

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->R:Ljava/lang/String;

    aput-object v4, v3, v11

    invoke-virtual {v8, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 2666265
    iget-object v3, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666266
    iget-object v3, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666267
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->F:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2666268
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->I:Landroid/view/View;

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2666269
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666270
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666271
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666272
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->K:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666273
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->L:Landroid/widget/ImageView;

    invoke-virtual {v1, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2666274
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->M:Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;

    invoke-virtual {v1, v12}, Lcom/facebook/dialtone/ui/LightswitchPhoneImageWithText;->setVisibility(I)V

    .line 2666275
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2666276
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2666277
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->P:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v11}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2666278
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->O:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666279
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->N:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v12}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_4
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2666208
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2666209
    invoke-static {p0, p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2666210
    invoke-direct {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->l()V

    .line 2666211
    invoke-direct {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->m()V

    .line 2666212
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->b()V

    .line 2666213
    invoke-direct {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->p()V

    .line 2666214
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 2666201
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    const/4 v1, 0x1

    .line 2666202
    iput-boolean v1, v0, LX/120;->y:Z

    .line 2666203
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->v:LX/0Sh;

    new-instance v1, Lcom/facebook/zero/settings/FlexSettingsActivity$6;

    invoke-direct {v1, p0, p1}, Lcom/facebook/zero/settings/FlexSettingsActivity$6;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;Z)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2666204
    new-instance v0, LX/JEe;

    invoke-direct {v0, p0, p1}, LX/JEe;-><init>(Lcom/facebook/zero/settings/FlexSettingsActivity;Z)V

    .line 2666205
    iget-object v1, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->p:LX/64L;

    invoke-virtual {v1, v0}, LX/64L;->a(LX/64J;)V

    .line 2666206
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->p:LX/64L;

    invoke-virtual {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "bookmark"

    const-string v3, "update_optin_state"

    invoke-virtual {v0, v1, v2, v3}, LX/64L;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2666207
    return-void
.end method

.method public final e_(Z)V
    .locals 0

    .prologue
    .line 2666198
    if-nez p1, :cond_0

    .line 2666199
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->finish()V

    .line 2666200
    :cond_0
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2666197
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x22

    const v1, -0x834bd3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2666189
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2666190
    invoke-virtual {p0, v2, v2}, Lcom/facebook/zero/settings/FlexSettingsActivity;->overridePendingTransition(II)V

    .line 2666191
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    invoke-virtual {v0}, LX/120;->b()V

    .line 2666192
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    iget-object v2, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->D:LX/12I;

    invoke-virtual {v0, v2}, LX/120;->b(LX/12I;)V

    .line 2666193
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->A:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2666194
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->e()V

    .line 2666195
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2666196
    const/16 v0, 0x23

    const v2, 0x4a33db41    # 2946768.2f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x22

    const v1, -0x6327b591

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2666181
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2666182
    invoke-virtual {p0, v2, v2}, Lcom/facebook/zero/settings/FlexSettingsActivity;->overridePendingTransition(II)V

    .line 2666183
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    iget-object v2, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->D:LX/12I;

    invoke-virtual {v0, v2}, LX/120;->a(LX/12I;)V

    .line 2666184
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    invoke-virtual {v0}, LX/120;->a()V

    .line 2666185
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->A:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2666186
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->c()V

    .line 2666187
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/settings/FlexSettingsActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2666188
    const/16 v0, 0x23

    const v2, -0x13ad25c5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
