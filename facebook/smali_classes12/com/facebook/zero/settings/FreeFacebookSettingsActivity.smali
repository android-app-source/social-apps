.class public Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:Landroid/widget/ArrayAdapter;

.field private B:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/4pL;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/64L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/FbZeroRequestHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0yU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Landroid/view/View;

.field public z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2666437
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2666438
    return-void
.end method

.method private a(LX/JEn;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2666505
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->s:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2666506
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->t:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2666507
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2666508
    invoke-virtual {p0, v0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2666509
    :goto_0
    return-object v0

    .line 2666510
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2666511
    new-instance v1, LX/JEg;

    invoke-direct {v1, p0, p1}, LX/JEg;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/JEn;)V

    .line 2666512
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/service/FbZeroRequestHandler;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2666513
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080e6b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2666503
    const v0, 0x7f0306cd

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->setContentView(I)V

    .line 2666504
    return-void
.end method

.method private static a(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/64L;LX/0yP;LX/0Ot;LX/0yI;LX/0Sh;LX/0yU;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;",
            "LX/64L;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/FbZeroRequestHandler;",
            ">;",
            "LX/0yI;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0yU;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2666502
    iput-object p1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->p:LX/64L;

    iput-object p2, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->q:LX/0yP;

    iput-object p3, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->s:LX/0yI;

    iput-object p5, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->t:LX/0Sh;

    iput-object p6, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->u:LX/0yU;

    iput-object p7, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->v:Lcom/facebook/content/SecureContextHelper;

    iput-object p8, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->x:LX/17Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    invoke-static {v9}, LX/64L;->b(LX/0QB;)LX/64L;

    move-result-object v1

    check-cast v1, LX/64L;

    invoke-static {v9}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v2

    check-cast v2, LX/0yP;

    const/16 v3, 0x13f1

    invoke-static {v9, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v9}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v4

    check-cast v4, LX/0yI;

    invoke-static {v9}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v9}, LX/0yU;->b(LX/0QB;)LX/0yU;

    move-result-object v6

    check-cast v6, LX/0yU;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v9}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v9}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v9

    check-cast v9, LX/17Y;

    invoke-static/range {v0 .. v9}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/64L;LX/0yP;LX/0Ot;LX/0yI;LX/0Sh;LX/0yU;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17Y;)V

    return-void
.end method

.method private b(Z)LX/JEn;
    .locals 6

    .prologue
    .line 2666496
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2666497
    new-instance v0, LX/JEn;

    const v1, 0x7f080e6a

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080e6a

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080e6b

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f080e6b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v5, p1

    invoke-direct/range {v0 .. v5}, LX/JEn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2666498
    invoke-direct {p0, v0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a(LX/JEn;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/JEn;->d:Ljava/lang/CharSequence;

    .line 2666499
    invoke-direct {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, LX/JEn;->e:Landroid/net/Uri;

    .line 2666500
    new-instance v1, LX/JEi;

    invoke-direct {v1, p0, v0}, LX/JEi;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/JEn;)V

    iput-object v1, v0, LX/JEn;->j:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 2666501
    return-object v0
.end method

.method private b()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2666494
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->u:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2666495
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2666514
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->t:LX/0Sh;

    new-instance v1, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity$2;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2666515
    return-void
.end method

.method public static c(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Z)V
    .locals 3

    .prologue
    .line 2666483
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2666484
    const-string v0, "ref"

    const-string v1, "dialtone_settings_screen"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2666485
    if-eqz p1, :cond_0

    const-string v0, "dialtone://switch_to_dialtone"

    .line 2666486
    :goto_0
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->x:LX/17Y;

    invoke-interface {v1, p0, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2666487
    if-nez v1, :cond_1

    .line 2666488
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2666489
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-object v0, v1

    .line 2666490
    :goto_1
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2666491
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->v:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2666492
    return-void

    .line 2666493
    :cond_0
    const-string v0, "dialtone://switch_to_full_fb"

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2666477
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2666478
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2666479
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2666480
    new-instance v1, LX/JEh;

    invoke-direct {v1, p0}, LX/JEh;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2666481
    const v1, 0x7f08065a

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2666482
    return-void
.end method

.method private m()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/JEn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2666466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2666467
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->u:LX/0yU;

    sget-object v2, LX/0yh;->DIALTONE:LX/0yh;

    invoke-virtual {v2}, LX/0yh;->getBaseToken()LX/0yi;

    move-result-object v2

    invoke-virtual {v2}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v1

    .line 2666468
    sget-object v2, LX/0yY;->DIALTONE_TOGGLE_BOOKMARK:LX/0yY;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2666469
    sget-object v2, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2666470
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->z:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 2666471
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2666472
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b(Z)LX/JEn;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2666473
    :cond_1
    :goto_0
    return-object v0

    .line 2666474
    :cond_2
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->z:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    .line 2666475
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->z:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2666476
    :cond_3
    invoke-direct {p0, v3}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->b(Z)LX/JEn;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2666456
    invoke-virtual {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2666457
    new-instance v1, LX/47x;

    invoke-direct {v1, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2666458
    invoke-virtual {v1, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2666459
    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2666460
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2666461
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00fc

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2666462
    const/16 v0, 0x21

    invoke-virtual {v1, v2, v0}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 2666463
    invoke-virtual {v1, p2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2666464
    invoke-virtual {v1}, LX/47x;->a()LX/47x;

    .line 2666465
    :cond_0
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/JEn;Z)V
    .locals 4

    .prologue
    .line 2666452
    new-instance v0, LX/JEk;

    invoke-direct {v0, p0, p1, p2}, LX/JEk;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;LX/JEn;Z)V

    .line 2666453
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->p:LX/64L;

    invoke-virtual {v1, v0}, LX/64L;->a(LX/64J;)V

    .line 2666454
    iget-object v0, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->p:LX/64L;

    invoke-virtual {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "bookmark"

    const-string v3, "update_optin_state"

    invoke-virtual {v0, v1, v2, v3}, LX/64L;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2666455
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2666443
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2666444
    invoke-static {p0, p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2666445
    invoke-direct {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->a()V

    .line 2666446
    const v0, 0x7f0d1260

    invoke-virtual {p0, v0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2666447
    const v1, 0x7f0d051d

    invoke-virtual {p0, v1}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->y:Landroid/view/View;

    .line 2666448
    new-instance v1, LX/JEm;

    invoke-direct {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->m()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, LX/JEm;-><init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->A:Landroid/widget/ArrayAdapter;

    .line 2666449
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->A:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2666450
    invoke-direct {p0}, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->l()V

    .line 2666451
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x625bbbf0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2666439
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2666440
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2666441
    iget-object v1, p0, Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2666442
    :cond_0
    const/16 v1, 0x23

    const v2, 0x52c70ccb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
