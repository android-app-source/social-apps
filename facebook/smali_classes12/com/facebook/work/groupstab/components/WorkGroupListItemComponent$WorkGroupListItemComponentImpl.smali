.class public final Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hfr;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

.field public b:LX/Hfh;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Ljava/lang/Boolean;

.field public e:I

.field public final synthetic f:LX/Hfr;


# direct methods
.method public constructor <init>(LX/Hfr;)V
    .locals 1

    .prologue
    .line 2492401
    iput-object p1, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->f:LX/Hfr;

    .line 2492402
    move-object v0, p1

    .line 2492403
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2492404
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2492405
    const-string v0, "WorkGroupListItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2492406
    if-ne p0, p1, :cond_1

    .line 2492407
    :cond_0
    :goto_0
    return v0

    .line 2492408
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2492409
    goto :goto_0

    .line 2492410
    :cond_3
    check-cast p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492411
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2492412
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2492413
    if-eq v2, v3, :cond_0

    .line 2492414
    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    iget-object v3, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2492415
    goto :goto_0

    .line 2492416
    :cond_5
    iget-object v2, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    if-nez v2, :cond_4

    .line 2492417
    :cond_6
    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    iget-object v3, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2492418
    goto :goto_0

    .line 2492419
    :cond_8
    iget-object v2, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->b:LX/Hfh;

    if-nez v2, :cond_7

    .line 2492420
    :cond_9
    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2492421
    goto :goto_0

    .line 2492422
    :cond_b
    iget-object v2, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_a

    .line 2492423
    :cond_c
    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2492424
    goto :goto_0

    .line 2492425
    :cond_e
    iget-object v2, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_d

    .line 2492426
    :cond_f
    iget v2, p0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2492427
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2492428
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;

    .line 2492429
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/work/groupstab/components/WorkGroupListItemComponent$WorkGroupListItemComponentImpl;->d:Ljava/lang/Boolean;

    .line 2492430
    return-object v0
.end method
