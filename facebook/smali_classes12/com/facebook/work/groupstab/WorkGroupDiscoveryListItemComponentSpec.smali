.class public Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HfO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8yV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HgN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2491683
    const-class v0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    const-string v1, "work_groups_tab"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491682
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)I
    .locals 3
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2491577
    sget-object v0, LX/HfK;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2491578
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shouldn\'t happen, gysj: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2491579
    :pswitch_0
    const v0, 0x7f08371a

    .line 2491580
    :goto_0
    return v0

    .line 2491581
    :pswitch_1
    const v0, 0x7f08371b

    goto :goto_0

    .line 2491582
    :pswitch_2
    const v0, 0x7f08371c

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;)LX/0Px;
    .locals 9
    .param p0    # Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            ")",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2491658
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2491659
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->k()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2491660
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->k()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;->j()LX/0Px;

    move-result-object v1

    .line 2491661
    :goto_0
    move-object v4, v1

    .line 2491662
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 2491663
    if-eqz v4, :cond_0

    .line 2491664
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v6, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel;

    .line 2491665
    const/4 v7, 0x5

    if-eq v1, v7, :cond_0

    .line 2491666
    const/4 v8, 0x0

    .line 2491667
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel$EdgesNodeModel;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 2491668
    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel$EdgesNodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel$EdgesNodeModel;->j()LX/1vs;

    move-result-object v7

    iget v7, v7, LX/1vs;->b:I

    .line 2491669
    if-eqz v7, :cond_4

    const/4 v7, 0x1

    :goto_2
    if-eqz v7, :cond_6

    .line 2491670
    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel$EdgesNodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel$EdgesModel$EdgesNodeModel;->j()LX/1vs;

    move-result-object v7

    iget-object p0, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    invoke-virtual {p0, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 2491671
    :goto_3
    move-object v0, v7

    .line 2491672
    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2491673
    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2491674
    add-int/lit8 v0, v1, 0x1

    .line 2491675
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 2491676
    :cond_0
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2491677
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2491678
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_4

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    move v7, v8

    .line 2491679
    goto :goto_2

    :cond_5
    move v7, v8

    goto :goto_2

    .line 2491680
    :cond_6
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;LX/1De;ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;LX/2kW;LX/1Ad;)LX/1Dh;
    .locals 7

    .prologue
    .line 2491626
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2491627
    if-eqz p3, :cond_2

    .line 2491628
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2491629
    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    if-eqz v2, :cond_4

    .line 2491630
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2491631
    const-class v6, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v2, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    if-eqz v2, :cond_6

    .line 2491632
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v6, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v2, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2491633
    if-eqz v2, :cond_5

    :goto_2
    if-eqz v3, :cond_7

    .line 2491634
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v5, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3, v2, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2491635
    :goto_3
    move-object v2, v2

    .line 2491636
    invoke-virtual {p5, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v2

    const v3, 0x7f0a00d6

    invoke-virtual {v2, v3}, LX/1nh;->i(I)LX/1nh;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v1

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2360

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b235f

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->e:LX/HfO;

    const/4 v2, 0x0

    .line 2491637
    new-instance v3, LX/HfN;

    invoke-direct {v3, v1}, LX/HfN;-><init>(LX/HfO;)V

    .line 2491638
    sget-object v4, LX/HfO;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HfM;

    .line 2491639
    if-nez v4, :cond_0

    .line 2491640
    new-instance v4, LX/HfM;

    invoke-direct {v4}, LX/HfM;-><init>()V

    .line 2491641
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/HfM;->a$redex0(LX/HfM;LX/1De;IILX/HfN;)V

    .line 2491642
    move-object v3, v4

    .line 2491643
    move-object v2, v3

    .line 2491644
    move-object v1, v2

    .line 2491645
    iget-object v2, v1, LX/HfM;->a:LX/HfN;

    iput p2, v2, LX/HfN;->a:I

    .line 2491646
    iget-object v2, v1, LX/HfM;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491647
    move-object v1, v1

    .line 2491648
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2491649
    iget-object v3, v1, LX/HfM;->a:LX/HfN;

    iput-object v2, v3, LX/HfN;->b:Ljava/lang/String;

    .line 2491650
    iget-object v3, v1, LX/HfM;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491651
    move-object v1, v1

    .line 2491652
    iget-object v2, v1, LX/HfM;->a:LX/HfN;

    iput-object p4, v2, LX/HfN;->c:LX/2kW;

    .line 2491653
    iget-object v2, v1, LX/HfM;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491654
    move-object v1, v1

    .line 2491655
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v4

    .line 2491656
    goto/16 :goto_0

    :cond_2
    move v2, v4

    goto/16 :goto_0

    :cond_3
    move v2, v4

    goto/16 :goto_1

    :cond_4
    move v2, v4

    goto/16 :goto_1

    :cond_5
    move v3, v4

    goto/16 :goto_2

    :cond_6
    move v3, v4

    goto/16 :goto_2

    .line 2491657
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;
    .locals 11

    .prologue
    .line 2491613
    const-class v1, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    monitor-enter v1

    .line 2491614
    :try_start_0
    sget-object v0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491615
    sput-object v2, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491616
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491617
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491618
    new-instance v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;

    invoke-direct {v3}, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;-><init>()V

    .line 2491619
    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/HfW;->a(LX/0QB;)LX/HfW;

    move-result-object v6

    check-cast v6, LX/HfW;

    invoke-static {v0}, LX/HfO;->a(LX/0QB;)LX/HfO;

    move-result-object v7

    check-cast v7, LX/HfO;

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v8

    check-cast v8, LX/8yV;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v9

    check-cast v9, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object p0

    check-cast p0, LX/HgN;

    .line 2491620
    iput-object v4, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->b:LX/0Or;

    iput-object v5, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->c:Ljava/util/concurrent/Executor;

    iput-object v6, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->d:LX/HfW;

    iput-object v7, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->e:LX/HfO;

    iput-object v8, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->f:LX/8yV;

    iput-object v9, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->g:LX/17Y;

    iput-object v10, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, v3, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->i:LX/HgN;

    .line 2491621
    move-object v0, v3

    .line 2491622
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491623
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491624
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;LX/2kW;)LX/1Dg;
    .locals 10
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/2kW;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2491583
    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    .line 2491584
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v7, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v7, v0, :cond_1

    const/4 v0, 0x1

    move v6, v0

    .line 2491585
    :goto_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083719

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->k()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$GroupMemberProfilesModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2491586
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    iget-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ad;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a(Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;LX/1De;ILcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;LX/2kW;LX/1Ad;)LX/1Dh;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0e0bb3

    invoke-static {p1, v1, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b2364

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b236a

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0e0bb4

    invoke-static {p1, v1, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b2364

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b236a

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v5, 0x2

    .line 2491587
    invoke-static {p3}, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a(Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;)LX/0Px;

    move-result-object v1

    .line 2491588
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2364

    invoke-interface {v2, v8, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2365

    invoke-interface {v2, v9, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2364

    invoke-interface {v2, v5, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->f:LX/8yV;

    invoke-virtual {v3, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v3

    sget-object v4, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v1

    const v3, 0x7f0b2369

    invoke-virtual {v1, v3}, LX/8yU;->l(I)LX/8yU;

    move-result-object v1

    const v3, 0x7f0b2368

    invoke-virtual {v1, v3}, LX/8yU;->h(I)LX/8yU;

    move-result-object v1

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, LX/8yU;->m(I)LX/8yU;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v1

    const v3, 0x7f0b235a

    invoke-virtual {v1, v3}, LX/8yU;->r(I)LX/8yU;

    move-result-object v1

    const v3, 0x7f0a00e7

    invoke-virtual {v1, v3}, LX/8yU;->i(I)LX/8yU;

    move-result-object v1

    const v3, 0x7f0b2359

    invoke-virtual {v1, v3}, LX/8yU;->k(I)LX/8yU;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    .line 2491589
    new-instance v3, LX/HfE;

    invoke-direct {v3}, LX/HfE;-><init>()V

    .line 2491590
    sget-object v4, LX/HfF;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HfD;

    .line 2491591
    if-nez v4, :cond_0

    .line 2491592
    new-instance v4, LX/HfD;

    invoke-direct {v4}, LX/HfD;-><init>()V

    .line 2491593
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/HfD;->a$redex0(LX/HfD;LX/1De;IILX/HfE;)V

    .line 2491594
    move-object v3, v4

    .line 2491595
    move-object v2, v3

    .line 2491596
    move-object v2, v2

    .line 2491597
    iget-object v3, v2, LX/HfD;->a:LX/HfE;

    iput-boolean v6, v3, LX/HfE;->a:Z

    .line 2491598
    iget-object v3, v2, LX/HfD;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491599
    move-object v2, v2

    .line 2491600
    invoke-static {v7}, Lcom/facebook/work/groupstab/WorkGroupDiscoveryListItemComponentSpec;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)I

    move-result v3

    .line 2491601
    iget-object v4, v2, LX/HfD;->a:LX/HfE;

    invoke-virtual {v2, v3}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/HfE;->b:Ljava/lang/CharSequence;

    .line 2491602
    iget-object v4, v2, LX/HfD;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2491603
    move-object v2, v2

    .line 2491604
    const v3, -0x3124dd89

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2491605
    iget-object v4, v2, LX/HfD;->a:LX/HfE;

    iput-object v3, v4, LX/HfE;->c:LX/1dQ;

    .line 2491606
    move-object v2, v2

    .line 2491607
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    move-object v1, v1

    .line 2491608
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x12c

    invoke-interface {v0, v1}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    .line 2491609
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0941

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b235c

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b235b

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    .line 2491610
    const v2, -0x3125134d

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2491611
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 2491612
    :cond_1
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0
.end method
