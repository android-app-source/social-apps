.class public Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TGroupInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hg7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hg3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hfr;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HgN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2492286
    const-class v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    const-string v1, "work_groups_tab"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492281
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2492282
    iput-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b:LX/0Ot;

    .line 2492283
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2492284
    iput-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->d:LX/0Ot;

    .line 2492285
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;
    .locals 8

    .prologue
    .line 2492251
    const-class v1, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    monitor-enter v1

    .line 2492252
    :try_start_0
    sget-object v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492253
    sput-object v2, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492254
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492255
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492256
    new-instance v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    invoke-direct {v3}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;-><init>()V

    .line 2492257
    const/16 v4, 0x38d3

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/Hg3;->a(LX/0QB;)LX/Hg3;

    move-result-object v5

    check-cast v5, LX/Hg3;

    const/16 v6, 0x38cf

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object p0

    check-cast p0, LX/HgN;

    .line 2492258
    iput-object v4, v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->c:LX/Hg3;

    iput-object v6, v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->d:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->e:LX/0Uh;

    iput-object p0, v3, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->f:LX/HgN;

    .line 2492259
    move-object v0, v3

    .line 2492260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V
    .locals 3

    .prologue
    .line 2492276
    invoke-virtual {p0}, LX/95R;->a()LX/2kW;

    move-result-object v0

    .line 2492277
    new-instance v1, LX/Hfj;

    invoke-direct {v1, p1}, LX/Hfj;-><init>(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)V

    move-object v1, v1

    .line 2492278
    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2kW;->a(LX/0Rl;Ljava/lang/String;)V

    .line 2492279
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;LX/Hfw;LX/95R;LX/95R;LX/95R;LX/95R;LX/BcP;)LX/1X1;
    .locals 10
    .param p4    # LX/95R;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/95R;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/95R;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/95R;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Object;",
            "LX/Hfw;",
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;",
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;",
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;",
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;",
            "LX/BcP;",
            ")",
            "LX/1X1;"
        }
    .end annotation

    .prologue
    .line 2492272
    move-object v5, p2

    check-cast v5, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    .line 2492273
    invoke-virtual {v5}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v1, v2, :cond_0

    .line 2492274
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hfr;

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/Hfr;->c(LX/1De;)LX/Hfp;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/Hfp;->h(I)LX/Hfp;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/Hfp;->a(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)LX/Hfp;

    move-result-object v9

    new-instance v1, LX/Hfh;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p5

    move-object v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, LX/Hfh;-><init>(Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;LX/Hfw;LX/95R;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;LX/95R;LX/95R;LX/95R;)V

    invoke-virtual {v9, v1}, LX/Hfp;->a(LX/Hfh;)LX/Hfp;

    move-result-object v1

    sget-object v2, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/Hfp;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/Hfp;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2492275
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, LX/Hfi;

    invoke-direct {v1, p0}, LX/Hfi;-><init>(Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;)V

    goto :goto_0
.end method

.method public final a(LX/48H;LX/Hfw;LX/5K7;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/48H",
            "<",
            "LX/Hfw;",
            ">;",
            "LX/Hfw;",
            "LX/5K7;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2492264
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/5K7;->a(Z)V

    .line 2492265
    iput-object p2, p1, LX/1np;->a:Ljava/lang/Object;

    .line 2492266
    iget-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->f:LX/HgN;

    .line 2492267
    iget-object v1, v0, LX/HgN;->a:LX/0if;

    sget-object v2, LX/0ig;->af:LX/0ih;

    const-string v3, "filter_changed"

    const/4 v4, 0x0

    iget-object v5, v0, LX/HgN;->b:LX/Hfw;

    .line 2492268
    invoke-static {v5}, LX/HgN;->c(LX/Hfw;)LX/1rQ;

    move-result-object p0

    const-string p1, "new_ordering"

    invoke-virtual {p2}, LX/Hfw;->name()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object p0

    move-object v5, p0

    .line 2492269
    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2492270
    iput-object p2, v0, LX/HgN;->b:LX/Hfw;

    .line 2492271
    return-void
.end method
