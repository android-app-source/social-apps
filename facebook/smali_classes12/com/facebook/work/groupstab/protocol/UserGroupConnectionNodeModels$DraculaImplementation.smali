.class public final Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2494222
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2494223
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2494220
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2494221
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2494176
    if-nez p1, :cond_0

    move v0, v1

    .line 2494177
    :goto_0
    return v0

    .line 2494178
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2494179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2494180
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494181
    const v2, -0x18705808

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2494182
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494183
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494184
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2494185
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494186
    const v2, 0x43ca8ee1

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2494187
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494188
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494189
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2494190
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494191
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2494192
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494193
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494194
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2494195
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494196
    const v2, 0x7c6873d2

    const/4 v5, 0x0

    .line 2494197
    if-nez v0, :cond_1

    move v4, v5

    .line 2494198
    :goto_1
    move v0, v4

    .line 2494199
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494200
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494201
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2494202
    :sswitch_4
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    .line 2494203
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2494204
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494205
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494206
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2494207
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2494208
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2494209
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2494210
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2494211
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2494212
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 2494213
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2494214
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2494215
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2494216
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2494217
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2494218
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2494219
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x58a65be1 -> :sswitch_5
        -0x3eeb8499 -> :sswitch_3
        -0x18705808 -> :sswitch_1
        0x43ca8ee1 -> :sswitch_2
        0x5460f9d3 -> :sswitch_0
        0x7c6873d2 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2494175
    new-instance v0, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2494170
    if-eqz p0, :cond_0

    .line 2494171
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2494172
    if-eq v0, p0, :cond_0

    .line 2494173
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2494174
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2494152
    sparse-switch p2, :sswitch_data_0

    .line 2494153
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2494154
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494155
    const v1, -0x18705808

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2494156
    :goto_0
    :sswitch_1
    return-void

    .line 2494157
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494158
    const v1, 0x43ca8ee1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2494159
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2494160
    const v1, 0x7c6873d2

    .line 2494161
    if-eqz v0, :cond_0

    .line 2494162
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2494163
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2494164
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2494165
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2494166
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2494167
    :cond_0
    goto :goto_0

    .line 2494168
    :sswitch_4
    const-class v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposesImageFragmentModel;

    .line 2494169
    invoke-static {v0, p3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x58a65be1 -> :sswitch_1
        -0x3eeb8499 -> :sswitch_3
        -0x18705808 -> :sswitch_2
        0x43ca8ee1 -> :sswitch_1
        0x5460f9d3 -> :sswitch_0
        0x7c6873d2 -> :sswitch_4
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2494146
    if-eqz p1, :cond_0

    .line 2494147
    invoke-static {p0, p1, p2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;

    move-result-object v1

    .line 2494148
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;

    .line 2494149
    if-eq v0, v1, :cond_0

    .line 2494150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2494151
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2494145
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2494224
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2494225
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2494140
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2494141
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2494142
    :cond_0
    iput-object p1, p0, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a:LX/15i;

    .line 2494143
    iput p2, p0, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->b:I

    .line 2494144
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2494139
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2494138
    new-instance v0, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2494135
    iget v0, p0, LX/1vt;->c:I

    .line 2494136
    move v0, v0

    .line 2494137
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2494132
    iget v0, p0, LX/1vt;->c:I

    .line 2494133
    move v0, v0

    .line 2494134
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2494129
    iget v0, p0, LX/1vt;->b:I

    .line 2494130
    move v0, v0

    .line 2494131
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2494126
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2494127
    move-object v0, v0

    .line 2494128
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2494117
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2494118
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2494119
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2494120
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2494121
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2494122
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2494123
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2494124
    invoke-static {v3, v9, v2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2494125
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2494114
    iget v0, p0, LX/1vt;->c:I

    .line 2494115
    move v0, v0

    .line 2494116
    return v0
.end method
