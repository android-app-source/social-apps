.class public final Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f1d5e93
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2493436
    const-class v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2493435
    const-class v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2493433
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2493434
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2493427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2493428
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2493429
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2493430
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2493431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2493432
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2493437
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2493438
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2493439
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    .line 2493440
    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2493441
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;

    .line 2493442
    iput-object v0, v1, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->e:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    .line 2493443
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2493444
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupsYouShouldJoin"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2493425
    iget-object v0, p0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->e:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    iput-object v0, p0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->e:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    .line 2493426
    iget-object v0, p0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->e:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2493420
    new-instance v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;

    invoke-direct {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;-><init>()V

    .line 2493421
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2493422
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2493424
    const v0, -0x2bbb31a8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2493423
    const v0, -0x6747e1ce

    return v0
.end method
