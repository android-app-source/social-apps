.class public final Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2493313
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2493314
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2493311
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2493312
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2493292
    if-nez p1, :cond_0

    move v0, v1

    .line 2493293
    :goto_0
    return v0

    .line 2493294
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2493295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2493296
    :sswitch_0
    const-class v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 2493297
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2493298
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2493299
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2493300
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2493301
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2493302
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2493303
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2493304
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2493305
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2493306
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2493307
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2493308
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2493309
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2493310
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x54461e87 -> :sswitch_0
        -0x28c789f5 -> :sswitch_2
        -0x1dfc12e1 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2493291
    new-instance v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2493286
    if-eqz p0, :cond_0

    .line 2493287
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2493288
    if-eq v0, p0, :cond_0

    .line 2493289
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2493290
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2493281
    sparse-switch p2, :sswitch_data_0

    .line 2493282
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2493283
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 2493284
    invoke-static {v0, p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2493285
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x54461e87 -> :sswitch_0
        -0x28c789f5 -> :sswitch_1
        -0x1dfc12e1 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2493280
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2493278
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2493279
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2493315
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2493316
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2493317
    :cond_0
    iput-object p1, p0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2493318
    iput p2, p0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->b:I

    .line 2493319
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2493277
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2493276
    new-instance v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2493273
    iget v0, p0, LX/1vt;->c:I

    .line 2493274
    move v0, v0

    .line 2493275
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2493270
    iget v0, p0, LX/1vt;->c:I

    .line 2493271
    move v0, v0

    .line 2493272
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2493267
    iget v0, p0, LX/1vt;->b:I

    .line 2493268
    move v0, v0

    .line 2493269
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2493264
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2493265
    move-object v0, v0

    .line 2493266
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2493255
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2493256
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2493257
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2493258
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2493259
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2493260
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2493261
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2493262
    invoke-static {v3, v9, v2}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2493263
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2493252
    iget v0, p0, LX/1vt;->c:I

    .line 2493253
    move v0, v0

    .line 2493254
    return v0
.end method
