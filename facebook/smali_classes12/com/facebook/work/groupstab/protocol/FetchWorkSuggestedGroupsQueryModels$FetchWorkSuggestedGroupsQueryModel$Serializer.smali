.class public final Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2493407
    const-class v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;

    new-instance v1, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2493408
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2493409
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2493410
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2493411
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2493412
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2493413
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2493414
    if-eqz v2, :cond_0

    .line 2493415
    const-string p0, "groups_you_should_join"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2493416
    invoke-static {v1, v2, p1, p2}, LX/Hga;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2493417
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2493418
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2493419
    check-cast p1, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$Serializer;->a(Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
