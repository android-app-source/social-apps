.class public Lcom/facebook/work/groupstab/WorkGroupsTabFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""

# interfaces
.implements LX/0fh;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->WORK_GROUPS_TAB:LX/0cQ;
.end annotation


# instance fields
.field private final a:LX/5K7;

.field private b:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/Hff;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HgJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HgM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HfJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Hf9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/HgN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2491810
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    .line 2491811
    new-instance v0, LX/5K7;

    invoke-direct {v0}, LX/5K7;-><init>()V

    iput-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->a:LX/5K7;

    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1

    .prologue
    .line 2491809
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 5

    .prologue
    .line 2491864
    const/4 v0, 0x0

    .line 2491865
    new-instance v1, LX/Hfm;

    invoke-direct {v1}, LX/Hfm;-><init>()V

    .line 2491866
    sget-object v2, LX/Hfn;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hfl;

    .line 2491867
    if-nez v2, :cond_0

    .line 2491868
    new-instance v2, LX/Hfl;

    invoke-direct {v2}, LX/Hfl;-><init>()V

    .line 2491869
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Hfl;->a$redex0(LX/Hfl;LX/1De;IILX/Hfm;)V

    .line 2491870
    move-object v1, v2

    .line 2491871
    move-object v0, v1

    .line 2491872
    move-object v0, v0

    .line 2491873
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b235d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2491874
    iget-object v2, v0, LX/Hfl;->a:LX/Hfm;

    iput v1, v2, LX/Hfm;->a:I

    .line 2491875
    iget-object v2, v0, LX/Hfl;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491876
    move-object v0, v0

    .line 2491877
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 2491878
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b236b

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 2491879
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    move v1, v1

    .line 2491880
    iget-object v2, v0, LX/Hfl;->a:LX/Hfm;

    iput v1, v2, LX/Hfm;->b:F

    .line 2491881
    iget-object v2, v0, LX/Hfl;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491882
    move-object v0, v0

    .line 2491883
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->k:LX/Hf9;

    .line 2491884
    new-instance v2, LX/Hf8;

    invoke-direct {v2, v1}, LX/Hf8;-><init>(LX/Hf9;)V

    .line 2491885
    sget-object v3, LX/Hf9;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hf7;

    .line 2491886
    if-nez v3, :cond_1

    .line 2491887
    new-instance v3, LX/Hf7;

    invoke-direct {v3}, LX/Hf7;-><init>()V

    .line 2491888
    :cond_1
    iput-object v2, v3, LX/BcN;->a:LX/BcO;

    .line 2491889
    iput-object v2, v3, LX/Hf7;->a:LX/Hf8;

    .line 2491890
    iget-object v1, v3, LX/Hf7;->d:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 2491891
    move-object v2, v3

    .line 2491892
    move-object v1, v2

    .line 2491893
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->f:LX/95R;

    invoke-virtual {v2}, LX/95R;->a()LX/2kW;

    move-result-object v2

    .line 2491894
    iget-object v3, v1, LX/Hf7;->a:LX/Hf8;

    iput-object v2, v3, LX/Hf8;->b:LX/2kW;

    .line 2491895
    iget-object v3, v1, LX/Hf7;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491896
    move-object v1, v1

    .line 2491897
    invoke-virtual {v1}, LX/Hf7;->b()LX/BcO;

    move-result-object v1

    .line 2491898
    iget-object v2, v0, LX/Hfl;->a:LX/Hfm;

    iput-object v1, v2, LX/Hfm;->c:LX/BcO;

    .line 2491899
    iget-object v2, v0, LX/Hfl;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491900
    move-object v0, v0

    .line 2491901
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2491902
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->g:LX/Hff;

    .line 2491903
    new-instance v2, LX/Hfe;

    invoke-direct {v2, v1}, LX/Hfe;-><init>(LX/Hff;)V

    .line 2491904
    iget-object v3, v1, LX/Hff;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hfb;

    .line 2491905
    if-nez v3, :cond_2

    .line 2491906
    new-instance v3, LX/Hfb;

    invoke-direct {v3, v1}, LX/Hfb;-><init>(LX/Hff;)V

    .line 2491907
    :cond_2
    iput-object v2, v3, LX/BcN;->a:LX/BcO;

    .line 2491908
    iput-object v2, v3, LX/Hfb;->a:LX/Hfe;

    .line 2491909
    iget-object v1, v3, LX/Hfb;->e:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 2491910
    move-object v2, v3

    .line 2491911
    move-object v1, v2

    .line 2491912
    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    move-object v2, v2

    .line 2491913
    iget-object v3, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v2, v3, LX/Hfe;->d:LX/5K7;

    .line 2491914
    iget-object v3, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491915
    move-object v1, v1

    .line 2491916
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->b:LX/95R;

    .line 2491917
    iget-object v3, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v2, v3, LX/Hfe;->e:LX/95R;

    .line 2491918
    iget-object v3, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491919
    move-object v1, v1

    .line 2491920
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->c:LX/95R;

    .line 2491921
    iget-object v3, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v2, v3, LX/Hfe;->f:LX/95R;

    .line 2491922
    iget-object v3, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491923
    move-object v1, v1

    .line 2491924
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->d:LX/95R;

    .line 2491925
    iget-object v3, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v2, v3, LX/Hfe;->g:LX/95R;

    .line 2491926
    iget-object v3, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491927
    move-object v1, v1

    .line 2491928
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->e:LX/95R;

    .line 2491929
    iget-object v3, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v2, v3, LX/Hfe;->h:LX/95R;

    .line 2491930
    iget-object v3, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2491931
    move-object v1, v1

    .line 2491932
    iget-object v2, v1, LX/Hfb;->a:LX/Hfe;

    iput-object v0, v2, LX/Hfe;->i:LX/1X1;

    .line 2491933
    iget-object v2, v1, LX/Hfb;->e:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491934
    move-object v0, v1

    .line 2491935
    invoke-virtual {v0, p2}, LX/Hfb;->b(LX/BcQ;)LX/Hfb;

    move-result-object v0

    invoke-virtual {v0}, LX/Hfb;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2491863
    const-string v0, "work_groups_tab"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2491836
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2491837
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;

    invoke-static {v0}, LX/Hff;->a(LX/0QB;)LX/Hff;

    move-result-object v3

    check-cast v3, LX/Hff;

    const-class v4, LX/HgJ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/HgJ;

    const-class v5, LX/HgM;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HgM;

    invoke-static {v0}, LX/HfJ;->a(LX/0QB;)LX/HfJ;

    move-result-object v6

    check-cast v6, LX/HfJ;

    invoke-static {v0}, LX/Hf9;->a(LX/0QB;)LX/Hf9;

    move-result-object v7

    check-cast v7, LX/Hf9;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object v0

    check-cast v0, LX/HgN;

    iput-object v3, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->g:LX/Hff;

    iput-object v4, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->h:LX/HgJ;

    iput-object v5, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->i:LX/HgM;

    iput-object v6, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->j:LX/HfJ;

    iput-object v7, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->k:LX/Hf9;

    iput-object p1, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->l:LX/17W;

    iput-object v0, v2, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->m:LX/HgN;

    .line 2491838
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b234d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2491839
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->h:LX/HgJ;

    invoke-virtual {v1, v0}, LX/HgJ;->a(I)LX/HgI;

    move-result-object v1

    .line 2491840
    iget-object v2, v1, LX/HgI;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2491841
    iget-object v2, v1, LX/HgI;->c:LX/95S;

    invoke-virtual {v2}, LX/95S;->a()LX/95R;

    move-result-object v2

    move-object v1, v2

    .line 2491842
    iput-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->b:LX/95R;

    .line 2491843
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->h:LX/HgJ;

    invoke-virtual {v1, v0}, LX/HgJ;->a(I)LX/HgI;

    move-result-object v1

    .line 2491844
    iget-object v2, v1, LX/HgI;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2491845
    iget-object v2, v1, LX/HgI;->d:LX/95S;

    invoke-virtual {v2}, LX/95S;->a()LX/95R;

    move-result-object v2

    move-object v1, v2

    .line 2491846
    iput-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->c:LX/95R;

    .line 2491847
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2368

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2491848
    iget-object v2, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->i:LX/HgM;

    .line 2491849
    new-instance v5, LX/HgL;

    const-class v3, LX/1rq;

    invoke-interface {v2, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1rq;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {v5, v3, v4, v1}, LX/HgL;-><init>(LX/1rq;LX/0Sh;I)V

    .line 2491850
    move-object v1, v5

    .line 2491851
    iget-object v2, v1, LX/HgL;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2491852
    iget-object v2, v1, LX/HgL;->c:LX/95S;

    invoke-virtual {v2}, LX/95S;->a()LX/95R;

    move-result-object v2

    move-object v1, v2

    .line 2491853
    iput-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->f:LX/95R;

    .line 2491854
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->h:LX/HgJ;

    invoke-virtual {v1, v0}, LX/HgJ;->a(I)LX/HgI;

    move-result-object v1

    .line 2491855
    iget-object v2, v1, LX/HgI;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2491856
    iget-object v2, v1, LX/HgI;->e:LX/95S;

    invoke-virtual {v2}, LX/95S;->a()LX/95R;

    move-result-object v2

    move-object v1, v2

    .line 2491857
    iput-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->d:LX/95R;

    .line 2491858
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->h:LX/HgJ;

    invoke-virtual {v1, v0}, LX/HgJ;->a(I)LX/HgI;

    move-result-object v0

    .line 2491859
    iget-object v1, v0, LX/HgI;->b:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2491860
    iget-object v1, v0, LX/HgI;->f:LX/95S;

    invoke-virtual {v1}, LX/95S;->a()LX/95R;

    move-result-object v1

    move-object v0, v1

    .line 2491861
    iput-object v0, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->e:LX/95R;

    .line 2491862
    return-void
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2491936
    new-instance v0, LX/HfT;

    invoke-direct {v0, p0}, LX/HfT;-><init>(Lcom/facebook/work/groupstab/WorkGroupsTabFragment;)V

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x506806d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2491832
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onDestroy()V

    .line 2491833
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->f:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2491834
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->c:LX/95R;

    invoke-virtual {v1}, LX/95R;->close()V

    .line 2491835
    const/16 v1, 0x2b

    const v2, 0x55d939aa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x13f21ea7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2491828
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onResume()V

    .line 2491829
    iget-object v1, p0, Lcom/facebook/work/groupstab/WorkGroupsTabFragment;->m:LX/HgN;

    .line 2491830
    iget-object v2, v1, LX/HgN;->a:LX/0if;

    sget-object p0, LX/0ig;->af:LX/0ih;

    invoke-virtual {v2, p0}, LX/0if;->a(LX/0ih;)V

    .line 2491831
    const/16 v1, 0x2b

    const v2, -0x651db7c3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 2491812
    invoke-super {p0, p1, p2}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2491813
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2491814
    if-eqz v0, :cond_0

    .line 2491815
    const v1, 0x7f083711

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2491816
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2491817
    const-string v1, "ref"

    const-string v2, "WORK_GROUPS_TAB_FAB"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2491818
    new-instance v1, Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0e0bb8

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/uicontrib/fab/FabView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2491819
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020970

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2491820
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableColor(I)V

    .line 2491821
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2491822
    const/16 v3, 0x55

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2491823
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b2356

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b2357

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2491824
    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/fab/FabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2491825
    check-cast p1, Landroid/widget/FrameLayout;

    invoke-virtual {p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2491826
    new-instance v2, LX/HfS;

    invoke-direct {v2, p0, v0}, LX/HfS;-><init>(Lcom/facebook/work/groupstab/WorkGroupsTabFragment;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2491827
    return-void
.end method
