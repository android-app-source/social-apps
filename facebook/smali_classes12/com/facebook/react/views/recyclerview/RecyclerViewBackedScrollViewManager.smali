.class public Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""

# interfaces
.implements LX/FUp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/K0o;",
        ">;",
        "LX/FUp",
        "<",
        "LX/K0o;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2681952
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private static a(LX/K0o;)I
    .locals 1

    .prologue
    .line 2681951
    invoke-virtual {p0}, LX/K0o;->getChildCountFromAdapter()I

    move-result v0

    return v0
.end method

.method private static a(LX/K0o;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681950
    invoke-virtual {p0, p1}, LX/K0o;->g(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/K0o;LX/FUr;)V
    .locals 2

    .prologue
    .line 2681948
    iget v0, p1, LX/FUr;->b:I

    iget-boolean v1, p1, LX/FUr;->c:Z

    invoke-virtual {p0, v0, v1}, LX/K0o;->b(IZ)V

    .line 2681949
    return-void
.end method


# virtual methods
.method public synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681947
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;->b(LX/5rJ;)LX/K0o;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681946
    check-cast p1, LX/K0o;

    invoke-static {p1, p2}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;->a(LX/K0o;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2681932
    check-cast p1, LX/K0o;

    invoke-static {p0, p1, p2, p3}, LX/FUs;->a(LX/FUp;Ljava/lang/Object;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2681943
    check-cast p1, LX/K0o;

    .line 2681944
    invoke-virtual {p1, p2, p3}, LX/K0o;->a(Landroid/view/View;I)V

    .line 2681945
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/FUr;)V
    .locals 0

    .prologue
    .line 2681942
    check-cast p1, LX/K0o;

    invoke-static {p1, p2}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;->a(LX/K0o;LX/FUr;)V

    return-void
.end method

.method public final synthetic b(Landroid/view/ViewGroup;)I
    .locals 1

    .prologue
    .line 2681941
    check-cast p1, LX/K0o;

    invoke-static {p1}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;->a(LX/K0o;)I

    move-result v0

    return v0
.end method

.method public b(LX/5rJ;)LX/K0o;
    .locals 1

    .prologue
    .line 2681940
    new-instance v0, LX/K0o;

    invoke-direct {v0, p1}, LX/K0o;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final bridge synthetic b(Landroid/view/ViewGroup;I)V
    .locals 0

    .prologue
    .line 2681937
    check-cast p1, LX/K0o;

    .line 2681938
    invoke-virtual {p1, p2}, LX/K0o;->f(I)V

    .line 2681939
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681936
    const-string v0, "AndroidRecyclerViewBackedScrollView"

    return-object v0
.end method

.method public setOnContentSizeChange(LX/K0o;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "onContentSizeChange"
    .end annotation

    .prologue
    .line 2681934
    iput-boolean p2, p1, LX/K0o;->h:Z

    .line 2681935
    return-void
.end method

.method public final x()Ljava/util/Map;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2681933
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    sget-object v1, LX/FUv;->SCROLL:LX/FUv;

    invoke-virtual {v1}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registrationName"

    const-string v3, "onScroll"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
