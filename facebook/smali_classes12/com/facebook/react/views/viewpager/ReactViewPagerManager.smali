.class public Lcom/facebook/react/views/viewpager/ReactViewPagerManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidViewPager"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/K1S;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671843
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private static a(LX/K1S;)I
    .locals 1

    .prologue
    .line 2671844
    invoke-virtual {p0}, LX/K1S;->getViewCountInAdapter()I

    move-result v0

    return v0
.end method

.method private static a(LX/K1S;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2671845
    invoke-virtual {p0, p1}, LX/K1S;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/K1S;ILX/5pC;)V
    .locals 6
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2671849
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671850
    invoke-static {p3}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671851
    packed-switch p2, :pswitch_data_0

    .line 2671852
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported command %d received by %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2671853
    :pswitch_0
    invoke-interface {p3, v4}, LX/5pC;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0, v5}, LX/K1S;->b(IZ)V

    .line 2671854
    :goto_0
    return-void

    .line 2671855
    :pswitch_1
    invoke-interface {p3, v4}, LX/5pC;->getInt(I)I

    move-result v0

    invoke-virtual {p1, v0, v4}, LX/K1S;->b(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/5rJ;)LX/K1S;
    .locals 1

    .prologue
    .line 2671846
    new-instance v0, LX/K1S;

    invoke-direct {v0, p0}, LX/K1S;-><init>(LX/5pX;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2671847
    invoke-static {p1}, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;->b(LX/5rJ;)LX/K1S;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2671848
    check-cast p1, LX/K1S;

    invoke-static {p1, p2}, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;->a(LX/K1S;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2671839
    check-cast p1, LX/K1S;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;->a(LX/K1S;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2671840
    check-cast p1, LX/K1S;

    .line 2671841
    invoke-virtual {p1, p2, p3}, LX/K1S;->a(Landroid/view/View;I)V

    .line 2671842
    return-void
.end method

.method public final synthetic b(Landroid/view/ViewGroup;)I
    .locals 1

    .prologue
    .line 2671838
    check-cast p1, LX/K1S;

    invoke-static {p1}, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;->a(LX/K1S;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Landroid/view/ViewGroup;I)V
    .locals 0

    .prologue
    .line 2671835
    check-cast p1, LX/K1S;

    .line 2671836
    invoke-virtual {p1, p2}, LX/K1S;->b(I)V

    .line 2671837
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2671834
    const-string v0, "AndroidViewPager"

    return-object v0
.end method

.method public setPageMargin(LX/K1S;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 0.0f
        name = "pageMargin"
    .end annotation

    .prologue
    .line 2671832
    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2671833
    return-void
.end method

.method public setScrollEnabled(LX/K1S;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2671830
    iput-boolean p2, p1, LX/K1S;->c:Z

    .line 2671831
    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 2671827
    const/4 v0, 0x1

    return v0
.end method

.method public final v()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2671829
    const-string v0, "setPage"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "setPageWithoutAnimation"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 7

    .prologue
    .line 2671828
    const-string v0, "topPageScroll"

    const-string v1, "registrationName"

    const-string v2, "onPageScroll"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "topPageScrollStateChanged"

    const-string v3, "registrationName"

    const-string v4, "onPageScrollStateChanged"

    invoke-static {v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    const-string v4, "topPageSelected"

    const-string v5, "registrationName"

    const-string v6, "onPageSelected"

    invoke-static {v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
