.class public Lcom/facebook/react/views/modal/ReactModalHostManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTModalHostView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/K0Y;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2670107
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private a(LX/5rJ;LX/K0Y;)V
    .locals 2

    .prologue
    .line 2670100
    const-class v0, LX/5rQ;

    invoke-virtual {p1, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2670101
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2670102
    new-instance v1, LX/K0U;

    invoke-direct {v1, p0, v0, p2}, LX/K0U;-><init>(Lcom/facebook/react/views/modal/ReactModalHostManager;LX/5s9;LX/K0Y;)V

    .line 2670103
    iput-object v1, p2, LX/K0Y;->g:LX/K0T;

    .line 2670104
    new-instance v1, LX/K0V;

    invoke-direct {v1, p0, v0, p2}, LX/K0V;-><init>(Lcom/facebook/react/views/modal/ReactModalHostManager;LX/5s9;LX/K0Y;)V

    .line 2670105
    iput-object v1, p2, LX/K0Y;->f:Landroid/content/DialogInterface$OnShowListener;

    .line 2670106
    return-void
.end method

.method private a(LX/K0Y;)V
    .locals 0

    .prologue
    .line 2670097
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/View;)V

    .line 2670098
    invoke-virtual {p1}, LX/K0Y;->b()V

    .line 2670099
    return-void
.end method

.method private static b(LX/5rJ;)LX/K0Y;
    .locals 1

    .prologue
    .line 2670081
    new-instance v0, LX/K0Y;

    invoke-direct {v0, p0}, LX/K0Y;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private b(LX/K0Y;)V
    .locals 0

    .prologue
    .line 2670094
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/ViewGroupManager;->b(Landroid/view/View;)V

    .line 2670095
    invoke-virtual {p1}, LX/K0Y;->d()V

    .line 2670096
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2670093
    invoke-static {p1}, Lcom/facebook/react/views/modal/ReactModalHostManager;->b(LX/5rJ;)LX/K0Y;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2670092
    check-cast p2, LX/K0Y;

    invoke-direct {p0, p1, p2}, Lcom/facebook/react/views/modal/ReactModalHostManager;->a(LX/5rJ;LX/K0Y;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2670091
    check-cast p1, LX/K0Y;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/modal/ReactModalHostManager;->a(LX/K0Y;)V

    return-void
.end method

.method public final bridge synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2670108
    check-cast p1, LX/K0Y;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/modal/ReactModalHostManager;->b(LX/K0Y;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2670090
    const-string v0, "RCTModalHostView"

    return-object v0
.end method

.method public h()Lcom/facebook/react/uimanager/LayoutShadowNode;
    .locals 1

    .prologue
    .line 2670089
    new-instance v0, Lcom/facebook/react/views/modal/ModalHostShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/modal/ModalHostShadowNode;-><init>()V

    return-object v0
.end method

.method public i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/uimanager/LayoutShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2670088
    const-class v0, Lcom/facebook/react/views/modal/ModalHostShadowNode;

    return-object v0
.end method

.method public synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2670087
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;->h()Lcom/facebook/react/uimanager/LayoutShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public setAnimationType(LX/K0Y;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "animationType"
    .end annotation

    .prologue
    .line 2670085
    invoke-virtual {p1, p2}, LX/K0Y;->setAnimationType(Ljava/lang/String;)V

    .line 2670086
    return-void
.end method

.method public setTransparent(LX/K0Y;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "transparent"
    .end annotation

    .prologue
    .line 2670083
    iput-boolean p2, p1, LX/K0Y;->c:Z

    .line 2670084
    return-void
.end method

.method public final x()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2670082
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topRequestClose"

    const-string v2, "registrationName"

    const-string v3, "onRequestClose"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topShow"

    const-string v2, "registrationName"

    const-string v3, "onShow"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
