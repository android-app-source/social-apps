.class public Lcom/facebook/react/views/textinput/ReactTextInputManager;
.super Lcom/facebook/react/uimanager/BaseViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidTextInput"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/BaseViewManager",
        "<",
        "LX/K19;",
        "Lcom/facebook/react/uimanager/LayoutShadowNode;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[Landroid/text/InputFilter;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2671289
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a:[I

    .line 2671290
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/text/InputFilter;

    sput-object v0, Lcom/facebook/react/views/textinput/ReactTextInputManager;->b:[Landroid/text/InputFilter;

    return-void

    .line 2671291
    :array_0
    .array-data 4
        0x8
        0x0
        0x2
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671292
    invoke-direct {p0}, Lcom/facebook/react/uimanager/BaseViewManager;-><init>()V

    .line 2671293
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2671294
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, "00"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x31

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0x64

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(LX/5rJ;LX/K19;)V
    .locals 1

    .prologue
    .line 2671295
    new-instance v0, LX/K1K;

    invoke-direct {v0, p0, p1, p2}, LX/K1K;-><init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5pX;LX/K19;)V

    invoke-virtual {p2, v0}, LX/K19;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2671296
    new-instance v0, LX/K1F;

    invoke-direct {v0, p0, p1, p2}, LX/K1F;-><init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5rJ;LX/K19;)V

    invoke-virtual {p2, v0}, LX/K19;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2671297
    new-instance v0, LX/K1G;

    invoke-direct {v0, p0, p1, p2}, LX/K1G;-><init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/5rJ;LX/K19;)V

    invoke-virtual {p2, v0}, LX/K19;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2671298
    return-void
.end method

.method private a(LX/K19;)V
    .locals 0

    .prologue
    .line 2671299
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/BaseViewManager;->b(Landroid/view/View;)V

    .line 2671300
    invoke-virtual {p1}, LX/K19;->a()V

    .line 2671301
    return-void
.end method

.method private static a(LX/K19;I)V
    .locals 0

    .prologue
    .line 2671302
    packed-switch p1, :pswitch_data_0

    .line 2671303
    :goto_0
    return-void

    .line 2671304
    :pswitch_0
    invoke-virtual {p0}, LX/K19;->b()V

    goto :goto_0

    .line 2671305
    :pswitch_1
    invoke-virtual {p0}, LX/K19;->clearFocus()V

    .line 2671306
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/K19;II)V
    .locals 2

    .prologue
    .line 2671307
    iget v0, p0, LX/K19;->j:I

    move v0, v0

    .line 2671308
    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    or-int/2addr v0, p2

    .line 2671309
    iput v0, p0, LX/K19;->j:I

    .line 2671310
    return-void
.end method

.method private static a(LX/K19;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2671311
    instance-of v0, p1, [F

    if-eqz v0, :cond_1

    .line 2671312
    check-cast p1, [F

    check-cast p1, [F

    .line 2671313
    const/4 v0, 0x0

    aget v0, p1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    const/4 v2, 0x2

    aget v2, p1, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    const/4 v3, 0x3

    aget v3, p1, v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, LX/K19;->setPadding(IIII)V

    .line 2671314
    :cond_0
    :goto_0
    return-void

    .line 2671315
    :cond_1
    instance-of v0, p1, LX/K10;

    if-eqz v0, :cond_0

    .line 2671316
    check-cast p1, LX/K10;

    .line 2671317
    iget-boolean v0, p1, LX/K10;->c:Z

    move v0, v0

    .line 2671318
    if-eqz v0, :cond_2

    .line 2671319
    iget-object v0, p1, LX/K10;->a:Landroid/text/Spannable;

    move-object v0, v0

    .line 2671320
    invoke-static {v0, p0}, LX/K13;->a(Landroid/text/Spannable;Landroid/widget/TextView;)V

    .line 2671321
    :cond_2
    invoke-virtual {p0, p1}, LX/K19;->a(LX/K10;)V

    goto :goto_0
.end method

.method private static b(LX/5rJ;)LX/K19;
    .locals 4

    .prologue
    .line 2671322
    new-instance v0, LX/K19;

    invoke-direct {v0, p0}, LX/K19;-><init>(Landroid/content/Context;)V

    .line 2671323
    invoke-virtual {v0}, LX/K19;->getInputType()I

    move-result v1

    .line 2671324
    const v2, -0x20001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, LX/K19;->setInputType(I)V

    .line 2671325
    const-string v1, "done"

    invoke-virtual {v0, v1}, LX/K19;->setReturnKeyType(Ljava/lang/String;)V

    .line 2671326
    const/4 v1, 0x0

    const/high16 v2, 0x41600000    # 14.0f

    invoke-static {v2}, LX/5r2;->b(F)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LX/K19;->setTextSize(IF)V

    .line 2671327
    return-object v0
.end method

.method private static b(LX/K19;)V
    .locals 2

    .prologue
    .line 2671328
    iget v0, p0, LX/K19;->j:I

    move v0, v0

    .line 2671329
    and-int/lit16 v0, v0, 0x3002

    if-eqz v0, :cond_0

    .line 2671330
    iget v0, p0, LX/K19;->j:I

    move v0, v0

    .line 2671331
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    .line 2671332
    const/16 v0, 0x80

    const/16 v1, 0x10

    invoke-static {p0, v0, v1}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671333
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2671334
    invoke-static {p1}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->b(LX/5rJ;)LX/K19;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2671335
    check-cast p2, LX/K19;

    invoke-direct {p0, p1, p2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/5rJ;LX/K19;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2671336
    check-cast p1, LX/K19;

    invoke-static {p1, p2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;I)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2671337
    check-cast p1, LX/K19;

    invoke-static {p1, p2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2671344
    check-cast p1, LX/K19;

    invoke-direct {p0, p1}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2671369
    const-string v0, "AndroidTextInput"

    return-object v0
.end method

.method public h()Lcom/facebook/react/uimanager/LayoutShadowNode;
    .locals 1

    .prologue
    .line 2671368
    new-instance v0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;

    invoke-direct {v0}, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;-><init>()V

    return-object v0
.end method

.method public i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/uimanager/LayoutShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2671367
    const-class v0, Lcom/facebook/react/views/textinput/ReactTextInputShadowNode;

    return-object v0
.end method

.method public synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2671364
    invoke-virtual {p0}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->h()Lcom/facebook/react/uimanager/LayoutShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public setAutoCapitalize(LX/K19;I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "autoCapitalize"
    .end annotation

    .prologue
    .line 2671362
    const/16 v0, 0x7000

    invoke-static {p1, v0, p2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671363
    return-void
.end method

.method public setAutoCorrect(LX/K19;Ljava/lang/Boolean;)V
    .locals 2
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "autoCorrect"
    .end annotation

    .prologue
    .line 2671359
    const v1, 0x88000

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x8000

    :goto_0
    invoke-static {p1, v1, v0}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671360
    return-void

    .line 2671361
    :cond_0
    const/high16 v0, 0x80000

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBlurOnSubmit(LX/K19;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "blurOnSubmit"
    .end annotation

    .prologue
    .line 2671357
    iput-boolean p2, p1, LX/K19;->l:Z

    .line 2671358
    return-void
.end method

.method public setBorderColor(LX/K19;ILjava/lang/Integer;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderColor",
            "borderLeftColor",
            "borderRightColor",
            "borderTopColor",
            "borderBottomColor"
        }
        customType = "Color"
    .end annotation

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 2671351
    if-nez p3, :cond_0

    move v1, v0

    .line 2671352
    :goto_0
    if-nez p3, :cond_1

    .line 2671353
    :goto_1
    sget-object v2, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a:[I

    aget v2, v2, p2

    invoke-virtual {p1, v2, v1, v0}, LX/K19;->a(IFF)V

    .line 2671354
    return-void

    .line 2671355
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0xffffff

    and-int/2addr v1, v2

    int-to-float v1, v1

    goto :goto_0

    .line 2671356
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    goto :goto_1
.end method

.method public setBorderRadius(LX/K19;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderRadius",
            "borderTopLeftRadius",
            "borderTopRightRadius",
            "borderBottomRightRadius",
            "borderBottomLeftRadius"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 2671338
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2671339
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 2671340
    :cond_0
    if-nez p2, :cond_1

    .line 2671341
    invoke-virtual {p1, p3}, LX/K19;->setBorderRadius(F)V

    .line 2671342
    :goto_0
    return-void

    .line 2671343
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, p3, v0}, LX/K19;->a(FI)V

    goto :goto_0
.end method

.method public setBorderStyle(LX/K19;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderStyle"
    .end annotation

    .prologue
    .line 2671349
    invoke-virtual {p1, p2}, LX/K19;->setBorderStyle(Ljava/lang/String;)V

    .line 2671350
    return-void
.end method

.method public setBorderWidth(LX/K19;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderWidth",
            "borderLeftWidth",
            "borderRightWidth",
            "borderTopWidth",
            "borderBottomWidth"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 2671345
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2671346
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 2671347
    :cond_0
    sget-object v0, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a:[I

    aget v0, v0, p2

    invoke-virtual {p1, v0, p3}, LX/K19;->a(IF)V

    .line 2671348
    return-void
.end method

.method public setColor(LX/K19;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "color"
    .end annotation

    .prologue
    .line 2671285
    if-nez p2, :cond_0

    .line 2671286
    invoke-virtual {p1}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/K0u;->b(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/K19;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 2671287
    :goto_0
    return-void

    .line 2671288
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/K19;->setTextColor(I)V

    goto :goto_0
.end method

.method public setDisableFullscreenUI(LX/K19;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "disableFullscreenUI"
    .end annotation

    .prologue
    .line 2671365
    invoke-virtual {p1, p2}, LX/K19;->setDisableFullscreenUI(Z)V

    .line 2671366
    return-void
.end method

.method public setEditable(LX/K19;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "editable"
    .end annotation

    .prologue
    .line 2671143
    invoke-virtual {p1, p2}, LX/K19;->setEnabled(Z)V

    .line 2671144
    return-void
.end method

.method public setFontFamily(LX/K19;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontFamily"
    .end annotation

    .prologue
    .line 2671225
    const/4 v0, 0x0

    .line 2671226
    invoke-virtual {p1}, LX/K19;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2671227
    invoke-virtual {p1}, LX/K19;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 2671228
    :cond_0
    invoke-static {p2, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2671229
    invoke-virtual {p1, v0}, LX/K19;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2671230
    return-void
.end method

.method public setFontSize(LX/K19;F)V
    .locals 4
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = 14.0f
        name = "fontSize"
    .end annotation

    .prologue
    .line 2671223
    const/4 v0, 0x0

    invoke-static {p2}, LX/5r2;->b(F)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LX/K19;->setTextSize(IF)V

    .line 2671224
    return-void
.end method

.method public setFontStyle(LX/K19;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontStyle"
    .end annotation

    .prologue
    .line 2671212
    const/4 v0, -0x1

    .line 2671213
    const-string v1, "italic"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2671214
    const/4 v0, 0x2

    .line 2671215
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/K19;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 2671216
    if-nez v1, :cond_1

    .line 2671217
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2671218
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 2671219
    invoke-virtual {p1, v1, v0}, LX/K19;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2671220
    :cond_2
    return-void

    .line 2671221
    :cond_3
    const-string v1, "normal"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2671222
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFontWeight(LX/K19;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontWeight"
    .end annotation

    .prologue
    const/16 v3, 0x1f4

    const/4 v0, -0x1

    .line 2671200
    if-eqz p2, :cond_4

    invoke-static {p2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(Ljava/lang/String;)I

    move-result v1

    .line 2671201
    :goto_0
    if-ge v1, v3, :cond_0

    const-string v2, "bold"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2671202
    :cond_0
    const/4 v0, 0x1

    .line 2671203
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/K19;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 2671204
    if-nez v1, :cond_2

    .line 2671205
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2671206
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v2

    if-eq v0, v2, :cond_3

    .line 2671207
    invoke-virtual {p1, v1, v0}, LX/K19;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2671208
    :cond_3
    return-void

    :cond_4
    move v1, v0

    .line 2671209
    goto :goto_0

    .line 2671210
    :cond_5
    const-string v2, "normal"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    if-eq v1, v0, :cond_1

    if-ge v1, v3, :cond_1

    .line 2671211
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setInlineImageLeft(LX/K19;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "inlineImageLeft"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2671197
    invoke-static {}, LX/9nS;->a()LX/9nS;

    move-result-object v0

    invoke-virtual {p1}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/9nS;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 2671198
    invoke-virtual {p1, v0, v2, v2, v2}, LX/K19;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2671199
    return-void
.end method

.method public setInlineImagePadding(LX/K19;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "inlineImagePadding"
    .end annotation

    .prologue
    .line 2671195
    invoke-virtual {p1, p2}, LX/K19;->setCompoundDrawablePadding(I)V

    .line 2671196
    return-void
.end method

.method public setKeyboardType(LX/K19;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "keyboardType"
    .end annotation

    .prologue
    .line 2671185
    const/4 v0, 0x1

    .line 2671186
    const-string v1, "numeric"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2671187
    const/16 v0, 0x3002

    .line 2671188
    :cond_0
    :goto_0
    const/16 v1, 0x3023

    invoke-static {p1, v1, v0}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671189
    invoke-static {p1}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->b(LX/K19;)V

    .line 2671190
    return-void

    .line 2671191
    :cond_1
    const-string v1, "email-address"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2671192
    const/16 v0, 0x21

    goto :goto_0

    .line 2671193
    :cond_2
    const-string v1, "phone-pad"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2671194
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public setMaxLength(LX/K19;Ljava/lang/Integer;)V
    .locals 6
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "maxLength"
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2671161
    invoke-virtual {p1}, LX/K19;->getFilters()[Landroid/text/InputFilter;

    move-result-object v4

    .line 2671162
    sget-object v1, Lcom/facebook/react/views/textinput/ReactTextInputManager;->b:[Landroid/text/InputFilter;

    .line 2671163
    if-nez p2, :cond_2

    .line 2671164
    array-length v2, v4

    if-lez v2, :cond_5

    .line 2671165
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 2671166
    :goto_0
    array-length v3, v4

    if-ge v0, v3, :cond_1

    .line 2671167
    aget-object v3, v4, v0

    instance-of v3, v3, Landroid/text/InputFilter$LengthFilter;

    if-nez v3, :cond_0

    .line 2671168
    aget-object v3, v4, v0

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2671169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2671170
    :cond_1
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2671171
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/text/InputFilter;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    check-cast v0, [Landroid/text/InputFilter;

    .line 2671172
    :goto_1
    invoke-virtual {p1, v0}, LX/K19;->setFilters([Landroid/text/InputFilter;)V

    .line 2671173
    return-void

    .line 2671174
    :cond_2
    array-length v1, v4

    if-lez v1, :cond_6

    move v1, v0

    move v2, v0

    .line 2671175
    :goto_2
    array-length v5, v4

    if-ge v1, v5, :cond_4

    .line 2671176
    aget-object v5, v4, v1

    instance-of v5, v5, Landroid/text/InputFilter$LengthFilter;

    if-eqz v5, :cond_3

    .line 2671177
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v2, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v4, v1

    move v2, v3

    .line 2671178
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2671179
    :cond_4
    if-nez v2, :cond_7

    .line 2671180
    array-length v1, v4

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    .line 2671181
    array-length v2, v4

    invoke-static {v4, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2671182
    array-length v0, v4

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v4, v0

    :cond_5
    :goto_3
    move-object v0, v1

    goto :goto_1

    .line 2671183
    :cond_6
    new-array v1, v3, [Landroid/text/InputFilter;

    .line 2671184
    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v0

    goto :goto_3

    :cond_7
    move-object v0, v4

    goto :goto_1
.end method

.method public setMultiline(LX/K19;Z)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "multiline"
    .end annotation

    .prologue
    const/high16 v0, 0x20000

    const/4 v1, 0x0

    .line 2671231
    if-eqz p2, :cond_0

    move v2, v1

    :goto_0
    if-eqz p2, :cond_1

    :goto_1
    invoke-static {p1, v2, v0}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671232
    return-void

    :cond_0
    move v2, v0

    .line 2671233
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public setNumLines(LX/K19;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x1
        name = "numberOfLines"
    .end annotation

    .prologue
    .line 2671159
    invoke-virtual {p1, p2}, LX/K19;->setLines(I)V

    .line 2671160
    return-void
.end method

.method public setOnContentSizeChange(LX/K19;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "onContentSizeChange"
    .end annotation

    .prologue
    .line 2671152
    if-eqz p2, :cond_0

    .line 2671153
    new-instance v0, LX/K1H;

    invoke-direct {v0, p0, p1}, LX/K1H;-><init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/K19;)V

    .line 2671154
    iput-object v0, p1, LX/K19;->p:LX/K15;

    .line 2671155
    :goto_0
    return-void

    .line 2671156
    :cond_0
    const/4 v0, 0x0

    .line 2671157
    iput-object v0, p1, LX/K19;->p:LX/K15;

    .line 2671158
    goto :goto_0
.end method

.method public setOnSelectionChange(LX/K19;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "onSelectionChange"
    .end annotation

    .prologue
    .line 2671145
    if-eqz p2, :cond_0

    .line 2671146
    new-instance v0, LX/K1J;

    invoke-direct {v0, p0, p1}, LX/K1J;-><init>(Lcom/facebook/react/views/textinput/ReactTextInputManager;LX/K19;)V

    .line 2671147
    iput-object v0, p1, LX/K19;->o:LX/K1I;

    .line 2671148
    :goto_0
    return-void

    .line 2671149
    :cond_0
    const/4 v0, 0x0

    .line 2671150
    iput-object v0, p1, LX/K19;->o:LX/K1I;

    .line 2671151
    goto :goto_0
.end method

.method public setPlaceholder(LX/K19;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "placeholder"
    .end annotation

    .prologue
    .line 2671141
    invoke-virtual {p1, p2}, LX/K19;->setHint(Ljava/lang/CharSequence;)V

    .line 2671142
    return-void
.end method

.method public setPlaceholderTextColor(LX/K19;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "placeholderTextColor"
    .end annotation

    .prologue
    .line 2671234
    if-nez p2, :cond_0

    .line 2671235
    invoke-virtual {p1}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/K0u;->a(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/K19;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 2671236
    :goto_0
    return-void

    .line 2671237
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/K19;->setHintTextColor(I)V

    goto :goto_0
.end method

.method public setReturnKeyLabel(LX/K19;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "returnKeyLabel"
    .end annotation

    .prologue
    .line 2671238
    const/16 v0, 0x670

    invoke-virtual {p1, p2, v0}, LX/K19;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 2671239
    return-void
.end method

.method public setReturnKeyType(LX/K19;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "returnKeyType"
    .end annotation

    .prologue
    .line 2671240
    invoke-virtual {p1, p2}, LX/K19;->setReturnKeyType(Ljava/lang/String;)V

    .line 2671241
    return-void
.end method

.method public setSecureTextEntry(LX/K19;Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "secureTextEntry"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2671242
    if-eqz p2, :cond_1

    move v1, v0

    :goto_0
    if-eqz p2, :cond_0

    const/16 v0, 0x80

    :cond_0
    invoke-static {p1, v1, v0}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->a(LX/K19;II)V

    .line 2671243
    invoke-static {p1}, Lcom/facebook/react/views/textinput/ReactTextInputManager;->b(LX/K19;)V

    .line 2671244
    return-void

    .line 2671245
    :cond_1
    const/16 v1, 0x90

    goto :goto_0
.end method

.method public setSelectTextOnFocus(LX/K19;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = false
        name = "selectTextOnFocus"
    .end annotation

    .prologue
    .line 2671246
    invoke-virtual {p1, p2}, LX/K19;->setSelectAllOnFocus(Z)V

    .line 2671247
    return-void
.end method

.method public setSelection(LX/K19;LX/5pG;)V
    .locals 2
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "selection"
    .end annotation

    .prologue
    .line 2671248
    if-nez p2, :cond_1

    .line 2671249
    :cond_0
    :goto_0
    return-void

    .line 2671250
    :cond_1
    const-string v0, "start"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "end"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2671251
    const-string v0, "start"

    invoke-interface {p2, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "end"

    invoke-interface {p2, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/K19;->setSelection(II)V

    goto :goto_0
.end method

.method public setSelectionColor(LX/K19;Ljava/lang/Integer;)V
    .locals 1
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "selectionColor"
    .end annotation

    .prologue
    .line 2671252
    if-nez p2, :cond_0

    .line 2671253
    invoke-virtual {p1}, LX/K19;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/K0u;->c(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p1, v0}, LX/K19;->setHighlightColor(I)V

    .line 2671254
    :goto_0
    return-void

    .line 2671255
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/K19;->setHighlightColor(I)V

    goto :goto_0
.end method

.method public setTextAlign(LX/K19;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textAlign"
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 2671256
    if-eqz p2, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2671257
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K19;->setGravityHorizontal(I)V

    .line 2671258
    :goto_0
    return-void

    .line 2671259
    :cond_1
    const-string v0, "left"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2671260
    invoke-virtual {p1, v1}, LX/K19;->setGravityHorizontal(I)V

    goto :goto_0

    .line 2671261
    :cond_2
    const-string v0, "right"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2671262
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/K19;->setGravityHorizontal(I)V

    goto :goto_0

    .line 2671263
    :cond_3
    const-string v0, "center"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2671264
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/K19;->setGravityHorizontal(I)V

    goto :goto_0

    .line 2671265
    :cond_4
    const-string v0, "justify"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2671266
    invoke-virtual {p1, v1}, LX/K19;->setGravityHorizontal(I)V

    goto :goto_0

    .line 2671267
    :cond_5
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid textAlign: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setTextAlignVertical(LX/K19;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textAlignVertical"
    .end annotation

    .prologue
    .line 2671268
    if-eqz p2, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2671269
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/K19;->setGravityVertical(I)V

    .line 2671270
    :goto_0
    return-void

    .line 2671271
    :cond_1
    const-string v0, "top"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2671272
    const/16 v0, 0x30

    invoke-virtual {p1, v0}, LX/K19;->setGravityVertical(I)V

    goto :goto_0

    .line 2671273
    :cond_2
    const-string v0, "bottom"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2671274
    const/16 v0, 0x50

    invoke-virtual {p1, v0}, LX/K19;->setGravityVertical(I)V

    goto :goto_0

    .line 2671275
    :cond_3
    const-string v0, "center"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2671276
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, LX/K19;->setGravityVertical(I)V

    goto :goto_0

    .line 2671277
    :cond_4
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid textAlignVertical: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setUnderlineColor(LX/K19;Ljava/lang/Integer;)V
    .locals 3
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "underlineColorAndroid"
    .end annotation

    .prologue
    .line 2671278
    if-nez p2, :cond_0

    .line 2671279
    invoke-virtual {p1}, LX/K19;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2671280
    :goto_0
    return-void

    .line 2671281
    :cond_0
    invoke-virtual {p1}, LX/K19;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method public final v()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2671282
    const-string v0, "focusTextInput"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "blurTextInput"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2671283
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topSubmitEditing"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onSubmitEditing"

    const-string v5, "captured"

    const-string v6, "onSubmitEditingCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topEndEditing"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onEndEditing"

    const-string v5, "captured"

    const-string v6, "onEndEditingCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topTextInput"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onTextInput"

    const-string v5, "captured"

    const-string v6, "onTextInputCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topFocus"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onFocus"

    const-string v5, "captured"

    const-string v6, "onFocusCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    const-string v1, "topBlur"

    const-string v2, "phasedRegistrationNames"

    const-string v3, "bubbled"

    const-string v4, "onBlur"

    const-string v5, "captured"

    const-string v6, "onBlurCapture"

    invoke-static {v3, v4, v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/util/Map;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2671284
    const-string v8, "AutoCapitalizationType"

    const-string v0, "none"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "characters"

    const/16 v3, 0x1000

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "words"

    const/16 v5, 0x2000

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "sentences"

    const/16 v7, 0x4000

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v8, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
