.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2556717
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2556718
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2556715
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2556716
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2556682
    if-nez p1, :cond_0

    .line 2556683
    :goto_0
    return v0

    .line 2556684
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2556685
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2556686
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v1

    .line 2556687
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2556688
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 2556689
    const v3, -0x3101eb87

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2556690
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 2556691
    const v4, 0x6418fceb

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2556692
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v4

    .line 2556693
    const v5, 0x2756542b

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 2556694
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2556695
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2556696
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2556697
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2556698
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2556699
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2556700
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2556701
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2556702
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2556703
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2556704
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2556705
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2556706
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2556707
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2556708
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2556709
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2556710
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2556711
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2556712
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2556713
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2556714
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3101eb87 -> :sswitch_1
        0x10cab9f -> :sswitch_0
        0x2756542b -> :sswitch_3
        0x6418fceb -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2556681
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2556672
    sparse-switch p2, :sswitch_data_0

    .line 2556673
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2556674
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2556675
    const v1, -0x3101eb87

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2556676
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2556677
    const v1, 0x6418fceb

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2556678
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2556679
    const v1, 0x2756542b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2556680
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3101eb87 -> :sswitch_1
        0x10cab9f -> :sswitch_0
        0x2756542b -> :sswitch_1
        0x6418fceb -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2556666
    if-eqz p1, :cond_0

    .line 2556667
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2556668
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;

    .line 2556669
    if-eq v0, v1, :cond_0

    .line 2556670
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2556671
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2556665
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2556632
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2556633
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2556660
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2556661
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2556662
    :cond_0
    iput-object p1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2556663
    iput p2, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->b:I

    .line 2556664
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2556659
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2556658
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2556655
    iget v0, p0, LX/1vt;->c:I

    .line 2556656
    move v0, v0

    .line 2556657
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2556652
    iget v0, p0, LX/1vt;->c:I

    .line 2556653
    move v0, v0

    .line 2556654
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2556649
    iget v0, p0, LX/1vt;->b:I

    .line 2556650
    move v0, v0

    .line 2556651
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2556646
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2556647
    move-object v0, v0

    .line 2556648
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2556637
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2556638
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2556639
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2556640
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2556641
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2556642
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2556643
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2556644
    invoke-static {v3, v9, v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2556645
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2556634
    iget v0, p0, LX/1vt;->c:I

    .line 2556635
    move v0, v0

    .line 2556636
    return v0
.end method
