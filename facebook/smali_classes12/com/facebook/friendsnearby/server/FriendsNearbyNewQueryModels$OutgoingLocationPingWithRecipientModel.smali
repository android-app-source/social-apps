.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x773038b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLLocationPingType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559691
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559690
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2559688
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2559689
    return-void
.end method

.method private j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559686
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    .line 2559687
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2559675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559676
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2559677
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->b()Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2559678
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2559679
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2559680
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2559681
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2559682
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2559683
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->h:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 2559684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2559667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559668
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2559669
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    .line 2559670
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2559671
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;

    .line 2559672
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    .line 2559673
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559674
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559665
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->e:Ljava/lang/String;

    .line 2559666
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2559662
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2559663
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->h:I

    .line 2559664
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLLocationPingType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559660
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->f:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->f:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    .line 2559661
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->f:Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2559652
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;-><init>()V

    .line 2559653
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2559654
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559659
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2559657
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2559658
    iget v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2559656
    const v0, 0x6ffdf439

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2559655
    const v0, 0x65006e7

    return v0
.end method
