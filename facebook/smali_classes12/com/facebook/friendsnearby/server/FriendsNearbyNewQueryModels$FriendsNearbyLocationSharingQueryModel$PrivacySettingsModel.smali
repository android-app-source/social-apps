.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6e7d1685
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558010
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558011
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2558008
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2558009
    return-void
.end method

.method private j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558006
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->e:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->e:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    .line 2558007
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->e:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2558012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2558013
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2558014
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2558015
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2558016
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558017
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2557998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2557999
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2558000
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    .line 2558001
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2558002
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;

    .line 2558003
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->e:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    .line 2558004
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558005
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557997
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;->j()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2557994
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;-><init>()V

    .line 2557995
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2557996
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2557992
    const v0, -0x4776fb76

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2557993
    const v0, 0x36cd6060

    return v0
.end method
