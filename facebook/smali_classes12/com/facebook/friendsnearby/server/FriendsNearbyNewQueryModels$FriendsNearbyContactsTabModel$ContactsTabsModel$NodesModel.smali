.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2db795ba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2556777
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2556776
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2556752
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2556753
    return-void
.end method

.method private j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2556774
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    .line 2556775
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2556768
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2556769
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2556770
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2556771
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2556772
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2556773
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2556760
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2556761
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2556762
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    .line 2556763
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2556764
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;

    .line 2556765
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    .line 2556766
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2556767
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2556759
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2556756
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;-><init>()V

    .line 2556757
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2556758
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2556755
    const v0, 0x6da1eeaf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2556754
    const v0, 0x65e74a22

    return v0
.end method
