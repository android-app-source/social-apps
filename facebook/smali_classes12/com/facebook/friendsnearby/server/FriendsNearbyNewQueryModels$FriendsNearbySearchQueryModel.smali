.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x264fb2f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559230
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559229
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2559227
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2559228
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559224
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2559225
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2559226
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2559216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559217
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2559218
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2559219
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2559220
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2559221
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2559222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559223
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2559197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559198
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2559199
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    .line 2559200
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2559201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;

    .line 2559202
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    .line 2559203
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559204
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2559215
    new-instance v0, LX/IH3;

    invoke-direct {v0, p1}, LX/IH3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559213
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    .line 2559214
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2559211
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2559212
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2559210
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2559207
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;-><init>()V

    .line 2559208
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2559209
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2559206
    const v0, -0x7153dc30

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2559205
    const v0, 0x3c2b9d5

    return v0
.end method
