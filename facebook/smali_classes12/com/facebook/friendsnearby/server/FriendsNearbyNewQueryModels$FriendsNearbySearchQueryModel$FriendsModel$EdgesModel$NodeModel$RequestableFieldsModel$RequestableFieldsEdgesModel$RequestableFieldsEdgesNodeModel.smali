.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x15e578d0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558955
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558954
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2558952
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2558953
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V
    .locals 4

    .prologue
    .line 2558945
    iput-object p1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2558946
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2558947
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2558948
    if-eqz v0, :cond_0

    .line 2558949
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2558950
    :cond_0
    return-void

    .line 2558951
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558943
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->e:Ljava/lang/String;

    .line 2558944
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2558935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2558936
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2558937
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2558938
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2558939
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2558940
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2558941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2558932
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2558933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558934
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2558956
    new-instance v0, LX/IH2;

    invoke-direct {v0, p1}, LX/IH2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558931
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2558925
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2558926
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2558927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2558928
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2558929
    :goto_0
    return-void

    .line 2558930
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2558922
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2558923
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V

    .line 2558924
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2558919
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;-><init>()V

    .line 2558920
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2558921
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2558918
    const v0, 0x270c7f39

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2558917
    const v0, -0x51dd9867

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558915
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2558916
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel$RequestableFieldsModel$RequestableFieldsEdgesModel$RequestableFieldsEdgesNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    return-object v0
.end method
