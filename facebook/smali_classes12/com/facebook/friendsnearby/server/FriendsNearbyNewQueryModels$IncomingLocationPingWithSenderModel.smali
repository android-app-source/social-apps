.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x266f6dde
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559552
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2559551
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2559549
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2559550
    return-void
.end method

.method private j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559547
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    .line 2559548
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559545
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2559546
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559543
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    .line 2559544
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2559530
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559531
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2559532
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2559533
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2559534
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2559535
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2559536
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2559537
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2559538
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2559539
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2559540
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2559541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559542
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2559512
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2559513
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2559514
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    .line 2559515
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2559516
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;

    .line 2559517
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    .line 2559518
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2559519
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2559520
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2559521
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;

    .line 2559522
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2559523
    :cond_1
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2559524
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    .line 2559525
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2559526
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;

    .line 2559527
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    .line 2559528
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2559529
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559511
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2559497
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2559498
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->g:J

    .line 2559499
    return-void
.end method

.method public final synthetic b()LX/1k1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559510
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2559507
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;-><init>()V

    .line 2559508
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2559509
    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 2559505
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2559506
    iget-wide v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->g:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559503
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->h:Ljava/lang/String;

    .line 2559504
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2559502
    const v0, 0x45ddfc9b

    return v0
.end method

.method public final synthetic e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2559501
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2559500
    const v0, 0x65006e7

    return v0
.end method
