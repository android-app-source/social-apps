.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x171c8894
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2557696
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2557695
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2557657
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2557658
    return-void
.end method

.method private j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557693
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    .line 2557694
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    return-object v0
.end method

.method private k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557691
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    .line 2557692
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2557681
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2557682
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2557683
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2557684
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 2557685
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2557686
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2557687
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2557688
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2557689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2557690
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2557668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2557669
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2557670
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    .line 2557671
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2557672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    .line 2557673
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->e:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    .line 2557674
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2557675
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    .line 2557676
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2557677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    .line 2557678
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    .line 2557679
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2557680
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557667
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557666
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2557663
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;-><init>()V

    .line 2557664
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2557665
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2557661
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->g:Ljava/util/List;

    .line 2557662
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2557660
    const v0, -0x7d0ae5ca

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2557659
    const v0, 0x18ca2c8a

    return v0
.end method
