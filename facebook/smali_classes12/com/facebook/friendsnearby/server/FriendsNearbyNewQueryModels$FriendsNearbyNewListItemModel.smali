.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6b17a8fd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558521
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2558526
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2558524
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2558525
    return-void
.end method

.method private j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558522
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    .line 2558523
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    return-object v0
.end method

.method private k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558519
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    .line 2558520
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558517
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->h:Ljava/lang/String;

    .line 2558518
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558515
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    .line 2558516
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    return-object v0
.end method

.method private n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558513
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    .line 2558514
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2558497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2558498
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->b()Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2558499
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2558500
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2558501
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2558502
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2558503
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2558504
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2558505
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2558506
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2558507
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2558508
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2558509
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2558510
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2558511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558512
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2558527
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2558528
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2558529
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    .line 2558530
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2558531
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2558532
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->f:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    .line 2558533
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2558534
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    .line 2558535
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2558536
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2558537
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->g:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    .line 2558538
    :cond_1
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2558539
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    .line 2558540
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2558541
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2558542
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->i:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    .line 2558543
    :cond_2
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2558544
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    .line 2558545
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2558546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2558547
    iput-object v0, v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    .line 2558548
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2558549
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558496
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558494
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->e:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->e:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    .line 2558495
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->e:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2558491
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;-><init>()V

    .line 2558492
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2558493
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558490
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558489
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2558485
    const v0, 0x2955d90a

    return v0
.end method

.method public final synthetic e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558488
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->m()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2558487
    const v0, 0x21f26a2

    return v0
.end method

.method public final synthetic mH_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2558486
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->n()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v0

    return-object v0
.end method
