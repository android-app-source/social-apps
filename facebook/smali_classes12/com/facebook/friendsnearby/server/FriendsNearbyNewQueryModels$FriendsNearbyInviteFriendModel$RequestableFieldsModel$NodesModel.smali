.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x15e578d0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2557212
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2557171
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2557210
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2557211
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V
    .locals 4

    .prologue
    .line 2557203
    iput-object p1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2557204
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2557205
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2557206
    if-eqz v0, :cond_0

    .line 2557207
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2557208
    :cond_0
    return-void

    .line 2557209
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557201
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->e:Ljava/lang/String;

    .line 2557202
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2557193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2557194
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2557195
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2557196
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2557197
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2557198
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2557199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2557200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2557190
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2557191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2557192
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2557189
    new-instance v0, LX/IGw;

    invoke-direct {v0, p1}, LX/IGw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557188
    invoke-direct {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2557182
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2557183
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2557184
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2557185
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2557186
    :goto_0
    return-void

    .line 2557187
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2557179
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2557180
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V

    .line 2557181
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2557176
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;-><init>()V

    .line 2557177
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2557178
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2557175
    const v0, -0x2036ee38

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2557174
    const v0, -0x51dd9867

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2557172
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2557173
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    return-object v0
.end method
