.class public Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2556504
    new-instance v0, LX/IGj;

    invoke-direct {v0}, LX/IGj;-><init>()V

    sput-object v0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2556493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556494
    const-string v0, "CURRENT_LOCATION"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->b:LX/0Px;

    .line 2556495
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->a:Ljava/lang/String;

    .line 2556496
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2556500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556501
    const-string v0, "CURRENT_LOCATION"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->b:LX/0Px;

    .line 2556502
    iput-object p1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->a:Ljava/lang/String;

    .line 2556503
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2556499
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2556497
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2556498
    return-void
.end method
