.class public final Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2557030
    const-class v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    new-instance v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2557031
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2557032
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2557033
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2557034
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2557035
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2557036
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2557037
    if-eqz v2, :cond_0

    .line 2557038
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557039
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2557040
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2557041
    if-eqz v2, :cond_1

    .line 2557042
    const-string p0, "can_viewer_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557043
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2557044
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2557045
    if-eqz v2, :cond_2

    .line 2557046
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557047
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2557048
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2557049
    if-eqz v2, :cond_3

    .line 2557050
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557051
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2557052
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2557053
    if-eqz v2, :cond_4

    .line 2557054
    const-string p0, "nearby_friends_contacts_set_item"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557055
    invoke-static {v1, v2, p1, p2}, LX/IHC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2557056
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2557057
    if-eqz v2, :cond_5

    .line 2557058
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2557059
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2557060
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2557061
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2557062
    check-cast p1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$Serializer;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
