.class public Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2556534
    new-instance v0, LX/IGl;

    invoke-direct {v0}, LX/IGl;-><init>()V

    sput-object v0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2556535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556536
    const-string v0, "CURRENT_LOCATION"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->c:LX/0Px;

    .line 2556537
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->a:Ljava/lang/String;

    .line 2556538
    const/4 v0, 0x1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->b:LX/0Px;

    .line 2556539
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2556540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556541
    const-string v0, "CURRENT_LOCATION"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->c:LX/0Px;

    .line 2556542
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->a:Ljava/lang/String;

    .line 2556543
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->b:LX/0Px;

    .line 2556544
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2556545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556546
    const-string v0, "CURRENT_LOCATION"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->c:LX/0Px;

    .line 2556547
    iput-object p1, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->a:Ljava/lang/String;

    .line 2556548
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2556549
    iput-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->b:LX/0Px;

    .line 2556550
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2556551
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2556552
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2556553
    iget-object v0, p0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2556554
    return-void
.end method
