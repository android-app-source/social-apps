.class public Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final u:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Landroid/widget/TextView;

.field public B:Landroid/widget/TextView;

.field private C:Landroid/widget/Button;

.field private D:Landroid/widget/Button;

.field private E:Ljava/lang/String;

.field public p:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field public x:Lcom/facebook/maps/FbStaticMapView;

.field public y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2553687
    const-class v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    const-string v1, "friends_nearby"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->u:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2553686
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;LX/1Ck;LX/0tX;LX/0wM;Lcom/facebook/content/SecureContextHelper;LX/03V;)V
    .locals 0

    .prologue
    .line 2553685
    iput-object p1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->p:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->q:LX/0tX;

    iput-object p3, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->r:LX/0wM;

    iput-object p4, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    invoke-static {v5}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v5}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->a(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;LX/1Ck;LX/0tX;LX/0wM;Lcom/facebook/content/SecureContextHelper;LX/03V;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2553644
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "section_title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2553645
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->v:Landroid/view/View;

    new-instance v1, LX/IF8;

    invoke-direct {v1, p0}, LX/IF8;-><init>(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2553646
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->r:LX/0wM;

    const v1, 0x7f020742

    const v2, 0x7f0a00e6

    invoke-static {p0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2553647
    iget-object v1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->C:Landroid/widget/Button;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2553648
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->C:Landroid/widget/Button;

    new-instance v1, LX/IF9;

    invoke-direct {v1, p0}, LX/IF9;-><init>(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2553649
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->r:LX/0wM;

    const v1, 0x7f02089c

    const v2, 0x7f0a00e6

    invoke-static {p0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2553650
    iget-object v1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->D:Landroid/widget/Button;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2553651
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->D:Landroid/widget/Button;

    new-instance v1, LX/IFA;

    invoke-direct {v1, p0}, LX/IFA;-><init>(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2553652
    return-void
.end method

.method public static l(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V
    .locals 2

    .prologue
    .line 2553680
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2553681
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2553682
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2553683
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2553684
    return-void
.end method

.method public static m(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V
    .locals 2

    .prologue
    .line 2553675
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2553676
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2553677
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2553678
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2553679
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2553669
    new-instance v0, LX/IFC;

    invoke-direct {v0}, LX/IFC;-><init>()V

    move-object v0, v0

    .line 2553670
    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2553671
    const-string v1, "image_size"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2553672
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2553673
    iget-object v1, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->p:LX/1Ck;

    const-string v2, "fetch_friend_info"

    iget-object v3, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->q:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/IFB;

    invoke-direct {v3, p0}, LX/IFB;-><init>(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2553674
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2553653
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2553654
    invoke-static {p0, p0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2553655
    const v0, 0x7f03040b

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->setContentView(I)V

    .line 2553656
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->E:Ljava/lang/String;

    .line 2553657
    const v0, 0x7f0d0c57

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->v:Landroid/view/View;

    .line 2553658
    const v0, 0x7f0d0c58

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->w:Landroid/widget/TextView;

    .line 2553659
    const v0, 0x7f0d0c59

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->x:Lcom/facebook/maps/FbStaticMapView;

    .line 2553660
    const v0, 0x7f0d0c5a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2553661
    const v0, 0x7f0d0c5b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->z:Landroid/widget/TextView;

    .line 2553662
    const v0, 0x7f0d0c5c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->A:Landroid/widget/TextView;

    .line 2553663
    const v0, 0x7f0d0c5d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->B:Landroid/widget/TextView;

    .line 2553664
    const v0, 0x7f0d0c5e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->C:Landroid/widget/Button;

    .line 2553665
    const v0, 0x7f0d0c5f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->D:Landroid/widget/Button;

    .line 2553666
    invoke-direct {p0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->b()V

    .line 2553667
    invoke-direct {p0}, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->n()V

    .line 2553668
    return-void
.end method
