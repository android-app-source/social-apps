.class public final Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x607eb65e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2553941
    const-class v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2553940
    const-class v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2553938
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2553939
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553935
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2553936
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2553937
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2553921
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2553922
    invoke-direct {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2553923
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x3b9dadc7

    invoke-static {v2, v1, v3}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2553924
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2553925
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2553926
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2553927
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2553928
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2553929
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2553930
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2553931
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2553932
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2553933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2553934
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2553901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2553902
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2553903
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3b9dadc7

    invoke-static {v2, v0, v3}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2553904
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2553905
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    .line 2553906
    iput v3, v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->f:I

    move-object v1, v0

    .line 2553907
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2553908
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    .line 2553909
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2553910
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    .line 2553911
    iput-object v0, v1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->h:Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    .line 2553912
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2553913
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2553914
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2553915
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    .line 2553916
    iput-object v0, v1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2553917
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2553918
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2553919
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2553920
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentApproximateLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553899
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2553900
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2553898
    new-instance v0, LX/IFE;

    invoke-direct {v0, p1}, LX/IFE;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2553942
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2553943
    const/4 v0, 0x1

    const v1, 0x3b9dadc7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->f:I

    .line 2553944
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2553896
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2553897
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2553895
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2553892
    new-instance v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;-><init>()V

    .line 2553893
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2553894
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2553891
    const v0, 0x48c7cd3c    # 409193.88f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2553890
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553888
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->g:Ljava/lang/String;

    .line 2553889
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553886
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->h:Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->h:Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    .line 2553887
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->h:Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553884
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2553885
    iget-object v0, p0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
