.class public final Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2553882
    const-class v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    new-instance v1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2553883
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2553881
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 2553846
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2553847
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2553848
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2553849
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2553850
    if-eqz v2, :cond_0

    .line 2553851
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553852
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2553853
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2553854
    if-eqz v2, :cond_3

    .line 2553855
    const-string v3, "current_approximate_location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553856
    const-wide/16 v8, 0x0

    .line 2553857
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2553858
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2553859
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_1

    .line 2553860
    const-string v6, "latitude"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553861
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 2553862
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2553863
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_2

    .line 2553864
    const-string v6, "longitude"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553865
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 2553866
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2553867
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2553868
    if-eqz v2, :cond_4

    .line 2553869
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553870
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2553871
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2553872
    if-eqz v2, :cond_5

    .line 2553873
    const-string v3, "nearby_friends_contacts_set_item"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553874
    invoke-static {v1, v2, p1, p2}, LX/IFH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2553875
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2553876
    if-eqz v2, :cond_6

    .line 2553877
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553878
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2553879
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2553880
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2553845
    check-cast p1, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$Serializer;->a(Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
