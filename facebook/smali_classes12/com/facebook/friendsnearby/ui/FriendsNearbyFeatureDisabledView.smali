.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/GVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;

.field private c:Lcom/facebook/fbui/facepile/FacepileView;

.field private d:Landroid/view/ViewStub;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/facebook/fbui/popover/PopoverSpinner;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/Button;

.field private j:LX/GUB;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2561723
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2561724
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a()V

    .line 2561725
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2561720
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2561721
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a()V

    .line 2561722
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2561717
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2561718
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a()V

    .line 2561719
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2561703
    const v0, 0x7f03071b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2561704
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2561705
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setOrientation(I)V

    .line 2561706
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2561707
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setPadding(IIII)V

    .line 2561708
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0114

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2561709
    const v0, 0x7f0d1304

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->b:Landroid/widget/TextView;

    .line 2561710
    const v0, 0x7f0d1305

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2561711
    const v0, 0x7f0d1306

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->d:Landroid/view/ViewStub;

    .line 2561712
    const v0, 0x7f0d1308

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->f:Landroid/widget/TextView;

    .line 2561713
    const v0, 0x7f0d1309

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/popover/PopoverSpinner;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->g:Lcom/facebook/fbui/popover/PopoverSpinner;

    .line 2561714
    const v0, 0x7f0d130a    # 1.8752E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->h:Landroid/widget/TextView;

    .line 2561715
    const v0, 0x7f0d130b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->i:Landroid/widget/Button;

    .line 2561716
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    invoke-static {v0}, LX/GVF;->a(LX/0QB;)LX/GVF;

    move-result-object v0

    check-cast v0, LX/GVF;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a:LX/GVF;

    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2561690
    const/4 v0, 0x2

    if-ge p1, v0, :cond_1

    .line 2561691
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2561692
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2561693
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2561694
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->e:Landroid/widget/ImageView;

    .line 2561695
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2561696
    :cond_0
    :goto_0
    return-void

    .line 2561697
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2561698
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2561699
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a:LX/GVF;

    invoke-virtual {v1, p1, p2}, LX/GVF;->a(ILX/0Px;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2561700
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {v0, p2}, LX/GVB;->a(Lcom/facebook/fbui/facepile/FacepileView;LX/0Px;)V

    .line 2561701
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2561702
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2561652
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->h:Landroid/widget/TextView;

    if-nez p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2561653
    if-eqz p1, :cond_0

    .line 2561654
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2561655
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2561656
    :cond_0
    return-void

    .line 2561657
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2561687
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2561688
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->i:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2561689
    return-void
.end method

.method public getSelectedPrivacy()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2561682
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561683
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->g:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelectedItemPosition()I

    move-result v0

    .line 2561684
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2561685
    const/4 v0, 0x0

    .line 2561686
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    invoke-virtual {v1, v0}, LX/GUB;->a(I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v0

    goto :goto_0
.end method

.method public setPrivacyPicker(LX/0Px;)V
    .locals 10
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOptionEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 2561658
    if-nez p1, :cond_0

    const/16 v0, 0x8

    .line 2561659
    :goto_0
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2561660
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->g:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->setVisibility(I)V

    .line 2561661
    if-nez p1, :cond_1

    .line 2561662
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    .line 2561663
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2561664
    goto :goto_0

    .line 2561665
    :cond_1
    new-instance v0, LX/GUB;

    invoke-direct {v0}, LX/GUB;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    .line 2561666
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2561667
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v5, v1

    move v2, v1

    move v3, v4

    :goto_2
    if-ge v5, v7, :cond_2

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;

    .line 2561668
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->c()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 2561669
    const-string v9, "{\"value\":\"SELF\"}"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 2561670
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->c()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2561671
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 2561672
    :goto_3
    const-string v3, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v2

    .line 2561673
    :goto_4
    add-int/lit8 v2, v2, 0x1

    .line 2561674
    :goto_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_2

    .line 2561675
    :cond_2
    if-ltz v3, :cond_3

    .line 2561676
    :goto_6
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GUB;->a(LX/0Px;)V

    .line 2561677
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->g:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->j:LX/GUB;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2561678
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->g:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/popover/PopoverSpinner;->setSelection(I)V

    goto :goto_1

    .line 2561679
    :cond_3
    if-ltz v4, :cond_4

    move v3, v4

    .line 2561680
    goto :goto_6

    :cond_4
    move v3, v1

    .line 2561681
    goto :goto_6

    :cond_5
    move v3, v4

    goto :goto_4

    :cond_6
    move v0, v3

    goto :goto_3

    :cond_7
    move v0, v3

    move v3, v4

    goto :goto_5
.end method
