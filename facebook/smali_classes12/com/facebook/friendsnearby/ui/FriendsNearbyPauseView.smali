.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IID;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2564022
    sget-object v0, LX/0ax;->dp:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "/tour/locationsharing/learnmore"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2564019
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2564020
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->b()V

    .line 2564021
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564016
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2564017
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->b()V

    .line 2564018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564013
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2564014
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->b()V

    .line 2564015
    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;LX/17W;LX/IID;)V
    .locals 0

    .prologue
    .line 2564012
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a:LX/17W;

    iput-object p2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->b:LX/IID;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-static {v1}, LX/IID;->a(LX/0QB;)LX/IID;

    move-result-object v1

    check-cast v1, LX/IID;

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;LX/17W;LX/IID;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2564002
    const v0, 0x7f030725

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2564003
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2564004
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->setOrientation(I)V

    .line 2564005
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0114

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2564006
    const v0, 0x7f0d1317

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    .line 2564007
    const v0, 0x7f0d1318

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->e:Landroid/widget/TextView;

    .line 2564008
    const v0, 0x7f0d1319

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->f:Landroid/widget/Button;

    .line 2564009
    const v0, 0x7f0d131a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->g:Landroid/widget/TextView;

    .line 2564010
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->c()V

    .line 2564011
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    .line 2563998
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083879

    const/4 v2, 0x1

    new-array v2, v2, [LX/47s;

    const/4 v3, 0x0

    new-instance v4, LX/47s;

    const v5, 0x7f08387a

    new-instance v6, LX/IJe;

    invoke-direct {v6, p0}, LX/IJe;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;)V

    const/16 v7, 0x21

    invoke-direct {v4, v5, v6, v7}, LX/47s;-><init>(ILjava/lang/Object;I)V

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v0

    .line 2563999
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2564000
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->g:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2564001
    return-void
.end method


# virtual methods
.method public final a(LX/IFx;LX/IJ4;)V
    .locals 1

    .prologue
    .line 2563996
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(LX/IFR;LX/IJ4;)V

    .line 2563997
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2563994
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2563995
    return-void
.end method

.method public setResumeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2563992
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563993
    return-void
.end method
