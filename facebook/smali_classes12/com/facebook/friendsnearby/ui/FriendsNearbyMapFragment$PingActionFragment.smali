.class public final Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/location/ImmutableLocation;

.field public l:Lcom/facebook/content/SecureContextHelper;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/IJZ;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/IID;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2563575
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2563576
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2563577
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2563578
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2563579
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IJZ;

    .line 2563580
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->k:Lcom/facebook/location/ImmutableLocation;

    iget-object v9, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v7, v8, v9}, LX/IJZ;->a(Landroid/content/Context;Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;

    move-result-object v7

    .line 2563581
    invoke-virtual {v7}, LX/0am;->isPresent()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2563582
    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2563583
    invoke-virtual {v0}, LX/IJZ;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2563584
    invoke-virtual {v0}, LX/IJZ;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2563585
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2563586
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2563587
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2563588
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v5, 0x7f083865

    invoke-virtual {v0, v5}, LX/0ju;->a(I)LX/0ju;

    move-result-object v5

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v2, LX/IJW;

    invoke-direct {v2, p0, v3, v1}, LX/IJW;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;LX/0Px;LX/0Px;)V

    invoke-virtual {v5, v0, v2}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2563589
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 2563590
    return-object v0
.end method
