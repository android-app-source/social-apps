.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:LX/IFX;

.field public final B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6ax;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lcom/facebook/maps/UrlImageMarkerController;

.field public b:LX/IJY;

.field public c:LX/6aE;

.field public d:Lcom/facebook/content/SecureContextHelper;

.field public e:Ljava/util/concurrent/ScheduledExecutorService;

.field public f:LX/0kL;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/IJZ;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/IID;

.field public i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

.field private j:LX/6aO;

.field public k:Landroid/view/View;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:Lcom/facebook/maps/FbMapFragmentDelegate;

.field public o:Landroid/view/View;

.field public p:Landroid/widget/TextView;

.field private q:Z

.field public r:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

.field public s:LX/IJ4;

.field public t:I

.field public u:LX/IJV;

.field public v:Landroid/animation/ObjectAnimator;

.field public w:Landroid/animation/ObjectAnimator;

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/692;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2563783
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2563784
    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563785
    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    .line 2563786
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    .line 2563787
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    .line 2563788
    return-void
.end method

.method private static a(LX/IFd;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IFd;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2563789
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 2563790
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2563791
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2563792
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;I)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2563793
    if-nez p1, :cond_0

    .line 2563794
    const/4 v0, 0x0

    .line 2563795
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6aM;JLjava/lang/Runnable;)V
    .locals 4
    .param p2    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563796
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v0

    .line 2563797
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-nez v1, :cond_1

    .line 2563798
    invoke-virtual {v0, p1}, LX/6al;->a(LX/6aM;)V

    .line 2563799
    if-eqz p4, :cond_0

    .line 2563800
    invoke-interface {p4}, Ljava/lang/Runnable;->run()V

    .line 2563801
    :cond_0
    :goto_0
    return-void

    .line 2563802
    :cond_1
    long-to-int v1, p2

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/6al;->a(LX/6aM;ILX/6aj;)V

    .line 2563803
    if-eqz p4, :cond_0

    .line 2563804
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->e:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p4, p2, p3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Ljava/util/Collection;Lcom/facebook/location/ImmutableLocation;)V
    .locals 12
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/6ax;",
            ">;",
            "Lcom/facebook/location/ImmutableLocation;",
            ")V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/high16 v8, 0x41600000    # 14.0f

    .line 2563805
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v1

    .line 2563806
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v2

    .line 2563807
    if-eqz p2, :cond_0

    .line 2563808
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2563809
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 2563810
    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    goto :goto_0

    .line 2563811
    :cond_1
    if-nez p2, :cond_2

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2563812
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v0, v10, v11, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2563813
    :cond_2
    invoke-virtual {v2}, LX/696;->a()LX/697;

    move-result-object v0

    .line 2563814
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42000000    # 32.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-static {v0, v2}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6al;->a(LX/6aM;)V

    .line 2563815
    invoke-virtual {v1}, LX/6al;->b()LX/6ay;

    move-result-object v2

    invoke-virtual {v2}, LX/6ay;->a()LX/69F;

    move-result-object v2

    iget-object v2, v2, LX/69F;->e:LX/697;

    .line 2563816
    iget-object v3, v0, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v2, v3}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v2, v3}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2563817
    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 2563818
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 2563819
    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0, v8}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6al;->a(LX/6aM;)V

    .line 2563820
    :cond_4
    :goto_1
    invoke-virtual {v1}, LX/6al;->e()LX/692;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->y:LX/692;

    .line 2563821
    return-void

    .line 2563822
    :cond_5
    if-eqz p2, :cond_6

    .line 2563823
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;)LX/6aM;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6al;->a(LX/6aM;)V

    goto :goto_1

    .line 2563824
    :cond_6
    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;)LX/6aM;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6al;->a(LX/6aM;)V

    goto :goto_1

    .line 2563825
    :cond_7
    invoke-virtual {v1}, LX/6al;->e()LX/692;

    move-result-object v2

    iget v2, v2, LX/692;->b:F

    cmpl-float v2, v2, v8

    if-lez v2, :cond_4

    .line 2563826
    invoke-virtual {v0}, LX/697;->b()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0, v8}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6al;->a(LX/6aM;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    new-instance v4, Lcom/facebook/maps/UrlImageMarkerController;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v2

    check-cast v2, LX/1HI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v4, v2, v3}, Lcom/facebook/maps/UrlImageMarkerController;-><init>(LX/1HI;Ljava/util/concurrent/ExecutorService;)V

    move-object v2, v4

    check-cast v2, Lcom/facebook/maps/UrlImageMarkerController;

    new-instance v5, LX/IJY;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v4

    check-cast v4, LX/1FZ;

    invoke-direct {v5, v3, v4}, LX/IJY;-><init>(Landroid/content/res/Resources;LX/1FZ;)V

    move-object v3, v5

    check-cast v3, LX/IJY;

    invoke-static {p0}, LX/6aE;->b(LX/0QB;)LX/6aE;

    move-result-object v4

    check-cast v4, LX/6aE;

    invoke-static {p0}, LX/IID;->a(LX/0QB;)LX/IID;

    move-result-object v5

    check-cast v5, LX/IID;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    new-instance v0, LX/IJb;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-direct {v0, v9, p1}, LX/IJb;-><init>(Landroid/content/Context;LX/0Uh;)V

    move-object v9, v0

    check-cast v9, LX/IJb;

    new-instance v0, LX/IJa;

    const-class p1, Landroid/content/Context;

    invoke-interface {p0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-direct {v0, p1}, LX/IJa;-><init>(Landroid/content/Context;)V

    move-object p0, v0

    check-cast p0, LX/IJa;

    iput-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    iput-object v3, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->b:LX/IJY;

    iput-object v4, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->c:LX/6aE;

    iput-object v5, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->h:LX/IID;

    iput-object v6, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->e:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v8, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->f:LX/0kL;

    invoke-static {v9, p0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    iput-object p1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->g:LX/0Px;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;J)V
    .locals 3

    .prologue
    .line 2563827
    invoke-direct {p0, p1, p2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->d(J)V

    .line 2563828
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2563829
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 2563830
    invoke-virtual {v0}, LX/6ax;->e()V

    .line 2563831
    :cond_0
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563832
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    .line 2563833
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/IJV;Ljava/lang/String;)V
    .locals 10
    .param p1    # LX/IJV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2563834
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v0

    .line 2563835
    if-nez v0, :cond_0

    .line 2563836
    :goto_0
    return-void

    .line 2563837
    :cond_0
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    .line 2563838
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->r:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v1, p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(LX/IJV;)LX/IJX;

    move-result-object v1

    .line 2563839
    sget-object v2, LX/IJM;->a:[I

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    invoke-virtual {v3}, LX/IJV;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2563840
    :goto_1
    iget-wide v0, v1, LX/IJX;->e:J

    invoke-static {p0, p2, v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Ljava/lang/String;J)V

    .line 2563841
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-le v0, v8, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    sget-object v1, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v0, v1, :cond_1

    .line 2563842
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2563843
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2563844
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->k:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2563845
    invoke-virtual {v0}, LX/6al;->c()LX/6az;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/6az;->a(Z)V

    .line 2563846
    iget v2, v1, LX/IJX;->a:I

    iget v3, v1, LX/IJX;->b:I

    iget v4, v1, LX/IJX;->c:I

    iget v5, v1, LX/IJX;->d:I

    invoke-virtual {v0, v2, v3, v4, v5}, LX/6al;->a(IIII)V

    goto :goto_1

    .line 2563847
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->k:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2563848
    invoke-virtual {v0}, LX/6al;->c()LX/6az;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/6az;->a(Z)V

    .line 2563849
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getHeight()I

    move-result v2

    .line 2563850
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 2563851
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2563852
    iget v4, v1, LX/IJX;->a:I

    iget v5, v1, LX/IJX;->b:I

    add-int/2addr v2, v5

    iget v5, v1, LX/IJX;->c:I

    iget v6, v1, LX/IJX;->d:I

    add-int/2addr v3, v6

    invoke-virtual {v0, v4, v2, v5, v3}, LX/6al;->a(IIII)V

    goto :goto_1

    .line 2563853
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2563854
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;D)V
    .locals 6

    .prologue
    .line 2563855
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    if-eqz v0, :cond_0

    .line 2563856
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    .line 2563857
    iget-object v1, v0, LX/6aO;->a:LX/693;

    if-eqz v1, :cond_1

    .line 2563858
    iget-object v1, v0, LX/6aO;->a:LX/693;

    .line 2563859
    iput-object p1, v1, LX/693;->o:Lcom/facebook/android/maps/model/LatLng;

    .line 2563860
    invoke-static {v1}, LX/693;->q(LX/693;)V

    .line 2563861
    invoke-virtual {v1}, LX/67m;->f()V

    .line 2563862
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    .line 2563863
    iget-object v4, v0, LX/6aO;->a:LX/693;

    if-eqz v4, :cond_2

    .line 2563864
    iget-object v4, v0, LX/6aO;->a:LX/693;

    .line 2563865
    iput-wide p2, v4, LX/693;->q:D

    .line 2563866
    invoke-static {v4}, LX/693;->q(LX/693;)V

    .line 2563867
    invoke-virtual {v4}, LX/67m;->f()V

    .line 2563868
    :goto_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6aO;->a(Z)V

    .line 2563869
    :goto_2
    return-void

    .line 2563870
    :cond_0
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v0

    .line 2563871
    new-instance v1, LX/694;

    invoke-direct {v1}, LX/694;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0951

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2563872
    iput v2, v1, LX/694;->b:I

    .line 2563873
    move-object v1, v1

    .line 2563874
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0950

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2563875
    iput v2, v1, LX/694;->d:I

    .line 2563876
    move-object v1, v1

    .line 2563877
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0036

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 2563878
    iput v2, v1, LX/694;->e:F

    .line 2563879
    move-object v1, v1

    .line 2563880
    iput-object p1, v1, LX/694;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 2563881
    move-object v1, v1

    .line 2563882
    iput-wide p2, v1, LX/694;->c:D

    .line 2563883
    move-object v1, v1

    .line 2563884
    invoke-virtual {v0, v1}, LX/6al;->a(LX/694;)LX/6aO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    goto :goto_2

    .line 2563885
    :cond_1
    iget-object v1, v0, LX/6aO;->b:LX/7c8;

    invoke-static {p1}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7c8;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0

    .line 2563886
    :cond_2
    iget-object v4, v0, LX/6aO;->b:LX/7c8;

    invoke-virtual {v4, p2, p3}, LX/7c8;->a(D)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;JLjava/lang/Runnable;)V
    .locals 2
    .param p2    # J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v1, 0x41600000    # 14.0f

    .line 2563887
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v0

    .line 2563888
    invoke-virtual {v0}, LX/6al;->e()LX/692;

    move-result-object v0

    iget v0, v0, LX/692;->b:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 2563889
    invoke-static {p1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;)LX/6aM;

    move-result-object v0

    invoke-static {p0, v0, p2, p3, p4}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6aM;JLjava/lang/Runnable;)V

    .line 2563890
    :goto_0
    return-void

    .line 2563891
    :cond_0
    invoke-static {p1, v1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    invoke-static {p0, v0, p2, p3, p4}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6aM;JLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Ljava/lang/String;J)V
    .locals 6
    .param p0    # Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563892
    if-nez p1, :cond_0

    .line 2563893
    invoke-static {p0, p2, p3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;J)V

    .line 2563894
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v0

    .line 2563895
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->y:LX/692;

    if-eqz v1, :cond_3

    .line 2563896
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->y:LX/692;

    invoke-static {v0}, LX/6aN;->a(LX/692;)LX/6aM;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, p3, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6aM;JLjava/lang/Runnable;)V

    .line 2563897
    :goto_0
    return-void

    .line 2563898
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2563899
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 2563900
    invoke-virtual {v0}, LX/6ax;->e()V

    .line 2563901
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    sget-object v1, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v0, v1, :cond_2

    .line 2563902
    iget-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    if-eqz v0, :cond_4

    .line 2563903
    :goto_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v0, p1}, LX/IFX;->e(Ljava/lang/String;)LX/IFb;

    move-result-object v2

    .line 2563904
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 2563905
    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v3

    .line 2563906
    iget-object v1, v2, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    move-object v1, v1

    .line 2563907
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v4, v1

    invoke-static {p0, v3, v4, v5}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;D)V

    .line 2563908
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->s:LX/IJ4;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(LX/IFR;LX/IJ4;)V

    .line 2563909
    if-nez p1, :cond_5

    .line 2563910
    const/4 v1, 0x0

    .line 2563911
    :goto_2
    move v1, v1

    .line 2563912
    iput v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563913
    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    new-instance v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$13;

    invoke-direct {v2, p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$13;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/6ax;)V

    invoke-static {p0, v1, p2, p3, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;JLjava/lang/Runnable;)V

    .line 2563914
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    goto :goto_0

    .line 2563915
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->d(J)V

    goto :goto_1

    .line 2563916
    :cond_3
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, LX/6al;->d()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/location/ImmutableLocation;->b(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Ljava/util/Collection;Lcom/facebook/location/ImmutableLocation;)V

    goto :goto_0

    .line 2563917
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    .line 2563918
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->w:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static c$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x0

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 2563708
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-virtual {v0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a()LX/6al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-virtual {v0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a()LX/6al;

    move-result-object v0

    invoke-virtual {v0}, LX/6al;->b()LX/6ay;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2563709
    :cond_0
    :goto_0
    return-void

    .line 2563710
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->e()LX/IFd;

    move-result-object v1

    .line 2563711
    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(LX/IFd;)LX/0Rf;

    move-result-object v0

    .line 2563712
    const/4 v2, 0x0

    .line 2563713
    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 2563714
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2563715
    invoke-virtual {v0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 2563716
    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6ax;

    .line 2563717
    invoke-virtual {v4}, LX/6ax;->c()V

    .line 2563718
    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2563719
    const/4 v2, 0x1

    :goto_2
    move v4, v2

    .line 2563720
    goto :goto_1

    .line 2563721
    :cond_2
    move v0, v4

    .line 2563722
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2563723
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v4

    .line 2563724
    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2563725
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v6

    .line 2563726
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 2563727
    check-cast v1, LX/IFb;

    .line 2563728
    new-instance v7, Lcom/facebook/android/maps/model/LatLng;

    .line 2563729
    iget-object v8, v1, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    move-object v8, v8

    .line 2563730
    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v8

    .line 2563731
    iget-object v10, v1, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    move-object v1, v10

    .line 2563732
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2563733
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6ax;

    .line 2563734
    if-eqz v1, :cond_3

    .line 2563735
    invoke-virtual {v4}, LX/6al;->b()LX/6ay;

    move-result-object v0

    invoke-virtual {v0}, LX/6ay;->a()LX/69F;

    move-result-object v0

    iget-object v0, v0, LX/69F;->e:LX/697;

    invoke-virtual {v1}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v0

    .line 2563736
    invoke-virtual {v4}, LX/6al;->b()LX/6ay;

    move-result-object v8

    invoke-virtual {v8}, LX/6ay;->a()LX/69F;

    move-result-object v8

    iget-object v8, v8, LX/69F;->e:LX/697;

    invoke-virtual {v8, v7}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v8

    .line 2563737
    invoke-virtual {v1, v7}, LX/6ax;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2563738
    iget-object v7, v1, LX/6ax;->a:LX/698;

    if-eqz v7, :cond_c

    .line 2563739
    iget-object v7, v1, LX/6ax;->a:LX/698;

    .line 2563740
    iput-object v6, v7, LX/698;->D:Ljava/lang/String;

    .line 2563741
    invoke-static {v7}, LX/698;->t(LX/698;)V

    .line 2563742
    :goto_4
    if-eq v8, v0, :cond_a

    move v0, v3

    :goto_5
    move v2, v0

    .line 2563743
    goto :goto_3

    .line 2563744
    :cond_3
    new-instance v1, LX/699;

    invoke-direct {v1}, LX/699;-><init>()V

    .line 2563745
    iput-object v7, v1, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2563746
    move-object v1, v1

    .line 2563747
    iput-object v6, v1, LX/699;->h:Ljava/lang/String;

    .line 2563748
    move-object v1, v1

    .line 2563749
    const v8, 0x7f020b95

    invoke-static {v8}, LX/690;->a(I)LX/68w;

    move-result-object v8

    .line 2563750
    iput-object v8, v1, LX/699;->c:LX/68w;

    .line 2563751
    move-object v1, v1

    .line 2563752
    invoke-virtual {v1, v12, v12}, LX/699;->a(FF)LX/699;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/6al;->a(LX/699;)LX/6ax;

    move-result-object v1

    .line 2563753
    iget-object v8, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    invoke-interface {v0}, LX/IFR;->b()Landroid/net/Uri;

    move-result-object v0

    iget-object v9, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->b:LX/IJY;

    invoke-virtual {v8, v1, v0, v9}, Lcom/facebook/maps/UrlImageMarkerController;->a(LX/6ax;Landroid/net/Uri;LX/IJY;)V

    .line 2563754
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v0, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2563755
    invoke-virtual {v4}, LX/6al;->b()LX/6ay;

    move-result-object v0

    invoke-virtual {v0}, LX/6ay;->a()LX/69F;

    move-result-object v0

    iget-object v0, v0, LX/69F;->e:LX/697;

    invoke-virtual {v0, v7}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v3

    :goto_6
    move v2, v0

    .line 2563756
    goto/16 :goto_3

    .line 2563757
    :cond_4
    if-eqz v2, :cond_5

    .line 2563758
    iput-object v14, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->y:LX/692;

    .line 2563759
    :cond_5
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    .line 2563760
    iget-object v1, v0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2563761
    iget-object v1, v0, LX/IFX;->o:LX/0Px;

    move-object v0, v1

    .line 2563762
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 2563763
    invoke-virtual {v0, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2563764
    if-eqz v0, :cond_6

    .line 2563765
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    .line 2563766
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    .line 2563767
    iget-object v1, v0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2563768
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2563769
    iput-object v1, v0, LX/IFX;->o:LX/0Px;

    .line 2563770
    sget-object v0, LX/IJV;->EXPANDED:LX/IJV;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    .line 2563771
    :cond_6
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2563772
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2563773
    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    .line 2563774
    iput v13, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563775
    iput-object v14, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    .line 2563776
    :cond_7
    :goto_7
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(LX/IJV;)V

    goto/16 :goto_0

    .line 2563777
    :cond_8
    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    goto :goto_7

    :cond_9
    move v0, v2

    goto :goto_6

    :cond_a
    move v0, v2

    goto/16 :goto_5

    :cond_b
    move v2, v4

    goto/16 :goto_2

    .line 2563778
    :cond_c
    iget-object v7, v1, LX/6ax;->b:LX/7cA;

    invoke-virtual {v7, v6}, LX/7cA;->a(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method private d(J)V
    .locals 1

    .prologue
    .line 2563779
    iget-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    if-nez v0, :cond_0

    .line 2563780
    :goto_0
    return-void

    .line 2563781
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    .line 2563782
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->v:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 2

    .prologue
    .line 2563635
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    if-eqz v0, :cond_0

    .line 2563636
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->j:LX/6aO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6aO;->a(Z)V

    .line 2563637
    :cond_0
    return-void
.end method

.method public static n(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 2

    .prologue
    .line 2563638
    iget v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563639
    iget v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2563640
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563641
    :cond_0
    return-void
.end method

.method public static o(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 2

    .prologue
    .line 2563642
    iget v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563643
    iget v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2563644
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->t:I

    .line 2563645
    :cond_0
    return-void
.end method

.method public static p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;
    .locals 1

    .prologue
    .line 2563646
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    invoke-virtual {v0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a()LX/6al;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/IJV;)V
    .locals 1

    .prologue
    .line 2563647
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->x:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/IJV;Ljava/lang/String;)V

    .line 2563648
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2563649
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2563650
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2563651
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/68I;->a(Landroid/content/Context;)I

    .line 2563652
    sget-object v0, LX/IJV;->HEADER:LX/IJV;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    .line 2563653
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    if-eqz v0, :cond_0

    .line 2563654
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->h:LX/IID;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v1}, LX/IFX;->e()LX/IFd;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(LX/IFd;)LX/0Rf;

    move-result-object v1

    .line 2563655
    const-string p0, "friends_nearby_dashboard_map_impression"

    invoke-static {v0, p0}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2563656
    const-string p1, "sender_ids"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2563657
    iget-object p1, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563658
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x254f2af

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2563659
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2563660
    const v0, 0x7f0d1311

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    .line 2563661
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    new-instance v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$3;

    invoke-direct {v2, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$3;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->post(Ljava/lang/Runnable;)Z

    .line 2563662
    const v0, 0x7f0d1312

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->k:Landroid/view/View;

    .line 2563663
    const v0, 0x7f0d1313

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->l:Landroid/view/View;

    .line 2563664
    const v0, 0x7f0d1314

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m:Landroid/view/View;

    .line 2563665
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030721

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->o:Landroid/view/View;

    .line 2563666
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030723

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p:Landroid/widget/TextView;

    .line 2563667
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->l:Landroid/view/View;

    new-instance v2, LX/IJR;

    invoke-direct {v2, p0}, LX/IJR;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563668
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->m:Landroid/view/View;

    new-instance v2, LX/IJS;

    invoke-direct {v2, p0}, LX/IJS;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563669
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->k:Landroid/view/View;

    new-instance v2, LX/IJT;

    invoke-direct {v2, p0}, LX/IJT;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563670
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "map_fragment"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2563671
    if-nez v0, :cond_0

    .line 2563672
    new-instance v0, LX/681;

    invoke-direct {v0}, LX/681;-><init>()V

    const-string v2, "friends_nearby"

    .line 2563673
    iput-object v2, v0, LX/681;->m:Ljava/lang/String;

    .line 2563674
    move-object v0, v0

    .line 2563675
    invoke-static {v0}, Lcom/facebook/maps/FbMapFragmentDelegate;->a(LX/681;)Lcom/facebook/maps/FbMapFragmentDelegate;

    move-result-object v0

    .line 2563676
    :goto_0
    move-object v0, v0

    .line 2563677
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    .line 2563678
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d043e

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->n:Lcom/facebook/maps/FbMapFragmentDelegate;

    const-string v4, "map_fragment"

    invoke-virtual {v0, v2, v3, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2563679
    const/16 v0, 0x2b

    const v2, -0x47b77849

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    check-cast v0, Lcom/facebook/maps/FbMapFragmentDelegate;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d093943

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563680
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2563681
    const v1, 0x7f030722

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1077f64f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3966f8d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563682
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2563683
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->C:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2563684
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    .line 2563685
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ca;

    .line 2563686
    invoke-interface {v2}, LX/1ca;->g()Z

    goto :goto_0

    .line 2563687
    :cond_0
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2563688
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2563689
    const/16 v1, 0x2b

    const v2, 0x6bfe8988

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x70b22aff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563598
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2563599
    iget-boolean v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->q:Z

    if-nez v1, :cond_1

    .line 2563600
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2563601
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v1

    .line 2563602
    if-nez v1, :cond_2

    .line 2563603
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->r:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    if-eqz v1, :cond_0

    .line 2563604
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->r:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2563605
    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2563606
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2563607
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ab:Z

    .line 2563608
    iget-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    const/4 v4, 0x1

    .line 2563609
    iput-boolean v4, v2, LX/IFX;->z:Z

    .line 2563610
    invoke-static {v2}, LX/IFX;->o(LX/IFX;)V

    .line 2563611
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->q:Z

    .line 2563612
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x1d3c6a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2563613
    :cond_2
    invoke-virtual {v1, v5}, LX/6al;->a(Z)V

    .line 2563614
    invoke-virtual {v1}, LX/6al;->c()LX/6az;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/6az;->c(Z)V

    .line 2563615
    invoke-virtual {v1}, LX/6al;->c()LX/6az;

    move-result-object v2

    .line 2563616
    iget-object v6, v2, LX/6az;->a:LX/68Q;

    if-eqz v6, :cond_5

    .line 2563617
    iget-object v6, v2, LX/6az;->a:LX/68Q;

    invoke-virtual {v6, v4}, LX/68Q;->f(Z)V

    .line 2563618
    :goto_1
    invoke-virtual {v1}, LX/6al;->c()LX/6az;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/6az;->b(Z)V

    .line 2563619
    new-instance v2, LX/IJU;

    invoke-direct {v2, p0}, LX/IJU;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v1, v2}, LX/6al;->a(LX/6Zu;)V

    .line 2563620
    new-instance v2, LX/IJJ;

    invoke-direct {v2, p0}, LX/IJJ;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v1, v2}, LX/6al;->a(LX/6ak;)V

    .line 2563621
    new-instance v2, LX/IJK;

    invoke-direct {v2, p0}, LX/IJK;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563622
    iget-object v4, v1, LX/6al;->a:LX/680;

    if-eqz v4, :cond_6

    .line 2563623
    iget-object v4, v1, LX/6al;->a:LX/680;

    new-instance v5, LX/6aR;

    invoke-direct {v5, v1, v2}, LX/6aR;-><init>(LX/6al;LX/IJK;)V

    .line 2563624
    iput-object v5, v4, LX/680;->o:LX/6aR;

    .line 2563625
    :cond_3
    :goto_2
    new-instance v2, LX/IJL;

    invoke-direct {v2, p0}, LX/IJL;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563626
    iget-object v4, v1, LX/6al;->a:LX/680;

    if-eqz v4, :cond_7

    .line 2563627
    iget-object v4, v1, LX/6al;->a:LX/680;

    new-instance v5, LX/6aU;

    invoke-direct {v5, v1, v2}, LX/6aU;-><init>(LX/6al;LX/IJL;)V

    .line 2563628
    iput-object v5, v4, LX/680;->N:LX/6aU;

    .line 2563629
    :cond_4
    :goto_3
    goto :goto_0

    .line 2563630
    :cond_5
    iget-object v6, v2, LX/6az;->b:LX/7ax;

    invoke-virtual {v6, v4}, LX/7ax;->a(Z)V

    goto :goto_1

    .line 2563631
    :cond_6
    iget-object v4, v1, LX/6al;->b:LX/7an;

    if-eqz v4, :cond_3

    .line 2563632
    iget-object v4, v1, LX/6al;->b:LX/7an;

    new-instance v5, LX/6aT;

    invoke-direct {v5, v1, v2}, LX/6aT;-><init>(LX/6al;LX/IJK;)V

    invoke-virtual {v4, v5}, LX/7an;->a(LX/6aS;)V

    goto :goto_2

    .line 2563633
    :cond_7
    iget-object v4, v1, LX/6al;->b:LX/7an;

    if-eqz v4, :cond_4

    .line 2563634
    iget-object v4, v1, LX/6al;->b:LX/7an;

    new-instance v5, LX/6aW;

    invoke-direct {v5, v1, v2}, LX/6aW;-><init>(LX/6al;LX/IJL;)V

    invoke-virtual {v4, v5}, LX/7an;->a(LX/6aV;)V

    goto :goto_3
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x70aa4b91

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563690
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2563691
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    .line 2563692
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->f:Z

    .line 2563693
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->g:Z

    .line 2563694
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    .line 2563695
    iget-boolean v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->g:Z

    move v1, v2

    .line 2563696
    if-eqz v1, :cond_0

    .line 2563697
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->c$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    .line 2563698
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3d217bf0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6037b562

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563699
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2563700
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a:Lcom/facebook/maps/UrlImageMarkerController;

    .line 2563701
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->f:Z

    .line 2563702
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1FJ;

    .line 2563703
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 2563704
    :cond_0
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2563705
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->g:Z

    .line 2563706
    :cond_1
    iget-object v2, v1, Lcom/facebook/maps/UrlImageMarkerController;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2563707
    const/16 v1, 0x2b

    const v2, -0x5e4c2f22

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
