.class public final Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 0

    .prologue
    .line 2563499
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$3;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2563500
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$3;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    const/4 p0, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2563501
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getHeight()I

    move-result v1

    .line 2563502
    iput-boolean v6, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->z:Z

    .line 2563503
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->setVisibility(I)V

    .line 2563504
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    const-string v3, "translationY"

    new-array v4, p0, [F

    neg-int v5, v1

    int-to-float v5, v5

    aput v5, v4, v6

    aput v7, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->w:Landroid/animation/ObjectAnimator;

    .line 2563505
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->w:Landroid/animation/ObjectAnimator;

    new-instance v3, LX/IJP;

    invoke-direct {v3, v0}, LX/IJP;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2563506
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->i:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    const-string v3, "translationY"

    new-array v4, p0, [F

    aput v7, v4, v6

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->v:Landroid/animation/ObjectAnimator;

    .line 2563507
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->v:Landroid/animation/ObjectAnimator;

    new-instance v2, LX/IJQ;

    invoke-direct {v2, v0}, LX/IJQ;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2563508
    return-void
.end method
