.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/GVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/Button;

.field private c:Lcom/facebook/fbui/facepile/FacepileView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2563431
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2563432
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a()V

    .line 2563433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563428
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2563429
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a()V

    .line 2563430
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563425
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2563426
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a()V

    .line 2563427
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2563414
    const v0, 0x7f030720

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2563415
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2563416
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->setOrientation(I)V

    .line 2563417
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2563418
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->setPadding(IIII)V

    .line 2563419
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0114

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2563420
    const v0, 0x7f0d130d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->b:Landroid/widget/Button;

    .line 2563421
    const v0, 0x7f0d130e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2563422
    const v0, 0x7f0d130f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->d:Landroid/widget/TextView;

    .line 2563423
    const v0, 0x7f0d1310

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->e:Landroid/widget/TextView;

    .line 2563424
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    invoke-static {v0}, LX/GVF;->a(LX/0QB;)LX/GVF;

    move-result-object v0

    check-cast v0, LX/GVF;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a:LX/GVF;

    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2563406
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 2563407
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2563408
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2563409
    :goto_0
    return-void

    .line 2563410
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2563411
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2563412
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a:LX/GVF;

    invoke-virtual {v1, p1, p2}, LX/GVF;->a(ILX/0Px;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2563413
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-static {v0, p2}, LX/GVB;->a(Lcom/facebook/fbui/facepile/FacepileView;LX/0Px;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563400
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->e:Landroid/widget/TextView;

    if-nez p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2563401
    if-eqz p1, :cond_0

    .line 2563402
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2563403
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563404
    :cond_0
    return-void

    .line 2563405
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2563397
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2563398
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->b:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2563399
    return-void
.end method
