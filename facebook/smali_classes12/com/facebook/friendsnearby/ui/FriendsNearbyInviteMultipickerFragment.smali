.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static u:Ljava/lang/String;

.field public static final v:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/IJ6;

.field public B:LX/0tX;

.field public C:Ljava/util/concurrent/ExecutorService;

.field public D:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/1Ck;

.field public x:LX/0kL;

.field public y:LX/0aG;

.field public z:LX/IJ7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2563223
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->v:Ljava/lang/Class;

    .line 2563224
    const-string v0, "all_friends_suggestion_section"

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2563258
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 2563259
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2563260
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->D:LX/0Rf;

    .line 2563261
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2563262
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->E:LX/0Rf;

    .line 2563263
    return-void
.end method

.method public static J(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V
    .locals 8

    .prologue
    .line 2563244
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2563245
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;-><init>(LX/0Px;)V

    const/4 v3, 0x1

    .line 2563246
    const v1, 0x7f080024

    const/4 v2, 0x0

    invoke-static {v1, v3, v2, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    .line 2563247
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "friends_nearby_invite_send"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2563248
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2563249
    const-string v3, "friendsNearbyInviteParams"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2563250
    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->w:LX/1Ck;

    sget-object v4, LX/IJC;->SEND_INVITE:LX/IJC;

    iget-object v5, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->y:LX/0aG;

    const-string v6, "send_invite"

    const v7, -0x575fbf3b

    invoke-static {v5, v6, v2, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    new-instance v5, LX/IJ9;

    invoke-direct {v5, p0, v1}, LX/IJ9;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;Landroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2563251
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->z:LX/IJ7;

    .line 2563252
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "friends_nearby_invite_send"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "background_location"

    .line 2563253
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2563254
    move-object v1, v1

    .line 2563255
    iget-object v2, v0, LX/IJ7;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563256
    :goto_0
    return-void

    .line 2563257
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->x:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08386c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;LX/0Rf;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2563225
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v1, LX/3Oq;->FRIENDS:LX/0Px;

    .line 2563226
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 2563227
    move-object v0, v0

    .line 2563228
    sget-object v1, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 2563229
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 2563230
    move-object v0, v0

    .line 2563231
    const/4 v1, 0x1

    .line 2563232
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 2563233
    move-object v0, v0

    .line 2563234
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->e:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 2563235
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2563236
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2563237
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2563238
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2563239
    invoke-virtual {p1, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2563240
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2563241
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    .line 2563242
    :cond_1
    :try_start_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2563243
    invoke-interface {v1}, LX/3On;->close()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2563222
    const-string v0, "friends_nearby_invite"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2563219
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2563220
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    new-instance v6, LX/IJ6;

    invoke-direct {v6}, LX/IJ6;-><init>()V

    move-object v6, v6

    move-object v6, v6

    check-cast v6, LX/IJ6;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/IJ7;->a(LX/0QB;)LX/IJ7;

    move-result-object v0

    check-cast v0, LX/IJ7;

    iput-object v3, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->w:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->x:LX/0kL;

    iput-object v5, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->y:LX/0aG;

    iput-object v6, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->A:LX/IJ6;

    iput-object v7, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->B:LX/0tX;

    iput-object p1, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->C:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->z:LX/IJ7;

    .line 2563221
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2563264
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->D:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2563216
    sget-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2563217
    const v0, 0x7f08386d

    .line 2563218
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final c()LX/8RE;
    .locals 1

    .prologue
    .line 2563215
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->A:LX/IJ6;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2563214
    const/4 v0, 0x1

    return v0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x428b03ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563195
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onResume()V

    .line 2563196
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2563197
    if-nez v1, :cond_0

    .line 2563198
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x5ebc5e3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2563199
    :cond_0
    const v2, 0x7f08386b

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2563200
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v4, 0x7f083869

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2563201
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2563202
    move-object v2, v2

    .line 2563203
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2563204
    new-instance v2, LX/IJ8;

    invoke-direct {v2, p0}, LX/IJ8;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method

.method public final p()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2563208
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2563209
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c:LX/0TD;

    new-instance v2, LX/IJB;

    invoke-direct {v2, p0}, LX/IJB;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2563210
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->B:LX/0tX;

    .line 2563211
    new-instance v3, LX/IGo;

    invoke-direct {v3}, LX/IGo;-><init>()V

    move-object v3, v3

    .line 2563212
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v2, v2

    .line 2563213
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v2, v3, v0

    invoke-static {v3}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/IJA;

    invoke-direct {v1, p0}, LX/IJA;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->C:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2563207
    sget-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->u:Ljava/lang/String;

    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final y()V
    .locals 0

    .prologue
    .line 2563205
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->J(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V

    .line 2563206
    return-void
.end method
