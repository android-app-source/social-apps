.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2561646
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2561647
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a()V

    .line 2561648
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2561643
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2561644
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a()V

    .line 2561645
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2561649
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2561650
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a()V

    .line 2561651
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2561633
    const v0, 0x7f03071a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2561634
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setOrientation(I)V

    .line 2561635
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setGravity(I)V

    .line 2561636
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2561637
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setPadding(IIII)V

    .line 2561638
    const v0, 0x7f0a0115

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setBackgroundResource(I)V

    .line 2561639
    const v0, 0x7f0d1301

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a:Landroid/widget/TextView;

    .line 2561640
    const v0, 0x7f0d1302

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->b:Landroid/widget/TextView;

    .line 2561641
    const v0, 0x7f0d1303

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->c:Landroid/widget/Button;

    .line 2561642
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2561630
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 2561631
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2561632
    return-void
.end method

.method public setSubtitle(I)V
    .locals 1

    .prologue
    .line 2561628
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2561629
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2561626
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2561627
    return-void
.end method
