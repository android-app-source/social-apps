.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/IJ4;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Lcom/facebook/location/FbLocationOperationParams;

.field public static final d:Lcom/facebook/location/FbLocationOperationParams;

.field public static final e:[Ljava/lang/String;


# instance fields
.field public A:LX/0wM;

.field public B:LX/IIE;

.field public C:LX/121;

.field public D:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/IIB;

.field private F:LX/0hy;

.field private G:LX/0Uh;

.field public H:LX/0Uo;

.field public I:LX/GUm;

.field public final J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IFU;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

.field public L:Landroid/widget/EditText;

.field public M:Landroid/view/View;

.field public N:Landroid/view/View;

.field public O:Lcom/facebook/widget/text/BetterButton;

.field public P:LX/62k;

.field public Q:Lcom/facebook/widget/listview/SplitHideableListView;

.field public R:Landroid/view/ViewGroup;

.field public S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

.field public T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

.field public U:Landroid/widget/TextView;

.field private V:Landroid/view/View;

.field private W:Landroid/widget/TextView;

.field public X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

.field private Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

.field public Z:LX/IFX;

.field public final aA:LX/IIm;

.field public final aB:LX/IIm;

.field public final aC:LX/IIm;

.field public final aD:LX/IIm;

.field public final aE:LX/IIm;

.field public final aF:LX/IIm;

.field public final aG:LX/IIm;

.field public final aH:LX/IIm;

.field public final aI:LX/IIm;

.field public aJ:LX/IJV;

.field public aK:LX/IIm;

.field private final aL:LX/2hK;

.field public final aM:LX/IIN;

.field public aa:LX/IJH;

.field public ab:Z

.field public ac:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field public ad:J

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:I

.field public ai:Z

.field public aj:Landroid/animation/ObjectAnimator;

.field public ak:Landroid/animation/ObjectAnimator;

.field private al:Landroid/animation/ObjectAnimator;

.field private am:Landroid/animation/ObjectAnimator;

.field public an:Ljava/lang/String;

.field public ao:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

.field private ap:LX/0Xl;

.field private aq:LX/IIx;

.field private ar:LX/0Yb;

.field private as:LX/0i4;

.field public at:LX/0i5;

.field public au:LX/6Zb;

.field private av:LX/6Ze;

.field public final aw:LX/IIm;

.field public final ax:LX/IIm;

.field public final ay:LX/IIm;

.field public final az:LX/IIm;

.field public f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IFy;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0y3;

.field public h:LX/0aG;

.field public i:LX/0kL;

.field public j:LX/03V;

.field public k:Lcom/facebook/content/SecureContextHelper;

.field public l:LX/IG1;

.field public m:LX/IFO;

.field public n:LX/IID;

.field public o:LX/0SG;

.field public p:LX/IG7;

.field private q:Landroid/view/inputmethod/InputMethodManager;

.field public r:LX/IFJ;

.field public s:LX/IFl;

.field public t:LX/IFV;

.field public u:LX/IFv;

.field private v:LX/0ad;

.field private w:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field private x:LX/2do;

.field public y:LX/2l5;

.field public z:LX/0aU;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2562856
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a:Ljava/lang/Class;

    .line 2562857
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const-string v1, "nearby_friends"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2562858
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    .line 2562859
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2562860
    move-object v0, v0

    .line 2562861
    const/high16 v1, 0x44960000    # 1200.0f

    .line 2562862
    iput v1, v0, LX/1S7;->c:F

    .line 2562863
    move-object v0, v0

    .line 2562864
    const-wide/16 v2, 0x3e8

    .line 2562865
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2562866
    move-object v0, v0

    .line 2562867
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->c:Lcom/facebook/location/FbLocationOperationParams;

    .line 2562868
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x1d4c0

    .line 2562869
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2562870
    move-object v0, v0

    .line 2562871
    const/high16 v1, 0x42c80000    # 100.0f

    .line 2562872
    iput v1, v0, LX/1S7;->c:F

    .line 2562873
    move-object v0, v0

    .line 2562874
    const-wide/16 v2, 0x5dc

    .line 2562875
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2562876
    move-object v0, v0

    .line 2562877
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->d:Lcom/facebook/location/FbLocationOperationParams;

    .line 2562878
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2562879
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2562880
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    .line 2562881
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ab:Z

    .line 2562882
    new-instance v0, LX/IIt;

    invoke-direct {v0, p0}, LX/IIt;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aw:LX/IIm;

    .line 2562883
    new-instance v0, LX/IIu;

    invoke-direct {v0, p0}, LX/IIu;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ax:LX/IIm;

    .line 2562884
    new-instance v0, LX/IIw;

    invoke-direct {v0, p0}, LX/IIw;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ay:LX/IIm;

    .line 2562885
    new-instance v0, LX/IIs;

    invoke-direct {v0, p0}, LX/IIs;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->az:LX/IIm;

    .line 2562886
    new-instance v0, LX/IIp;

    invoke-direct {v0, p0}, LX/IIp;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aA:LX/IIm;

    .line 2562887
    new-instance v0, LX/IIq;

    invoke-direct {v0, p0}, LX/IIq;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aB:LX/IIm;

    .line 2562888
    new-instance v0, LX/IIr;

    invoke-direct {v0, p0}, LX/IIr;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aC:LX/IIm;

    .line 2562889
    new-instance v0, LX/IIz;

    invoke-direct {v0, p0}, LX/IIz;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aD:LX/IIm;

    .line 2562890
    new-instance v0, LX/IJ0;

    invoke-direct {v0, p0}, LX/IJ0;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aE:LX/IIm;

    .line 2562891
    new-instance v0, LX/IIo;

    invoke-direct {v0, p0}, LX/IIo;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aF:LX/IIm;

    .line 2562892
    new-instance v0, LX/IJ3;

    invoke-direct {v0, p0}, LX/IJ3;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aG:LX/IIm;

    .line 2562893
    new-instance v0, LX/IJ2;

    invoke-direct {v0, p0}, LX/IJ2;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aH:LX/IIm;

    .line 2562894
    new-instance v0, LX/IJ1;

    invoke-direct {v0, p0}, LX/IJ1;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aI:LX/IIm;

    .line 2562895
    sget-object v0, LX/IJV;->HEADER:LX/IJV;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    .line 2562896
    new-instance v0, LX/IIO;

    invoke-direct {v0, p0}, LX/IIO;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aL:LX/2hK;

    .line 2562897
    new-instance v0, LX/IIN;

    invoke-direct {v0, p0}, LX/IIN;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aM:LX/IIN;

    .line 2562898
    return-void
.end method

.method public static B$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2562899
    iput-boolean v5, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562900
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2562901
    sget-object v1, LX/0ax;->dZ:Ljava/lang/String;

    .line 2562902
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "-1"

    aput-object v4, v2, v3

    const-string v3, "-1"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2562903
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2562904
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2562905
    return-void
.end method

.method public static D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562906
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O()V

    .line 2562907
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->V:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2562908
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2562909
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/62k;->setHeaderVisibility(I)V

    .line 2562910
    return-void
.end method

.method public static E$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2562911
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2562912
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2562913
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2562914
    :cond_0
    return-void
.end method

.method public static F(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 3

    .prologue
    .line 2562915
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2562916
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2562917
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2562918
    :cond_0
    return-void
.end method

.method public static H(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562919
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->N:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2562920
    return-void
.end method

.method public static I(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562921
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setVisibility(I)V

    .line 2562922
    return-void
.end method

.method public static J(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562840
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->setVisibility(I)V

    .line 2562841
    return-void
.end method

.method public static K(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562923
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setVisibility(I)V

    .line 2562924
    return-void
.end method

.method public static M(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562925
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->setVisibility(I)V

    .line 2562926
    return-void
.end method

.method public static N$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 3

    .prologue
    .line 2562927
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    sget-object v1, LX/IIC;->INVITE:LX/IIC;

    invoke-virtual {v0, v1}, LX/IID;->a(LX/IIC;)V

    .line 2562928
    const v0, 0x7f083860

    const v1, 0x7f083861

    const v2, 0x7f083862

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;III)V

    .line 2562929
    return-void
.end method

.method private O()V
    .locals 5

    .prologue
    .line 2562930
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2562931
    if-nez v0, :cond_1

    .line 2562932
    :cond_0
    :goto_0
    return-void

    .line 2562933
    :cond_1
    const v1, 0x7f08384c

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2562934
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    sget-object v2, LX/IJV;->HEADER:LX/IJV;

    if-ne v1, v2, :cond_2

    .line 2562935
    const v1, 0x7f08383f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2562936
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 2562937
    iput-object v1, v2, LX/108;->j:Ljava/lang/String;

    .line 2562938
    move-object v1, v2

    .line 2562939
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->A:LX/0wM;

    .line 2562940
    const v3, 0x7f0208ac

    move v3, v3

    .line 2562941
    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2562942
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2562943
    move-object v1, v1

    .line 2562944
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2562945
    new-instance v1, LX/IIc;

    invoke-direct {v1, p0}, LX/IIc;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0

    .line 2562946
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    sget-object v2, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v1, v2, :cond_0

    .line 2562947
    const v1, 0x7f083840

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2562948
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    .line 2562949
    iput-object v1, v2, LX/108;->j:Ljava/lang/String;

    .line 2562950
    move-object v1, v2

    .line 2562951
    const v2, 0x7f020b9d

    .line 2562952
    iput v2, v1, LX/108;->i:I

    .line 2562953
    move-object v1, v1

    .line 2562954
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2562955
    new-instance v1, LX/IId;

    invoke-direct {v1, p0}, LX/IId;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method

.method public static T(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562956
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/SplitHideableListView;->g()V

    .line 2562957
    return-void
.end method

.method public static U(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 4

    .prologue
    .line 2562958
    const-string v0, "FriendsNearbyFragment.setupMap"

    const v1, 0x7cee4050

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2562959
    :try_start_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    if-nez v0, :cond_0

    .line 2562960
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "map_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2562961
    if-nez v0, :cond_1

    .line 2562962
    new-instance v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;-><init>()V

    .line 2562963
    :goto_0
    move-object v0, v0

    .line 2562964
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2562965
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2562966
    iput-object p0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->r:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2562967
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2562968
    iput-object p0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->s:LX/IJ4;

    .line 2562969
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d12f2

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    const-string v3, "map_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2562970
    :cond_0
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2562971
    const v0, 0x6d6cff76

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2562972
    return-void

    .line 2562973
    :catchall_0
    move-exception v0

    const v1, 0x577b05a4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    goto :goto_0
.end method

.method private W()Z
    .locals 1

    .prologue
    .line 2562974
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2562975
    if-nez p0, :cond_0

    .line 2562976
    const/4 v0, 0x0

    .line 2562977
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/0SG;LX/1Ck;LX/0y3;LX/0kL;LX/0aG;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/IFO;LX/IJI;LX/IID;LX/IG7;LX/2do;Landroid/view/inputmethod/InputMethodManager;LX/0Xl;LX/IFJ;LX/IFm;LX/IFV;LX/IFw;LX/0ad;LX/0Or;LX/2l5;LX/0aU;LX/0wM;LX/0i4;LX/6Zb;LX/6Ze;LX/IIE;LX/121;LX/0Or;LX/IIB;LX/0hy;LX/0Uh;LX/0Uo;LX/GUm;)V
    .locals 2
    .param p14    # Landroid/view/inputmethod/InputMethodManager;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/1Ck;",
            "LX/0y3;",
            "LX/0kL;",
            "LX/0aG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/IFX;",
            ">;",
            "LX/IFO;",
            "LX/IJI;",
            "LX/IID;",
            "LX/IG7;",
            "LX/2do;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/0Xl;",
            "LX/IFJ;",
            "LX/IFm;",
            "LX/IFV;",
            "LX/IFw;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/2l5;",
            "LX/0aU;",
            "LX/0wM;",
            "LX/0i4;",
            "LX/6Zb;",
            "LX/6Ze;",
            "LX/IIE;",
            "LX/121;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/IIB;",
            "LX/0hy;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Uo;",
            "LX/GUm;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2562978
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->o:LX/0SG;

    .line 2562979
    iput-object p5, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->h:LX/0aG;

    .line 2562980
    iput-object p2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    .line 2562981
    iput-object p3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->g:LX/0y3;

    .line 2562982
    iput-object p4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->i:LX/0kL;

    .line 2562983
    iput-object p6, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->j:LX/03V;

    .line 2562984
    iput-object p7, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    .line 2562985
    iput-object p9, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->m:LX/IFO;

    .line 2562986
    invoke-virtual {p10, p0, p0}, LX/IJI;->a(LX/IJ4;Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)LX/IJH;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aa:LX/IJH;

    .line 2562987
    iput-object p11, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2562988
    iput-object p12, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    .line 2562989
    iput-object p13, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x:LX/2do;

    .line 2562990
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->q:Landroid/view/inputmethod/InputMethodManager;

    .line 2562991
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ap:LX/0Xl;

    .line 2562992
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->r:LX/IFJ;

    .line 2562993
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    move-object/from16 v0, p17

    invoke-virtual {v0, v1}, LX/IFm;->a(LX/1Ck;)LX/IFl;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->s:LX/IFl;

    .line 2562994
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->t:LX/IFV;

    .line 2562995
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    move-object/from16 v0, p19

    invoke-virtual {v0, v1}, LX/IFw;->a(LX/1Ck;)LX/IFv;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->u:LX/IFv;

    .line 2562996
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->v:LX/0ad;

    .line 2562997
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->w:LX/0Or;

    .line 2562998
    invoke-interface {p8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IFX;

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562999
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->y:LX/2l5;

    .line 2563000
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->z:LX/0aU;

    .line 2563001
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->A:LX/0wM;

    .line 2563002
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->as:LX/0i4;

    .line 2563003
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->au:LX/6Zb;

    .line 2563004
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->av:LX/6Ze;

    .line 2563005
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->B:LX/IIE;

    .line 2563006
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->C:LX/121;

    .line 2563007
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D:LX/0Or;

    .line 2563008
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    .line 2563009
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->F:LX/0hy;

    .line 2563010
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->G:LX/0Uh;

    .line 2563011
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->H:LX/0Uo;

    .line 2563012
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I:LX/GUm;

    .line 2563013
    return-void
.end method

.method public static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Z)V
    .locals 5

    .prologue
    .line 2563014
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2563015
    sget-object v2, LX/0ax;->ea:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "dashboard:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2563016
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2563017
    const-string v4, "source"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    if-eqz p1, :cond_0

    const-string v0, "informational"

    :goto_0
    invoke-static {v2, v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2563018
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2563019
    return-void

    .line 2563020
    :cond_0
    const-string v0, "nearby_friends_undecided"

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 38

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v37

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static/range {v37 .. v37}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static/range {v37 .. v37}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static/range {v37 .. v37}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v5

    check-cast v5, LX/0y3;

    invoke-static/range {v37 .. v37}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static/range {v37 .. v37}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static/range {v37 .. v37}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {v37 .. v37}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    const/16 v10, 0x22e2

    move-object/from16 v0, v37

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v37 .. v37}, LX/IFO;->a(LX/0QB;)LX/IFO;

    move-result-object v11

    check-cast v11, LX/IFO;

    const-class v12, LX/IJI;

    move-object/from16 v0, v37

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/IJI;

    invoke-static/range {v37 .. v37}, LX/IID;->a(LX/0QB;)LX/IID;

    move-result-object v13

    check-cast v13, LX/IID;

    invoke-static/range {v37 .. v37}, LX/IG7;->a(LX/0QB;)LX/IG7;

    move-result-object v14

    check-cast v14, LX/IG7;

    invoke-static/range {v37 .. v37}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v15

    check-cast v15, LX/2do;

    invoke-static/range {v37 .. v37}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v16

    check-cast v16, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v37 .. v37}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    invoke-static/range {v37 .. v37}, LX/IFJ;->a(LX/0QB;)LX/IFJ;

    move-result-object v18

    check-cast v18, LX/IFJ;

    const-class v19, LX/IFm;

    move-object/from16 v0, v37

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/IFm;

    const-class v20, LX/IFV;

    move-object/from16 v0, v37

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/IFV;

    const-class v21, LX/IFw;

    move-object/from16 v0, v37

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/IFw;

    invoke-static/range {v37 .. v37}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v22

    check-cast v22, LX/0ad;

    const/16 v23, 0xc81

    move-object/from16 v0, v37

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {v37 .. v37}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v24

    check-cast v24, LX/2l5;

    invoke-static/range {v37 .. v37}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v25

    check-cast v25, LX/0aU;

    invoke-static/range {v37 .. v37}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v26

    check-cast v26, LX/0wM;

    const-class v27, LX/0i4;

    move-object/from16 v0, v37

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/0i4;

    invoke-static/range {v37 .. v37}, LX/6Zb;->a(LX/0QB;)LX/6Zb;

    move-result-object v28

    check-cast v28, LX/6Zb;

    invoke-static/range {v37 .. v37}, LX/6Ze;->a(LX/0QB;)LX/6Ze;

    move-result-object v29

    check-cast v29, LX/6Ze;

    invoke-static/range {v37 .. v37}, LX/IIE;->a(LX/0QB;)LX/IIE;

    move-result-object v30

    check-cast v30, LX/IIE;

    invoke-static/range {v37 .. v37}, LX/128;->a(LX/0QB;)LX/128;

    move-result-object v31

    check-cast v31, LX/121;

    const/16 v32, 0x122d

    move-object/from16 v0, v37

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v32

    invoke-static/range {v37 .. v37}, LX/IIB;->a(LX/0QB;)LX/IIB;

    move-result-object v33

    check-cast v33, LX/IIB;

    invoke-static/range {v37 .. v37}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v34

    check-cast v34, LX/0hy;

    invoke-static/range {v37 .. v37}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v35

    check-cast v35, LX/0Uh;

    invoke-static/range {v37 .. v37}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v36

    check-cast v36, LX/0Uo;

    invoke-static/range {v37 .. v37}, LX/GUm;->a(LX/0QB;)LX/GUm;

    move-result-object v37

    check-cast v37, LX/GUm;

    invoke-static/range {v2 .. v37}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/0SG;LX/1Ck;LX/0y3;LX/0kL;LX/0aG;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/IFO;LX/IJI;LX/IID;LX/IG7;LX/2do;Landroid/view/inputmethod/InputMethodManager;LX/0Xl;LX/IFJ;LX/IFm;LX/IFV;LX/IFw;LX/0ad;LX/0Or;LX/2l5;LX/0aU;LX/0wM;LX/0i4;LX/6Zb;LX/6Ze;LX/IIE;LX/121;LX/0Or;LX/IIB;LX/0hy;LX/0Uh;LX/0Uo;LX/GUm;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;III)V
    .locals 2

    .prologue
    .line 2563021
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setVisibility(I)V

    .line 2563022
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setTitle(I)V

    .line 2563023
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    invoke-virtual {v0, p2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->setSubtitle(I)V

    .line 2563024
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    new-instance v1, LX/IIV;

    invoke-direct {v1, p0}, LX/IIV;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, p3, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;->a(ILandroid/view/View$OnClickListener;)V

    .line 2563025
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V
    .locals 9

    .prologue
    .line 2563026
    invoke-virtual {p1}, LX/IIm;->h()V

    .line 2563027
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_FETCH_DATA1:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->a(LX/IG6;)V

    .line 2563028
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2563029
    const-string v1, "fbid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/47C;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2563030
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->u:LX/IFv;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->g:LX/0y3;

    invoke-virtual {v2}, LX/0y3;->a()LX/0yG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    new-instance v4, LX/IIQ;

    invoke-direct {v4, p0, p1}, LX/IIQ;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2563031
    const-string v5, "no callback"

    invoke-static {v4, v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2563032
    const-string v5, "FriendsNearbySection"

    invoke-static {v5}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v6

    .line 2563033
    iget-object v5, v1, LX/IFv;->d:LX/IFg;

    invoke-virtual {v5, v2, v3}, LX/IFg;->a(LX/0yG;LX/0am;)LX/0zO;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    iget-object v7, v1, LX/IFv;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, v7}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v5

    new-instance v7, LX/IFr;

    invoke-direct {v7, v1}, LX/IFr;-><init>(LX/IFv;)V

    invoke-virtual {v5, v7}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v7

    .line 2563034
    iget-object v5, v1, LX/IFv;->d:LX/IFg;

    .line 2563035
    const/4 v8, 0x1

    const/4 p0, 0x0

    invoke-static {v5, v8, p0, v3}, LX/IFg;->a(LX/IFg;ZLjava/lang/String;LX/0am;)LX/0zO;

    move-result-object v8

    move-object v5, v8

    .line 2563036
    invoke-virtual {v6, v5}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    iget-object v8, v1, LX/IFv;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, v8}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v5

    new-instance v8, LX/IFs;

    invoke-direct {v8, v1}, LX/IFs;-><init>(LX/IFv;)V

    invoke-virtual {v5, v8}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v8

    .line 2563037
    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v5

    .line 2563038
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    .line 2563039
    iget-object v5, v1, LX/IFv;->d:LX/IFg;

    .line 2563040
    new-instance p0, LX/IGn;

    invoke-direct {p0}, LX/IGn;-><init>()V

    move-object p0, p0

    .line 2563041
    const-string p1, "ids"

    invoke-virtual {p0, p1, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object p1

    const-string v2, "pic_size"

    iget-object v3, v5, LX/IFg;->a:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2563042
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    move-object v5, p0

    .line 2563043
    invoke-virtual {v6, v5}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    iget-object p0, v1, LX/IFv;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, p0}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v5

    new-instance p0, LX/IFt;

    invoke-direct {p0, v1}, LX/IFt;-><init>(LX/IFv;)V

    invoke-virtual {v5, p0}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v5

    .line 2563044
    :cond_0
    new-instance p0, LX/0zX;

    iget-object p1, v7, LX/0zX;->a:LX/0v9;

    iget-object v0, v8, LX/0zX;->a:LX/0v9;

    iget-object v2, v5, LX/0zX;->a:LX/0v9;

    .line 2563045
    const/4 v7, 0x3

    new-array v7, v7, [LX/0v9;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    const/4 v8, 0x2

    aput-object v2, v7, v8

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, LX/0v9;->b(Ljava/lang/Iterable;)LX/0v9;

    move-result-object v7

    invoke-static {v7}, LX/0v9;->e(LX/0v9;)LX/0v9;

    move-result-object v7

    move-object p1, v7

    .line 2563046
    invoke-direct {p0, p1}, LX/0zX;-><init>(LX/0v9;)V

    move-object v5, p0

    .line 2563047
    iget-object v7, v1, LX/IFv;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, v7}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v5

    new-instance v7, LX/IFu;

    invoke-direct {v7, v1, v4}, LX/IFu;-><init>(LX/IFv;LX/IIQ;)V

    invoke-virtual {v5, v7}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 2563048
    iget-object v5, v1, LX/IFv;->c:LX/0tX;

    invoke-virtual {v5, v6}, LX/0tX;->a(LX/0v6;)V

    .line 2563049
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;LX/IG6;Lcom/facebook/location/ImmutableLocation;)V
    .locals 1

    .prologue
    .line 2563050
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    invoke-virtual {v0, p2}, LX/IG7;->b(LX/IG6;)V

    .line 2563051
    invoke-static {p3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    .line 2563052
    invoke-virtual {p1}, LX/IIm;->q()V

    .line 2563053
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;LX/IG6;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2563054
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    invoke-virtual {v0, p2}, LX/IG7;->c(LX/IG6;)V

    .line 2563055
    sget-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed to get location"

    invoke-static {v0, v1, p3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2563056
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    .line 2563057
    invoke-virtual {p1}, LX/IIm;->q()V

    .line 2563058
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;Lcom/facebook/location/FbLocationOperationParams;LX/IG6;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2562842
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2562843
    invoke-virtual {p1}, LX/IIm;->p()V

    .line 2562844
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    .line 2562845
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    invoke-virtual {v2, p3}, LX/IG7;->a(LX/IG6;)V

    .line 2562846
    sget-object v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2562847
    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2562848
    :try_start_0
    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 2562849
    :goto_0
    if-nez v1, :cond_0

    .line 2562850
    invoke-static {p0, p1, p3, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;LX/IG6;Ljava/lang/Throwable;)V

    .line 2562851
    :goto_1
    return-void

    .line 2562852
    :catch_0
    move-exception v0

    .line 2562853
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 2562854
    :cond_0
    invoke-static {p0, p1, p3, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;LX/IG6;Lcom/facebook/location/ImmutableLocation;)V

    goto :goto_1

    .line 2562855
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    sget-object v2, LX/IFy;->h:LX/IFy;

    new-instance v3, LX/IIP;

    invoke-direct {v3, p0, p1, p3}, LX/IIP;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;LX/IG6;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    .line 2563059
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2563060
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    sget-object v1, LX/IFd;->c:LX/IFd;

    invoke-virtual {v0, v1}, LX/IFX;->a(LX/IFd;)V

    .line 2563061
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->H(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2563062
    :goto_0
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->s:LX/IFl;

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_1
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v2}, LX/IFX;->d()LX/0Rf;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aM:LX/IIN;

    .line 2563063
    iget-object v4, v1, LX/IFl;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2563064
    :cond_0
    :goto_2
    return-void

    .line 2563065
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->N:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2563066
    goto :goto_0

    .line 2563067
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2563068
    :cond_3
    const/4 v4, 0x0

    iput-object v4, v1, LX/IFl;->i:Ljava/lang/String;

    .line 2563069
    iget-object v4, v1, LX/IFl;->c:LX/1Ck;

    invoke-virtual {v4}, LX/1Ck;->c()V

    .line 2563070
    iput-object v0, v1, LX/IFl;->j:Ljava/lang/String;

    .line 2563071
    sget-object v4, LX/IFk;->INITIAL:LX/IFk;

    iput-object v4, v1, LX/IFl;->h:LX/IFk;

    .line 2563072
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    iput-object v4, v1, LX/IFl;->k:LX/0Pz;

    .line 2563073
    iput-object v2, v1, LX/IFl;->g:LX/0Rf;

    .line 2563074
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2563075
    invoke-static {}, LX/IGs;->d()LX/IGr;

    move-result-object v4

    .line 2563076
    const-string v5, "searchText"

    iget-object p0, v1, LX/IFl;->j:Ljava/lang/String;

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string p0, "pic_size"

    iget-object p1, v1, LX/IFl;->f:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string p0, "order"

    sget-object p1, LX/IFl;->b:Ljava/util/List;

    invoke-virtual {v5, p0, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v5

    const-string p0, "count"

    const-string p1, "20"

    invoke-virtual {v5, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2563077
    iget-object v5, v1, LX/IFl;->d:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2563078
    iget-object v5, v1, LX/IFl;->c:LX/1Ck;

    sget-object p0, LX/IFy;->k:LX/IFy;

    new-instance p1, LX/IFi;

    invoke-direct {p1, v1, v3}, LX/IFi;-><init>(LX/IFl;LX/IIN;)V

    invoke-virtual {v5, p0, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_2
.end method

.method public static a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V
    .locals 1

    .prologue
    .line 2562624
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    .line 2562625
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->f()V

    .line 2562626
    return-void
.end method

.method public static b(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2562607
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    sget-object v2, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v0, v2, :cond_0

    .line 2562608
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2562609
    if-nez v0, :cond_4

    .line 2562610
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->W:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2562611
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aa:LX/IJH;

    invoke-virtual {v0}, LX/IJH;->isEmpty()Z

    move-result v2

    .line 2562612
    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->V:Landroid/view/View;

    if-eqz v2, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2562613
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    :cond_1
    invoke-interface {v0, v1}, LX/62k;->setHeaderVisibility(I)V

    .line 2562614
    if-eqz v2, :cond_3

    .line 2562615
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    invoke-interface {v0}, LX/62k;->d()V

    .line 2562616
    :goto_2
    return-void

    .line 2562617
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    .line 2562618
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    invoke-interface {v0}, LX/62k;->e()V

    goto :goto_2

    .line 2562619
    :cond_4
    const v2, 0x7f08384c

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2562620
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f020b9d

    .line 2562621
    iput v3, v2, LX/108;->i:I

    .line 2562622
    move-object v2, v2

    .line 2562623
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2562592
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562593
    iget v3, v2, LX/IG1;->c:I

    move v2, v3

    .line 2562594
    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562595
    iget-object v4, v3, LX/IG1;->b:LX/0Px;

    move-object v3, v4

    .line 2562596
    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a(ILX/0Px;)V

    .line 2562597
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    if-eqz p1, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setPrivacyPicker(LX/0Px;)V

    .line 2562598
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    if-eqz p1, :cond_1

    :goto_1
    new-instance v2, LX/IIY;

    invoke-direct {v2, p0}, LX/IIY;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 2562599
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    if-eqz p1, :cond_2

    const v0, 0x7f08002c

    :goto_2
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/IIa;

    invoke-direct {v2, p0}, LX/IIa;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->b(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 2562600
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->setVisibility(I)V

    .line 2562601
    return-void

    .line 2562602
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562603
    iget-object v3, v0, LX/IG1;->e:LX/0Px;

    move-object v0, v3

    .line 2562604
    goto :goto_0

    .line 2562605
    :cond_1
    const v1, 0x7f083850

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2562606
    :cond_2
    const v0, 0x7f08002d

    goto :goto_2
.end method

.method public static b$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 2562583
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562584
    iget v2, v1, LX/IG1;->c:I

    move v1, v2

    .line 2562585
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->l:LX/IG1;

    .line 2562586
    iget-object v3, v2, LX/IG1;->b:LX/0Px;

    move-object v2, v3

    .line 2562587
    invoke-virtual {v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a(ILX/0Px;)V

    .line 2562588
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    new-instance v1, LX/IIW;

    invoke-direct {v1, p0}, LX/IIW;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->b(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 2562589
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    const v1, 0x7f083850

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IIX;

    invoke-direct {v2, p0}, LX/IIX;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 2562590
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;->setVisibility(I)V

    .line 2562591
    return-void
.end method

.method private c(LX/IFR;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2562579
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-interface {p1}, LX/IFR;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IFX;->d(Ljava/lang/String;)LX/IFd;

    move-result-object v0

    .line 2562580
    if-nez v0, :cond_0

    .line 2562581
    const-string v0, ""

    .line 2562582
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/622;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2562449
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2562450
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "notif_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2562451
    const-string v3, "friends_nearby_dashboard_open"

    invoke-static {v1, v3}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2562452
    const-string p0, "source"

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2562453
    if-eqz v0, :cond_0

    .line 2562454
    const-string p0, "notif_id"

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2562455
    :cond_0
    iget-object p0, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562456
    return-void
.end method

.method public static q(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562500
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->U:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/SplitHideableListView;->setEmptyView(Landroid/view/View;)V

    .line 2562501
    return-void
.end method

.method public static u(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2562492
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    .line 2562493
    const-string v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->am:Landroid/animation/ObjectAnimator;

    .line 2562494
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->am:Landroid/animation/ObjectAnimator;

    new-instance v2, LX/IIL;

    invoke-direct {v2, p0, v0}, LX/IIL;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2562495
    const-string v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->al:Landroid/animation/ObjectAnimator;

    .line 2562496
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->al:Landroid/animation/ObjectAnimator;

    new-instance v2, LX/IIM;

    invoke-direct {v2, p0, v0}, LX/IIM;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2562497
    return-void

    .line 2562498
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 2562499
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static v$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 3

    .prologue
    .line 2562488
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->q:Landroid/view/inputmethod/InputMethodManager;

    .line 2562489
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2562490
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2562491
    return-void
.end method

.method public static w(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2562486
    iget-boolean v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ab:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2562487
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v1}, LX/IFX;->e()LX/IFd;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v1}, LX/IFX;->e()LX/IFd;

    move-result-object v1

    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v1}, LX/IFX;->e()LX/IFd;

    move-result-object v1

    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 2

    .prologue
    .line 2562476
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->w(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2562477
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562478
    :cond_0
    :goto_0
    return-void

    .line 2562479
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    if-eqz v0, :cond_0

    .line 2562480
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/SplitHideableListView;->h()V

    .line 2562481
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562482
    iget-object p0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    if-ne p0, v1, :cond_2

    .line 2562483
    :goto_1
    goto :goto_0

    .line 2562484
    :cond_2
    iput-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    .line 2562485
    new-instance p0, LX/IJN;

    invoke-direct {p0, v0}, LX/IJN;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V

    invoke-virtual {v1, p0}, LX/IFX;->a(LX/IFW;)V

    goto :goto_1
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2562457
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2562458
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2562459
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearComposingText()V

    .line 2562460
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 2562461
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O:Lcom/facebook/widget/text/BetterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2562462
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562463
    const/4 v0, 0x1

    .line 2562464
    :goto_0
    return v0

    .line 2562465
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    if-eqz v0, :cond_1

    .line 2562466
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    .line 2562467
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    sget-object v2, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v1, v2, :cond_2

    .line 2562468
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->h:LX/IID;

    .line 2562469
    const-string v2, "friends_nearby_dashboard_map_contract"

    invoke-static {v1, v2}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2562470
    iget-object p0, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562471
    sget-object v1, LX/IJV;->HEADER:LX/IJV;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;LX/IJV;Ljava/lang/String;)V

    .line 2562472
    const/4 v1, 0x1

    .line 2562473
    :goto_1
    move v0, v1

    .line 2562474
    goto :goto_0

    .line 2562475
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/IJV;)LX/IJX;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2562627
    sget-object v0, LX/IIe;->a:[I

    invoke-virtual {p1}, LX/IJV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2562628
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid map state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2562629
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/SplitHideableListView;->f()J

    move-result-wide v6

    .line 2562630
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->am:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2562631
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ak:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2562632
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2562633
    new-instance v1, LX/IJX;

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    .line 2562634
    iget-object v5, v4, Lcom/facebook/widget/listview/BetterListView;->r:LX/2i5;

    move-object v4, v5

    .line 2562635
    iget-object v5, v4, LX/2i5;->c:LX/2ht;

    if-nez v5, :cond_0

    .line 2562636
    const/4 v5, 0x0

    .line 2562637
    :goto_0
    move v4, v5

    .line 2562638
    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v4

    sub-int v5, v4, v0

    move v4, v2

    invoke-direct/range {v1 .. v7}, LX/IJX;-><init>(IIIIJ)V

    .line 2562639
    :goto_1
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    .line 2562640
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O()V

    .line 2562641
    return-object v1

    .line 2562642
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/SplitHideableListView;->e()J

    move-result-wide v6

    .line 2562643
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->al:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2562644
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aj:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2562645
    new-instance v1, LX/IJX;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v1 .. v7}, LX/IJX;-><init>(IIIIJ)V

    goto :goto_1

    :cond_0
    iget-object v5, v4, LX/2i5;->c:LX/2ht;

    iget-object v8, v4, LX/2i5;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v8}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v8

    invoke-interface {v5, v8}, LX/2ht;->e(I)I

    move-result v5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2562646
    const-string v0, "friends_nearby"

    return-object v0
.end method

.method public final a(LX/IFR;)V
    .locals 14

    .prologue
    .line 2562502
    invoke-interface {p1}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v0

    .line 2562503
    invoke-interface {p1}, LX/IFR;->e()Ljava/lang/String;

    move-result-object v1

    .line 2562504
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2562505
    const-string v2, "check_in"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2562506
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 2562507
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    .line 2562508
    iput-object v0, v1, LX/89k;->b:Ljava/lang/String;

    .line 2562509
    move-object v0, v1

    .line 2562510
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2562511
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->F:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2562512
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2562513
    :goto_0
    return-void

    .line 2562514
    :cond_0
    instance-of v1, p1, LX/IFx;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->r:LX/IFJ;

    .line 2562515
    iget-object v2, v1, LX/IFJ;->a:LX/EnT;

    const-string v3, "friends_nearby"

    invoke-virtual {v2, v3}, LX/EnT;->a(Ljava/lang/String;)Z

    move-result v2

    move v1, v2

    .line 2562516
    if-nez v1, :cond_2

    .line 2562517
    :cond_1
    const/4 p1, 0x0

    .line 2562518
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    invoke-virtual {v1, v0, p1}, LX/IID;->a(Ljava/lang/String;Z)V

    .line 2562519
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2562520
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, p1

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2562521
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2562522
    goto :goto_0

    .line 2562523
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->G:LX/0Uh;

    const/16 v2, 0x586

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2562524
    invoke-direct {p0, p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->c(LX/IFR;)Ljava/lang/String;

    move-result-object v1

    .line 2562525
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class p1, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    invoke-direct {v2, v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2562526
    const-string v3, "user_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2562527
    const-string v3, "section_title"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2562528
    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v3, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2562529
    goto :goto_0

    .line 2562530
    :cond_3
    invoke-direct {p0, p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->c(LX/IFR;)Ljava/lang/String;

    move-result-object v1

    .line 2562531
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-interface {p1}, LX/IFR;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/IFX;->d(Ljava/lang/String;)LX/IFd;

    move-result-object v2

    .line 2562532
    if-nez v2, :cond_7

    .line 2562533
    new-instance v2, LX/IFL;

    invoke-interface {p1}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-interface {p1}, LX/IFR;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/IFR;->b()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LX/IFL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2562534
    :goto_1
    move-object v2, v2

    .line 2562535
    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, LX/IID;->a(Ljava/lang/String;Z)V

    .line 2562536
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    .line 2562537
    if-eqz v4, :cond_6

    .line 2562538
    iget-object v5, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->r:LX/IFJ;

    .line 2562539
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2562540
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_2
    if-ge v7, v9, :cond_4

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/IFL;

    .line 2562541
    iget-object v10, v6, LX/IFL;->a:Ljava/lang/String;

    move-object v6, v10

    .line 2562542
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2562543
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 2562544
    :cond_4
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v10, v6

    .line 2562545
    invoke-static {v10, v0}, LX/EnR;->a(LX/0Px;Ljava/lang/String;)LX/EnQ;

    move-result-object v8

    .line 2562546
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v9

    .line 2562547
    iget v6, v8, LX/EnQ;->d:I

    move v7, v6

    .line 2562548
    :goto_3
    iget v6, v8, LX/EnQ;->e:I

    if-gt v7, v6, :cond_5

    .line 2562549
    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/IFL;

    .line 2562550
    new-instance v11, LX/Eov;

    invoke-direct {v11}, LX/Eov;-><init>()V

    .line 2562551
    iget-object v12, v6, LX/IFL;->a:Ljava/lang/String;

    move-object v12, v12

    .line 2562552
    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 2562553
    iput-object v12, v11, LX/Eov;->k:Ljava/lang/String;

    .line 2562554
    move-object v11, v11

    .line 2562555
    iget-object v12, v6, LX/IFL;->b:Ljava/lang/String;

    move-object v12, v12

    .line 2562556
    iput-object v12, v11, LX/Eov;->p:Ljava/lang/String;

    .line 2562557
    move-object v11, v11

    .line 2562558
    new-instance v12, LX/4aM;

    invoke-direct {v12}, LX/4aM;-><init>()V

    .line 2562559
    iget-object v13, v6, LX/IFL;->c:Ljava/lang/String;

    move-object v13, v13

    .line 2562560
    iput-object v13, v12, LX/4aM;->b:Ljava/lang/String;

    .line 2562561
    move-object v12, v12

    .line 2562562
    invoke-virtual {v12}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v12

    .line 2562563
    iput-object v12, v11, LX/Eov;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2562564
    move-object v11, v11

    .line 2562565
    invoke-virtual {v11}, LX/Eov;->a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v11

    move-object v6, v11

    .line 2562566
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2562567
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_3

    .line 2562568
    :cond_5
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 2562569
    const-string v6, "preliminary_entities"

    invoke-static {v13, v6, v9}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2562570
    const-string v6, "extra_friending_location_name"

    sget-object v7, LX/2h7;->NEARBY_FRIENDS:LX/2h7;

    invoke-virtual {v7}, LX/2h7;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562571
    const-string v6, "extra_friend_request_make_ref"

    sget-object v7, LX/5P2;->NEARBY_FRIENDS:LX/5P2;

    invoke-virtual {v7}, LX/5P2;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562572
    iget-object v6, v5, LX/IFJ;->a:LX/EnT;

    const-string v8, "friends_nearby"

    const/4 v9, 0x0

    move-object v7, v4

    move-object v11, v0

    move-object v12, v1

    invoke-virtual/range {v6 .. v13}, LX/EnT;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2562573
    :cond_6
    goto/16 :goto_0

    .line 2562574
    :cond_7
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2562575
    invoke-virtual {v2}, LX/622;->e()Ljava/util/List;

    move-result-object v2

    .line 2562576
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IFR;

    .line 2562577
    new-instance v5, LX/IFL;

    invoke-interface {v2}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-interface {v2}, LX/IFR;->c()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2}, LX/IFR;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v7, v2}, LX/IFL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2562578
    :cond_8
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2562647
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2562648
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2562649
    if-eqz p1, :cond_0

    .line 2562650
    const-string v0, "redirected_to_nux"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->af:Z

    .line 2562651
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->a(LX/IG6;)V

    .line 2562652
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_INIT:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->a(LX/IG6;)V

    .line 2562653
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aa:LX/IJH;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562654
    iget-object v2, v0, LX/IJH;->e:LX/IFX;

    if-ne v2, v1, :cond_1

    .line 2562655
    :goto_0
    new-instance v0, LX/IIx;

    invoke-direct {v0, p0}, LX/IIx;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aq:LX/IIx;

    .line 2562656
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ap:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/2ga;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aq:LX/IIx;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    sget-object v1, LX/1rz;->a:Ljava/lang/String;

    new-instance v2, LX/IIZ;

    invoke-direct {v2, p0}, LX/IIZ;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ar:LX/0Yb;

    .line 2562657
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ar:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2562658
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x:LX/2do;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aL:LX/2hK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2562659
    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->y:LX/2l5;

    const-wide v5, 0x211dfd19646adL

    const/4 v4, 0x0

    invoke-interface {v3, v5, v6, v4}, LX/2l5;->a(JI)V

    .line 2562660
    return-void

    .line 2562661
    :cond_1
    iput-object v1, v0, LX/IJH;->e:LX/IFX;

    .line 2562662
    new-instance v2, LX/IJD;

    invoke-direct {v2, v0}, LX/IJD;-><init>(LX/IJH;)V

    invoke-virtual {v1, v2}, LX/IFX;->a(LX/IFW;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;LX/IFR;)V
    .locals 10

    .prologue
    .line 2562663
    invoke-interface {p2}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v4

    .line 2562664
    instance-of v0, p2, LX/IFx;

    if-eqz v0, :cond_1

    .line 2562665
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->G:LX/0Uh;

    const/16 v1, 0x21d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2562666
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    .line 2562667
    iget-object v1, p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2562668
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/IIB;->a(Landroid/view/View;LX/0gc;LX/IFX;LX/IIm;)V

    .line 2562669
    :goto_0
    return-void

    .line 2562670
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    .line 2562671
    iget-object v0, p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v2, v0

    .line 2562672
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2562673
    iget-wide v8, v0, LX/IID;->c:J

    move-wide v4, v8

    .line 2562674
    iget-object v6, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ao:Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    iget-object v7, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    invoke-virtual/range {v1 .. v7}, LX/IIB;->a(Landroid/view/View;LX/0gc;JLcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;LX/0am;)V

    goto :goto_0

    .line 2562675
    :cond_1
    instance-of v0, p2, LX/IFZ;

    if-nez v0, :cond_2

    instance-of v0, p2, LX/IFf;

    if-eqz v0, :cond_3

    .line 2562676
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    move-object v1, p1

    move-object v5, p2

    .line 2562677
    invoke-interface {v5}, LX/IFR;->h()LX/3OL;

    move-result-object v6

    sget-object v7, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v6, v7, :cond_6

    .line 2562678
    invoke-virtual {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 2562679
    sget-object v7, LX/II1;->a:[I

    invoke-interface {v5}, LX/IFR;->h()LX/3OL;

    move-result-object v8

    invoke-virtual {v8}, LX/3OL;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2562680
    :goto_1
    goto :goto_0

    .line 2562681
    :cond_3
    instance-of v0, p2, LX/IFS;

    if-eqz v0, :cond_5

    .line 2562682
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    .line 2562683
    check-cast p2, LX/IFS;

    .line 2562684
    iget-object v1, p2, LX/IFS;->e:LX/IFQ;

    move-object v1, v1

    .line 2562685
    sget-object v2, LX/IFQ;->NOT_INVITED:LX/IFQ;

    if-ne v1, v2, :cond_a

    .line 2562686
    iget-object v1, v0, LX/IIB;->f:LX/IID;

    .line 2562687
    const-string v2, "friends_nearby_search_section_onetap_invite"

    invoke-static {v1, v2}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2562688
    iget-object v3, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562689
    sget-object v1, LX/IFQ;->INVITING:LX/IFQ;

    .line 2562690
    iput-object v1, p2, LX/IFS;->e:LX/IFQ;

    .line 2562691
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->e()V

    .line 2562692
    iget-object v1, v0, LX/IIB;->a:LX/1Ck;

    .line 2562693
    sget-object v2, LX/IFy;->n:LX/IFy;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/IFy;

    move-object v2, v2

    .line 2562694
    iget-object v3, v0, LX/IIB;->m:LX/0aG;

    const-string v5, "send_invite"

    const-string v6, "friendsNearbyInviteParams"

    new-instance v7, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;

    invoke-direct {v7, v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, LX/IHu;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Bundle;

    move-result-object v6

    const v7, 0x617d3365

    invoke-static {v3, v5, v6, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    new-instance v5, LX/IHz;

    invoke-direct {v5, v0, p2, p1}, LX/IHz;-><init>(LX/IIB;LX/IFS;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v1, v2, v3, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2562695
    :cond_4
    :goto_2
    goto/16 :goto_0

    .line 2562696
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action button behavior for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2562697
    :cond_6
    iget-object v6, v0, LX/IIB;->d:LX/0Uh;

    const/16 v7, 0x583

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2562698
    invoke-virtual {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v0, v6, v4}, LX/IIB;->a(LX/IIB;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2562699
    :cond_7
    invoke-interface {v5}, LX/IFR;->l()Ljava/lang/String;

    move-result-object v6

    .line 2562700
    invoke-virtual {v3, v4}, LX/IFX;->c(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v7

    .line 2562701
    if-nez v7, :cond_8

    .line 2562702
    iget-object v7, v0, LX/IIB;->c:LX/03V;

    const-string v8, "friends_nearby_ping_failed"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "target user "

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string p0, " information cannot be fetched in section "

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562703
    :goto_3
    goto/16 :goto_1

    .line 2562704
    :pswitch_0
    iget-object v7, v0, LX/IIB;->f:LX/IID;

    invoke-interface {v5}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/IID;->d(Ljava/lang/String;)V

    .line 2562705
    sget-object v7, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    invoke-interface {v5, v7}, LX/IFR;->a(LX/3OL;)V

    .line 2562706
    const v7, 0x7f082eca

    invoke-interface {v5, v6, v7}, LX/IFR;->a(Landroid/content/Context;I)V

    .line 2562707
    invoke-interface {v5}, LX/IFR;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2562708
    iget-object v7, v0, LX/IIB;->n:LX/3LX;

    new-instance v8, LX/IHv;

    invoke-direct {v8, v0, v5, v6, v1}, LX/IHv;-><init>(LX/IIB;LX/IFR;Landroid/content/Context;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v7, v4, v8}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto/16 :goto_1

    .line 2562709
    :pswitch_1
    iget-object v7, v0, LX/IIB;->f:LX/IID;

    invoke-interface {v5}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/IID;->d(Ljava/lang/String;)V

    .line 2562710
    sget-object v7, LX/3OL;->INTERACTED:LX/3OL;

    invoke-interface {v5, v7}, LX/IFR;->a(LX/3OL;)V

    .line 2562711
    const v7, 0x7f082eca

    invoke-interface {v5, v6, v7}, LX/IFR;->a(Landroid/content/Context;I)V

    .line 2562712
    invoke-interface {v5}, LX/IFR;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2562713
    iget-object v7, v0, LX/IIB;->n:LX/3LX;

    new-instance v8, LX/IHw;

    invoke-direct {v8, v0, v5, v6, v1}, LX/IHw;-><init>(LX/IIB;LX/IFR;Landroid/content/Context;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v7, v4, v8}, LX/3LX;->a(Ljava/lang/String;LX/DI1;)V

    goto/16 :goto_1

    .line 2562714
    :pswitch_2
    iget-object v7, v0, LX/IIB;->f:LX/IID;

    invoke-interface {v5}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v8

    .line 2562715
    const-string v9, "friends_nearby_dashboard_ping"

    invoke-static {v7, v9}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 2562716
    const-string p0, "targetID"

    invoke-virtual {v9, p0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v2, "action"

    const-string v3, "wave_canceled"

    invoke-virtual {p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2562717
    iget-object p0, v7, LX/IID;->a:LX/0Zb;

    invoke-interface {p0, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562718
    sget-object v7, LX/3OL;->NOT_SENT:LX/3OL;

    invoke-interface {v5, v7}, LX/IFR;->a(LX/3OL;)V

    .line 2562719
    const v7, 0x7f082ecb

    invoke-interface {v5, v6, v7}, LX/IFR;->a(Landroid/content/Context;I)V

    .line 2562720
    invoke-interface {v5}, LX/IFR;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2562721
    iget-object v7, v0, LX/IIB;->n:LX/3LX;

    new-instance v8, LX/IHx;

    invoke-direct {v8, v0, v5, v6, v1}, LX/IHx;-><init>(LX/IIB;LX/IFR;Landroid/content/Context;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v7, v4, v8}, LX/3LX;->b(Ljava/lang/String;LX/DI1;)V

    goto/16 :goto_1

    .line 2562722
    :pswitch_3
    invoke-static {v0, v6, v4}, LX/IIB;->a(LX/IIB;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2562723
    :cond_8
    iget-boolean v8, v3, LX/IFX;->x:Z

    move v8, v8

    .line 2562724
    if-eqz v8, :cond_9

    .line 2562725
    invoke-virtual {v3, v4}, LX/IFX;->a(Ljava/lang/String;)Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    move-result-object v8

    .line 2562726
    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/user/model/User;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Z)Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    move-result-object v9

    move-object v7, v9

    .line 2562727
    :goto_4
    new-instance v8, LX/IHy;

    invoke-direct {v8, v0, v3, v4}, LX/IHy;-><init>(LX/IIB;LX/IFX;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;->a(LX/IHy;)V

    .line 2562728
    const-string v8, "ping_dialog"

    invoke-virtual {v7, v2, v8}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2562729
    :cond_9
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/user/model/User;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Z)Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    move-result-object v8

    move-object v7, v8

    .line 2562730
    goto :goto_4

    .line 2562731
    :cond_a
    sget-object v2, LX/IFQ;->INVITED:LX/IFQ;

    if-ne v1, v2, :cond_4

    .line 2562732
    iget-object v1, v0, LX/IIB;->f:LX/IID;

    .line 2562733
    const-string v2, "friends_nearby_search_section_onetap_undo"

    invoke-static {v1, v2}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2562734
    iget-object v3, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562735
    sget-object v1, LX/IFQ;->UNINVITING:LX/IFQ;

    .line 2562736
    iput-object v1, p2, LX/IFS;->e:LX/IFQ;

    .line 2562737
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->e()V

    .line 2562738
    iget-object v1, v0, LX/IIB;->a:LX/1Ck;

    .line 2562739
    sget-object v2, LX/IFy;->o:LX/IFy;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/IFy;

    move-object v2, v2

    .line 2562740
    iget-object v3, v0, LX/IIB;->m:LX/0aG;

    const-string v5, "delete_invite"

    const-string v6, "friendsNearbyDeleteInviteParams"

    new-instance v7, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;

    invoke-direct {v7, v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, LX/IHu;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Bundle;

    move-result-object v6

    const v7, -0x10722371

    invoke-static {v3, v5, v6, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    new-instance v5, LX/II0;

    invoke-direct {v5, v0, p2, p1}, LX/II0;-><init>(LX/IIB;LX/IFS;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v1, v2, v3, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2562741
    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c284daa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2562742
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2562743
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->as:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->at:LX/0i5;

    .line 2562744
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->au:LX/6Zb;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->av:LX/6Ze;

    invoke-virtual {v0, p0, v2}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2562745
    const v0, 0x7f0d12f5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    .line 2562746
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2562747
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearComposingText()V

    .line 2562748
    const v0, 0x7f0d12f4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M:Landroid/view/View;

    .line 2562749
    const v0, 0x7f0d12f6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->N:Landroid/view/View;

    .line 2562750
    const v0, 0x7f0d12f7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O:Lcom/facebook/widget/text/BetterButton;

    .line 2562751
    const v0, 0x7f0d12f8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/62k;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    .line 2562752
    const v0, 0x7f0d12f9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/SplitHideableListView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    .line 2562753
    const v0, 0x7f0d12fa

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->U:Landroid/widget/TextView;

    .line 2562754
    const v0, 0x7f0d12fb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    .line 2562755
    const v0, 0x7f0d12fc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    .line 2562756
    const v0, 0x7f0d12fd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->V:Landroid/view/View;

    .line 2562757
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->V:Landroid/view/View;

    const v2, 0x7f0d12fe

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->W:Landroid/widget/TextView;

    .line 2562758
    const v0, 0x7f0d12ff

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    .line 2562759
    const v0, 0x7f0d1300

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Y:Lcom/facebook/friendsnearby/ui/FriendsNearbyLocationDisabledView;

    .line 2562760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562761
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ad:J

    .line 2562762
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2562763
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->P:LX/62k;

    new-instance v2, LX/IIg;

    invoke-direct {v2, p0}, LX/IIg;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-interface {v0, v2}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 2562764
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->u(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562765
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2562766
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03071e

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2562767
    new-instance v2, LX/IIh;

    invoke-direct {v2, p0, v0}, LX/IIh;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2562768
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/listview/SplitHideableListView;->setExpandableHeader(Landroid/view/View;)V

    .line 2562769
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03071f

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    .line 2562770
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->R:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v6, v5}, Lcom/facebook/widget/listview/SplitHideableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2562771
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03071d

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2562772
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2, v0, v6, v5}, Lcom/facebook/widget/listview/SplitHideableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2562773
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aa:LX/IJH;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/SplitHideableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2562774
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2, v7}, Lcom/facebook/widget/listview/BetterListView;->setBroadcastInteractionChanges(Z)V

    .line 2562775
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    new-instance v3, LX/IIi;

    invoke-direct {v3, p0, v0}, LX/IIi;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V

    .line 2562776
    iput-object v3, v2, Lcom/facebook/widget/listview/SplitHideableListView;->h:LX/62K;

    .line 2562777
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    new-instance v2, LX/IIj;

    invoke-direct {v2, p0}, LX/IIj;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2562778
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    new-instance v2, LX/IIk;

    invoke-direct {v2, p0}, LX/IIk;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2562779
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->X:Lcom/facebook/friendsnearby/ui/FriendsNearbyErrorScreenView;

    new-instance v2, LX/IIl;

    invoke-direct {v2, p0}, LX/IIl;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2562780
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M:Landroid/view/View;

    new-instance v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment$10;

    invoke-direct {v2, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment$10;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2562781
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->A:LX/0wM;

    const v2, 0x7f020886

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0a00d2

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2562782
    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v2, v0, v6, v6, v6}, Lcom/facebook/widget/text/BetterButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2562783
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O:Lcom/facebook/widget/text/BetterButton;

    new-instance v2, LX/IIF;

    invoke-direct {v2, p0}, LX/IIF;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2562784
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    new-instance v2, LX/IIG;

    invoke-direct {v2, p0}, LX/IIG;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2562785
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    new-instance v2, LX/IIH;

    invoke-direct {v2, p0}, LX/IIH;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2562786
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    new-instance v2, LX/III;

    invoke-direct {v2, p0}, LX/III;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v0, v2}, LX/IFX;->a(LX/IFW;)V

    .line 2562787
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562788
    const/16 v0, 0x2b

    const v2, -0x65d82528

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2562789
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    sget-object v1, LX/IJV;->HEADER:LX/IJV;

    if-ne v0, v1, :cond_0

    .line 2562790
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ag:Z

    .line 2562791
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7f6eb0a0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2562792
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0bd1

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 2562793
    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2562794
    const v2, 0x7f030719

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x39740645

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x279e3c64

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2562795
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2562796
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->s:LX/IFl;

    .line 2562797
    iget-object v2, v0, LX/IFl;->c:LX/1Ck;

    sget-object v3, LX/IFy;->k:LX/IFy;

    invoke-virtual {v2, v3}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2562798
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFU;

    .line 2562799
    iget-object v3, v0, LX/IFU;->d:LX/1Ck;

    sget-object v4, LX/IFy;->i:LX/IFy;

    invoke-virtual {v3, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2562800
    goto :goto_0

    .line 2562801
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2562802
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    .line 2562803
    invoke-static {}, LX/IG6;->values()[LX/IG6;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2562804
    iget-object v6, v0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v7, v5, LX/IG6;->markerId:I

    iget-object v8, v5, LX/IG6;->markerName:Ljava/lang/String;

    invoke-interface {v6, v7, v8}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2562805
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2562806
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ar:LX/0Yb;

    if-eqz v0, :cond_2

    .line 2562807
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ar:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2562808
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x:LX/2do;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aL:LX/2hK;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2562809
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->au:LX/6Zb;

    invoke-virtual {v0}, LX/6Zb;->a()V

    .line 2562810
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2562811
    const v0, -0x622e54a7

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4095f514

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2562812
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2562813
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->at:LX/0i5;

    .line 2562814
    const/16 v1, 0x2b

    const v2, 0x1ba6b778

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x74f35189

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2562815
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2562816
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2562817
    if-nez v1, :cond_0

    .line 2562818
    :goto_0
    invoke-static {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->v$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562819
    const/16 v1, 0x2b

    const v2, 0x7d8ca6de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2562820
    :cond_0
    const-string v2, ""

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2562821
    invoke-interface {v1}, LX/1ZF;->lH_()V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x7afc3523

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2562822
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2562823
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    const/4 v13, 0x0

    .line 2562824
    iget-wide v7, v1, LX/IID;->c:J

    .line 2562825
    iget-object v9, v1, LX/IID;->b:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-virtual {v9}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v9

    .line 2562826
    const-wide/16 v11, 0x0

    cmp-long v11, v9, v11

    if-eqz v11, :cond_0

    sub-long v7, v9, v7

    const-wide/32 v11, 0x493e0

    cmp-long v7, v7, v11

    if-lez v7, :cond_1

    .line 2562827
    :cond_0
    iput-wide v9, v1, LX/IID;->c:J

    .line 2562828
    sget-object v7, LX/IIC;->UNKNOWN:LX/IIC;

    iput-object v7, v1, LX/IID;->d:LX/IIC;

    .line 2562829
    iput-boolean v13, v1, LX/IID;->e:Z

    .line 2562830
    iput-boolean v13, v1, LX/IID;->f:Z

    .line 2562831
    :cond_1
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n()V

    .line 2562832
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O()V

    .line 2562833
    iget-boolean v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->o:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ad:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x493e0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 2562834
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v1}, LX/IFX;->a()V

    .line 2562835
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aw:LX/IIm;

    invoke-static {p0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562836
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x4babfaff    # 2.2541822E7f

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2562837
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2562838
    const-string v0, "redirected_to_nux"

    iget-boolean v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->af:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2562839
    return-void
.end method
