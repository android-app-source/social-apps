.class public Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;
.super Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/IID;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private p:Landroid/widget/Button;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

.field public s:LX/IFR;

.field public t:LX/IJ4;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2564137
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    const-string v1, "friends_nearby"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2564138
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;)V

    .line 2564139
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->f()V

    .line 2564140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2564074
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2564075
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->f()V

    .line 2564076
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2564141
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2564142
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->f()V

    .line 2564143
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2564144
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2564145
    const v1, 0x7f082ece

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 2564146
    const v1, 0x7f082ecf

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2564147
    const/4 v1, -0x1

    .line 2564148
    iput v1, v0, LX/0hs;->t:I

    .line 2564149
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2564150
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->m:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4541"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2564151
    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;LX/0Uh;LX/0wM;LX/IID;LX/0iA;)V
    .locals 0

    .prologue
    .line 2564152
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->j:LX/0Uh;

    iput-object p2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->k:LX/0wM;

    iput-object p3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->l:LX/IID;

    iput-object p4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->m:LX/0iA;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-static {v3}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {v3}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v3}, LX/IID;->a(LX/0QB;)LX/IID;

    move-result-object v2

    check-cast v2, LX/IID;

    invoke-static {v3}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;LX/0Uh;LX/0wM;LX/IID;LX/0iA;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2564120
    if-eqz p1, :cond_0

    .line 2564121
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2564122
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->k:LX/0wM;

    const v2, 0x7f020912

    const v3, 0x7f0a094e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2564123
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2564124
    const v2, 0x7f0b0f3a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2564125
    invoke-virtual {v1, v4, v4, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2564126
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2564127
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2564128
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v1, v5}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v4, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2564129
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, LX/IFR;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2564130
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2564131
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2564132
    const/4 v0, 0x2

    invoke-virtual {p0, v5, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 2564133
    :goto_0
    return-void

    .line 2564134
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LX/IFR;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2564135
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->d()V

    .line 2564136
    invoke-virtual {p0, v5}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2564102
    const-class v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2564103
    const v0, 0x7f030726

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2564104
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2564105
    const v0, 0x7f0207fb

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->setBackgroundResource(I)V

    .line 2564106
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a094f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 2564107
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v2, v0, v2, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2564108
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2564109
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 2564110
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2564111
    move-object v0, v0

    .line 2564112
    const v1, 0x7f0203b2

    invoke-virtual {v0, v1}, LX/1Uo;->b(I)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2564113
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2564114
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 2564115
    sget-object v0, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2564116
    const v0, 0x7f0d131b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->p:Landroid/widget/Button;

    .line 2564117
    new-instance v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->r:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    .line 2564118
    new-instance v0, LX/IJf;

    invoke-direct {v0, p0}, LX/IJf;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2564119
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2564097
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "aura"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020ba5

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    .line 2564098
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->requestLayout()V

    .line 2564099
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->invalidate()V

    .line 2564100
    return-void

    .line 2564101
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020b8d

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Z
    .locals 3

    .prologue
    .line 2564095
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->m:LX/0iA;

    sget-object v1, LX/DHy;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/DHy;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/DHy;

    .line 2564096
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/DHy;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4541"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/IFR;LX/IJ4;)V
    .locals 0

    .prologue
    .line 2564091
    iput-object p1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    .line 2564092
    iput-object p2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->t:LX/IJ4;

    .line 2564093
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->e()V

    .line 2564094
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2564077
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2564078
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 2564079
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->l:LX/IID;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v1}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v1

    .line 2564080
    const-string v2, "friends_nearby_dashboard_friend_row_seen"

    invoke-static {v0, v2}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2564081
    const-string v3, "targetID"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2564082
    iget-object v3, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2564083
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->h()LX/3OL;

    move-result-object v0

    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->h()LX/3OL;

    move-result-object v0

    sget-object v1, LX/3OL;->INTERACTED:LX/3OL;

    if-eq v0, v1, :cond_0

    .line 2564084
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->l:LX/IID;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v1}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v2}, LX/IFR;->h()LX/3OL;

    move-result-object v2

    .line 2564085
    const-string v3, "friends_nearby_wave_impression"

    invoke-static {v0, v3}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2564086
    const-string v4, "targetID"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "wave_state"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2564087
    iget-object v4, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2564088
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2564089
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2564090
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2564038
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2564039
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2564040
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2564041
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v4}, LX/IFR;->b()Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2564042
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->e()Ljava/lang/String;

    move-result-object v0

    const-string v4, "check_in"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(Z)V

    .line 2564043
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2564044
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getContext()Landroid/content/Context;

    invoke-interface {v0}, LX/IFR;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2564045
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v2}, LX/IFR;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2564046
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->g()V

    .line 2564047
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->j:LX/0Uh;

    const/16 v4, 0x21d

    invoke-virtual {v0, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2564048
    new-instance v0, Lcom/facebook/widget/SwitchCompat;

    invoke-direct {v0, v2}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 2564049
    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v1}, LX/IFR;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setCheckedNoAnimation(Z)V

    .line 2564050
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2564051
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->h()LX/3OL;

    move-result-object v0

    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v0, v1, :cond_4

    .line 2564052
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    new-instance v1, LX/IJg;

    invoke-direct {v1, p0}, LX/IJg;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    .line 2564053
    iput-object v1, v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->d:LX/DHx;

    .line 2564054
    :goto_1
    return-void

    .line 2564055
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->h()LX/3OL;

    move-result-object v0

    sget-object v4, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v0, v4, :cond_2

    .line 2564056
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->r:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v1, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v1}, LX/IFR;->h()LX/3OL;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setWaveState(LX/3OL;)V

    .line 2564057
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->r:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2564058
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v2}, LX/IFR;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2564059
    invoke-direct {p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2564060
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->r:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    invoke-direct {p0, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(Landroid/view/View;)V

    goto :goto_0

    .line 2564061
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->p:Landroid/widget/Button;

    iget-object v4, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-virtual {p0, v0, v4, v5}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2564062
    iget-object v4, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2564063
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2564064
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    iget-object v4, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v4}, LX/IFR;->j()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2564065
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v3}, LX/IFR;->b(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2564066
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v2}, LX/IFR;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 2564067
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->k()LX/6VG;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTheme(LX/6VG;)V

    .line 2564068
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v2}, LX/IFR;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2564069
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0}, LX/IFR;->k()LX/6VG;

    move-result-object v0

    sget-object v2, LX/6VG;->NONE:LX/6VG;

    if-ne v0, v2, :cond_0

    .line 2564070
    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v0, v3}, LX/IFR;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2564071
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 2564072
    :cond_3
    const/16 v0, 0x8

    goto :goto_2

    .line 2564073
    :cond_4
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    new-instance v1, LX/IJh;

    invoke-direct {v1, p0}, LX/IJh;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v2, -0x1

    .line 2564032
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->onLayout(ZIIII)V

    .line 2564033
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    if-nez v0, :cond_1

    .line 2564034
    :cond_0
    :goto_0
    return-void

    .line 2564035
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    const/16 v1, 0x24

    .line 2564036
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    move-object v3, v0

    move v5, v1

    move v6, v2

    move v7, v2

    move v8, v2

    invoke-static/range {v3 .. v8}, LX/190;->a(Landroid/view/View;Landroid/view/ViewParent;IIII)Landroid/view/TouchDelegate;

    move-result-object v3

    move-object v1, v3

    .line 2564037
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->s:LX/IFR;

    invoke-interface {v2}, LX/IFR;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
