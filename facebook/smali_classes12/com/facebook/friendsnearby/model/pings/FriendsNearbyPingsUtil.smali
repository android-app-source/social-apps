.class public Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile l:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;


# instance fields
.field private final b:LX/0SG;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/11S;

.field private final e:LX/IFO;

.field private final f:LX/0ad;

.field private final g:LX/IFc;

.field private final h:LX/0Uh;

.field private final i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0aG;

.field public final k:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2555172
    const-class v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;LX/11S;LX/IFO;LX/0ad;LX/IFc;LX/0Uh;LX/1Ck;LX/0aG;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2555173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555174
    iput-object p1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->b:LX/0SG;

    .line 2555175
    iput-object p2, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c:Landroid/content/res/Resources;

    .line 2555176
    iput-object p3, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->d:LX/11S;

    .line 2555177
    iput-object p4, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->e:LX/IFO;

    .line 2555178
    iput-object p5, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->f:LX/0ad;

    .line 2555179
    iput-object p6, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->g:LX/IFc;

    .line 2555180
    iput-object p7, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->h:LX/0Uh;

    .line 2555181
    iput-object p8, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->i:LX/1Ck;

    .line 2555182
    iput-object p9, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->j:LX/0aG;

    .line 2555183
    iput-object p10, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->k:LX/03V;

    .line 2555184
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;
    .locals 14

    .prologue
    .line 2555185
    sget-object v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->l:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    if-nez v0, :cond_1

    .line 2555186
    const-class v1, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    monitor-enter v1

    .line 2555187
    :try_start_0
    sget-object v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->l:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2555188
    if-eqz v2, :cond_0

    .line 2555189
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2555190
    new-instance v3, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11S;

    invoke-static {v0}, LX/IFO;->a(LX/0QB;)LX/IFO;

    move-result-object v7

    check-cast v7, LX/IFO;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    const-class v9, LX/IFc;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/IFc;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v12

    check-cast v12, LX/0aG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;-><init>(LX/0SG;Landroid/content/res/Resources;LX/11S;LX/IFO;LX/0ad;LX/IFc;LX/0Uh;LX/1Ck;LX/0aG;LX/03V;)V

    .line 2555191
    move-object v0, v3

    .line 2555192
    sput-object v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->l:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2555193
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2555194
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2555195
    :cond_1
    sget-object v0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->l:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    return-object v0

    .line 2555196
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2555197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2555107
    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v1, LX/IGG;->FOREVER:LX/IGG;

    if-ne v0, v1, :cond_0

    .line 2555108
    iget-object v0, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c:Landroid/content/res/Resources;

    const v1, 0x7f083855    # 1.810675E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2555109
    :goto_0
    return-object v0

    .line 2555110
    :cond_0
    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2555111
    iget-object v1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->d:LX/11S;

    sget-object v2, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2555112
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2555167
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingDeleteParams;

    invoke-direct {v0, p1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingDeleteParams;-><init>(Ljava/lang/String;)V

    .line 2555168
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2555169
    const-string v1, "locationPingDeleteParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2555170
    iget-object v6, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->i:LX/1Ck;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "delete_outgoing_ping"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->j:LX/0aG;

    const-string v1, "delete_ping"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0xa70171e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/IG3;

    invoke-direct {v1, p0}, LX/IG3;-><init>(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2555171
    return-void
.end method


# virtual methods
.method public final a(LX/IG2;)LX/IFd;
    .locals 3

    .prologue
    .line 2555160
    iget-object v0, p1, LX/IG2;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2555161
    if-nez v0, :cond_0

    .line 2555162
    sget-object v0, LX/IFX;->b:LX/IFd;

    .line 2555163
    :goto_1
    return-object v0

    :cond_0
    const-string v0, "Outgoing_Pings"

    iget-object v1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c:Landroid/content/res/Resources;

    const v2, 0x7f083856

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2555164
    iget-object v2, p1, LX/IG2;->a:Ljava/util/ArrayList;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2555165
    new-instance p0, LX/IFe;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    invoke-direct {p0, v1, v0, p1}, LX/IFe;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    move-object v0, p0

    .line 2555166
    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/IG2;LX/IFz;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/IG2;
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    .line 2555115
    iget-object v0, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v5

    .line 2555116
    if-nez p3, :cond_3

    .line 2555117
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2555118
    :goto_0
    move-object v0, v0

    .line 2555119
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;

    .line 2555120
    iget-object v0, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->h:LX/0Uh;

    const/16 v1, 0x583

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2555121
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Ljava/lang/String;)V

    goto :goto_1

    .line 2555122
    :cond_0
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->b()Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->d()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v10

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v6}, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLocationPingType;JLjava/lang/String;J)Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    move-result-object v0

    .line 2555123
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->d()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Outgoing_Pings"

    invoke-static {v0, v1, v2, v3, v4}, LX/IFf;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/IFf;

    move-result-object v0

    .line 2555124
    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2555125
    invoke-virtual {p1, v1, v0}, LX/IG2;->a(Ljava/lang/String;LX/IFf;)V

    .line 2555126
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    invoke-virtual {v7}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$OutgoingLocationPingWithRecipientModel$RecipientModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2555127
    iput-object v2, v0, LX/0XI;->h:Ljava/lang/String;

    .line 2555128
    move-object v0, v0

    .line 2555129
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    move-object v0, v0

    .line 2555130
    invoke-virtual {p2, v1, v0}, LX/IFz;->a(Ljava/lang/String;Lcom/facebook/user/model/User;)V

    goto/16 :goto_1

    .line 2555131
    :cond_1
    if-nez p3, :cond_5

    .line 2555132
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2555133
    :goto_2
    move-object v0, v0

    .line 2555134
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;

    .line 2555135
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 2555136
    iget-object v1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->d:LX/11S;

    sget-object v2, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->c()J

    move-result-wide v4

    mul-long/2addr v4, v10

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    .line 2555137
    iget-object v1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->e:LX/IFO;

    const-string v3, "Pings"

    invoke-virtual {p1, v7}, LX/IG2;->b(Ljava/lang/String;)Z

    move-result v4

    iget-object v5, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->g:LX/IFc;

    invoke-static/range {v0 .. v5}, LX/IFb;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;LX/IFO;Ljava/lang/String;Ljava/lang/String;ZLX/IFc;)LX/IFb;

    move-result-object v1

    .line 2555138
    iget-object v2, p1, LX/IG2;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2555139
    iget-object v2, p1, LX/IG2;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2555140
    new-instance v1, LX/0XI;

    invoke-direct {v1}, LX/0XI;-><init>()V

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$SenderModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2555141
    iput-object v2, v1, LX/0XI;->h:Ljava/lang/String;

    .line 2555142
    move-object v1, v1

    .line 2555143
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    move-object v0, v1

    .line 2555144
    invoke-virtual {p2, v7, v0}, LX/IFz;->a(Ljava/lang/String;Lcom/facebook/user/model/User;)V

    goto :goto_3

    .line 2555145
    :cond_2
    return-object p1

    .line 2555146
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->mF_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$OutgoingPingsModel;

    move-result-object v0

    .line 2555147
    if-nez v0, :cond_4

    .line 2555148
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2555149
    goto/16 :goto_0

    .line 2555150
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2555151
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$OutgoingPingsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2555152
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2555153
    :cond_5
    invoke-virtual {p3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$IncomingPingsModel;

    move-result-object v0

    .line 2555154
    if-nez v0, :cond_6

    .line 2555155
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2555156
    goto/16 :goto_2

    .line 2555157
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2555158
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$IncomingPingsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2555159
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final c(LX/IG2;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2555113
    iget-object v0, p1, LX/IG2;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v0, v0

    .line 2555114
    iget-object v1, p0, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c:Landroid/content/res/Resources;

    const v2, 0x7f0f017c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
