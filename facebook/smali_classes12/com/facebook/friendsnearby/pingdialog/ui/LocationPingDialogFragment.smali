.class public Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;
.super Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final n:Lcom/facebook/common/callercontext/CallerContext;

.field public static final o:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public A:Lcom/facebook/user/model/User;

.field public B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Z

.field private D:Landroid/app/Dialog;

.field public E:LX/IHy;

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IGe;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/IGf;

.field private H:Landroid/view/View;

.field private I:Landroid/widget/TextView;

.field public J:Lcom/facebook/widget/listview/BetterListView;

.field public K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

.field public L:Landroid/view/View;

.field public M:Landroid/widget/EditText;

.field private N:Landroid/widget/TextView;

.field public O:Landroid/view/View;

.field private P:Landroid/view/View;

.field private Q:Landroid/widget/TextView;

.field private R:Landroid/widget/Button;

.field private S:Landroid/widget/Button;

.field public p:LX/0SG;

.field public q:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IGd;",
            ">;"
        }
    .end annotation
.end field

.field public r:Landroid/view/inputmethod/InputMethodManager;

.field public s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/0tX;

.field public u:LX/0aG;

.field public v:LX/IGh;

.field public w:LX/0kL;

.field public x:Lcom/facebook/performancelogger/PerformanceLogger;

.field public y:LX/2Oj;

.field public z:LX/IGP;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2556330
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    sput-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m:Ljava/lang/Class;

    .line 2556331
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    const-string v1, "nearby_friends"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 2556332
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xea60

    .line 2556333
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2556334
    move-object v0, v0

    .line 2556335
    const/high16 v1, 0x43480000    # 200.0f

    .line 2556336
    iput v1, v0, LX/1S7;->c:F

    .line 2556337
    move-object v0, v0

    .line 2556338
    const-wide/16 v2, 0x1b58

    .line 2556339
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2556340
    move-object v0, v0

    .line 2556341
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->o:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2556328
    invoke-direct {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;-><init>()V

    .line 2556329
    return-void
.end method

.method public static a(Lcom/facebook/user/model/User;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Z)Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;
    .locals 3
    .param p1    # Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2556320
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2556321
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;-><init>()V

    .line 2556322
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2556323
    const-string v2, "user"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2556324
    const-string v2, "existing_ping"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2556325
    const-string v2, "ping_status_fetch"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2556326
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2556327
    return-object v0
.end method

.method public static a(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2556318
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2556319
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    const/16 v5, 0xc81

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    new-instance v11, LX/IGh;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v9

    check-cast v9, LX/11S;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-direct {v11, v8, v9, v10}, LX/IGh;-><init>(LX/0SG;LX/11S;Landroid/content/res/Resources;)V

    move-object v8, v11

    check-cast v8, LX/IGh;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v10

    check-cast v10, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v11

    check-cast v11, LX/2Oj;

    new-instance v0, LX/IGP;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-direct {v0, p1}, LX/IGP;-><init>(LX/0Zb;)V

    move-object p0, v0

    check-cast p0, LX/IGP;

    iput-object v2, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->p:LX/0SG;

    iput-object v3, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    iput-object v4, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->r:Landroid/view/inputmethod/InputMethodManager;

    iput-object v5, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->s:LX/0Or;

    iput-object v6, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->t:LX/0tX;

    iput-object v7, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->u:LX/0aG;

    iput-object v8, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->v:LX/IGh;

    iput-object v9, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->w:LX/0kL;

    iput-object v10, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v11, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->y:LX/2Oj;

    iput-object p0, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->z:LX/IGP;

    return-void
.end method

.method private a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2556310
    iget-object v2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v2, :cond_2

    .line 2556311
    iget-object v2, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v3, LX/IGG;->STOP:LX/IGG;

    if-ne v2, v3, :cond_1

    .line 2556312
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2556313
    goto :goto_0

    .line 2556314
    :cond_2
    iget-object v2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v2, v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    .line 2556315
    iget-object v3, v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    iget-object v4, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    if-eq v3, v4, :cond_3

    move v0, v1

    .line 2556316
    goto :goto_0

    .line 2556317
    :cond_3
    iget-object v3, v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v4, LX/IGG;->FOREVER:LX/IGG;

    if-eq v3, v4, :cond_0

    iget-object v2, v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    iget-object v3, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v2, v3}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;LX/IGe;)V
    .locals 2

    .prologue
    .line 2556308
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->I:Landroid/widget/TextView;

    iget-object v1, p1, LX/IGe;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2556309
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2556285
    invoke-direct {p0, p1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2556286
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2556287
    :goto_0
    return-void

    .line 2556288
    :cond_0
    iget-object v0, p1, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    sget-object v1, LX/IGG;->STOP:LX/IGG;

    if-ne v0, v1, :cond_1

    .line 2556289
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingDeleteParams;

    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    .line 2556290
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2556291
    invoke-direct {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingDeleteParams;-><init>(Ljava/lang/String;)V

    .line 2556292
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2556293
    const-string v2, "locationPingDeleteParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2556294
    iget-object v2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0x300009

    const-string v4, "FriendsNearbyPingDelete"

    invoke-interface {v2, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2556295
    iget-object v2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    sget-object v3, LX/IGd;->DELETE_PING:LX/IGd;

    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->u:LX/0aG;

    const-string p1, "delete_ping"

    const p2, 0x72d2daab

    invoke-static {v4, p1, v1, p2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    new-instance v4, LX/IGT;

    invoke-direct {v4, p0}, LX/IGT;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2556296
    goto :goto_0

    .line 2556297
    :cond_1
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    .line 2556298
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2556299
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;-><init>(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;LX/0am;)V

    .line 2556300
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v1, :cond_2

    .line 2556301
    const v1, 0x7f083886

    invoke-static {p0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    .line 2556302
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x30000a

    const-string v3, "FriendsNearbyPingWrite"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2556303
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->s:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1sS;

    .line 2556304
    sget-object v2, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->o:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2556305
    iget-object v2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    sget-object v3, LX/IGd;->GET_LOCATION:LX/IGd;

    new-instance v4, LX/IGR;

    invoke-direct {v4, p0, v0}, LX/IGR;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2556306
    goto :goto_0

    .line 2556307
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V
    .locals 2

    .prologue
    .line 2556281
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2556282
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->H:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2556283
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->Q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2556284
    return-void
.end method

.method public static b$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;LX/IGe;)V
    .locals 2

    .prologue
    .line 2556275
    iget-object v0, p1, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-direct {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2556276
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->R:Landroid/widget/Button;

    const v1, 0x7f080016

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 2556277
    :goto_0
    return-void

    .line 2556278
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v0, :cond_1

    .line 2556279
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->R:Landroid/widget/Button;

    const v1, 0x7f083884

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 2556280
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->R:Landroid/widget/Button;

    const v1, 0x7f083885

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V
    .locals 6

    .prologue
    .line 2556268
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2556269
    const-string v0, "locationPingParams"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2556270
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v0, :cond_0

    const v0, 0x7f080025

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    .line 2556271
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x30000a

    const-string v3, "FriendsNearbyPingWrite"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2556272
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    sget-object v2, LX/IGd;->SEND_PING:LX/IGd;

    iget-object v3, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->u:LX/0aG;

    const-string v4, "location_ping"

    const v5, 0x38c7441d

    invoke-static {v3, v4, v1, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    new-instance v3, LX/IGS;

    invoke-direct {v3, p0, p1}, LX/IGS;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2556273
    return-void

    .line 2556274
    :cond_0
    const v0, 0x7f080026

    goto :goto_0
.end method

.method public static c(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V
    .locals 7

    .prologue
    .line 2556342
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->N:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083883

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0076

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2556343
    return-void
.end method

.method public static c(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2556263
    iget-object v3, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2556264
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setVisibility(I)V

    .line 2556265
    return-void

    :cond_0
    move v0, v2

    .line 2556266
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2556267
    goto :goto_1
.end method

.method public static e(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Z)V
    .locals 2

    .prologue
    .line 2556260
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->N:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2556261
    return-void

    .line 2556262
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static k(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2556225
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2556226
    :goto_0
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->v:LX/IGh;

    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->y:LX/2Oj;

    iget-object v5, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    invoke-virtual {v4, v5}, LX/2Oj;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, LX/IGh;->a(LX/0am;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 2556227
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-eqz v0, :cond_2

    .line 2556228
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    move v1, v2

    :goto_1
    if-ge v4, v6, :cond_1

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IGe;

    .line 2556229
    iget-object v0, v0, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-direct {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2556230
    add-int/lit8 v1, v1, 0x1

    .line 2556231
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 2556232
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 2556233
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->d:LX/0am;

    .line 2556234
    iget-object v6, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v6, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2556235
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    :goto_2
    invoke-static {p0, v4}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->c(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    .line 2556236
    move v0, v1

    .line 2556237
    :goto_3
    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IGe;

    .line 2556238
    iput-object v5, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->F:LX/0Px;

    .line 2556239
    new-instance v4, LX/IGf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, LX/IGf;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->G:LX/IGf;

    .line 2556240
    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->G:LX/IGf;

    .line 2556241
    iput-object v5, v4, LX/IGf;->b:LX/0Px;

    .line 2556242
    const v6, 0x717f9a6c

    invoke-static {v4, v6}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2556243
    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    iget-object v6, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->G:LX/IGf;

    invoke-virtual {v4, v6}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2556244
    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    const/4 v6, 0x1

    invoke-virtual {v4, v0, v6}, Lcom/facebook/widget/listview/BetterListView;->setItemChecked(IZ)V

    .line 2556245
    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    invoke-virtual {v4, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setPingOption(LX/IGe;)V

    .line 2556246
    invoke-static {p0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;LX/IGe;)V

    .line 2556247
    invoke-static {p0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;LX/IGe;)V

    .line 2556248
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v0, :cond_3

    move v0, v3

    .line 2556249
    :goto_4
    iget-object v4, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->L:Landroid/view/View;

    if-eqz v0, :cond_6

    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2556250
    invoke-static {p0, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->e(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Z)V

    .line 2556251
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-eqz v0, :cond_4

    .line 2556252
    :goto_6
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->O:Landroid/view/View;

    if-eqz v3, :cond_7

    const/4 v0, 0x0

    :goto_7
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2556253
    return-void

    .line 2556254
    :cond_2
    invoke-static {p0, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->c(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v2

    .line 2556255
    goto :goto_4

    :cond_4
    move v3, v2

    .line 2556256
    goto :goto_6

    .line 2556257
    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    .line 2556258
    :cond_6
    const/16 v1, 0x8

    goto :goto_5

    .line 2556259
    :cond_7
    const/16 v0, 0x8

    goto :goto_7
.end method

.method public static m$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 2

    .prologue
    .line 2556222
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->P:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2556223
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2556224
    return-void
.end method

.method public static p(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 3

    .prologue
    .line 2556220
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->r:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2556221
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 2556217
    new-instance v0, LX/IGU;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/IGU;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->D:Landroid/app/Dialog;

    .line 2556218
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->D:Landroid/app/Dialog;

    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 2556219
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->D:Landroid/app/Dialog;

    return-object v0
.end method

.method public final a(LX/IHy;)V
    .locals 0

    .prologue
    .line 2556215
    iput-object p1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    .line 2556216
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2556212
    invoke-static {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->p(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    .line 2556213
    invoke-super {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;->d()V

    .line 2556214
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2de1e9f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2556204
    invoke-super {p0, p1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;->onCreate(Landroid/os/Bundle;)V

    .line 2556205
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2556206
    const-string v0, "user"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    .line 2556207
    const-string v0, "existing_ping"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2556208
    const-string v0, "ping_status_fetch"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->C:Z

    .line 2556209
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2556210
    const v0, 0x7f0e0249

    invoke-virtual {p0, v3, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2556211
    const/16 v0, 0x2b

    const v2, 0x2ce7bf56

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x39852073

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2556171
    const v0, 0x7f030a46

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2556172
    const v0, 0x7f0d19e2

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->H:Landroid/view/View;

    .line 2556173
    const v0, 0x7f0d19e3

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->I:Landroid/widget/TextView;

    .line 2556174
    const v0, 0x7f0d19e4

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    .line 2556175
    const v0, 0x7f0d19e5

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    .line 2556176
    const v0, 0x7f0d19e6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->L:Landroid/view/View;

    .line 2556177
    const v0, 0x7f0d19e7

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    .line 2556178
    const v0, 0x7f0d19e8

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->N:Landroid/widget/TextView;

    .line 2556179
    const v0, 0x7f0d19e9

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->O:Landroid/view/View;

    .line 2556180
    const v0, 0x7f0d19ec

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->P:Landroid/view/View;

    .line 2556181
    const v0, 0x7f0d19ed

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->Q:Landroid/widget/TextView;

    .line 2556182
    const v0, 0x7f0d19eb

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->R:Landroid/widget/Button;

    .line 2556183
    const v0, 0x7f0d19ea

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->S:Landroid/widget/Button;

    .line 2556184
    invoke-static {p0, v4}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->c(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    .line 2556185
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    new-instance v3, LX/IGV;

    invoke-direct {v3, p0}, LX/IGV;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2556186
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    new-instance v3, LX/IGW;

    invoke-direct {v3, p0}, LX/IGW;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2556187
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 2556188
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    new-instance v3, LX/IGX;

    invoke-direct {v3, p0}, LX/IGX;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2556189
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    new-instance v3, LX/IGY;

    invoke-direct {v3, p0}, LX/IGY;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2556190
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->K:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    invoke-virtual {v0, v4}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setChecked(Z)V

    .line 2556191
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->O:Landroid/view/View;

    new-instance v3, LX/IGZ;

    invoke-direct {v3, p0}, LX/IGZ;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2556192
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->R:Landroid/widget/Button;

    new-instance v3, LX/IGa;

    invoke-direct {v3, p0}, LX/IGa;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2556193
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->S:Landroid/widget/Button;

    new-instance v3, LX/IGb;

    invoke-direct {v3, p0}, LX/IGb;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2556194
    iget-boolean v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->C:Z

    if-eqz v0, :cond_0

    .line 2556195
    const v0, 0x7f080024

    invoke-static {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;I)V

    .line 2556196
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0x30000b

    const-string v4, "FriendsNearbyPingFetchExist"

    invoke-interface {v0, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2556197
    iget-object v3, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    sget-object v4, LX/IGd;->OBTAIN_EXISTING_PING:LX/IGd;

    iget-object v5, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->t:LX/0tX;

    .line 2556198
    new-instance v0, LX/IGH;

    invoke-direct {v0}, LX/IGH;-><init>()V

    move-object v0, v0

    .line 2556199
    const-string p1, "uid"

    iget-object p2, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    .line 2556200
    iget-object p3, p2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p2, p3

    .line 2556201
    invoke-virtual {v0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/IGH;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v5, LX/IGc;

    invoke-direct {v5, p0}, LX/IGc;-><init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    invoke-virtual {v3, v4, v0, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2556202
    :goto_0
    const v0, 0xd92d7d8

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2556203
    :cond_0
    invoke-static {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->k(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x33150d27

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2556165
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->q:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2556166
    const v1, 0x30000b

    const-string v2, "FriendsNearbyPingFetchExist"

    invoke-static {p0, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;ILjava/lang/String;)V

    .line 2556167
    const v1, 0x30000a

    const-string v2, "FriendsNearbyPingWrite"

    invoke-static {p0, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;ILjava/lang/String;)V

    .line 2556168
    const v1, 0x300009

    const-string v2, "FriendsNearbyPingDelete"

    invoke-static {p0, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;ILjava/lang/String;)V

    .line 2556169
    invoke-super {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialog;->onDestroy()V

    .line 2556170
    const/16 v1, 0x2b

    const v2, -0x2a0048a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
