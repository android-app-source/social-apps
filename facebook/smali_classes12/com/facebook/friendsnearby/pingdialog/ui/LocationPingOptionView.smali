.class public Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/ImageView;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2556368
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2556369
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2556366
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2556367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2556359
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2556360
    const v0, 0x7f030a45

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2556361
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2556362
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setPadding(IIII)V

    .line 2556363
    const v0, 0x7f0d19e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->a:Landroid/widget/TextView;

    .line 2556364
    const v0, 0x7f0d19e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->b:Landroid/widget/ImageView;

    .line 2556365
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2556370
    iget-boolean v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->c:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 2556350
    iput-boolean p1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->c:Z

    .line 2556351
    iget-object v1, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->b:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2556352
    return-void

    .line 2556353
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setPingOption(LX/IGe;)V
    .locals 2

    .prologue
    .line 2556357
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->a:Landroid/widget/TextView;

    iget-object v1, p1, LX/IGe;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2556358
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2556354
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setChecked(Z)V

    .line 2556355
    return-void

    .line 2556356
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
