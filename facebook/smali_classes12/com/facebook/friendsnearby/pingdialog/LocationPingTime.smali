.class public Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/IGG;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2555454
    new-instance v0, LX/IGE;

    invoke-direct {v0}, LX/IGE;-><init>()V

    sput-object v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IGG;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IGG;",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2555450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555451
    iput-object p1, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    .line 2555452
    iput-object p2, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    .line 2555453
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2555444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/IGG;->valueOf(Ljava/lang/String;)LX/IGG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    .line 2555446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2555447
    if-nez v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    .line 2555448
    return-void

    .line 2555449
    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;
    .locals 4

    .prologue
    .line 2555433
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    sget-object v1, LX/IGG;->DURATION:LX/IGG;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;-><init>(LX/IGG;LX/0am;)V

    return-object v0
.end method

.method public static a(LX/15i;IJ)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2555455
    sget-object v1, LX/IGF;->a:[I

    const/4 v0, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2555456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ping type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2555457
    :pswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->j(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 2555458
    add-long/2addr v0, p2

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    .line 2555459
    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b()Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLLocationPingType;JJ)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;
    .locals 3

    .prologue
    .line 2555440
    sget-object v0, LX/IGF;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2555441
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ping type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2555442
    :pswitch_0
    add-long v0, p3, p1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(J)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    .line 2555443
    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b()Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b()Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;
    .locals 3

    .prologue
    .line 2555439
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    sget-object v1, LX/IGG;->FOREVER:LX/IGG;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;-><init>(LX/IGG;LX/0am;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2555438
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2555434
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a:LX/IGG;

    invoke-virtual {v0}, LX/IGG;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2555435
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2555436
    return-void

    .line 2555437
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
