.class public Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2555399
    new-instance v0, LX/IGD;

    invoke-direct {v0}, LX/IGD;-><init>()V

    sput-object v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2555400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555401
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a:Ljava/lang/String;

    .line 2555402
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    .line 2555403
    const-class v0, Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 2555404
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->c:LX/0am;

    .line 2555405
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->d:LX/0am;

    .line 2555406
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;",
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2555407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555408
    iput-object p1, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a:Ljava/lang/String;

    .line 2555409
    iput-object p2, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    .line 2555410
    iput-object p3, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->c:LX/0am;

    .line 2555411
    iput-object p4, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->d:LX/0am;

    .line 2555412
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLocationPingType;JLjava/lang/String;J)Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;
    .locals 4

    .prologue
    .line 2555413
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2555414
    :goto_0
    new-instance v1, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    invoke-static {p1, p2, p3, p5, p6}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(Lcom/facebook/graphql/enums/GraphQLLocationPingType;JJ)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-direct {v1, p4, v2, v3, v0}, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;-><init>(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;LX/0am;)V

    return-object v1

    .line 2555415
    :cond_0
    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2555416
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2555417
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2555418
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2555419
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2555420
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2555421
    return-void
.end method
