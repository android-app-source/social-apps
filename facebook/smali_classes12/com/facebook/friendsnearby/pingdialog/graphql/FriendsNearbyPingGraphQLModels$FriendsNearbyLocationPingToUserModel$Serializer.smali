.class public final Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2555808
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;

    new-instance v1, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2555809
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2555810
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2555811
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2555812
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2555813
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2555814
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2555815
    if-eqz v2, :cond_1

    .line 2555816
    const-string p0, "location_sharing"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555817
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2555818
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2555819
    if-eqz p0, :cond_0

    .line 2555820
    const-string v0, "locationPingToUser"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555821
    invoke-static {v1, p0, p1, p2}, LX/IGL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2555822
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2555823
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2555824
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2555825
    check-cast p1, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$Serializer;->a(Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;LX/0nX;LX/0my;)V

    return-void
.end method
