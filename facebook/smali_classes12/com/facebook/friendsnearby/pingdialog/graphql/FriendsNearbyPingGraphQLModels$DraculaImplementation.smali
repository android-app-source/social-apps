.class public final Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2555612
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2555613
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2555610
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2555611
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, 0x3

    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 2555551
    if-nez p1, :cond_0

    move v0, v6

    .line 2555552
    :goto_0
    return v0

    .line 2555553
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2555554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2555555
    :sswitch_0
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 2555556
    const v1, -0x7837ca96

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2555557
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2555558
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2555559
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2555560
    :sswitch_1
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 2555561
    const v1, 0x315ee330

    const/4 v3, 0x0

    .line 2555562
    if-nez v0, :cond_1

    move v2, v3

    .line 2555563
    :goto_1
    move v0, v2

    .line 2555564
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2555565
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2555566
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2555567
    :sswitch_2
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    .line 2555568
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2555569
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2555570
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2555571
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2555572
    :sswitch_3
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v0

    .line 2555573
    const v2, 0x65e67650

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 2555574
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2555575
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2555576
    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2555577
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2555578
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2555579
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2555580
    invoke-virtual {p3, v6, v4}, LX/186;->b(II)V

    .line 2555581
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 2555582
    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2555583
    invoke-virtual {p3, v9, v7}, LX/186;->b(II)V

    .line 2555584
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2555585
    :sswitch_4
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2555586
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2555587
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2555588
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2555589
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    move-object v0, p3

    move v1, v8

    .line 2555590
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2555591
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2555592
    :sswitch_5
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2555593
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2555594
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    move-result-object v2

    .line 2555595
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2555596
    invoke-virtual {p0, p1, v1, v6}, LX/15i;->a(III)I

    move-result v3

    .line 2555597
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2555598
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2555599
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2555600
    invoke-virtual {p3, v1, v3, v6}, LX/186;->a(III)V

    .line 2555601
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2555602
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 2555603
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 2555604
    :goto_2
    if-ge v3, v4, :cond_3

    .line 2555605
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 2555606
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 2555607
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2555608
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 2555609
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7837ca96 -> :sswitch_1
        -0x54711c38 -> :sswitch_2
        0x22f7f983 -> :sswitch_5
        0x315ee330 -> :sswitch_3
        0x456c53b7 -> :sswitch_0
        0x65e67650 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2555542
    if-nez p0, :cond_0

    move v0, v1

    .line 2555543
    :goto_0
    return v0

    .line 2555544
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2555545
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2555546
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2555547
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2555548
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2555549
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2555550
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2555535
    const/4 v7, 0x0

    .line 2555536
    const/4 v1, 0x0

    .line 2555537
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2555538
    invoke-static {v2, v3, v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2555539
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2555540
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2555541
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2555534
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2555529
    if-eqz p0, :cond_0

    .line 2555530
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2555531
    if-eq v0, p0, :cond_0

    .line 2555532
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2555533
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2555509
    sparse-switch p2, :sswitch_data_0

    .line 2555510
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2555511
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2555512
    const v1, -0x7837ca96

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2555513
    :goto_0
    :sswitch_1
    return-void

    .line 2555514
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2555515
    const v1, 0x315ee330

    .line 2555516
    if-eqz v0, :cond_0

    .line 2555517
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2555518
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2555519
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2555520
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2555521
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2555522
    :cond_0
    goto :goto_0

    .line 2555523
    :sswitch_3
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    .line 2555524
    invoke-static {v0, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 2555525
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2555526
    const v1, 0x65e67650

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2555527
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 2555528
    invoke-static {v0, p3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7837ca96 -> :sswitch_2
        -0x54711c38 -> :sswitch_3
        0x22f7f983 -> :sswitch_1
        0x315ee330 -> :sswitch_4
        0x456c53b7 -> :sswitch_0
        0x65e67650 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2555503
    if-eqz p1, :cond_0

    .line 2555504
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2555505
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    .line 2555506
    if-eq v0, v1, :cond_0

    .line 2555507
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2555508
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2555614
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2555479
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2555480
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2555481
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2555482
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2555483
    :cond_0
    iput-object p1, p0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2555484
    iput p2, p0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->b:I

    .line 2555485
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2555486
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2555487
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2555488
    iget v0, p0, LX/1vt;->c:I

    .line 2555489
    move v0, v0

    .line 2555490
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2555491
    iget v0, p0, LX/1vt;->c:I

    .line 2555492
    move v0, v0

    .line 2555493
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2555494
    iget v0, p0, LX/1vt;->b:I

    .line 2555495
    move v0, v0

    .line 2555496
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2555497
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2555498
    move-object v0, v0

    .line 2555499
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2555470
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2555471
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2555472
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2555473
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2555474
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2555475
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2555476
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2555477
    invoke-static {v3, v9, v2}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2555478
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2555500
    iget v0, p0, LX/1vt;->c:I

    .line 2555501
    move v0, v0

    .line 2555502
    return v0
.end method
