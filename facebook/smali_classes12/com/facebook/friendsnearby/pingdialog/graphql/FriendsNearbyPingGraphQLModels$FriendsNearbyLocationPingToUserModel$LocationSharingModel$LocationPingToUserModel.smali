.class public final Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29b6dfc0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2555807
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2555806
    const-class v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2555804
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2555805
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2555798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2555799
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 2555800
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2555801
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2555802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2555803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2555790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2555791
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2555792
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2555793
    if-eqz v1, :cond_0

    .line 2555794
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    .line 2555795
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->e:LX/3Sb;

    .line 2555796
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2555797
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2555783
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x22f7f983

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->e:LX/3Sb;

    .line 2555784
    iget-object v0, p0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2555787
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-direct {v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;-><init>()V

    .line 2555788
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2555789
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2555786
    const v0, 0x7623b78a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2555785
    const v0, -0x2df4f1f5

    return v0
.end method
