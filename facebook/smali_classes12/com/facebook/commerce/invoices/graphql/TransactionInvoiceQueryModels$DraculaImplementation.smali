.class public final Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2506173
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2506174
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2506171
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2506172
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2506064
    if-nez p1, :cond_0

    move v0, v1

    .line 2506065
    :goto_0
    return v0

    .line 2506066
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2506067
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2506068
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2506069
    const v2, -0x390684db

    const/4 v4, 0x0

    .line 2506070
    if-nez v0, :cond_1

    move v3, v4

    .line 2506071
    :goto_1
    move v0, v3

    .line 2506072
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2506073
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2506074
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2506075
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506076
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2506077
    const-class v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionItemProductFieldsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionItemProductFieldsModel$EdgesModel$NodeModel;

    .line 2506078
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2506079
    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v3

    .line 2506080
    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v4

    .line 2506081
    const v5, -0x660fd2d4

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 2506082
    invoke-virtual {p0, p1, v10, v1}, LX/15i;->a(III)I

    move-result v5

    .line 2506083
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2506084
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 2506085
    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 2506086
    invoke-virtual {p3, v8, v3, v1}, LX/186;->a(III)V

    .line 2506087
    invoke-virtual {p3, v9, v4}, LX/186;->b(II)V

    .line 2506088
    invoke-virtual {p3, v10, v5, v1}, LX/186;->a(III)V

    .line 2506089
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2506090
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2506091
    const v2, -0x7b19207

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2506092
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2506093
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2506094
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2506095
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506096
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2506097
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2506098
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2506099
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2506100
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506101
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2506102
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v2

    .line 2506103
    const v3, 0x41d72549

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2506104
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2506105
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2506106
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2506107
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2506108
    :sswitch_5
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2506109
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2506110
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2506111
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2506112
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2506113
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2506114
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2506115
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 2506116
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2506117
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2506118
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2506119
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 2506120
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2506121
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2506122
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x660fd2d4 -> :sswitch_2
        -0x46bb3fae -> :sswitch_4
        -0x390684db -> :sswitch_1
        -0x7b19207 -> :sswitch_3
        0x41d72549 -> :sswitch_5
        0x69d2fa18 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2506170
    new-instance v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2506165
    if-eqz p0, :cond_0

    .line 2506166
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2506167
    if-eq v0, p0, :cond_0

    .line 2506168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2506169
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2506146
    sparse-switch p2, :sswitch_data_0

    .line 2506147
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2506148
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2506149
    const v1, -0x390684db

    .line 2506150
    if-eqz v0, :cond_0

    .line 2506151
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2506152
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2506153
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2506154
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2506155
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2506156
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2506157
    :sswitch_2
    const-class v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionItemProductFieldsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionItemProductFieldsModel$EdgesModel$NodeModel;

    .line 2506158
    invoke-static {v0, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2506159
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2506160
    const v1, -0x660fd2d4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 2506161
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2506162
    const v1, -0x7b19207

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 2506163
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2506164
    const v1, 0x41d72549

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x660fd2d4 -> :sswitch_3
        -0x46bb3fae -> :sswitch_4
        -0x390684db -> :sswitch_2
        -0x7b19207 -> :sswitch_1
        0x41d72549 -> :sswitch_1
        0x69d2fa18 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2506140
    if-eqz p1, :cond_0

    .line 2506141
    invoke-static {p0, p1, p2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2506142
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;

    .line 2506143
    if-eq v0, v1, :cond_0

    .line 2506144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2506145
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2506139
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2506137
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2506138
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2506175
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2506176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2506177
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2506178
    iput p2, p0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->b:I

    .line 2506179
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2506136
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2506135
    new-instance v0, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2506132
    iget v0, p0, LX/1vt;->c:I

    .line 2506133
    move v0, v0

    .line 2506134
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2506129
    iget v0, p0, LX/1vt;->c:I

    .line 2506130
    move v0, v0

    .line 2506131
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2506126
    iget v0, p0, LX/1vt;->b:I

    .line 2506127
    move v0, v0

    .line 2506128
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2506123
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2506124
    move-object v0, v0

    .line 2506125
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2506055
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2506056
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2506057
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2506058
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2506059
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2506060
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2506061
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2506062
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2506063
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2506052
    iget v0, p0, LX/1vt;->c:I

    .line 2506053
    move v0, v0

    .line 2506054
    return v0
.end method
