.class public Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;
.super LX/0te;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/6C4;

.field public B:LX/6D0;

.field public C:LX/HnQ;

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Be;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/Hni;

.field public F:LX/2nD;

.field public G:LX/17Y;

.field public b:LX/D3V;

.field public c:LX/0gh;

.field public d:LX/1rD;

.field public e:LX/168;

.field public f:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;

.field public g:LX/0Uo;

.field public h:Lcom/facebook/content/SecureContextHelper;

.field public i:LX/BWU;

.field public j:LX/2nC;

.field public k:LX/6G2;

.field public l:LX/6C5;

.field public m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1Kf;

.field public o:LX/1Bn;

.field public p:LX/H6T;

.field public q:LX/H5w;

.field public r:LX/HnT;

.field public s:LX/0Uh;

.field public t:LX/15W;

.field public u:LX/CK7;

.field public v:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public w:LX/0Sh;

.field private x:LX/0Zb;

.field public y:LX/0ad;

.field private z:LX/1Bf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2502380
    const-class v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2502343
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2502344
    return-void
.end method

.method public static a(LX/6Cx;)Ljava/lang/String;
    .locals 1
    .param p0    # LX/6Cx;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2502379
    sget-object v0, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    if-ne p0, v0, :cond_0

    const-string v0, "ix_webview"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "webview"

    goto :goto_0
.end method

.method private a(LX/D3V;LX/0gh;LX/1rD;LX/168;LX/0Uo;Lcom/facebook/content/SecureContextHelper;LX/BWU;LX/2nC;LX/6C5;LX/6G2;LX/0Or;LX/1Kf;LX/1Bn;LX/H6T;LX/H5w;LX/HnT;LX/0Uh;LX/15W;LX/CK7;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0Zb;LX/0ad;LX/1Bf;LX/6C4;LX/6D0;LX/HnQ;LX/0Ot;LX/Hni;LX/2nD;LX/17Y;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D3V;",
            "LX/0gh;",
            "LX/1rD;",
            "LX/168;",
            "LX/0Uo;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/BWU;",
            "LX/2nC;",
            "LX/6C5;",
            "LX/6G2;",
            "LX/0Or",
            "<",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            ">;",
            "LX/1Kf;",
            "LX/1Bn;",
            "LX/H6T;",
            "LX/H5w;",
            "LX/HnT;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/15W;",
            "LX/CK7;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Zb;",
            "LX/0ad;",
            "LX/1Bf;",
            "LX/6C4;",
            "LX/6D0;",
            "LX/HnQ;",
            "LX/0Ot",
            "<",
            "LX/1Be;",
            ">;",
            "LX/Hni;",
            "LX/2nD;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2502346
    iput-object p1, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    .line 2502347
    iput-object p2, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->c:LX/0gh;

    .line 2502348
    iput-object p3, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->d:LX/1rD;

    .line 2502349
    iput-object p4, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->e:LX/168;

    .line 2502350
    iput-object p5, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->g:LX/0Uo;

    .line 2502351
    iput-object p6, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2502352
    new-instance v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;

    invoke-direct {v1}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;-><init>()V

    iput-object v1, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->f:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;

    .line 2502353
    iput-object p7, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->i:LX/BWU;

    .line 2502354
    iput-object p8, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->j:LX/2nC;

    .line 2502355
    iput-object p9, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->l:LX/6C5;

    .line 2502356
    iput-object p10, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->k:LX/6G2;

    .line 2502357
    iput-object p11, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->m:LX/0Or;

    .line 2502358
    iput-object p12, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->n:LX/1Kf;

    .line 2502359
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->o:LX/1Bn;

    .line 2502360
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->p:LX/H6T;

    .line 2502361
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->q:LX/H5w;

    .line 2502362
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->r:LX/HnT;

    .line 2502363
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->s:LX/0Uh;

    .line 2502364
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->t:LX/15W;

    .line 2502365
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->u:LX/CK7;

    .line 2502366
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2502367
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->w:LX/0Sh;

    .line 2502368
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->x:LX/0Zb;

    .line 2502369
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->y:LX/0ad;

    .line 2502370
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->z:LX/1Bf;

    .line 2502371
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502372
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->B:LX/6D0;

    .line 2502373
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->C:LX/HnQ;

    .line 2502374
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->D:LX/0Ot;

    .line 2502375
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502376
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->F:LX/2nD;

    .line 2502377
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->G:LX/17Y;

    .line 2502378
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 34

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v33

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-static/range {v33 .. v33}, LX/D3V;->a(LX/0QB;)LX/D3V;

    move-result-object v3

    check-cast v3, LX/D3V;

    invoke-static/range {v33 .. v33}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v4

    check-cast v4, LX/0gh;

    invoke-static/range {v33 .. v33}, LX/1rD;->a(LX/0QB;)LX/1rD;

    move-result-object v5

    check-cast v5, LX/1rD;

    invoke-static/range {v33 .. v33}, LX/168;->a(LX/0QB;)LX/168;

    move-result-object v6

    check-cast v6, LX/168;

    invoke-static/range {v33 .. v33}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    invoke-static/range {v33 .. v33}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v33 .. v33}, LX/BWU;->a(LX/0QB;)LX/BWU;

    move-result-object v9

    check-cast v9, LX/BWU;

    invoke-static/range {v33 .. v33}, LX/2nC;->a(LX/0QB;)LX/2nC;

    move-result-object v10

    check-cast v10, LX/2nC;

    invoke-static/range {v33 .. v33}, LX/6C5;->a(LX/0QB;)LX/6C5;

    move-result-object v11

    check-cast v11, LX/6C5;

    invoke-static/range {v33 .. v33}, LX/6G2;->a(LX/0QB;)LX/6G2;

    move-result-object v12

    check-cast v12, LX/6G2;

    const/16 v13, 0x34e8

    move-object/from16 v0, v33

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {v33 .. v33}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v14

    check-cast v14, LX/1Kf;

    invoke-static/range {v33 .. v33}, LX/1Bn;->a(LX/0QB;)LX/1Bn;

    move-result-object v15

    check-cast v15, LX/1Bn;

    invoke-static/range {v33 .. v33}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v16

    check-cast v16, LX/H6T;

    invoke-static/range {v33 .. v33}, LX/H5w;->a(LX/0QB;)LX/H5w;

    move-result-object v17

    check-cast v17, LX/H5w;

    invoke-static/range {v33 .. v33}, LX/HnT;->a(LX/0QB;)LX/HnT;

    move-result-object v18

    check-cast v18, LX/HnT;

    invoke-static/range {v33 .. v33}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v19

    check-cast v19, LX/0Uh;

    invoke-static/range {v33 .. v33}, LX/15W;->a(LX/0QB;)LX/15W;

    move-result-object v20

    check-cast v20, LX/15W;

    invoke-static/range {v33 .. v33}, LX/CK7;->a(LX/0QB;)LX/CK7;

    move-result-object v21

    check-cast v21, LX/CK7;

    invoke-static/range {v33 .. v33}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v22

    check-cast v22, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v33 .. v33}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v23

    check-cast v23, LX/0Sh;

    invoke-static/range {v33 .. v33}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v24

    check-cast v24, LX/0Zb;

    invoke-static/range {v33 .. v33}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {v33 .. v33}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v26

    check-cast v26, LX/1Bf;

    invoke-static/range {v33 .. v33}, LX/6C4;->a(LX/0QB;)LX/6C4;

    move-result-object v27

    check-cast v27, LX/6C4;

    const-class v28, LX/6D0;

    move-object/from16 v0, v33

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/6D0;

    invoke-static/range {v33 .. v33}, LX/HnQ;->a(LX/0QB;)LX/HnQ;

    move-result-object v29

    check-cast v29, LX/HnQ;

    const/16 v30, 0x1f4

    move-object/from16 v0, v33

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    invoke-static/range {v33 .. v33}, LX/Hni;->a(LX/0QB;)LX/Hni;

    move-result-object v31

    check-cast v31, LX/Hni;

    invoke-static/range {v33 .. v33}, LX/2nD;->a(LX/0QB;)LX/2nD;

    move-result-object v32

    check-cast v32, LX/2nD;

    invoke-static/range {v33 .. v33}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v33

    check-cast v33, LX/17Y;

    invoke-direct/range {v2 .. v33}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->a(LX/D3V;LX/0gh;LX/1rD;LX/168;LX/0Uo;Lcom/facebook/content/SecureContextHelper;LX/BWU;LX/2nC;LX/6C5;LX/6G2;LX/0Or;LX/1Kf;LX/1Bn;LX/H6T;LX/H5w;LX/HnT;LX/0Uh;LX/15W;LX/CK7;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0Zb;LX/0ad;LX/1Bf;LX/6C4;LX/6D0;LX/HnQ;LX/0Ot;LX/Hni;LX/2nD;LX/17Y;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 2502345
    new-instance v0, LX/HnR;

    invoke-direct {v0, p0}, LX/HnR;-><init>(Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;)V

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x405c9ca9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2502339
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2502340
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 2502341
    invoke-static {p0, p0}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2502342
    const/16 v1, 0x25

    const v2, 0x7335d601

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
