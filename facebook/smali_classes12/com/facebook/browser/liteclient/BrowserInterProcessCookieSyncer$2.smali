.class public final Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Landroid/webkit/CookieManager;

.field public final synthetic c:LX/HnQ;


# direct methods
.method public constructor <init>(LX/HnQ;Ljava/util/List;Landroid/webkit/CookieManager;)V
    .locals 0

    .prologue
    .line 2501817
    iput-object p1, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->c:LX/HnQ;

    iput-object p2, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->b:Landroid/webkit/CookieManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2501818
    iget-object v0, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EC;

    .line 2501819
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 2501820
    invoke-virtual {v0, v1}, LX/0EC;->a(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2501821
    invoke-virtual {v0}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v3

    .line 2501822
    invoke-virtual {v0}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v4

    .line 2501823
    if-eqz v3, :cond_0

    .line 2501824
    iget-object v5, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->b:Landroid/webkit/CookieManager;

    new-instance v6, LX/HnP;

    iget-object v7, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->b:Landroid/webkit/CookieManager;

    iget-object v1, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->c:LX/HnQ;

    iget-object v1, v1, LX/HnQ;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v8, LX/03R;->YES:LX/03R;

    if-ne v1, v8, :cond_1

    const/4 v1, 0x1

    :goto_1
    iget-object v8, p0, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;->c:LX/HnQ;

    iget-object v8, v8, LX/HnQ;->d:LX/0Zb;

    invoke-direct {v6, v0, v7, v1, v8}, LX/HnP;-><init>(LX/0EC;Landroid/webkit/CookieManager;ZLX/0Zb;)V

    invoke-virtual {v5, v4, v3, v6}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2501825
    :cond_2
    return-void
.end method
