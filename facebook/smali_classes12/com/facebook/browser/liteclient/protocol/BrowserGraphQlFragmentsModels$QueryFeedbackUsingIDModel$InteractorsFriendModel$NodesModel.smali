.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d5d8189
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2502543
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2502581
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2502579
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2502580
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502576
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2502577
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2502578
    :cond_0
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502574
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2502575
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2502566
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502567
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2502568
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x58219f04

    invoke-static {v2, v1, v3}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2502569
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2502570
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2502571
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2502572
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502573
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2502556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502557
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2502558
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x58219f04

    invoke-static {v2, v0, v3}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2502559
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2502560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;

    .line 2502561
    iput v3, v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->f:I

    .line 2502562
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502563
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2502564
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2502565
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2502555
    new-instance v0, LX/HnU;

    invoke-direct {v0, p1}, LX/HnU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2502552
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2502553
    const/4 v0, 0x1

    const v1, 0x58219f04

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;->f:I

    .line 2502554
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2502550
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2502551
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2502549
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2502546
    new-instance v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel$NodesModel;-><init>()V

    .line 2502547
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2502548
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2502545
    const v0, 0x33c56f05

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2502544
    const v0, 0x3c2b9d5

    return v0
.end method
