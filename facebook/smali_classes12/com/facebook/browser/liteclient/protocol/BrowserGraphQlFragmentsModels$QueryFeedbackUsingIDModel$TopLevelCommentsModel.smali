.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2502870
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2502866
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2502864
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2502865
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2502862
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2502863
    iget v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2502857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502858
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2502859
    iget v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2502860
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502861
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2502867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502869
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2502843
    iput p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->e:I

    .line 2502844
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2502845
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2502846
    if-eqz v0, :cond_0

    .line 2502847
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 2502848
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2502854
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2502855
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->e:I

    .line 2502856
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2502851
    new-instance v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    invoke-direct {v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;-><init>()V

    .line 2502852
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2502853
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2502850
    const v0, -0x4bbdd24a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2502849
    const v0, 0x35a359a1

    return v0
.end method
