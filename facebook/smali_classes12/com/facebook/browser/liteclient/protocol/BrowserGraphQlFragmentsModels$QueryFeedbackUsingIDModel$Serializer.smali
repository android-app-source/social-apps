.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2502774
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    new-instance v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2502775
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2502776
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2502777
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2502778
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2502779
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2502780
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2502781
    if-eqz v2, :cond_0

    .line 2502782
    const-string p0, "can_see_voice_switcher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502783
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2502784
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2502785
    if-eqz v2, :cond_1

    .line 2502786
    const-string p0, "can_viewer_comment"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502787
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2502788
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2502789
    if-eqz v2, :cond_2

    .line 2502790
    const-string p0, "can_viewer_like"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502791
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2502792
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2502793
    if-eqz v2, :cond_3

    .line 2502794
    const-string p0, "does_viewer_like"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502795
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2502796
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2502797
    if-eqz v2, :cond_4

    .line 2502798
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502799
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2502800
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2502801
    if-eqz v2, :cond_5

    .line 2502802
    const-string p0, "interactors_friend"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502803
    invoke-static {v1, v2, p1, p2}, LX/Hna;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2502804
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2502805
    if-eqz v2, :cond_6

    .line 2502806
    const-string p0, "interactors_not_friend"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502807
    invoke-static {v1, v2, p1, p2}, LX/Hnc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2502808
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2502809
    if-eqz v2, :cond_7

    .line 2502810
    const-string p0, "legacy_api_post_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502811
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2502812
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2502813
    if-eqz v2, :cond_8

    .line 2502814
    const-string p0, "likers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502815
    invoke-static {v1, v2, p1}, LX/Hnd;->a(LX/15i;ILX/0nX;)V

    .line 2502816
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2502817
    if-eqz v2, :cond_9

    .line 2502818
    const-string p0, "top_level_comments"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2502819
    invoke-static {v1, v2, p1}, LX/Hne;->a(LX/15i;ILX/0nX;)V

    .line 2502820
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2502821
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2502822
    check-cast p1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;->a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;LX/0nX;LX/0my;)V

    return-void
.end method
