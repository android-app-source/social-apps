.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2503102
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;

    new-instance v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2503103
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2503104
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2503105
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2503106
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2503107
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2503108
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2503109
    if-eqz v2, :cond_0

    .line 2503110
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503111
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2503112
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2503113
    if-eqz v2, :cond_2

    .line 2503114
    const-string p0, "all_share_stories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503115
    const/4 p0, 0x0

    .line 2503116
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2503117
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 2503118
    if-eqz p0, :cond_1

    .line 2503119
    const-string p2, "count"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503120
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 2503121
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2503122
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2503123
    if-eqz v2, :cond_3

    .line 2503124
    const-string p0, "external_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503125
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2503126
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2503127
    if-eqz v2, :cond_4

    .line 2503128
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503129
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2503130
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2503131
    if-eqz v2, :cond_6

    .line 2503132
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503133
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2503134
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2503135
    if-eqz p0, :cond_5

    .line 2503136
    const-string v0, "text"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2503137
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2503138
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2503139
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2503140
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2503141
    check-cast p1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Serializer;->a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;LX/0nX;LX/0my;)V

    return-void
.end method
