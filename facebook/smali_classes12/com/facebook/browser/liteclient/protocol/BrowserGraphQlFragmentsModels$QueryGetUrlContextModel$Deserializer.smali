.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2503098
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;

    new-instance v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2503099
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2503033
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2503034
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2503035
    const/4 v2, 0x0

    .line 2503036
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 2503037
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2503038
    :goto_0
    move v1, v2

    .line 2503039
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2503040
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2503041
    new-instance v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;

    invoke-direct {v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryGetUrlContextModel;-><init>()V

    .line 2503042
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2503043
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2503044
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2503045
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2503046
    :cond_0
    return-object v1

    .line 2503047
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2503048
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 2503049
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2503050
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2503051
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_2

    if-eqz v7, :cond_2

    .line 2503052
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2503053
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 2503054
    :cond_4
    const-string v8, "all_share_stories"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2503055
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2503056
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_e

    .line 2503057
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2503058
    :goto_2
    move v5, v7

    .line 2503059
    goto :goto_1

    .line 2503060
    :cond_5
    const-string v8, "external_url"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2503061
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2503062
    :cond_6
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2503063
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2503064
    :cond_7
    const-string v8, "title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2503065
    const/4 v7, 0x0

    .line 2503066
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v8, :cond_12

    .line 2503067
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2503068
    :goto_3
    move v1, v7

    .line 2503069
    goto :goto_1

    .line 2503070
    :cond_8
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2503071
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2503072
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2503073
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2503074
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2503075
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2503076
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 2503077
    :cond_a
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_c

    .line 2503078
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2503079
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2503080
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v10, :cond_a

    .line 2503081
    const-string p0, "count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2503082
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v8

    goto :goto_4

    .line 2503083
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2503084
    :cond_c
    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2503085
    if-eqz v5, :cond_d

    .line 2503086
    invoke-virtual {v0, v7, v9, v7}, LX/186;->a(III)V

    .line 2503087
    :cond_d
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_e
    move v5, v7

    move v9, v7

    goto :goto_4

    .line 2503088
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2503089
    :cond_10
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_11

    .line 2503090
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2503091
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2503092
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_10

    if-eqz v8, :cond_10

    .line 2503093
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 2503094
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 2503095
    :cond_11
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2503096
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2503097
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_3

    :cond_12
    move v1, v7

    goto :goto_5
.end method
