.class public final Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5e91c25c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2503030
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2503029
    const-class v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2503027
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2503028
    return-void
.end method

.method private a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2503021
    iput-object p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    .line 2503022
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2503023
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2503024
    if-eqz v0, :cond_0

    .line 2503025
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2503026
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2503015
    iput-object p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    .line 2503016
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2503017
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2503018
    if-eqz v0, :cond_0

    .line 2503019
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2503020
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2503009
    iput-boolean p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->f:Z

    .line 2503010
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2503011
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2503012
    if-eqz v0, :cond_0

    .line 2503013
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2503014
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 2503003
    iput-boolean p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->g:Z

    .line 2503004
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2503005
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2503006
    if-eqz v0, :cond_0

    .line 2503007
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2503008
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 2502997
    iput-boolean p1, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->h:Z

    .line 2502998
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2502999
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2503000
    if-eqz v0, :cond_0

    .line 2503001
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2503002
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 2502995
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2502996
    iget-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->f:Z

    return v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2502993
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2502994
    iget-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->g:Z

    return v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 2502991
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2502992
    iget-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->h:Z

    return v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502989
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->i:Ljava/lang/String;

    .line 2502990
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInteractorsFriend"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502987
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->j:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->j:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    .line 2502988
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->j:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    return-object v0
.end method

.method private o()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInteractorsNotFriend"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2503031
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->k:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->k:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    .line 2503032
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->k:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502876
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->l:Ljava/lang/String;

    .line 2502877
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502878
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    .line 2502879
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    return-object v0
.end method

.method private r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502880
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    .line 2502881
    iget-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2502882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502883
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2502884
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2502885
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->o()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2502886
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2502887
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2502888
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2502889
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2502890
    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->e:Z

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 2502891
    const/4 v6, 0x1

    iget-boolean v7, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->f:Z

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 2502892
    const/4 v6, 0x2

    iget-boolean v7, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->g:Z

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 2502893
    const/4 v6, 0x3

    iget-boolean v7, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->h:Z

    invoke-virtual {p1, v6, v7}, LX/186;->a(IZ)V

    .line 2502894
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2502895
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2502896
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2502897
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2502898
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2502899
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2502900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502901
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2502902
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2502903
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2502904
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    .line 2502905
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2502906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    .line 2502907
    iput-object v0, v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->j:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsFriendModel;

    .line 2502908
    :cond_0
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->o()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2502909
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->o()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    .line 2502910
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->o()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2502911
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    .line 2502912
    iput-object v0, v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->k:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$InteractorsNotFriendModel;

    .line 2502913
    :cond_1
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2502914
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    .line 2502915
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2502916
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    .line 2502917
    iput-object v0, v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    .line 2502918
    :cond_2
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2502919
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    .line 2502920
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2502921
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    .line 2502922
    iput-object v0, v1, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    .line 2502923
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2502924
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2502925
    new-instance v0, LX/HnW;

    invoke-direct {v0, p1}, LX/HnW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502926
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2502927
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2502928
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->e:Z

    .line 2502929
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->f:Z

    .line 2502930
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->g:Z

    .line 2502931
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->h:Z

    .line 2502932
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2502961
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2502962
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2502963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2502964
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2502965
    :goto_0
    return-void

    .line 2502966
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2502967
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2502968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2502969
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2502970
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2502971
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2502972
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2502973
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2502974
    :cond_2
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2502975
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v0

    .line 2502976
    if-eqz v0, :cond_4

    .line 2502977
    invoke-virtual {v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2502978
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2502979
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 2502980
    :cond_3
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2502981
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2502982
    if-eqz v0, :cond_4

    .line 2502983
    invoke-virtual {v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2502984
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2502985
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 2502986
    :cond_4
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2502871
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2502872
    check-cast p2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;)V

    .line 2502873
    :cond_0
    :goto_0
    return-void

    .line 2502874
    :cond_1
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2502875
    check-cast p2, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->a(Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 2502933
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2502934
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->a(Z)V

    .line 2502935
    :cond_0
    :goto_0
    return-void

    .line 2502936
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2502937
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->b(Z)V

    goto :goto_0

    .line 2502938
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2502939
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->c(Z)V

    goto :goto_0

    .line 2502940
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2502941
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->q()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    move-result-object v0

    .line 2502942
    if-eqz v0, :cond_0

    .line 2502943
    if-eqz p3, :cond_4

    .line 2502944
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    .line 2502945
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;->a(I)V

    .line 2502946
    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->m:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;

    goto :goto_0

    .line 2502947
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$LikersModel;->a(I)V

    goto :goto_0

    .line 2502948
    :cond_5
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2502949
    invoke-direct {p0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->r()Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    move-result-object v0

    .line 2502950
    if-eqz v0, :cond_0

    .line 2502951
    if-eqz p3, :cond_6

    .line 2502952
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    .line 2502953
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->a(I)V

    .line 2502954
    iput-object v0, p0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;->n:Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;

    goto :goto_0

    .line 2502955
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel$TopLevelCommentsModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2502956
    new-instance v0, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;

    invoke-direct {v0}, Lcom/facebook/browser/liteclient/protocol/BrowserGraphQlFragmentsModels$QueryFeedbackUsingIDModel;-><init>()V

    .line 2502957
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2502958
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2502959
    const v0, 0x10279040

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2502960
    const v0, -0x78fb05b

    return v0
.end method
