.class public abstract Lcom/facebook/share/model/ShareMedia;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/share/model/ShareModel;


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/HcB;)V
    .locals 2

    .prologue
    .line 2486840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486841
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p1, LX/HcB;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/facebook/share/model/ShareMedia;->a:Landroid/os/Bundle;

    .line 2486842
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2486843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486844
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/ShareMedia;->a:Landroid/os/Bundle;

    .line 2486845
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2486846
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2486847
    iget-object v0, p0, Lcom/facebook/share/model/ShareMedia;->a:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2486848
    return-void
.end method
