.class public final Lcom/facebook/share/model/ShareVideoContent;
.super Lcom/facebook/share/model/ShareContent;
.source ""

# interfaces
.implements Lcom/facebook/share/model/ShareModel;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/share/model/ShareContent",
        "<",
        "Lcom/facebook/share/model/ShareVideoContent;",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/facebook/share/model/ShareModel;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/model/ShareVideoContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/share/model/SharePhoto;

.field private final d:Lcom/facebook/share/model/ShareVideo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2486990
    new-instance v0, LX/HcM;

    invoke-direct {v0}, LX/HcM;-><init>()V

    sput-object v0, Lcom/facebook/share/model/ShareVideoContent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 2486991
    invoke-direct {p0, p1}, Lcom/facebook/share/model/ShareContent;-><init>(Landroid/os/Parcel;)V

    .line 2486992
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->a:Ljava/lang/String;

    .line 2486993
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->b:Ljava/lang/String;

    .line 2486994
    new-instance v0, LX/HcI;

    invoke-direct {v0}, LX/HcI;-><init>()V

    .line 2486995
    const-class v1, Lcom/facebook/share/model/SharePhoto;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/share/model/SharePhoto;

    invoke-static {v0, v1}, LX/HcI;->a(LX/HcI;Lcom/facebook/share/model/SharePhoto;)LX/HcI;

    move-result-object v1

    move-object v0, v1

    .line 2486996
    iget-object v1, v0, LX/HcI;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2486997
    if-nez v1, :cond_0

    .line 2486998
    iget-object v1, v0, LX/HcI;->a:Landroid/graphics/Bitmap;

    move-object v1, v1

    .line 2486999
    if-eqz v1, :cond_1

    .line 2487000
    :cond_0
    new-instance v1, Lcom/facebook/share/model/SharePhoto;

    invoke-direct {v1, v0}, Lcom/facebook/share/model/SharePhoto;-><init>(LX/HcI;)V

    move-object v0, v1

    .line 2487001
    iput-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->c:Lcom/facebook/share/model/SharePhoto;

    .line 2487002
    :goto_0
    new-instance v0, LX/HcL;

    invoke-direct {v0}, LX/HcL;-><init>()V

    .line 2487003
    const-class v1, Lcom/facebook/share/model/ShareVideo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/share/model/ShareVideo;

    invoke-static {v0, v1}, LX/HcL;->a(LX/HcL;Lcom/facebook/share/model/ShareVideo;)LX/HcL;

    move-result-object v1

    move-object v0, v1

    .line 2487004
    new-instance v1, Lcom/facebook/share/model/ShareVideo;

    invoke-direct {v1, v0}, Lcom/facebook/share/model/ShareVideo;-><init>(LX/HcL;)V

    move-object v0, v1

    .line 2487005
    iput-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->d:Lcom/facebook/share/model/ShareVideo;

    .line 2487006
    return-void

    .line 2487007
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->c:Lcom/facebook/share/model/SharePhoto;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2487008
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2487009
    invoke-super {p0, p1, p2}, Lcom/facebook/share/model/ShareContent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2487010
    iget-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2487011
    iget-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2487012
    iget-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->c:Lcom/facebook/share/model/SharePhoto;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2487013
    iget-object v0, p0, Lcom/facebook/share/model/ShareVideoContent;->d:Lcom/facebook/share/model/ShareVideo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2487014
    return-void
.end method
