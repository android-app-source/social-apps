.class public Lcom/facebook/credentials/AuthTokenDebugLogger;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Ya;


# static fields
.field private static c:I


# instance fields
.field private a:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2521353
    const/4 v0, -0x1

    sput v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2521354
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/credentials/AuthTokenDebugLogger;LX/0Zb;LX/0W3;)V
    .locals 0

    .prologue
    .line 2521355
    iput-object p1, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->a:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->b:LX/0W3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/credentials/AuthTokenDebugLogger;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {v1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {p0, v0, v1}, Lcom/facebook/credentials/AuthTokenDebugLogger;->a(Lcom/facebook/credentials/AuthTokenDebugLogger;LX/0Zb;LX/0W3;)V

    return-void
.end method

.method public static native getCurrentAuthToken(I)Ljava/lang/String;
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/16 v0, 0x26

    const v1, -0x48d1889b

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2521356
    sget v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    .line 2521357
    invoke-static {p0, p1}, Lcom/facebook/credentials/AuthTokenDebugLogger;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2521358
    iget-object v0, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->b:LX/0W3;

    sget-wide v2, LX/0X5;->v:J

    const/4 v4, 0x1

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 2521359
    iget-object v2, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->b:LX/0W3;

    sget-wide v4, LX/0X5;->s:J

    const/4 v3, 0x5

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    .line 2521360
    iget-object v3, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->b:LX/0W3;

    sget-wide v4, LX/0X5;->t:J

    const/16 v6, 0x12c

    invoke-interface {v3, v4, v5, v6}, LX/0W4;->a(JI)I

    move-result v3

    .line 2521361
    iget-object v4, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->b:LX/0W3;

    sget-wide v6, LX/0X5;->u:J

    const/4 v5, 0x3

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    .line 2521362
    if-nez v0, :cond_0

    .line 2521363
    const/16 v0, 0x27

    const v2, -0x1c787e64    # -4.9992996E21f

    invoke-static {p2, v9, v0, v2, v1}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 2521364
    :goto_0
    return-void

    .line 2521365
    :cond_0
    :try_start_0
    const-string v0, "com.facebook.credentials.AuthTokenStore"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2521366
    :goto_1
    const-string v0, "debugId"

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 2521367
    const-string v0, "keyId"

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 2521368
    if-eq v6, v8, :cond_1

    if-ne v5, v8, :cond_2

    .line 2521369
    :cond_1
    const-string v0, "fb4a-AuthTokenDebugLogger"

    const-string v7, "keyId or debugId not supplied"

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2521370
    :cond_2
    const/4 v0, 0x0

    .line 2521371
    const v7, 0xf4240

    if-le v5, v7, :cond_3

    const v7, 0x1e8480

    if-ge v5, v7, :cond_3

    .line 2521372
    const/high16 v0, 0x49fe0000    # 2080768.0f

    .line 2521373
    :cond_3
    and-int/2addr v0, v6

    if-eqz v0, :cond_4

    .line 2521374
    :try_start_1
    invoke-static {v6}, Lcom/facebook/credentials/AuthTokenDebugLogger;->getCurrentAuthToken(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2521375
    :cond_4
    :goto_2
    sget v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    if-lt v0, v2, :cond_5

    sget v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    if-ge v0, v3, :cond_5

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    int-to-double v4, v4

    div-double v4, v8, v4

    cmpl-double v0, v6, v4

    if-ltz v0, :cond_5

    .line 2521376
    const v0, 0x78203a4f    # 1.2999221E34f

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2521377
    :catch_0
    move-exception v0

    .line 2521378
    const-string v5, "fb4a-AuthTokenDebugLogger"

    const-string v6, "More parameters are required"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 2521379
    :cond_5
    sget v0, Lcom/facebook/credentials/AuthTokenDebugLogger;->c:I

    if-lt v0, v3, :cond_6

    .line 2521380
    const v0, 0x30c7bd6c

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2521381
    :cond_6
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "honey_auth_debug"

    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2521382
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 2521383
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 2521384
    if-eqz v4, :cond_7

    .line 2521385
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2521386
    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 2521387
    :try_start_2
    invoke-virtual {v3, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 2521388
    :catch_1
    move-exception v6

    .line 2521389
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "(error: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 2521390
    :catch_2
    goto :goto_3

    .line 2521391
    :cond_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2521392
    const-string v4, "extras"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521393
    const-string v3, "action"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521394
    const-string v3, "data"

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521395
    const-string v3, "scheme"

    invoke-virtual {p2}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521396
    const-string v3, "flags"

    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521397
    const-string v3, "type"

    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521398
    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2521399
    const-string v0, "AuthTokenDebugLogger"

    .line 2521400
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2521401
    iget-object v0, p0, Lcom/facebook/credentials/AuthTokenDebugLogger;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2521402
    const v0, 0xf5bf4c4

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    :catch_3
    goto/16 :goto_1
.end method
