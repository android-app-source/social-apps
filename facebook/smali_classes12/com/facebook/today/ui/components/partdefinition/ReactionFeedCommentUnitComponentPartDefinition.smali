.class public Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/Hcg;",
        "TE;",
        "LX/Hcl;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/9Cd;

.field private final f:LX/8qs;

.field private final g:Landroid/content/Context;

.field private final h:LX/1Uf;

.field public final i:LX/E1f;

.field private final j:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2487319
    new-instance v0, LX/Hca;

    invoke-direct {v0}, LX/Hca;-><init>()V

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a:LX/1Cz;

    .line 2487320
    const-class v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9Cd;LX/8qs;Landroid/content/Context;LX/1Uf;LX/E1f;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487309
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2487310
    iput-object p1, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2487311
    iput-object p2, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2487312
    iput-object p3, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->e:LX/9Cd;

    .line 2487313
    iput-object p4, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->f:LX/8qs;

    .line 2487314
    iput-object p5, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->g:Landroid/content/Context;

    .line 2487315
    iput-object p6, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->h:LX/1Uf;

    .line 2487316
    iput-object p7, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->i:LX/E1f;

    .line 2487317
    iput-object p8, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->j:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2487318
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/Hcg;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/Hcg;"
        }
    .end annotation

    .prologue
    .line 2487289
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v4

    invoke-interface {v4}, LX/9uc;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    move-result-object v4

    invoke-static {v4}, LX/9tr;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    move-object/from16 v4, p3

    .line 2487290
    check-cast v4, LX/1Pr;

    new-instance v6, LX/HcW;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v7

    invoke-interface {v7}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, LX/HcW;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-interface {v4, v6, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/HcX;

    .line 2487291
    new-instance v9, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v10}, LX/HcX;->a()LX/03R;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->m()Z

    move-result v6

    invoke-virtual {v4, v6}, LX/03R;->asBoolean(Z)Z

    move-result v4

    invoke-direct {v9, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 2487292
    new-instance v14, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v10}, LX/HcX;->b()Z

    move-result v4

    invoke-direct {v14, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 2487293
    invoke-static {v5, v9}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 2487294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->h:LX/1Uf;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-static {v6}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v6

    const/4 v7, 0x0

    const v8, 0x7f0a0038

    const/4 v11, 0x0

    invoke-virtual {v4, v6, v7, v8, v11}, LX/1Uf;->a(LX/1y5;LX/0lF;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-static {v4}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v15

    .line 2487295
    const/4 v6, 0x0

    .line 2487296
    const/4 v4, 0x0

    .line 2487297
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2487298
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->h:LX/1Uf;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {v6}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v6, v7, v8, v11}, LX/1Uf;->a(LX/1eE;ZLX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-static {v4}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2487299
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->h:LX/1Uf;

    invoke-static {v4, v14, v10}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a(Ljava/lang/CharSequence;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)Landroid/view/View$OnClickListener;

    move-result-object v7

    const/16 v8, 0x5a

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->g:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08117c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v4, v7, v8, v11}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v6

    move-object v12, v4

    move-object v13, v6

    .line 2487300
    :goto_0
    new-instance v16, LX/Hcb;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, LX/Hcb;-><init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V

    .line 2487301
    new-instance v6, LX/Hcc;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v6, v0, v1, v2, v3}, LX/Hcc;-><init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Landroid/view/View$OnClickListener;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2487302
    new-instance v17, LX/Hcd;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3, v6}, LX/Hcd;-><init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/view/View$OnClickListener;)V

    move-object/from16 v4, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    .line 2487303
    invoke-static/range {v4 .. v10}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a$redex0(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View$OnClickListener;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)Ljava/util/List;

    move-result-object v4

    .line 2487304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-object/from16 v18, v0

    new-instance v6, LX/E1o;

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v7

    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v11, 0x0

    move-object/from16 v9, p2

    move-object/from16 v10, v16

    invoke-direct/range {v6 .. v11}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2487305
    const v6, 0x7f0d09cb

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v6, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2487306
    const v6, 0x7f0d09cd

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v6, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2487307
    const v6, 0x7f0d09cb

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->j:Lcom/facebook/multirow/parts/TextPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7, v15}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2487308
    new-instance v6, LX/Hcg;

    move-object v7, v13

    move-object v8, v12

    move-object v9, v5

    move-object v10, v4

    move-object v11, v14

    invoke-direct/range {v6 .. v11}, LX/Hcg;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    return-object v6

    :cond_0
    move-object v12, v4

    move-object v13, v6

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2487288
    new-instance v0, LX/Hcf;

    invoke-direct {v0, p1, p2, p0}, LX/Hcf;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;
    .locals 12

    .prologue
    .line 2487237
    const-class v1, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    monitor-enter v1

    .line 2487238
    :try_start_0
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487239
    sput-object v2, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487242
    new-instance v3, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/9Cd;->b(LX/0QB;)LX/9Cd;

    move-result-object v6

    check-cast v6, LX/9Cd;

    invoke-static {v0}, LX/8qs;->b(LX/0QB;)LX/8qs;

    move-result-object v7

    check-cast v7, LX/8qs;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v9

    check-cast v9, LX/1Uf;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v10

    check-cast v10, LX/E1f;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9Cd;LX/8qs;Landroid/content/Context;LX/1Uf;LX/E1f;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2487243
    move-object v0, v3

    .line 2487244
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487245
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487246
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View$OnClickListener;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            "LX/HcX;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2487284
    new-instance v0, LX/Hce;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p1

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/Hce;-><init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Landroid/view/View$OnClickListener;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;)V

    .line 2487285
    new-instance v1, LX/8qr;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->g:Landroid/content/Context;

    const v4, 0x7f0e0a3d

    invoke-direct {v2, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v2}, LX/8qr;-><init>(Landroid/content/Context;)V

    .line 2487286
    iput-object v0, v1, LX/8qr;->b:LX/8qq;

    .line 2487287
    iget-object v0, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->f:LX/8qs;

    invoke-virtual {v0, v1, p1, p1}, LX/8qs;->a(LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/view/View;)LX/Hcl;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2487278
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 2487279
    instance-of v1, v0, LX/Hcl;

    if-eqz v1, :cond_0

    .line 2487280
    check-cast v0, LX/Hcl;

    .line 2487281
    :goto_1
    return-object v0

    .line 2487282
    :cond_0
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 2487283
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLComment;Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2487266
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->k()I

    move-result v3

    .line 2487267
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sub-int v0, v3, v0

    .line 2487268
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2487269
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    invoke-static {v3}, LX/3dN;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;

    move-result-object v3

    .line 2487270
    iput v0, v3, LX/3dN;->b:I

    .line 2487271
    move-object v0, v3

    .line 2487272
    invoke-virtual {v0}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v0, v2}, LX/3dM;->j(Z)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 2487273
    iput-object v0, v1, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2487274
    move-object v0, v1

    .line 2487275
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 2487276
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2487277
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2487265
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2487264
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->a(LX/1aD;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/Hcg;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3bf9534e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487254
    check-cast p2, LX/Hcg;

    check-cast p4, LX/Hcl;

    const/4 p0, 0x0

    .line 2487255
    iget-object v1, p4, LX/Hcl;->b:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    move-object v2, v1

    .line 2487256
    sget-object v1, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0, p0, v1}, LX/3Wq;->a(Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2487257
    iget-object v1, p2, LX/Hcg;->a:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p2, LX/Hcg;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p2, LX/Hcg;->b:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v2, v1}, LX/3Wq;->setBody(Ljava/lang/CharSequence;)V

    .line 2487258
    iget-object v1, p2, LX/Hcg;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->B()Z

    move-result v1

    invoke-virtual {v2, v1}, LX/3Wq;->setVerifiedBadgeVisibility(Z)V

    .line 2487259
    iget-object v1, p2, LX/Hcg;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->D()Z

    move-result v1

    invoke-virtual {v2, v1}, LX/3Wq;->setPinnedIconVisibility(Z)V

    .line 2487260
    iget-object v1, p4, LX/Hcl;->a:Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    move-object v1, v1

    .line 2487261
    iget-object v2, p2, LX/Hcg;->d:Ljava/util/List;

    invoke-virtual {p4}, LX/Hcl;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {v2, p0}, LX/8qs;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->setMetadataText(Ljava/lang/CharSequence;)V

    .line 2487262
    const/16 v1, 0x1f

    const v2, 0x24422cb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2487263
    :cond_1
    iget-object v1, p2, LX/Hcg;->a:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2487248
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2487249
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2487250
    invoke-interface {v0}, LX/9uc;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;

    move-result-object v0

    .line 2487251
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hn_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hn_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel;->hr_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$FeedbackModel$LikersModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$AuthorModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;->hm_()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel$ParentFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2487252
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2487253
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
