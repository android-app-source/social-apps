.class public Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kp;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2487328
    new-instance v0, LX/Hch;

    invoke-direct {v0}, LX/Hch;-><init>()V

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487329
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2487330
    iput-object p1, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 2487331
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 2487332
    const-class v1, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;

    monitor-enter v1

    .line 2487333
    :try_start_0
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487334
    sput-object v2, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487335
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487336
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487337
    new-instance p0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 2487338
    move-object v0, p0

    .line 2487339
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487340
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487341
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487342
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2487343
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2487344
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v2, 0x0

    .line 2487345
    new-instance v0, LX/E1o;

    .line 2487346
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2487347
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    new-instance v4, LX/Hci;

    invoke-direct {v4, p0, p3, p2}, LX/Hci;-><init>(Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v3, p2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    .line 2487348
    iget-object v1, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2487349
    const v1, 0x7f0d2f77

    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/ReactionTopLevelCommentComposerUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-interface {p1, v1, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2487350
    return-object v2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2487351
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2487352
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2487353
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
