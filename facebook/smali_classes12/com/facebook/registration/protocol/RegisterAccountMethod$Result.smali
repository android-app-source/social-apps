.class public final Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/registration/protocol/RegisterAccountMethod_ResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final accountType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "account_type"
    .end annotation
.end field

.field public final completionUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "completion_url"
    .end annotation
.end field

.field public final sessionInfo:Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_info"
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "new_user_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2483467
    const-class v0, Lcom/facebook/registration/protocol/RegisterAccountMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2483486
    new-instance v0, LX/HaB;

    invoke-direct {v0}, LX/HaB;-><init>()V

    sput-object v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2483480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483481
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->accountType:Ljava/lang/String;

    .line 2483482
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->completionUrl:Ljava/lang/String;

    .line 2483483
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2483484
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->sessionInfo:Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;

    .line 2483485
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2483474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483475
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->accountType:Ljava/lang/String;

    .line 2483476
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->completionUrl:Ljava/lang/String;

    .line 2483477
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2483478
    const-class v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->sessionInfo:Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;

    .line 2483479
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2483473
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2483468
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->accountType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483469
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->completionUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483470
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483471
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->sessionInfo:Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2483472
    return-void
.end method
