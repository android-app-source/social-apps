.class public final Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/registration/protocol/RegisterAccountMethod_SessionInfoDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final accessToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "access_token"
    .end annotation
.end field

.field public final confirmed:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "confirmed"
    .end annotation
.end field

.field public final secret:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secret"
    .end annotation
.end field

.field public final sessionKey:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_key"
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2483497
    const-class v0, Lcom/facebook/registration/protocol/RegisterAccountMethod_SessionInfoDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2483505
    new-instance v0, LX/HaC;

    invoke-direct {v0}, LX/HaC;-><init>()V

    sput-object v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2483498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483499
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->sessionKey:Ljava/lang/String;

    .line 2483500
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->uid:Ljava/lang/String;

    .line 2483501
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->secret:Ljava/lang/String;

    .line 2483502
    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->accessToken:Ljava/lang/String;

    .line 2483503
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->confirmed:Z

    .line 2483504
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2483506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483507
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->sessionKey:Ljava/lang/String;

    .line 2483508
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->uid:Ljava/lang/String;

    .line 2483509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->secret:Ljava/lang/String;

    .line 2483510
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->accessToken:Ljava/lang/String;

    .line 2483511
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->confirmed:Z

    .line 2483512
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2483490
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2483491
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->sessionKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483492
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->uid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483493
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->secret:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483494
    iget-object v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483495
    iget-boolean v0, p0, Lcom/facebook/registration/protocol/RegisterAccountMethod$SessionInfo;->confirmed:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483496
    return-void
.end method
