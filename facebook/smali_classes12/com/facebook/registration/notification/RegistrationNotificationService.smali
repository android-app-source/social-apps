.class public Lcom/facebook/registration/notification/RegistrationNotificationService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Landroid/app/NotificationManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2483372
    const-string v0, "RegistrationNotificationService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2483373
    return-void
.end method

.method private a()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2483374
    sget-object v0, LX/Ha7;->OPEN_REGISTRATION_FLOW:LX/Ha7;

    invoke-static {p0, v0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->a(Landroid/content/Context;LX/Ha7;)Landroid/content/Intent;

    move-result-object v0

    .line 2483375
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/Ha7;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2483376
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/registration/notification/RegistrationNotificationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2483377
    const-string v1, "operation_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2483378
    return-object v0
.end method

.method private static a(Lcom/facebook/registration/notification/RegistrationNotificationService;Landroid/app/NotificationManager;Lcom/facebook/content/SecureContextHelper;LX/0WJ;LX/HZt;LX/0Uh;)V
    .locals 0

    .prologue
    .line 2483379
    iput-object p1, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->a:Landroid/app/NotificationManager;

    iput-object p2, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->c:LX/0WJ;

    iput-object p4, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->d:LX/HZt;

    iput-object p5, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->e:LX/0Uh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/registration/notification/RegistrationNotificationService;

    invoke-static {v5}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v5}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v4

    check-cast v4, LX/HZt;

    invoke-static {v5}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static/range {v0 .. v5}, Lcom/facebook/registration/notification/RegistrationNotificationService;->a(Lcom/facebook/registration/notification/RegistrationNotificationService;Landroid/app/NotificationManager;Lcom/facebook/content/SecureContextHelper;LX/0WJ;LX/HZt;LX/0Uh;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x24

    const v1, -0x75855c43

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2483380
    iget-object v0, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "operation_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2483381
    :cond_0
    const/16 v0, 0x25

    const v2, 0x3b77778b

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2483382
    :goto_0
    return-void

    .line 2483383
    :cond_1
    const-string v0, "operation_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Ha7;

    .line 2483384
    sget-object v2, LX/Ha6;->a:[I

    invoke-virtual {v0}, LX/Ha7;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2483385
    :goto_1
    const v0, 0x36630f7b

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 2483386
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->d:LX/HZt;

    .line 2483387
    iget-object v2, v0, LX/HZt;->a:LX/0Zb;

    sget-object v4, LX/HZu;->FINISH_REGISTRATION_NOTIF_CREATED:LX/HZu;

    invoke-static {v0, v4}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2483388
    new-instance v0, LX/2HB;

    invoke-direct {v0, p0}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 2483389
    iput v3, v0, LX/2HB;->j:I

    .line 2483390
    move-object v2, v0

    .line 2483391
    invoke-virtual {v2, v3}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0835a1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0835a1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/5MF;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    const v3, 0x7f0218e4

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->a()Landroid/app/PendingIntent;

    move-result-object v3

    .line 2483392
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2483393
    move-object v2, v2

    .line 2483394
    const/4 v3, 0x4

    new-array v3, v3, [J

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, LX/2HB;->a([J)LX/2HB;

    move-result-object v2

    const v3, -0xffff01

    const/16 v4, 0x1f4

    const/16 v5, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, LX/2HB;->a(III)LX/2HB;

    .line 2483395
    iget-object v2, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->a:Landroid/app/NotificationManager;

    sget-object v3, LX/Ha7;->CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

    invoke-virtual {v3}, LX/Ha7;->name()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v2, v3, v4, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_1

    .line 2483396
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->d:LX/HZt;

    .line 2483397
    iget-object v2, v0, LX/HZt;->a:LX/0Zb;

    sget-object v3, LX/HZu;->FINISH_REGISTRATION_NOTIF_CLICKED:LX/HZu;

    invoke-static {v0, v3}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2483398
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2483399
    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2483400
    iget-object v2, p0, Lcom/facebook/registration/notification/RegistrationNotificationService;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    nop

    :array_0
    .array-data 8
        0x0
        0xfa
        0xc8
        0xfa
    .end array-data

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x567a75c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2483401
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2483402
    invoke-static {p0, p0}, Lcom/facebook/registration/notification/RegistrationNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2483403
    const/16 v1, 0x25

    const v2, -0x7c1c3d6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
