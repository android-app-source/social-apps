.class public Lcom/facebook/registration/controller/RegistrationFragmentController;
.super Lcom/facebook/base/fragment/AbstractNavigableFragmentController;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/F9o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HYv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Ha5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/27t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2481347
    const-class v0, Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/registration/controller/RegistrationFragmentController;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2481345
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;-><init>()V

    .line 2481346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->n:Z

    return-void
.end method

.method private static a(Lcom/facebook/registration/controller/RegistrationFragmentController;LX/F9o;LX/HYv;Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/Ha5;LX/0if;Ljava/util/concurrent/Executor;LX/HaQ;LX/27t;LX/0Uh;LX/0aG;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 2481344
    iput-object p1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->a:LX/F9o;

    iput-object p2, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->b:LX/HYv;

    iput-object p3, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object p4, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    iput-object p5, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->e:LX/Ha5;

    iput-object p6, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->f:LX/0if;

    iput-object p7, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->g:Ljava/util/concurrent/Executor;

    iput-object p8, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->h:LX/HaQ;

    iput-object p9, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->i:LX/27t;

    iput-object p10, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->j:LX/0Uh;

    iput-object p11, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->k:LX/0aG;

    iput-object p12, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->l:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-static {v12}, LX/F9o;->a(LX/0QB;)LX/F9o;

    move-result-object v1

    check-cast v1, LX/F9o;

    invoke-static {v12}, LX/HYv;->b(LX/0QB;)LX/HYv;

    move-result-object v2

    check-cast v2, LX/HYv;

    invoke-static {v12}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v12}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v4

    check-cast v4, LX/HZt;

    invoke-static {v12}, LX/Ha5;->a(LX/0QB;)LX/Ha5;

    move-result-object v5

    check-cast v5, LX/Ha5;

    invoke-static {v12}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    invoke-static {v12}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v12}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v8

    check-cast v8, LX/HaQ;

    invoke-static {v12}, LX/27t;->b(LX/0QB;)LX/27t;

    move-result-object v9

    check-cast v9, LX/27t;

    invoke-static {v12}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v12}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v11

    check-cast v11, LX/0aG;

    invoke-static {v12}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v0 .. v12}, Lcom/facebook/registration/controller/RegistrationFragmentController;->a(Lcom/facebook/registration/controller/RegistrationFragmentController;LX/F9o;LX/HYv;Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/Ha5;LX/0if;Ljava/util/concurrent/Executor;LX/HaQ;LX/27t;LX/0Uh;LX/0aG;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2481334
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2481335
    const-string v2, "email"

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481336
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2481337
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2481338
    new-instance v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;

    const-string v4, ""

    const-string v5, ""

    move-object v2, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2481339
    const-string v1, "accountRecoverySearchAccountParamsKey"

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2481340
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->k:LX/0aG;

    const-string v1, "account_recovery_search_account"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/registration/controller/RegistrationFragmentController;->m:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0xd0a0486

    move-object v2, v6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2481341
    new-instance v1, LX/HZ0;

    invoke-direct {v1, p0, p1}, LX/HZ0;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;Ljava/util/List;)V

    iget-object v2, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2481342
    :goto_0
    return-void

    .line 2481343
    :catch_0
    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 2481323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->n:Z

    .line 2481324
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->b:LX/HYv;

    .line 2481325
    invoke-virtual {v0}, LX/HYp;->c()LX/HYm;

    move-result-object v1

    invoke-interface {v1}, LX/HYm;->a()Landroid/content/Intent;

    move-result-object v1

    .line 2481326
    const-string v2, "com.facebook.fragment.ENTER_ANIM"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2481327
    move-object v0, v1

    .line 2481328
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    .line 2481329
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->f:LX/0if;

    sget-object v1, LX/0ig;->c:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2481330
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->e:LX/Ha5;

    .line 2481331
    iget-object v1, v0, LX/Ha5;->c:Landroid/app/NotificationManager;

    sget-object v2, LX/Ha7;->CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

    invoke-virtual {v2}, LX/Ha7;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 2481332
    iget-object v1, v0, LX/Ha5;->b:LX/12x;

    invoke-static {v0}, LX/Ha5;->c(LX/Ha5;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 2481333
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    .line 2481316
    const-wide/16 v2, 0x1f4

    .line 2481317
    invoke-direct {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481318
    const-wide/16 v2, 0x7d0

    .line 2481319
    :cond_0
    invoke-direct {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2481320
    new-instance v0, LX/HYx;

    const-wide/16 v4, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/HYx;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;JJ)V

    invoke-virtual {v0}, LX/HYx;->start()Landroid/os/CountDownTimer;

    .line 2481321
    :goto_0
    return-void

    .line 2481322
    :cond_1
    invoke-static {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->n(Lcom/facebook/registration/controller/RegistrationFragmentController;)V

    goto :goto_0
.end method

.method private m()Z
    .locals 6

    .prologue
    .line 2481244
    invoke-direct {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2481245
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    const-string v1, "GK_FAILED"

    invoke-virtual {v0, v1}, LX/HZt;->h(Ljava/lang/String;)V

    .line 2481246
    const/4 v0, 0x0

    .line 2481247
    :goto_0
    return v0

    .line 2481248
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    const-string v1, "ATTEMPT"

    invoke-virtual {v0, v1}, LX/HZt;->h(Ljava/lang/String;)V

    .line 2481249
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->k:LX/0aG;

    const-string v1, "registration_header_prefill_kickoff"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/registration/controller/RegistrationFragmentController;->m:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0xd06f2d0

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2481250
    new-instance v1, LX/HYy;

    invoke-direct {v1, p0}, LX/HYy;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V

    iget-object v2, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2481251
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static n(Lcom/facebook/registration/controller/RegistrationFragmentController;)V
    .locals 6

    .prologue
    .line 2481308
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->i:LX/27t;

    .line 2481309
    iget-object v1, v0, LX/27t;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dH;->c:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    move v0, v1

    .line 2481310
    if-nez v0, :cond_0

    .line 2481311
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/HZt;->b(Z)V

    .line 2481312
    :goto_0
    return-void

    .line 2481313
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/HZt;->b(Z)V

    .line 2481314
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->k:LX/0aG;

    const-string v1, "registration_contact_point_suggestions"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/registration/controller/RegistrationFragmentController;->m:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x2e76fc3e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2481315
    new-instance v1, LX/HYz;

    invoke-direct {v1, p0}, LX/HYz;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V

    iget-object v2, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private o()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2481306
    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->j:LX/0Uh;

    const/16 v2, 0x34

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 2481307
    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private p()Z
    .locals 3

    .prologue
    .line 2481305
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->j:LX/0Uh;

    const/16 v1, 0x3c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2481266
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/os/Bundle;)V

    .line 2481267
    const-class v0, Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-static {v0, p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->a(Ljava/lang/Class;LX/02k;)V

    .line 2481268
    new-instance v0, LX/HYw;

    invoke-direct {v0, p0}, LX/HYw;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V

    .line 2481269
    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    .line 2481270
    if-eqz p1, :cond_0

    .line 2481271
    const-string v0, "controller_started"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->n:Z

    .line 2481272
    const-string v0, "form_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481273
    if-eqz v0, :cond_0

    .line 2481274
    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(Lcom/facebook/registration/model/SimpleRegFormData;)V

    .line 2481275
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->n:Z

    if-nez v0, :cond_3

    .line 2481276
    invoke-direct {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->l()V

    .line 2481277
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/registration/controller/RegistrationFragmentController$2;

    invoke-direct {v1, p0}, Lcom/facebook/registration/controller/RegistrationFragmentController$2;-><init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V

    const v2, 0x1c57d6bc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2481278
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->a:LX/F9o;

    invoke-virtual {v0}, LX/F9o;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2481279
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->a:LX/F9o;

    .line 2481280
    iget-object v2, v1, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    move-object v1, v2

    .line 2481281
    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(Lcom/facebook/growth/model/DeviceOwnerData;)V

    .line 2481282
    invoke-direct {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->k()V

    .line 2481283
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->h:LX/HaQ;

    .line 2481284
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2481285
    iget-object v1, v0, LX/HaQ;->l:LX/1Ml;

    const-string v4, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v4}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2481286
    iget-object v1, v0, LX/HaQ;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 2481287
    const-string v4, "com.google"

    invoke-virtual {v1, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 2481288
    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object p1, v4, v1

    .line 2481289
    iget-object p1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2481290
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2481291
    :cond_1
    move-object v0, v2

    .line 2481292
    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481293
    iput-object v0, v1, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    .line 2481294
    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->j:LX/0Uh;

    const/16 v2, 0x30

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2481295
    invoke-direct {p0, v0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->a(Ljava/util/List;)V

    .line 2481296
    :cond_2
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->h:LX/HaQ;

    const/4 v2, 0x0

    .line 2481297
    invoke-static {v0}, LX/HaQ;->f(LX/HaQ;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2481298
    :cond_3
    :goto_1
    return-void

    .line 2481299
    :cond_4
    iget-object v1, v0, LX/HaQ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Ha8;->d:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2481300
    :try_start_0
    iget-object v3, v0, LX/HaQ;->n:LX/0lB;

    const-class v4, Lcom/facebook/registration/model/RegistrationFormData;

    invoke-virtual {v3, v1, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/registration/model/RegistrationFormData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2481301
    :goto_2
    if-eqz v1, :cond_3

    .line 2481302
    iget-object v2, v0, LX/HaQ;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/registration/model/RegistrationFormData;)V

    goto :goto_1

    .line 2481303
    :catch_0
    move-exception v1

    .line 2481304
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "Error converting JSON to RegistrationFormData"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_2
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2481265
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->a()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2481264
    iget-object v0, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->b()Z

    move-result v0

    return v0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2481260
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2481261
    const-string v0, "controller_started"

    iget-boolean v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2481262
    const-string v0, "form_data"

    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2481263
    return-void
.end method

.method public final onStop()V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x654f8eb4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2481252
    invoke-virtual {p0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2481253
    iget-object v1, p0, Lcom/facebook/registration/controller/RegistrationFragmentController;->e:LX/Ha5;

    .line 2481254
    iget-object v4, v1, LX/Ha5;->d:LX/HZt;

    .line 2481255
    iget-object v5, v4, LX/HZt;->a:LX/0Zb;

    sget-object v6, LX/HZu;->FINISH_REGISTRATION_NOTIF_SCHEDULED:LX/HZu;

    invoke-static {v4, v6}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 2481257
    iget-object v6, v1, LX/Ha5;->b:LX/12x;

    const/4 v7, 0x3

    invoke-static {v1}, LX/Ha5;->c(LX/Ha5;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v6, v7, v4, v5, v8}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    .line 2481258
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->onStop()V

    .line 2481259
    const/16 v1, 0x2b

    const v2, -0x3c4f8d6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
