.class public Lcom/facebook/registration/activity/RegistrationCampaignActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HZt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HYg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480779
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 2480780
    invoke-virtual {p0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2480781
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_4

    :cond_0
    move-object v1, v2

    .line 2480782
    :goto_0
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    .line 2480783
    if-nez v1, :cond_5

    .line 2480784
    const-string v0, "unknown"

    move-object v3, v2

    move-object v2, v0

    .line 2480785
    :goto_1
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    .line 2480786
    const-string v0, "reg_instance"

    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2480787
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2480788
    iget-object v1, p0, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYg;

    invoke-virtual {v1, v0}, LX/HYg;->a(Ljava/lang/String;)V

    .line 2480789
    :cond_1
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HZt;

    .line 2480790
    sget-object v1, LX/HZu;->FB4A_REGISTRATION_UPSELL_CAMPAIGN:LX/HZu;

    invoke-static {v0, v1}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2480791
    const-string v1, "campaign_name"

    invoke-virtual {v6, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2480792
    if-eqz v3, :cond_2

    .line 2480793
    const-string v1, "campaign_path_keys"

    invoke-virtual {v6, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2480794
    :cond_2
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2480795
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 2480796
    :cond_3
    iget-object v1, v0, LX/HZt;->a:LX/0Zb;

    invoke-interface {v1, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2480797
    return-void

    .line 2480798
    :cond_4
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2480799
    :cond_5
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 2480800
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 2480801
    if-eqz v5, :cond_9

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2480802
    const-string v6, "key-value-pairs"

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2480803
    const/4 v0, 0x1

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 2480804
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v6, v0, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v1, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2480805
    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    :cond_6
    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    goto/16 :goto_1

    .line 2480806
    :cond_7
    const-string v0, ","

    new-array v2, v7, [Ljava/lang/String;

    invoke-interface {v5, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2480807
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 2480808
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 2480809
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2480810
    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_4

    :cond_8
    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    goto/16 :goto_1

    :cond_9
    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    goto/16 :goto_1
.end method

.method private static a(Lcom/facebook/registration/activity/RegistrationCampaignActivity;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/registration/activity/RegistrationCampaignActivity;",
            "LX/0Ot",
            "<",
            "LX/HZt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HYg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2480811
    iput-object p1, p0, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->p:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->q:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/activity/RegistrationCampaignActivity;

    const/16 v1, 0x3195

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3191

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->a(Lcom/facebook/registration/activity/RegistrationCampaignActivity;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2480812
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2480813
    invoke-static {p0, p0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2480814
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->a()V

    .line 2480815
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2480816
    invoke-virtual {p0, v0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->startActivity(Landroid/content/Intent;)V

    .line 2480817
    invoke-virtual {p0}, Lcom/facebook/registration/activity/RegistrationCampaignActivity;->finish()V

    .line 2480818
    return-void
.end method
