.class public Lcom/facebook/registration/activity/RegistrationLoginActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public A:LX/HYf;

.field private B:I

.field private C:Z

.field private final D:Landroid/os/Handler;

.field private final E:Ljava/lang/Runnable;

.field public p:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/HZv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/0h5;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:LX/0Yb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2480954
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2480955
    iput v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->B:I

    .line 2480956
    iput-boolean v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->C:Z

    .line 2480957
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->D:Landroid/os/Handler;

    .line 2480958
    new-instance v0, Lcom/facebook/registration/activity/RegistrationLoginActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity$1;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->E:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V
    .locals 1

    .prologue
    .line 2480952
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->q:LX/GvB;

    invoke-virtual {v0, p0}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 2480953
    return-void
.end method

.method private static a(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/GvB;LX/GvB;LX/0Xl;LX/HZt;LX/HZv;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/registration/activity/RegistrationLoginActivity;",
            "Lcom/facebook/auth/login/ipc/BackgroundAuthUtil;",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            "LX/0Xl;",
            "LX/HZt;",
            "LX/HZv;",
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2480951
    iput-object p1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->p:LX/GvB;

    iput-object p2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->q:LX/GvB;

    iput-object p3, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->r:LX/0Xl;

    iput-object p4, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    iput-object p5, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->t:LX/HZv;

    iput-object p6, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->v:LX/0Uh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/registration/activity/RegistrationLoginActivity;

    invoke-static {v7}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v1

    check-cast v1, LX/GvB;

    invoke-static {v7}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v2

    check-cast v2, LX/GvB;

    invoke-static {v7}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v7}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v4

    check-cast v4, LX/HZt;

    invoke-static {v7}, LX/HZv;->a(LX/0QB;)LX/HZv;

    move-result-object v5

    check-cast v5, LX/HZv;

    const/16 v6, 0x8a

    invoke-static {v7, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static/range {v0 .. v7}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/GvB;LX/GvB;LX/0Xl;LX/HZt;LX/HZv;LX/0Ot;LX/0Uh;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/HYf;)V
    .locals 1

    .prologue
    .line 2480949
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/HYf;Landroid/content/Intent;)V

    .line 2480950
    return-void
.end method

.method public static a$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/HYf;Landroid/content/Intent;)V
    .locals 8
    .param p1    # LX/HYf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2480909
    iput-object p1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->A:LX/HYf;

    .line 2480910
    sget-object v0, LX/HYe;->a:[I

    invoke-virtual {p1}, LX/HYf;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2480911
    :goto_0
    :pswitch_0
    return-void

    .line 2480912
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    .line 2480913
    iget-object v1, v0, LX/HZt;->a:LX/0Zb;

    sget-object v2, LX/HZu;->REGISTRATION_LOGIN_START:LX/HZu;

    invoke-static {v0, v2}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2480914
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->t:LX/HZv;

    const/4 p1, 0x1

    .line 2480915
    iget-object v1, v0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x400003

    const-string p0, "RegistrationLoginTime"

    invoke-direct {v2, v3, p0}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2}, LX/0Yj;->b()LX/0Yj;

    move-result-object v2

    .line 2480916
    iput-boolean p1, v2, LX/0Yj;->n:Z

    .line 2480917
    move-object v2, v2

    .line 2480918
    invoke-interface {v1, v2, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 2480919
    goto :goto_0

    .line 2480920
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    sget-object v1, LX/HYf;->LOGIN_COMPLETE:LX/HYf;

    invoke-virtual {v1}, LX/HYf;->name()Ljava/lang/String;

    move-result-object v1

    .line 2480921
    iget-object v2, v0, LX/HZt;->a:LX/0Zb;

    sget-object v3, LX/HZu;->REGISTRATION_LOGIN_SUCCESS:LX/HZu;

    invoke-static {v0, v3}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "state"

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2480922
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->t:LX/HZv;

    .line 2480923
    iget-object v1, v0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x400003

    const-string v3, "RegistrationLoginTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2480924
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->v:LX/0Uh;

    const/16 v1, 0x3e1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2480925
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    iget-object v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->x:Ljava/lang/String;

    .line 2480926
    iget-object v4, v0, LX/HZt;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/294;

    iget-object v5, v0, LX/HZt;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7, v1}, LX/294;->a(JLjava/lang/String;)V

    .line 2480927
    :goto_1
    invoke-static {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    goto :goto_0

    .line 2480928
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CZ;

    invoke-virtual {v0}, LX/2CZ;->a()V

    goto :goto_1

    .line 2480929
    :pswitch_3
    if-eqz p2, :cond_4

    .line 2480930
    const-string v0, "AUTH_FAILED_THROWABLE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2480931
    instance-of v2, v0, LX/2Oo;

    if-eqz v2, :cond_3

    .line 2480932
    check-cast v0, LX/2Oo;

    .line 2480933
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    .line 2480934
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 2480935
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    .line 2480936
    const/16 v3, 0x196

    if-eq v2, v3, :cond_1

    const/16 v3, 0x195

    if-ne v2, v3, :cond_2

    .line 2480937
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->C:Z

    .line 2480938
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    invoke-virtual {p1}, LX/HYf;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, LX/HZt;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2480939
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->t:LX/HZv;

    invoke-virtual {v0}, LX/HZv;->h()V

    .line 2480940
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->l()V

    .line 2480941
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->o()V

    goto/16 :goto_0

    .line 2480942
    :cond_3
    if-eqz v0, :cond_4

    .line 2480943
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 2480944
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2480945
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->s:LX/HZt;

    sget-object v2, LX/HYf;->TIMEOUT:LX/HYf;

    invoke-virtual {v2}, LX/HYf;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v1}, LX/HZt;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2480946
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->t:LX/HZv;

    invoke-virtual {v0}, LX/HZv;->h()V

    .line 2480947
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->l()V

    .line 2480948
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->n()V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V
    .locals 5

    .prologue
    .line 2480899
    iget v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->B:I

    .line 2480900
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->C:Z

    .line 2480901
    sget-object v0, LX/HYf;->LOGGING_IN:LX/HYf;

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;LX/HYf;)V

    .line 2480902
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->E:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    const v4, -0x4aa3cf04

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2480903
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->m()V

    .line 2480904
    new-instance v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->y:Ljava/lang/String;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    .line 2480905
    const/4 v4, 0x0

    .line 2480906
    invoke-static {p0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v2

    .line 2480907
    const-string v3, "auth"

    invoke-static {v0, v3, v2, v4, v4}, LX/2CQ;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V

    .line 2480908
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2480895
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->z:LX/0Yb;

    if-eqz v0, :cond_0

    .line 2480896
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->z:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2480897
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->E:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2480898
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 2480892
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->r:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.AUTH_COMPLETE"

    new-instance v2, LX/HYa;

    invoke-direct {v2, p0}, LX/HYa;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    new-instance v2, LX/HYZ;

    invoke-direct {v2, p0}, LX/HYZ;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.AUTH_FAILED"

    new-instance v2, LX/HYY;

    invoke-direct {v2, p0}, LX/HYY;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->z:LX/0Yb;

    .line 2480893
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->z:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2480894
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2480886
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800e5

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0800e8

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    .line 2480887
    iget v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->B:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 2480888
    const v1, 0x7f080043

    new-instance v2, LX/HYc;

    invoke-direct {v2, p0}, LX/HYc;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080018

    new-instance v3, LX/HYb;

    invoke-direct {v3, p0}, LX/HYb;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2480889
    :goto_0
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2480890
    return-void

    .line 2480891
    :cond_0
    const v1, 0x7f080016

    new-instance v2, LX/HYd;

    invoke-direct {v2, p0}, LX/HYd;-><init>(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2480879
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->A:LX/HYf;

    sget-object v1, LX/HYf;->LOGIN_COMPLETE:LX/HYf;

    if-ne v0, v1, :cond_1

    .line 2480880
    invoke-static {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    .line 2480881
    :cond_0
    :goto_0
    return-void

    .line 2480882
    :cond_1
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->A:LX/HYf;

    sget-object v1, LX/HYf;->AUTH_FAILED:LX/HYf;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->A:LX/HYf;

    sget-object v1, LX/HYf;->TIMEOUT:LX/HYf;

    if-ne v0, v1, :cond_0

    .line 2480883
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->C:Z

    if-eqz v0, :cond_3

    .line 2480884
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->q:LX/GvB;

    invoke-virtual {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/GvB;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2480885
    :cond_3
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->n()V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2480865
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2480866
    invoke-static {p0, p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2480867
    const v0, 0x7f0311b9

    invoke-virtual {p0, v0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->setContentView(I)V

    .line 2480868
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2480869
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->w:LX/0h5;

    .line 2480870
    iget-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->w:LX/0h5;

    const v1, 0x7f0800ef

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2480871
    invoke-virtual {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2480872
    if-eqz v0, :cond_0

    .line 2480873
    const-string v1, "extra_uid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->x:Ljava/lang/String;

    .line 2480874
    const-string v1, "extra_pwd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->y:Ljava/lang/String;

    .line 2480875
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->x:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/registration/activity/RegistrationLoginActivity;->y:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2480876
    invoke-static {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->a(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    .line 2480877
    :goto_0
    return-void

    .line 2480878
    :cond_1
    invoke-static {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->b$redex0(Lcom/facebook/registration/activity/RegistrationLoginActivity;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x79eab298

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2480862
    invoke-direct {p0}, Lcom/facebook/registration/activity/RegistrationLoginActivity;->l()V

    .line 2480863
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2480864
    const/16 v1, 0x23

    const v2, 0x60e4e752

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
