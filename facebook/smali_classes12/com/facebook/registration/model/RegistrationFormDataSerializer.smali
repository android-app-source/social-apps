.class public Lcom/facebook/registration/model/RegistrationFormDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/registration/model/RegistrationFormData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2483258
    const-class v0, Lcom/facebook/registration/model/RegistrationFormData;

    new-instance v1, Lcom/facebook/registration/model/RegistrationFormDataSerializer;

    invoke-direct {v1}, Lcom/facebook/registration/model/RegistrationFormDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2483259
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2483257
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/registration/model/RegistrationFormData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2483240
    if-nez p0, :cond_0

    .line 2483241
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2483242
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2483243
    invoke-static {p0, p1, p2}, Lcom/facebook/registration/model/RegistrationFormDataSerializer;->b(Lcom/facebook/registration/model/RegistrationFormData;LX/0nX;LX/0my;)V

    .line 2483244
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2483245
    return-void
.end method

.method private static b(Lcom/facebook/registration/model/RegistrationFormData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2483247
    const-string v0, "first_name"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2483248
    const-string v0, "last_name"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2483249
    const-string v0, "phone_number_input_raw"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneNumberInputRaw()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2483250
    const-string v0, "phone_iso_country_code"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneIsoCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2483251
    const-string v0, "email"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2483252
    const-string v0, "gender"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getGender()LX/F8q;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2483253
    const-string v0, "birthday_year"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayYear()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2483254
    const-string v0, "birthday_month"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayMonth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2483255
    const-string v0, "birthday_day"

    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayDay()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2483256
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2483246
    check-cast p1, Lcom/facebook/registration/model/RegistrationFormData;

    invoke-static {p1, p2, p3}, Lcom/facebook/registration/model/RegistrationFormDataSerializer;->a(Lcom/facebook/registration/model/RegistrationFormData;LX/0nX;LX/0my;)V

    return-void
.end method
