.class public Lcom/facebook/registration/model/ContactPointSuggestions;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/registration/model/ContactPointSuggestionsDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/model/ContactPointSuggestions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final autocompleteContactPoints:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "autocomplete"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/registration/model/ContactPointSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field public final prefillContactPoints:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prefill"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/registration/model/ContactPointSuggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2482966
    const-class v0, Lcom/facebook/registration/model/ContactPointSuggestionsDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2482967
    new-instance v0, LX/HZx;

    invoke-direct {v0}, LX/HZx;-><init>()V

    sput-object v0, Lcom/facebook/registration/model/ContactPointSuggestions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2482968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2482969
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->prefillContactPoints:Ljava/util/List;

    .line 2482970
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->autocompleteContactPoints:Ljava/util/List;

    .line 2482971
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2482972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2482973
    sget-object v0, Lcom/facebook/registration/model/ContactPointSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->prefillContactPoints:Ljava/util/List;

    .line 2482974
    sget-object v0, Lcom/facebook/registration/model/ContactPointSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->autocompleteContactPoints:Ljava/util/List;

    .line 2482975
    return-void
.end method

.method private a(LX/HZz;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HZz;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/registration/model/ContactPointSuggestion;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2482976
    sget-object v0, LX/HZy;->a:[I

    invoke-virtual {p1}, LX/HZz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2482977
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2482978
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->prefillContactPoints:Ljava/util/List;

    goto :goto_0

    .line 2482979
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->autocompleteContactPoints:Ljava/util/List;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;I)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2482980
    invoke-direct {p0, p1}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;)Ljava/util/List;

    move-result-object v1

    .line 2482981
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2482982
    :goto_0
    return-object v0

    .line 2482983
    :cond_1
    const/4 v0, 0x0

    .line 2482984
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestion;

    .line 2482985
    invoke-virtual {v0}, Lcom/facebook/registration/model/ContactPointSuggestion;->a()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v4

    if-ne v4, p2, :cond_4

    .line 2482986
    if-ne v1, p3, :cond_2

    .line 2482987
    invoke-virtual {v0}, Lcom/facebook/registration/model/ContactPointSuggestion;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2482988
    :cond_2
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 2482989
    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 2482990
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HZz;",
            "Lcom/facebook/growth/model/ContactpointType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2482991
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2482992
    invoke-direct {p0, p1}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;)Ljava/util/List;

    move-result-object v0

    .line 2482993
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 2482994
    :goto_0
    return-object v0

    .line 2482995
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestion;

    .line 2482996
    invoke-virtual {v0}, Lcom/facebook/registration/model/ContactPointSuggestion;->a()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v3

    if-ne v3, p2, :cond_2

    .line 2482997
    invoke-virtual {v0}, Lcom/facebook/registration/model/ContactPointSuggestion;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 2482998
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2482999
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2483000
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->prefillContactPoints:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2483001
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestions;->autocompleteContactPoints:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2483002
    return-void
.end method
