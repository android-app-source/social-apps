.class public Lcom/facebook/registration/model/RegistrationFormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/registration/model/RegistrationFormDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/registration/model/RegistrationFormDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/model/RegistrationFormData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/facebook/growth/model/ContactpointType;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;

.field private p:LX/F8q;

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2483116
    const-class v0, Lcom/facebook/registration/model/RegistrationFormDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2483117
    const-class v0, Lcom/facebook/registration/model/RegistrationFormDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2483118
    new-instance v0, LX/Ha1;

    invoke-direct {v0}, LX/Ha1;-><init>()V

    sput-object v0, Lcom/facebook/registration/model/RegistrationFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2483119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483120
    iput-boolean v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    .line 2483121
    iput-boolean v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    .line 2483122
    iput-boolean v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    .line 2483123
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    .line 2483124
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    .line 2483125
    sget-object v0, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    .line 2483126
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    .line 2483127
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    .line 2483128
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    .line 2483129
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    .line 2483130
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    .line 2483131
    iput v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    .line 2483132
    iput v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    .line 2483133
    iput v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    .line 2483134
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    .line 2483135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    .line 2483136
    iput-boolean v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    .line 2483137
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    .line 2483138
    iput-boolean v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    .line 2483139
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2483140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483141
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    .line 2483142
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    .line 2483143
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    .line 2483144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    .line 2483145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    .line 2483146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/growth/model/ContactpointType;->valueOf(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    .line 2483147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    .line 2483148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    .line 2483149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    .line 2483150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    .line 2483151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    .line 2483152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    .line 2483153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    .line 2483154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    .line 2483155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    .line 2483156
    const-class v0, LX/F8q;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F8q;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    .line 2483157
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    .line 2483158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    .line 2483159
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    .line 2483160
    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 0

    .prologue
    .line 2483161
    iput p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    .line 2483162
    iput p2, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    .line 2483163
    iput p3, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    .line 2483164
    return-void
.end method

.method public final a(Lcom/facebook/growth/model/ContactpointType;)V
    .locals 0

    .prologue
    .line 2483165
    if-nez p1, :cond_0

    sget-object p1, Lcom/facebook/growth/model/ContactpointType;->UNKNOWN:Lcom/facebook/growth/model/ContactpointType;

    :cond_0
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    .line 2483166
    return-void
.end method

.method public final a(Lcom/facebook/registration/model/RegistrationFormData;)V
    .locals 1

    .prologue
    .line 2483167
    iget-boolean v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    .line 2483168
    iget-boolean v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    .line 2483169
    iget-boolean v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    .line 2483170
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    .line 2483171
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    .line 2483172
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    .line 2483173
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    .line 2483174
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    .line 2483175
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    .line 2483176
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    .line 2483177
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    .line 2483178
    iget v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    .line 2483179
    iget v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    .line 2483180
    iget v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    iput v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    .line 2483181
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    .line 2483182
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    .line 2483183
    iget-boolean v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    .line 2483184
    iget-object v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    .line 2483185
    iget-boolean v0, p1, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    iput-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    .line 2483186
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2483187
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    .line 2483188
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2483189
    iput-boolean p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    .line 2483190
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2483191
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2483192
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    .line 2483193
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2483195
    iput-boolean p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    .line 2483196
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2483210
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2483208
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    .line 2483209
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 2483206
    iput-boolean p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    .line 2483207
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2483205
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    return v0
.end method

.method public final d()Lcom/facebook/growth/model/ContactpointType;
    .locals 1

    .prologue
    .line 2483204
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2483202
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    .line 2483203
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 2483200
    iput-boolean p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    .line 2483201
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2483194
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2483199
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 2483197
    iput-boolean p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    .line 2483198
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2483115
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/util/Date;
    .locals 4

    .prologue
    .line 2483112
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    if-nez v0, :cond_0

    .line 2483113
    const/4 v0, 0x0

    .line 2483114
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/GregorianCalendar;

    iget v1, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    iget v2, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    iget v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    invoke-direct {v0, v1, v2, v3}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public getBirthdayDay()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_day"
    .end annotation

    .prologue
    .line 2483050
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    return v0
.end method

.method public getBirthdayMonth()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_month"
    .end annotation

    .prologue
    .line 2483049
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    return v0
.end method

.method public getBirthdayYear()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_year"
    .end annotation

    .prologue
    .line 2483048
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "email"
    .end annotation

    .prologue
    .line 2483039
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_name"
    .end annotation

    .prologue
    .line 2483047
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()LX/F8q;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gender"
    .end annotation

    .prologue
    .line 2483046
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_name"
    .end annotation

    .prologue
    .line 2483045
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneIsoCountryCode()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phone_iso_country_code"
    .end annotation

    .prologue
    .line 2483044
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumberInputRaw()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phone_number_input_raw"
    .end annotation

    .prologue
    .line 2483043
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2483042
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 2483041
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2483040
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2483051
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2483052
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2483053
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "phone"

    iget-object v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483054
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2483055
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "email"

    iget-object v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483056
    :cond_1
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2483057
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "firstname"

    iget-object v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483058
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "lastname"

    iget-object v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483059
    :cond_3
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    if-eqz v0, :cond_4

    .line 2483060
    sget-object v0, LX/Ha2;->a:[I

    iget-object v2, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    invoke-virtual {v2}, LX/F8q;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2483061
    const-string v0, "U"

    .line 2483062
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "gender"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483063
    :cond_4
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2483064
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "password"

    iget-object v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483065
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2483066
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2483067
    invoke-virtual {p0}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2483068
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "birthday"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483069
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    if-eqz v0, :cond_7

    .line 2483070
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ignore_suma_check"

    iget-boolean v3, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483071
    :cond_7
    return-object v1

    .line 2483072
    :pswitch_0
    const-string v0, "M"

    goto :goto_0

    .line 2483073
    :pswitch_1
    const-string v0, "F"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setBirthdayDay(I)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_day"
    .end annotation

    .prologue
    .line 2483074
    iput p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    .line 2483075
    return-void
.end method

.method public setBirthdayMonth(I)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_month"
    .end annotation

    .prologue
    .line 2483076
    iput p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    .line 2483077
    return-void
.end method

.method public setBirthdayYear(I)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthday_year"
    .end annotation

    .prologue
    .line 2483078
    iput p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    .line 2483079
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "email"
    .end annotation

    .prologue
    .line 2483080
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    .line 2483081
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_name"
    .end annotation

    .prologue
    .line 2483082
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    .line 2483083
    return-void
.end method

.method public setGender(LX/F8q;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gender"
    .end annotation

    .prologue
    .line 2483084
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    .line 2483085
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_name"
    .end annotation

    .prologue
    .line 2483086
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    .line 2483087
    return-void
.end method

.method public setPhoneIsoCountryCode(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phone_iso_country_code"
    .end annotation

    .prologue
    .line 2483088
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    .line 2483089
    return-void
.end method

.method public setPhoneNumberInputRaw(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phone_number_input_raw"
    .end annotation

    .prologue
    .line 2483090
    iput-object p1, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    .line 2483091
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2483092
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483093
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483094
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483095
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483096
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483097
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->f:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483098
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483099
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483100
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483101
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483102
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483103
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2483104
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2483105
    iget v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2483106
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483107
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->p:LX/F8q;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2483108
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->q:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483109
    iget-object v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2483110
    iget-boolean v0, p0, Lcom/facebook/registration/model/RegistrationFormData;->s:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2483111
    return-void
.end method
