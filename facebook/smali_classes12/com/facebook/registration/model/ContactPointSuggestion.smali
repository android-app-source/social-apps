.class public Lcom/facebook/registration/model/ContactPointSuggestion;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/registration/model/ContactPointSuggestionDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/model/ContactPointSuggestion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contactPoint:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value"
    .end annotation
.end field

.field private contactPointTypeString:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2482933
    const-class v0, Lcom/facebook/registration/model/ContactPointSuggestionDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2482932
    new-instance v0, LX/HZw;

    invoke-direct {v0}, LX/HZw;-><init>()V

    sput-object v0, Lcom/facebook/registration/model/ContactPointSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2482928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2482929
    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPointTypeString:Ljava/lang/String;

    .line 2482930
    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPoint:Ljava/lang/String;

    .line 2482931
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2482918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2482919
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPointTypeString:Ljava/lang/String;

    .line 2482920
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPoint:Ljava/lang/String;

    .line 2482921
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/growth/model/ContactpointType;
    .locals 2

    .prologue
    .line 2482927
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPointTypeString:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/growth/model/ContactpointType;->fromString(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482926
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPoint:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2482925
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2482922
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPointTypeString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2482923
    iget-object v0, p0, Lcom/facebook/registration/model/ContactPointSuggestion;->contactPoint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2482924
    return-void
.end method
