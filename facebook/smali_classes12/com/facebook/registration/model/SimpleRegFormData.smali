.class public Lcom/facebook/registration/model/SimpleRegFormData;
.super Lcom/facebook/registration/model/RegistrationFormData;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/registration/model/SimpleRegFormData;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/growth/model/DeviceOwnerData;

.field private b:Lcom/facebook/registration/model/ContactPointSuggestions;

.field public c:LX/HYi;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/HYi;",
            "LX/Ha0;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2483297
    new-instance v0, LX/Ha3;

    invoke-direct {v0}, LX/Ha3;-><init>()V

    sput-object v0, Lcom/facebook/registration/model/SimpleRegFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2483298
    invoke-direct {p0}, Lcom/facebook/registration/model/RegistrationFormData;-><init>()V

    .line 2483299
    iput-object v1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2483300
    iput-object v1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    .line 2483301
    iput-object v1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2483302
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    .line 2483303
    iput-object v1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2483304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    .line 2483305
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2483306
    invoke-direct {p0, p1}, Lcom/facebook/registration/model/RegistrationFormData;-><init>(Landroid/os/Parcel;)V

    .line 2483307
    const-class v0, Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/DeviceOwnerData;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2483308
    const-class v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    .line 2483309
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/HYi;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2483310
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    .line 2483311
    const-class v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2483312
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    .line 2483313
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;
    .locals 3

    .prologue
    .line 2483314
    const-class v1, Lcom/facebook/registration/model/SimpleRegFormData;

    monitor-enter v1

    .line 2483315
    :try_start_0
    sget-object v0, Lcom/facebook/registration/model/SimpleRegFormData;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2483316
    sput-object v2, Lcom/facebook/registration/model/SimpleRegFormData;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2483317
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483318
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2483319
    new-instance v0, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-direct {v0}, Lcom/facebook/registration/model/SimpleRegFormData;-><init>()V

    .line 2483320
    move-object v0, v0

    .line 2483321
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2483322
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/registration/model/SimpleRegFormData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2483323
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2483324
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/HYi;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2483325
    new-instance v0, LX/Ha0;

    invoke-direct {v0, p2, p3}, LX/Ha0;-><init>(ILjava/lang/String;)V

    .line 2483326
    iget-object p2, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2483327
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/growth/model/DeviceOwnerData;)V
    .locals 1

    .prologue
    .line 2483331
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2483332
    monitor-exit p0

    return-void

    .line 2483333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/registration/model/ContactPointSuggestions;)V
    .locals 1

    .prologue
    .line 2483328
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2483329
    monitor-exit p0

    return-void

    .line 2483330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/registration/model/SimpleRegFormData;)V
    .locals 1

    .prologue
    .line 2483338
    invoke-super {p0, p1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/registration/model/RegistrationFormData;)V

    .line 2483339
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2483340
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    .line 2483341
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2483342
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    .line 2483343
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2483344
    iget-object v0, p1, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    .line 2483345
    return-void
.end method

.method public final b(LX/HYi;)V
    .locals 1

    .prologue
    .line 2483334
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2483335
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    if-ne v0, p1, :cond_0

    .line 2483336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2483337
    :cond_0
    return-void
.end method

.method public final c(LX/HYi;)I
    .locals 1

    .prologue
    .line 2483293
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ha0;

    .line 2483294
    if-nez v0, :cond_0

    .line 2483295
    const/4 v0, -0x1

    .line 2483296
    :goto_0
    return v0

    :cond_0
    iget v0, v0, LX/Ha0;->code:I

    goto :goto_0
.end method

.method public final d(LX/HYi;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2483289
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ha0;

    .line 2483290
    if-nez v0, :cond_0

    .line 2483291
    const/4 v0, 0x0

    .line 2483292
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LX/Ha0;->message:Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2483264
    const/4 v0, 0x0

    return v0
.end method

.method public final e(LX/HYi;)Z
    .locals 1

    .prologue
    .line 2483288
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized l()Lcom/facebook/growth/model/DeviceOwnerData;
    .locals 1

    .prologue
    .line 2483284
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    if-nez v0, :cond_0

    .line 2483285
    new-instance v0, Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-direct {v0}, Lcom/facebook/growth/model/DeviceOwnerData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2483286
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2483287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Lcom/facebook/registration/model/ContactPointSuggestions;
    .locals 1

    .prologue
    .line 2483280
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    if-nez v0, :cond_0

    .line 2483281
    new-instance v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    invoke-direct {v0}, Lcom/facebook/registration/model/ContactPointSuggestions;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2483282
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2483283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized n()Z
    .locals 1

    .prologue
    .line 2483279
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2483277
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/model/SimpleRegFormData;->b(LX/HYi;)V

    .line 2483278
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2483274
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2483275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    .line 2483276
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2483273
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2483265
    invoke-super {p0, p1, p2}, Lcom/facebook/registration/model/RegistrationFormData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2483266
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->a:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2483267
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->b:Lcom/facebook/registration/model/ContactPointSuggestions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2483268
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2483269
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->d:Ljava/util/Map;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2483270
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2483271
    iget-object v0, p0, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2483272
    return-void
.end method
