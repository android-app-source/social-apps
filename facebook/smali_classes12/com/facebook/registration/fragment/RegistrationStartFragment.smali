.class public Lcom/facebook/registration/fragment/RegistrationStartFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field public b:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482629
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/registration/fragment/RegistrationStartFragment;)V
    .locals 0

    .prologue
    .line 2482628
    invoke-super {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->p()V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/registration/fragment/RegistrationStartFragment;)V
    .locals 0

    .prologue
    .line 2482627
    invoke-super {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->p()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2482624
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/os/Bundle;)V

    .line 2482625
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;

    invoke-static {p1}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v2

    check-cast v2, LX/HaQ;

    invoke-static {p1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    const/16 v0, 0x1430

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->b:LX/HaQ;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->d:LX/0Ot;

    .line 2482626
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2482623
    const v0, 0x7f08356d

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2482622
    const v0, 0x7f0311c1

    return v0
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 2482630
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482621
    const v0, 0x7f08356b

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2482620
    const v0, 0x7f08356c

    return v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2482618
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Z)V

    .line 2482619
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2482617
    sget-object v0, LX/HYj;->START_COMPLETED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2482616
    sget-object v0, LX/HYi;->START:LX/HYi;

    return-object v0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2482614
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->b:LX/HaQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HaQ;->a(Landroid/app/Activity;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/HZo;

    invoke-direct {v2, p0}, LX/HZo;-><init>(Lcom/facebook/registration/fragment/RegistrationStartFragment;)V

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationStartFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2482615
    return-void
.end method
