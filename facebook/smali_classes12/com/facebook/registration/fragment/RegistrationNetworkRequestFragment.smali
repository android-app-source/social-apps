.class public abstract Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field public h:I

.field public i:Z

.field public j:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/HaJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2481827
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    .line 2481828
    iput v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->h:I

    .line 2481829
    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->i:Z

    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 2481812
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2481813
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;Z)V

    .line 2481814
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481815
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v3, 0x37

    const/4 v0, 0x0

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 2481816
    if-eqz v2, :cond_0

    .line 2481817
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0835c1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2481818
    new-instance v3, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2481819
    const-string v4, "[[check_internet_connection]]"

    invoke-static {v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2481820
    const-string v4, "[[check_internet_connection]]"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0835c2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->r:LX/HaJ;

    sget-object v0, LX/HaH;->DATA_USAGE:LX/HaH;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object p2

    const/16 v0, 0x21

    invoke-virtual {v3, v4, p1, p2, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2481821
    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    .line 2481822
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481823
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481824
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->s:LX/8tu;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2481825
    :goto_0
    return-void

    .line 2481826
    :cond_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2481761
    if-eqz p1, :cond_1

    .line 2481762
    invoke-direct {p0, v1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b(Z)V

    .line 2481763
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2481764
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2481765
    iput-boolean v2, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->i:Z

    .line 2481766
    iget v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->h:I

    if-lez v0, :cond_0

    .line 2481767
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2481768
    :goto_0
    return-void

    .line 2481769
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2481770
    :cond_1
    invoke-direct {p0, v2}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b(Z)V

    .line 2481771
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2481772
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2481773
    iput-boolean v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->i:Z

    goto :goto_0
.end method

.method private static b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2481830
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2481831
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2481832
    :goto_0
    return-object v0

    .line 2481833
    :cond_0
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v0, v2

    .line 2481834
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2481835
    goto :goto_0

    .line 2481836
    :cond_1
    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2481837
    instance-of v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 2481838
    goto :goto_0

    .line 2481839
    :cond_2
    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 2481853
    if-eqz p1, :cond_0

    .line 2481854
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2481855
    :goto_0
    return-void

    .line 2481856
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public static q(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V
    .locals 1

    .prologue
    .line 2481840
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;Z)V

    .line 2481841
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->m()V

    .line 2481842
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;)LX/Ha0;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2481843
    invoke-static {p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 2481844
    if-nez v1, :cond_0

    .line 2481845
    const/4 v0, 0x0

    .line 2481846
    :goto_0
    return-object v0

    .line 2481847
    :cond_0
    new-instance v0, LX/HZg;

    invoke-direct {v0, p0}, LX/HZg;-><init>(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V

    invoke-virtual {p0, v0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(LX/266;Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2481848
    if-eqz v0, :cond_1

    const-string v2, "error_message"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2481849
    const-string v2, "error_message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2481850
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    .line 2481851
    new-instance v1, LX/Ha0;

    invoke-direct {v1, v2, v0}, LX/Ha0;-><init>(ILjava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 2481852
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/266;Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/266",
            "<TT;>;",
            "Lcom/facebook/fbservice/service/ServiceException;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2481801
    invoke-static {p2}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2481802
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2481803
    :goto_0
    return-object v0

    .line 2481804
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v2

    .line 2481805
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2481806
    goto :goto_0

    .line 2481807
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-virtual {v0, v2, p1}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2481808
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2481809
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2481810
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v5

    check-cast v5, LX/16I;

    const/16 v6, 0x2ba

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const-class p1, LX/HaJ;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/HaJ;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v0

    check-cast v0, LX/8tu;

    iput-object v3, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->m:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->n:LX/0kb;

    iput-object v5, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->o:LX/16I;

    iput-object v6, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->p:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->r:LX/HaJ;

    iput-object v0, v2, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->s:LX/8tu;

    .line 2481811
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2481785
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2481786
    const v0, 0x7f0d0555

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b:Landroid/widget/TextView;

    .line 2481787
    const v0, 0x7f0d06e3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->c:Landroid/widget/TextView;

    .line 2481788
    const v0, 0x7f0d29a9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->e:Landroid/widget/TextView;

    .line 2481789
    const v0, 0x7f0d0ab3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->f:Landroid/view/View;

    .line 2481790
    const v0, 0x7f0d29aa

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->d:Landroid/widget/TextView;

    .line 2481791
    const v0, 0x7f0d29a8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->g:Landroid/view/View;

    .line 2481792
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2481793
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->d:Landroid/widget/TextView;

    .line 2481794
    const v1, 0x7f08359d

    move v1, v1

    .line 2481795
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2481796
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->d:Landroid/widget/TextView;

    new-instance v1, LX/HZe;

    invoke-direct {v1, p0}, LX/HZe;-><init>(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481797
    const v0, 0x7f0d06e5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/HZf;

    invoke-direct {v1, p0}, LX/HZf;-><init>(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481798
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->o:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance p1, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment$4;

    invoke-direct {p1, p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment$4;-><init>(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V

    invoke-virtual {v0, v1, p1}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->j:LX/0Yb;

    .line 2481799
    invoke-static {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q(Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;)V

    .line 2481800
    return-void
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2481784
    const v0, 0x7f0311bc

    return v0
.end method

.method public abstract m()V
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2481780
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->n:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2481781
    const v0, 0x7f08003f

    const v1, 0x7f08003a

    invoke-direct {p0, v0, v1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(II)V

    .line 2481782
    :goto_0
    return-void

    .line 2481783
    :cond_0
    const v0, 0x7f08003c

    const v1, 0x7f08003a

    invoke-direct {p0, v0, v1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(II)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2035c4a7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2481774
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->m:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2481775
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->b(Z)V

    .line 2481776
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->j:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2481777
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->j:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2481778
    :cond_0
    invoke-super {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;->onDestroyView()V

    .line 2481779
    const/16 v1, 0x2b

    const v2, 0x602e4fec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
