.class public Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field public b:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HaJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Landroid/widget/EditText;

.field public n:Landroid/widget/DatePicker;

.field public o:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2481641
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    .line 2481642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->o:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2481639
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 2481640
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2481636
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/os/Bundle;)V

    .line 2481637
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-static {v3}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v2

    check-cast v2, LX/8tu;

    const/16 p1, 0x1617

    invoke-static {v3, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    const-class v0, LX/HaJ;

    invoke-interface {v3, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HaJ;

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->b:LX/8tu;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->c:LX/0Or;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->d:LX/HaJ;

    .line 2481638
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 2481607
    const v0, 0x7f0d2a99

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->m:Landroid/widget/EditText;

    .line 2481608
    const v0, 0x7f0d2980

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->n:Landroid/widget/DatePicker;

    .line 2481609
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2481610
    new-instance v1, LX/47x;

    invoke-direct {v1, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2481611
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->e()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "[[birthday_help]]"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2481612
    const-string v2, "[[birthday_help]]"

    const v3, 0x7f08002c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->d:LX/HaJ;

    sget-object v4, LX/HaH;->BROWSER:LX/HaH;

    const-string p1, "http://m.facebook.com/birthday_help.php"

    invoke-virtual {v3, v4, p1}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v0, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2481613
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481614
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481615
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->b:LX/8tu;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2481616
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2481617
    new-instance v1, Lcom/facebook/growth/model/Birthday;

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayYear()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v3}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayMonth()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v4}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayDay()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/growth/model/Birthday;-><init>(III)V

    .line 2481618
    :goto_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->n:Landroid/widget/DatePicker;

    if-eqz v2, :cond_2

    .line 2481619
    iget-object v5, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->n:Landroid/widget/DatePicker;

    iget v6, v1, Lcom/facebook/growth/model/Birthday;->c:I

    iget v7, v1, Lcom/facebook/growth/model/Birthday;->b:I

    iget v8, v1, Lcom/facebook/growth/model/Birthday;->a:I

    new-instance v9, LX/HZ5;

    invoke-direct {v9, p0}, LX/HZ5;-><init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;)V

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 2481620
    iget-boolean v5, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->o:Z

    .line 2481621
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->n:Landroid/widget/DatePicker;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 2481622
    iput-boolean v5, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->o:Z

    .line 2481623
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget v3, v1, Lcom/facebook/growth/model/Birthday;->c:I

    iget v4, v1, Lcom/facebook/growth/model/Birthday;->b:I

    iget v1, v1, Lcom/facebook/growth/model/Birthday;->a:I

    invoke-virtual {v2, v3, v4, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(III)V

    .line 2481624
    :goto_1
    return-void

    .line 2481625
    :cond_0
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->n:Landroid/widget/DatePicker;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->o:Z

    .line 2481626
    const/4 v3, 0x1

    .line 2481627
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2481628
    const/16 v2, -0x12

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->add(II)V

    .line 2481629
    new-instance v2, Lcom/facebook/growth/model/Birthday;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-direct {v2, v3, v4, v1}, Lcom/facebook/growth/model/Birthday;-><init>(III)V

    move-object v1, v2

    .line 2481630
    goto :goto_0

    .line 2481631
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 2481632
    :cond_2
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2481633
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->m:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v3}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->a$redex0(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2481634
    :cond_3
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->m:Landroid/widget/EditText;

    new-instance v3, LX/HZ8;

    invoke-direct {v3, p0, v1}, LX/HZ8;-><init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;Lcom/facebook/growth/model/Birthday;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481635
    goto :goto_1
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2481606
    const v0, 0x7f083586

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2481590
    const v0, 0x7f0311ad

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2481603
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2481604
    new-instance v0, LX/HZb;

    const v1, 0x7f083585

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2481605
    :cond_0
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2481602
    const v0, 0x7f083582

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2481601
    const v0, 0x7f083583

    return v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2481600
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2481599
    sget-object v0, LX/HYj;->BIRTHDAY_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2481598
    sget-object v0, LX/HYi;->BIRTHDAY:LX/HYi;

    return-object v0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2481591
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->o:Z

    if-eqz v0, :cond_0

    .line 2481592
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2481593
    const v0, 0x7f083588

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->g()Ljava/util/Date;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;->a$redex0(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2481594
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083587

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080020

    new-instance v2, LX/HZ4;

    invoke-direct {v2, p0}, LX/HZ4;-><init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080021

    new-instance v2, LX/HZ3;

    invoke-direct {v2, p0}, LX/HZ3;-><init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/HZ2;

    invoke-direct {v1, p0}, LX/HZ2;-><init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2481595
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2481596
    :goto_0
    return-void

    .line 2481597
    :cond_0
    invoke-super {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->p()V

    goto :goto_0
.end method
