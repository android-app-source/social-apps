.class public Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public b:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481649
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2481650
    const v0, 0x7f0d2985

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->f:Landroid/widget/Button;

    .line 2481651
    const v0, 0x7f0d2986

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->g:Landroid/widget/TextView;

    .line 2481652
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->f:Landroid/widget/Button;

    new-instance v1, LX/HZ9;

    invoke-direct {v1, p0}, LX/HZ9;-><init>(Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481653
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->g:Landroid/widget/TextView;

    new-instance v1, LX/HZA;

    invoke-direct {v1, p0}, LX/HZA;-><init>(Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481654
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->b:LX/HaQ;

    const v0, 0x7f0d2982

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, LX/HaQ;->a(Landroid/widget/TextView;)V

    .line 2481655
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->b:LX/HaQ;

    const v0, 0x7f0d2984

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2481656
    iget-object v2, v1, LX/HaQ;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2481657
    new-instance v3, LX/47x;

    invoke-direct {v3, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2481658
    const v4, 0x7f0835ad

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "[[learn_more]]"

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2481659
    const-string v4, "[[learn_more]]"

    const v5, 0x7f08002c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, v1, LX/HaQ;->c:LX/HaJ;

    sget-object p0, LX/HaH;->FRIEND_FINDER_LEARN_MORE:LX/HaH;

    const/4 p1, 0x0

    invoke-virtual {v5, p0, p1}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object v5

    const/16 p0, 0x21

    invoke-virtual {v3, v4, v2, v5, p0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2481660
    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    .line 2481661
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481662
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481663
    iget-object v2, v1, LX/HaQ;->b:LX/8tu;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2481664
    return-void
.end method

.method public static a$redex0(Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;Z)V
    .locals 4

    .prologue
    .line 2481665
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->b(Z)V

    .line 2481666
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, p1}, Lcom/facebook/registration/model/RegistrationFormData;->c(Z)V

    .line 2481667
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->d:LX/HZt;

    .line 2481668
    iget-object v1, v0, LX/HZt;->a:LX/0Zb;

    sget-object v2, LX/HZu;->REGISTRATION_CONTACTS_TERMS_ACCEPT:LX/HZu;

    invoke-static {v0, v2}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "accept"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481669
    sget-object v0, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2481670
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2481671
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2481672
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;

    invoke-static {p1}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v2

    check-cast v2, LX/HaQ;

    invoke-static {p1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p1}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v4

    check-cast v4, LX/HZt;

    const/16 v0, 0x1430

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->b:LX/HaQ;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v4, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->d:LX/HZt;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->e:LX/0Ot;

    .line 2481673
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2481674
    invoke-direct {p0, p1}, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->a(Landroid/view/View;)V

    .line 2481675
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2481676
    const v0, 0x7f0311b8

    return v0
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2481677
    const v0, 0x7f0835a2

    return v0
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2481678
    const v0, 0x7f0311ae

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 2481679
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2481680
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2481681
    check-cast v0, Landroid/view/ViewGroup;

    .line 2481682
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 2481683
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2481684
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->d()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2481685
    const v0, 0x7f0d299f

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2481686
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->lQ_()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2481687
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2481688
    invoke-direct {p0, v1}, Lcom/facebook/registration/fragment/RegistrationContactsTermsFragment;->a(Landroid/view/View;)V

    .line 2481689
    return-void
.end method
