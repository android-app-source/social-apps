.class public Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;
.super Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;
.source ""


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482413
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HYh;)V
    .locals 2

    .prologue
    .line 2482414
    sget-object v0, LX/HYh;->ADDED:LX/HYh;

    invoke-virtual {p1, v0}, LX/HYh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2482415
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    .line 2482416
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setEmail(Ljava/lang/String;)V

    .line 2482417
    sget-object v0, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    move-object v0, v0

    .line 2482418
    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482419
    :goto_0
    return-void

    .line 2482420
    :cond_0
    sget-object v0, LX/HYj;->PREFILL_EMAIL_UNFINISHED:LX/HYj;

    move-object v0, v0

    .line 2482421
    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2482422
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(Landroid/os/Bundle;)V

    .line 2482423
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2482424
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482425
    const v0, 0x7f083576

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final lN_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482426
    const v0, 0x7f0835b7

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
