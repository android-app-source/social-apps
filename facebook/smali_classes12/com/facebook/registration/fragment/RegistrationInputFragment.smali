.class public abstract Lcom/facebook/registration/fragment/RegistrationInputFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public e:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Z

.field public i:Landroid/widget/TextView;

.field public j:Lcom/facebook/resources/ui/FbButton;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481507
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    .line 2481508
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 2481509
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2481510
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2481511
    if-eqz p2, :cond_0

    .line 2481512
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2481513
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2481514
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2481515
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2481516
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2481517
    instance-of v1, v0, Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_0

    .line 2481518
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2481519
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481520
    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2481521
    :goto_0
    return-void

    .line 2481522
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/registration/fragment/RegistrationInputFragment;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v2

    check-cast v2, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object p0

    check-cast p0, LX/HZt;

    iput-object v1, p1, Lcom/facebook/registration/fragment/RegistrationInputFragment;->e:LX/0Uh;

    iput-object v2, p1, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object p0, p1, Lcom/facebook/registration/fragment/RegistrationInputFragment;->g:LX/HZt;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/registration/fragment/RegistrationInputFragment;Z)V
    .locals 3

    .prologue
    .line 2481523
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->r()[Landroid/widget/EditText;

    move-result-object v1

    .line 2481524
    if-eqz v1, :cond_0

    array-length v0, v1

    if-nez v0, :cond_2

    .line 2481525
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2481526
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-static {v0, v1, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/view/View;Landroid/view/View;Z)V

    .line 2481527
    :cond_1
    return-void

    .line 2481528
    :cond_2
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2481529
    aget-object v2, v1, v0

    if-eqz v2, :cond_3

    .line 2481530
    aget-object v2, v1, v0

    invoke-virtual {v2}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2481531
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/registration/fragment/RegistrationInputFragment;Z)V
    .locals 4

    .prologue
    .line 2481581
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2481582
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-static {v0, v1, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/view/View;Landroid/view/View;Z)V

    .line 2481583
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->r()[Landroid/widget/EditText;

    move-result-object v1

    .line 2481584
    if-eqz v1, :cond_0

    array-length v0, v1

    if-nez v0, :cond_1

    .line 2481585
    :cond_0
    return-void

    .line 2481586
    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2481587
    aget-object v2, v1, v0

    if-eqz v2, :cond_2

    .line 2481588
    aget-object v2, v1, v0

    invoke-virtual {v2}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0a00d3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2481589
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/HYj;)V
    .locals 1

    .prologue
    .line 2481532
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2481533
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2481534
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2481535
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2481536
    const-class v0, Lcom/facebook/registration/fragment/RegistrationInputFragment;

    invoke-static {v0, p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2481537
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->e:LX/0Uh;

    const/16 v1, 0x33

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 2481538
    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    .line 2481539
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2481540
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2481541
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2481542
    const v0, 0x7f0d299c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2481543
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2481544
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2481545
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->q()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2481546
    const v0, 0x7f0d299e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2481547
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2481548
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2481549
    :cond_1
    const v0, 0x7f0d0b0e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    .line 2481550
    const v0, 0x7f0d2985

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->j:Lcom/facebook/resources/ui/FbButton;

    .line 2481551
    const v0, 0x7f0d2988

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    .line 2481552
    const v0, 0x7f0d090e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->l:Landroid/widget/TextView;

    .line 2481553
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->lP_()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2481554
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->lP_()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481555
    const/4 p2, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x3

    .line 2481556
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->e()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2481557
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->e()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2481558
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481559
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481560
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->o()LX/HYi;

    move-result-object v2

    .line 2481561
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, v2}, Lcom/facebook/registration/model/SimpleRegFormData;->e(LX/HYi;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, v2}, Lcom/facebook/registration/model/SimpleRegFormData;->d(LX/HYi;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2481562
    const/4 v0, 0x1

    .line 2481563
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v3, v2}, Lcom/facebook/registration/model/SimpleRegFormData;->d(LX/HYi;)Ljava/lang/String;

    move-result-object v2

    .line 2481564
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481565
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481566
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    packed-switch v2, :pswitch_data_0

    .line 2481567
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMinLines(I)V

    .line 2481568
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMinLines(I)V

    .line 2481569
    :goto_1
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    new-instance v3, LX/HZZ;

    invoke-direct {v3, p0}, LX/HZZ;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481570
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    new-instance v3, LX/HZa;

    invoke-direct {v3, p0}, LX/HZa;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481571
    if-eqz v0, :cond_3

    .line 2481572
    invoke-static {p0, v1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->b$redex0(Lcom/facebook/registration/fragment/RegistrationInputFragment;Z)V

    .line 2481573
    :goto_2
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->j:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HZX;

    invoke-direct {v1, p0}, LX/HZX;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481574
    invoke-virtual {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/view/View;)V

    .line 2481575
    return-void

    .line 2481576
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setMinLines(I)V

    .line 2481577
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setMinLines(I)V

    goto :goto_1

    .line 2481578
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMinLines(I)V

    .line 2481579
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMinLines(I)V

    goto :goto_1

    .line 2481580
    :cond_3
    invoke-static {p0, v1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a$redex0(Lcom/facebook/registration/fragment/RegistrationInputFragment;Z)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 2481505
    new-instance v0, LX/HZY;

    invoke-direct {v0, p0}, LX/HZY;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2481506
    return-void
.end method

.method public abstract e()I
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public abstract lP_()I
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2481491
    const v0, 0x7f0311b7

    return v0
.end method

.method public abstract m()V
.end method

.method public abstract n()LX/HYj;
.end method

.method public abstract o()LX/HYi;
.end method

.method public p()V
    .locals 2

    .prologue
    .line 2481492
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->l()V

    .line 2481493
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->m()V

    .line 2481494
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->s()V

    .line 2481495
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->n()LX/HYj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(LX/HYj;)V
    :try_end_0
    .catch LX/HZb; {:try_start_0 .. :try_end_0} :catch_0

    .line 2481496
    :goto_0
    return-void

    .line 2481497
    :catch_0
    move-exception v0

    .line 2481498
    invoke-virtual {v0}, LX/HZb;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 2481499
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481500
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2481501
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->b$redex0(Lcom/facebook/registration/fragment/RegistrationInputFragment;Z)V

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 2481504
    const/4 v0, 0x0

    return v0
.end method

.method public r()[Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2481502
    const/4 v0, 0x0

    return-object v0
.end method

.method public s()V
    .locals 0

    .prologue
    .line 2481503
    return-void
.end method
