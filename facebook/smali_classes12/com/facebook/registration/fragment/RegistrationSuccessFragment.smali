.class public Lcom/facebook/registration/fragment/RegistrationSuccessFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/8YK;

.field public g:Landroid/os/CountDownTimer;

.field public h:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482689
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2482664
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2482665
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v2

    check-cast v2, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v3

    check-cast v3, LX/GvB;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->c:LX/GvB;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->e:LX/0Uh;

    .line 2482666
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2482684
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2482685
    if-eqz v0, :cond_0

    .line 2482686
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2482687
    :cond_0
    const v0, 0x7f0d298a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2482688
    return-void
.end method

.method public final c_()V
    .locals 12

    .prologue
    .line 2482677
    const-wide/16 v8, 0xfa

    .line 2482678
    new-instance v6, LX/HZq;

    move-object v7, p0

    move-wide v10, v8

    invoke-direct/range {v6 .. v11}, LX/HZq;-><init>(Lcom/facebook/registration/fragment/RegistrationSuccessFragment;JJ)V

    move-object v0, v6

    .line 2482679
    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    .line 2482680
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    new-instance v1, LX/8YL;

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    invoke-direct {v1, v2, v3, v4, v5}, LX/8YL;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->f:LX/8YK;

    .line 2482681
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->f:LX/8YK;

    new-instance v1, LX/HZp;

    invoke-direct {v1, p0}, LX/HZp;-><init>(Lcom/facebook/registration/fragment/RegistrationSuccessFragment;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    .line 2482682
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->f:LX/8YK;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 2482683
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482676
    const/4 v0, 0x0

    return v0
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2482675
    const v0, 0x7f0311b0

    return v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x32381347    # -4.1927248E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2482667
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_0

    .line 2482668
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 2482669
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->g:Landroid/os/CountDownTimer;

    .line 2482670
    :cond_0
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->f:LX/8YK;

    if-eqz v1, :cond_1

    .line 2482671
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->f:LX/8YK;

    .line 2482672
    iget-object v2, v1, LX/8YK;->n:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 2482673
    :cond_1
    invoke-super {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;->onDestroyView()V

    .line 2482674
    const/16 v1, 0x2b

    const v2, -0x406ff55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
