.class public abstract Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field public d:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/widget/TextView;

.field public h:Ljava/lang/String;

.field public i:Landroid/widget/Button;

.field public j:Landroid/widget/RadioGroup;

.field public k:Landroid/widget/RadioButton;

.field public l:Landroid/widget/RadioButton;

.field public m:Landroid/widget/RadioButton;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481393
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2481394
    if-eqz p1, :cond_0

    .line 2481395
    const v0, 0x7f0835b4    # 1.8105385E38f

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2481396
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0835b5

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/HYh;)V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2481397
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2481398
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;

    invoke-static {p1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v2

    check-cast v2, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p1}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v3

    check-cast v3, LX/HZt;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->e:LX/HZt;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->f:Landroid/content/Context;

    .line 2481399
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2481400
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2481401
    const v0, 0x7f0d090e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->b:Landroid/widget/TextView;

    .line 2481402
    const v0, 0x7f0d2988

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->c:Landroid/widget/TextView;

    .line 2481403
    const v0, 0x7f0d0526

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->g:Landroid/widget/TextView;

    .line 2481404
    const v0, 0x7f0d2985

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->i:Landroid/widget/Button;

    .line 2481405
    const v0, 0x7f0d298e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->j:Landroid/widget/RadioGroup;

    .line 2481406
    const v0, 0x7f0d298f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->k:Landroid/widget/RadioButton;

    .line 2481407
    const v0, 0x7f0d2990

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->l:Landroid/widget/RadioButton;

    .line 2481408
    const v0, 0x7f0d2991

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->m:Landroid/widget/RadioButton;

    .line 2481409
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481410
    iget-object v1, v0, Lcom/facebook/registration/model/SimpleRegFormData;->f:Ljava/util/List;

    move-object v0, v1

    .line 2481411
    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    .line 2481412
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 2481413
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 2481414
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2481415
    sget-object v0, LX/HYh;->NO_GOOGLE_ACCOUNT:LX/HYh;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(LX/HYh;)V

    .line 2481416
    :goto_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->i:Landroid/widget/Button;

    new-instance v1, LX/HZl;

    invoke-direct {v1, p0}, LX/HZl;-><init>(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481417
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->lN_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481418
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->g:Landroid/widget/TextView;

    new-instance v1, LX/HZm;

    invoke-direct {v1, p0}, LX/HZm;-><init>(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2481419
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481420
    return-void

    .line 2481421
    :cond_0
    if-eqz v0, :cond_1

    .line 2481422
    sget-object v0, LX/HYh;->PLAY_SERVICE_NOT_AVAILABLE:LX/HYh;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(LX/HYh;)V

    goto :goto_0

    .line 2481423
    :cond_1
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2481424
    const/4 p1, 0x0

    const/4 v1, 0x1

    .line 2481425
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    const-string p2, "setUpSingleEmailSuggestionView() requires exactly 1 Google account"

    invoke-static {v0, p2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2481426
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->h:Ljava/lang/String;

    .line 2481427
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->b:Landroid/widget/TextView;

    invoke-static {p0, v1}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481428
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->i:Landroid/widget/Button;

    const v1, 0x7f080020

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 2481429
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->j:Landroid/widget/RadioGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 2481430
    goto :goto_0

    .line 2481431
    :cond_2
    const/4 v1, 0x1

    const/4 p2, 0x2

    const/4 v2, 0x0

    .line 2481432
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p2, :cond_4

    move v0, v1

    :goto_2
    const-string p1, "setUpMultipleEmailSuggestionView() requires 2 or more Google accounts"

    invoke-static {v0, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2481433
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->b:Landroid/widget/TextView;

    invoke-static {p0, v2}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481434
    iget-object p1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->k:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2481435
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->l:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2481436
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_5

    .line 2481437
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->m:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->n:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2481438
    :goto_3
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->k:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->toggle()V

    .line 2481439
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->k:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->h:Ljava/lang/String;

    .line 2481440
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->j:Landroid/widget/RadioGroup;

    new-instance v1, LX/HZn;

    invoke-direct {v1, p0}, LX/HZn;-><init>(Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2481441
    goto/16 :goto_0

    :cond_3
    move v0, p1

    .line 2481442
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 2481443
    goto :goto_2

    .line 2481444
    :cond_5
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->m:Landroid/widget/RadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto :goto_3
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract lN_()Ljava/lang/String;
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2481445
    const v0, 0x7f083579

    return v0
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2481446
    const v0, 0x7f0311b3

    return v0
.end method
