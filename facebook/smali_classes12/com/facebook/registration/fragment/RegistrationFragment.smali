.class public abstract Lcom/facebook/registration/fragment/RegistrationFragment;
.super Lcom/facebook/base/fragment/AbstractNavigableFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481375
    invoke-direct {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/HYj;)V
    .locals 2

    .prologue
    .line 2481373
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, LX/HYj;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a(Landroid/content/Intent;)V

    .line 2481374
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2481392
    return-void
.end method

.method public c_()V
    .locals 2

    .prologue
    .line 2481387
    invoke-super {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c_()V

    .line 2481388
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2481389
    if-eqz v0, :cond_0

    .line 2481390
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;->lO_()I

    move-result v1

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2481391
    :cond_0
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 2481386
    const v0, 0x7f0311c0

    return v0
.end method

.method public abstract lO_()I
.end method

.method public abstract lQ_()I
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x74e09529

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2481376
    iget-boolean v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    move v0, v0

    .line 2481377
    if-eqz v0, :cond_0

    .line 2481378
    const/4 v0, 0x0

    const/16 v1, 0x2b

    const v3, -0x7cbf25db

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2481379
    :goto_0
    return-object v0

    .line 2481380
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;->d()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2481381
    const v0, 0x7f0d299f

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2481382
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;->lQ_()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2481383
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2481384
    invoke-virtual {p0, v1, p3}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2481385
    const v0, 0x1c780e1f

    invoke-static {v0, v2}, LX/02F;->f(II)V

    move-object v0, v1

    goto :goto_0
.end method
