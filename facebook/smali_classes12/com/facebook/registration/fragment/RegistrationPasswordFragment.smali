.class public Lcom/facebook/registration/fragment/RegistrationPasswordFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field private b:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482441
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2482442
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    if-eqz v0, :cond_0

    .line 2482443
    const v0, 0x7f0d29ad

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    .line 2482444
    const v0, 0x7f0d29ac

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2482445
    :goto_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2482446
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2482447
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    new-instance v1, LX/HZh;

    invoke-direct {v1, p0}, LX/HZh;-><init>(Lcom/facebook/registration/fragment/RegistrationPasswordFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2482448
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/TextView;)V

    .line 2482449
    return-void

    .line 2482450
    :cond_0
    const v0, 0x7f0d29ab

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2482440
    const v0, 0x7f083591

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2482439
    const v0, 0x7f0311bd

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2482436
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 2482437
    new-instance v0, LX/HZb;

    const v1, 0x7f083593

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2482438
    :cond_0
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482451
    const v0, 0x7f08358f

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2482435
    const v0, 0x7f083590

    return v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2482433
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->c(Ljava/lang/String;)V

    .line 2482434
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2482432
    sget-object v0, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2482430
    sget-object v0, LX/HYi;->PASSWORD:LX/HYi;

    return-object v0
.end method

.method public final r()[Landroid/widget/EditText;
    .locals 3

    .prologue
    .line 2482431
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPasswordFragment;->b:Landroid/widget/EditText;

    aput-object v2, v0, v1

    return-object v0
.end method
