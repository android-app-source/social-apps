.class public Lcom/facebook/registration/fragment/RegistrationEmailFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field public b:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/AutoCompleteTextView;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2482018
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    .line 2482019
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    .line 2482020
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->m:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2482015
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/os/Bundle;)V

    .line 2482016
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v0

    check-cast v0, LX/HZt;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->b:LX/HZt;

    .line 2482017
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2481986
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    if-eqz v0, :cond_2

    .line 2481987
    const v0, 0x7f0d298c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    .line 2481988
    const v0, 0x7f0d298b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2481989
    :goto_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 2481990
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481991
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/HZF;

    invoke-direct {v1, p0}, LX/HZF;-><init>(Lcom/facebook/registration/fragment/RegistrationEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2481992
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/TextView;)V

    .line 2481993
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2481994
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->m()Lcom/facebook/registration/model/ContactPointSuggestions;

    move-result-object v1

    .line 2481995
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->b:LX/HZt;

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/SimpleRegFormData;->n()Z

    move-result v2

    const-string v3, "email"

    invoke-virtual {v0, v2, v3}, LX/HZt;->a(ZLjava/lang/String;)V

    .line 2481996
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->l()Lcom/facebook/growth/model/DeviceOwnerData;

    move-result-object v0

    .line 2481997
    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v2

    .line 2481998
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481999
    iput-boolean v6, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->m:Z

    .line 2482000
    sget-object v0, LX/HZz;->PREFILL:LX/HZz;

    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v1, v0, v3, v5}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;I)Ljava/lang/String;

    move-result-object v0

    .line 2482001
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2482002
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482003
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2482004
    sget-object v0, LX/HZz;->AUTOCOMPLETE:LX/HZz;

    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v1, v0, v3}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;)Ljava/util/List;

    move-result-object v0

    .line 2482005
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482006
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482007
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2482008
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x109000a

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2482009
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v6}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2482010
    :cond_1
    const v0, 0x7f0d298d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/HZG;

    invoke-direct {v1, p0}, LX/HZG;-><init>(Lcom/facebook/registration/fragment/RegistrationEmailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482011
    return-void

    .line 2482012
    :cond_2
    const v0, 0x7f0d0b0c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    goto/16 :goto_0

    .line 2482013
    :cond_3
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2482014
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2481985
    const v0, 0x7f083576

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2481984
    const v0, 0x7f0311b1

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2481980
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2481981
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2481982
    :cond_0
    new-instance v0, LX/HZb;

    const v1, 0x7f083578

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2481983
    :cond_1
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2481979
    const v0, 0x7f083579

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2481978
    const v0, 0x7f083575

    return v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2481966
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    .line 2481967
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setEmail(Ljava/lang/String;)V

    .line 2481968
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2481977
    sget-object v0, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2481976
    sget-object v0, LX/HYi;->EMAIL:LX/HYi;

    return-object v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 2481975
    const v0, 0x7f0311b2

    return v0
.end method

.method public final r()[Landroid/widget/EditText;
    .locals 3

    .prologue
    .line 2481974
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final s()V
    .locals 4

    .prologue
    .line 2481969
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->m:Z

    if-nez v0, :cond_0

    .line 2481970
    :goto_0
    return-void

    .line 2481971
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->m:Z

    .line 2481972
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->c:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2481973
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->b:LX/HZt;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->o()LX/HYi;

    move-result-object v2

    invoke-virtual {v2}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationEmailFragment;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, LX/HZt;->a(Ljava/lang/String;II)V

    goto :goto_0
.end method
