.class public Lcom/facebook/registration/fragment/RegistrationNameFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field public b:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/AutoCompleteTextView;

.field public m:Landroid/widget/AutoCompleteTextView;

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2482396
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    .line 2482397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2482393
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/os/Bundle;)V

    .line 2482394
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object p1

    check-cast p1, LX/HZt;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->b:LX/HZt;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->c:LX/0W9;

    .line 2482395
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2482343
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    if-eqz v0, :cond_7

    .line 2482344
    const v0, 0x7f0d29a5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    .line 2482345
    const v0, 0x7f0d29a4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2482346
    :goto_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 2482347
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482348
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/HZc;

    invoke-direct {v1, p0}, LX/HZc;-><init>(Lcom/facebook/registration/fragment/RegistrationNameFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2482349
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    if-eqz v0, :cond_8

    .line 2482350
    const v0, 0x7f0d29a7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    .line 2482351
    const v0, 0x7f0d29a6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2482352
    :goto_1
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 2482353
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482354
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/HZd;

    invoke-direct {v1, p0}, LX/HZd;-><init>(Lcom/facebook/registration/fragment/RegistrationNameFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2482355
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/TextView;)V

    .line 2482356
    const/4 v1, 0x0

    .line 2482357
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->c:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2482358
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 2482359
    :cond_1
    :goto_3
    const p1, 0x109000a

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2482360
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->l()Lcom/facebook/growth/model/DeviceOwnerData;

    move-result-object v0

    .line 2482361
    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->b()LX/0Px;

    move-result-object v3

    .line 2482362
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2482363
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2482364
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2482365
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_4
    if-ge v1, v7, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/FullName;

    .line 2482366
    if-eqz v0, :cond_4

    .line 2482367
    iget-object v8, v0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2482368
    iget-object v8, v0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2482369
    :cond_2
    iget-object v8, v0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 2482370
    iget-object v8, v0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2482371
    :cond_3
    iget-object v8, v0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 2482372
    iget-object v0, v0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2482373
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2482374
    :cond_5
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2482375
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482376
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482377
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482378
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2482379
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    new-array v5, v2, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v1, v3, p1, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2482380
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v9}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2482381
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v3, p1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2482382
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v9}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2482383
    :cond_6
    return-void

    .line 2482384
    :cond_7
    const v0, 0x7f0d29a2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    goto/16 :goto_0

    .line 2482385
    :cond_8
    const v0, 0x7f0d29a3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    goto/16 :goto_1

    .line 2482386
    :sswitch_0
    const-string p1, "zh_CN"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_2

    :sswitch_1
    const-string p1, "zh_TW"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_2

    :sswitch_2
    const-string p1, "zh_HK"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_2

    .line 2482387
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->e:LX/0Uh;

    const/16 v2, 0x36

    invoke-virtual {v0, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    .line 2482388
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->b:LX/HZt;

    const-string p1, "fb4a_reg_name_hint_zh_promo"

    invoke-virtual {v2, p1, v0}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2482389
    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2482390
    const v0, 0x7f0835c3

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2482391
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const p1, 0x7f08357e

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2482392
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const p1, 0x7f08357f

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x6e7e71c -> :sswitch_0
        0x6e7e7b4 -> :sswitch_2
        0x6e7e934 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2482342
    const v0, 0x7f083580

    return v0
.end method

.method public final k()I
    .locals 5

    .prologue
    .line 2482334
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2482335
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->c:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2482336
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 2482337
    :pswitch_0
    move v0, v0

    .line 2482338
    if-eqz v0, :cond_1

    .line 2482339
    const v0, 0x7f0311bb

    .line 2482340
    :goto_1
    return v0

    :cond_1
    const v0, 0x7f0311ba

    goto :goto_1

    .line 2482341
    :sswitch_0
    const-string v4, "zh_CN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "zh_TW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    goto :goto_0

    :sswitch_2
    const-string v4, "zh_HK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    :sswitch_3
    const-string v4, "vi_VN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :sswitch_4
    const-string v4, "ja_JP"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x4

    goto :goto_0

    :sswitch_5
    const-string v4, "ko_KR"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x5

    goto :goto_0

    :sswitch_6
    const-string v4, "hu_HU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x6

    goto :goto_0

    :sswitch_7
    const-string v4, "te_IN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x7

    goto :goto_0

    :sswitch_8
    const-string v4, "tg_TJ"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x8

    goto :goto_0

    :sswitch_9
    const-string v4, "rw_RW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x9

    goto :goto_0

    :sswitch_a
    const-string v4, "mn_MN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xa

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5f0297f -> :sswitch_6
        0x603414e -> :sswitch_4
        0x617b622 -> :sswitch_5
        0x63370ff -> :sswitch_a
        0x67dfe7f -> :sswitch_9
        0x691fdb3 -> :sswitch_7
        0x692e7c2 -> :sswitch_8
        0x6afffc4 -> :sswitch_3
        0x6e7e71c -> :sswitch_0
        0x6e7e7b4 -> :sswitch_2
        0x6e7e934 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2482331
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2482332
    :cond_0
    new-instance v0, LX/HZb;

    const v1, 0x7f083581

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2482333
    :cond_1
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482318
    const v0, 0x7f08357b

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2482330
    const v0, 0x7f08357c

    return v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2482327
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setFirstName(Ljava/lang/String;)V

    .line 2482328
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setLastName(Ljava/lang/String;)V

    .line 2482329
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2482326
    sget-object v0, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2482325
    sget-object v0, LX/HYi;->NAME:LX/HYi;

    return-object v0
.end method

.method public final r()[Landroid/widget/EditText;
    .locals 3

    .prologue
    .line 2482324
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final s()V
    .locals 5

    .prologue
    .line 2482319
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482320
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->m:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2482321
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->b:LX/HZt;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationNameFragment;->o()LX/HYi;

    move-result-object v4

    invoke-virtual {v4}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_first_name"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v3, v0, v4}, LX/HZt;->a(Ljava/lang/String;II)V

    .line 2482322
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->b:LX/HZt;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationNameFragment;->o()LX/HYi;

    move-result-object v3

    invoke-virtual {v3}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_last_name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationNameFragment;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, LX/HZt;->a(Ljava/lang/String;II)V

    .line 2482323
    return-void
.end method
