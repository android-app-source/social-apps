.class public Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;
.super Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;
.source ""


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481452
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HYh;)V
    .locals 5

    .prologue
    .line 2481453
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;->c:LX/HZt;

    invoke-virtual {p1}, LX/HYh;->name()Ljava/lang/String;

    move-result-object v1

    .line 2481454
    iget-object v2, v0, LX/HZt;->a:LX/0Zb;

    sget-object v3, LX/HZu;->REGISTRATION_ADDITIONAL_EMAIL_STATE:LX/HZu;

    invoke-static {v0, v3}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "state"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481455
    sget-object v0, LX/HYh;->ADDED:LX/HYh;

    invoke-virtual {p1, v0}, LX/HYh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2481456
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->d(Ljava/lang/String;)V

    .line 2481457
    :cond_0
    :goto_0
    sget-object v0, LX/HYj;->ADDITIONAL_EMAIL_ACQUIRED:LX/HYj;

    move-object v0, v0

    .line 2481458
    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2481459
    return-void

    .line 2481460
    :cond_1
    sget-object v0, LX/HYh;->SKIPPED:LX/HYh;

    invoke-virtual {p1, v0}, LX/HYh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481461
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2481449
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationPrefillEmailFragment;->a(Landroid/os/Bundle;)V

    .line 2481450
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object p1

    check-cast p1, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v0

    check-cast v0, LX/HZt;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationAdditionalEmailFragment;->c:LX/HZt;

    .line 2481451
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2481448
    const v0, 0x7f0835b6

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final lN_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2481447
    const v0, 0x7f080030

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
