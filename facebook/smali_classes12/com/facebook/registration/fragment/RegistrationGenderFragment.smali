.class public Lcom/facebook/registration/fragment/RegistrationGenderFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# instance fields
.field private b:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482201
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v11, 0x5a

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2482177
    const v0, 0x7f0d0419

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationGenderFragment;->b:Landroid/widget/RadioGroup;

    .line 2482178
    const v0, 0x7f0d041b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2482179
    const v1, 0x7f0d041c

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 2482180
    const v2, 0x7f08358c

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setText(I)V

    .line 2482181
    const v2, 0x7f08358d

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setText(I)V

    .line 2482182
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->getGender()LX/F8q;

    move-result-object v2

    .line 2482183
    sget-object v5, LX/F8q;->MALE:LX/F8q;

    if-ne v2, v5, :cond_3

    .line 2482184
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2482185
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationGenderFragment;->b:Landroid/widget/RadioGroup;

    new-instance v5, LX/HZN;

    invoke-direct {v5, p0}, LX/HZN;-><init>(Lcom/facebook/registration/fragment/RegistrationGenderFragment;)V

    invoke-virtual {v2, v5}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2482186
    invoke-virtual {v0}, Landroid/widget/RadioButton;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2482187
    aget-object v2, v5, v4

    invoke-virtual {v2, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2482188
    aget-object v2, v5, v4

    aget-object v6, v5, v3

    aget-object v7, v5, v9

    aget-object v8, v5, v10

    invoke-virtual {v0, v2, v6, v7, v8}, Landroid/widget/RadioButton;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2482189
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v2, v6, :cond_4

    move v2, v3

    .line 2482190
    :goto_1
    if-eqz v2, :cond_1

    .line 2482191
    aget-object v6, v5, v4

    aget-object v7, v5, v3

    aget-object v8, v5, v9

    aget-object v5, v5, v10

    invoke-virtual {v0, v6, v7, v8, v5}, Landroid/widget/RadioButton;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2482192
    :cond_1
    invoke-virtual {v1}, Landroid/widget/RadioButton;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2482193
    aget-object v5, v0, v4

    invoke-virtual {v5, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2482194
    aget-object v5, v0, v4

    aget-object v6, v0, v3

    aget-object v7, v0, v9

    aget-object v8, v0, v10

    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/widget/RadioButton;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2482195
    if-eqz v2, :cond_2

    .line 2482196
    aget-object v2, v0, v4

    aget-object v3, v0, v3

    aget-object v4, v0, v9

    aget-object v0, v0, v10

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/widget/RadioButton;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2482197
    :cond_2
    return-void

    .line 2482198
    :cond_3
    sget-object v5, LX/F8q;->FEMALE:LX/F8q;

    if-ne v2, v5, :cond_0

    .line 2482199
    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 2482200
    goto :goto_1
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2482176
    const v0, 0x7f08358b

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2482175
    const v0, 0x7f0311b4

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2482171
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getGender()LX/F8q;

    move-result-object v0

    .line 2482172
    sget-object v1, LX/F8q;->MALE:LX/F8q;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/F8q;->FEMALE:LX/F8q;

    if-eq v0, v1, :cond_0

    .line 2482173
    new-instance v0, LX/HZb;

    const v1, 0x7f08358e

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2482174
    :cond_0
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482170
    const v0, 0x7f083589

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2482166
    const v0, 0x7f08358a

    return v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2482169
    return-void
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2482168
    sget-object v0, LX/HYj;->GENDER_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2482167
    sget-object v0, LX/HYi;->GENDER:LX/HYi;

    return-object v0
.end method
