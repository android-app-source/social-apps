.class public Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;
.super Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final t:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HZv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/5Qo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0i5;

.field private v:LX/0ju;

.field private w:Z

.field private x:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2481958
    const-class v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->t:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2481957
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2481953
    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->x:Ljava/lang/String;

    .line 2481954
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->w:Z

    .line 2481955
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->v:LX/0ju;

    const v1, 0x7f08001a

    new-instance v2, LX/HZE;

    invoke-direct {v2, p0, p1}, LX/HZE;-><init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2481956
    return-void
.end method

.method public static b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    .line 2481909
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->q()V

    .line 2481910
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, v7}, Lcom/facebook/registration/model/RegistrationFormData;->d(Z)V

    .line 2481911
    new-instance v0, LX/HZD;

    invoke-direct {v0, p0}, LX/HZD;-><init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V

    invoke-virtual {p0, v0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(LX/266;Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2481912
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2481913
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2481914
    invoke-virtual {p0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)LX/Ha0;

    move-result-object v1

    .line 2481915
    if-eqz v1, :cond_1

    iget-object v2, v1, LX/Ha0;->message:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2481916
    iget v2, v1, LX/Ha0;->code:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v1, v1, LX/Ha0;->message:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481917
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2481918
    :cond_2
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2481919
    if-eqz v0, :cond_3

    .line 2481920
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2481921
    if-eqz v1, :cond_3

    .line 2481922
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2481923
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 2481924
    invoke-virtual {v2}, LX/1nY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2481925
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v0, v2

    .line 2481926
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2481927
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    .line 2481928
    :goto_1
    return-void

    .line 2481929
    :cond_3
    const-string v0, "No response or malformed failed response"

    goto :goto_0

    .line 2481930
    :cond_4
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2481931
    sget-object v1, LX/HYl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYi;

    .line 2481932
    if-nez v1, :cond_a

    .line 2481933
    sget-object v1, LX/HYi;->UNKNOWN:LX/HYi;

    move-object v2, v1

    .line 2481934
    :goto_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v4, 0xc29

    if-ne v1, v4, :cond_6

    .line 2481935
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v4, 0x31

    invoke-virtual {v1, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 2481936
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    const-string v5, "fb4a_reg_email_taken_step"

    invoke-virtual {v4, v5, v1}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481937
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v5, 0x32

    invoke-virtual {v4, v5}, LX/0Uh;->a(I)LX/03R;

    move-result-object v4

    .line 2481938
    iget-object v5, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    const-string v6, "fb4a_reg_email_taken_to_ar"

    invoke-virtual {v5, v6, v4}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481939
    invoke-virtual {v1, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v4, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2481940
    :cond_5
    sget-object v2, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    .line 2481941
    :cond_6
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v4, 0xcea

    if-ne v1, v4, :cond_8

    .line 2481942
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v4, 0x3a

    invoke-virtual {v1, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 2481943
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    const-string v5, "fb4a_reg_phone_taken_step"

    invoke-virtual {v4, v5, v1}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481944
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v5, 0x3b

    invoke-virtual {v4, v5}, LX/0Uh;->a(I)LX/03R;

    move-result-object v4

    .line 2481945
    iget-object v5, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    const-string v6, "fb4a_reg_phone_taken_to_ar"

    invoke-virtual {v5, v6, v4}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481946
    invoke-virtual {v1, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v4, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2481947
    :cond_7
    sget-object v2, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    .line 2481948
    :cond_8
    iget-object v4, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v2, v5, v1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/HYi;ILjava/lang/String;)V

    .line 2481949
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    .line 2481950
    iget-object v8, v1, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v9, 0x400001

    const-string v10, "AccountCreationTime"

    const/4 v11, 0x0

    const-string v12, "result"

    const-string v13, "server_validation"

    invoke-interface/range {v8 .. v13}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2481951
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/HZt;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2481952
    :cond_9
    sget-object v0, LX/HYj;->CREATE_ERROR:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto/16 :goto_1

    :cond_a
    move-object v2, v1

    goto/16 :goto_3
.end method

.method public static b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2481904
    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->n()V

    .line 2481905
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    const-string v1, "unknown"

    invoke-virtual {v0, v1, p1}, LX/HZt;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2481906
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    .line 2481907
    iget-object v1, v0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const p0, 0x400001

    const-string p1, "AccountCreationTime"

    invoke-interface {v1, p0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2481908
    return-void
.end method

.method public static o(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V
    .locals 3

    .prologue
    .line 2481857
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->l:LX/HaQ;

    .line 2481858
    iget-object v1, v0, LX/HaQ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Ha8;->d:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2481859
    sget-object v0, LX/HYj;->CREATE_SUCCESS:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2481860
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 2481896
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-eq v0, v1, :cond_0

    .line 2481897
    :goto_0
    return-void

    .line 2481898
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    .line 2481899
    iget-object v1, v0, LX/2U8;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/confirmation/task/BackgroundConfirmationHelper$1;

    invoke-direct {v2, v0}, Lcom/facebook/confirmation/task/BackgroundConfirmationHelper$1;-><init>(LX/2U8;)V

    const p0, -0xdb08b4e

    invoke-static {v1, v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2481900
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2U8;->a(I)V

    .line 2481901
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v1}, LX/2U8;->a(Ljava/util/Set;)Z

    .line 2481902
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v1}, LX/2U8;->b(Ljava/util/Set;)Z

    .line 2481903
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2481887
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Landroid/os/Bundle;)V

    .line 2481888
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    const/16 v3, 0x3ea

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/HZv;->a(LX/0QB;)LX/HZv;

    move-result-object v4

    check-cast v4, LX/HZv;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v6

    check-cast v6, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v7

    check-cast v7, LX/HZt;

    invoke-static {v0}, LX/5Qo;->a(LX/0QB;)LX/5Qo;

    move-result-object v8

    check-cast v8, LX/5Qo;

    const/16 v9, 0x12c4

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    const-class v11, LX/0i4;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/0i4;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v0

    check-cast v0, LX/HaQ;

    iput-object v3, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    iput-object v5, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v7, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    iput-object v8, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->g:LX/5Qo;

    iput-object v9, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->h:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->i:LX/03V;

    iput-object v11, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->j:LX/0i4;

    iput-object v12, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->l:LX/HaQ;

    .line 2481889
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f08359e

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08359f

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->v:LX/0ju;

    .line 2481890
    if-eqz p1, :cond_0

    .line 2481891
    const-string v0, "completion_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->x:Ljava/lang/String;

    .line 2481892
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "completion_dialog_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481893
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->x:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->a(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    .line 2481894
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->j:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->u:LX/0i5;

    .line 2481895
    return-void
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2481886
    const v0, 0x7f083596

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2481883
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->q()V

    .line 2481884
    sget-object v0, LX/HYj;->ERROR_CONTINUE:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2481885
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2481882
    const v0, 0x7f083594

    return v0
.end method

.method public final m()V
    .locals 10

    .prologue
    .line 2481865
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->w:Z

    if-nez v0, :cond_0

    .line 2481866
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->q()V

    .line 2481867
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->l:LX/HaQ;

    sget-object v1, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->t:Lcom/facebook/common/callercontext/CallerContext;

    .line 2481868
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2481869
    const-string v4, "registrationRegisterAccountParams"

    iget-object v5, v0, LX/HaQ;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v6, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2481870
    iget-object v4, v0, LX/HaQ;->h:LX/0aG;

    const-string v5, "registration_register_account"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const v9, 0x5224536e

    move-object v8, v1

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2481871
    move-object v0, v4

    .line 2481872
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    const/4 v6, 0x1

    .line 2481873
    iget-object v2, v1, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v3, LX/0Yj;

    const v4, 0x400001

    const-string v5, "AccountCreationTime"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3}, LX/0Yj;->b()LX/0Yj;

    move-result-object v3

    .line 2481874
    iput-boolean v6, v3, LX/0Yj;->n:Z

    .line 2481875
    move-object v3, v3

    .line 2481876
    invoke-interface {v2, v3, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 2481877
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->m:LX/1Ck;

    const/4 v2, 0x0

    new-instance v3, LX/HZB;

    invoke-direct {v3, p0}, LX/HZB;-><init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2481878
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    .line 2481879
    iget-object v1, v0, LX/HZt;->a:LX/0Zb;

    sget-object v2, LX/HZu;->ACCOUNT_CREATION_ATTEMPT:LX/HZu;

    invoke-static {v0, v2}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481880
    const-string v1, "create_attempt"

    invoke-static {v0, v1}, LX/HZt;->i(LX/HZt;Ljava/lang/String;)V

    .line 2481881
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2481861
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2481862
    const-string v0, "completion_url"

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2481863
    const-string v0, "completion_dialog_shown"

    iget-boolean v1, p0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2481864
    return-void
.end method
