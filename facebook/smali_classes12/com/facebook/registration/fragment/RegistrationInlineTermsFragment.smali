.class public Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/widget/TextView;

.field public h:Landroid/widget/Button;

.field public i:Landroid/view/ViewGroup;

.field public j:Landroid/view/View;

.field public k:Landroid/view/View;

.field public l:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482290
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    .line 2482291
    return-void
.end method

.method private a(Landroid/view/View;LX/HZW;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2482275
    const v0, 0x7f0d2992

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2482276
    const v1, 0x7f0d2993

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 2482277
    const v2, 0x7f0d2994

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 2482278
    const v3, 0x7f0d2995

    invoke-static {p1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckedTextView;

    .line 2482279
    const v4, 0x7f0d2996

    invoke-static {p1, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckedTextView;

    .line 2482280
    iget v5, p2, LX/HZW;->titleResId:I

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 2482281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";FB_FW/1"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482282
    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 2482283
    invoke-virtual {v1, v6}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 2482284
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v7}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2482285
    new-instance v0, LX/HZQ;

    invoke-direct {v0, p0, v2, v1}, LX/HZQ;-><init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;Landroid/widget/ProgressBar;Landroid/webkit/WebView;)V

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 2482286
    invoke-virtual {v1, v6, v7}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2482287
    iget-object v0, p2, LX/HZW;->url:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 2482288
    new-instance v0, LX/HZT;

    invoke-direct {v0, p0, v3, v4, p2}, LX/HZT;-><init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;Landroid/widget/CheckedTextView;Landroid/widget/CheckedTextView;LX/HZW;)V

    invoke-virtual {v4, v0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482289
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2482249
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2482250
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    invoke-static {p1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p1}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v4

    check-cast v4, LX/HaQ;

    invoke-static {p1}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v5

    check-cast v5, LX/HZt;

    invoke-static {p1}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v6

    check-cast v6, LX/GvB;

    const/16 v0, 0x1430

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v4, v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->c:LX/HaQ;

    iput-object v5, v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->d:LX/HZt;

    iput-object v6, v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->e:LX/GvB;

    iput-object p1, v2, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->f:LX/0Ot;

    .line 2482251
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2482254
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2482255
    const v0, 0x7f0d2998

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->g:Landroid/widget/TextView;

    .line 2482256
    const v0, 0x7f0d2985

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->h:Landroid/widget/Button;

    .line 2482257
    const v0, 0x7f0d2997

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->i:Landroid/view/ViewGroup;

    .line 2482258
    const v0, 0x7f0d2999

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->j:Landroid/view/View;

    .line 2482259
    const v0, 0x7f0d299a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->k:Landroid/view/View;

    .line 2482260
    const v0, 0x7f0d299b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->l:Landroid/view/View;

    .line 2482261
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->c:LX/HaQ;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, LX/HaQ;->a(Landroid/widget/TextView;)V

    .line 2482262
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->h:Landroid/widget/Button;

    new-instance v1, LX/HZP;

    invoke-direct {v1, p0}, LX/HZP;-><init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482263
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->j:Landroid/view/View;

    sget-object v1, LX/HZW;->TERMS_OF_SERVICE:LX/HZW;

    invoke-direct {p0, v0, v1}, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->a(Landroid/view/View;LX/HZW;)V

    .line 2482264
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->k:Landroid/view/View;

    sget-object v1, LX/HZW;->DATA_POLICY:LX/HZW;

    invoke-direct {p0, v0, v1}, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->a(Landroid/view/View;LX/HZW;)V

    .line 2482265
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->l:Landroid/view/View;

    sget-object v1, LX/HZW;->LOCATION_SUPPLEMENT:LX/HZW;

    invoke-direct {p0, v0, v1}, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->a(Landroid/view/View;LX/HZW;)V

    .line 2482266
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->j:Landroid/view/View;

    const v1, 0x7f0d2993

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 2482267
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->k:Landroid/view/View;

    const v2, 0x7f0d2993

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 2482268
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->l:Landroid/view/View;

    const p1, 0x7f0d2993

    invoke-static {v2, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    .line 2482269
    new-instance p1, LX/HZU;

    invoke-direct {p1, p0}, LX/HZU;-><init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;)V

    .line 2482270
    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2482271
    invoke-virtual {v1, p1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2482272
    invoke-virtual {v2, p1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2482273
    iget-object p1, p0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->i:Landroid/view/ViewGroup;

    new-instance p2, LX/HZV;

    invoke-direct {p2, p0, v0, v1, v2}, LX/HZV;-><init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;Landroid/webkit/WebView;Landroid/webkit/WebView;Landroid/webkit/WebView;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2482274
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482253
    const v0, 0x7f0835a2

    return v0
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2482252
    const v0, 0x7f0311b6

    return v0
.end method
