.class public Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;
.super Lcom/facebook/registration/fragment/RegistrationFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3fx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Landroid/widget/ProgressBar;

.field public j:Landroid/view/ViewGroup;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public n:Landroid/widget/Button;

.field public o:Landroid/widget/TextView;

.field public p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

.field public q:Z

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2482156
    const-class v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482155
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationFragment;-><init>()V

    return-void
.end method

.method public static m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V
    .locals 5

    .prologue
    .line 2482150
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v1, "AUTO_SKIP_DUE_TO_ERROR"

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482151
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_0

    .line 2482152
    sget-object v0, LX/HYj;->PHONE_SWITCH_TO_EMAIL:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482153
    :goto_0
    return-void

    .line 2482154
    :cond_0
    sget-object v0, LX/HYj;->EMAIL_SWITCH_TO_PHONE:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V
    .locals 3

    .prologue
    .line 2482146
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/account/recovery/AccountRecoveryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2482147
    const-string v1, "account_profile"

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2482148
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2482149
    return-void
.end method

.method public static p(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V
    .locals 2

    .prologue
    .line 2482157
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->p()V

    .line 2482158
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->e(Z)V

    .line 2482159
    sget-object v0, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482160
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2482143
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(Landroid/os/Bundle;)V

    .line 2482144
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v0}, LX/3fx;->a(LX/0QB;)LX/3fx;

    move-result-object v4

    check-cast v4, LX/3fx;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v0

    check-cast v0, LX/HZt;

    iput-object v3, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v4, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->c:LX/3fx;

    iput-object v5, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->d:LX/0aG;

    iput-object v6, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->e:Ljava/util/concurrent/Executor;

    iput-object p1, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    .line 2482145
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 2482104
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2482105
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    .line 2482106
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/SimpleRegFormData;->c(LX/HYi;)I

    move-result v0

    iput v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    .line 2482107
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v1, "STEP_CREATE"

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482108
    const v0, 0x7f0d04de

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->i:Landroid/widget/ProgressBar;

    .line 2482109
    const v0, 0x7f0d2987

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->j:Landroid/view/ViewGroup;

    .line 2482110
    const v0, 0x7f0d090e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->k:Landroid/widget/TextView;

    .line 2482111
    const v0, 0x7f0d2988

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->l:Landroid/widget/TextView;

    .line 2482112
    const v0, 0x7f0d036f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2482113
    const v0, 0x7f0d2989

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->n:Landroid/widget/Button;

    .line 2482114
    const v0, 0x7f0d0b9b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->o:Landroid/widget/TextView;

    .line 2482115
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2482116
    const-string v1, "auto_redirect_to_ar"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->q:Z

    .line 2482117
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2482118
    const-string v1, "allow_reg_using_same_cp"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->r:Z

    .line 2482119
    const/4 v12, 0x1

    const/4 v7, 0x0

    const/4 v11, 0x0

    .line 2482120
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v6, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2482121
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->j:Landroid/view/ViewGroup;

    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2482122
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v8, "SEARCH_START"

    iget-object v9, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget v10, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    invoke-virtual {v6, v8, v9, v10, v7}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482123
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2482124
    iget-object v8, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v8}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v8

    sget-object v9, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v8, v9, :cond_0

    .line 2482125
    iget-object v8, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v8}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v8

    .line 2482126
    const-string v9, "phone"

    new-array v10, v12, [Ljava/lang/String;

    aput-object v8, v10, v11

    invoke-static {v10}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-interface {v6, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482127
    :goto_0
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 2482128
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 2482129
    new-instance v6, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;

    const-string v10, ""

    const-string v11, ""

    move-object v8, v7

    invoke-direct/range {v6 .. v11}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2482130
    const-string v7, "accountRecoverySearchAccountParamsKey"

    invoke-virtual {v12, v7, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2482131
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->d:LX/0aG;

    const-string v7, "account_recovery_search_account"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v10, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    const v11, -0xf17ef1e

    move-object v8, v12

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 2482132
    new-instance v7, LX/HZH;

    invoke-direct {v7, p0}, LX/HZH;-><init>(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    iget-object v8, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->e:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2482133
    :goto_1
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->n:Landroid/widget/Button;

    new-instance v1, LX/HZI;

    invoke-direct {v1, p0}, LX/HZI;-><init>(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482134
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->r:Z

    if-eqz v0, :cond_2

    .line 2482135
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->o:Landroid/widget/TextView;

    new-instance v1, LX/HZJ;

    invoke-direct {v1, p0}, LX/HZJ;-><init>(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482136
    :goto_2
    return-void

    .line 2482137
    :cond_0
    iget-object v8, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v8}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v8

    sget-object v9, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v8, v9, :cond_1

    .line 2482138
    iget-object v8, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v8}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v8

    .line 2482139
    const-string v9, "email"

    new-array v10, v12, [Ljava/lang/String;

    aput-object v8, v10, v11

    invoke-static {v10}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-interface {v6, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2482140
    :cond_1
    invoke-static {p0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    goto :goto_1

    .line 2482141
    :catch_0
    invoke-static {p0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    goto :goto_1

    .line 2482142
    :cond_2
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->o:Landroid/widget/TextView;

    new-instance v1, LX/HZM;

    invoke-direct {v1, p0}, LX/HZM;-><init>(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method public final lO_()I
    .locals 2

    .prologue
    .line 2482101
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_0

    .line 2482102
    const v0, 0x7f0835b8

    .line 2482103
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0835b9

    goto :goto_0
.end method

.method public final lQ_()I
    .locals 1

    .prologue
    .line 2482100
    const v0, 0x7f0311af

    return v0
.end method
