.class public Lcom/facebook/registration/fragment/RegistrationPhoneFragment;
.super Lcom/facebook/registration/fragment/RegistrationInputFragment;
.source ""


# static fields
.field public static final o:Ljava/lang/String;


# instance fields
.field public b:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/widget/AutoCompleteTextView;

.field public q:Lcom/facebook/resources/ui/FbButton;

.field private r:Landroid/text/TextWatcher;

.field public s:LX/7Tj;

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2482506
    const-class v0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2482507
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;-><init>()V

    .line 2482508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    .line 2482509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->w:Z

    return-void
.end method

.method public static a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;LX/4hT;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2482510
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    sget-object v1, LX/4hG;->INTERNATIONAL:LX/4hG;

    invoke-virtual {v0, p1, v1}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v0

    .line 2482511
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2482512
    iget v2, p1, LX/4hT;->countryCode_:I

    move v2, v2

    .line 2482513
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2482514
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2482515
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2482516
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const/16 v2, 0x2d

    invoke-static {v2}, LX/1IA;->is(C)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2482517
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2482518
    if-nez p1, :cond_0

    .line 2482519
    :goto_0
    return-void

    .line 2482520
    :cond_0
    new-instance v0, LX/7Tl;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    invoke-virtual {v2, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Locale;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->b:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->b:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, LX/7Tl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a$redex0(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;LX/7Tl;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;LX/7Tl;)V
    .locals 3

    .prologue
    .line 2482587
    iget-object v0, p1, LX/7Tl;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->u:Ljava/lang/String;

    .line 2482588
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->q:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p1, LX/7Tl;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2482589
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2482590
    new-instance v0, LX/A8g;

    iget-object v1, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->r:Landroid/text/TextWatcher;

    .line 2482591
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2482592
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482593
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, "()-."

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2482594
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2482595
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-static {v1, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2482596
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    iget-object v1, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setPhoneIsoCountryCode(Ljava/lang/String;)V

    .line 2482597
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2482521
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2482522
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->j:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2482523
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 2482524
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2482525
    :goto_0
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2482526
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2482527
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2482528
    return-void

    .line 2482529
    :pswitch_0
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2482530
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2482531
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2482532
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/os/Bundle;)V

    .line 2482533
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;

    invoke-static {p1}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-static {p1}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v4

    check-cast v4, LX/3Lz;

    invoke-static {p1}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v5

    check-cast v5, LX/HZt;

    const-class v6, LX/7Tk;

    invoke-interface {p1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/7Tk;

    const/16 v0, 0x15ec

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->b:Ljava/util/Locale;

    iput-object v4, v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    iput-object v5, v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->d:LX/HZt;

    iput-object v6, v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->m:LX/7Tk;

    iput-object p1, v2, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->n:LX/0Or;

    .line 2482534
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2482535
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->h:Z

    if-eqz v0, :cond_6

    .line 2482536
    const v0, 0x7f0d29af

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    .line 2482537
    const v0, 0x7f0d29ae

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2482538
    :goto_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 2482539
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneNumberInputRaw()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482540
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->a(Landroid/widget/TextView;)V

    .line 2482541
    const v0, 0x7f0d0b10

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->q:Lcom/facebook/resources/ui/FbButton;

    .line 2482542
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneIsoCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2482543
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneIsoCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;Ljava/lang/String;)V

    .line 2482544
    :cond_0
    new-instance v0, LX/HZk;

    invoke-direct {v0, p0}, LX/HZk;-><init>(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;)V

    .line 2482545
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482546
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v()V

    .line 2482547
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 2482548
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->m()Lcom/facebook/registration/model/ContactPointSuggestions;

    move-result-object v2

    .line 2482549
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->d:LX/HZt;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v3}, Lcom/facebook/registration/model/SimpleRegFormData;->n()Z

    move-result v3

    const-string v4, "phone"

    invoke-virtual {v0, v3, v4}, LX/HZt;->a(ZLjava/lang/String;)V

    .line 2482550
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->l()Lcom/facebook/growth/model/DeviceOwnerData;

    move-result-object v3

    .line 2482551
    invoke-virtual {v3}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v4

    .line 2482552
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2482553
    iput-boolean v9, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->w:Z

    .line 2482554
    sget-object v0, LX/HZz;->PREFILL:LX/HZz;

    sget-object v5, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v2, v0, v5, v1}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;I)Ljava/lang/String;

    move-result-object v5

    .line 2482555
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2482556
    :try_start_0
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    invoke-virtual {v3}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 2482557
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;LX/4hT;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482558
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    invoke-virtual {v6, v0}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    .line 2482559
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;Ljava/lang/String;)V
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    .line 2482560
    :cond_1
    :goto_1
    sget-object v0, LX/HZz;->AUTOCOMPLETE:LX/HZz;

    sget-object v5, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v2, v0, v5}, Lcom/facebook/registration/model/ContactPointSuggestions;->a(LX/HZz;Lcom/facebook/growth/model/ContactpointType;)Ljava/util/List;

    move-result-object v0

    .line 2482561
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2482562
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2482563
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2482564
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2482565
    :try_start_1
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    invoke-virtual {v3}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v6

    .line 2482566
    invoke-static {p0, v6}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;LX/4hT;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2482567
    iget-object v7, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    sget-object v8, LX/4hG;->NATIONAL:LX/4hG;

    invoke-virtual {v7, v6, v8}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/4hE; {:try_start_1 .. :try_end_1} :catch_1

    .line 2482568
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2482569
    :catch_0
    move-exception v0

    .line 2482570
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6, v5}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482571
    sget-object v5, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->o:Ljava/lang/String;

    const-string v6, "Incorrectly formatted suggested contact point prefill phone number: "

    invoke-static {v5, v6, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2482572
    :cond_2
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2482573
    iget-object v5, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v5, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2482574
    :catch_1
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2482575
    :cond_3
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2482576
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2482577
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x109000a

    iget-object v5, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    invoke-direct {v1, v2, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2482578
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v9}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 2482579
    :cond_4
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->u:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2482580
    invoke-virtual {v3}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2482581
    invoke-virtual {v3}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    .line 2482582
    :goto_4
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->a(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;Ljava/lang/String;)V

    .line 2482583
    :cond_5
    const v0, 0x7f0d29b0

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/HZi;

    invoke-direct {v1, p0}, LX/HZi;-><init>(Lcom/facebook/registration/fragment/RegistrationPhoneFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482584
    return-void

    .line 2482585
    :cond_6
    const v0, 0x7f0d0b11

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    goto/16 :goto_0

    .line 2482586
    :cond_7
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    goto :goto_4
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2482504
    const v0, 0x7f08356f

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2482505
    const v0, 0x7f0311be

    return v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2482500
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482501
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2482502
    :cond_0
    new-instance v0, LX/HZb;

    const v1, 0x7f083573

    invoke-direct {v0, p0, v1}, LX/HZb;-><init>(Lcom/facebook/registration/fragment/RegistrationInputFragment;I)V

    throw v0

    .line 2482503
    :cond_1
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482499
    const v0, 0x7f083570

    return v0
.end method

.method public final lP_()I
    .locals 1

    .prologue
    .line 2482498
    const v0, 0x7f08356e

    return v0
.end method

.method public final m()V
    .locals 5

    .prologue
    .line 2482488
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2482489
    const-string v0, ""

    .line 2482490
    :try_start_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v2

    .line 2482491
    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    sget-object v4, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v3, v2, v4}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2482492
    :goto_0
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2482493
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->c:LX/3Lz;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482494
    :cond_0
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v2, v3}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    .line 2482495
    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setPhoneNumberInputRaw(Ljava/lang/String;)V

    .line 2482496
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationInputFragment;->f:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/RegistrationFormData;->a(Ljava/lang/String;)V

    .line 2482497
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final n()LX/HYj;
    .locals 1

    .prologue
    .line 2482487
    sget-object v0, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    return-object v0
.end method

.method public final o()LX/HYi;
    .locals 1

    .prologue
    .line 2482486
    sget-object v0, LX/HYi;->PHONE:LX/HYi;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 2482483
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationInputFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2482484
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v()V

    .line 2482485
    return-void
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 2482482
    const v0, 0x7f0311bf

    return v0
.end method

.method public final r()[Landroid/widget/EditText;
    .locals 3

    .prologue
    .line 2482481
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final s()V
    .locals 5

    .prologue
    .line 2482469
    iget-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->w:Z

    if-nez v0, :cond_0

    .line 2482470
    :goto_0
    return-void

    .line 2482471
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->w:Z

    .line 2482472
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->p:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2482473
    iget-object v1, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->d:LX/HZt;

    invoke-virtual {p0}, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->o()LX/HYi;

    move-result-object v2

    invoke-virtual {v2}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->t:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, LX/HZt;->a(Ljava/lang/String;II)V

    .line 2482474
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->d:LX/HZt;

    const-string v1, "COUNTRY_CODE"

    iget-object v2, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->v:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;->u:Ljava/lang/String;

    .line 2482475
    sget-object v4, LX/HZu;->PREFILL:LX/HZu;

    invoke-static {v0, v4}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2482476
    const-string p0, "step_name"

    invoke-virtual {v4, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2482477
    const-string p0, "prefilled_value"

    invoke-virtual {v4, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2482478
    const-string p0, "used_value"

    invoke-virtual {v4, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2482479
    iget-object p0, v0, LX/HZt;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2482480
    goto :goto_0
.end method
