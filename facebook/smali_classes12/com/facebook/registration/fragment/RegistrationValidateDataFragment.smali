.class public Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;
.super Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HZv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2482799
    const-class v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2482798
    invoke-direct {p0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;-><init>()V

    return-void
.end method

.method public static p(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482793
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2482794
    iget-object p0, v0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    move-object v0, p0

    .line 2482795
    if-nez v0, :cond_0

    .line 2482796
    sget-object v0, LX/HYi;->UNKNOWN:LX/HYi;

    .line 2482797
    :cond_0
    invoke-virtual {v0}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2482800
    invoke-super {p0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Landroid/os/Bundle;)V

    .line 2482801
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    invoke-static {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v2

    check-cast v2, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v3

    check-cast v3, LX/HZt;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object p1

    check-cast p1, LX/0aG;

    invoke-static {v0}, LX/HZv;->a(LX/0QB;)LX/HZv;

    move-result-object v0

    check-cast v0, LX/HZv;

    iput-object v2, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    iput-object v3, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    iput-object p1, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->d:LX/0aG;

    iput-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->e:LX/HZv;

    .line 2482802
    return-void
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2482792
    const v0, 0x7f08359b

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2482789
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/SimpleRegFormData;->q()V

    .line 2482790
    sget-object v0, LX/HYj;->ERROR_CONTINUE:LX/HYj;

    invoke-virtual {p0, v0}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482791
    return-void
.end method

.method public final lO_()I
    .locals 1

    .prologue
    .line 2482788
    const v0, 0x7f083594

    return v0
.end method

.method public final m()V
    .locals 10

    .prologue
    .line 2482757
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    const/4 v1, 0x0

    .line 2482758
    iget-object v2, v0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    if-nez v2, :cond_1

    .line 2482759
    :goto_0
    move-object v0, v1

    .line 2482760
    if-nez v0, :cond_0

    .line 2482761
    sget-object v1, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    invoke-virtual {p0, v1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482762
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2482763
    const-string v1, "registrationValidateRegistrationDataParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2482764
    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->e:LX/HZv;

    invoke-static {p0}, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->p(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x1

    .line 2482765
    goto/16 :goto_3

    .line 2482766
    :goto_1
    iget-object v5, v0, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v6, LX/0Yj;

    const v7, 0x400002

    const-string v8, "RegistrationStepValidationTime"

    invoke-direct {v6, v7, v8}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 2482767
    iput-object v1, v6, LX/0Yj;->e:Ljava/lang/String;

    .line 2482768
    move-object v6, v6

    .line 2482769
    const-string v7, "step_info"

    invoke-virtual {v6, v7, v4}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    move-result-object v4

    invoke-virtual {v4}, LX/0Yj;->b()LX/0Yj;

    move-result-object v4

    .line 2482770
    iput-boolean v9, v4, LX/0Yj;->n:Z

    .line 2482771
    move-object v4, v4

    .line 2482772
    invoke-interface {v5, v4, v9}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 2482773
    iget-object v6, p0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->m:LX/1Ck;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->d:LX/0aG;

    const-string v1, "registration_validate_registration_data"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->f:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x154fbde4

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/HZr;

    invoke-direct {v1, p0}, LX/HZr;-><init>(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2482774
    return-void

    .line 2482775
    :cond_1
    new-instance v2, Lcom/facebook/registration/model/RegistrationFormData;

    invoke-direct {v2}, Lcom/facebook/registration/model/RegistrationFormData;-><init>()V

    .line 2482776
    sget-object v3, LX/Ha4;->a:[I

    iget-object v4, v0, Lcom/facebook/registration/model/SimpleRegFormData;->c:LX/HYi;

    invoke-virtual {v4}, LX/HYi;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 2482777
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v1

    .line 2482778
    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    .line 2482779
    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v3, :cond_2

    .line 2482780
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setEmail(Ljava/lang/String;)V

    :goto_2
    move-object v1, v2

    .line 2482781
    goto :goto_0

    .line 2482782
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2482783
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setFirstName(Ljava/lang/String;)V

    .line 2482784
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setLastName(Ljava/lang/String;)V

    goto :goto_2

    .line 2482785
    :pswitch_2
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayYear()I

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayMonth()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getBirthdayDay()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/facebook/registration/model/RegistrationFormData;->a(III)V

    goto :goto_2

    .line 2482786
    :pswitch_3
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getGender()LX/F8q;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->setGender(LX/F8q;)V

    goto :goto_2

    .line 2482787
    :pswitch_4
    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/registration/model/RegistrationFormData;->c(Ljava/lang/String;)V

    goto :goto_2

    :goto_3
    move-object v4, v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
