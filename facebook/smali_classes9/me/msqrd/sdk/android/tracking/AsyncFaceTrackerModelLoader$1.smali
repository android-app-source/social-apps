.class public final Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/BZP;


# direct methods
.method public constructor <init>(LX/BZP;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797232
    iput-object p1, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->e:LX/BZP;

    iput-object p2, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->c:Ljava/lang/String;

    iput-object p5, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1797233
    iget-object v0, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->e:LX/BZP;

    iget-object v1, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->a:Landroid/content/Context;

    iget-object v2, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->b:Ljava/lang/String;

    iget-object v3, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->c:Ljava/lang/String;

    iget-object v4, p0, Lme/msqrd/sdk/android/tracking/AsyncFaceTrackerModelLoader$1;->d:Ljava/lang/String;

    .line 1797234
    :try_start_0
    const-string v5, "msqrd-1.bin"

    invoke-static {v1, v2, v5}, LX/7ew;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 1797235
    const-string v6, "msqrd-2.bin"

    invoke-static {v1, v3, v6}, LX/7ew;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 1797236
    const-string v7, "msqrd-3.bin"

    invoke-static {v1, v4, v7}, LX/7ew;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 1797237
    iget-object v8, v0, LX/BZP;->a:LX/BZQ;

    if-eqz v8, :cond_0

    .line 1797238
    iget-object v8, v0, LX/BZP;->a:LX/BZQ;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, v10, p0}, LX/BZQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1797239
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 1797240
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 1797241
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1797242
    :goto_0
    return-void

    .line 1797243
    :catch_0
    move-exception v5

    .line 1797244
    const-string v6, "AsyncFaceTrackerModelLoader"

    const-string v7, "Error while loading msqrd models from asset."

    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
