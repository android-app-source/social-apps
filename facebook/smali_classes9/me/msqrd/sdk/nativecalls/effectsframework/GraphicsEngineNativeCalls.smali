.class public Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 1797289
    invoke-static {}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->a()V

    .line 1797290
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1797288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()V
    .locals 1

    .prologue
    .line 1797286
    const-string v0, "filters-native"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1797287
    return-void
.end method

.method public static native deleteInstance(J)V
.end method

.method public static native doFrame(J[F[F[FJ)V
.end method

.method public static native getCurrentEffectNeedsFaceTracker(J)Z
.end method

.method public static native getFacesCount(J)I
.end method

.method public static native isFaceTrackerReady(J)Z
.end method

.method public static native loadModels(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native newInstance()J
.end method

.method public static native prepareGl(JZZIIIZ)V
.end method

.method public static native releaseGl(J)V
.end method

.method public static native releaseModels(J)V
.end method

.method public static native resize(JII)V
.end method

.method public static native setEffect(JLjava/lang/String;)V
.end method

.method public static native setUpImageSourceFacet(JIIIIZ)V
.end method

.method public static native writeImage(J[B)V
.end method

.method public static native writeImageByteBuffer(JLjava/nio/ByteBuffer;I)V
.end method
