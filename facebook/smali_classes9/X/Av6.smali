.class public final LX/Av6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Av8;


# direct methods
.method public constructor <init>(LX/Av8;)V
    .locals 0

    .prologue
    .line 1723721
    iput-object p1, p0, LX/Av6;->a:LX/Av8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1723722
    iget-object v0, p0, LX/Av6;->a:LX/Av8;

    iget-object v0, v0, LX/Av8;->d:LX/03V;

    const-class v1, LX/Av8;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error fetching inspirations"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1723723
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1723724
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 1723725
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1723726
    iget-object v4, p0, LX/Av6;->a:LX/Av8;

    iget-object v4, v4, LX/Av8;->b:LX/Av5;

    invoke-virtual {v4, v0}, LX/Av5;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V

    .line 1723727
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1723728
    :cond_0
    return-void
.end method
