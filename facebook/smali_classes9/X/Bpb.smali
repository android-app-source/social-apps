.class public final LX/Bpb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bpc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/Bpe;

.field public c:LX/1Ps;

.field public final synthetic d:LX/Bpc;


# direct methods
.method public constructor <init>(LX/Bpc;)V
    .locals 1

    .prologue
    .line 1823334
    iput-object p1, p0, LX/Bpb;->d:LX/Bpc;

    .line 1823335
    move-object v0, p1

    .line 1823336
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1823337
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1823338
    const-string v0, "PhotosFeedBackgroundComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1823339
    if-ne p0, p1, :cond_1

    .line 1823340
    :cond_0
    :goto_0
    return v0

    .line 1823341
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1823342
    goto :goto_0

    .line 1823343
    :cond_3
    check-cast p1, LX/Bpb;

    .line 1823344
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1823345
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1823346
    if-eq v2, v3, :cond_0

    .line 1823347
    iget-object v2, p0, LX/Bpb;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bpb;->a:LX/1X1;

    iget-object v3, p1, LX/Bpb;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1823348
    goto :goto_0

    .line 1823349
    :cond_5
    iget-object v2, p1, LX/Bpb;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1823350
    :cond_6
    iget-object v2, p0, LX/Bpb;->b:LX/Bpe;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bpb;->b:LX/Bpe;

    iget-object v3, p1, LX/Bpb;->b:LX/Bpe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1823351
    goto :goto_0

    .line 1823352
    :cond_8
    iget-object v2, p1, LX/Bpb;->b:LX/Bpe;

    if-nez v2, :cond_7

    .line 1823353
    :cond_9
    iget-object v2, p0, LX/Bpb;->c:LX/1Ps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Bpb;->c:LX/1Ps;

    iget-object v3, p1, LX/Bpb;->c:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1823354
    goto :goto_0

    .line 1823355
    :cond_a
    iget-object v2, p1, LX/Bpb;->c:LX/1Ps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1823356
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Bpb;

    .line 1823357
    iget-object v1, v0, LX/Bpb;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Bpb;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Bpb;->a:LX/1X1;

    .line 1823358
    return-object v0

    .line 1823359
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
