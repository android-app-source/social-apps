.class public LX/CFw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1864717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(F)I
    .locals 4

    .prologue
    .line 1864716
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static a(LX/0Px;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1864708
    if-nez p0, :cond_0

    .line 1864709
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    .line 1864710
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v6, :cond_1

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    :cond_1
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1864711
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 1864712
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v2

    invoke-virtual {p0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v0

    invoke-static {v1, v2, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    .line 1864713
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 1864714
    goto :goto_0

    .line 1864715
    :cond_3
    invoke-virtual {p0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v3

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v2

    invoke-virtual {p0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/CFw;->a(F)I

    move-result v0

    invoke-static {v3, v1, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/greetingcards/verve/model/VMColor;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 1864680
    if-nez p0, :cond_0

    .line 1864681
    const/4 v0, 0x0

    .line 1864682
    :goto_0
    return-object v0

    .line 1864683
    :cond_0
    const-string v0, "linear-gradient"

    iget-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1864684
    const/4 v2, 0x0

    .line 1864685
    iget-object v3, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->locations:LX/0Px;

    .line 1864686
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->colors:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1864687
    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_1

    .line 1864688
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->colors:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-static {v0}, LX/CFw;->a(LX/0Px;)I

    move-result v0

    aput v0, v4, v1

    .line 1864689
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1864690
    :cond_1
    if-nez v3, :cond_5

    .line 1864691
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    move v0, v2

    .line 1864692
    :goto_2
    array-length v3, v4

    if-ge v0, v3, :cond_2

    .line 1864693
    int-to-float v3, v0

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1864694
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1864695
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1864696
    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    new-array v3, v0, [F

    .line 1864697
    :goto_4
    array-length v0, v3

    if-ge v2, v0, :cond_3

    .line 1864698
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v3, v2

    .line 1864699
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1864700
    :cond_3
    new-instance v0, LX/CFv;

    invoke-direct {v0, p0, v4, v3}, LX/CFv;-><init>(Lcom/facebook/greetingcards/verve/model/VMColor;[I[F)V

    .line 1864701
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    .line 1864702
    new-instance v2, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1864703
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1864704
    move-object v0, v1

    .line 1864705
    goto :goto_0

    .line 1864706
    :cond_4
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, LX/CFw;->b(Lcom/facebook/greetingcards/verve/model/VMColor;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto/16 :goto_0

    :cond_5
    move-object v1, v3

    goto :goto_3
.end method

.method public static b(Lcom/facebook/greetingcards/verve/model/VMColor;)I
    .locals 1

    .prologue
    .line 1864707
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->color:LX/0Px;

    invoke-static {v0}, LX/CFw;->a(LX/0Px;)I

    move-result v0

    return v0
.end method
