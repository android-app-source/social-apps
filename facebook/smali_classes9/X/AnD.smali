.class public LX/AnD;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AnC;


# instance fields
.field public a:Lcom/facebook/widget/CustomViewPager;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/friends/ui/SmartButtonLite;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:LX/AnF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1712163
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1712164
    const/4 p1, 0x0

    .line 1712165
    const v0, 0x7f030890

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1712166
    invoke-virtual {p0}, LX/AnD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1712167
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1712168
    invoke-virtual {p0}, LX/AnD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b09af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1712169
    invoke-virtual {p0, p1, p1, p1, v0}, LX/AnD;->setPadding(IIII)V

    .line 1712170
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 p1, -0x2

    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 1712171
    invoke-virtual {p0, v0}, LX/AnD;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1712172
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/AnD;->setOrientation(I)V

    .line 1712173
    const v0, 0x7f0d1637

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    .line 1712174
    const v0, 0x7f0d15bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/AnD;->b:Landroid/widget/TextView;

    .line 1712175
    const v0, 0x7f0d1634

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AnD;->f:Landroid/view/View;

    .line 1712176
    const v0, 0x7f0d1635

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AnD;->c:Landroid/view/View;

    .line 1712177
    const v0, 0x7f0d1636

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AnD;->d:Landroid/view/View;

    .line 1712178
    const v0, 0x7f0d1639

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/AnD;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1712179
    const v0, 0x7f0d1638

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AnD;->g:Landroid/view/View;

    .line 1712180
    iget-object v0, p0, LX/AnD;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1712181
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 6

    .prologue
    .line 1712182
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1712183
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1712184
    iget-object v0, p0, LX/AnD;->h:LX/AnF;

    iget-object v1, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    check-cast v1, LX/AnE;

    iget-object v2, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/AnF;->a(LX/AnE;Landroid/support/v4/view/ViewPager;Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 1712185
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1712186
    iget-object v0, p0, LX/AnD;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1712187
    return-void
.end method

.method public final lY_()V
    .locals 2

    .prologue
    .line 1712188
    iget-object v0, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 1712189
    if-eqz v0, :cond_0

    instance-of v0, v0, LX/AnE;

    if-nez v0, :cond_1

    .line 1712190
    :cond_0
    :goto_0
    return-void

    .line 1712191
    :cond_1
    iget-object v0, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    check-cast v0, LX/AnE;

    .line 1712192
    iget-object v1, p0, LX/AnD;->h:LX/AnF;

    if-eqz v1, :cond_0

    .line 1712193
    iget-object v1, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-static {v0, v1}, LX/AnF;->a(LX/AnE;Landroid/support/v4/view/ViewPager;)V

    goto :goto_0
.end method

.method public setActionInstagramButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1712194
    iget-object v0, p0, LX/AnD;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1712195
    return-void
.end method

.method public setMenuButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1712196
    iget-object v1, p0, LX/AnD;->d:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1712197
    iget-object v0, p0, LX/AnD;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1712198
    return-void

    .line 1712199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 1

    .prologue
    .line 1712200
    iget-object v0, p0, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1712201
    return-void
.end method

.method public setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1712202
    iget-object v1, p0, LX/AnD;->c:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1712203
    iget-object v0, p0, LX/AnD;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1712204
    return-void

    .line 1712205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
