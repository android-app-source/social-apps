.class public LX/BHq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private c:Landroid/content/res/Resources;

.field private d:Landroid/view/ViewStub;

.field public e:Landroid/view/View;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;)V
    .locals 1

    .prologue
    .line 1769664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769665
    iget v0, p2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    move v0, v0

    .line 1769666
    iput v0, p0, LX/BHq;->a:I

    .line 1769667
    iget v0, p2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    move v0, v0

    .line 1769668
    iput v0, p0, LX/BHq;->b:I

    .line 1769669
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/BHq;->c:Landroid/content/res/Resources;

    .line 1769670
    const v0, 0x7f0d2c84

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/BHq;->d:Landroid/view/ViewStub;

    .line 1769671
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1769672
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769673
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1769674
    :cond_0
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 1769675
    :cond_1
    :goto_0
    return-void

    .line 1769676
    :cond_2
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object v4, p0, LX/BHq;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1769677
    new-instance v1, LX/BHp;

    invoke-direct {v1, p0}, LX/BHp;-><init>(LX/BHq;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1769678
    move-object v0, v0

    .line 1769679
    iput-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    .line 1769680
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1769681
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1769682
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1769683
    :cond_0
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1769684
    iget-object v0, p0, LX/BHq;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BHq;->e:Landroid/view/View;

    .line 1769685
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    const v1, 0x7f0d2caf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/BHq;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1769686
    :cond_1
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/BHq;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1769687
    :goto_0
    return-void

    .line 1769688
    :cond_2
    iget-object v0, p0, LX/BHq;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1769689
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    invoke-virtual {v0, v2, v2}, Landroid/view/View;->measure(II)V

    .line 1769690
    iget-object v0, p0, LX/BHq;->e:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object p1, p0, LX/BHq;->e:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    aput p1, v2, v3

    const/4 v3, 0x1

    const/4 p1, 0x0

    aput p1, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1769691
    new-instance v1, LX/BHo;

    invoke-direct {v1, p0}, LX/BHo;-><init>(LX/BHq;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1769692
    move-object v0, v0

    .line 1769693
    iput-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    .line 1769694
    iget-object v0, p0, LX/BHq;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1769695
    if-nez p1, :cond_0

    .line 1769696
    invoke-direct {p0}, LX/BHq;->a()V

    .line 1769697
    :goto_0
    return-void

    .line 1769698
    :cond_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/BHq;->a:I

    if-ge v0, v1, :cond_1

    .line 1769699
    iget-object v0, p0, LX/BHq;->c:Landroid/content/res/Resources;

    const v1, 0x7f08137e

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LX/BHq;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BHq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1769700
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/BHq;->b:I

    if-le v0, v1, :cond_2

    .line 1769701
    iget-object v0, p0, LX/BHq;->c:Landroid/content/res/Resources;

    const v1, 0x7f08137f

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LX/BHq;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BHq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1769702
    :cond_2
    invoke-direct {p0}, LX/BHq;->a()V

    goto :goto_0
.end method
