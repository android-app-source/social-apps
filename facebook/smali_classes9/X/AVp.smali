.class public LX/AVp;
.super LX/AVk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVk",
        "<",
        "Lcom/facebook/facecastdisplay/LiveVideoStatusView;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680412
    invoke-direct {p0}, LX/AVk;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1680413
    check-cast p1, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x1

    .line 1680414
    invoke-virtual {p1, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsLiveNow(Z)V

    .line 1680415
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setLiveIndicatorClickable(Z)V

    .line 1680416
    invoke-virtual {p1, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setShowViewerCount(Z)V

    .line 1680417
    invoke-virtual {p0}, LX/AVj;->e()V

    .line 1680418
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1680419
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVp;->e:LX/AW2;

    .line 1680420
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680421
    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v1, p0, LX/AVj;->b:LX/AW2;

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 1680422
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680423
    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setVisibility(I)V

    .line 1680424
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1680425
    invoke-super {p0}, LX/AVk;->e()V

    .line 1680426
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVp;->b:LX/AW2;

    .line 1680427
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680428
    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setViewerCount(I)V

    .line 1680429
    return-void
.end method
