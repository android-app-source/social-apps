.class public LX/C83;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/C87;

.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/C87;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C87;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1852279
    iput-object p1, p0, LX/C83;->a:LX/C87;

    .line 1852280
    iput-object p2, p0, LX/C83;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1852281
    iput-object p3, p0, LX/C83;->c:LX/0Ot;

    .line 1852282
    return-void
.end method

.method public static a(LX/0QB;)LX/C83;
    .locals 6

    .prologue
    .line 1852283
    const-class v1, LX/C83;

    monitor-enter v1

    .line 1852284
    :try_start_0
    sget-object v0, LX/C83;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852285
    sput-object v2, LX/C83;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852286
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852287
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852288
    new-instance v5, LX/C83;

    invoke-static {v0}, LX/C87;->a(LX/0QB;)LX/C87;

    move-result-object v3

    check-cast v3, LX/C87;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const/16 p0, 0xb2d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/C83;-><init>(LX/C87;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Ot;)V

    .line 1852289
    move-object v0, v5

    .line 1852290
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852291
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C83;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852292
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
