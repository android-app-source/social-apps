.class public final LX/Axv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Axw;


# direct methods
.method public constructor <init>(LX/Axw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1729087
    iput-object p1, p0, LX/Axv;->b:LX/Axw;

    iput-object p2, p0, LX/Axv;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1729088
    iget-object v0, p0, LX/Axv;->b:LX/Axw;

    iget-object v0, v0, LX/Axw;->b:LX/AyG;

    iget-object v1, p0, LX/Axv;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Axv;->b:LX/Axw;

    iget-object v2, v2, LX/Axw;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1729089
    iget-object v4, v0, LX/AyG;->b:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 1729090
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "Give us a valid, non-empty ID"

    invoke-static {v4, v7}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1729091
    const-wide/16 v8, 0x0

    cmp-long v4, v2, v8

    if-ltz v4, :cond_1

    :goto_1
    const-string v4, "Please give us a valid UNIX timestamp"

    invoke-static {v5, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1729092
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1729093
    sget-object v5, LX/AyK;->a:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1729094
    sget-object v5, LX/AyK;->b:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729095
    iget-object v5, v0, LX/AyG;->a:LX/AyH;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "prompted_models"

    const-string v7, ""

    const v8, 0x717e6219

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v5, v6, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, 0x260c0d3

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1729096
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v4, v6

    .line 1729097
    goto :goto_0

    :cond_1
    move v5, v6

    .line 1729098
    goto :goto_1
.end method
