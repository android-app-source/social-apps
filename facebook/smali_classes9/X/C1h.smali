.class public final LX/C1h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1842911
    iput-object p1, p0, LX/C1h;->c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iput-object p2, p0, LX/C1h;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/C1h;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 3

    .prologue
    .line 1842912
    iget-object v0, p0, LX/C1h;->c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->j:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1842913
    iget-object v0, p0, LX/C1h;->c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->i:LX/AVV;

    const-string v1, "explicit"

    iget-object v2, p0, LX/C1h;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/AVV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1842914
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 3

    .prologue
    .line 1842915
    iget-object v0, p0, LX/C1h;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1842916
    iget-object v0, p0, LX/C1h;->c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->f:LX/Abd;

    iget-object v1, p2, LX/7Jv;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/Abd;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842917
    iget-object v0, p0, LX/C1h;->c:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->i:LX/AVV;

    const-string v1, "explicit"

    iget-object v2, p2, LX/7Jv;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1842918
    :cond_0
    return-void
.end method
