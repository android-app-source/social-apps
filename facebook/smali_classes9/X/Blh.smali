.class public LX/Blh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/Blh;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0id;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816444
    const-class v0, LX/Blh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Blh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0id;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0id;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1816476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816477
    iput-object p1, p0, LX/Blh;->b:LX/0Or;

    .line 1816478
    iput-object p2, p0, LX/Blh;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1816479
    iput-object p3, p0, LX/Blh;->d:LX/0id;

    .line 1816480
    return-void
.end method

.method public static a(LX/0QB;)LX/Blh;
    .locals 6

    .prologue
    .line 1816452
    sget-object v0, LX/Blh;->e:LX/Blh;

    if-nez v0, :cond_1

    .line 1816453
    const-class v1, LX/Blh;

    monitor-enter v1

    .line 1816454
    :try_start_0
    sget-object v0, LX/Blh;->e:LX/Blh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1816455
    if-eqz v2, :cond_0

    .line 1816456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1816457
    new-instance v5, LX/Blh;

    const/16 v3, 0xc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-direct {v5, p0, v3, v4}, LX/Blh;-><init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0id;)V

    .line 1816458
    move-object v0, v5

    .line 1816459
    sput-object v0, LX/Blh;->e:LX/Blh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1816460
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1816461
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1816462
    :cond_1
    sget-object v0, LX/Blh;->e:LX/Blh;

    return-object v0

    .line 1816463
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1816464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Blh;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 3
    .param p2    # Lcom/facebook/events/common/EventActionContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1816465
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1816466
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1816467
    const-string v0, "event_id"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816468
    if-eqz p2, :cond_0

    .line 1816469
    const-string v0, "extras_event_action_context"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1816470
    :cond_0
    if-eqz p3, :cond_1

    .line 1816471
    const-string v0, "event_ref_mechanism"

    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816472
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/Blh;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1816473
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1816474
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1816475
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V
    .locals 1

    .prologue
    .line 1816450
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)V

    .line 1816451
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 2
    .param p3    # Lcom/facebook/events/common/EventActionContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/common/ActionMechanism;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1816445
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1816446
    iget-object v0, p0, LX/Blh;->d:LX/0id;

    sget-object v1, LX/Blh;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1816447
    invoke-static {p0, p2, p3, p4}, LX/Blh;->a(LX/Blh;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v0

    .line 1816448
    iget-object v1, p0, LX/Blh;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1816449
    return-void
.end method
