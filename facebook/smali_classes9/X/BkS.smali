.class public final LX/BkS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/BkU;


# direct methods
.method public constructor <init>(LX/BkU;)V
    .locals 0

    .prologue
    .line 1814237
    iput-object p1, p0, LX/BkS;->a:LX/BkU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1814238
    iget-object v0, p0, LX/BkS;->a:LX/BkU;

    .line 1814239
    iput-boolean v1, v0, LX/BkU;->m:Z

    .line 1814240
    iget-object v0, p0, LX/BkS;->a:LX/BkU;

    iget-object v0, v0, LX/BkU;->d:LX/BkO;

    .line 1814241
    iget-object v2, v0, LX/BkO;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, v0, LX/BkO;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    sget-object p0, LX/0ax;->fh:Ljava/lang/String;

    invoke-interface {v2, v3, p0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1814242
    const-string v3, "extra_photo_title_text"

    iget-object p0, v0, LX/BkO;->a:Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f082139

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814243
    iget-object v3, v0, LX/BkO;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x26b9

    iget-object p1, v0, LX/BkO;->a:Landroid/app/Activity;

    invoke-interface {v3, v2, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814244
    return v1
.end method
