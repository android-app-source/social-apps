.class public final synthetic LX/Aos;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1714950
    invoke-static {}, LX/Aou;->values()[LX/Aou;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Aos;->b:[I

    :try_start_0
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->SHARE_NOW:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->WRITE_POST:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->SHARE_EXTERNAL:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->COPY_LINK:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, LX/Aos;->b:[I

    sget-object v1, LX/Aou;->SHARE_TO_PAGE:LX/Aou;

    invoke-virtual {v1}, LX/Aou;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    .line 1714951
    :goto_5
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->values()[Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Aos;->a:[I

    :try_start_6
    sget-object v0, LX/Aos;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, LX/Aos;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    goto :goto_7

    :catch_1
    goto :goto_6

    :catch_2
    goto :goto_5

    :catch_3
    goto :goto_4

    :catch_4
    goto :goto_3

    :catch_5
    goto :goto_2

    :catch_6
    goto :goto_1

    :catch_7
    goto :goto_0
.end method
