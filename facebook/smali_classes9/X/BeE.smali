.class public final enum LX/BeE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BeE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BeE;

.field public static final enum DOESNT_HAVE_VALUE_FIELD:LX/BeE;

.field public static final enum HOURS_FIELD:LX/BeE;

.field public static final enum LOCATION:LX/BeE;

.field public static final enum MULTI_TEXT_FIELD:LX/BeE;

.field public static final enum MULTI_VALUE_FIELD:LX/BeE;

.field public static final enum NO_SECTION_HEADER:LX/BeE;

.field public static final enum PAGE_HEADER:LX/BeE;

.field public static final enum SECTION_TITLE:LX/BeE;

.field public static final enum TEXT_FIELD:LX/BeE;

.field public static final enum UNSUPPORTED:LX/BeE;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1804685
    new-instance v0, LX/BeE;

    const-string v1, "NO_SECTION_HEADER"

    invoke-direct {v0, v1, v3}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->NO_SECTION_HEADER:LX/BeE;

    .line 1804686
    new-instance v0, LX/BeE;

    const-string v1, "SECTION_TITLE"

    invoke-direct {v0, v1, v4}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->SECTION_TITLE:LX/BeE;

    .line 1804687
    new-instance v0, LX/BeE;

    const-string v1, "DOESNT_HAVE_VALUE_FIELD"

    invoke-direct {v0, v1, v5}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->DOESNT_HAVE_VALUE_FIELD:LX/BeE;

    .line 1804688
    new-instance v0, LX/BeE;

    const-string v1, "HOURS_FIELD"

    invoke-direct {v0, v1, v6}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->HOURS_FIELD:LX/BeE;

    .line 1804689
    new-instance v0, LX/BeE;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v7}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->LOCATION:LX/BeE;

    .line 1804690
    new-instance v0, LX/BeE;

    const-string v1, "MULTI_VALUE_FIELD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->MULTI_VALUE_FIELD:LX/BeE;

    .line 1804691
    new-instance v0, LX/BeE;

    const-string v1, "PAGE_HEADER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->PAGE_HEADER:LX/BeE;

    .line 1804692
    new-instance v0, LX/BeE;

    const-string v1, "TEXT_FIELD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->TEXT_FIELD:LX/BeE;

    .line 1804693
    new-instance v0, LX/BeE;

    const-string v1, "MULTI_TEXT_FIELD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->MULTI_TEXT_FIELD:LX/BeE;

    .line 1804694
    new-instance v0, LX/BeE;

    const-string v1, "UNSUPPORTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/BeE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeE;->UNSUPPORTED:LX/BeE;

    .line 1804695
    const/16 v0, 0xa

    new-array v0, v0, [LX/BeE;

    sget-object v1, LX/BeE;->NO_SECTION_HEADER:LX/BeE;

    aput-object v1, v0, v3

    sget-object v1, LX/BeE;->SECTION_TITLE:LX/BeE;

    aput-object v1, v0, v4

    sget-object v1, LX/BeE;->DOESNT_HAVE_VALUE_FIELD:LX/BeE;

    aput-object v1, v0, v5

    sget-object v1, LX/BeE;->HOURS_FIELD:LX/BeE;

    aput-object v1, v0, v6

    sget-object v1, LX/BeE;->LOCATION:LX/BeE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BeE;->MULTI_VALUE_FIELD:LX/BeE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BeE;->PAGE_HEADER:LX/BeE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BeE;->TEXT_FIELD:LX/BeE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BeE;->MULTI_TEXT_FIELD:LX/BeE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BeE;->UNSUPPORTED:LX/BeE;

    aput-object v2, v0, v1

    sput-object v0, LX/BeE;->$VALUES:[LX/BeE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1804697
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BeE;
    .locals 1

    .prologue
    .line 1804698
    const-class v0, LX/BeE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BeE;

    return-object v0
.end method

.method public static values()[LX/BeE;
    .locals 1

    .prologue
    .line 1804696
    sget-object v0, LX/BeE;->$VALUES:[LX/BeE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BeE;

    return-object v0
.end method
