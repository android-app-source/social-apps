.class public LX/AhD;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/AhB;

.field public final b:LX/3HT;

.field private final c:LX/AhK;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(LX/3HT;LX/AhK;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1702226
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1702227
    iput-object p1, p0, LX/AhD;->b:LX/3HT;

    .line 1702228
    iput-object p2, p0, LX/AhD;->c:LX/AhK;

    .line 1702229
    new-instance v0, LX/AhB;

    invoke-direct {v0, p0}, LX/AhB;-><init>(LX/AhD;)V

    iput-object v0, p0, LX/AhD;->a:LX/AhB;

    .line 1702230
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1702231
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1702232
    sget-object v1, LX/AhA;->a:[I

    invoke-static {}, LX/AhC;->values()[LX/AhC;

    move-result-object v2

    aget-object v2, v2, p2

    invoke-virtual {v2}, LX/AhC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1702233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1702234
    :pswitch_0
    const v1, 0x7f031623

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1702235
    new-instance v0, LX/AhL;

    invoke-direct {v0, v1}, LX/AhL;-><init>(Landroid/view/View;)V

    .line 1702236
    :goto_0
    return-object v0

    .line 1702237
    :pswitch_1
    const v1, 0x7f031625

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1702238
    iget-object v1, p0, LX/AhD;->c:LX/AhK;

    .line 1702239
    new-instance p0, LX/AhJ;

    invoke-static {v1}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v2

    check-cast v2, LX/154;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v0, v2, v3}, LX/AhJ;-><init>(Landroid/view/View;LX/154;Landroid/content/res/Resources;)V

    .line 1702240
    move-object v0, p0

    .line 1702241
    goto :goto_0

    .line 1702242
    :pswitch_2
    const v1, 0x7f031622

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1702243
    new-instance v0, LX/AhI;

    invoke-direct {v0, v1}, LX/AhI;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 1702244
    sget-object v0, LX/AhA;->a:[I

    invoke-static {}, LX/AhC;->values()[LX/AhC;

    move-result-object v1

    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/AhC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1702245
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1702246
    :pswitch_0
    check-cast p1, LX/AhL;

    iget-object v0, p0, LX/AhD;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;

    move-result-object v0

    const/4 v2, 0x0

    .line 1702247
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1702248
    iget-object v4, p1, LX/AhL;->l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

    new-instance p0, LX/Ac5;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, LX/Ac5;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x64

    invoke-virtual {p0, v1, v3}, LX/Ac5;->a(Ljava/lang/String;I)LX/Ac5;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/facecastdisplay/FacecastUserTileView;->setParam(LX/Ac5;)V

    .line 1702249
    iget-object v1, p1, LX/AhL;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1702250
    iget-object v3, p1, LX/AhL;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f021a25

    :goto_0
    invoke-static {v3, v2, v2, v1, v2}, LX/4lM;->a(Landroid/widget/TextView;IIII)V

    .line 1702251
    :goto_1
    :pswitch_1
    return-void

    .line 1702252
    :pswitch_2
    check-cast p1, LX/AhJ;

    iget v0, p0, LX/AhD;->f:I

    invoke-virtual {p0}, LX/AhD;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1702253
    iget-object v1, p1, LX/AhJ;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p1, LX/AhJ;->n:Landroid/content/res/Resources;

    const v3, 0x7f080c34

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget-object p2, p1, LX/AhJ;->l:LX/154;

    invoke-virtual {p2, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v4, p0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1702254
    goto :goto_1

    :cond_0
    move v1, v2

    .line 1702255
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final d()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1702256
    iget-object v1, p0, LX/AhD;->d:LX/0Px;

    if-nez v1, :cond_1

    .line 1702257
    :cond_0
    :goto_0
    return v0

    .line 1702258
    :cond_1
    iget v1, p0, LX/AhD;->f:I

    iget-object v2, p0, LX/AhD;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1702259
    iget v0, p0, LX/AhD;->f:I

    goto :goto_0

    .line 1702260
    :cond_2
    const/4 v1, -0x1

    move v2, v1

    move v1, v0

    .line 1702261
    :goto_1
    iget-object v0, p0, LX/AhD;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1702262
    iget-object v0, p0, LX/AhD;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1702263
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->k()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel$NodeModel;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1702264
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1702265
    :cond_4
    iget-object v0, p0, LX/AhD;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1702266
    sub-int v1, v0, v2

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 1702267
    iget v1, p0, LX/AhD;->f:I

    if-ne v1, v0, :cond_5

    iget v1, p0, LX/AhD;->e:I

    rem-int v1, v0, v1

    if-eqz v1, :cond_0

    .line 1702268
    :cond_5
    iget v1, p0, LX/AhD;->e:I

    div-int/2addr v0, v1

    iget v1, p0, LX/AhD;->e:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1702269
    invoke-virtual {p0}, LX/AhD;->d()I

    move-result v0

    .line 1702270
    if-ge p1, v0, :cond_0

    .line 1702271
    sget-object v0, LX/AhC;->PROFILE:LX/AhC;

    invoke-virtual {v0}, LX/AhC;->ordinal()I

    move-result v0

    .line 1702272
    :goto_0
    return v0

    .line 1702273
    :cond_0
    if-ne p1, v0, :cond_1

    iget v1, p0, LX/AhD;->f:I

    if-le v1, v0, :cond_1

    .line 1702274
    sget-object v0, LX/AhC;->PLUS:LX/AhC;

    invoke-virtual {v0}, LX/AhC;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1702275
    :cond_1
    sget-object v0, LX/AhC;->EMPTY:LX/AhC;

    invoke-virtual {v0}, LX/AhC;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1702276
    invoke-virtual {p0}, LX/AhD;->d()I

    move-result v3

    .line 1702277
    iget v2, p0, LX/AhD;->f:I

    if-le v2, v3, :cond_0

    move v2, v0

    .line 1702278
    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    add-int/2addr v0, v3

    .line 1702279
    if-nez v0, :cond_2

    .line 1702280
    const/4 v1, 0x1

    .line 1702281
    :goto_2
    move v0, v1

    .line 1702282
    iget v1, p0, LX/AhD;->e:I

    mul-int/2addr v0, v1

    return v0

    :cond_0
    move v2, v1

    .line 1702283
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1702284
    goto :goto_1

    :cond_2
    add-int/lit8 v1, v0, -0x1

    iget v2, p0, LX/AhD;->e:I

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
