.class public LX/C7g;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7e;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1851603
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C7g;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851604
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851605
    iput-object p1, p0, LX/C7g;->b:LX/0Ot;

    .line 1851606
    return-void
.end method

.method public static a(LX/0QB;)LX/C7g;
    .locals 4

    .prologue
    .line 1851607
    const-class v1, LX/C7g;

    monitor-enter v1

    .line 1851608
    :try_start_0
    sget-object v0, LX/C7g;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851609
    sput-object v2, LX/C7g;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851610
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851611
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851612
    new-instance v3, LX/C7g;

    const/16 p0, 0x1f9b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7g;-><init>(LX/0Ot;)V

    .line 1851613
    move-object v0, v3

    .line 1851614
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851615
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851616
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851617
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1851618
    check-cast p2, LX/C7f;

    .line 1851619
    iget-object v0, p0, LX/C7g;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;

    iget-object v1, p2, LX/C7f;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1851620
    if-nez v1, :cond_0

    .line 1851621
    :goto_0
    move-object v0, v2

    .line 1851622
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1d56

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1d56

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020a43

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    .line 1851623
    :cond_1
    iget-object v4, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->a:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    sget-object p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p0, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    const p0, 0x7f020c6c

    invoke-virtual {v4, p0}, LX/1nw;->h(I)LX/1nw;

    move-result-object v4

    sget-object p0, LX/1Up;->a:LX/1Up;

    invoke-virtual {v4, p0}, LX/1nw;->b(LX/1Up;)LX/1nw;

    move-result-object v4

    sget-object p0, LX/1Up;->h:LX/1Up;

    invoke-virtual {v4, p0}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1d5c

    invoke-interface {v4, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1d59

    invoke-interface {v4, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const/16 p0, 0x8

    const p2, 0x7f0b1d67

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    move-object v2, v4

    .line 1851624
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v2, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    const v4, 0x7f0b1d5a

    invoke-interface {v2, v6, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x3

    const v5, 0x7f0b1d52

    invoke-interface {v2, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x6

    const v5, 0x7f0b1d52

    invoke-interface {v2, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v4, 0x7f082a02

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, LX/1De;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1851625
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b1d54

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    move-object v4, v5

    .line 1851626
    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->n()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, ""

    .line 1851627
    :goto_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b1d55

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    move-object v2, v5

    .line 1851628
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {v1}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLGroup;)Ljava/util/List;

    move-result-object v4

    .line 1851629
    iget-object v5, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->b:LX/8yV;

    invoke-virtual {v5, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d65

    invoke-virtual {v5, v6}, LX/8yU;->l(I)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d64

    invoke-virtual {v5, v6}, LX/8yU;->h(I)LX/8yU;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, LX/8yU;->m(I)LX/8yU;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/8yU;->d(F)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d66

    invoke-virtual {v5, v6}, LX/8yU;->k(I)LX/8yU;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x1

    const v7, 0x7f0b1d52

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v4, v5

    .line 1851630
    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1851631
    const v3, 0x75b4ed1

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1851632
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->n()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_3
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1851633
    invoke-static {}, LX/1dS;->b()V

    .line 1851634
    iget v0, p1, LX/1dQ;->b:I

    .line 1851635
    packed-switch v0, :pswitch_data_0

    .line 1851636
    :goto_0
    return-object v2

    .line 1851637
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1851638
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1851639
    check-cast v1, LX/C7f;

    .line 1851640
    iget-object v3, p0, LX/C7g;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;

    iget-object v4, v1, LX/C7f;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1851641
    iget-object p1, v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1g8;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object p2

    const-string p0, "gmw_unit_desc_card_click"

    invoke-virtual {p1, p2, p0}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851642
    iget-object p1, v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeDescriptionCardSpec;->c:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->C:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p2, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1851643
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x75b4ed1
        :pswitch_0
    .end packed-switch
.end method
