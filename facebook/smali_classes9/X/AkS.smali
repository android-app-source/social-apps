.class public LX/AkS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/AkS;


# instance fields
.field private final a:LX/0oz;

.field public final b:LX/0tn;

.field private final c:LX/0ad;

.field public final d:Landroid/os/Handler;

.field private final e:Landroid/os/Handler;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0oz;LX/0tn;LX/0ad;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709109
    new-instance v0, LX/026;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/AkS;->f:Ljava/util/Map;

    .line 1709110
    iput-object p1, p0, LX/AkS;->a:LX/0oz;

    .line 1709111
    iput-object p2, p0, LX/AkS;->b:LX/0tn;

    .line 1709112
    iput-object p3, p0, LX/AkS;->c:LX/0ad;

    .line 1709113
    iput-object p4, p0, LX/AkS;->d:Landroid/os/Handler;

    .line 1709114
    iput-object p5, p0, LX/AkS;->e:Landroid/os/Handler;

    .line 1709115
    return-void
.end method

.method public static a(LX/0QB;)LX/AkS;
    .locals 9

    .prologue
    .line 1709116
    sget-object v0, LX/AkS;->g:LX/AkS;

    if-nez v0, :cond_1

    .line 1709117
    const-class v1, LX/AkS;

    monitor-enter v1

    .line 1709118
    :try_start_0
    sget-object v0, LX/AkS;->g:LX/AkS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1709119
    if-eqz v2, :cond_0

    .line 1709120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1709121
    new-instance v3, LX/AkS;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {v0}, LX/0tn;->a(LX/0QB;)LX/0tn;

    move-result-object v5

    check-cast v5, LX/0tn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/AkS;-><init>(LX/0oz;LX/0tn;LX/0ad;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1709122
    move-object v0, v3

    .line 1709123
    sput-object v0, LX/AkS;->g:LX/AkS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1709124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1709125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1709126
    :cond_1
    sget-object v0, LX/AkS;->g:LX/AkS;

    return-object v0

    .line 1709127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1709128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6

    .prologue
    .line 1709129
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1709130
    :cond_0
    :goto_0
    return-void

    .line 1709131
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 1709132
    if-eqz v0, :cond_0

    .line 1709133
    iget-object v0, p0, LX/AkS;->a:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    .line 1709134
    sget-object v1, LX/0p3;->EXCELLENT:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-gez v1, :cond_0

    sget-object v1, LX/0p3;->POOR:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1709135
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1709136
    new-instance v1, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$1;-><init>(LX/AkS;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1709137
    iget-object v2, p0, LX/AkS;->c:LX/0ad;

    sget v3, LX/0fe;->t:I

    const/16 v4, 0x1388

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 1709138
    iget-object v3, p0, LX/AkS;->f:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709139
    iget-object v3, p0, LX/AkS;->d:Landroid/os/Handler;

    int-to-long v4, v2

    const v2, -0x482d5759

    invoke-static {v3, v1, v4, v5, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1709140
    iget-object v2, p0, LX/AkS;->e:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;-><init>(LX/AkS;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/Runnable;)V

    const v0, 0x4d9537af    # 3.12931808E8f

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
