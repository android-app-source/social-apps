.class public final LX/BqS;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/BqS;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BqR;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/BqT;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824716
    const/4 v0, 0x0

    sput-object v0, LX/BqS;->a:LX/BqS;

    .line 1824717
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BqS;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1824703
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1824704
    new-instance v0, LX/BqT;

    invoke-direct {v0}, LX/BqT;-><init>()V

    iput-object v0, p0, LX/BqS;->c:LX/BqT;

    .line 1824705
    return-void
.end method

.method public static declared-synchronized q()LX/BqS;
    .locals 2

    .prologue
    .line 1824712
    const-class v1, LX/BqS;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BqS;->a:LX/BqS;

    if-nez v0, :cond_0

    .line 1824713
    new-instance v0, LX/BqS;

    invoke-direct {v0}, LX/BqS;-><init>()V

    sput-object v0, LX/BqS;->a:LX/BqS;

    .line 1824714
    :cond_0
    sget-object v0, LX/BqS;->a:LX/BqS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1824715
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1824708
    check-cast p2, LX/BqQ;

    .line 1824709
    iget-object v0, p2, LX/BqQ;->a:Ljava/lang/CharSequence;

    iget-boolean v1, p2, LX/BqQ;->b:Z

    const/4 p2, 0x0

    .line 1824710
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a004b

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b09c6

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    const p0, 0x7f0b09c6

    invoke-interface {v2, v3, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b09c6

    invoke-interface {v2, p2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const p0, 0x7f0b09c7

    invoke-interface {v2, v3, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    if-eqz v1, :cond_0

    const v2, 0x7f02069c

    :goto_0
    invoke-interface {v3, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const/16 v3, 0x1f

    invoke-interface {v2, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1824711
    return-object v0

    :cond_0
    const v2, 0x7f02069f

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1824706
    invoke-static {}, LX/1dS;->b()V

    .line 1824707
    const/4 v0, 0x0

    return-object v0
.end method
