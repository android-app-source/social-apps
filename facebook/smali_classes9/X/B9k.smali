.class public LX/B9k;
.super LX/B9j;
.source ""


# instance fields
.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Lcom/facebook/user/tiles/UserTileView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1752218
    invoke-direct {p0, p1}, LX/B9j;-><init>(Landroid/content/Context;)V

    .line 1752219
    const-class v0, LX/B9k;

    invoke-static {v0, p0}, LX/B9k;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1752220
    const v0, 0x7f0d31ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/B9k;->d:Lcom/facebook/user/tiles/UserTileView;

    .line 1752221
    const v0, 0x106000b

    invoke-virtual {p0, v0}, LX/B9k;->setBackgroundResource(I)V

    .line 1752222
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/B9k;

    const/16 p0, 0x15e7

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    iput-object v1, p1, LX/B9k;->c:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 2

    .prologue
    .line 1752223
    invoke-super {p0, p1, p2, p3}, LX/B9j;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 1752224
    iget-object v1, p0, LX/B9k;->d:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p0, LX/B9k;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1752225
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 1752226
    const v0, 0x7f031616

    invoke-super {p0, v0}, LX/B9j;->setContentView(I)V

    .line 1752227
    return-void
.end method
