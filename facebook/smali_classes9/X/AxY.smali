.class public LX/AxY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Locale;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/GregorianCalendar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Locale;",
            "LX/0Or",
            "<",
            "Ljava/util/GregorianCalendar;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728488
    iput-object p1, p0, LX/AxY;->a:Landroid/content/Context;

    .line 1728489
    iput-object p2, p0, LX/AxY;->b:Ljava/util/Locale;

    .line 1728490
    iput-object p3, p0, LX/AxY;->c:LX/0Or;

    .line 1728491
    return-void
.end method

.method private static a(LX/AxY;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1728485
    invoke-static {p1, p2}, LX/AxY;->b(II)I

    move-result v0

    .line 1728486
    iget-object v1, p0, LX/AxY;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f10003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v0, v1, v0

    return-object v0
.end method

.method private static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 4

    .prologue
    .line 1728481
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1728482
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1728483
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1728484
    invoke-static {p0, v0}, LX/AxY;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    return v0
.end method

.method private static b(II)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728480
    add-int/lit8 v0, p0, -0x1

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v0, p1

    return v0
.end method

.method private static b(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v0, 0x1

    .line 1728479
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 1728455
    iget-object v0, p0, LX/AxY;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1728456
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1728457
    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1728458
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1728459
    invoke-virtual {v3, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1728460
    iget-object v0, p0, LX/AxY;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1728461
    const/16 v4, 0xb

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1728462
    const/16 v5, 0xb

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 1728463
    const/4 v6, 0x7

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 1728464
    invoke-static {v2, v3}, LX/AxY;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/16 v7, 0x12

    if-lt v4, v7, :cond_0

    const/4 v7, 0x5

    if-gt v5, v7, :cond_0

    .line 1728465
    const/4 v0, 0x3

    invoke-static {p0, v6, v0}, LX/AxY;->a(LX/AxY;II)Ljava/lang/String;

    move-result-object v0

    .line 1728466
    :goto_0
    return-object v0

    .line 1728467
    :cond_0
    invoke-static {v2, v3}, LX/AxY;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1728468
    invoke-static {v2, v0}, LX/AxY;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1728469
    const v0, 0x7f08275f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1728470
    :cond_1
    invoke-static {v2, v0}, LX/AxY;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1728471
    const v0, 0x7f082760

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1728472
    :cond_2
    const/4 v0, 0x4

    if-lt v4, v0, :cond_3

    const/16 v0, 0xc

    if-ge v5, v0, :cond_3

    .line 1728473
    const/4 v0, 0x0

    invoke-static {p0, v6, v0}, LX/AxY;->a(LX/AxY;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1728474
    :cond_3
    const/16 v0, 0xc

    if-lt v4, v0, :cond_4

    const/16 v0, 0x12

    if-gt v5, v0, :cond_4

    .line 1728475
    const/4 v0, 0x1

    invoke-static {p0, v6, v0}, LX/AxY;->a(LX/AxY;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1728476
    :cond_4
    const/16 v0, 0x10

    if-lt v4, v0, :cond_5

    .line 1728477
    const/4 v0, 0x2

    invoke-static {p0, v6, v0}, LX/AxY;->a(LX/AxY;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1728478
    :cond_5
    const/4 v0, 0x7

    const/4 v1, 0x2

    iget-object v3, p0, LX/AxY;->b:Ljava/util/Locale;

    invoke-virtual {v2, v0, v1, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
