.class public LX/Aee;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AeJ;
.implements LX/Aeb;


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field private final b:LX/0Sh;

.field public final c:LX/Ag1;

.field private final d:LX/AaZ;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Aea;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Aea;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Aec;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field private m:I

.field private n:I

.field public o:I

.field private p:I

.field private q:I

.field public r:Z

.field public s:Z

.field private final t:LX/Aed;

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/AcX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/AfJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1697949
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "live_indicator_info_event_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Aee;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/Ag1;LX/1b4;LX/AaZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/AfG;LX/Afz;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1697950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697951
    new-instance v0, LX/Aed;

    invoke-direct {v0}, LX/Aed;-><init>()V

    iput-object v0, p0, LX/Aee;->t:LX/Aed;

    .line 1697952
    iput-object p1, p0, LX/Aee;->b:LX/0Sh;

    .line 1697953
    iput-object p2, p0, LX/Aee;->c:LX/Ag1;

    .line 1697954
    iput-object p4, p0, LX/Aee;->d:LX/AaZ;

    .line 1697955
    iput-object p5, p0, LX/Aee;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1697956
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Aee;->f:Ljava/util/List;

    .line 1697957
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Aee;->g:Ljava/util/List;

    .line 1697958
    invoke-virtual {p3}, LX/1b4;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697959
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    .line 1697960
    :cond_0
    iput-boolean v1, p0, LX/Aee;->s:Z

    .line 1697961
    invoke-direct {p0}, LX/Aee;->d()V

    .line 1697962
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Aee;->h:Ljava/util/List;

    .line 1697963
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0, p6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697964
    iget-object v0, p3, LX/1b4;->a:LX/0ad;

    sget-short v1, LX/1v6;->W:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1697965
    if-eqz v0, :cond_1

    .line 1697966
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0, p7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697967
    :cond_1
    return-void
.end method

.method public static a(LX/Aee;Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V
    .locals 1

    .prologue
    .line 1697968
    iget-object v0, p0, LX/Aee;->x:LX/AfJ;

    if-eqz v0, :cond_0

    .line 1697969
    iget-object v0, p0, LX/Aee;->x:LX/AfJ;

    invoke-virtual {v0, p1}, LX/AfJ;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V

    .line 1697970
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1698054
    iget-object v0, p0, LX/Aee;->d:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698055
    const/16 v0, 0x14

    iput v0, p0, LX/Aee;->n:I

    .line 1698056
    const/16 v0, 0xa

    iput v0, p0, LX/Aee;->m:I

    .line 1698057
    :goto_0
    return-void

    .line 1698058
    :cond_0
    const/16 v0, 0x3c

    iput v0, p0, LX/Aee;->n:I

    .line 1698059
    const/16 v0, 0xf

    iput v0, p0, LX/Aee;->m:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1697971
    iget-object v0, p0, LX/Aee;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697972
    iget-object v0, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1697973
    iget-object v0, p0, LX/Aee;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1697974
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aec;

    .line 1697975
    invoke-virtual {v0}, LX/Aec;->b()V

    goto :goto_0

    .line 1697976
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 5

    .prologue
    .line 1697977
    iget-object v0, p0, LX/Aee;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697978
    iget v0, p0, LX/Aee;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/Aee;->l:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 1697979
    :cond_0
    float-to-int v0, p1

    .line 1697980
    iput v0, p0, LX/Aee;->i:I

    .line 1697981
    add-int/lit8 v1, v0, -0x2

    iput v1, p0, LX/Aee;->o:I

    .line 1697982
    iput v0, p0, LX/Aee;->l:I

    .line 1697983
    add-int/lit8 v1, v0, -0x2

    iput v1, p0, LX/Aee;->j:I

    .line 1697984
    iget-object v1, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1697985
    iget-object v1, p0, LX/Aee;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1697986
    iget-object v1, p0, LX/Aee;->w:Ljava/util/Queue;

    if-eqz v1, :cond_1

    .line 1697987
    iget-object v1, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1697988
    :cond_1
    iget-object v1, p0, LX/Aee;->x:LX/AfJ;

    if-eqz v1, :cond_2

    .line 1697989
    iget-object v1, p0, LX/Aee;->x:LX/AfJ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/AfJ;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V

    .line 1697990
    :cond_2
    iget-object v1, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Aec;

    .line 1697991
    invoke-virtual {v1}, LX/Aec;->b()V

    .line 1697992
    invoke-virtual {v1, v0}, LX/Aec;->a(I)V

    goto :goto_0

    .line 1697993
    :cond_3
    iget-object v1, p0, LX/Aee;->v:LX/AcX;

    if-eqz v1, :cond_4

    .line 1697994
    iget-object v1, p0, LX/Aee;->v:LX/AcX;

    .line 1697995
    iget-object v2, v1, LX/AcX;->n:LX/AeS;

    invoke-virtual {v2}, LX/AeS;->d()V

    .line 1697996
    :cond_4
    float-to-int v0, p1

    iput v0, p0, LX/Aee;->l:I

    .line 1697997
    iget v0, p0, LX/Aee;->o:I

    add-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_5

    .line 1697998
    float-to-int v0, p1

    const/4 v4, 0x0

    .line 1697999
    iget-object v1, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1698000
    :cond_5
    :goto_1
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    if-eqz v0, :cond_7

    iget v0, p0, LX/Aee;->j:I

    add-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_7

    .line 1698001
    :goto_2
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->l()I

    move-result v0

    iget v1, p0, LX/Aee;->j:I

    if-ge v0, v1, :cond_6

    .line 1698002
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto :goto_2

    .line 1698003
    :cond_6
    float-to-int v0, p1

    .line 1698004
    iget-object v1, p0, LX/Aee;->w:Ljava/util/Queue;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1698005
    :cond_7
    :goto_3
    iget v0, p0, LX/Aee;->i:I

    iget v1, p0, LX/Aee;->m:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_9

    .line 1698006
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aec;

    .line 1698007
    invoke-virtual {v0}, LX/Aec;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1698008
    :cond_9
    return-void

    .line 1698009
    :cond_a
    iget v0, p0, LX/Aee;->p:I

    if-nez v0, :cond_9

    iget v0, p0, LX/Aee;->q:I

    if-nez v0, :cond_9

    .line 1698010
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aec;

    .line 1698011
    iget v2, p0, LX/Aee;->i:I

    iget v3, p0, LX/Aee;->n:I

    invoke-virtual {v0, v2, v3}, LX/Aec;->a(II)V

    goto :goto_4

    .line 1698012
    :cond_b
    iget-object v1, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Aea;

    .line 1698013
    invoke-interface {v1}, LX/Aea;->b()I

    move-result v2

    add-int/lit8 v3, v0, 0x2

    if-ge v2, v3, :cond_5

    .line 1698014
    iget-object v2, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1698015
    iget v2, p0, LX/Aee;->k:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_11

    iget-boolean v2, p0, LX/Aee;->r:Z

    if-nez v2, :cond_11

    invoke-interface {v1}, LX/Aea;->c()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1698016
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Aee;->r:Z

    .line 1698017
    iget v2, p0, LX/Aee;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/Aee;->k:I

    .line 1698018
    iget-object v2, p0, LX/Aee;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/Aee;->a:LX/0Tn;

    iget v4, p0, LX/Aee;->k:I

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1698019
    new-instance v2, LX/Afa;

    sget-object v3, LX/AfZ;->LIVE_INDICATOR_INFO:LX/AfZ;

    invoke-direct {v2, v3}, LX/Afa;-><init>(LX/AfZ;)V

    .line 1698020
    :goto_5
    move-object v3, v2

    .line 1698021
    const/4 v2, 0x0

    .line 1698022
    iget-boolean v4, p0, LX/Aee;->s:Z

    if-eqz v4, :cond_c

    instance-of v4, v1, LX/Aeu;

    if-eqz v4, :cond_c

    .line 1698023
    iget-object v4, p0, LX/Aee;->c:LX/Ag1;

    move-object v2, v1

    check-cast v2, LX/Aeu;

    invoke-virtual {v4, v2}, LX/Ag1;->a(LX/Aeu;)LX/Aen;

    move-result-object v2

    .line 1698024
    :cond_c
    if-nez v3, :cond_e

    if-nez v2, :cond_e

    .line 1698025
    iget-object v2, p0, LX/Aee;->v:LX/AcX;

    if-eqz v2, :cond_d

    .line 1698026
    iget-object v2, p0, LX/Aee;->v:LX/AcX;

    invoke-virtual {v2, v1}, LX/AcX;->a(LX/AeO;)V

    .line 1698027
    :cond_d
    :goto_6
    iput v0, p0, LX/Aee;->o:I

    goto/16 :goto_1

    .line 1698028
    :cond_e
    iget-object v4, p0, LX/Aee;->v:LX/AcX;

    if-eqz v4, :cond_d

    .line 1698029
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1698030
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1698031
    if-eqz v3, :cond_f

    .line 1698032
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1698033
    :cond_f
    if-eqz v2, :cond_10

    .line 1698034
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Aee;->s:Z

    .line 1698035
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1698036
    :cond_10
    iget-object v1, p0, LX/Aee;->v:LX/AcX;

    invoke-virtual {v1, v4}, LX/AcX;->a(Ljava/util/List;)V

    goto :goto_6

    :cond_11
    const/4 v2, 0x0

    goto :goto_5

    .line 1698037
    :cond_12
    iget-object v1, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    .line 1698038
    iget-object v2, p0, LX/Aee;->w:Ljava/util/Queue;

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->l()I

    move-result v2

    iget v3, p0, LX/Aee;->j:I

    if-le v2, v3, :cond_7

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->l()I

    move-result v2

    add-int/lit8 v3, v0, 0x2

    if-ge v2, v3, :cond_7

    .line 1698039
    iget-object v2, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 1698040
    invoke-static {p0, v1}, LX/Aee;->a(LX/Aee;Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V

    .line 1698041
    iput v0, p0, LX/Aee;->j:I

    goto/16 :goto_3
.end method

.method public final a(LX/0Px;ZI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 1698042
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    if-nez v0, :cond_4

    .line 1698043
    :cond_0
    if-eqz p2, :cond_3

    .line 1698044
    const/4 v0, 0x0

    move-object v1, v0

    .line 1698045
    :goto_0
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->l()I

    move-result v0

    if-ge v0, p3, :cond_1

    .line 1698046
    iget-object v0, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    move-object v1, v0

    goto :goto_0

    .line 1698047
    :cond_1
    if-eqz v1, :cond_2

    .line 1698048
    invoke-static {p0, v1}, LX/Aee;->a(LX/Aee;Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V

    .line 1698049
    :cond_2
    iput p3, p0, LX/Aee;->j:I

    .line 1698050
    :cond_3
    return-void

    .line 1698051
    :cond_4
    invoke-virtual {p1}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    .line 1698052
    iget-object v4, p0, LX/Aee;->w:Ljava/util/Queue;

    invoke-interface {v4, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1698053
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(LX/AcX;)V
    .locals 1

    .prologue
    .line 1697913
    iget-object v0, p0, LX/Aee;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697914
    iput-object p1, p0, LX/Aee;->v:LX/AcX;

    .line 1697915
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1697917
    iget-object v0, p0, LX/Aee;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697918
    iput-object p1, p0, LX/Aee;->u:Ljava/lang/String;

    .line 1697919
    const/4 v0, -0x1

    iput v0, p0, LX/Aee;->l:I

    .line 1697920
    iget-object v0, p0, LX/Aee;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Aee;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    iput v0, p0, LX/Aee;->k:I

    .line 1697921
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    if-eqz v0, :cond_0

    .line 1697922
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    invoke-virtual {v0}, LX/AcX;->h()V

    .line 1697923
    :cond_0
    iget-object v0, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aec;

    .line 1697924
    iput-object p0, v0, LX/Aec;->a:LX/Aeb;

    .line 1697925
    iget-object v2, p0, LX/Aee;->u:Ljava/lang/String;

    .line 1697926
    iput-object v2, v0, LX/Aec;->b:Ljava/lang/String;

    .line 1697927
    instance-of v2, v0, LX/AfG;

    if-eqz v2, :cond_1

    .line 1697928
    check-cast v0, LX/AfG;

    .line 1697929
    iput-object p0, v0, LX/AfG;->h:LX/Aee;

    .line 1697930
    goto :goto_0

    .line 1697931
    :cond_2
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Aea;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1697932
    if-eqz p2, :cond_3

    .line 1697933
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697934
    iget-object v0, p0, LX/Aee;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1697935
    iget-object v0, p0, LX/Aee;->g:Ljava/util/List;

    iget-object v1, p0, LX/Aee;->t:LX/Aed;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1697936
    :cond_0
    iget v0, p0, LX/Aee;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Aee;->q:I

    .line 1697937
    iget v0, p0, LX/Aee;->q:I

    iget-object v1, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1697938
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    if-eqz v0, :cond_1

    .line 1697939
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    iget-object v1, p0, LX/Aee;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/AcX;->a(Ljava/util/List;)V

    .line 1697940
    :cond_1
    iput v2, p0, LX/Aee;->q:I

    .line 1697941
    :cond_2
    :goto_0
    return-void

    .line 1697942
    :cond_3
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1697943
    iget-object v0, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1697944
    iget-object v0, p0, LX/Aee;->f:Ljava/util/List;

    iget-object v1, p0, LX/Aee;->t:LX/Aed;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1697945
    :cond_4
    iget v0, p0, LX/Aee;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Aee;->p:I

    .line 1697946
    iget v0, p0, LX/Aee;->p:I

    iget-object v1, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1697947
    iget v0, p0, LX/Aee;->i:I

    iget v1, p0, LX/Aee;->n:I

    add-int/2addr v0, v1

    iput v0, p0, LX/Aee;->i:I

    .line 1697948
    iput v2, p0, LX/Aee;->p:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1697916
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1697905
    iget-object v0, p0, LX/Aee;->d:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1697906
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1697907
    iget-object v0, p0, LX/Aee;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aea;

    .line 1697908
    instance-of v3, v0, LX/Aeu;

    if-eqz v3, :cond_0

    .line 1697909
    check-cast v0, LX/Aeu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1697910
    :cond_1
    iget-object v0, p0, LX/Aee;->c:LX/Ag1;

    invoke-virtual {v0, v1}, LX/Ag1;->a(Ljava/util/List;)V

    .line 1697911
    :cond_2
    invoke-direct {p0}, LX/Aee;->d()V

    .line 1697912
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1697904
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1697903
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1697902
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1697891
    if-eqz p1, :cond_2

    .line 1697892
    iget v0, p0, LX/Aee;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Aee;->q:I

    .line 1697893
    iget v0, p0, LX/Aee;->q:I

    iget-object v1, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1697894
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aee;->g:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697895
    iget-object v0, p0, LX/Aee;->v:LX/AcX;

    iget-object v1, p0, LX/Aee;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/AcX;->a(Ljava/util/List;)V

    .line 1697896
    :cond_0
    iput v2, p0, LX/Aee;->q:I

    .line 1697897
    :cond_1
    :goto_0
    return-void

    .line 1697898
    :cond_2
    iget v0, p0, LX/Aee;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Aee;->p:I

    .line 1697899
    iget v0, p0, LX/Aee;->p:I

    iget-object v1, p0, LX/Aee;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1697900
    iget v0, p0, LX/Aee;->i:I

    iget v1, p0, LX/Aee;->n:I

    add-int/2addr v0, v1

    iput v0, p0, LX/Aee;->i:I

    .line 1697901
    iput v2, p0, LX/Aee;->p:I

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1697890
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1697889
    return-void
.end method
