.class public final LX/AXM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AXY;


# direct methods
.method public constructor <init>(LX/AXY;)V
    .locals 0

    .prologue
    .line 1683922
    iput-object p1, p0, LX/AXM;->a:LX/AXY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x37d25096

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1683870
    iget-object v1, p0, LX/AXM;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->aj:LX/AXD;

    .line 1683871
    iget-boolean p1, v1, LX/AXD;->g:Z

    move v1, p1

    .line 1683872
    if-eqz v1, :cond_0

    .line 1683873
    const v1, -0x43748bd2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1683874
    :goto_0
    return-void

    .line 1683875
    :cond_0
    iget-object v1, p0, LX/AXM;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->aj:LX/AXD;

    .line 1683876
    iget-boolean v2, v1, LX/AXD;->f:Z

    move v1, v2

    .line 1683877
    if-nez v1, :cond_7

    .line 1683878
    iget-object v1, p0, LX/AXM;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->aj:LX/AXD;

    .line 1683879
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/AXD;->f:Z

    .line 1683880
    new-instance v4, LX/AX1;

    invoke-direct {v4}, LX/AX1;-><init>()V

    .line 1683881
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$1;

    invoke-direct {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$1;-><init>(LX/AXD;)V

    .line 1683882
    new-instance v3, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$2;

    invoke-direct {v3, v1}, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$2;-><init>(LX/AXD;)V

    .line 1683883
    invoke-virtual {v4, v2}, LX/AX1;->a(Ljava/lang/Runnable;)LX/AX1;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/AX1;->c(Ljava/lang/Runnable;)LX/AX1;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/AX1;->b(Ljava/lang/Runnable;)LX/AX1;

    move-result-object v2

    .line 1683884
    iget-object v5, v2, LX/AX1;->f:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1683885
    iget-object v2, v1, LX/AXD;->i:LX/AXP;

    if-eqz v2, :cond_1

    .line 1683886
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$4;

    invoke-direct {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$4;-><init>(LX/AXD;)V

    invoke-virtual {v4, v2}, LX/AX1;->a(Ljava/lang/Runnable;)LX/AX1;

    move-result-object v2

    new-instance v3, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$3;

    invoke-direct {v3, v1}, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$3;-><init>(LX/AXD;)V

    invoke-virtual {v2, v3}, LX/AX1;->b(Ljava/lang/Runnable;)LX/AX1;

    .line 1683887
    :cond_1
    invoke-static {v1}, LX/AXD;->g(LX/AXD;)F

    move-result v2

    .line 1683888
    iget-object v3, v1, LX/AXD;->c:Landroid/view/ViewGroup;

    .line 1683889
    invoke-static {v3, v2}, LX/AX3;->b(Landroid/view/View;F)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/AX1;->a(Landroid/animation/Animator;)LX/AX1;

    move-result-object v5

    const/4 p0, 0x0

    invoke-static {v3, p0}, LX/AX3;->b(Landroid/view/View;F)Landroid/animation/Animator;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/AX1;->b(Landroid/animation/Animator;)LX/AX1;

    .line 1683890
    new-instance v2, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$5;

    invoke-direct {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndScreenV2FullscreenTransitionController$5;-><init>(LX/AXD;)V

    invoke-virtual {v4, v2}, LX/AX1;->c(Ljava/lang/Runnable;)LX/AX1;

    .line 1683891
    iget-object v2, v1, LX/AXD;->e:Landroid/view/ViewGroup;

    invoke-static {v4, v2}, LX/AX3;->a(LX/AX1;Landroid/view/View;)V

    .line 1683892
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1683893
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1683894
    const/4 v2, 0x0

    :goto_1
    iget-object p0, v1, LX/AXD;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    if-ge v2, p0, :cond_3

    .line 1683895
    iget-object p0, v1, LX/AXD;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p0

    .line 1683896
    instance-of p1, p0, Landroid/widget/ScrollView;

    if-nez p1, :cond_2

    instance-of p1, p0, Landroid/view/ViewStub;

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_2

    .line 1683897
    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1683898
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1683899
    :cond_3
    move-object v2, v3

    .line 1683900
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1683901
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1683902
    const/4 v2, 0x0

    :goto_2
    iget-object p0, v1, LX/AXD;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    if-ge v2, p0, :cond_5

    .line 1683903
    iget-object p0, v1, LX/AXD;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p0

    .line 1683904
    instance-of p1, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    if-nez p1, :cond_4

    instance-of p1, p0, Landroid/view/ViewStub;

    if-nez p1, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_4

    .line 1683905
    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1683906
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1683907
    :cond_5
    move-object v2, v3

    .line 1683908
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1683909
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result p0

    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, p0, :cond_6

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1683910
    invoke-static {v4, v2}, LX/AX3;->a(LX/AX1;Landroid/view/View;)V

    .line 1683911
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1683912
    :cond_6
    iget-object v2, v1, LX/AXD;->a:Landroid/view/ViewGroup;

    const/4 v3, -0x1

    const/high16 v5, -0x1000000

    .line 1683913
    invoke-static {v2, v3, v5}, LX/AX3;->a(Landroid/view/View;II)Landroid/animation/Animator;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/AX1;->a(Landroid/animation/Animator;)LX/AX1;

    move-result-object p0

    invoke-static {v2, v5, v3}, LX/AX3;->a(Landroid/view/View;II)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/AX1;->b(Landroid/animation/Animator;)LX/AX1;

    .line 1683914
    iget-object v2, v4, LX/AX1;->a:Ljava/util/List;

    iget-object v3, v4, LX/AX1;->c:Ljava/util/List;

    iget-object v5, v4, LX/AX1;->e:Ljava/util/List;

    invoke-static {v4, v2, v3, v5}, LX/AX1;->a(LX/AX1;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Landroid/animation/AnimatorSet;

    move-result-object v2

    .line 1683915
    iget-object v3, v4, LX/AX1;->b:Ljava/util/List;

    iget-object v5, v4, LX/AX1;->d:Ljava/util/List;

    iget-object p0, v4, LX/AX1;->f:Ljava/util/List;

    invoke-static {v4, v3, v5, p0}, LX/AX1;->a(LX/AX1;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Landroid/animation/AnimatorSet;

    move-result-object v3

    .line 1683916
    new-instance v5, LX/AX2;

    invoke-direct {v5, v2, v3}, LX/AX2;-><init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;)V

    move-object v2, v5

    .line 1683917
    move-object v2, v2

    .line 1683918
    iput-object v2, v1, LX/AXD;->h:LX/AX2;

    .line 1683919
    iget-object v2, v1, LX/AXD;->h:LX/AX2;

    .line 1683920
    iget-object v3, v2, LX/AX2;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1683921
    :cond_7
    const v1, 0x51d0c7a8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
