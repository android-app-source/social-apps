.class public final LX/CJC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attribution/InlineReplyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V
    .locals 0

    .prologue
    .line 1875121
    iput-object p1, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1875122
    const-string v0, "InlineReplyFragment"

    const-string v1, "Failed to add metadata to media resources"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1875123
    iget-object v0, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1875124
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875113
    check-cast p1, Ljava/util/List;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1875114
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1875115
    iget-object v2, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1875116
    iput-object v0, v2, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1875117
    iget-object v0, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    iget-object v1, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    iget-object v1, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/CJG;->setMediaResource(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 1875118
    iget-object v0, p0, LX/CJC;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    iget-object v0, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    invoke-virtual {v0}, LX/CJG;->a()V

    .line 1875119
    return-void

    :cond_0
    move v0, v1

    .line 1875120
    goto :goto_0
.end method
