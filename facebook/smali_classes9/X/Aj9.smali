.class public final LX/Aj9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/1du;


# direct methods
.method public constructor <init>(LX/1du;Ljava/lang/String;LX/0TF;)V
    .locals 0

    .prologue
    .line 1707617
    iput-object p1, p0, LX/Aj9;->c:LX/1du;

    iput-object p2, p0, LX/Aj9;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Aj9;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1707618
    iget-object v0, p0, LX/Aj9;->c:LX/1du;

    iget-object v1, p0, LX/Aj9;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1du;->d(LX/1du;Ljava/lang/String;)V

    .line 1707619
    iget-object v0, p0, LX/Aj9;->c:LX/1du;

    iget-object v0, v0, LX/1du;->b:LX/03V;

    sget-object v1, LX/1du;->a:Ljava/lang/String;

    const-string v2, "Undo curation flow failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1707620
    iget-object v0, p0, LX/Aj9;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1707621
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1707622
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1707623
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 1707624
    iget-object v1, p0, LX/Aj9;->c:LX/1du;

    iget-object v2, p0, LX/Aj9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/1du;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707625
    iget-object v0, p0, LX/Aj9;->c:LX/1du;

    iget-object v1, p0, LX/Aj9;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1du;->d(LX/1du;Ljava/lang/String;)V

    .line 1707626
    iget-object v0, p0, LX/Aj9;->c:LX/1du;

    iget-object v0, v0, LX/1du;->f:Ljava/util/Map;

    iget-object v1, p0, LX/Aj9;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 1707627
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1707628
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1707629
    :cond_0
    iget-object v0, p0, LX/Aj9;->b:LX/0TF;

    if-eqz v0, :cond_1

    .line 1707630
    iget-object v0, p0, LX/Aj9;->b:LX/0TF;

    iget-object v1, p0, LX/Aj9;->c:LX/1du;

    iget-object v2, p0, LX/Aj9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1du;->b(Ljava/lang/String;)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1707631
    :cond_1
    return-void
.end method
