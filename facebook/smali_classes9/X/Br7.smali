.class public final LX/Br7;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Br8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/Br8;


# direct methods
.method public constructor <init>(LX/Br8;)V
    .locals 1

    .prologue
    .line 1825940
    iput-object p1, p0, LX/Br7;->d:LX/Br8;

    .line 1825941
    move-object v0, p1

    .line 1825942
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1825943
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1825939
    const-string v0, "ThumbnailWithTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1825922
    if-ne p0, p1, :cond_1

    .line 1825923
    :cond_0
    :goto_0
    return v0

    .line 1825924
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1825925
    goto :goto_0

    .line 1825926
    :cond_3
    check-cast p1, LX/Br7;

    .line 1825927
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1825928
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1825929
    if-eq v2, v3, :cond_0

    .line 1825930
    iget-object v2, p0, LX/Br7;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Br7;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/Br7;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1825931
    goto :goto_0

    .line 1825932
    :cond_5
    iget-object v2, p1, LX/Br7;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 1825933
    :cond_6
    iget-object v2, p0, LX/Br7;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Br7;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Br7;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1825934
    goto :goto_0

    .line 1825935
    :cond_8
    iget-object v2, p1, LX/Br7;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1825936
    :cond_9
    iget-object v2, p0, LX/Br7;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Br7;->c:Ljava/lang/String;

    iget-object v3, p1, LX/Br7;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1825937
    goto :goto_0

    .line 1825938
    :cond_a
    iget-object v2, p1, LX/Br7;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
