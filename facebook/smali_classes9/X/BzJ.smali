.class public final LX/BzJ;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/BzL;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/BzK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BzL",
            "<TE;>.",
            "LifeEventTitleTextComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/BzL;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/BzL;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1838541
    iput-object p1, p0, LX/BzJ;->b:LX/BzL;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1838542
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "flyoutClickSource"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "propertiesMapKey"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "textSize"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "textColor"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/BzJ;->c:[Ljava/lang/String;

    .line 1838543
    iput v3, p0, LX/BzJ;->d:I

    .line 1838544
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/BzJ;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/BzJ;LX/1De;IILX/BzK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/BzL",
            "<TE;>.",
            "LifeEventTitleTextComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1838537
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1838538
    iput-object p4, p0, LX/BzJ;->a:LX/BzK;

    .line 1838539
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1838540
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838534
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->c:LX/1Pq;

    .line 1838535
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838536
    return-object p0
.end method

.method public final a(LX/1eK;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eK;",
            ")",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838531
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->e:LX/1eK;

    .line 1838532
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838533
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838503
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838504
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838505
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838528
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1838529
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838530
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1838545
    invoke-super {p0}, LX/1X5;->a()V

    .line 1838546
    const/4 v0, 0x0

    iput-object v0, p0, LX/BzJ;->a:LX/BzK;

    .line 1838547
    iget-object v0, p0, LX/BzJ;->b:LX/BzL;

    iget-object v0, v0, LX/BzL;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1838548
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838525
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838526
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838527
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/BzJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838522
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    iput-object p1, v0, LX/BzK;->f:Ljava/lang/String;

    .line 1838523
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838524
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/BzL;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1838512
    iget-object v1, p0, LX/BzJ;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/BzJ;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/BzJ;->d:I

    if-ge v1, v2, :cond_2

    .line 1838513
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1838514
    :goto_0
    iget v2, p0, LX/BzJ;->d:I

    if-ge v0, v2, :cond_1

    .line 1838515
    iget-object v2, p0, LX/BzJ;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1838516
    iget-object v2, p0, LX/BzJ;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1838517
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1838518
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1838519
    :cond_2
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    .line 1838520
    invoke-virtual {p0}, LX/BzJ;->a()V

    .line 1838521
    return-object v0
.end method

.method public final h(I)LX/BzJ;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838509
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/BzK;->g:I

    .line 1838510
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838511
    return-object p0
.end method

.method public final j(I)LX/BzJ;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838506
    iget-object v0, p0, LX/BzJ;->a:LX/BzK;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/BzK;->h:I

    .line 1838507
    iget-object v0, p0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838508
    return-object p0
.end method
