.class public final LX/CUA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;)V
    .locals 2

    .prologue
    .line 1896272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896273
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896274
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896275
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896276
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896277
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896278
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896279
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CUA;->a:Ljava/lang/String;

    .line 1896280
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CUA;->b:Ljava/lang/String;

    .line 1896281
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CUA;->c:Ljava/lang/String;

    .line 1896282
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CUA;->d:Ljava/lang/String;

    .line 1896283
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v0

    iput-object v0, p0, LX/CUA;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 1896284
    return-void

    .line 1896285
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
