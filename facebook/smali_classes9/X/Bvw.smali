.class public final LX/Bvw;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;)V
    .locals 0

    .prologue
    .line 1832995
    iput-object p1, p0, LX/Bvw;->a:Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1832996
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1832997
    check-cast p1, LX/2ou;

    .line 1832998
    iget-object v0, p0, LX/Bvw;->a:Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bvw;->a:Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1832999
    :cond_0
    :goto_0
    return-void

    .line 1833000
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1833001
    iget-object v0, p0, LX/Bvw;->a:Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    sget-object v1, LX/Bvv;->PLAYBACK_COMPLETE:LX/Bvv;

    invoke-static {v0, v1}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V

    goto :goto_0
.end method
