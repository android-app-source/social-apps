.class public final LX/CT6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/CTC;


# direct methods
.method public constructor <init>(LX/CTC;)V
    .locals 0

    .prologue
    .line 1895275
    iput-object p1, p0, LX/CT6;->a:LX/CTC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1895276
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->b:LX/CSz;

    invoke-virtual {v0}, LX/CSz;->a()V

    .line 1895277
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    .line 1895278
    iget-object v1, v0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1895279
    invoke-static {v0}, LX/CTC;->r(LX/CTC;)V

    .line 1895280
    :cond_0
    iget-object v1, v0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1895281
    iget-object v2, v0, LX/CTC;->e:LX/CSc;

    iget-object v1, v0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    iget-object v1, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, LX/CUD;

    iget-object v1, v1, LX/CUD;->a:LX/CUA;

    iget-object v3, v1, LX/CUA;->a:Ljava/lang/String;

    iget-object v1, v0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    iget-object v1, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, LX/CSb;

    invoke-virtual {v2, v3, v1}, LX/CSc;->a(Ljava/lang/String;LX/CSb;)LX/CSb;

    .line 1895282
    :cond_1
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1895283
    invoke-virtual {p0}, LX/CT6;->b()V

    .line 1895284
    :goto_0
    return-void

    .line 1895285
    :cond_2
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    sget-object v1, LX/CTB;->READY:LX/CTB;

    invoke-static {v0, v1}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1895286
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    sget-object v1, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    invoke-static {v0, v1}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895287
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->b:LX/CSz;

    iget-object v1, p0, LX/CT6;->a:LX/CTC;

    iget-object v1, v1, LX/CTC;->p:Ljava/lang/String;

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->a:LX/CUA;

    iget-object v3, v2, LX/CUA;->a:Ljava/lang/String;

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->a:LX/CUA;

    iget-object v4, v2, LX/CUA;->b:Ljava/lang/String;

    const-string v5, "GET"

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->e:LX/CSc;

    invoke-virtual {v2}, LX/CSc;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v8, v2, LX/CTC;->g:LX/CT8;

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v9, v2, LX/CTC;->q:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v0 .. v9}, LX/CSz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CT8;Ljava/lang/String;)V

    .line 1895288
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1895289
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->b:LX/CSz;

    invoke-virtual {v0}, LX/CSz;->a()V

    .line 1895290
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->o:LX/CST;

    .line 1895291
    iget-object p0, v0, LX/CST;->a:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1895292
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1895293
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    sget-object v1, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    invoke-static {v0, v1}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895294
    iget-object v0, p0, LX/CT6;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->b:LX/CSz;

    iget-object v1, p0, LX/CT6;->a:LX/CTC;

    iget-object v1, v1, LX/CTC;->p:Ljava/lang/String;

    iget-object v2, p0, LX/CT6;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->a:LX/CUA;

    iget-object v2, v2, LX/CUA;->a:Ljava/lang/String;

    iget-object v3, p0, LX/CT6;->a:LX/CTC;

    iget-object v3, v3, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3rL;

    iget-object v3, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/CUD;

    iget-object v3, v3, LX/CUD;->a:LX/CUA;

    iget-object v3, v3, LX/CUA;->a:Ljava/lang/String;

    iget-object v4, p0, LX/CT6;->a:LX/CTC;

    iget-object v4, v4, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3rL;

    iget-object v4, v4, LX/3rL;->a:Ljava/lang/Object;

    check-cast v4, LX/CUD;

    iget-object v4, v4, LX/CUD;->a:LX/CUA;

    iget-object v4, v4, LX/CUA;->b:Ljava/lang/String;

    const-string v5, "POST"

    iget-object v6, p0, LX/CT6;->a:LX/CTC;

    iget-object v6, v6, LX/CTC;->e:LX/CSc;

    invoke-virtual {v6}, LX/CSc;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/CT6;->a:LX/CTC;

    iget-object v8, v7, LX/CTC;->g:LX/CT8;

    iget-object v7, p0, LX/CT6;->a:LX/CTC;

    iget-object v9, v7, LX/CTC;->q:Ljava/lang/String;

    move-object v7, p1

    invoke-virtual/range {v0 .. v9}, LX/CSz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CT8;Ljava/lang/String;)V

    .line 1895295
    return-void
.end method
