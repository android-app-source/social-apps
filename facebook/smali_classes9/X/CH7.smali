.class public LX/CH7;
.super LX/CH6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CH6",
        "<",
        "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/CH7;


# instance fields
.field public final a:LX/0Uh;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8bW;

.field public final d:LX/CjE;

.field public final e:LX/0ad;

.field public final f:LX/8bK;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6VQ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/CjG;

.field public final j:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/CH2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;LX/0Ot;LX/8bW;LX/CjE;LX/0ad;LX/8bK;LX/CjG;LX/0Ot;LX/8bL;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0t2;",
            "LX/0So;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;",
            "LX/8bW;",
            "LX/CjE;",
            "LX/0ad;",
            "LX/8bK;",
            "LX/CjG;",
            "LX/0Ot",
            "<",
            "LX/6VQ;",
            ">;",
            "LX/8bL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1866273
    const-wide/16 v8, 0x1

    sget-object v10, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    move-object v7, p3

    invoke-direct/range {v3 .. v10}, LX/CH6;-><init>(LX/0tX;LX/0t2;LX/0Uh;LX/0So;JLjava/util/concurrent/TimeUnit;)V

    .line 1866274
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x384

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, LX/CH7;->h:LX/0QI;

    .line 1866275
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, LX/0QN;->a(J)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, LX/CH7;->j:LX/0QI;

    .line 1866276
    iput-object p4, p0, LX/CH7;->a:LX/0Uh;

    .line 1866277
    move-object/from16 v0, p5

    iput-object v0, p0, LX/CH7;->b:LX/0Ot;

    .line 1866278
    move-object/from16 v0, p6

    iput-object v0, p0, LX/CH7;->c:LX/8bW;

    .line 1866279
    move-object/from16 v0, p7

    iput-object v0, p0, LX/CH7;->d:LX/CjE;

    .line 1866280
    move-object/from16 v0, p8

    iput-object v0, p0, LX/CH7;->e:LX/0ad;

    .line 1866281
    move-object/from16 v0, p9

    iput-object v0, p0, LX/CH7;->f:LX/8bK;

    .line 1866282
    move-object/from16 v0, p10

    iput-object v0, p0, LX/CH7;->i:LX/CjG;

    .line 1866283
    move-object/from16 v0, p11

    iput-object v0, p0, LX/CH7;->g:LX/0Ot;

    .line 1866284
    return-void
.end method

.method public static a(LX/0QB;)LX/CH7;
    .locals 3

    .prologue
    .line 1866263
    sget-object v0, LX/CH7;->k:LX/CH7;

    if-nez v0, :cond_1

    .line 1866264
    const-class v1, LX/CH7;

    monitor-enter v1

    .line 1866265
    :try_start_0
    sget-object v0, LX/CH7;->k:LX/CH7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1866266
    if-eqz v2, :cond_0

    .line 1866267
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/CH7;->b(LX/0QB;)LX/CH7;

    move-result-object v0

    sput-object v0, LX/CH7;->k:LX/CH7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1866268
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1866269
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1866270
    :cond_1
    sget-object v0, LX/CH7;->k:LX/CH7;

    return-object v0

    .line 1866271
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1866272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8Yz;LX/CH3;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1866285
    sget-object v0, LX/CH3;->NONE:LX/CH3;

    if-eq p1, v0, :cond_0

    if-nez p0, :cond_1

    .line 1866286
    :cond_0
    const/4 v0, 0x0

    .line 1866287
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/CH3;->FINAL_IMAGE:LX/CH3;

    if-ne p1, v0, :cond_3

    .line 1866288
    if-eqz p0, :cond_2

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1866289
    :cond_2
    const/4 v0, 0x0

    .line 1866290
    :goto_1
    move-object v0, v0

    .line 1866291
    goto :goto_0

    .line 1866292
    :cond_3
    if-eqz p0, :cond_4

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->j()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_6

    .line 1866293
    :cond_4
    const/4 v0, 0x0

    .line 1866294
    :goto_2
    move-object v0, v0

    .line 1866295
    goto :goto_0

    :cond_5
    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static synthetic a(LX/CH7;LX/CH2;Landroid/content/Context;Ljava/lang/String;LX/B5a;LX/CH3;Lcom/facebook/common/callercontext/CallerContext;LX/3Bl;)V
    .locals 6

    .prologue
    .line 1866245
    invoke-interface {p4}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v0, v1, :cond_2

    invoke-static {p4}, LX/CH7;->a(LX/8Yz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1866246
    invoke-static {p4, p5}, LX/CH7;->a(LX/8Yz;LX/CH3;)Ljava/lang/String;

    move-result-object v2

    .line 1866247
    invoke-static {p0, p1}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1866248
    iget-object v0, p0, LX/CH7;->i:LX/CjG;

    new-instance v4, LX/CGz;

    iget-object v1, p0, LX/CH7;->g:LX/0Ot;

    invoke-direct {v4, v2, v1, p7}, LX/CGz;-><init>(Ljava/lang/String;LX/0Ot;LX/3Bl;)V

    move-object v1, p2

    move-object v3, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, LX/CjG;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1866249
    iget-object v0, p0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    invoke-virtual {v0, p3, v2, p7}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bl;)V

    .line 1866250
    :cond_0
    :goto_0
    return-void

    .line 1866251
    :cond_1
    iget-object v0, p0, LX/CH7;->i:LX/CjG;

    invoke-virtual {v0, p2, v2, p3, p6}, LX/CjG;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1866252
    :cond_2
    invoke-interface {p4}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v0, v1, :cond_0

    .line 1866253
    invoke-interface {p4}, LX/B5a;->G()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 1866254
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;

    .line 1866255
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->eo_()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v4, v5, :cond_4

    invoke-static {v0}, LX/CH7;->a(LX/8Yz;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1866256
    invoke-static {p4, p5}, LX/CH7;->a(LX/8Yz;LX/CH3;)Ljava/lang/String;

    move-result-object v2

    .line 1866257
    invoke-static {p0, p1}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1866258
    iget-object v0, p0, LX/CH7;->i:LX/CjG;

    new-instance v4, LX/CGz;

    iget-object v1, p0, LX/CH7;->g:LX/0Ot;

    invoke-direct {v4, v2, v1, p7}, LX/CGz;-><init>(Ljava/lang/String;LX/0Ot;LX/3Bl;)V

    move-object v1, p2

    move-object v3, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, LX/CjG;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1866259
    iget-object v0, p0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    invoke-virtual {v0, p3, v2, p7}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bl;)V

    goto :goto_0

    .line 1866260
    :cond_3
    iget-object v0, p0, LX/CH7;->i:LX/CjG;

    invoke-virtual {v0, p2, v2, p3, p6}, LX/CjG;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1866261
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public static a(LX/8Yz;)Z
    .locals 1

    .prologue
    .line 1866262
    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8Yz;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/CH7;LX/CH2;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1866244
    iget-object v1, p0, LX/CH7;->e:LX/0ad;

    sget-short v2, LX/0fe;->Q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {p1, v1}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static b(LX/0QB;)LX/CH7;
    .locals 13

    .prologue
    .line 1866218
    new-instance v0, LX/CH7;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v2

    check-cast v2, LX/0t2;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x31ec

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/8bW;->b(LX/0QB;)LX/8bW;

    move-result-object v6

    check-cast v6, LX/8bW;

    invoke-static {p0}, LX/CjE;->a(LX/0QB;)LX/CjE;

    move-result-object v7

    check-cast v7, LX/CjE;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v9

    check-cast v9, LX/8bK;

    invoke-static {p0}, LX/CjG;->a(LX/0QB;)LX/CjG;

    move-result-object v10

    check-cast v10, LX/CjG;

    const/16 v11, 0x1ca6

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v12

    check-cast v12, LX/8bL;

    invoke-direct/range {v0 .. v12}, LX/CH7;-><init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;LX/0Ot;LX/8bW;LX/CjE;LX/0ad;LX/8bK;LX/CjG;LX/0Ot;LX/8bL;)V

    .line 1866219
    return-object v0
.end method

.method public static d(LX/CH7;)Z
    .locals 3

    .prologue
    .line 1866243
    iget-object v0, p0, LX/CH7;->e:LX/0ad;

    sget-short v1, LX/2yD;->l:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 1866220
    sget-object v0, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {v0, p3}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, LX/CH7;->h:LX/0QI;

    invoke-interface {v1, p2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1866221
    new-instance v5, LX/CGx;

    invoke-direct {v5}, LX/CGx;-><init>()V

    .line 1866222
    :goto_0
    return-object v5

    .line 1866223
    :cond_0
    new-instance v8, LX/CGu;

    invoke-direct {v8, p1, p2}, LX/CGu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1866224
    const-wide/16 v0, 0x384

    .line 1866225
    invoke-static {p0, p3}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 1866226
    if-eqz p3, :cond_1

    if-nez p1, :cond_5

    .line 1866227
    :cond_1
    :goto_1
    move v2, v2

    .line 1866228
    if-eqz v2, :cond_3

    .line 1866229
    :cond_2
    const-wide/32 v0, 0x15180

    .line 1866230
    :cond_3
    iput-wide v0, v8, LX/CGt;->j:J

    .line 1866231
    iput-boolean v3, v8, LX/CGt;->p:Z

    .line 1866232
    new-instance v5, LX/CH1;

    invoke-direct {v5, p0, p2}, LX/CH1;-><init>(LX/CH7;Ljava/lang/String;)V

    .line 1866233
    iget-object v0, p0, LX/CH7;->h:LX/0QI;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p2, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1866234
    new-instance v4, LX/3Bl;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v4, v0, p2}, LX/3Bl;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    .line 1866235
    invoke-static {p0, p3}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1866236
    iget-object v0, p0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    .line 1866237
    if-nez p2, :cond_7

    .line 1866238
    :cond_4
    new-instance v0, LX/CGv;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/CGv;-><init>(LX/CH7;Ljava/lang/String;LX/CH2;LX/3Bl;LX/CH1;Lcom/facebook/common/callercontext/CallerContext;Landroid/content/Context;)V

    invoke-virtual {p0, v8, v0}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0

    :cond_5
    sget-object v4, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {p3, v4}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    instance-of v4, p1, Landroid/app/Application;

    if-eqz v4, :cond_1

    :cond_6
    const/4 v2, 0x1

    goto :goto_1

    .line 1866239
    :cond_7
    invoke-virtual {v4}, LX/3Bl;->a()V

    .line 1866240
    iget-object v1, v0, LX/6VQ;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1fI;

    .line 1866241
    const-string v0, "ATTACHMENT_TEXT"

    invoke-static {v1, p2, p2, v0}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1866242
    goto :goto_2
.end method
