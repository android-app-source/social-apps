.class public final LX/Aoo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1714857
    iput-object p1, p0, LX/Aoo;->e:LX/Aov;

    iput-object p2, p0, LX/Aoo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aoo;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aoo;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aoo;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    .line 1714858
    iget-object v0, p0, LX/Aoo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1714859
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1714860
    move-object v4, v0

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714861
    iget-object v0, p0, LX/Aoo;->e:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aoo;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Aoo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1714862
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1714863
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aoo;->e:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714864
    iget-object v5, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1714865
    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aoo;->c:Ljava/lang/String;

    .line 1714866
    iget-object v6, v0, LX/1EQ;->a:LX/0Zb;

    const-string v7, "feed_share_action"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1714867
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1714868
    invoke-virtual {v6, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "share_type"

    const-string p1, "share_external"

    invoke-virtual {v7, v8, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "story_id"

    invoke-virtual {v7, v8, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "user_id"

    invoke-virtual {v7, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "shareable_id"

    invoke-virtual {v7, v8, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714869
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 1714870
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/Aoo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/14w;->r(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {v1}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&ref=fb4aextshare"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1714871
    iget-object v1, p0, LX/Aoo;->e:LX/Aov;

    iget-object v1, v1, LX/Aov;->i:LX/BYi;

    iget-object v2, p0, LX/Aoo;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x1

    .line 1714872
    new-instance v3, LX/3Af;

    invoke-direct {v3, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1714873
    new-instance v4, LX/7TY;

    invoke-direct {v4, v2}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1714874
    const/4 v6, 0x0

    .line 1714875
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 1714876
    new-instance v7, Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1714877
    const-string v9, "text/plain"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1714878
    invoke-virtual {v8, v7, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    .line 1714879
    iget-object v7, v1, LX/BYi;->b:LX/BYg;

    .line 1714880
    iget-object v10, v7, LX/BYg;->a:LX/0ad;

    sget-char v11, LX/BYe;->f:C

    const-string v12, ""

    invoke-interface {v10, v11, v12}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1714881
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 1714882
    const-string v12, ","

    invoke-virtual {v10, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1714883
    array-length p0, v12

    const/4 v10, 0x0

    :goto_0
    if-ge v10, p0, :cond_1

    aget-object p1, v12, v10

    .line 1714884
    invoke-interface {v11, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1714885
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1714886
    :cond_1
    move-object v10, v11

    .line 1714887
    move v7, v6

    .line 1714888
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_4

    .line 1714889
    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 1714890
    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v11, :cond_2

    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1714891
    :cond_2
    invoke-virtual {v6, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v11

    .line 1714892
    invoke-virtual {v6, v8}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v11, v12}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1714893
    new-instance v12, LX/BYh;

    invoke-direct {v12, v1, v6, v0, v2}, LX/BYh;-><init>(LX/BYi;Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1714894
    :cond_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 1714895
    :cond_4
    iput-boolean v5, v4, LX/7TY;->c:Z

    .line 1714896
    iput-boolean v5, v4, LX/7TY;->e:Z

    .line 1714897
    invoke-virtual {v3, v4}, LX/3Af;->a(LX/1OM;)V

    .line 1714898
    invoke-virtual {v3}, LX/3Af;->show()V

    .line 1714899
    const/4 v0, 0x1

    return v0
.end method
