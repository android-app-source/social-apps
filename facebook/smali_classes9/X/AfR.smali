.class public LX/AfR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AeO;


# instance fields
.field public final a:LX/AfQ;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(LX/AfQ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1699311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699312
    iput-object p1, p0, LX/AfR;->a:LX/AfQ;

    .line 1699313
    iput-object v1, p0, LX/AfR;->b:Ljava/lang/String;

    .line 1699314
    iput-object v1, p0, LX/AfR;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1699315
    iput v0, p0, LX/AfR;->d:I

    .line 1699316
    iput v0, p0, LX/AfR;->e:I

    .line 1699317
    return-void
.end method

.method public constructor <init>(LX/AfQ;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1699304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699305
    iput-object p1, p0, LX/AfR;->a:LX/AfQ;

    .line 1699306
    iput p2, p0, LX/AfR;->e:I

    .line 1699307
    iput-object v0, p0, LX/AfR;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1699308
    iput-object v0, p0, LX/AfR;->b:Ljava/lang/String;

    .line 1699309
    const/4 v0, -0x1

    iput v0, p0, LX/AfR;->d:I

    .line 1699310
    return-void
.end method

.method public constructor <init>(LX/AfQ;Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;I)V
    .locals 1

    .prologue
    .line 1699318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699319
    iput-object p1, p0, LX/AfR;->a:LX/AfQ;

    .line 1699320
    iput-object p2, p0, LX/AfR;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1699321
    iput p3, p0, LX/AfR;->d:I

    .line 1699322
    const/4 v0, 0x0

    iput-object v0, p0, LX/AfR;->b:Ljava/lang/String;

    .line 1699323
    const/4 v0, -0x1

    iput v0, p0, LX/AfR;->e:I

    .line 1699324
    return-void
.end method

.method public constructor <init>(LX/AfQ;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1699297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699298
    iput-object p1, p0, LX/AfR;->a:LX/AfQ;

    .line 1699299
    iput-object p2, p0, LX/AfR;->b:Ljava/lang/String;

    .line 1699300
    const/4 v0, 0x0

    iput-object v0, p0, LX/AfR;->c:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1699301
    iput v1, p0, LX/AfR;->d:I

    .line 1699302
    iput v1, p0, LX/AfR;->e:I

    .line 1699303
    return-void
.end method


# virtual methods
.method public final a()LX/AeN;
    .locals 1

    .prologue
    .line 1699296
    sget-object v0, LX/AeN;->LIVE_COMMERCIAL_BREAK_EVENT:LX/AeN;

    return-object v0
.end method
