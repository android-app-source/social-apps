.class public LX/CZz;
.super LX/74x;
.source ""


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field public h:LX/4n9;

.field public i:I

.field public j:I

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Ljava/lang/String;


# virtual methods
.method public final a(LX/74z;)LX/4n9;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 1916889
    sget-object v1, LX/74z;->THUMBNAIL:LX/74z;

    if-ne p1, v1, :cond_1

    .line 1916890
    iget-object v1, p0, LX/CZz;->h:LX/4n9;

    move-object v1, v1

    .line 1916891
    if-eqz v1, :cond_1

    .line 1916892
    iget-object v0, p0, LX/CZz;->h:LX/4n9;

    move-object v0, v0

    .line 1916893
    :cond_0
    :goto_0
    return-object v0

    .line 1916894
    :cond_1
    sget-object v1, LX/74z;->THUMBNAIL:LX/74z;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, LX/CZz;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1916895
    :cond_2
    sget-object v1, LX/74z;->SCREENNAIL:LX/74z;

    if-ne p1, v1, :cond_3

    iget-object v1, p0, LX/CZz;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1916896
    :cond_3
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v0

    .line 1916897
    iget v1, p0, LX/CZz;->f:I

    if-eqz v1, :cond_4

    iget v1, p0, LX/CZz;->g:I

    if-nez v1, :cond_5

    .line 1916898
    :cond_4
    sget-object p1, LX/74z;->SCREENNAIL:LX/74z;

    .line 1916899
    :cond_5
    sget-object v1, LX/CZy;->a:[I

    invoke-virtual {p1}, LX/74z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1916900
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1916901
    :pswitch_0
    invoke-virtual {v0, v3}, LX/4n6;->a(Z)LX/4n6;

    .line 1916902
    sget-object v1, LX/4n4;->MinScaleNonPowerOfTwo:LX/4n4;

    .line 1916903
    iput-object v1, v0, LX/4n6;->d:LX/4n4;

    .line 1916904
    :goto_1
    invoke-virtual {v0}, LX/4n6;->f()LX/4n5;

    move-result-object v1

    .line 1916905
    sget-object v0, LX/74z;->THUMBNAIL:LX/74z;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, LX/CZz;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/CZz;->e:Ljava/lang/String;

    :goto_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1916906
    invoke-static {v0}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v0

    .line 1916907
    iput-object v1, v0, LX/4n8;->e:LX/4n5;

    .line 1916908
    move-object v0, v0

    .line 1916909
    iput-boolean v3, v0, LX/4n8;->h:Z

    .line 1916910
    move-object v0, v0

    .line 1916911
    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object v0

    goto :goto_0

    .line 1916912
    :pswitch_1
    iget v1, p0, LX/CZz;->f:I

    iget v2, p0, LX/CZz;->g:I

    invoke-virtual {v0, v1, v2}, LX/4n6;->a(II)LX/4n6;

    .line 1916913
    sget-object v1, LX/4n4;->MaxScaleNonPowerOfTwo:LX/4n4;

    .line 1916914
    iput-object v1, v0, LX/4n6;->d:LX/4n4;

    .line 1916915
    goto :goto_1

    .line 1916916
    :cond_6
    iget-object v0, p0, LX/CZz;->d:Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
