.class public LX/AWU;
.super LX/AWT;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AWT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683051
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683052
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AWU;->a:Ljava/util/List;

    .line 1683053
    return-void
.end method


# virtual methods
.method public final a(LX/AWT;LX/AWT;)V
    .locals 3
    .param p2    # LX/AWT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 1683054
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1683055
    if-ne v0, v1, :cond_1

    .line 1683056
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1683057
    :goto_0
    iget-boolean v0, p0, LX/AWT;->a:Z

    move v0, v0

    .line 1683058
    if-eqz v0, :cond_0

    .line 1683059
    if-eqz p2, :cond_3

    .line 1683060
    invoke-virtual {p2}, LX/AWT;->getFirstIndexInParent()I

    move-result v0

    .line 1683061
    :goto_1
    if-ne v0, v1, :cond_2

    .line 1683062
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/AWT;->e:LX/AVF;

    .line 1683063
    const/4 v2, -0x1

    invoke-virtual {p1, v0, v1, v2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;I)V

    .line 1683064
    :cond_0
    :goto_2
    return-void

    .line 1683065
    :cond_1
    iget-object v2, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v2, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 1683066
    :cond_2
    iget-object v1, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/AWT;->e:LX/AVF;

    invoke-virtual {p1, v1, v2, v0}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;I)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 2

    .prologue
    .line 1683067
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1683068
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 1683069
    invoke-virtual {v0}, LX/AWT;->d()V

    goto :goto_0

    .line 1683070
    :cond_0
    return-void
.end method

.method public final varargs a([LX/AWT;)V
    .locals 3

    .prologue
    .line 1683047
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1683048
    invoke-virtual {p0, v2}, LX/AWU;->b(LX/AWT;)V

    .line 1683049
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1683050
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 1683025
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 1683026
    invoke-virtual {v0}, LX/AWT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1683027
    const/4 v0, 0x1

    .line 1683028
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, LX/AWT;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/AWT;)V
    .locals 1

    .prologue
    .line 1683045
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/AWU;->a(LX/AWT;LX/AWT;)V

    .line 1683046
    return-void
.end method

.method public iF_()V
    .locals 5

    .prologue
    .line 1683029
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1683030
    const/4 v2, -0x1

    .line 1683031
    iget-boolean v0, p0, LX/AWT;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v1, v2

    .line 1683032
    :cond_1
    :goto_0
    move v0, v1

    .line 1683033
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    move v1, v0

    .line 1683034
    :goto_1
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_3

    .line 1683035
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AWT;

    .line 1683036
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v4, p0, LX/AWT;->e:LX/AVF;

    invoke-virtual {v0, v3, v4, v1}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;I)V

    .line 1683037
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    .line 1683038
    :cond_2
    add-int/lit8 v0, v0, 0x1

    move v1, v0

    goto :goto_1

    .line 1683039
    :cond_3
    return-void

    .line 1683040
    :cond_4
    iget-object v0, p0, LX/AWT;->b:Ljava/util/List;

    iget-object v1, p0, LX/AWT;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1683041
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 1683042
    iget-object v3, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 1683043
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    move v1, v2

    .line 1683044
    goto :goto_0
.end method
