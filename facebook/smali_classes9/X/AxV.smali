.class public final LX/AxV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierModelParamsQueryModel;",
        ">;",
        "Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AxW;


# direct methods
.method public constructor <init>(LX/AxW;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1728331
    iput-object p1, p0, LX/AxV;->b:LX/AxW;

    iput-object p2, p0, LX/AxV;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1728332
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v5, 0x0

    .line 1728333
    new-instance v2, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;-><init>()V

    .line 1728334
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1728335
    :goto_0
    return-object v0

    .line 1728336
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1728337
    check-cast v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierModelParamsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierModelParamsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, -0x3ea3ceed

    invoke-static {v3, v0, v5, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, v5}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v5}, LX/15i;->g(II)I

    move-result v0

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1728338
    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 1728339
    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1728340
    const-string v6, "21"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ""

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, LX/AxV;->a:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1728341
    iget-object v0, p0, LX/AxV;->b:LX/AxW;

    iget-object v0, v0, LX/AxW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/AxW;->a:Ljava/lang/String;

    const-string v3, "Error retrieving serialized model."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1728342
    goto :goto_0

    .line 1728343
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1728344
    :cond_2
    invoke-virtual {v3, v0, v7}, LX/15i;->l(II)D

    .line 1728345
    iput-object v5, v2, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mSerializedModel:Ljava/lang/String;

    .line 1728346
    iput-object v4, v2, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mModelId:Ljava/lang/String;

    .line 1728347
    invoke-virtual {v3, v0, v7}, LX/15i;->l(II)D

    move-result-wide v0

    .line 1728348
    iput-wide v0, v2, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mThreshold:D

    .line 1728349
    move-object v0, v2

    .line 1728350
    goto :goto_0
.end method
