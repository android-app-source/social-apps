.class public final LX/C1S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1842413
    iput-object p1, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    iput-object p2, p0, LX/C1S;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 1842414
    iget-object v0, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    iget-object v1, p0, LX/C1S;->a:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;->a$redex0(Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;Landroid/widget/EditText;)V

    .line 1842415
    iget-object v0, p0, LX/C1S;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1842416
    iget-object v1, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    .line 1842417
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1842418
    iget-object v2, v1, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;->o:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842419
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v4

    .line 1842420
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842421
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_0
    move-object v2, v3

    .line 1842422
    :goto_0
    move-object v1, v2

    .line 1842423
    iget-object v2, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CGg;->a(Ljava/lang/String;Ljava/util/List;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1842424
    if-eqz v1, :cond_1

    .line 1842425
    new-instance v0, LX/0ju;

    iget-object v2, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1842426
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1842427
    iget-object v1, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    const v2, 0x7f080036

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/C1Q;

    invoke-direct {v2, p0}, LX/C1Q;-><init>(LX/C1S;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1842428
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1842429
    :goto_1
    return-void

    .line 1842430
    :cond_1
    iget-object v1, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;->m:LX/C1W;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    iget-object v2, v2, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;->n:Ljava/lang/String;

    iget-object v3, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    iget-object v3, v3, Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;->o:Lcom/facebook/feed/rows/core/props/FeedProps;

    new-instance v4, LX/C1R;

    invoke-direct {v4, p0}, LX/C1R;-><init>(LX/C1S;)V

    .line 1842431
    iget-object v5, v4, LX/C1R;->b:LX/C1S;

    iget-object v5, v5, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object p0, v4, LX/C1R;->b:LX/C1S;

    iget-object p0, p0, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    const p1, 0x7f0824fd

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    iget-object p1, v4, LX/C1R;->b:LX/C1S;

    iget-object p1, p1, LX/C1S;->b:Lcom/facebook/feedplugins/attachments/poll/QuestionAddPollOptionDialogFragment;

    const p2, 0x7f0824fe

    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1842432
    const/4 p2, 0x0

    invoke-static {v5, p0, p1, p2}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;

    move-result-object p2

    move-object v5, p2

    .line 1842433
    iput-object v5, v4, LX/C1R;->a:LX/4BY;

    .line 1842434
    new-instance v5, LX/4J0;

    invoke-direct {v5}, LX/4J0;-><init>()V

    .line 1842435
    const-string p0, "question_id"

    invoke-virtual {v5, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842436
    iget-object p0, v1, LX/C1W;->e:Ljava/lang/String;

    .line 1842437
    const-string p1, "actor_id"

    invoke-virtual {v5, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842438
    const-string p0, "option_text"

    invoke-virtual {v5, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842439
    new-instance p0, LX/81o;

    invoke-direct {p0}, LX/81o;-><init>()V

    move-object p0, p0

    .line 1842440
    const-string p1, "input"

    invoke-virtual {p0, p1, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842441
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 1842442
    iget-object p0, v1, LX/C1W;->b:LX/1Ck;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "task_key_update_poll_vote"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, v1, LX/C1W;->d:LX/0tX;

    invoke-virtual {p2, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance p2, LX/C1U;

    invoke-direct {p2, v1, v4, v0, v3}, LX/C1U;-><init>(LX/C1W;LX/C1R;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p0, p1, v5, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1842443
    goto :goto_1

    .line 1842444
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, p2, :cond_3

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 1842445
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1842446
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    :cond_3
    move-object v2, v3

    .line 1842447
    goto/16 :goto_0
.end method
