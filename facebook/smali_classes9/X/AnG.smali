.class public LX/AnG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/AnF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/AnF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712271
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/AnG;->a:Ljava/util/Map;

    .line 1712272
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnF;

    .line 1712273
    invoke-virtual {v0}, LX/AnF;->c()Ljava/lang/Class;

    move-result-object v2

    .line 1712274
    iget-object p1, p0, LX/AnG;->a:Ljava/util/Map;

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1712275
    goto :goto_0

    .line 1712276
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/AnG;
    .locals 6

    .prologue
    .line 1712277
    const-class v1, LX/AnG;

    monitor-enter v1

    .line 1712278
    :try_start_0
    sget-object v0, LX/AnG;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712279
    sput-object v2, LX/AnG;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712280
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712281
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712282
    new-instance v3, LX/AnG;

    .line 1712283
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/AnI;

    invoke-direct {p0, v0}, LX/AnI;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1712284
    invoke-direct {v3, v4}, LX/AnG;-><init>(Ljava/util/Set;)V

    .line 1712285
    move-object v0, v3

    .line 1712286
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712287
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AnG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712288
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/AnF;
    .locals 2

    .prologue
    .line 1712290
    const-class v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    const-string v1, "HScrollChainingView can only be bound to FeedUnits which implement the ScrollableItemListFeedUnit interface."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1712291
    iget-object v0, p0, LX/AnG;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnF;

    return-object v0
.end method
