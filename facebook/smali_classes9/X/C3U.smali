.class public LX/C3U;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3V;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3U",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3V;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845804
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845805
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3U;->b:LX/0Zi;

    .line 1845806
    iput-object p1, p0, LX/C3U;->a:LX/0Ot;

    .line 1845807
    return-void
.end method

.method public static a(LX/0QB;)LX/C3U;
    .locals 4

    .prologue
    .line 1845808
    const-class v1, LX/C3U;

    monitor-enter v1

    .line 1845809
    :try_start_0
    sget-object v0, LX/C3U;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845810
    sput-object v2, LX/C3U;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845811
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845812
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845813
    new-instance v3, LX/C3U;

    const/16 p0, 0x1ec6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3U;-><init>(LX/0Ot;)V

    .line 1845814
    move-object v0, v3

    .line 1845815
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845816
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845817
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1845776
    check-cast p2, LX/C3T;

    .line 1845777
    iget-object v0, p0, LX/C3U;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3V;

    iget-object v2, p2, LX/C3T;->a:LX/C33;

    iget-object v3, p2, LX/C3T;->b:LX/1Ps;

    iget v4, p2, LX/C3T;->c:I

    iget-boolean v5, p2, LX/C3T;->d:Z

    move-object v1, p1

    .line 1845778
    if-eqz v5, :cond_0

    .line 1845779
    iget-object p0, v0, LX/C3V;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3E;

    invoke-virtual {p0, v1}, LX/C3E;->c(LX/1De;)LX/C3C;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C3C;->a(LX/C33;)LX/C3C;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/C3C;->a(LX/1Pb;)LX/C3C;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/C3C;->h(I)LX/C3C;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    .line 1845780
    :goto_0
    move-object v0, p0

    .line 1845781
    return-object v0

    .line 1845782
    :cond_0
    iget-object p0, v0, LX/C3V;->f:LX/1VL;

    invoke-static {v2, p0}, LX/C3Z;->a(LX/C33;LX/1VL;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1845783
    iget-object p0, v0, LX/C3V;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3Y;

    invoke-virtual {p0, v1}, LX/C3Y;->c(LX/1De;)LX/C3W;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C3W;->a(LX/C33;)LX/C3W;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/C3W;->a(LX/1Pb;)LX/C3W;

    move-result-object p0

    .line 1845784
    iget-object p1, p0, LX/C3W;->a:LX/C3X;

    iput v4, p1, LX/C3X;->c:I

    .line 1845785
    move-object p0, p0

    .line 1845786
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto :goto_0

    .line 1845787
    :cond_1
    invoke-static {v2}, LX/C3R;->a(LX/C33;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1845788
    iget-object p0, v0, LX/C3V;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3Q;

    invoke-virtual {p0, v1}, LX/C3Q;->c(LX/1De;)LX/C3O;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C3O;->a(LX/C33;)LX/C3O;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/C3O;->a(LX/1Pb;)LX/C3O;

    move-result-object p0

    .line 1845789
    iget-object p1, p0, LX/C3O;->a:LX/C3P;

    iput v4, p1, LX/C3P;->c:I

    .line 1845790
    move-object p0, p0

    .line 1845791
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto :goto_0

    .line 1845792
    :cond_2
    invoke-static {v2}, LX/C3N;->a(LX/C33;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1845793
    iget-object p0, v0, LX/C3V;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3M;

    invoke-virtual {p0, v1}, LX/C3M;->c(LX/1De;)LX/C3K;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C3K;->a(LX/C33;)LX/C3K;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/C3K;->a(LX/1Pb;)LX/C3K;

    move-result-object p0

    .line 1845794
    iget-object p1, p0, LX/C3K;->a:LX/C3L;

    iput v4, p1, LX/C3L;->c:I

    .line 1845795
    move-object p0, p0

    .line 1845796
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto :goto_0

    .line 1845797
    :cond_3
    sget-object p0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object p1, v2, LX/C33;->b:LX/C34;

    if-eq p0, p1, :cond_4

    sget-object p0, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    iget-object p1, v2, LX/C33;->b:LX/C34;

    if-ne p0, p1, :cond_5

    :cond_4
    invoke-static {v2}, LX/C3N;->b(LX/C33;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1845798
    iget-object p0, v0, LX/C3V;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C0P;

    invoke-virtual {p0, v1}, LX/C0P;->c(LX/1De;)LX/C0N;

    move-result-object p1

    iget-object p0, v2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845799
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, v0

    .line 1845800
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/C0N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0N;

    move-result-object p0

    check-cast v3, LX/1Pq;

    invoke-virtual {p0, v3}, LX/C0N;->a(LX/1Pq;)LX/C0N;

    move-result-object p0

    const p1, 0x7f0b1d7b

    invoke-virtual {p0, p1}, LX/C0N;->h(I)LX/C0N;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto/16 :goto_0

    .line 1845801
    :cond_5
    iget-object p0, v2, LX/C33;->b:LX/C34;

    sget-object p1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-eq p0, p1, :cond_6

    iget-object p0, v2, LX/C33;->b:LX/C34;

    sget-object p1, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-eq p0, p1, :cond_6

    iget-object p0, v2, LX/C33;->b:LX/C34;

    sget-object p1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    if-ne p0, p1, :cond_7

    .line 1845802
    :cond_6
    iget-object p0, v0, LX/C3V;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3E;

    invoke-virtual {p0, v1}, LX/C3E;->c(LX/1De;)LX/C3C;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C3C;->a(LX/C33;)LX/C3C;

    move-result-object p0

    check-cast v3, LX/1Pb;

    invoke-virtual {p0, v3}, LX/C3C;->a(LX/1Pb;)LX/C3C;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/C3C;->h(I)LX/C3C;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto/16 :goto_0

    .line 1845803
    :cond_7
    const/4 p0, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1845774
    invoke-static {}, LX/1dS;->b()V

    .line 1845775
    const/4 v0, 0x0

    return-object v0
.end method
