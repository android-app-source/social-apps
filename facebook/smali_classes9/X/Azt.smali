.class public final LX/Azt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field public b:LX/Azj;

.field private c:LX/Azs;


# direct methods
.method public constructor <init>(LX/Azj;)V
    .locals 1

    .prologue
    .line 1733048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733049
    sget-object v0, LX/Azs;->DESTROYED:LX/Azs;

    iput-object v0, p0, LX/Azt;->c:LX/Azs;

    .line 1733050
    iput-object p1, p0, LX/Azt;->b:LX/Azj;

    .line 1733051
    return-void
.end method

.method public static b(LX/Azt;LX/Azs;)V
    .locals 3

    .prologue
    .line 1733015
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported state transition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Azt;->c:LX/Azs;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static f(LX/Azt;)V
    .locals 2

    .prologue
    .line 1733016
    invoke-virtual {p0}, LX/Azt;->b()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1733017
    iget-object v0, p0, LX/Azt;->b:LX/Azj;

    invoke-interface {v0}, LX/Azj;->b()V

    .line 1733018
    sget-object v0, LX/Azs;->RESUMED:LX/Azs;

    iput-object v0, p0, LX/Azt;->c:LX/Azs;

    .line 1733019
    return-void
.end method

.method public static g(LX/Azt;)V
    .locals 2

    .prologue
    .line 1733020
    invoke-virtual {p0}, LX/Azt;->b()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1733021
    sget-object v0, LX/Azs;->PAUSED:LX/Azs;

    iput-object v0, p0, LX/Azt;->c:LX/Azs;

    .line 1733022
    return-void
.end method

.method public static h(LX/Azt;)V
    .locals 1

    .prologue
    .line 1733023
    sget-object v0, LX/Azs;->DESTROYED:LX/Azs;

    iput-object v0, p0, LX/Azt;->c:LX/Azs;

    .line 1733024
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 1733025
    iget-object v0, p0, LX/Azt;->b:LX/Azj;

    invoke-interface {v0}, LX/Azj;->getAspectRatio()F

    move-result v0

    return v0
.end method

.method public final a(LX/Azs;)V
    .locals 2

    .prologue
    .line 1733026
    iget-object v0, p0, LX/Azt;->c:LX/Azs;

    if-ne p1, v0, :cond_0

    .line 1733027
    :goto_0
    return-void

    .line 1733028
    :cond_0
    sget-object v0, LX/Azr;->a:[I

    iget-object v1, p0, LX/Azt;->c:LX/Azs;

    invoke-virtual {v1}, LX/Azs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1733029
    invoke-static {p0, p1}, LX/Azt;->b(LX/Azt;LX/Azs;)V

    goto :goto_0

    .line 1733030
    :pswitch_0
    sget-object v0, LX/Azr;->a:[I

    invoke-virtual {p1}, LX/Azs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1733031
    invoke-static {p0, p1}, LX/Azt;->b(LX/Azt;LX/Azs;)V

    .line 1733032
    :goto_1
    goto :goto_0

    .line 1733033
    :pswitch_1
    sget-object v0, LX/Azr;->a:[I

    invoke-virtual {p1}, LX/Azs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 1733034
    :pswitch_2
    invoke-static {p0, p1}, LX/Azt;->b(LX/Azt;LX/Azs;)V

    .line 1733035
    :goto_2
    goto :goto_0

    .line 1733036
    :pswitch_3
    sget-object v0, LX/Azr;->a:[I

    invoke-virtual {p1}, LX/Azs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    .line 1733037
    invoke-static {p0, p1}, LX/Azt;->b(LX/Azt;LX/Azs;)V

    .line 1733038
    :goto_3
    goto :goto_0

    .line 1733039
    :pswitch_4
    invoke-static {p0}, LX/Azt;->f(LX/Azt;)V

    goto :goto_1

    .line 1733040
    :pswitch_5
    invoke-static {p0}, LX/Azt;->h(LX/Azt;)V

    goto :goto_2

    .line 1733041
    :pswitch_6
    invoke-static {p0}, LX/Azt;->f(LX/Azt;)V

    goto :goto_2

    .line 1733042
    :pswitch_7
    invoke-static {p0}, LX/Azt;->g(LX/Azt;)V

    goto :goto_3

    .line 1733043
    :pswitch_8
    invoke-static {p0}, LX/Azt;->g(LX/Azt;)V

    .line 1733044
    invoke-static {p0}, LX/Azt;->h(LX/Azt;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1733045
    iget-object v0, p0, LX/Azt;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1733046
    iget-object v0, p0, LX/Azt;->b:LX/Azj;

    invoke-interface {v0}, LX/Azj;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Azt;->a:Landroid/view/View;

    .line 1733047
    :cond_0
    iget-object v0, p0, LX/Azt;->a:Landroid/view/View;

    return-object v0
.end method
