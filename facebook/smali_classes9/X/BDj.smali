.class public LX/BDj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763761
    sget-object v0, LX/11I;->GET_NOTIFICATION_URI:LX/11I;

    iget-object v0, v0, LX/11I;->requestNameString:Ljava/lang/String;

    sput-object v0, LX/BDj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1763762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763763
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1763764
    check-cast p1, Ljava/lang/String;

    .line 1763765
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1763766
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "type"

    const-string v3, "shorturl"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763767
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "url"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763768
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/BDj;->a:Ljava/lang/String;

    .line 1763769
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1763770
    move-object v1, v1

    .line 1763771
    const-string v2, "GET"

    .line 1763772
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1763773
    move-object v1, v1

    .line 1763774
    const-string v2, "search"

    .line 1763775
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1763776
    move-object v1, v1

    .line 1763777
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 1763778
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1763779
    move-object v0, v1

    .line 1763780
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1763781
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1763782
    move-object v0, v0

    .line 1763783
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1763784
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1763785
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/notifications/model/SMSNotificationURL;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/SMSNotificationURL;

    .line 1763786
    new-instance v1, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;-><init>(Lcom/facebook/notifications/model/SMSNotificationURL;LX/0ta;J)V

    return-object v1
.end method
