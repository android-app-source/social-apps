.class public final LX/AYa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)V
    .locals 0

    .prologue
    .line 1685830
    iput-object p1, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x2

    const v0, 0x262b72

    invoke-static {v4, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1685831
    iget-object v0, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, LX/0ew;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1685832
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "broadcast_invite_friend_dialog"

    invoke-virtual {v2, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1685833
    :cond_0
    const v0, -0x7d8ca89d

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1685834
    :goto_0
    return-void

    .line 1685835
    :cond_1
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-nez v2, :cond_2

    .line 1685836
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    new-instance v3, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-direct {v3}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;-><init>()V

    .line 1685837
    iput-object v3, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1685838
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object v3, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v3, v3, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->h:Ljava/lang/String;

    .line 1685839
    iput-object v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    .line 1685840
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v2, :cond_2

    .line 1685841
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object v3, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v3, v3, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v4, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1685842
    iput-object v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->q:Ljava/lang/String;

    .line 1685843
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->GROUP:LX/2rw;

    if-ne v2, v3, :cond_2

    .line 1685844
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    const v3, 0x41e065f

    .line 1685845
    iput v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->r:I

    .line 1685846
    :cond_2
    iget-object v2, p0, LX/AYa;->a:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iget-object v2, v2, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const-string v3, "broadcast_invite_friend_dialog"

    invoke-virtual {v2, v0, v3, v6}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1685847
    const v0, -0x317ed51d

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
