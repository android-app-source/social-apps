.class public LX/AvF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/AvF;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yD;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yD;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1723909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723910
    iput-object p1, p0, LX/AvF;->a:LX/0Ot;

    .line 1723911
    iput-object p2, p0, LX/AvF;->b:LX/0Ot;

    .line 1723912
    iput-object p3, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1723913
    iput-object p4, p0, LX/AvF;->d:LX/0Uh;

    .line 1723914
    return-void
.end method

.method public static a(LX/0QB;)LX/AvF;
    .locals 7

    .prologue
    .line 1723915
    sget-object v0, LX/AvF;->e:LX/AvF;

    if-nez v0, :cond_1

    .line 1723916
    const-class v1, LX/AvF;

    monitor-enter v1

    .line 1723917
    :try_start_0
    sget-object v0, LX/AvF;->e:LX/AvF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1723918
    if-eqz v2, :cond_0

    .line 1723919
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1723920
    new-instance v5, LX/AvF;

    const/16 v3, 0xc7e

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0xc89

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v6, p0, v3, v4}, LX/AvF;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 1723921
    move-object v0, v5

    .line 1723922
    sput-object v0, LX/AvF;->e:LX/AvF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1723923
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1723924
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1723925
    :cond_1
    sget-object v0, LX/AvF;->e:LX/AvF;

    return-object v0

    .line 1723926
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1723927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/AvF;Lcom/facebook/location/ImmutableLocation;D)V
    .locals 6
    .param p0    # LX/AvF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1723928
    iget-object v0, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1723929
    if-nez p1, :cond_0

    .line 1723930
    sget-object v0, LX/AwE;->i:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723931
    sget-object v0, LX/AwE;->j:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723932
    sget-object v0, LX/AwE;->k:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723933
    sget-object v0, LX/AwE;->l:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723934
    :goto_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1723935
    return-void

    .line 1723936
    :cond_0
    sget-object v0, LX/AwE;->i:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    .line 1723937
    sget-object v0, LX/AwE;->j:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    .line 1723938
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1723939
    sget-object v2, LX/AwE;->k:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    .line 1723940
    :goto_1
    const-wide/16 v2, 0x0

    cmpl-double v0, p2, v2

    if-ltz v0, :cond_2

    .line 1723941
    sget-object v0, LX/AwE;->l:LX/0Tn;

    invoke-interface {v1, v0, p2, p3}, LX/0hN;->a(LX/0Tn;D)LX/0hN;

    goto :goto_0

    .line 1723942
    :cond_1
    sget-object v0, LX/AwE;->k:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_1

    .line 1723943
    :cond_2
    sget-object v0, LX/AwE;->l:LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)LX/3Aj;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1723944
    iget-object v0, p0, LX/AvF;->d:LX/0Uh;

    sget v1, LX/86c;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1723945
    iget-object v0, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AwE;->i:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1723946
    :goto_0
    const/4 v0, 0x0

    .line 1723947
    :goto_1
    return-object v0

    .line 1723948
    :cond_0
    if-eqz p1, :cond_3

    .line 1723949
    const-wide/16 v5, 0x0

    .line 1723950
    const/4 v4, 0x0

    .line 1723951
    iget-object v3, p0, LX/AvF;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-virtual {v3}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v8

    .line 1723952
    if-eqz v8, :cond_6

    .line 1723953
    new-instance v3, LX/3Aj;

    invoke-direct {v3}, LX/3Aj;-><init>()V

    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v3

    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v7

    .line 1723954
    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1723955
    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 1723956
    :cond_1
    iget-object v3, p0, LX/AvF;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0yD;

    invoke-virtual {v3, v8}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v3

    invoke-static {v3, v4}, LX/1lQ;->n(J)D

    move-result-wide v3

    .line 1723957
    cmpl-double v5, v3, v5

    if-ltz v5, :cond_2

    .line 1723958
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    :cond_2
    move-object v5, v7

    .line 1723959
    :goto_2
    invoke-static {p0, v8, v3, v4}, LX/AvF;->a(LX/AvF;Lcom/facebook/location/ImmutableLocation;D)V

    .line 1723960
    move-object v0, v5

    .line 1723961
    goto :goto_1

    .line 1723962
    :cond_3
    const-wide/16 v7, 0x0

    .line 1723963
    iget-object v3, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/AwE;->i:LX/0Tn;

    invoke-interface {v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1723964
    const/4 v3, 0x0

    .line 1723965
    :cond_4
    :goto_3
    move-object v0, v3

    .line 1723966
    goto :goto_1

    .line 1723967
    :cond_5
    iget-object v0, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1723968
    sget-object v1, LX/AwE;->i:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723969
    sget-object v1, LX/AwE;->j:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723970
    sget-object v1, LX/AwE;->k:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723971
    sget-object v1, LX/AwE;->l:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1723972
    invoke-interface {v0}, LX/0hN;->commit()V

    goto/16 :goto_0

    :cond_6
    move-wide v11, v5

    move-object v5, v4

    move-wide v3, v11

    goto :goto_2

    .line 1723973
    :cond_7
    new-instance v3, LX/3Aj;

    invoke-direct {v3}, LX/3Aj;-><init>()V

    .line 1723974
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->i:LX/0Tn;

    invoke-interface {v4, v5, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 1723975
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->j:LX/0Tn;

    invoke-interface {v4, v5, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 1723976
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->k:LX/0Tn;

    invoke-interface {v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1723977
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->k:LX/0Tn;

    invoke-interface {v4, v5, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 1723978
    :cond_8
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->l:LX/0Tn;

    invoke-interface {v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1723979
    iget-object v4, p0, LX/AvF;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AwE;->l:LX/0Tn;

    invoke-interface {v4, v5, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    goto :goto_3
.end method
