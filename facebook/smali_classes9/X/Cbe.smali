.class public LX/Cbe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/7US;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919717
    return-void
.end method

.method public static a(LX/0QB;)LX/Cbe;
    .locals 3

    .prologue
    .line 1919718
    const-class v1, LX/Cbe;

    monitor-enter v1

    .line 1919719
    :try_start_0
    sget-object v0, LX/Cbe;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1919720
    sput-object v2, LX/Cbe;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1919721
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919722
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1919723
    new-instance v0, LX/Cbe;

    invoke-direct {v0}, LX/Cbe;-><init>()V

    .line 1919724
    move-object v0, v0

    .line 1919725
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1919726
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cbe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1919727
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1919728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
