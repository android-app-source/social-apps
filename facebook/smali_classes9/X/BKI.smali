.class public final LX/BKI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BKK;


# direct methods
.method public constructor <init>(LX/BKK;)V
    .locals 0

    .prologue
    .line 1772756
    iput-object p1, p0, LX/BKI;->a:LX/BKK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, 0x7024d3be

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772757
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v1, v1, LX/BKi;->c:LX/BKg;

    sget-object v2, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    if-eq v1, v2, :cond_5

    .line 1772758
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->T:LX/BLr;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->T:LX/BLr;

    const/4 v3, 0x0

    .line 1772759
    iget-object v2, v1, LX/BLr;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v6, "dataProvider was garbage collected"

    invoke-static {v2, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BKL;

    .line 1772760
    iget-object v6, v2, LX/BKL;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v6, v6, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v6, v6, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    move-object v6, v6

    .line 1772761
    invoke-virtual {v6}, Lcom/facebook/platform/composer/model/PlatformComposition;->d()Z

    move-result v6

    if-eqz v6, :cond_6

    move v2, v3

    .line 1772762
    :goto_0
    move v1, v2

    .line 1772763
    if-eqz v1, :cond_3

    .line 1772764
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    if-nez v1, :cond_0

    .line 1772765
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    new-instance v2, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1772766
    iput-object v2, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    .line 1772767
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    invoke-virtual {v1, v4}, LX/5OM;->a(Z)V

    .line 1772768
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1772769
    :cond_0
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->T:LX/BLr;

    iget-object v2, p0, LX/BKI;->a:LX/BKK;

    iget-object v2, v2, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    invoke-virtual {v2}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1772770
    invoke-static {v1}, LX/BLr;->b(LX/BLr;)LX/0P1;

    move-result-object v8

    .line 1772771
    iget-object v3, v1, LX/BLr;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    const-string v6, "targetSelectorClient was garbage collected"

    invoke-static {v3, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772772
    invoke-virtual {v2}, LX/5OG;->clear()V

    .line 1772773
    invoke-virtual {v8}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v6

    invoke-virtual {v6}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2rw;

    .line 1772774
    invoke-virtual {v8, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/BLq;

    .line 1772775
    iget-boolean v7, v7, LX/BLq;->a:Z

    if-nez v7, :cond_1

    .line 1772776
    iget-object v7, v1, LX/BLr;->f:LX/0P1;

    invoke-virtual {v7, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/BLm;

    .line 1772777
    if-eqz v7, :cond_1

    .line 1772778
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772779
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772780
    sget-object v10, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v6, v10, :cond_a

    .line 1772781
    iget-object v11, v1, LX/BLr;->d:Landroid/content/res/Resources;

    iget-object v10, v1, LX/BLr;->e:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_9

    const v10, 0x7f08245f

    :goto_2
    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1772782
    :goto_3
    move-object v10, v10

    .line 1772783
    const/4 v11, 0x0

    iget p1, v7, LX/BLm;->b:I

    invoke-virtual {v2, v11, p1, v10}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v10

    .line 1772784
    iget v7, v7, LX/BLm;->d:I

    invoke-virtual {v10, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1772785
    new-instance v7, LX/BLn;

    invoke-direct {v7, v1, v3, v6}, LX/BLn;-><init>(LX/BLr;Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/2rw;)V

    invoke-virtual {v10, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 1772786
    :cond_2
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1772787
    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 1772788
    invoke-interface {v1, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-interface {v1, v2, v5}, Landroid/view/Menu;->performIdentifierAction(II)Z

    .line 1772789
    :cond_3
    :goto_4
    const v1, -0x7aa3d99d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1772790
    :cond_4
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->V:LX/6WS;

    invoke-virtual {v1}, LX/0ht;->d()V

    goto :goto_4

    .line 1772791
    :cond_5
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->at:LX/BJX;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1772792
    iget-object v1, p0, LX/BKI;->a:LX/BKK;

    iget-object v1, v1, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->at:LX/BJX;

    const/16 v2, 0x46

    .line 1772793
    invoke-virtual {v1, v2}, LX/BJX;->a(I)V

    goto :goto_4

    .line 1772794
    :cond_6
    invoke-static {v1}, LX/BLr;->b(LX/BLr;)LX/0P1;

    move-result-object v6

    .line 1772795
    iget-object v7, v2, LX/BKL;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v7, v7, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v7, v7, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v2, v7

    .line 1772796
    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v7, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v2, v7, :cond_7

    sget-object v2, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-static {v6}, LX/BLr;->a(LX/0P1;)LX/0Rf;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_8
    move v2, v3

    goto/16 :goto_0

    .line 1772797
    :cond_9
    const v10, 0x7f08245e

    goto/16 :goto_2

    .line 1772798
    :cond_a
    iget-object v10, v1, LX/BLr;->d:Landroid/content/res/Resources;

    iget v11, v7, LX/BLm;->c:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_3
.end method
