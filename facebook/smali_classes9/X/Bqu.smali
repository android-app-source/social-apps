.class public final LX/Bqu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Bqw;

.field private final b:LX/Br2;

.field private final c:LX/1Pq;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(LX/Bqw;ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/Br2;",
            "LX/1Pq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1825472
    iput-object p1, p0, LX/Bqu;->a:LX/Bqw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825473
    iput-object p3, p0, LX/Bqu;->b:LX/Br2;

    .line 1825474
    iput-object p4, p0, LX/Bqu;->c:LX/1Pq;

    .line 1825475
    iput-object p5, p0, LX/Bqu;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825476
    iput-boolean p2, p0, LX/Bqu;->e:Z

    .line 1825477
    return-void
.end method

.method public static a(LX/Bqu;LX/CSG;Z)V
    .locals 5

    .prologue
    .line 1825463
    iget-object v0, p0, LX/Bqu;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825464
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1825465
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825466
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/CSG;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1825467
    :cond_0
    :goto_0
    return-void

    .line 1825468
    :cond_1
    if-eqz p2, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1825469
    :goto_1
    iget-object v2, p0, LX/Bqu;->b:LX/Br2;

    iget-object v3, p0, LX/Bqu;->a:LX/Bqw;

    iget-boolean v4, p0, LX/Bqu;->e:Z

    invoke-virtual {v3, v0, v1, v4}, LX/Bqw;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;Z)I

    move-result v0

    invoke-virtual {v2, v0}, LX/Br2;->a(I)V

    .line 1825470
    iget-object v0, p0, LX/Bqu;->c:LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Bqu;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    .line 1825471
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_1
.end method
