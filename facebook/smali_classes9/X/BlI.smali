.class public final enum LX/BlI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BlI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BlI;

.field public static final enum FAILURE:LX/BlI;

.field public static final enum SENDING:LX/BlI;

.field public static final enum SUCCESS:LX/BlI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1816201
    new-instance v0, LX/BlI;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v2}, LX/BlI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BlI;->SENDING:LX/BlI;

    .line 1816202
    new-instance v0, LX/BlI;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, LX/BlI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BlI;->SUCCESS:LX/BlI;

    .line 1816203
    new-instance v0, LX/BlI;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, LX/BlI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BlI;->FAILURE:LX/BlI;

    .line 1816204
    const/4 v0, 0x3

    new-array v0, v0, [LX/BlI;

    sget-object v1, LX/BlI;->SENDING:LX/BlI;

    aput-object v1, v0, v2

    sget-object v1, LX/BlI;->SUCCESS:LX/BlI;

    aput-object v1, v0, v3

    sget-object v1, LX/BlI;->FAILURE:LX/BlI;

    aput-object v1, v0, v4

    sput-object v0, LX/BlI;->$VALUES:[LX/BlI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1816205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BlI;
    .locals 1

    .prologue
    .line 1816206
    const-class v0, LX/BlI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BlI;

    return-object v0
.end method

.method public static values()[LX/BlI;
    .locals 1

    .prologue
    .line 1816207
    sget-object v0, LX/BlI;->$VALUES:[LX/BlI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BlI;

    return-object v0
.end method
