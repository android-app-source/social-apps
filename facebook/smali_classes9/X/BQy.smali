.class public LX/BQy;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BQx;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782995
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1782996
    return-void
.end method


# virtual methods
.method public final a(JLX/BQo;LX/B37;LX/0gc;)LX/BQx;
    .locals 13

    .prologue
    .line 1782997
    new-instance v0, LX/BQx;

    const-class v1, LX/BRh;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BRh;

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, LX/BQx;-><init>(LX/BRh;JLX/BQo;LX/B37;LX/0gc;)V

    .line 1782998
    const/16 v1, 0x455

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x36f3

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x123b

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/BRP;->a(LX/0QB;)LX/BRP;

    move-result-object v4

    check-cast v4, LX/BRP;

    const-class v5, LX/BRC;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/BRC;

    const-class v6, LX/BRH;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BRH;

    const-class v7, LX/63c;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/63c;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const/16 v9, 0x24e5

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x3c

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x259

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v0 .. v11}, LX/BQx;->a(LX/BQx;LX/0Or;LX/0Or;LX/0Or;LX/BRP;LX/BRC;LX/BRH;LX/63c;Landroid/content/Context;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 1782999
    return-object v0
.end method
