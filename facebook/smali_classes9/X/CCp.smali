.class public LX/CCp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CCn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CCs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1858411
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CCp;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CCs;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858353
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1858354
    iput-object p1, p0, LX/CCp;->b:LX/0Ot;

    .line 1858355
    return-void
.end method

.method public static a(LX/0QB;)LX/CCp;
    .locals 4

    .prologue
    .line 1858356
    const-class v1, LX/CCp;

    monitor-enter v1

    .line 1858357
    :try_start_0
    sget-object v0, LX/CCp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858358
    sput-object v2, LX/CCp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858361
    new-instance v3, LX/CCp;

    const/16 p0, 0x21da

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CCp;-><init>(LX/0Ot;)V

    .line 1858362
    move-object v0, v3

    .line 1858363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/CCr;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "LX/CCr;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1858367
    const v0, 0x1d675f8d

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1858368
    check-cast p2, LX/CCo;

    .line 1858369
    iget-object v0, p0, LX/CCp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CCs;

    iget-object v2, p2, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/CCo;->b:LX/1Pm;

    iget-boolean v4, p2, LX/CCo;->c:Z

    iget v5, p2, LX/CCo;->d:I

    iget v6, p2, LX/CCo;->e:I

    iget-object v7, p2, LX/CCo;->f:LX/1dc;

    move-object v1, p1

    .line 1858370
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1858371
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    const v9, -0x74569f26

    invoke-static {v8, v9}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v10

    .line 1858372
    if-nez v10, :cond_0

    .line 1858373
    const/4 v8, 0x0

    .line 1858374
    :goto_0
    move-object v0, v8

    .line 1858375
    return-object v0

    .line 1858376
    :cond_0
    if-eqz v4, :cond_1

    invoke-static {v10}, LX/CCs;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)I

    move-result v8

    if-nez v8, :cond_2

    :cond_1
    const/4 v8, 0x0

    move-object v9, v8

    .line 1858377
    :goto_1
    new-instance p0, LX/CCr;

    .line 1858378
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1858379
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v8}, LX/CCr;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1858380
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1858381
    check-cast v8, LX/0jW;

    invoke-interface {v3, p0, v8}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 1858382
    if-eqz v8, :cond_3

    .line 1858383
    const/4 v8, 0x0

    goto :goto_0

    .line 1858384
    :cond_2
    iget-object v8, v0, LX/CCs;->a:LX/1vg;

    invoke-virtual {v8, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/2xv;->i(I)LX/2xv;

    move-result-object v8

    invoke-static {v10}, LX/CCs;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)I

    move-result v9

    invoke-virtual {v8, v9}, LX/2xv;->h(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v8}, LX/1n6;->b()LX/1dc;

    move-result-object v8

    move-object v9, v8

    goto :goto_1

    .line 1858385
    :cond_3
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x2

    invoke-interface {v8, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x0

    invoke-interface {v8, p1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    const p1, 0x7f0a00d5

    invoke-interface {v8, p1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x1

    const p2, 0x7f0b1dab

    invoke-interface {v8, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x3

    const p2, 0x7f0b1dac

    invoke-interface {v8, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x4

    const p2, 0x7f0b1dad

    invoke-interface {v8, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object v8

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v8, p2}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/8yk;->i(I)LX/8yk;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/8yk;->a(LX/1dc;)LX/8yk;

    move-result-object v8

    const v9, 0x7f0b1db3

    invoke-virtual {v8, v9}, LX/8yk;->h(I)LX/8yk;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    .line 1858386
    const v9, 0x1d675f8d

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v10, p2, v0

    const/4 v0, 0x1

    aput-object p0, p2, v0

    invoke-static {v1, v9, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 1858387
    invoke-interface {v8, v9}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v8

    invoke-interface {v8, v7}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v9

    const/4 v10, 0x4

    if-eqz v4, :cond_4

    const v8, 0x7f0b1dae

    :goto_2
    invoke-interface {v9, v10, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x5

    const v10, 0x7f0b1db0

    invoke-interface {v8, v9, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    const v10, 0x7f0b1db1

    invoke-interface {v8, v9, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x3

    const v10, 0x7f0b1db2

    invoke-interface {v8, v9, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    invoke-interface {p1, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    goto/16 :goto_0

    :cond_4
    const v8, 0x7f0b1daf

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 1858388
    invoke-static {}, LX/1dS;->b()V

    .line 1858389
    iget v0, p1, LX/1dQ;->b:I

    .line 1858390
    packed-switch v0, :pswitch_data_0

    .line 1858391
    :goto_0
    return-object v4

    .line 1858392
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1858393
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, LX/CCr;

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    .line 1858394
    check-cast v3, LX/CCo;

    .line 1858395
    iget-object v5, p0, LX/CCp;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CCs;

    iget-object v7, v3, LX/CCo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v8, v3, LX/CCo;->b:LX/1Pm;

    move-object v6, v2

    move-object v9, v0

    move-object p1, v1

    const/4 v2, 0x1

    .line 1858396
    sget-object p2, LX/CCq;->a:[I

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bx()Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ordinal()I

    move-result p0

    aget p2, p2, p0

    packed-switch p2, :pswitch_data_1

    .line 1858397
    invoke-interface {v8}, LX/1Po;->c()LX/1PT;

    move-result-object p2

    invoke-static {p2}, LX/9Ir;->b(LX/1PT;)LX/21D;

    move-result-object v0

    .line 1858398
    iget-object p2, v5, LX/CCs;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-class p0, LX/CCs;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 1858399
    iget-object p0, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1858400
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p2, v0, v1, p0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    .line 1858401
    iget-object p2, v5, LX/CCs;->b:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1Kf;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v3, 0x6de

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class v9, Landroid/app/Activity;

    invoke-static {p0, v9}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/Activity;

    invoke-interface {p2, v0, v1, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1858402
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {v8, p1, p2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1858403
    new-array p2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    aput-object v7, p2, p0

    invoke-interface {v8, p2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1858404
    goto/16 :goto_0

    .line 1858405
    :pswitch_1
    iget-object p2, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1858406
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p2}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    .line 1858407
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_1

    .line 1858408
    :cond_0
    :goto_2
    goto :goto_1

    .line 1858409
    :cond_1
    iget-object p2, v5, LX/CCs;->d:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/9jH;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class v3, Landroid/app/Activity;

    invoke-static {p0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p2, v0, v1, p0}, LX/9jH;->a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;Landroid/app/Activity;)V

    .line 1858410
    const/4 p2, 0x1

    new-array p2, p2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    aput-object v7, p2, p0

    invoke-interface {v8, p2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1d675f8d
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
