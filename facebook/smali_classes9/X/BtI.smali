.class public LX/BtI;
.super Lcom/facebook/fbui/widget/text/TextLayoutView;
.source ""

# interfaces
.implements LX/2dX;
.implements LX/0wO;


# instance fields
.field private a:LX/2db;

.field private b:LX/BtO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1828970
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BtI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1828971
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1828966
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/TextLayoutView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1828967
    new-instance v0, LX/BtO;

    invoke-direct {v0, p0}, LX/BtO;-><init>(Lcom/facebook/fbui/widget/text/TextLayoutView;)V

    iput-object v0, p0, LX/BtI;->b:LX/BtO;

    .line 1828968
    new-instance v0, LX/2db;

    invoke-virtual {p0}, LX/BtI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2db;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BtI;->a:LX/2db;

    .line 1828969
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1828947
    iget-object v0, p0, LX/BtI;->a:LX/2db;

    invoke-virtual {v0}, LX/2db;->a()V

    .line 1828948
    return-void
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1828963
    iget-object v0, p0, LX/BtI;->b:LX/BtO;

    invoke-virtual {v0, p1}, LX/556;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828964
    const/4 v0, 0x1

    .line 1828965
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .prologue
    .line 1828960
    iget-object v0, p0, LX/BtI;->b:LX/BtO;

    .line 1828961
    iget-object p0, v0, LX/3t3;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 1828962
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    return-object v0
.end method

.method public getTextSize()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1828954
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1828955
    if-eqz v1, :cond_0

    .line 1828956
    invoke-virtual {v1}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    .line 1828957
    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v0, v1

    .line 1828958
    :cond_0
    return v0

    .line 1828959
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalPaddingLeft()I
    .locals 1

    .prologue
    .line 1828953
    invoke-virtual {p0}, LX/BtI;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public getTotalPaddingTop()I
    .locals 1

    .prologue
    .line 1828952
    invoke-virtual {p0}, LX/BtI;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x193bbd0c

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1828951
    iget-object v2, p0, LX/BtI;->a:LX/2db;

    invoke-virtual {v2, p1}, LX/2db;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    const v2, -0x12e554ae

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCopyTextGestureListener(LX/BtK;)V
    .locals 1

    .prologue
    .line 1828949
    iget-object v0, p0, LX/BtI;->a:LX/2db;

    invoke-virtual {v0, p1}, LX/2db;->setCopyTextGestureListener(LX/BtK;)V

    .line 1828950
    return-void
.end method
