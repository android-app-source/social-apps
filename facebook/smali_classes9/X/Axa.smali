.class public final LX/Axa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/BOy;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Axb;


# direct methods
.method public constructor <init>(LX/Axb;)V
    .locals 0

    .prologue
    .line 1728495
    iput-object p1, p0, LX/Axa;->a:LX/Axb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 1728496
    check-cast p1, LX/BOy;

    check-cast p2, LX/BOy;

    .line 1728497
    iget-object v0, p0, LX/Axa;->a:LX/Axb;

    invoke-static {v0, p1}, LX/Axb;->k(LX/Axb;LX/BOy;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    .line 1728498
    iget-object v1, p0, LX/Axa;->a:LX/Axb;

    invoke-static {v1, p2}, LX/Axb;->k(LX/Axb;LX/BOy;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 1728499
    iget-wide v6, v0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    move-wide v2, v6

    .line 1728500
    iget-wide v6, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    move-wide v4, v6

    .line 1728501
    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1728502
    const/4 v0, 0x1

    .line 1728503
    :goto_0
    return v0

    .line 1728504
    :cond_0
    iget-wide v6, v0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    move-wide v2, v6

    .line 1728505
    iget-wide v6, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    move-wide v0, v6

    .line 1728506
    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 1728507
    const/4 v0, -0x1

    goto :goto_0

    .line 1728508
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
