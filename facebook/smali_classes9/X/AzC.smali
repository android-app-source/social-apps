.class public final enum LX/AzC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AzC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AzC;

.field public static final enum ASSET_COUNT_BEFORE_EDIT:LX/AzC;

.field public static final enum ASSET_TYPE:LX/AzC;

.field public static final enum BURST_COUNT:LX/AzC;

.field public static final enum CLASSIFIER_TAG:LX/AzC;

.field public static final enum CURRENT_INDEX:LX/AzC;

.field public static final enum IS_EDITING_FROM_COMPOSER:LX/AzC;

.field public static final enum PHOTOS_COUNT:LX/AzC;

.field public static final enum SOUVENIR_UNIQUE_ID:LX/AzC;

.field public static final enum TILE_COUNT:LX/AzC;

.field public static final enum TIME_SINCE_STORY:LX/AzC;

.field public static final enum TITLE_LENGTH:LX/AzC;

.field public static final enum TOTAL_ASSETS_COUNT:LX/AzC;

.field public static final enum TOTAL_ASSETS_WITHIN_BURSTS_COUNT:LX/AzC;

.field public static final enum UPDATED_ASSET_COUNT:LX/AzC;

.field public static final enum VIDEOS_COUNT:LX/AzC;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1732251
    new-instance v0, LX/AzC;

    const-string v1, "CURRENT_INDEX"

    const-string v2, "current_index"

    invoke-direct {v0, v1, v4, v2}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->CURRENT_INDEX:LX/AzC;

    .line 1732252
    new-instance v0, LX/AzC;

    const-string v1, "PHOTOS_COUNT"

    const-string v2, "photos_count"

    invoke-direct {v0, v1, v5, v2}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->PHOTOS_COUNT:LX/AzC;

    .line 1732253
    new-instance v0, LX/AzC;

    const-string v1, "VIDEOS_COUNT"

    const-string v2, "videos_count"

    invoke-direct {v0, v1, v6, v2}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->VIDEOS_COUNT:LX/AzC;

    .line 1732254
    new-instance v0, LX/AzC;

    const-string v1, "BURST_COUNT"

    const-string v2, "burst_count"

    invoke-direct {v0, v1, v7, v2}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->BURST_COUNT:LX/AzC;

    .line 1732255
    new-instance v0, LX/AzC;

    const-string v1, "TILE_COUNT"

    const-string v2, "tile_count"

    invoke-direct {v0, v1, v8, v2}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->TILE_COUNT:LX/AzC;

    .line 1732256
    new-instance v0, LX/AzC;

    const-string v1, "TOTAL_ASSETS_COUNT"

    const/4 v2, 0x5

    const-string v3, "total_assets_count"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->TOTAL_ASSETS_COUNT:LX/AzC;

    .line 1732257
    new-instance v0, LX/AzC;

    const-string v1, "TOTAL_ASSETS_WITHIN_BURSTS_COUNT"

    const/4 v2, 0x6

    const-string v3, "total_assets_within_bursts_count"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->TOTAL_ASSETS_WITHIN_BURSTS_COUNT:LX/AzC;

    .line 1732258
    new-instance v0, LX/AzC;

    const-string v1, "TIME_SINCE_STORY"

    const/4 v2, 0x7

    const-string v3, "time_since_story"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->TIME_SINCE_STORY:LX/AzC;

    .line 1732259
    new-instance v0, LX/AzC;

    const-string v1, "SOUVENIR_UNIQUE_ID"

    const/16 v2, 0x8

    const-string v3, "souvenir_unique_id"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->SOUVENIR_UNIQUE_ID:LX/AzC;

    .line 1732260
    new-instance v0, LX/AzC;

    const-string v1, "IS_EDITING_FROM_COMPOSER"

    const/16 v2, 0x9

    const-string v3, "is_editing_from_composer"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->IS_EDITING_FROM_COMPOSER:LX/AzC;

    .line 1732261
    new-instance v0, LX/AzC;

    const-string v1, "ASSET_COUNT_BEFORE_EDIT"

    const/16 v2, 0xa

    const-string v3, "asset_count_before_edit"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->ASSET_COUNT_BEFORE_EDIT:LX/AzC;

    .line 1732262
    new-instance v0, LX/AzC;

    const-string v1, "UPDATED_ASSET_COUNT"

    const/16 v2, 0xb

    const-string v3, "updated_asset_count"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->UPDATED_ASSET_COUNT:LX/AzC;

    .line 1732263
    new-instance v0, LX/AzC;

    const-string v1, "ASSET_TYPE"

    const/16 v2, 0xc

    const-string v3, "asset_type"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->ASSET_TYPE:LX/AzC;

    .line 1732264
    new-instance v0, LX/AzC;

    const-string v1, "CLASSIFIER_TAG"

    const/16 v2, 0xd

    const-string v3, "classifier_"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->CLASSIFIER_TAG:LX/AzC;

    .line 1732265
    new-instance v0, LX/AzC;

    const-string v1, "TITLE_LENGTH"

    const/16 v2, 0xe

    const-string v3, "title_length"

    invoke-direct {v0, v1, v2, v3}, LX/AzC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzC;->TITLE_LENGTH:LX/AzC;

    .line 1732266
    const/16 v0, 0xf

    new-array v0, v0, [LX/AzC;

    sget-object v1, LX/AzC;->CURRENT_INDEX:LX/AzC;

    aput-object v1, v0, v4

    sget-object v1, LX/AzC;->PHOTOS_COUNT:LX/AzC;

    aput-object v1, v0, v5

    sget-object v1, LX/AzC;->VIDEOS_COUNT:LX/AzC;

    aput-object v1, v0, v6

    sget-object v1, LX/AzC;->BURST_COUNT:LX/AzC;

    aput-object v1, v0, v7

    sget-object v1, LX/AzC;->TILE_COUNT:LX/AzC;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/AzC;->TOTAL_ASSETS_COUNT:LX/AzC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AzC;->TOTAL_ASSETS_WITHIN_BURSTS_COUNT:LX/AzC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AzC;->TIME_SINCE_STORY:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/AzC;->SOUVENIR_UNIQUE_ID:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/AzC;->IS_EDITING_FROM_COMPOSER:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/AzC;->ASSET_COUNT_BEFORE_EDIT:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/AzC;->UPDATED_ASSET_COUNT:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/AzC;->ASSET_TYPE:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/AzC;->CLASSIFIER_TAG:LX/AzC;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/AzC;->TITLE_LENGTH:LX/AzC;

    aput-object v2, v0, v1

    sput-object v0, LX/AzC;->$VALUES:[LX/AzC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1732248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1732249
    iput-object p3, p0, LX/AzC;->name:Ljava/lang/String;

    .line 1732250
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AzC;
    .locals 1

    .prologue
    .line 1732247
    const-class v0, LX/AzC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AzC;

    return-object v0
.end method

.method public static values()[LX/AzC;
    .locals 1

    .prologue
    .line 1732246
    sget-object v0, LX/AzC;->$VALUES:[LX/AzC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AzC;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1732245
    iget-object v0, p0, LX/AzC;->name:Ljava/lang/String;

    return-object v0
.end method
