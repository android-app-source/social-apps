.class public LX/BtT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jp;
.implements LX/0oY;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1829280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1829281
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1829187
    const-string v0, "extra_permalink_param_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829188
    invoke-static {v0}, LX/89m;->valueOf(Ljava/lang/String;)LX/89m;

    move-result-object v1

    .line 1829189
    new-instance v0, LX/BFO;

    invoke-direct {v0}, LX/BFO;-><init>()V

    .line 1829190
    iput-object v1, v0, LX/BFO;->a:LX/89m;

    .line 1829191
    move-object v0, v0

    .line 1829192
    const-string v2, "use_photo_mode"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1829193
    iput-boolean v2, v0, LX/BFO;->p:Z

    .line 1829194
    move-object v2, v0

    .line 1829195
    const-string v0, "feedback_logging_params"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v0}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v0

    sget-object v3, LX/21D;->PERMALINK:LX/21D;

    .line 1829196
    iput-object v3, v0, LX/21A;->i:LX/21D;

    .line 1829197
    move-object v0, v0

    .line 1829198
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 1829199
    iput-object v0, v2, LX/BFO;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1829200
    move-object v0, v2

    .line 1829201
    const-string v2, "RESHARE_BUTTON_EXPERIMENT_CLICKED"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1829202
    iput-boolean v2, v0, LX/BFO;->s:Z

    .line 1829203
    move-object v0, v0

    .line 1829204
    const-string v2, "is_from_deferred feedback"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/BFO;->a(Ljava/lang/Boolean;)LX/BFO;

    move-result-object v2

    .line 1829205
    const-string v0, "NOTIF_LOG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1829206
    if-eqz v0, :cond_0

    .line 1829207
    iput-object v0, v2, LX/BFO;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1829208
    :cond_0
    const-string v0, "search_results_decoration"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1829209
    if-eqz v0, :cond_1

    .line 1829210
    iput-object v0, v2, LX/BFO;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1829211
    :cond_1
    const-string v0, "include_comments_disabled_fields"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1829212
    iput-boolean v0, v2, LX/BFO;->j:Z

    .line 1829213
    const-string v0, "show_keyboard_on_first_load"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1829214
    iput-boolean v0, v2, LX/BFO;->o:Z

    .line 1829215
    const-string v0, "default_comment_ordering"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829216
    if-eqz v0, :cond_2

    .line 1829217
    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    .line 1829218
    iput-object v0, v2, LX/BFO;->i:LX/21y;

    .line 1829219
    const-string v0, "story_fbid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829220
    if-eqz v0, :cond_2

    .line 1829221
    iput-object v0, v2, LX/BFO;->h:Ljava/lang/String;

    .line 1829222
    :cond_2
    sget-object v0, LX/BtS;->a:[I

    invoke-virtual {v1}, LX/89m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1829223
    const-string v0, "story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829224
    iput-object v0, v2, LX/BFO;->c:Ljava/lang/String;

    .line 1829225
    const-string v0, "story_cache_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829226
    iput-object v0, v2, LX/BFO;->d:Ljava/lang/String;

    .line 1829227
    const-string v0, "permalink_cache_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829228
    if-eqz v0, :cond_3

    .line 1829229
    invoke-static {v0}, LX/89g;->valueOf(Ljava/lang/String;)LX/89g;

    move-result-object v0

    .line 1829230
    iput-object v0, v2, LX/BFO;->b:LX/89g;

    .line 1829231
    :cond_3
    const-string v0, "relevant_comment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829232
    iput-object v0, v2, LX/BFO;->f:Ljava/lang/String;

    .line 1829233
    const-string v0, "relevant_comment"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1829234
    iput-object v0, v2, LX/BFO;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1829235
    const-string v0, "comment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829236
    iput-object v0, v2, LX/BFO;->g:Ljava/lang/String;

    .line 1829237
    const-string v0, "comment"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1829238
    iput-object v0, v2, LX/BFO;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1829239
    const-string v0, "relevant_reaction_key"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1829240
    iput v0, v2, LX/BFO;->q:I

    .line 1829241
    const-string v0, "notification_source"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829242
    if-eqz v0, :cond_4

    .line 1829243
    invoke-static {v0}, LX/21C;->valueOf(Ljava/lang/String;)LX/21C;

    move-result-object v0

    .line 1829244
    iput-object v0, v2, LX/BFO;->k:LX/21C;

    .line 1829245
    :cond_4
    const-string v0, "autofill_mention_tagging_profile"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1829246
    iput-object v0, v2, LX/BFO;->v:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1829247
    :cond_5
    :goto_0
    invoke-virtual {v2}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    .line 1829248
    new-instance v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {v1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;-><init>()V

    .line 1829249
    invoke-virtual {v0}, Lcom/facebook/permalink/PermalinkParams;->u()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1829250
    move-object v0, v1

    .line 1829251
    :goto_1
    return-object v0

    .line 1829252
    :pswitch_0
    const-string v0, "extra_platform_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829253
    iput-object v0, v2, LX/BFO;->c:Ljava/lang/String;

    .line 1829254
    goto :goto_0

    .line 1829255
    :pswitch_1
    const-string v0, "story_fbid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829256
    iput-object v0, v2, LX/BFO;->c:Ljava/lang/String;

    .line 1829257
    goto :goto_0

    .line 1829258
    :pswitch_2
    const-string v0, "permalink_story"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829259
    iput-object v0, v2, LX/BFO;->e:Ljava/lang/String;

    .line 1829260
    goto :goto_0

    .line 1829261
    :pswitch_3
    const-string v0, "permalink_story"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829262
    iput-object v0, v2, LX/BFO;->e:Ljava/lang/String;

    .line 1829263
    iget-object v0, p0, LX/BtT;->a:LX/0Uh;

    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1829264
    invoke-virtual {v2}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    .line 1829265
    new-instance v1, Lcom/facebook/feed/storypermalink/AdPreviewPermalinkFragment;

    invoke-direct {v1}, Lcom/facebook/feed/storypermalink/AdPreviewPermalinkFragment;-><init>()V

    .line 1829266
    invoke-virtual {v0}, Lcom/facebook/permalink/PermalinkParams;->u()Landroid/os/Bundle;

    move-result-object v2

    .line 1829267
    const-string v3, "use_recycler_view"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1829268
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1829269
    move-object v0, v1

    .line 1829270
    goto :goto_1

    .line 1829271
    :pswitch_4
    const-string v0, "permalink_story"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1829272
    iput-object v0, v2, LX/BFO;->e:Ljava/lang/String;

    .line 1829273
    invoke-virtual {v2}, LX/BFO;->a()Lcom/facebook/permalink/PermalinkParams;

    move-result-object v0

    .line 1829274
    new-instance v1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;

    invoke-direct {v1}, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;-><init>()V

    .line 1829275
    invoke-virtual {v0}, Lcom/facebook/permalink/PermalinkParams;->u()Landroid/os/Bundle;

    move-result-object v2

    .line 1829276
    const-string v3, "use_recycler_view"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1829277
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1829278
    move-object v0, v1

    .line 1829279
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1829185
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oh;

    const-class v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v0, v1}, LX/0oh;->a(Ljava/lang/Class;)LX/1Av;

    .line 1829186
    return-void
.end method
