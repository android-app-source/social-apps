.class public LX/CNw;
.super LX/0a9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0a9",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "LX/0aC",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1882320
    invoke-direct {p0}, LX/0a9;-><init>()V

    .line 1882321
    return-void
.end method

.method public static a(LX/0QB;)LX/CNw;
    .locals 3

    .prologue
    .line 1882309
    const-class v1, LX/CNw;

    monitor-enter v1

    .line 1882310
    :try_start_0
    sget-object v0, LX/CNw;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1882311
    sput-object v2, LX/CNw;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1882312
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1882313
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1882314
    new-instance v0, LX/CNw;

    invoke-direct {v0}, LX/CNw;-><init>()V

    .line 1882315
    move-object v0, v0

    .line 1882316
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1882317
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CNw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1882318
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1882319
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
