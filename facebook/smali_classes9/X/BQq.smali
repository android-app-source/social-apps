.class public LX/BQq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BQq;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1782819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782820
    return-void
.end method

.method public static a(LX/0QB;)LX/BQq;
    .locals 6

    .prologue
    .line 1782821
    sget-object v0, LX/BQq;->d:LX/BQq;

    if-nez v0, :cond_1

    .line 1782822
    const-class v1, LX/BQq;

    monitor-enter v1

    .line 1782823
    :try_start_0
    sget-object v0, LX/BQq;->d:LX/BQq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1782824
    if-eqz v2, :cond_0

    .line 1782825
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1782826
    new-instance v3, LX/BQq;

    invoke-direct {v3}, LX/BQq;-><init>()V

    .line 1782827
    const/16 v4, 0x5cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1b

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x5c8

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1782828
    iput-object v4, v3, LX/BQq;->a:LX/0Or;

    iput-object v5, v3, LX/BQq;->b:LX/0Or;

    iput-object p0, v3, LX/BQq;->c:LX/0Or;

    .line 1782829
    move-object v0, v3

    .line 1782830
    sput-object v0, LX/BQq;->d:LX/BQq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1782831
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1782832
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1782833
    :cond_1
    sget-object v0, LX/BQq;->d:LX/BQq;

    return-object v0

    .line 1782834
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1782835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ZLcom/facebook/fig/button/FigButton;)V
    .locals 2

    .prologue
    .line 1782836
    iget-object v0, p0, LX/BQq;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 1782837
    if-eqz p1, :cond_0

    const v1, 0x7f081899

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1782838
    :goto_0
    iget-object v0, p0, LX/BQq;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23P;

    .line 1782839
    invoke-virtual {v0, v1, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1782840
    return-void

    .line 1782841
    :cond_0
    const v1, 0x7f081898

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method
