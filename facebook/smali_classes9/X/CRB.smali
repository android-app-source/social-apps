.class public final LX/CRB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1890496
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1890497
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1890498
    :goto_0
    return v1

    .line 1890499
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 1890500
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1890501
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1890502
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1890503
    const-string v12, "rating_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1890504
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v6

    goto :goto_1

    .line 1890505
    :cond_1
    const-string v12, "scale"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1890506
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    goto :goto_1

    .line 1890507
    :cond_2
    const-string v12, "value"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1890508
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 1890509
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1890510
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1890511
    if-eqz v8, :cond_5

    .line 1890512
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 1890513
    :cond_5
    if-eqz v7, :cond_6

    .line 1890514
    invoke-virtual {p1, v6, v9, v1}, LX/186;->a(III)V

    .line 1890515
    :cond_6
    if-eqz v0, :cond_7

    .line 1890516
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1890517
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1890518
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1890519
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1890520
    if-eqz v0, :cond_0

    .line 1890521
    const-string v1, "rating_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890522
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1890523
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1890524
    if-eqz v0, :cond_1

    .line 1890525
    const-string v1, "scale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890526
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1890527
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1890528
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1890529
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890530
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1890531
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1890532
    return-void
.end method
