.class public LX/CCk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CCi;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CCl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1858284
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CCk;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CCl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858281
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1858282
    iput-object p1, p0, LX/CCk;->b:LX/0Ot;

    .line 1858283
    return-void
.end method

.method public static a(LX/0QB;)LX/CCk;
    .locals 4

    .prologue
    .line 1858270
    const-class v1, LX/CCk;

    monitor-enter v1

    .line 1858271
    :try_start_0
    sget-object v0, LX/CCk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858272
    sput-object v2, LX/CCk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858273
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858274
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858275
    new-instance v3, LX/CCk;

    const/16 p0, 0x2199

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CCk;-><init>(LX/0Ot;)V

    .line 1858276
    move-object v0, v3

    .line 1858277
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858278
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858279
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1858268
    invoke-static {}, LX/1dS;->b()V

    .line 1858269
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1858265
    iget-object v0, p0, LX/CCk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1858266
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 1858267
    return-object v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1858264
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 11

    .prologue
    .line 1858251
    check-cast p3, LX/CCj;

    .line 1858252
    iget-object v0, p0, LX/CCk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CCl;

    iget-object v1, p3, LX/CCj;->a:Ljava/lang/String;

    .line 1858253
    iget-object v2, v0, LX/CCl;->a:LX/D1j;

    .line 1858254
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    .line 1858255
    const-string v4, "story_id"

    invoke-virtual {v3, v4, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 1858256
    iget-object v3, v2, LX/D1j;->b:LX/0if;

    sget-object v4, LX/D1j;->a:LX/0ih;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    int-to-long v5, v5

    .line 1858257
    invoke-static {}, LX/15o;->a()LX/15p;

    move-result-object v7

    .line 1858258
    iput-object v4, v7, LX/15p;->a:LX/0ih;

    .line 1858259
    move-object v7, v7

    .line 1858260
    invoke-virtual {v7, v5, v6}, LX/15p;->a(J)LX/15p;

    move-result-object v7

    iget-object v8, v3, LX/0if;->d:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, LX/15p;->b(J)LX/15p;

    move-result-object v7

    invoke-virtual {v7}, LX/15p;->a()LX/15o;

    move-result-object v7

    .line 1858261
    iget-object v8, v3, LX/0if;->g:LX/11Y;

    iget-object v9, v3, LX/0if;->g:LX/11Y;

    const/4 v10, 0x7

    invoke-virtual {v9, v10, v7}, LX/11Y;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/11Y;->sendMessage(Landroid/os/Message;)Z

    .line 1858262
    iget-object v3, v2, LX/D1j;->b:LX/0if;

    sget-object v4, LX/D1j;->a:LX/0ih;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v3, v4, v5, v6, v1}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1858263
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1858250
    const/16 v0, 0xf

    return v0
.end method
