.class public final enum LX/ArH;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/5oU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArH;",
        ">;",
        "LX/5oU;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArH;

.field public static final enum CAMERA_BUTTON_IN_FEED_IMPRESSION:LX/ArH;

.field public static final enum CAMERA_CAPTURE:LX/ArH;

.field public static final enum CAMERA_FLIP:LX/ArH;

.field public static final enum CANCEL_COMPOSER:LX/ArH;

.field public static final enum EFFECT_APPLIED:LX/ArH;

.field public static final enum END_CAMERA_SESSION:LX/ArH;

.field public static final enum END_DOODLE_SESSION:LX/ArH;

.field public static final enum END_EDITING_SESSION:LX/ArH;

.field public static final enum END_EFFECTS_TRAY_SESSION:LX/ArH;

.field public static final enum END_GALLERY_SESSION:LX/ArH;

.field public static final enum END_INSPIRATION_SESSION:LX/ArH;

.field public static final enum END_NUX_SESSION:LX/ArH;

.field public static final enum END_SHARE_SHEET_SESSION:LX/ArH;

.field public static final enum END_TEXT_SESSION:LX/ArH;

.field public static final enum GALLERY_SELECT:LX/ArH;

.field public static final enum IMPRESSION:LX/ArH;

.field public static final enum IMPRESSION_END:LX/ArH;

.field public static final enum NO_EFFECTS:LX/ArH;

.field public static final enum NUX_BACK_TO_TOP:LX/ArH;

.field public static final enum NUX_BUTTON_CLICKED:LX/ArH;

.field public static final enum NUX_DIALOG_BUTTON_CLICKED_CANCEL:LX/ArH;

.field public static final enum NUX_DIALOG_BUTTON_CLICKED_NOT_NOW:LX/ArH;

.field public static final enum NUX_DIALOG_BUTTON_CLICKED_OK:LX/ArH;

.field public static final enum NUX_DIALOG_BUTTON_CLICKED_OPEN_SETTINGS:LX/ArH;

.field public static final enum NUX_SHOWN:LX/ArH;

.field public static final enum NUX_VIDEO_DOWNLOAD:LX/ArH;

.field public static final enum PERMISSION_ACCEPTED:LX/ArH;

.field public static final enum PERMISSION_DENIED:LX/ArH;

.field public static final enum POST_CREATED:LX/ArH;

.field public static final enum POST_PROMPT:LX/ArH;

.field public static final enum PREVIEW_CONFIRM:LX/ArH;

.field public static final enum SAVE_TO_CAMERA_ROLL:LX/ArH;

.field public static final enum SCREENSHOT:LX/ArH;

.field public static final enum START_CAMERA_SESSION:LX/ArH;

.field public static final enum START_DOODLE_SESSION:LX/ArH;

.field public static final enum START_EDITING_SESSION:LX/ArH;

.field public static final enum START_EFFECTS_TRAY_SESSION:LX/ArH;

.field public static final enum START_GALLERY_SESSION:LX/ArH;

.field public static final enum START_INSPIRATION_SESSION:LX/ArH;

.field public static final enum START_NUX_SESSION:LX/ArH;

.field public static final enum START_SHARE_SHEET_SESSION:LX/ArH;

.field public static final enum START_TEXT_SESSION:LX/ArH;

.field public static final enum TAP_FLASH_BUTTON:LX/ArH;

.field public static final enum THUMBNAIL_IMPRESSION:LX/ArH;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1718758
    new-instance v0, LX/ArH;

    const-string v1, "CAMERA_FLIP"

    const-string v2, "camera_flip"

    invoke-direct {v0, v1, v4, v2}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->CAMERA_FLIP:LX/ArH;

    .line 1718759
    new-instance v0, LX/ArH;

    const-string v1, "SAVE_TO_CAMERA_ROLL"

    const-string v2, "save_to_camera_roll"

    invoke-direct {v0, v1, v5, v2}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->SAVE_TO_CAMERA_ROLL:LX/ArH;

    .line 1718760
    new-instance v0, LX/ArH;

    const-string v1, "TAP_FLASH_BUTTON"

    const-string v2, "tap_flash_button"

    invoke-direct {v0, v1, v6, v2}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->TAP_FLASH_BUTTON:LX/ArH;

    .line 1718761
    new-instance v0, LX/ArH;

    const-string v1, "SCREENSHOT"

    const-string v2, "screenshot"

    invoke-direct {v0, v1, v7, v2}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->SCREENSHOT:LX/ArH;

    .line 1718762
    new-instance v0, LX/ArH;

    const-string v1, "START_NUX_SESSION"

    const-string v2, "start_nux_session"

    invoke-direct {v0, v1, v8, v2}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_NUX_SESSION:LX/ArH;

    .line 1718763
    new-instance v0, LX/ArH;

    const-string v1, "END_NUX_SESSION"

    const/4 v2, 0x5

    const-string v3, "end_nux_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_NUX_SESSION:LX/ArH;

    .line 1718764
    new-instance v0, LX/ArH;

    const-string v1, "NUX_SHOWN"

    const/4 v2, 0x6

    const-string v3, "nux_shown"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_SHOWN:LX/ArH;

    .line 1718765
    new-instance v0, LX/ArH;

    const-string v1, "NUX_BUTTON_CLICKED"

    const/4 v2, 0x7

    const-string v3, "nux_button_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_BUTTON_CLICKED:LX/ArH;

    .line 1718766
    new-instance v0, LX/ArH;

    const-string v1, "NUX_DIALOG_BUTTON_CLICKED_OK"

    const/16 v2, 0x8

    const-string v3, "nux_dialog_button_clicked_ok"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_OK:LX/ArH;

    .line 1718767
    new-instance v0, LX/ArH;

    const-string v1, "NUX_DIALOG_BUTTON_CLICKED_CANCEL"

    const/16 v2, 0x9

    const-string v3, "nux_dialog_button_clicked_cancel"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_CANCEL:LX/ArH;

    .line 1718768
    new-instance v0, LX/ArH;

    const-string v1, "NUX_DIALOG_BUTTON_CLICKED_OPEN_SETTINGS"

    const/16 v2, 0xa

    const-string v3, "nux_dialog_button_clicked_open_settings"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_OPEN_SETTINGS:LX/ArH;

    .line 1718769
    new-instance v0, LX/ArH;

    const-string v1, "NUX_DIALOG_BUTTON_CLICKED_NOT_NOW"

    const/16 v2, 0xb

    const-string v3, "nux_dialog_button_clicked_not_now"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_NOT_NOW:LX/ArH;

    .line 1718770
    new-instance v0, LX/ArH;

    const-string v1, "PERMISSION_ACCEPTED"

    const/16 v2, 0xc

    const-string v3, "permission_accepted"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->PERMISSION_ACCEPTED:LX/ArH;

    .line 1718771
    new-instance v0, LX/ArH;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0xd

    const-string v3, "permission_denied"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->PERMISSION_DENIED:LX/ArH;

    .line 1718772
    new-instance v0, LX/ArH;

    const-string v1, "NUX_VIDEO_DOWNLOAD"

    const/16 v2, 0xe

    const-string v3, "nux_video_download"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_VIDEO_DOWNLOAD:LX/ArH;

    .line 1718773
    new-instance v0, LX/ArH;

    const-string v1, "NUX_BACK_TO_TOP"

    const/16 v2, 0xf

    const-string v3, "nux_back_to_top"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NUX_BACK_TO_TOP:LX/ArH;

    .line 1718774
    new-instance v0, LX/ArH;

    const-string v1, "IMPRESSION"

    const/16 v2, 0x10

    const-string v3, "impression"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->IMPRESSION:LX/ArH;

    .line 1718775
    new-instance v0, LX/ArH;

    const-string v1, "IMPRESSION_END"

    const/16 v2, 0x11

    const-string v3, "impression_end"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->IMPRESSION_END:LX/ArH;

    .line 1718776
    new-instance v0, LX/ArH;

    const-string v1, "EFFECT_APPLIED"

    const/16 v2, 0x12

    const-string v3, "effect_applied"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->EFFECT_APPLIED:LX/ArH;

    .line 1718777
    new-instance v0, LX/ArH;

    const-string v1, "CAMERA_CAPTURE"

    const/16 v2, 0x13

    const-string v3, "camera_capture"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->CAMERA_CAPTURE:LX/ArH;

    .line 1718778
    new-instance v0, LX/ArH;

    const-string v1, "GALLERY_SELECT"

    const/16 v2, 0x14

    const-string v3, "gallery_select"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->GALLERY_SELECT:LX/ArH;

    .line 1718779
    new-instance v0, LX/ArH;

    const-string v1, "PREVIEW_CONFIRM"

    const/16 v2, 0x15

    const-string v3, "preview_confirm"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->PREVIEW_CONFIRM:LX/ArH;

    .line 1718780
    new-instance v0, LX/ArH;

    const-string v1, "CANCEL_COMPOSER"

    const/16 v2, 0x16

    const-string v3, "cancel_composer"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->CANCEL_COMPOSER:LX/ArH;

    .line 1718781
    new-instance v0, LX/ArH;

    const-string v1, "POST_PROMPT"

    const/16 v2, 0x17

    const-string v3, "post_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->POST_PROMPT:LX/ArH;

    .line 1718782
    new-instance v0, LX/ArH;

    const-string v1, "POST_CREATED"

    const/16 v2, 0x18

    const-string v3, "post_created"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->POST_CREATED:LX/ArH;

    .line 1718783
    new-instance v0, LX/ArH;

    const-string v1, "START_INSPIRATION_SESSION"

    const/16 v2, 0x19

    const-string v3, "start_inspiration_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_INSPIRATION_SESSION:LX/ArH;

    .line 1718784
    new-instance v0, LX/ArH;

    const-string v1, "END_INSPIRATION_SESSION"

    const/16 v2, 0x1a

    const-string v3, "end_inspiration_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_INSPIRATION_SESSION:LX/ArH;

    .line 1718785
    new-instance v0, LX/ArH;

    const-string v1, "START_CAMERA_SESSION"

    const/16 v2, 0x1b

    const-string v3, "start_camera_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_CAMERA_SESSION:LX/ArH;

    .line 1718786
    new-instance v0, LX/ArH;

    const-string v1, "END_CAMERA_SESSION"

    const/16 v2, 0x1c

    const-string v3, "end_camera_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_CAMERA_SESSION:LX/ArH;

    .line 1718787
    new-instance v0, LX/ArH;

    const-string v1, "START_EDITING_SESSION"

    const/16 v2, 0x1d

    const-string v3, "start_editing_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_EDITING_SESSION:LX/ArH;

    .line 1718788
    new-instance v0, LX/ArH;

    const-string v1, "END_EDITING_SESSION"

    const/16 v2, 0x1e

    const-string v3, "end_editing_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_EDITING_SESSION:LX/ArH;

    .line 1718789
    new-instance v0, LX/ArH;

    const-string v1, "START_SHARE_SHEET_SESSION"

    const/16 v2, 0x1f

    const-string v3, "start_share_sheet_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_SHARE_SHEET_SESSION:LX/ArH;

    .line 1718790
    new-instance v0, LX/ArH;

    const-string v1, "END_SHARE_SHEET_SESSION"

    const/16 v2, 0x20

    const-string v3, "end_share_sheet_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_SHARE_SHEET_SESSION:LX/ArH;

    .line 1718791
    new-instance v0, LX/ArH;

    const-string v1, "START_EFFECTS_TRAY_SESSION"

    const/16 v2, 0x21

    const-string v3, "start_effects_tray_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_EFFECTS_TRAY_SESSION:LX/ArH;

    .line 1718792
    new-instance v0, LX/ArH;

    const-string v1, "END_EFFECTS_TRAY_SESSION"

    const/16 v2, 0x22

    const-string v3, "end_effects_tray_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_EFFECTS_TRAY_SESSION:LX/ArH;

    .line 1718793
    new-instance v0, LX/ArH;

    const-string v1, "START_GALLERY_SESSION"

    const/16 v2, 0x23

    const-string v3, "start_gallery_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_GALLERY_SESSION:LX/ArH;

    .line 1718794
    new-instance v0, LX/ArH;

    const-string v1, "END_GALLERY_SESSION"

    const/16 v2, 0x24

    const-string v3, "end_gallery_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_GALLERY_SESSION:LX/ArH;

    .line 1718795
    new-instance v0, LX/ArH;

    const-string v1, "START_DOODLE_SESSION"

    const/16 v2, 0x25

    const-string v3, "start_doodle_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_DOODLE_SESSION:LX/ArH;

    .line 1718796
    new-instance v0, LX/ArH;

    const-string v1, "END_DOODLE_SESSION"

    const/16 v2, 0x26

    const-string v3, "end_doodle_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_DOODLE_SESSION:LX/ArH;

    .line 1718797
    new-instance v0, LX/ArH;

    const-string v1, "START_TEXT_SESSION"

    const/16 v2, 0x27

    const-string v3, "start_text_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->START_TEXT_SESSION:LX/ArH;

    .line 1718798
    new-instance v0, LX/ArH;

    const-string v1, "END_TEXT_SESSION"

    const/16 v2, 0x28

    const-string v3, "end_text_session"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->END_TEXT_SESSION:LX/ArH;

    .line 1718799
    new-instance v0, LX/ArH;

    const-string v1, "THUMBNAIL_IMPRESSION"

    const/16 v2, 0x29

    const-string v3, "thumbnail_impression"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->THUMBNAIL_IMPRESSION:LX/ArH;

    .line 1718800
    new-instance v0, LX/ArH;

    const-string v1, "NO_EFFECTS"

    const/16 v2, 0x2a

    const-string v3, "no_effects"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->NO_EFFECTS:LX/ArH;

    .line 1718801
    new-instance v0, LX/ArH;

    const-string v1, "CAMERA_BUTTON_IN_FEED_IMPRESSION"

    const/16 v2, 0x2b

    const-string v3, "camera_button_in_feed_impression"

    invoke-direct {v0, v1, v2, v3}, LX/ArH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArH;->CAMERA_BUTTON_IN_FEED_IMPRESSION:LX/ArH;

    .line 1718802
    const/16 v0, 0x2c

    new-array v0, v0, [LX/ArH;

    sget-object v1, LX/ArH;->CAMERA_FLIP:LX/ArH;

    aput-object v1, v0, v4

    sget-object v1, LX/ArH;->SAVE_TO_CAMERA_ROLL:LX/ArH;

    aput-object v1, v0, v5

    sget-object v1, LX/ArH;->TAP_FLASH_BUTTON:LX/ArH;

    aput-object v1, v0, v6

    sget-object v1, LX/ArH;->SCREENSHOT:LX/ArH;

    aput-object v1, v0, v7

    sget-object v1, LX/ArH;->START_NUX_SESSION:LX/ArH;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ArH;->END_NUX_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ArH;->NUX_SHOWN:LX/ArH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ArH;->NUX_BUTTON_CLICKED:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_OK:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_CANCEL:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_OPEN_SETTINGS:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/ArH;->NUX_DIALOG_BUTTON_CLICKED_NOT_NOW:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/ArH;->PERMISSION_ACCEPTED:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/ArH;->PERMISSION_DENIED:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/ArH;->NUX_VIDEO_DOWNLOAD:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/ArH;->NUX_BACK_TO_TOP:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/ArH;->IMPRESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/ArH;->IMPRESSION_END:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/ArH;->EFFECT_APPLIED:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/ArH;->CAMERA_CAPTURE:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/ArH;->GALLERY_SELECT:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/ArH;->PREVIEW_CONFIRM:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/ArH;->CANCEL_COMPOSER:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/ArH;->POST_PROMPT:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/ArH;->POST_CREATED:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/ArH;->START_INSPIRATION_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/ArH;->END_INSPIRATION_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/ArH;->START_CAMERA_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/ArH;->END_CAMERA_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/ArH;->START_EDITING_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/ArH;->END_EDITING_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/ArH;->START_SHARE_SHEET_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/ArH;->END_SHARE_SHEET_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/ArH;->START_EFFECTS_TRAY_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/ArH;->END_EFFECTS_TRAY_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/ArH;->START_GALLERY_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/ArH;->END_GALLERY_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/ArH;->START_DOODLE_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/ArH;->END_DOODLE_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/ArH;->START_TEXT_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/ArH;->END_TEXT_SESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/ArH;->THUMBNAIL_IMPRESSION:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/ArH;->NO_EFFECTS:LX/ArH;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/ArH;->CAMERA_BUTTON_IN_FEED_IMPRESSION:LX/ArH;

    aput-object v2, v0, v1

    sput-object v0, LX/ArH;->$VALUES:[LX/ArH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1718803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1718804
    iput-object p3, p0, LX/ArH;->mName:Ljava/lang/String;

    .line 1718805
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArH;
    .locals 1

    .prologue
    .line 1718806
    const-class v0, LX/ArH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArH;

    return-object v0
.end method

.method public static values()[LX/ArH;
    .locals 1

    .prologue
    .line 1718807
    sget-object v0, LX/ArH;->$VALUES:[LX/ArH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArH;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718808
    iget-object v0, p0, LX/ArH;->mName:Ljava/lang/String;

    return-object v0
.end method
