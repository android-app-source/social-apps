.class public LX/Arq;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/0fO;

.field public B:LX/6JR;

.field public C:Z

.field public D:LX/6Ia;

.field public E:LX/6JB;

.field public F:LX/6JC;

.field public G:LX/6JF;

.field public H:Z

.field private I:Z

.field public J:Ljava/lang/Integer;

.field private final K:LX/6KB;

.field private final b:LX/6Ik;

.field public final c:LX/6Ik;

.field private final d:Landroid/content/Context;

.field public final e:LX/Arh;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/view/WindowManager;

.field public final h:LX/03V;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private final k:LX/6KN;

.field public final l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

.field public final m:LX/BAC;

.field public final n:LX/8Fw;

.field public final o:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

.field private final p:LX/6Jw;

.field public final q:LX/BVQ;

.field public final r:LX/6Jt;

.field private final s:LX/0Zr;

.field private final t:LX/6KO;

.field private final u:Landroid/os/Handler;

.field public final v:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/Arb;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final x:LX/1Er;

.field public final y:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field public final z:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1719835
    const-class v0, LX/Arq;

    sput-object v0, LX/Arq;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Arh;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;ZLandroid/content/Context;Landroid/view/WindowManager;LX/03V;LX/0Zr;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;LX/BAD;LX/8Fx;LX/7SL;LX/BVQ;LX/6KO;LX/1Er;LX/6KT;LX/1FZ;LX/03R;LX/BA4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0fO;)V
    .locals 10
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p19    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719725
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/Arq;->u:Landroid/os/Handler;

    .line 1719726
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    iput-object v1, p0, LX/Arq;->v:LX/0UE;

    .line 1719727
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1719728
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->J:Ljava/lang/Integer;

    .line 1719729
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Arq;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1719730
    invoke-static/range {p17 .. p17}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v1

    invoke-virtual {v1}, LX/6KD;->a()LX/6KB;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->K:LX/6KB;

    .line 1719731
    iput-object p4, p0, LX/Arq;->d:Landroid/content/Context;

    .line 1719732
    iput-object p1, p0, LX/Arq;->e:LX/Arh;

    .line 1719733
    sget-object v1, LX/6KV;->PURPLE_RAIN:LX/6KV;

    invoke-virtual {v1}, LX/6KV;->name()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->f:Ljava/lang/String;

    .line 1719734
    iput-object p2, p0, LX/Arq;->y:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1719735
    invoke-static {p3}, LX/Arq;->a(Z)LX/6JF;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->G:LX/6JF;

    .line 1719736
    iput-object p5, p0, LX/Arq;->g:Landroid/view/WindowManager;

    .line 1719737
    move-object/from16 v0, p6

    iput-object v0, p0, LX/Arq;->h:LX/03V;

    .line 1719738
    move-object/from16 v0, p8

    iput-object v0, p0, LX/Arq;->i:Ljava/util/concurrent/ExecutorService;

    .line 1719739
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Arq;->j:Ljava/util/concurrent/ExecutorService;

    .line 1719740
    move-object/from16 v0, p7

    iput-object v0, p0, LX/Arq;->s:LX/0Zr;

    .line 1719741
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Arq;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    .line 1719742
    new-instance v1, LX/6Jw;

    new-instance v2, LX/7Sq;

    move-object/from16 v0, p18

    invoke-direct {v2, v0}, LX/7Sq;-><init>(LX/1FZ;)V

    invoke-direct {v1, v2}, LX/6Jw;-><init>(LX/7Sq;)V

    iput-object v1, p0, LX/Arq;->p:LX/6Jw;

    .line 1719743
    invoke-virtual/range {p20 .. p20}, LX/BA4;->a()LX/7ex;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, LX/BAD;->a(LX/7ex;)LX/BAC;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->m:LX/BAC;

    .line 1719744
    const/4 v1, 0x0

    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, LX/8Fx;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)LX/8Fw;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->n:LX/8Fw;

    .line 1719745
    iget-object v1, p0, LX/Arq;->d:Landroid/content/Context;

    move-object/from16 v0, p13

    invoke-virtual {v0, v1}, LX/7SL;->a(Landroid/content/Context;)Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->o:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    .line 1719746
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Arq;->q:LX/BVQ;

    .line 1719747
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Arq;->t:LX/6KO;

    .line 1719748
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Arq;->x:LX/1Er;

    .line 1719749
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Arq;->A:LX/0fO;

    .line 1719750
    iget-object v1, p0, LX/Arq;->t:LX/6KO;

    iget-object v2, p0, LX/Arq;->f:Ljava/lang/String;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/6KO;->a(Ljava/lang/String;Ljava/lang/String;)LX/6KN;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->k:LX/6KN;

    .line 1719751
    new-instance v1, LX/6Jt;

    iget-object v2, p0, LX/Arq;->s:LX/0Zr;

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, LX/Arq;->u:Landroid/os/Handler;

    iget-object v5, p0, LX/Arq;->j:Ljava/util/concurrent/ExecutorService;

    iget-object v6, p0, LX/Arq;->k:LX/6KN;

    iget-object v7, p0, LX/Arq;->K:LX/6KB;

    iget-object v8, p0, LX/Arq;->g:Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getRotation()I

    move-result v8

    invoke-direct {p0}, LX/Arq;->w()LX/6Jp;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, LX/6Jt;-><init>(LX/0Zr;Landroid/content/res/Resources;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/6KN;LX/6KB;ILX/6Jp;)V

    iput-object v1, p0, LX/Arq;->r:LX/6Jt;

    .line 1719752
    invoke-direct {p0}, LX/Arq;->u()LX/6Ik;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->b:LX/6Ik;

    .line 1719753
    invoke-direct {p0}, LX/Arq;->v()LX/6Ik;

    move-result-object v1

    iput-object v1, p0, LX/Arq;->c:LX/6Ik;

    .line 1719754
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1719755
    sget-object v2, LX/03R;->YES:LX/03R;

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1719756
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->p:LX/6Jw;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719757
    :cond_0
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->m:LX/BAC;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719758
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719759
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->n:LX/8Fw;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719760
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->o:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719761
    new-instance v2, LX/6KY;

    iget-object v3, p0, LX/Arq;->q:LX/BVQ;

    invoke-direct {v2, v3}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1719762
    iget-object v2, p0, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v2, v1}, LX/6Jt;->a(Ljava/util/List;)V

    .line 1719763
    return-void
.end method

.method private static a(Z)LX/6JF;
    .locals 1

    .prologue
    .line 1719834
    if-eqz p0, :cond_0

    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6JF;->BACK:LX/6JF;

    goto :goto_0
.end method

.method private static a(Ljava/util/List;II)LX/6JR;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;II)",
            "LX/6JR;"
        }
    .end annotation

    .prologue
    .line 1719828
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1719829
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JR;

    .line 1719830
    iget v2, v0, LX/6JR;->a:I

    if-ne v2, p1, :cond_0

    iget v2, v0, LX/6JR;->b:I

    if-ne v2, p2, :cond_0

    .line 1719831
    :goto_1
    return-object v0

    .line 1719832
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1719833
    :cond_1
    invoke-static {p0, p1, p2}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v0

    goto :goto_1
.end method

.method public static a$redex0(LX/Arq;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 1719823
    iget-object v0, p0, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v0}, LX/6Jt;->a()LX/6K9;

    move-result-object v0

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    if-eq v0, v1, :cond_0

    .line 1719824
    :goto_0
    return-void

    .line 1719825
    :cond_0
    iget-object v0, p0, LX/Arq;->r:LX/6Jt;

    iget-object v1, p0, LX/Arq;->B:LX/6JR;

    .line 1719826
    new-instance v2, LX/Aro;

    invoke-direct {v2, p0, p1}, LX/Aro;-><init>(LX/Arq;Ljava/io/File;)V

    move-object v2, v2

    .line 1719827
    invoke-virtual {v0, v1, v2}, LX/6Jt;->a(LX/6JR;LX/6JU;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1719814
    iget-object v0, p0, LX/Arq;->h:LX/03V;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1719815
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 1719816
    move-object v1, v1

    .line 1719817
    iput-object p2, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1719818
    move-object v1, v1

    .line 1719819
    iput v2, v1, LX/0VK;->e:I

    .line 1719820
    move-object v1, v1

    .line 1719821
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1719822
    return-void
.end method

.method public static d(LX/Arq;Ljava/io/File;)LX/6JG;
    .locals 1

    .prologue
    .line 1719813
    new-instance v0, LX/Arn;

    invoke-direct {v0, p0, p1}, LX/Arn;-><init>(LX/Arq;Ljava/io/File;)V

    return-object v0
.end method

.method public static p(LX/Arq;)V
    .locals 2

    .prologue
    .line 1719807
    iget-boolean v0, p0, LX/Arq;->I:Z

    if-nez v0, :cond_0

    .line 1719808
    iget-object v0, p0, LX/Arq;->r:LX/6Jt;

    iget-object v1, p0, LX/Arq;->y:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0, v1}, LX/6Jt;->a(Landroid/view/SurfaceView;)V

    .line 1719809
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Arq;->I:Z

    .line 1719810
    :cond_0
    iget-object v0, p0, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1719811
    iget-object v0, p0, LX/Arq;->D:LX/6Ia;

    iget-object v1, p0, LX/Arq;->b:LX/6Ik;

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6Ik;)V

    .line 1719812
    return-void
.end method

.method public static s(LX/Arq;)Ljava/io/File;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1719801
    iget-object v0, p0, LX/Arq;->x:LX/1Er;

    const-string v1, "FB_VIDEO_FOR_UPLOAD_"

    const-string v2, ".mp4"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1719802
    :try_start_0
    invoke-static {v0}, LX/6JM;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1719803
    :goto_0
    return-object v0

    .line 1719804
    :catch_0
    move-exception v0

    .line 1719805
    const-string v1, "pr_camera_take_video_file_error"

    invoke-static {p0, v1, v0}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719806
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/Arq;)LX/6JB;
    .locals 12

    .prologue
    .line 1719785
    iget-object v0, p0, LX/Arq;->A:LX/0fO;

    .line 1719786
    iget-object v7, v0, LX/0fO;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0oj;

    .line 1719787
    iget-object v8, v7, LX/0oj;->a:LX/0W3;

    sget-wide v10, LX/0X5;->jS:J

    invoke-interface {v8, v10, v11}, LX/0W4;->c(J)J

    move-result-wide v8

    long-to-int v8, v8

    move v7, v8

    .line 1719788
    move v1, v7

    .line 1719789
    iget-object v0, p0, LX/Arq;->A:LX/0fO;

    .line 1719790
    iget-object v7, v0, LX/0fO;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0oj;

    .line 1719791
    iget-object v8, v7, LX/0oj;->a:LX/0W3;

    sget-wide v10, LX/0X5;->jT:J

    invoke-interface {v8, v10, v11}, LX/0W4;->c(J)J

    move-result-wide v8

    long-to-int v8, v8

    move v7, v8

    .line 1719792
    move v2, v7

    .line 1719793
    :try_start_0
    iget-object v0, p0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    .line 1719794
    invoke-interface {v0}, LX/6IP;->c()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v1, v2}, LX/Arq;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v3

    iput-object v3, p0, LX/Arq;->B:LX/6JR;

    .line 1719795
    invoke-interface {v0}, LX/6IP;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1, v2}, LX/Arq;->a(Ljava/util/List;II)LX/6JR;
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    .line 1719796
    :goto_0
    new-instance v0, LX/6JB;

    iget v1, v4, LX/6JR;->a:I

    iget v2, v4, LX/6JR;->b:I

    iget v3, v4, LX/6JR;->a:I

    iget v4, v4, LX/6JR;->b:I

    iget-object v5, p0, LX/Arq;->g:Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/6JB;-><init>(IIIIILjava/util/List;)V

    return-object v0

    .line 1719797
    :catch_0
    move-exception v0

    .line 1719798
    const-string v3, "pr_camera_get_characteristics"

    invoke-static {p0, v3, v0}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719799
    new-instance v0, LX/6JR;

    invoke-direct {v0, v1, v2}, LX/6JR;-><init>(II)V

    iput-object v0, p0, LX/Arq;->B:LX/6JR;

    .line 1719800
    iget-object v0, p0, LX/Arq;->B:LX/6JR;

    move-object v4, v0

    goto :goto_0
.end method

.method private u()LX/6Ik;
    .locals 1

    .prologue
    .line 1719784
    new-instance v0, LX/Ark;

    invoke-direct {v0, p0}, LX/Ark;-><init>(LX/Arq;)V

    return-object v0
.end method

.method private v()LX/6Ik;
    .locals 1

    .prologue
    .line 1719783
    new-instance v0, LX/Arl;

    invoke-direct {v0, p0}, LX/Arl;-><init>(LX/Arq;)V

    return-object v0
.end method

.method private w()LX/6Jp;
    .locals 1

    .prologue
    .line 1719782
    new-instance v0, LX/Arp;

    invoke-direct {v0, p0}, LX/Arp;-><init>(LX/Arq;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1719771
    invoke-static {}, LX/6JD;->a()Z

    move-result v0

    .line 1719772
    if-nez v0, :cond_0

    .line 1719773
    iget-object v0, p0, LX/Arq;->d:Landroid/content/Context;

    iget-object v1, p0, LX/Arq;->G:LX/6JF;

    iget-object v2, p0, LX/Arq;->k:LX/6KN;

    iget-object v3, p0, LX/Arq;->K:LX/6KB;

    .line 1719774
    iget-object v4, v3, LX/6KB;->a:LX/6KF;

    move-object v3, v4

    .line 1719775
    invoke-static {v0, v1, v2, v3}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6KF;)LX/6Ia;

    move-result-object v0

    iput-object v0, p0, LX/Arq;->D:LX/6Ia;

    .line 1719776
    :goto_0
    invoke-static {p0}, LX/Arq;->p(LX/Arq;)V

    .line 1719777
    return-void

    .line 1719778
    :cond_0
    sget-object v0, LX/Arq;->a:Ljava/lang/Class;

    const-string v1, "Forcing camera 1 api on emulator"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1719779
    iget-object v0, p0, LX/Arq;->d:Landroid/content/Context;

    iget-object v1, p0, LX/Arq;->G:LX/6JF;

    iget-object v2, p0, LX/Arq;->k:LX/6KN;

    sget-object v3, LX/6J9;->CAMERA1:LX/6J9;

    iget-object v4, p0, LX/Arq;->K:LX/6KB;

    .line 1719780
    iget-object v5, v4, LX/6KB;->a:LX/6KF;

    move-object v4, v5

    .line 1719781
    invoke-static {v0, v1, v2, v3, v4}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v0

    iput-object v0, p0, LX/Arq;->D:LX/6Ia;

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1719770
    iget-object v0, p0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->d()LX/6JF;

    move-result-object v0

    sget-object v1, LX/6JF;->FRONT:LX/6JF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 1719767
    :try_start_0
    iget-object v0, p0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    invoke-interface {v0}, LX/6IP;->g()I
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 1719768
    :catch_0
    move-exception v0

    .line 1719769
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1719764
    :try_start_0
    iget-object v0, p0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    invoke-interface {v0}, LX/6IP;->h()Z
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 1719765
    :catch_0
    move-exception v0

    .line 1719766
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
