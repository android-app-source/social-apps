.class public LX/B4A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/app/Activity;

.field public final d:LX/B3T;

.field public final e:Lcom/facebook/resources/ui/FbEditText;

.field public final f:Landroid/widget/ImageButton;

.field public final g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public final h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

.field public j:LX/B4O;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/B41;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Landroid/text/TextWatcher;

.field private final n:Landroid/view/View$OnClickListener;

.field public o:Landroid/widget/Filter$FilterListener;

.field public p:LX/B40;

.field public q:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/B3T;Landroid/widget/FrameLayout;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B3T;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/widget/FrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741369
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1741370
    iput-object v0, p0, LX/B4A;->l:LX/0Ot;

    .line 1741371
    new-instance v0, LX/B45;

    invoke-direct {v0, p0}, LX/B45;-><init>(LX/B4A;)V

    iput-object v0, p0, LX/B4A;->m:Landroid/text/TextWatcher;

    .line 1741372
    new-instance v0, LX/B46;

    invoke-direct {v0, p0}, LX/B46;-><init>(LX/B4A;)V

    iput-object v0, p0, LX/B4A;->n:Landroid/view/View$OnClickListener;

    .line 1741373
    iput-object p1, p0, LX/B4A;->c:Landroid/app/Activity;

    .line 1741374
    iput-object p2, p0, LX/B4A;->d:LX/B3T;

    .line 1741375
    const v0, 0x7f0d0a6b

    invoke-virtual {p3, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/B4A;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 1741376
    const v0, 0x7f0d094d

    invoke-virtual {p3, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LX/B4A;->f:Landroid/widget/ImageButton;

    .line 1741377
    iput-object p4, p0, LX/B4A;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1741378
    iput-object p5, p0, LX/B4A;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1741379
    iput-object p6, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1741380
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1741381
    iget-object v0, p0, LX/B4A;->e:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f082748

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 1741382
    iget-object v0, p0, LX/B4A;->e:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, LX/B4A;->m:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1741383
    iget-object v0, p0, LX/B4A;->f:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/B4A;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1741384
    return-void
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;LX/1DI;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;",
            ">;",
            "LX/1DI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1741365
    iget-object v0, p0, LX/B4A;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 1741366
    new-instance v1, LX/B49;

    invoke-direct {v1, p0, p2}, LX/B49;-><init>(LX/B4A;LX/1DI;)V

    invoke-static {p1, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1741367
    return-void
.end method

.method public static b(LX/B4A;)V
    .locals 6

    .prologue
    .line 1741349
    iget-object v0, p0, LX/B4A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    .line 1741350
    iget-object v1, p0, LX/B4A;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1741351
    iget-object v1, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1741352
    iget-object v2, v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1741353
    new-instance v2, LX/B57;

    invoke-direct {v2}, LX/B57;-><init>()V

    move-object v2, v2

    .line 1741354
    const-string v3, "image_overlay_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "page_logo_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_high_width"

    iget-object v4, v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->c:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/B57;

    .line 1741355
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741356
    iput-object v3, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741357
    move-object v2, v2

    .line 1741358
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v4, 0xe10

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 1741359
    iget-object v3, v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1741360
    new-instance v3, LX/B42;

    invoke-direct {v3, v0, v1}, LX/B42;-><init>(Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;Ljava/lang/String;)V

    .line 1741361
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 1741362
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1741363
    new-instance v1, LX/B47;

    invoke-direct {v1, p0}, LX/B47;-><init>(LX/B4A;)V

    invoke-direct {p0, v0, v1}, LX/B4A;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/1DI;)V

    .line 1741364
    return-void
.end method

.method public static c(LX/B4A;)V
    .locals 6

    .prologue
    .line 1741325
    iget-object v0, p0, LX/B4A;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    .line 1741326
    iget-object v1, p0, LX/B4A;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1741327
    iget-object v1, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1741328
    iget-object v2, v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1741329
    new-instance v2, LX/B55;

    invoke-direct {v2}, LX/B55;-><init>()V

    move-object v2, v2

    .line 1741330
    const-string v3, "category_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "page_logo_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "image_high_width"

    iget-object v5, v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->c:LX/0sa;

    invoke-virtual {v5}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1741331
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741332
    iput-object v3, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741333
    move-object v2, v2

    .line 1741334
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v4, 0xe10

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 1741335
    iget-object v3, v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1741336
    new-instance v3, LX/B43;

    invoke-direct {v3, v0, v1}, LX/B43;-><init>(Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;Ljava/lang/String;)V

    .line 1741337
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 1741338
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1741339
    new-instance v1, LX/B48;

    invoke-direct {v1, p0}, LX/B48;-><init>(LX/B4A;)V

    invoke-direct {p0, v0, v1}, LX/B4A;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/1DI;)V

    .line 1741340
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1741341
    iget-object v0, p0, LX/B4A;->j:LX/B4O;

    iget-object v1, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->e()Ljava/lang/String;

    move-result-object v2

    .line 1741342
    const-string v4, "heisman_open_pivot"

    const-string v5, "heisman_composer_session_id"

    const-string v7, "heisman_entry_point"

    move-object v3, v0

    move-object v6, v1

    move-object v8, v2

    invoke-static/range {v3 .. v8}, LX/B4O;->a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741343
    iget-object v0, p0, LX/B4A;->i:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1741344
    iget-object v1, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1741345
    if-eqz v0, :cond_0

    .line 1741346
    invoke-static {p0}, LX/B4A;->c(LX/B4A;)V

    .line 1741347
    :goto_0
    return-void

    .line 1741348
    :cond_0
    invoke-static {p0}, LX/B4A;->b(LX/B4A;)V

    goto :goto_0
.end method
