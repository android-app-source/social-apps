.class public final LX/Ama;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/232;


# instance fields
.field public final synthetic a:LX/0rB;

.field public final synthetic b:LX/Amb;


# direct methods
.method public constructor <init>(LX/Amb;LX/0rB;)V
    .locals 0

    .prologue
    .line 1710931
    iput-object p1, p0, LX/Ama;->b:LX/Amb;

    iput-object p2, p0, LX/Ama;->a:LX/0rB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 5

    .prologue
    .line 1710932
    iget-object v0, p0, LX/Ama;->b:LX/Amb;

    iget-object v0, v0, LX/Amb;->o:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    iget-object v1, p0, LX/Ama;->a:LX/0rB;

    invoke-virtual {v1}, LX/0rB;->b()LX/0rn;

    move-result-object v1

    const/4 p0, 0x0

    .line 1710933
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1710934
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v2

    sget-object v3, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 1710935
    :goto_0
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v2, v2

    .line 1710936
    sget-object v4, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v2, v4, :cond_1

    .line 1710937
    invoke-static {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    .line 1710938
    :goto_1
    move-object v0, v2

    .line 1710939
    return-object v0

    .line 1710940
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    .line 1710941
    :cond_1
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1710942
    iget-object v4, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v4

    .line 1710943
    sget-object v4, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v2, v4}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1710944
    iget-boolean v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v2, v2

    .line 1710945
    if-eqz v2, :cond_2

    .line 1710946
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v2, v2

    .line 1710947
    sget-object v4, LX/0rU;->TAIL:LX/0rU;

    if-eq v2, v4, :cond_2

    .line 1710948
    invoke-static {v0, p2, p1, p0, v1}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Lcom/facebook/api/feed/FetchFeedParams;LX/14U;LX/0rn;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    move-object v4, v2

    .line 1710949
    :goto_2
    iget-object v2, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    invoke-virtual {v2, v4}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    move-object v4, v2

    .line 1710950
    :goto_3
    iget-object v2, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0qU;

    invoke-virtual {v2, v4, v3}, LX/0qU;->a(Lcom/facebook/api/feed/FetchFeedResult;Z)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    goto :goto_1

    .line 1710951
    :cond_2
    iget-object v2, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/18V;

    invoke-virtual {v2, v1, p1, p0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/feed/FetchFeedResult;

    move-object v4, v2

    goto :goto_2

    .line 1710952
    :cond_3
    iget-object v2, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/18V;

    invoke-virtual {v2, v1, p1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/feed/FetchFeedResult;

    move-object v4, v2

    goto :goto_3
.end method
