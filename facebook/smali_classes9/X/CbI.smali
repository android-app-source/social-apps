.class public LX/CbI;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:LX/Cau;

.field public final b:LX/0ad;

.field public c:LX/8Jm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8Jm",
            "<",
            "LX/CbA;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/Caq;

.field public e:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "LX/CbA;",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$TagInfoQuery$Edges;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Cau;LX/0ad;LX/8Jn;LX/Caq;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Cau;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919379
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1919380
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/CbI;->e:LX/0Ri;

    .line 1919381
    iput-object p3, p0, LX/CbI;->b:LX/0ad;

    .line 1919382
    iput-object p2, p0, LX/CbI;->a:LX/Cau;

    .line 1919383
    invoke-virtual {p0}, LX/CbI;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p4, p0, v0}, LX/8Jn;->a(Landroid/view/View;F)LX/8Jm;

    move-result-object v0

    iput-object v0, p0, LX/CbI;->c:LX/8Jm;

    .line 1919384
    iput-object p5, p0, LX/CbI;->d:LX/Caq;

    .line 1919385
    return-void
.end method

.method public static a(LX/CbI;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;Z)LX/CbA;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1919376
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CbI;->b:LX/0ad;

    sget-short v1, LX/1xL;->c:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919377
    new-instance v0, LX/CbA;

    invoke-virtual {p0}, LX/CbI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->a()Z

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/CbI;->a:LX/Cau;

    move v7, p2

    invoke-direct/range {v0 .. v7}, LX/CbA;-><init>(Landroid/content/Context;ZZLjava/lang/String;Ljava/lang/String;LX/Cau;Z)V

    .line 1919378
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/CbA;

    invoke-virtual {p0}, LX/CbI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->a()Z

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/CbI;->a:LX/Cau;

    move v6, p2

    invoke-direct/range {v0 .. v6}, LX/CbA;-><init>(Landroid/content/Context;ZZLjava/lang/String;LX/Cau;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CbA;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1919386
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919387
    iget-object v0, p0, LX/CbI;->e:LX/0Ri;

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1919371
    iget-object v0, p0, LX/CbI;->e:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CbA;

    .line 1919372
    iget-object p0, v0, LX/CbA;->d:Lcom/facebook/widget/PhotoButton;

    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->isShown()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1919373
    iget-object p0, v0, LX/CbA;->o:LX/8Hm;

    invoke-virtual {v0, p0}, LX/CbA;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1919374
    :cond_0
    goto :goto_0

    .line 1919375
    :cond_1
    return-void
.end method
