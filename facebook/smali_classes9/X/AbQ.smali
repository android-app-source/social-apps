.class public LX/AbQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690763
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1690670
    check-cast p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;

    .line 1690671
    iget-object v0, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1690672
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1690673
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "description"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690674
    iget-object p0, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    move-object v3, p0

    .line 1690675
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690676
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    const/4 v4, 0x0

    .line 1690677
    iget-object v5, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-nez v5, :cond_6

    .line 1690678
    :cond_0
    :goto_0
    move-object v3, v4

    .line 1690679
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690680
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690681
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1690682
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690683
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_action_type_id"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690684
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->f:Ljava/lang/String;

    move-object v3, v4

    .line 1690685
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690686
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_object_id"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690687
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->g:Ljava/lang/String;

    move-object v3, v4

    .line 1690688
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690689
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_phrase"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690690
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->i:Ljava/lang/String;

    move-object v3, v4

    .line 1690691
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690692
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "og_icon_id"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690693
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->h:Ljava/lang/String;

    move-object v3, v4

    .line 1690694
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690695
    iget-object v1, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690696
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    move-object v1, v2

    .line 1690697
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1690698
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690699
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    move-object v3, v4

    .line 1690700
    invoke-static {v3}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v3

    .line 1690701
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x2c

    invoke-static {v5}, LX/0PO;->on(C)LX/0PO;

    move-result-object v5

    invoke-virtual {v5}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1690702
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690703
    :cond_1
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_eligible_for_commercial_break"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690704
    iget-boolean v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->m:Z

    move v3, v4

    .line 1690705
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690706
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_audio_only"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690707
    iget-boolean v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->q:Z

    move v3, v4

    .line 1690708
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690709
    iget-object v1, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690710
    iget-boolean v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->r:Z

    move v1, v2

    .line 1690711
    if-eqz v1, :cond_2

    .line 1690712
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "status"

    const-string v3, "LIVE_NOW"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690713
    :cond_2
    iget-object v1, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690714
    iget-boolean v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->n:Z

    move v1, v2

    .line 1690715
    if-eqz v1, :cond_3

    .line 1690716
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "post_surfaces_blacklist"

    const-string v3, "2"

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v3

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690717
    :cond_3
    iget-object v1, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690718
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    move-object v1, v2

    .line 1690719
    if-eqz v1, :cond_4

    .line 1690720
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sponsor_id"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690721
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    move-object v3, v4

    .line 1690722
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690723
    :cond_4
    iget-object v1, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690724
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastCompositionData;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1690725
    if-eqz v1, :cond_5

    .line 1690726
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "targeting"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690727
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->p:Ljava/lang/String;

    move-object v3, v4

    .line 1690728
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690729
    :cond_5
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "video_broadcast_update"

    .line 1690730
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1690731
    move-object v1, v1

    .line 1690732
    const-string v2, "POST"

    .line 1690733
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1690734
    move-object v1, v1

    .line 1690735
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.7/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1690736
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1690737
    move-object v1, v1

    .line 1690738
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1690739
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1690740
    move-object v1, v1

    .line 1690741
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1690742
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1690743
    move-object v0, v1

    .line 1690744
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1690745
    :cond_6
    iget-object v5, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1690746
    iget-object p0, v5, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v5, p0

    .line 1690747
    if-eqz v5, :cond_7

    iget-object v5, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1690748
    iget-object p0, v5, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v5, p0

    .line 1690749
    iget-object p0, v5, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v5, p0

    .line 1690750
    if-eqz v5, :cond_7

    .line 1690751
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1690752
    iget-object v5, v4, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v4, v5

    .line 1690753
    iget-object v5, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v5

    .line 1690754
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 1690755
    :cond_7
    iget-object v5, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1690756
    iget-object p0, v5, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    move-object v5, p0

    .line 1690757
    if-eqz v5, :cond_0

    .line 1690758
    iget-object v4, v3, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1690759
    iget-object v5, v4, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    move-object v4, v5

    .line 1690760
    iget-object v5, v4, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v5, v5, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1690761
    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1690668
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1690669
    const/4 v0, 0x0

    return-object v0
.end method
