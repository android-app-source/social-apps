.class public final LX/BT3;
.super LX/BT2;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/BT9;


# direct methods
.method public constructor <init>(LX/BT9;Z)V
    .locals 0

    .prologue
    .line 1786696
    iput-object p1, p0, LX/BT3;->b:LX/BT9;

    iput-boolean p2, p0, LX/BT3;->a:Z

    invoke-direct {p0}, LX/BT2;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1786697
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0}, LX/BTJ;->a()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1786698
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0, p1}, LX/BTL;->a(I)V

    .line 1786699
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    if-eqz v0, :cond_0

    .line 1786700
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    iget-object v1, p0, LX/BT3;->b:LX/BT9;

    invoke-static {v1}, LX/BT9;->D(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1786701
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1786702
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1786703
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0}, LX/BTJ;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1786704
    iget-boolean v0, p0, LX/BT3;->a:Z

    if-eqz v0, :cond_1

    .line 1786705
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    .line 1786706
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->q:LX/BTJ;

    .line 1786707
    iget v1, p0, LX/BT2;->a:I

    move v1, v1

    .line 1786708
    sub-int v1, p1, v1

    const/4 p1, 0x0

    .line 1786709
    iget-object v2, v0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v1

    if-ltz v2, :cond_0

    iget-object v2, v0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v1

    iget-object p0, v0, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result p0

    if-le v2, p0, :cond_2

    .line 1786710
    :cond_0
    :goto_0
    return-void

    .line 1786711
    :cond_1
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v0, p1}, LX/BTL;->a(I)V

    .line 1786712
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    if-eqz v0, :cond_0

    .line 1786713
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    iget-object v1, p0, LX/BT3;->b:LX/BT9;

    invoke-static {v1}, LX/BT9;->D(LX/BT9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1786714
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0

    .line 1786715
    :cond_2
    invoke-virtual {v0}, LX/BTJ;->a()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v0, v2, p1}, LX/BTJ;->a(IZ)V

    .line 1786716
    invoke-virtual {v0}, LX/BTJ;->b()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v0, v2, p1}, LX/BTJ;->b(IZ)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1786717
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    .line 1786718
    iget v1, v0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->e:I

    .line 1786719
    iget-boolean v0, p0, LX/BT3;->a:Z

    if-eqz v0, :cond_1

    .line 1786720
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    invoke-static {v0}, LX/BT9;->u$redex0(LX/BT9;)V

    .line 1786721
    :cond_0
    :goto_0
    return-void

    .line 1786722
    :cond_1
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    invoke-static {v0}, LX/BT9;->y(LX/BT9;)V

    .line 1786723
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    if-eqz v0, :cond_0

    .line 1786724
    iget-object v0, p0, LX/BT3;->b:LX/BT9;

    iget-object v0, v0, LX/BT9;->N:LX/BT8;

    invoke-virtual {v0}, LX/0ht;->l()V

    goto :goto_0
.end method
