.class public LX/AfJ;
.super LX/AVi;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements LX/AfI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;",
        ">;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Lcom/facebook/facecastdisplay/liveevent/PinnedCommentEventVodListener;",
        "Lcom/facebook/facecastdisplay/liveevent/comment/LiveCommentsMenuHelper$PinnedCommentWorker;",
        "LX/AfI;",
        "Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningSubscription$PinningSubscriptionListener;"
    }
.end annotation


# instance fields
.field private final a:LX/AfH;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AfO;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/Handler;

.field public d:LX/AeU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/Aev;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/AfO;

.field public h:F

.field public i:LX/Aeu;

.field public j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;


# direct methods
.method public constructor <init>(LX/AfH;LX/0Ot;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AfH;",
            "LX/0Ot",
            "<",
            "LX/AfO;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698934
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1698935
    iput-object p1, p0, LX/AfJ;->a:LX/AfH;

    .line 1698936
    iput-object p2, p0, LX/AfJ;->b:LX/0Ot;

    .line 1698937
    iput-object p3, p0, LX/AfJ;->c:Landroid/os/Handler;

    .line 1698938
    return-void
.end method

.method public static a(LX/AfJ;LX/Aeu;F)V
    .locals 6

    .prologue
    .line 1699047
    iget-object v0, p0, LX/AfJ;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1699048
    iget-object v0, p0, LX/AfJ;->a:LX/AfH;

    iget-object v1, p0, LX/AfJ;->f:Ljava/lang/String;

    float-to-double v2, p2

    .line 1699049
    new-instance v4, LX/6SH;

    invoke-direct {v4}, LX/6SH;-><init>()V

    move-object v4, v4

    .line 1699050
    new-instance v5, LX/4ID;

    invoke-direct {v5}, LX/4ID;-><init>()V

    .line 1699051
    iget-object p0, v4, LX/0gW;->h:Ljava/lang/String;

    move-object p0, p0

    .line 1699052
    const-string p2, "client_mutation_id"

    invoke-virtual {v5, p2, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699053
    move-object v5, v5

    .line 1699054
    iget-object p0, v0, LX/AfH;->b:Ljava/lang/String;

    .line 1699055
    const-string p2, "actor_id"

    invoke-virtual {v5, p2, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699056
    move-object v5, v5

    .line 1699057
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    .line 1699058
    const-string p2, "video_time_offset"

    invoke-virtual {v5, p2, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1699059
    move-object v5, v5

    .line 1699060
    const-string p0, "video_id"

    invoke-virtual {v5, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699061
    move-object v5, v5

    .line 1699062
    iget-object p0, p1, LX/Aeu;->f:Ljava/lang/String;

    .line 1699063
    const-string p2, "comment_id"

    invoke-virtual {v5, p2, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699064
    move-object v5, v5

    .line 1699065
    const-string p0, "input"

    invoke-virtual {v4, p0, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1699066
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1699067
    iget-object v5, v0, LX/AfH;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1699068
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/AfJ;Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;LX/Aeu;)V
    .locals 1

    .prologue
    .line 1699040
    iput-object p2, p0, LX/AfJ;->i:LX/Aeu;

    .line 1699041
    iget-boolean v0, p1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->s:Z

    move v0, v0

    .line 1699042
    if-nez v0, :cond_0

    .line 1699043
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->b()V

    .line 1699044
    :cond_0
    iget-object v0, p0, LX/AfJ;->d:LX/AeU;

    invoke-virtual {p1, p2, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a(LX/Aeu;LX/AeU;)V

    .line 1699045
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->setVisibility(I)V

    .line 1699046
    return-void
.end method

.method public static j(LX/AfJ;)Z
    .locals 1

    .prologue
    .line 1699039
    iget-object v0, p0, LX/AfJ;->d:LX/AeU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfJ;->d:LX/AeU;

    iget-boolean v0, v0, LX/AeU;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1699030
    check-cast p1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    check-cast p2, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    .line 1699031
    const/4 v0, 0x0

    .line 1699032
    iput-object v0, p2, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->r:LX/AfI;

    .line 1699033
    iput-object p0, p1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->r:LX/AfI;

    .line 1699034
    iget-object v0, p2, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->t:LX/Aeu;

    move-object v0, v0

    .line 1699035
    if-eqz v0, :cond_0

    .line 1699036
    iget-object v0, p2, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->t:LX/Aeu;

    move-object v0, v0

    .line 1699037
    invoke-static {p0, p1, v0}, LX/AfJ;->a$redex0(LX/AfJ;Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;LX/Aeu;)V

    .line 1699038
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1699025
    iput-object p1, p0, LX/AfJ;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    .line 1699026
    if-nez p1, :cond_0

    .line 1699027
    invoke-virtual {p0, v0}, LX/AfJ;->c(LX/Aeu;)V

    .line 1699028
    :goto_0
    return-void

    .line 1699029
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/AfJ;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/Aeu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, LX/AfJ;->c(LX/Aeu;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1699022
    check-cast p1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    .line 1699023
    iput-object p0, p1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->r:LX/AfI;

    .line 1699024
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1699069
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1699070
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    const/4 v1, 0x0

    .line 1699071
    iput-object v1, v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->r:LX/AfI;

    .line 1699072
    return-void
.end method

.method public final c(LX/Aeu;)V
    .locals 3

    .prologue
    .line 1699020
    iget-object v0, p0, LX/AfJ;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningController$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningController$1;-><init>(LX/AfJ;LX/Aeu;)V

    const v2, 0x6c1b6e0c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1699021
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1698997
    invoke-static {p0}, LX/AfJ;->j(LX/AfJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698998
    iget-object v0, p0, LX/AfJ;->g:LX/AfO;

    if-nez v0, :cond_0

    .line 1698999
    iget-object v0, p0, LX/AfJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AfO;

    iput-object v0, p0, LX/AfJ;->g:LX/AfO;

    .line 1699000
    :cond_0
    iget-object v0, p0, LX/AfJ;->g:LX/AfO;

    move-object v0, v0

    .line 1699001
    iget-object v1, p0, LX/AfJ;->f:Ljava/lang/String;

    .line 1699002
    iget-boolean v2, v0, LX/AfO;->j:Z

    if-eqz v2, :cond_2

    .line 1699003
    :cond_1
    :goto_0
    return-void

    .line 1699004
    :cond_2
    iget-object v2, v0, LX/AfO;->c:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1699005
    iput-object v1, v0, LX/AfO;->h:Ljava/lang/String;

    .line 1699006
    iput-object p0, v0, LX/AfO;->i:LX/AfJ;

    .line 1699007
    iget-object v2, v0, LX/AfO;->h:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/AfO;->i:LX/AfJ;

    if-eqz v2, :cond_1

    .line 1699008
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/AfO;->j:Z

    .line 1699009
    new-instance v2, LX/6SF;

    invoke-direct {v2}, LX/6SF;-><init>()V

    move-object v2, v2

    .line 1699010
    const-string p0, "targetID"

    iget-object v1, v0, LX/AfO;->h:Ljava/lang/String;

    invoke-virtual {v2, p0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1699011
    iget-object p0, v0, LX/AfO;->f:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1699012
    new-instance p0, LX/AfM;

    invoke-direct {p0, v0}, LX/AfM;-><init>(LX/AfO;)V

    iget-object v1, v0, LX/AfO;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, p0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1699013
    new-instance v2, LX/6SI;

    invoke-direct {v2}, LX/6SI;-><init>()V

    move-object v2, v2

    .line 1699014
    new-instance v3, LX/4IE;

    invoke-direct {v3}, LX/4IE;-><init>()V

    iget-object p0, v0, LX/AfO;->h:Ljava/lang/String;

    .line 1699015
    const-string v1, "video_id"

    invoke-virtual {v3, v1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699016
    move-object v3, v3

    .line 1699017
    const-string p0, "input"

    invoke-virtual {v2, p0, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1699018
    :try_start_0
    iget-object v3, v0, LX/AfO;->b:LX/0gX;

    new-instance p0, LX/AfN;

    invoke-direct {p0, v0}, LX/AfN;-><init>(LX/AfO;)V

    invoke-virtual {v3, v2, p0}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v2

    iput-object v2, v0, LX/AfO;->g:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 1699019
    :goto_1
    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1698993
    iget-object v0, p0, LX/AfJ;->i:LX/Aeu;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfJ;->d:LX/AeU;

    if-eqz v0, :cond_0

    .line 1698994
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698995
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    iget-object v1, p0, LX/AfJ;->i:LX/Aeu;

    iget-object v2, p0, LX/AfJ;->d:LX/AeU;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a(LX/Aeu;LX/AeU;)V

    .line 1698996
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1698987
    iget-object v1, p0, LX/AfJ;->e:LX/Aev;

    .line 1698988
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698989
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    .line 1698990
    iget-object p0, v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->t:LX/Aeu;

    move-object v0, p0

    .line 1698991
    invoke-virtual {v1, v0}, LX/Aev;->a(LX/Aeu;)V

    .line 1698992
    return-void
.end method

.method public final h()V
    .locals 8

    .prologue
    .line 1698957
    invoke-static {p0}, LX/AfJ;->j(LX/AfJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698958
    iget-object v0, p0, LX/AfJ;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1698959
    const/4 v0, 0x0

    iput-object v0, p0, LX/AfJ;->i:LX/Aeu;

    .line 1698960
    iget-object v0, p0, LX/AfJ;->a:LX/AfH;

    iget-object v1, p0, LX/AfJ;->f:Ljava/lang/String;

    iget v2, p0, LX/AfJ;->h:F

    float-to-double v2, v2

    .line 1698961
    new-instance v4, LX/6SJ;

    invoke-direct {v4}, LX/6SJ;-><init>()V

    move-object v4, v4

    .line 1698962
    new-instance v5, LX/4Jy;

    invoke-direct {v5}, LX/4Jy;-><init>()V

    .line 1698963
    iget-object v6, v4, LX/0gW;->h:Ljava/lang/String;

    move-object v6, v6

    .line 1698964
    const-string v7, "client_mutation_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698965
    move-object v5, v5

    .line 1698966
    iget-object v6, v0, LX/AfH;->b:Ljava/lang/String;

    .line 1698967
    const-string v7, "actor_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698968
    move-object v5, v5

    .line 1698969
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    .line 1698970
    const-string v7, "video_time_offset"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1698971
    move-object v5, v5

    .line 1698972
    const-string v6, "video_id"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698973
    move-object v5, v5

    .line 1698974
    const-string v6, "input"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1698975
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1698976
    iget-object v5, v0, LX/AfH;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698977
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698978
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->setVisibility(I)V

    .line 1698979
    :cond_0
    :goto_0
    return-void

    .line 1698980
    :cond_1
    iget-object v0, p0, LX/AfJ;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    if-eqz v0, :cond_2

    .line 1698981
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698982
    if-nez v0, :cond_3

    .line 1698983
    :cond_2
    :goto_1
    goto :goto_0

    .line 1698984
    :cond_3
    new-instance v1, LX/0ju;

    .line 1698985
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698986
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080c48

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080c49

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080c4b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080c4a

    invoke-virtual {v0, v1, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1698939
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1698940
    iget-object v0, p0, LX/AfJ;->a:LX/AfH;

    iget-object v1, p0, LX/AfJ;->j:Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$PinnedCommentsFragmentModel$PinnedCommentEventsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1698941
    new-instance v2, LX/6S6;

    invoke-direct {v2}, LX/6S6;-><init>()V

    move-object v2, v2

    .line 1698942
    new-instance v3, LX/4E1;

    invoke-direct {v3}, LX/4E1;-><init>()V

    .line 1698943
    iget-object p1, v2, LX/0gW;->h:Ljava/lang/String;

    move-object p1, p1

    .line 1698944
    const-string p2, "client_mutation_id"

    invoke-virtual {v3, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698945
    move-object v3, v3

    .line 1698946
    iget-object p1, v0, LX/AfH;->b:Ljava/lang/String;

    .line 1698947
    const-string p2, "actor_id"

    invoke-virtual {v3, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698948
    move-object v3, v3

    .line 1698949
    const-string p1, "pin_event_id"

    invoke-virtual {v3, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698950
    move-object v3, v3

    .line 1698951
    const-string p1, "input"

    invoke-virtual {v2, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1698952
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1698953
    iget-object v3, v0, LX/AfH;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698954
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1698955
    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->setVisibility(I)V

    .line 1698956
    :cond_0
    return-void
.end method
