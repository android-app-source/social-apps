.class public LX/CSP;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CSP;


# instance fields
.field public final a:LX/17Y;


# direct methods
.method public constructor <init>(LX/17Y;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1894534
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1894535
    iput-object p1, p0, LX/CSP;->a:LX/17Y;

    .line 1894536
    const-string v0, "platform_first_party?id={%s}&cta_id={%s}&referrer={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "id unknown"

    const-string v2, "cta_id unknown"

    const-string v3, "referrer unknown"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/CSO;

    invoke-direct {v1, p0}, LX/CSO;-><init>(LX/CSP;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1894537
    return-void
.end method

.method public static a(LX/0QB;)LX/CSP;
    .locals 4

    .prologue
    .line 1894538
    sget-object v0, LX/CSP;->b:LX/CSP;

    if-nez v0, :cond_1

    .line 1894539
    const-class v1, LX/CSP;

    monitor-enter v1

    .line 1894540
    :try_start_0
    sget-object v0, LX/CSP;->b:LX/CSP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1894541
    if-eqz v2, :cond_0

    .line 1894542
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1894543
    new-instance p0, LX/CSP;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-direct {p0, v3}, LX/CSP;-><init>(LX/17Y;)V

    .line 1894544
    move-object v0, p0

    .line 1894545
    sput-object v0, LX/CSP;->b:LX/CSP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1894546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1894547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1894548
    :cond_1
    sget-object v0, LX/CSP;->b:LX/CSP;

    return-object v0

    .line 1894549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1894550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
