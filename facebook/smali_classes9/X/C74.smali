.class public LX/C74;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C75;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C74",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C75;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850814
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850815
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C74;->b:LX/0Zi;

    .line 1850816
    iput-object p1, p0, LX/C74;->a:LX/0Ot;

    .line 1850817
    return-void
.end method

.method public static a(LX/0QB;)LX/C74;
    .locals 4

    .prologue
    .line 1850818
    const-class v1, LX/C74;

    monitor-enter v1

    .line 1850819
    :try_start_0
    sget-object v0, LX/C74;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850820
    sput-object v2, LX/C74;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850821
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850822
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850823
    new-instance v3, LX/C74;

    const/16 p0, 0x1f84

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C74;-><init>(LX/0Ot;)V

    .line 1850824
    move-object v0, v3

    .line 1850825
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850826
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C74;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850827
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1850829
    const v0, 0xc4fccff

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1850830
    check-cast p2, LX/C73;

    .line 1850831
    iget-object v0, p0, LX/C74;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/C73;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x1

    const/4 p0, 0x2

    .line 1850832
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0048

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    .line 1850833
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1850834
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/1ne;->i(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a043a

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v3, 0x7f0b0918

    invoke-interface {v1, p2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x3

    const p0, 0x7f0b0918

    invoke-interface {v1, v3, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    .line 1850835
    const v3, 0xc4fccff

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1850836
    invoke-interface {v1, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1850837
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1850838
    invoke-static {}, LX/1dS;->b()V

    .line 1850839
    iget v0, p1, LX/1dQ;->b:I

    .line 1850840
    packed-switch v0, :pswitch_data_0

    .line 1850841
    :goto_0
    return-object v2

    .line 1850842
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1850843
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1850844
    check-cast v1, LX/C73;

    .line 1850845
    iget-object p1, p0, LX/C74;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C75;

    iget-object p2, v1, LX/C73;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850846
    iget-object p0, p1, LX/C75;->a:LX/3Do;

    invoke-virtual {p0, p2}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/2tm;->onClick(Landroid/view/View;)V

    .line 1850847
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xc4fccff
        :pswitch_0
    .end packed-switch
.end method
