.class public final LX/BaU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/audience/model/UploadShot;

.field public final synthetic b:LX/BaH;

.field public final synthetic c:LX/BaV;


# direct methods
.method public constructor <init>(LX/BaV;Lcom/facebook/audience/model/UploadShot;LX/BaH;)V
    .locals 0

    .prologue
    .line 1799160
    iput-object p1, p0, LX/BaU;->c:LX/BaV;

    iput-object p2, p0, LX/BaU;->a:Lcom/facebook/audience/model/UploadShot;

    iput-object p3, p0, LX/BaU;->b:LX/BaH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1799161
    sget-object v0, LX/BaV;->a:Ljava/lang/String;

    const-string v1, "Error occured while trying to upload a video via BlueService"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1799162
    iget-object v0, p0, LX/BaU;->b:LX/BaH;

    invoke-virtual {v0, p1}, LX/BaH;->a(Ljava/lang/Throwable;)V

    .line 1799163
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799149
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1799150
    :try_start_0
    const-string v0, "fbids"

    invoke-virtual {p1, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1799151
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 1799152
    sget-object v0, LX/BaV;->a:Ljava/lang/String;

    const-string v1, "no fbid returned as part of upload flow"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1799153
    :goto_0
    return-void

    .line 1799154
    :cond_0
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1799155
    iget-object v1, p0, LX/BaU;->c:LX/BaV;

    iget-object v2, p0, LX/BaU;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v1, v0, v2}, LX/BaV;->a$redex0(LX/BaV;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)LX/0Px;

    move-result-object v0

    .line 1799156
    iget-object v1, p0, LX/BaU;->b:LX/BaH;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/BaH;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1799157
    :catch_0
    move-exception v0

    .line 1799158
    sget-object v1, LX/BaV;->a:Ljava/lang/String;

    const-string v2, "Error in parsing for fbid post upload"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1799159
    iget-object v1, p0, LX/BaU;->b:LX/BaH;

    invoke-virtual {v1, v0}, LX/BaH;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
