.class public final LX/CaE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;)V
    .locals 0

    .prologue
    .line 1917163
    iput-object p1, p0, LX/CaE;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x2f782ecb

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1917164
    iget-object v1, p0, LX/CaE;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v1, v1, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->l:LX/CES;

    if-eqz v1, :cond_0

    .line 1917165
    iget-object v1, p0, LX/CaE;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v1, v1, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->l:LX/CES;

    .line 1917166
    iget-object v3, v1, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v3, :cond_1

    .line 1917167
    :cond_0
    :goto_0
    const v1, -0x4c521f46

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1917168
    :cond_1
    iget-object v3, v1, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1917169
    iget-object v3, v1, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object p0, LX/21D;->SOUVENIR:LX/21D;

    const-string p1, "shareFromSouvenir"

    invoke-interface {v3, v4, p0, p1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    .line 1917170
    const-string v3, "newsfeed_ufi"

    invoke-virtual {p0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v4, "ANDROID_COMPOSER"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1917171
    iget-object v3, v1, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Landroid/app/Activity;

    invoke-static {v3, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 1917172
    if-eqz v3, :cond_0

    .line 1917173
    iget-object v4, v1, LX/CES;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v4, v4, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->w:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/89P;

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p0

    const/16 p1, 0x6dc

    invoke-virtual {v4, p0, p1, v3}, LX/89P;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method
