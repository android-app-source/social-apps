.class public final LX/CdP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V
    .locals 0

    .prologue
    .line 1922442
    iput-object p1, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1922443
    iget-object v0, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v0, v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1922444
    iget-object v0, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v0, v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08293f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1922445
    iget-object v0, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-static {v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->e(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V

    .line 1922446
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1922447
    iget-object v0, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v0, v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1922448
    iget-object v0, p0, LX/CdP;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v0, v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082940

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1922449
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1922450
    invoke-direct {p0}, LX/CdP;->a()V

    return-void
.end method
