.class public final LX/AaS;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V
    .locals 0

    .prologue
    .line 1688532
    iput-object p1, p0, LX/AaS;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1688533
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1688534
    const-string v1, "https://m.facebook.com/legal/tips"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1688535
    iget-object v1, p0, LX/AaS;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/AaS;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1688536
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1688537
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1688538
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1688539
    return-void
.end method
