.class public final LX/CU5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/CTu;

.field public final c:I

.field public final d:LX/CU0;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1896235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896236
    invoke-virtual {p1, p2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896237
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v0

    const v3, -0x2897e73b

    invoke-static {p1, v0, v3}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1896238
    invoke-virtual {p1, p2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU5;->a:Ljava/lang/String;

    .line 1896239
    new-instance v0, LX/CTu;

    invoke-direct {v0, p1, p2}, LX/CTu;-><init>(LX/15i;I)V

    iput-object v0, p0, LX/CU5;->b:LX/CTu;

    .line 1896240
    invoke-virtual {p1, p2, v2}, LX/15i;->j(II)I

    move-result v0

    iput v0, p0, LX/CU5;->c:I

    .line 1896241
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, LX/CU0;

    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-virtual {p1, p2, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-static {v0}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;)LX/CUx;

    move-result-object v0

    invoke-direct {v2, v0}, LX/CU0;-><init>(LX/CUx;)V

    move-object v0, v2

    :goto_1
    iput-object v0, p0, LX/CU5;->d:LX/CU0;

    .line 1896242
    return-void

    :cond_0
    move v0, v2

    .line 1896243
    goto :goto_0

    .line 1896244
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
