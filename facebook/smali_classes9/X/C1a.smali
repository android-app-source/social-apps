.class public LX/C1a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/1nP;

.field private c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;LX/1nP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842533
    iput-object p1, p0, LX/C1a;->a:LX/0ad;

    .line 1842534
    iput-object p2, p0, LX/C1a;->b:LX/1nP;

    .line 1842535
    return-void
.end method

.method public static a(LX/0QB;)LX/C1a;
    .locals 5

    .prologue
    .line 1842536
    const-class v1, LX/C1a;

    monitor-enter v1

    .line 1842537
    :try_start_0
    sget-object v0, LX/C1a;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842538
    sput-object v2, LX/C1a;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842541
    new-instance p0, LX/C1a;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/1nP;->a(LX/0QB;)LX/1nP;

    move-result-object v4

    check-cast v4, LX/1nP;

    invoke-direct {p0, v3, v4}, LX/C1a;-><init>(LX/0ad;LX/1nP;)V

    .line 1842542
    move-object v0, p0

    .line 1842543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1842547
    iget-object v0, p0, LX/C1a;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1842548
    iget-object v0, p0, LX/C1a;->a:LX/0ad;

    sget-short v1, LX/C1Z;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/C1a;->c:Ljava/lang/Boolean;

    .line 1842549
    :cond_0
    iget-object v0, p0, LX/C1a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
