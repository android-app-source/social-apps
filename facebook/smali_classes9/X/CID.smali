.class public final LX/CID;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1873105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1873106
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1873107
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1873108
    invoke-static {p0, p1}, LX/CID;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1873109
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1873110
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1873111
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1873112
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1873113
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/CID;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1873115
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1873116
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 38

    .prologue
    .line 1872969
    const/16 v34, 0x0

    .line 1872970
    const/16 v33, 0x0

    .line 1872971
    const/16 v32, 0x0

    .line 1872972
    const/16 v31, 0x0

    .line 1872973
    const/16 v30, 0x0

    .line 1872974
    const/16 v29, 0x0

    .line 1872975
    const/16 v28, 0x0

    .line 1872976
    const/16 v27, 0x0

    .line 1872977
    const/16 v26, 0x0

    .line 1872978
    const/16 v25, 0x0

    .line 1872979
    const/16 v24, 0x0

    .line 1872980
    const/16 v23, 0x0

    .line 1872981
    const/16 v22, 0x0

    .line 1872982
    const/16 v21, 0x0

    .line 1872983
    const/16 v20, 0x0

    .line 1872984
    const/16 v19, 0x0

    .line 1872985
    const/16 v18, 0x0

    .line 1872986
    const/16 v17, 0x0

    .line 1872987
    const/16 v16, 0x0

    .line 1872988
    const/4 v15, 0x0

    .line 1872989
    const/4 v14, 0x0

    .line 1872990
    const/4 v13, 0x0

    .line 1872991
    const/4 v12, 0x0

    .line 1872992
    const/4 v11, 0x0

    .line 1872993
    const/4 v10, 0x0

    .line 1872994
    const/4 v9, 0x0

    .line 1872995
    const/4 v8, 0x0

    .line 1872996
    const/4 v7, 0x0

    .line 1872997
    const/4 v6, 0x0

    .line 1872998
    const/4 v5, 0x0

    .line 1872999
    const/4 v4, 0x0

    .line 1873000
    const/4 v3, 0x0

    .line 1873001
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    .line 1873002
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873003
    const/4 v3, 0x0

    .line 1873004
    :goto_0
    return v3

    .line 1873005
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873006
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1f

    .line 1873007
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v35

    .line 1873008
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1873009
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_1

    if-eqz v35, :cond_1

    .line 1873010
    const-string v36, "__type__"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-nez v36, :cond_2

    const-string v36, "__typename"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 1873011
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v34

    goto :goto_1

    .line 1873012
    :cond_3
    const-string v36, "action"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 1873013
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v33

    goto :goto_1

    .line 1873014
    :cond_4
    const-string v36, "annotations"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 1873015
    invoke-static/range {p0 .. p1}, LX/CI0;->a(LX/15w;LX/186;)I

    move-result v32

    goto :goto_1

    .line 1873016
    :cond_5
    const-string v36, "autoplay_style"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 1873017
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v31

    goto :goto_1

    .line 1873018
    :cond_6
    const-string v36, "block_elements"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_7

    .line 1873019
    invoke-static/range {p0 .. p1}, LX/CI2;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 1873020
    :cond_7
    const-string v36, "child_elements"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 1873021
    invoke-static/range {p0 .. p1}, LX/CIG;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 1873022
    :cond_8
    const-string v36, "color"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_9

    .line 1873023
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 1873024
    :cond_9
    const-string v36, "color_types"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 1873025
    invoke-static/range {p0 .. p1}, LX/CI1;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 1873026
    :cond_a
    const-string v36, "content_element"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 1873027
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1873028
    :cond_b
    const-string v36, "control_style"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 1873029
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    goto/16 :goto_1

    .line 1873030
    :cond_c
    const-string v36, "do_action"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_d

    .line 1873031
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1873032
    :cond_d
    const-string v36, "document_element_type"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 1873033
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    goto/16 :goto_1

    .line 1873034
    :cond_e
    const-string v36, "element_descriptor"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_f

    .line 1873035
    invoke-static/range {p0 .. p1}, LX/CI9;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1873036
    :cond_f
    const-string v36, "element_photo"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 1873037
    invoke-static/range {p0 .. p1}, LX/CIF;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1873038
    :cond_10
    const-string v36, "element_text"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 1873039
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1873040
    :cond_11
    const-string v36, "element_video"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 1873041
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1873042
    :cond_12
    const-string v36, "grid_width_percent"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 1873043
    const/4 v5, 0x1

    .line 1873044
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 1873045
    :cond_13
    const-string v36, "image"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 1873046
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1873047
    :cond_14
    const-string v36, "is_on"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_15

    .line 1873048
    const/4 v4, 0x1

    .line 1873049
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 1873050
    :cond_15
    const-string v36, "logging_token"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_16

    .line 1873051
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1873052
    :cond_16
    const-string v36, "looping_style"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_17

    .line 1873053
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    goto/16 :goto_1

    .line 1873054
    :cond_17
    const-string v36, "off_text"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_18

    .line 1873055
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1873056
    :cond_18
    const-string v36, "on_text"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_19

    .line 1873057
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1873058
    :cond_19
    const-string v36, "section_header"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1a

    .line 1873059
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1873060
    :cond_1a
    const-string v36, "selected_index"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1b

    .line 1873061
    const/4 v3, 0x1

    .line 1873062
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1873063
    :cond_1b
    const-string v36, "style_list"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1c

    .line 1873064
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1873065
    :cond_1c
    const-string v36, "target_uri"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1d

    .line 1873066
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1873067
    :cond_1d
    const-string v36, "touch_targets"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1e

    .line 1873068
    invoke-static/range {p0 .. p1}, LX/CIM;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1873069
    :cond_1e
    const-string v36, "undo_action"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_0

    .line 1873070
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1873071
    :cond_1f
    const/16 v35, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1873072
    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873073
    const/16 v34, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873074
    const/16 v33, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873075
    const/16 v32, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873076
    const/16 v31, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873077
    const/16 v30, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873078
    const/16 v29, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873079
    const/16 v28, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873080
    const/16 v27, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873081
    const/16 v26, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873082
    const/16 v25, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873083
    const/16 v24, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873084
    const/16 v23, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873085
    const/16 v22, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873086
    const/16 v21, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873087
    const/16 v20, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873088
    if-eqz v5, :cond_20

    .line 1873089
    const/16 v5, 0x10

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v5, v1, v2}, LX/186;->a(III)V

    .line 1873090
    :cond_20
    const/16 v5, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1873091
    if-eqz v4, :cond_21

    .line 1873092
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1873093
    :cond_21
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1873094
    const/16 v4, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1873095
    const/16 v4, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1873096
    const/16 v4, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1873097
    const/16 v4, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1873098
    if-eqz v3, :cond_22

    .line 1873099
    const/16 v3, 0x18

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v4}, LX/186;->a(III)V

    .line 1873100
    :cond_22
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1873101
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1873102
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1873103
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1873104
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/16 v5, 0xb

    const/16 v4, 0x9

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 1872850
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1872851
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1872852
    if-eqz v0, :cond_0

    .line 1872853
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872854
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1872855
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872856
    if-eqz v0, :cond_1

    .line 1872857
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872858
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872859
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872860
    if-eqz v0, :cond_2

    .line 1872861
    const-string v1, "annotations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872862
    invoke-static {p0, v0, p2, p3}, LX/CI0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872863
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1872864
    if-eqz v0, :cond_3

    .line 1872865
    const-string v0, "autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872866
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872867
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872868
    if-eqz v0, :cond_4

    .line 1872869
    const-string v1, "block_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872870
    invoke-static {p0, v0, p2, p3}, LX/CI2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872871
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872872
    if-eqz v0, :cond_5

    .line 1872873
    const-string v1, "child_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872874
    invoke-static {p0, v0, p2, p3}, LX/CIG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872875
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872876
    if-eqz v0, :cond_6

    .line 1872877
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872878
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872879
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872880
    if-eqz v0, :cond_7

    .line 1872881
    const-string v1, "color_types"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872882
    invoke-static {p0, v0, p2, p3}, LX/CI1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872883
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872884
    if-eqz v0, :cond_8

    .line 1872885
    const-string v1, "content_element"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872886
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872887
    :cond_8
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1872888
    if-eqz v0, :cond_9

    .line 1872889
    const-string v0, "control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872890
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872891
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872892
    if-eqz v0, :cond_a

    .line 1872893
    const-string v1, "do_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872894
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872895
    :cond_a
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1872896
    if-eqz v0, :cond_b

    .line 1872897
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872898
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872899
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872900
    if-eqz v0, :cond_c

    .line 1872901
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872902
    invoke-static {p0, v0, p2, p3}, LX/CI9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872903
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872904
    if-eqz v0, :cond_d

    .line 1872905
    const-string v1, "element_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872906
    invoke-static {p0, v0, p2}, LX/CIF;->a(LX/15i;ILX/0nX;)V

    .line 1872907
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872908
    if-eqz v0, :cond_e

    .line 1872909
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872910
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872911
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872912
    if-eqz v0, :cond_f

    .line 1872913
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872914
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872915
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1872916
    if-eqz v0, :cond_10

    .line 1872917
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872918
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1872919
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872920
    if-eqz v0, :cond_11

    .line 1872921
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872922
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1872923
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1872924
    if-eqz v0, :cond_12

    .line 1872925
    const-string v1, "is_on"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872926
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1872927
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872928
    if-eqz v0, :cond_13

    .line 1872929
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872930
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872931
    :cond_13
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1872932
    if-eqz v0, :cond_14

    .line 1872933
    const-string v0, "looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872934
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872935
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872936
    if-eqz v0, :cond_15

    .line 1872937
    const-string v1, "off_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872938
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872939
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872940
    if-eqz v0, :cond_16

    .line 1872941
    const-string v1, "on_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872942
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872943
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872944
    if-eqz v0, :cond_17

    .line 1872945
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872946
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872947
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1872948
    if-eqz v0, :cond_18

    .line 1872949
    const-string v1, "selected_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872950
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1872951
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872952
    if-eqz v0, :cond_19

    .line 1872953
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872954
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1872955
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872956
    if-eqz v0, :cond_1a

    .line 1872957
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872958
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872959
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872960
    if-eqz v0, :cond_1b

    .line 1872961
    const-string v1, "touch_targets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872962
    invoke-static {p0, v0, p2, p3}, LX/CIM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872963
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872964
    if-eqz v0, :cond_1c

    .line 1872965
    const-string v1, "undo_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872966
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872967
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1872968
    return-void
.end method
