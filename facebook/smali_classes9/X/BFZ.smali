.class public LX/BFZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:I

.field private static volatile l:LX/BFZ;


# instance fields
.field public a:Landroid/widget/ImageButton;

.field public b:Landroid/content/res/Resources;

.field public c:LX/89b;

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Landroid/graphics/drawable/LayerDrawable;

.field public k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765976
    return-void
.end method

.method public static a(LX/0QB;)LX/BFZ;
    .locals 3

    .prologue
    .line 1765977
    sget-object v0, LX/BFZ;->l:LX/BFZ;

    if-nez v0, :cond_1

    .line 1765978
    const-class v1, LX/BFZ;

    monitor-enter v1

    .line 1765979
    :try_start_0
    sget-object v0, LX/BFZ;->l:LX/BFZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1765980
    if-eqz v2, :cond_0

    .line 1765981
    :try_start_1
    new-instance v0, LX/BFZ;

    invoke-direct {v0}, LX/BFZ;-><init>()V

    .line 1765982
    move-object v0, v0

    .line 1765983
    sput-object v0, LX/BFZ;->l:LX/BFZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1765984
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1765985
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1765986
    :cond_1
    sget-object v0, LX/BFZ;->l:LX/BFZ;

    return-object v0

    .line 1765987
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1765988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/89b;)V
    .locals 3

    .prologue
    .line 1765989
    iput-object p1, p0, LX/BFZ;->c:LX/89b;

    .line 1765990
    sget-object v0, LX/89b;->IMAGE:LX/89b;

    if-ne p1, v0, :cond_0

    .line 1765991
    iget-object v0, p0, LX/BFZ;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/BFZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f0a04ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1765992
    iget-object v0, p0, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1765993
    :goto_0
    return-void

    .line 1765994
    :cond_0
    iget-object v0, p0, LX/BFZ;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/BFZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f0a04f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method
