.class public final LX/AgR;
.super LX/AgL;
.source ""


# instance fields
.field public final a:LX/1aX;

.field public final b:LX/AgN;

.field public final c:Landroid/animation/AnimatorSet;

.field public final d:LX/1Ai;

.field public e:Z

.field public f:LX/AgS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final synthetic g:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/1aX;LX/AgN;Landroid/animation/AnimatorSet;)V
    .locals 1

    .prologue
    .line 1700748
    iput-object p1, p0, LX/AgR;->g:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-direct {p0}, LX/AgL;-><init>()V

    .line 1700749
    iput-object p2, p0, LX/AgR;->a:LX/1aX;

    .line 1700750
    iput-object p3, p0, LX/AgR;->b:LX/AgN;

    .line 1700751
    iput-object p4, p0, LX/AgR;->c:Landroid/animation/AnimatorSet;

    .line 1700752
    new-instance v0, LX/AgQ;

    invoke-direct {v0, p0, p1}, LX/AgQ;-><init>(LX/AgR;Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;)V

    iput-object v0, p0, LX/AgR;->d:LX/1Ai;

    .line 1700753
    invoke-virtual {p0}, LX/AgL;->ik_()V

    .line 1700754
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1700747
    iget-object v0, p0, LX/AgR;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1700755
    iget-boolean v0, p0, LX/AgR;->e:Z

    if-eqz v0, :cond_0

    .line 1700756
    iget-object v0, p0, LX/AgR;->f:LX/AgS;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AgR;->f:LX/AgS;

    int-to-float v1, p1

    invoke-interface {v0, v1}, LX/AgS;->a(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1700757
    iget-object v0, p0, LX/AgR;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1700758
    iget-object v0, p0, LX/AgR;->g:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    .line 1700759
    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a$redex0(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/AgR;)V

    .line 1700760
    :cond_0
    return-void
.end method

.method public final a(LX/AgS;)V
    .locals 0
    .param p1    # LX/AgS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700745
    iput-object p1, p0, LX/AgR;->f:LX/AgS;

    .line 1700746
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1700742
    invoke-virtual {p0}, LX/AgL;->ik_()V

    .line 1700743
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AgR;->e:Z

    .line 1700744
    return-void
.end method
