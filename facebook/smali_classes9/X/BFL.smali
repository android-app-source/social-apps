.class public final enum LX/BFL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BFL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BFL;

.field public static final enum PRIVACY_EDUCATION:LX/BFL;

.field public static final enum UNKNOWN:LX/BFL;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1765602
    new-instance v0, LX/BFL;

    const-string v1, "PRIVACY_EDUCATION"

    invoke-direct {v0, v1, v2}, LX/BFL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BFL;->PRIVACY_EDUCATION:LX/BFL;

    .line 1765603
    new-instance v0, LX/BFL;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/BFL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BFL;->UNKNOWN:LX/BFL;

    .line 1765604
    const/4 v0, 0x2

    new-array v0, v0, [LX/BFL;

    sget-object v1, LX/BFL;->PRIVACY_EDUCATION:LX/BFL;

    aput-object v1, v0, v2

    sget-object v1, LX/BFL;->UNKNOWN:LX/BFL;

    aput-object v1, v0, v3

    sput-object v0, LX/BFL;->$VALUES:[LX/BFL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1765605
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BFL;
    .locals 1

    .prologue
    .line 1765606
    const-class v0, LX/BFL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BFL;

    return-object v0
.end method

.method public static values()[LX/BFL;
    .locals 1

    .prologue
    .line 1765607
    sget-object v0, LX/BFL;->$VALUES:[LX/BFL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BFL;

    return-object v0
.end method
