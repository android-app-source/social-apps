.class public final LX/BGW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BGX;


# direct methods
.method public constructor <init>(LX/BGX;)V
    .locals 0

    .prologue
    .line 1767149
    iput-object p1, p0, LX/BGW;->a:LX/BGX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767150
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1767151
    invoke-static {p1}, LX/1kR;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v2

    .line 1767152
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1767153
    if-eqz v2, :cond_1

    .line 1767154
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    .line 1767155
    iget-object v5, p0, LX/BGW;->a:LX/BGX;

    iget-object v5, v5, LX/BGX;->c:LX/0fO;

    invoke-virtual {v5}, LX/0fO;->a()Z

    move-result v5

    invoke-static {v0, v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    .line 1767156
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/BGW;->a:LX/BGX;

    iget-object v5, v5, LX/BGX;->a:LX/1kR;

    invoke-virtual {v5, v0}, LX/1kR;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1767157
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1767158
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1767159
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
