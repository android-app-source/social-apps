.class public LX/CBf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CBd;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextHScrollItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1856689
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CBf;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextHScrollItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856690
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1856691
    iput-object p1, p0, LX/CBf;->b:LX/0Ot;

    .line 1856692
    return-void
.end method

.method public static a(LX/0QB;)LX/CBf;
    .locals 4

    .prologue
    .line 1856655
    const-class v1, LX/CBf;

    monitor-enter v1

    .line 1856656
    :try_start_0
    sget-object v0, LX/CBf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856657
    sput-object v2, LX/CBf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856658
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856659
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856660
    new-instance v3, LX/CBf;

    const/16 p0, 0x213e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CBf;-><init>(LX/0Ot;)V

    .line 1856661
    move-object v0, v3

    .line 1856662
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856663
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856664
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1856688
    const v0, 0x4fac51ad

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1856677
    check-cast p2, LX/CBe;

    .line 1856678
    iget-object v0, p0, LX/CBf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextHScrollItemComponentSpec;

    iget v1, p2, LX/CBe;->a:I

    iget-object v2, p2, LX/CBe;->c:Landroid/net/Uri;

    .line 1856679
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v3, v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextHScrollItemComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    .line 1856680
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object p2

    new-instance v0, LX/1o9;

    invoke-direct {v0, v1, v1}, LX/1o9;-><init>(II)V

    .line 1856681
    iput-object v0, p2, LX/1bX;->c:LX/1o9;

    .line 1856682
    move-object p2, p2

    .line 1856683
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/1bX;->a(Z)LX/1bX;

    move-result-object p2

    invoke-virtual {p2}, LX/1bX;->n()LX/1bf;

    move-result-object p2

    move-object p2, p2

    .line 1856684
    invoke-virtual {v3, p2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    sget-object p2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionSocialContextHScrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    const/4 p2, 0x1

    invoke-virtual {v3, p2}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    sget-object p0, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, p0}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1856685
    const p0, 0x4fac51ad

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1856686
    invoke-interface {v3, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v1}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1856687
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1856666
    invoke-static {}, LX/1dS;->b()V

    .line 1856667
    iget v0, p1, LX/1dQ;->b:I

    .line 1856668
    packed-switch v0, :pswitch_data_0

    .line 1856669
    :goto_0
    return-object v2

    .line 1856670
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1856671
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1856672
    check-cast v1, LX/CBe;

    .line 1856673
    iget-object p1, p0, LX/CBf;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/CBe;->b:Landroid/view/View$OnClickListener;

    .line 1856674
    if-eqz p1, :cond_0

    .line 1856675
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1856676
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4fac51ad
        :pswitch_0
    .end packed-switch
.end method
