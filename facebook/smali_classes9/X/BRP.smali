.class public LX/BRP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783952
    invoke-static {}, LX/BRP;->c()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object v0

    iput-object v0, p0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783953
    return-void
.end method

.method public static a(LX/0QB;)LX/BRP;
    .locals 3

    .prologue
    .line 1783954
    const-class v1, LX/BRP;

    monitor-enter v1

    .line 1783955
    :try_start_0
    sget-object v0, LX/BRP;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1783956
    sput-object v2, LX/BRP;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1783957
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1783958
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1783959
    new-instance v0, LX/BRP;

    invoke-direct {v0}, LX/BRP;-><init>()V

    .line 1783960
    move-object v0, v0

    .line 1783961
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1783962
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BRP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1783963
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1783964
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1783965
    new-instance v0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    invoke-direct {v0, v1, v1, v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;-><init>(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1783966
    invoke-static {}, LX/BRP;->c()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object v0

    iput-object v0, p0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783967
    return-void
.end method

.method public final a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V
    .locals 1
    .param p2    # Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1783968
    new-instance v0, LX/BRO;

    invoke-direct {v0}, LX/BRO;-><init>()V

    .line 1783969
    iput-object p1, v0, LX/BRO;->a:LX/5QV;

    .line 1783970
    move-object v0, v0

    .line 1783971
    iput-object p2, v0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783972
    move-object v0, v0

    .line 1783973
    iput-object p3, v0, LX/BRO;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783974
    move-object v0, v0

    .line 1783975
    invoke-virtual {v0}, LX/BRO;->a()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object v0

    iput-object v0, p0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783976
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1783977
    iget-object v0, p0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->g()LX/BRO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BRO;->a(Ljava/lang/String;)LX/BRO;

    move-result-object v0

    invoke-virtual {v0}, LX/BRO;->a()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object v0

    iput-object v0, p0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783978
    return-void
.end method
