.class public LX/CUG;
.super LX/CUD;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final i:LX/CUF;

.field public final j:LX/CUE;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;LX/CSr;)V
    .locals 2

    .prologue
    const v1, 0x5de9ea56

    .line 1896416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    .line 1896417
    iput v1, v0, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 1896418
    move-object v0, v0

    .line 1896419
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/CUD;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;LX/CSr;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    .line 1896420
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896421
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1896422
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->p()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget p2, v0, LX/1vs;->b:I

    .line 1896423
    new-instance v0, LX/CUF;

    invoke-direct {v0, v1, p2}, LX/CUF;-><init>(LX/15i;I)V

    :goto_1
    move-object v0, v0

    .line 1896424
    iput-object v0, p0, LX/CUG;->i:LX/CUF;

    .line 1896425
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1896426
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget p2, v0, LX/1vs;->b:I

    .line 1896427
    new-instance v0, LX/CUE;

    invoke-direct {v0, v1, p2}, LX/CUE;-><init>(LX/15i;I)V

    :goto_2
    move-object v0, v0

    .line 1896428
    iput-object v0, p0, LX/CUG;->j:LX/CUE;

    .line 1896429
    return-void

    .line 1896430
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method
