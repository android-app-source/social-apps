.class public LX/C5a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17Q;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849125
    iput-object p1, p0, LX/C5a;->a:LX/0Zb;

    .line 1849126
    iput-object p2, p0, LX/C5a;->b:LX/17Q;

    .line 1849127
    return-void
.end method

.method public static a(LX/0QB;)LX/C5a;
    .locals 5

    .prologue
    .line 1849113
    const-class v1, LX/C5a;

    monitor-enter v1

    .line 1849114
    :try_start_0
    sget-object v0, LX/C5a;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849115
    sput-object v2, LX/C5a;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849116
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849117
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849118
    new-instance p0, LX/C5a;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-direct {p0, v3, v4}, LX/C5a;-><init>(LX/0Zb;LX/17Q;)V

    .line 1849119
    move-object v0, p0

    .line 1849120
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849121
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849122
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849123
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
