.class public final LX/Cgn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARN;


# instance fields
.field public final synthetic c:LX/Cgz;


# direct methods
.method public constructor <init>(LX/Cgz;)V
    .locals 0

    .prologue
    .line 1927493
    iput-object p1, p0, LX/Cgn;->c:LX/Cgz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    .line 1927494
    iget-object v0, p0, LX/Cgn;->c:LX/Cgz;

    .line 1927495
    iget-object v1, v0, LX/Cgz;->g:LX/Ch2;

    .line 1927496
    invoke-static {v1}, LX/Ch2;->g(LX/Ch2;)I

    move-result v2

    invoke-static {v1, v2}, LX/Ch2;->a(LX/Ch2;I)Z

    move-result v2

    move v1, v2

    .line 1927497
    if-nez v1, :cond_0

    .line 1927498
    const/4 v1, 0x0

    .line 1927499
    :goto_0
    move v0, v1

    .line 1927500
    return v0

    .line 1927501
    :cond_0
    iget-object v1, v0, LX/Cgz;->h:LX/79D;

    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v3, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    .line 1927502
    const-string v4, "composer_submit_button_tapped_with_short_review"

    invoke-static {v4, v2, v3}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1927503
    iget-object v5, v1, LX/79D;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927504
    iget-object v1, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1927505
    const v2, 0x7f040020

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1927506
    iget-object v2, v0, LX/Cgz;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1927507
    const/4 v1, 0x1

    goto :goto_0
.end method
