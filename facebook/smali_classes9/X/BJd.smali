.class public final LX/BJd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BKm;

.field public final synthetic b:LX/BJp;

.field public final synthetic c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

.field public final synthetic d:LX/BJi;


# direct methods
.method public constructor <init>(LX/BJi;LX/BKm;LX/BJp;Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;)V
    .locals 0

    .prologue
    .line 1772256
    iput-object p1, p0, LX/BJd;->d:LX/BJi;

    iput-object p2, p0, LX/BJd;->a:LX/BKm;

    iput-object p3, p0, LX/BJd;->b:LX/BJp;

    iput-object p4, p0, LX/BJd;->c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x5a27c125

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772257
    iget-object v1, p0, LX/BJd;->d:LX/BJi;

    iget-object v1, v1, LX/BJi;->a:LX/BJ7;

    iget-object v2, p0, LX/BJd;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->a:Ljava/lang/String;

    .line 1772258
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/BJ6;->DRAFT_DIALOG_SAVE_CLICKED:LX/BJ6;

    iget-object p1, p1, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "composer_session_id"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v1, v4}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1772259
    iget-object v1, p0, LX/BJd;->b:LX/BJp;

    .line 1772260
    iget-object v2, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v4, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v4}, LX/BKm;->a()LX/BKl;

    move-result-object v4

    iget-object v5, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v5, v5, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v5

    const/4 p1, 0x1

    .line 1772261
    iput-boolean p1, v5, LX/BKp;->v:Z

    .line 1772262
    move-object v5, v5

    .line 1772263
    invoke-virtual {v5}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v5

    .line 1772264
    iput-object v5, v4, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1772265
    move-object v4, v4

    .line 1772266
    invoke-virtual {v4}, LX/BKl;->a()LX/BKm;

    move-result-object v4

    .line 1772267
    iput-object v4, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772268
    iget-object v2, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v4, -0x1

    iget-object v5, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v5}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->k(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Landroid/content/Intent;

    move-result-object v5

    const/4 p1, 0x0

    invoke-static {v2, v4, v5, p1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;ILandroid/content/Intent;Z)V

    .line 1772269
    iget-object v1, p0, LX/BJd;->c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1772270
    const v1, -0x30432c8a

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
