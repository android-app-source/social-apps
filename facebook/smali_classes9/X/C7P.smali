.class public LX/C7P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:Z

.field public final b:LX/3AF;

.field public final c:LX/3AE;

.field public final d:LX/3AD;

.field public final e:LX/17W;

.field public final f:LX/1nM;

.field public final g:LX/1VI;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;LX/3AF;LX/3AE;LX/3AD;LX/1nM;LX/17W;LX/1VI;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/feed/annotations/IsAutoTranslateEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851299
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/C7P;->a:Z

    .line 1851300
    iput-object p2, p0, LX/C7P;->b:LX/3AF;

    .line 1851301
    iput-object p3, p0, LX/C7P;->c:LX/3AE;

    .line 1851302
    iput-object p4, p0, LX/C7P;->d:LX/3AD;

    .line 1851303
    iput-object p5, p0, LX/C7P;->f:LX/1nM;

    .line 1851304
    iput-object p6, p0, LX/C7P;->e:LX/17W;

    .line 1851305
    iput-object p7, p0, LX/C7P;->g:LX/1VI;

    .line 1851306
    return-void
.end method

.method public static a(LX/C7P;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 1851318
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/C7P;->a(LX/1De;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    .line 1851319
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851320
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851321
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTranslation;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 1851322
    iget-object p1, p0, LX/C7P;->b:LX/3AF;

    invoke-virtual {p1, v3}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1851323
    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v2, 0x7f0b0050

    invoke-virtual {v0, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v2}, LX/1ne;->i(F)LX/1ne;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    iget-object v2, p0, LX/C7P;->g:LX/1VI;

    invoke-static {v2}, LX/C7I;->a(LX/1VI;)I

    move-result v2

    invoke-virtual {v0, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v2, 0x7f0a0159

    invoke-virtual {v0, v2}, LX/1ne;->v(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    const/4 v2, 0x7

    invoke-interface {v0, v2, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-interface {v0, v2, v3}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const/4 v2, 0x5

    const v3, 0x7f0b0917

    invoke-interface {v0, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v5, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    const v2, 0x7f020afa

    invoke-interface {v0, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;)LX/1Di;
    .locals 3

    .prologue
    .line 1851324
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a0042

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->j(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/C7P;
    .locals 11

    .prologue
    .line 1851307
    const-class v1, LX/C7P;

    monitor-enter v1

    .line 1851308
    :try_start_0
    sget-object v0, LX/C7P;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851309
    sput-object v2, LX/C7P;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851310
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851311
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851312
    new-instance v3, LX/C7P;

    invoke-static {v0}, LX/1W3;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-static {v0}, LX/3AF;->a(LX/0QB;)LX/3AF;

    move-result-object v5

    check-cast v5, LX/3AF;

    invoke-static {v0}, LX/3AE;->b(LX/0QB;)LX/3AE;

    move-result-object v6

    check-cast v6, LX/3AE;

    invoke-static {v0}, LX/3AD;->b(LX/0QB;)LX/3AD;

    move-result-object v7

    check-cast v7, LX/3AD;

    invoke-static {v0}, LX/1nM;->a(LX/0QB;)LX/1nM;

    move-result-object v8

    check-cast v8, LX/1nM;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v10

    check-cast v10, LX/1VI;

    invoke-direct/range {v3 .. v10}, LX/C7P;-><init>(Ljava/lang/Boolean;LX/3AF;LX/3AE;LX/3AD;LX/1nM;LX/17W;LX/1VI;)V

    .line 1851313
    move-object v0, v3

    .line 1851314
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851315
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851316
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851317
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
