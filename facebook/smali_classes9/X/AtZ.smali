.class public final enum LX/AtZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AtZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AtZ;

.field public static final enum AUDIO_PERMISSION:LX/AtZ;

.field public static final enum CAMERA_PERMISSION:LX/AtZ;

.field public static final enum LOCATION_PERMISSION:LX/AtZ;

.field public static final enum STORAGE_PERMISSION:LX/AtZ;


# instance fields
.field public final mLoggingKey:Ljava/lang/String;

.field public final mManifestFlags:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1721552
    new-instance v0, LX/AtZ;

    const-string v1, "CAMERA_PERMISSION"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "android.permission.CAMERA"

    aput-object v3, v2, v4

    const-string v3, "camera"

    invoke-direct {v0, v1, v4, v2, v3}, LX/AtZ;-><init>(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/AtZ;->CAMERA_PERMISSION:LX/AtZ;

    .line 1721553
    new-instance v0, LX/AtZ;

    const-string v1, "AUDIO_PERMISSION"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "android.permission.RECORD_AUDIO"

    aput-object v3, v2, v4

    const-string v3, "microphone"

    invoke-direct {v0, v1, v5, v2, v3}, LX/AtZ;-><init>(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/AtZ;->AUDIO_PERMISSION:LX/AtZ;

    .line 1721554
    new-instance v0, LX/AtZ;

    const-string v1, "STORAGE_PERMISSION"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v3, v2, v4

    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v3, v2, v5

    const-string v3, "photo"

    invoke-direct {v0, v1, v6, v2, v3}, LX/AtZ;-><init>(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/AtZ;->STORAGE_PERMISSION:LX/AtZ;

    .line 1721555
    new-instance v0, LX/AtZ;

    const-string v1, "LOCATION_PERMISSION"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v3, v2, v4

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v3, v2, v5

    const-string v3, "location"

    invoke-direct {v0, v1, v7, v2, v3}, LX/AtZ;-><init>(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/AtZ;->LOCATION_PERMISSION:LX/AtZ;

    .line 1721556
    const/4 v0, 0x4

    new-array v0, v0, [LX/AtZ;

    sget-object v1, LX/AtZ;->CAMERA_PERMISSION:LX/AtZ;

    aput-object v1, v0, v4

    sget-object v1, LX/AtZ;->AUDIO_PERMISSION:LX/AtZ;

    aput-object v1, v0, v5

    sget-object v1, LX/AtZ;->STORAGE_PERMISSION:LX/AtZ;

    aput-object v1, v0, v6

    sget-object v1, LX/AtZ;->LOCATION_PERMISSION:LX/AtZ;

    aput-object v1, v0, v7

    sput-object v0, LX/AtZ;->$VALUES:[LX/AtZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1721557
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1721558
    iput-object p3, p0, LX/AtZ;->mManifestFlags:[Ljava/lang/String;

    .line 1721559
    iput-object p4, p0, LX/AtZ;->mLoggingKey:Ljava/lang/String;

    .line 1721560
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AtZ;
    .locals 1

    .prologue
    .line 1721561
    const-class v0, LX/AtZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AtZ;

    return-object v0
.end method

.method public static values()[LX/AtZ;
    .locals 1

    .prologue
    .line 1721562
    sget-object v0, LX/AtZ;->$VALUES:[LX/AtZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AtZ;

    return-object v0
.end method
