.class public final LX/AjO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AjP;

.field private b:Z

.field private c:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(LX/AjP;Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 0

    .prologue
    .line 1707896
    iput-object p1, p0, LX/AjO;->a:LX/AjP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707897
    iput-boolean p3, p0, LX/AjO;->b:Z

    .line 1707898
    iput-object p2, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1707899
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1707876
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1707877
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1707878
    iget-object v0, p0, LX/AjO;->a:LX/AjP;

    iget-object v0, v0, LX/AjP;->c:LX/AjN;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1707879
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707880
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_1

    .line 1707881
    :cond_0
    :goto_0
    return-void

    .line 1707882
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707883
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1707884
    iget-object v1, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eq v1, v0, :cond_0

    .line 1707885
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1707886
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v4, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    if-eq v1, v4, :cond_5

    move v1, v2

    .line 1707887
    :goto_1
    iget-boolean v4, p0, LX/AjO;->b:Z

    if-eqz v4, :cond_6

    .line 1707888
    :cond_2
    :goto_2
    if-nez v1, :cond_3

    if-eqz v2, :cond_0

    .line 1707889
    :cond_3
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 1707890
    iget-boolean v1, p0, LX/AjO;->b:Z

    if-nez v1, :cond_4

    .line 1707891
    iget-object v1, p0, LX/AjO;->a:LX/AjP;

    iget-object v1, v1, LX/AjP;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 1707892
    :cond_4
    iget-object v1, p0, LX/AjO;->a:LX/AjP;

    iget-object v1, v1, LX/AjP;->c:LX/AjN;

    invoke-interface {v1, v0}, LX/AjN;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1707893
    iput-object v0, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_5
    move v1, v3

    .line 1707894
    goto :goto_1

    .line 1707895
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v4

    iget-object v6, p0, LX/AjO;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    move v2, v3

    goto :goto_2
.end method
