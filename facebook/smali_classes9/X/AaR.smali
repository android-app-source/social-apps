.class public final LX/AaR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V
    .locals 0

    .prologue
    .line 1688520
    iput-object p1, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    .line 1688521
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v0, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->y:LX/AaY;

    if-eqz v0, :cond_0

    .line 1688522
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v0, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->y:LX/AaY;

    .line 1688523
    iput-boolean p2, v0, LX/AaY;->a:Z

    .line 1688524
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v0, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->y:LX/AaY;

    .line 1688525
    iget-object v1, v0, LX/AaY;->c:LX/0if;

    sget-object v2, LX/0ig;->G:LX/0ih;

    const-string v3, "toggle_tip_jar"

    const/4 v4, 0x0

    invoke-static {v0}, LX/AaY;->g(LX/AaY;)LX/1rQ;

    move-result-object p1

    invoke-virtual {v1, v2, v3, v4, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1688526
    :cond_0
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    .line 1688527
    iput-boolean p2, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    .line 1688528
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v0, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v2, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-boolean v2, v2, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-static {v1, v2}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->d(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688529
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v0, v0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    iget-object v1, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v2, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-boolean v2, v2, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-static {v1, v2}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->d(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1688530
    iget-object v0, p0, LX/AaR;->a:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-static {v0}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->m(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    .line 1688531
    return-void
.end method
