.class public final LX/Avh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89p;


# instance fields
.field public final synthetic a:LX/Avj;


# direct methods
.method public constructor <init>(LX/Avj;)V
    .locals 0

    .prologue
    .line 1724517
    iput-object p1, p0, LX/Avh;->a:LX/Avj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1724518
    iget-object v0, p0, LX/Avh;->a:LX/Avj;

    .line 1724519
    iget-object v1, v0, LX/Avj;->e:LX/Avf;

    iget-object v2, v0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1724520
    iget-object v3, v1, LX/Avf;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    .line 1724521
    invoke-virtual {v2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->k()Ljava/lang/String;

    move-result-object p0

    .line 1724522
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v3, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    invoke-virtual {v2}, LX/AwR;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 1724523
    move-object v1, v3

    .line 1724524
    iput-object v1, v0, LX/Avj;->m:Ljava/lang/String;

    .line 1724525
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Avj;->n:Z

    .line 1724526
    iget-boolean v1, v0, LX/Avj;->j:Z

    if-eqz v1, :cond_0

    .line 1724527
    invoke-static {v0}, LX/Avj;->n(LX/Avj;)V

    .line 1724528
    iget-object v1, v0, LX/Avj;->f:LX/Arh;

    iget-object v2, v0, LX/Avj;->h:LX/Arb;

    invoke-virtual {v1, v2}, LX/Arh;->a(LX/Arb;)V

    .line 1724529
    iget-object v1, v0, LX/Avj;->f:LX/Arh;

    .line 1724530
    iget-object v2, v1, LX/Arh;->j:LX/Arq;

    .line 1724531
    iget-object v1, v2, LX/Arq;->m:LX/BAC;

    invoke-virtual {v1, v0}, LX/BAC;->a(LX/Avi;)V

    .line 1724532
    iget-object v1, v0, LX/Avj;->d:LX/AsL;

    invoke-virtual {v1}, LX/AsL;->c()V

    .line 1724533
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1724534
    iget-object v0, p0, LX/Avh;->a:LX/Avj;

    iget-object v0, v0, LX/Avj;->d:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->d()V

    .line 1724535
    iget-object v0, p0, LX/Avh;->a:LX/Avj;

    iget-object v0, v0, LX/Avj;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0827a0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1724536
    return-void
.end method
