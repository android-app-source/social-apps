.class public final LX/BLD;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1775416
    iput-object p1, p0, LX/BLD;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1775417
    iget-object v0, p0, LX/BLD;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1775418
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1775419
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BLD;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
