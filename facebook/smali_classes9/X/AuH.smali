.class public LX/AuH;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;ZZI)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1722421
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1722422
    iput-object p1, p0, LX/AuH;->c:Landroid/view/View;

    .line 1722423
    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p0, LX/AuH;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    mul-int/2addr v0, v4

    :goto_1
    iput v0, p0, LX/AuH;->a:I

    .line 1722424
    if-eqz p2, :cond_2

    :goto_2
    iput v3, p0, LX/AuH;->b:I

    .line 1722425
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, LX/AuH;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1722426
    int-to-long v0, p4

    invoke-virtual {p0, v0, v1}, LX/AuH;->setDuration(J)V

    .line 1722427
    return-void

    :cond_0
    move v0, v2

    .line 1722428
    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    .line 1722429
    :cond_2
    if-eqz p3, :cond_3

    :goto_3
    iget-object v0, p0, LX/AuH;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    mul-int v3, v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 1722430
    iget v0, p0, LX/AuH;->a:I

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, LX/AuH;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    .line 1722431
    iget-object v1, p0, LX/AuH;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1722432
    return-void
.end method
