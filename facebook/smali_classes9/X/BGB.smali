.class public final LX/BGB;
.super LX/0wa;
.source ""


# instance fields
.field public final synthetic a:LX/BGC;


# direct methods
.method public constructor <init>(LX/BGC;)V
    .locals 0

    .prologue
    .line 1766776
    iput-object p1, p0, LX/BGB;->a:LX/BGC;

    invoke-direct {p0}, LX/0wa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 4

    .prologue
    .line 1766777
    iget-object v0, p0, LX/BGB;->a:LX/BGC;

    iget-object v0, v0, LX/BGC;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1766778
    iget-object v2, p0, LX/BGB;->a:LX/BGC;

    iget-wide v2, v2, LX/BGC;->f:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1766779
    iget-object v1, p0, LX/BGB;->a:LX/BGC;

    iget-object v1, v1, LX/BGC;->d:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_3

    .line 1766780
    iget-object v1, p0, LX/BGB;->a:LX/BGC;

    iget-object v1, v1, LX/BGC;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    .line 1766781
    if-lez v1, :cond_0

    if-ge v0, v1, :cond_2

    .line 1766782
    :cond_0
    iget-object v2, p0, LX/BGB;->a:LX/BGC;

    invoke-static {v2, v0}, LX/BGC;->a$redex0(LX/BGC;I)V

    .line 1766783
    iget-object v2, p0, LX/BGB;->a:LX/BGC;

    iget-object v2, v2, LX/BGC;->d:Landroid/widget/ProgressBar;

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1766784
    iget-object v0, p0, LX/BGB;->a:LX/BGC;

    iget-object v0, v0, LX/BGC;->h:LX/0wY;

    iget-object v1, p0, LX/BGB;->a:LX/BGC;

    iget-object v1, v1, LX/BGC;->n:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 1766785
    :goto_0
    return-void

    .line 1766786
    :cond_2
    iget-object v0, p0, LX/BGB;->a:LX/BGC;

    iget-object v0, v0, LX/BGC;->a:LX/Jvi;

    .line 1766787
    iget-object v1, v0, LX/Jvi;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->k()V

    .line 1766788
    goto :goto_0

    .line 1766789
    :cond_3
    iget-object v1, p0, LX/BGB;->a:LX/BGC;

    invoke-static {v1, v0}, LX/BGC;->a$redex0(LX/BGC;I)V

    .line 1766790
    iget-object v0, p0, LX/BGB;->a:LX/BGC;

    iget-object v0, v0, LX/BGC;->h:LX/0wY;

    iget-object v1, p0, LX/BGB;->a:LX/BGC;

    iget-object v1, v1, LX/BGC;->n:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    goto :goto_0
.end method
