.class public final LX/Bcc;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Bce;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/Bcd;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1802376
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "component"

    aput-object v2, v0, v1

    sput-object v0, LX/Bcc;->b:[Ljava/lang/String;

    .line 1802377
    sput v3, LX/Bcc;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1802378
    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1802379
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/Bcc;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bcc;->d:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/Bcc;
    .locals 2

    .prologue
    .line 1802380
    iget-object v0, p0, LX/Bcc;->a:LX/Bcd;

    iput-object p1, v0, LX/Bcd;->b:LX/1X1;

    .line 1802381
    iget-object v0, p0, LX/Bcc;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1802382
    return-object p0
.end method

.method public final a(LX/1X5;)LX/Bcc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X5",
            "<+",
            "LX/1S3;",
            ">;)",
            "LX/Bcc;"
        }
    .end annotation

    .prologue
    .line 1802383
    iget-object v0, p0, LX/Bcc;->a:LX/Bcd;

    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    iput-object v1, v0, LX/Bcd;->b:LX/1X1;

    .line 1802384
    iget-object v0, p0, LX/Bcc;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1802385
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1802386
    invoke-super {p0}, LX/BcN;->a()V

    .line 1802387
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bcc;->a:LX/Bcd;

    .line 1802388
    sget-object v0, LX/Bce;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1802389
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Bce;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1802390
    iget-object v1, p0, LX/Bcc;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bcc;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/Bcc;->c:I

    if-ge v1, v2, :cond_2

    .line 1802391
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1802392
    :goto_0
    sget v2, LX/Bcc;->c:I

    if-ge v0, v2, :cond_1

    .line 1802393
    iget-object v2, p0, LX/Bcc;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1802394
    sget-object v2, LX/Bcc;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802395
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802396
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802397
    :cond_2
    iget-object v0, p0, LX/Bcc;->a:LX/Bcd;

    .line 1802398
    invoke-virtual {p0}, LX/Bcc;->a()V

    .line 1802399
    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/Bcc;
    .locals 0

    .prologue
    .line 1802400
    invoke-super {p0, p1}, LX/BcN;->a(Ljava/lang/String;)V

    .line 1802401
    return-object p0
.end method
