.class public final LX/BTt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1788049
    iput-object p1, p0, LX/BTt;->b:LX/BUA;

    iput-object p2, p0, LX/BTt;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1788050
    iget-object v1, p0, LX/BTt;->b:LX/BUA;

    monitor-enter v1

    .line 1788051
    :try_start_0
    iget-object v0, p0, LX/BTt;->b:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    iget-object v2, p0, LX/BTt;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 1788052
    if-eqz v0, :cond_0

    iget-object v2, v0, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-ne v2, v3, :cond_0

    .line 1788053
    iget-object v2, p0, LX/BTt;->b:LX/BUA;

    iget-object v3, v0, LX/7Jg;->a:Ljava/lang/String;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-static {v2, v3, v4}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788054
    sget-object v2, LX/BUA;->d:Ljava/lang/String;

    const-string v3, "Retrying download for video %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, LX/7Jg;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788055
    iget-object v2, p0, LX/BTt;->b:LX/BUA;

    .line 1788056
    invoke-static {v2, v0}, LX/BUA;->a$redex0(LX/BUA;LX/7Jg;)V

    .line 1788057
    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    return-object v0

    .line 1788058
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
