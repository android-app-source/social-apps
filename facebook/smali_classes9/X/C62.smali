.class public LX/C62;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Landroid/animation/PropertyValuesHolder;

.field public static final b:Landroid/animation/PropertyValuesHolder;

.field private static volatile g:LX/C62;


# instance fields
.field public final c:Landroid/animation/PropertyValuesHolder;

.field public final d:Landroid/animation/PropertyValuesHolder;

.field public final e:Landroid/animation/PropertyValuesHolder;

.field public final f:Landroid/animation/PropertyValuesHolder;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1849593
    const-string v0, "alpha"

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    sput-object v0, LX/C62;->a:Landroid/animation/PropertyValuesHolder;

    .line 1849594
    const-string v0, "alpha"

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    sput-object v0, LX/C62;->b:Landroid/animation/PropertyValuesHolder;

    return-void

    .line 1849595
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1849596
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1849597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849598
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1849599
    const-string v1, "translationY"

    new-array v2, v7, [F

    int-to-float v3, v0

    aput v3, v2, v5

    aput v4, v2, v6

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    iput-object v1, p0, LX/C62;->c:Landroid/animation/PropertyValuesHolder;

    .line 1849600
    const-string v1, "translationY"

    new-array v2, v7, [F

    aput v4, v2, v5

    int-to-float v3, v0

    aput v3, v2, v6

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    iput-object v1, p0, LX/C62;->d:Landroid/animation/PropertyValuesHolder;

    .line 1849601
    const-string v1, "translationY"

    new-array v2, v7, [F

    neg-int v3, v0

    int-to-float v3, v3

    aput v3, v2, v5

    aput v4, v2, v6

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    iput-object v1, p0, LX/C62;->e:Landroid/animation/PropertyValuesHolder;

    .line 1849602
    const-string v1, "translationY"

    new-array v2, v7, [F

    aput v4, v2, v5

    neg-int v0, v0

    int-to-float v0, v0

    aput v0, v2, v6

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iput-object v0, p0, LX/C62;->f:Landroid/animation/PropertyValuesHolder;

    .line 1849603
    return-void
.end method

.method public static a(LX/0QB;)LX/C62;
    .locals 4

    .prologue
    .line 1849604
    sget-object v0, LX/C62;->g:LX/C62;

    if-nez v0, :cond_1

    .line 1849605
    const-class v1, LX/C62;

    monitor-enter v1

    .line 1849606
    :try_start_0
    sget-object v0, LX/C62;->g:LX/C62;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1849607
    if-eqz v2, :cond_0

    .line 1849608
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1849609
    new-instance p0, LX/C62;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/C62;-><init>(Landroid/content/Context;)V

    .line 1849610
    move-object v0, p0

    .line 1849611
    sput-object v0, LX/C62;->g:LX/C62;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849612
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1849613
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1849614
    :cond_1
    sget-object v0, LX/C62;->g:LX/C62;

    return-object v0

    .line 1849615
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1849616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
