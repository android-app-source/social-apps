.class public LX/BTh;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1787861
    const-class v0, LX/BTh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BTh;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1787862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787863
    return-void
.end method

.method public static a(LX/0QB;)LX/BTh;
    .locals 1

    .prologue
    .line 1787864
    invoke-static {p0}, LX/BTh;->b(LX/0QB;)LX/BTh;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BTh;
    .locals 3

    .prologue
    .line 1787865
    new-instance v0, LX/BTh;

    invoke-direct {v0}, LX/BTh;-><init>()V

    .line 1787866
    const/16 v1, 0x259

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2d9

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 1787867
    iput-object v1, v0, LX/BTh;->a:LX/0Or;

    iput-object v2, v0, LX/BTh;->b:LX/0Or;

    .line 1787868
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;JILandroid/content/Context;)Landroid/net/Uri;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1787869
    iget-object v0, p0, LX/BTh;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    .line 1787870
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1787871
    invoke-virtual {v3, p5, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1787872
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v4

    const/4 v1, 0x2

    invoke-virtual {v3, v4, v5, v1}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1787873
    if-nez v1, :cond_0

    .line 1787874
    iget-object v0, p0, LX/BTh;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1787875
    sget-object v1, LX/BTh;->c:Ljava/lang/String;

    const-string v2, "MediaMetadataRetriever could not retrieve thumbnail"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787876
    const/4 v0, 0x0

    .line 1787877
    :goto_0
    return-object v0

    .line 1787878
    :cond_0
    if-eqz p4, :cond_1

    .line 1787879
    const/4 v2, 0x1

    invoke-static {v1, p4, v2}, LX/2Qx;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1787880
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "FB_VCT_"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v5}, Landroid/graphics/Bitmap$CompressFormat;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v2, v4, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v4

    .line 1787881
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    invoke-static {v1, v0, v2, v4}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1787882
    if-eqz v1, :cond_2

    .line 1787883
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1787884
    :cond_2
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1787885
    :goto_1
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1787886
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1787887
    :try_start_1
    iget-object v0, p0, LX/BTh;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1787888
    sget-object v5, LX/BTh;->c:Ljava/lang/String;

    const-string v6, "Could not save thumbnail for edited video"

    invoke-virtual {v0, v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1787889
    if-eqz v1, :cond_3

    .line 1787890
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1787891
    :cond_3
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 1787892
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 1787893
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1787894
    :cond_4
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method
