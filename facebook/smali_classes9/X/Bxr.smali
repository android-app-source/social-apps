.class public LX/Bxr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/36S;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/Ap5;

.field public final c:LX/ApL;

.field public final d:LX/Bxu;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public constructor <init>(LX/0Ot;LX/Bxu;LX/Ap5;LX/ApL;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/36S;",
            ">;",
            "LX/Bxu;",
            "LX/Ap5;",
            "LX/ApL;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836253
    iput-object p1, p0, LX/Bxr;->a:LX/0Ot;

    .line 1836254
    iput-object p2, p0, LX/Bxr;->d:LX/Bxu;

    .line 1836255
    iput-object p3, p0, LX/Bxr;->b:LX/Ap5;

    .line 1836256
    iput-object p4, p0, LX/Bxr;->c:LX/ApL;

    .line 1836257
    iput-object p5, p0, LX/Bxr;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1836258
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxr;
    .locals 9

    .prologue
    .line 1836259
    const-class v1, LX/Bxr;

    monitor-enter v1

    .line 1836260
    :try_start_0
    sget-object v0, LX/Bxr;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836261
    sput-object v2, LX/Bxr;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836262
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836263
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836264
    new-instance v3, LX/Bxr;

    const/16 v4, 0x7db

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/Bxu;->a(LX/0QB;)LX/Bxu;

    move-result-object v5

    check-cast v5, LX/Bxu;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v6

    check-cast v6, LX/Ap5;

    invoke-static {v0}, LX/ApL;->a(LX/0QB;)LX/ApL;

    move-result-object v7

    check-cast v7, LX/ApL;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct/range {v3 .. v8}, LX/Bxr;-><init>(LX/0Ot;LX/Bxu;LX/Ap5;LX/ApL;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 1836265
    move-object v0, v3

    .line 1836266
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836267
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836268
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
