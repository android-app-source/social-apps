.class public LX/C4m;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1847912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;ILandroid/content/res/Resources;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1847906
    if-nez p0, :cond_1

    move p1, v0

    .line 1847907
    :cond_0
    :goto_0
    return p1

    .line 1847908
    :cond_1
    const-string v1, "MARGIN_"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_2

    .line 1847909
    invoke-static {p0, p2}, LX/C4m;->a(Ljava/lang/String;Landroid/content/res/Resources;)I

    move-result p1

    goto :goto_0

    .line 1847910
    :cond_2
    const-string v1, "BOTTOM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move p1, v0

    .line 1847911
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/content/res/Resources;)I
    .locals 3

    .prologue
    .line 1847898
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1847899
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1847900
    const/4 v1, 0x1

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    float-to-int v0, v0

    .line 1847901
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;ILandroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 1847902
    if-nez p0, :cond_1

    .line 1847903
    :cond_0
    :goto_0
    return p1

    .line 1847904
    :cond_1
    const-string v0, "MARGIN_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 1847905
    invoke-static {p0, p2}, LX/C4m;->a(Ljava/lang/String;Landroid/content/res/Resources;)I

    move-result p1

    goto :goto_0
.end method
