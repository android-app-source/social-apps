.class public LX/AXD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field private final b:Landroid/widget/ScrollView;

.field public final c:Landroid/view/ViewGroup;

.field private final d:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

.field public final e:Landroid/view/ViewGroup;

.field public f:Z

.field public g:Z

.field public h:LX/AX2;

.field public i:LX/AXP;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/widget/ScrollView;Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1683817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1683818
    iput-boolean v0, p0, LX/AXD;->f:Z

    .line 1683819
    iput-boolean v0, p0, LX/AXD;->g:Z

    .line 1683820
    iput-object p1, p0, LX/AXD;->a:Landroid/view/ViewGroup;

    .line 1683821
    iput-object p2, p0, LX/AXD;->b:Landroid/widget/ScrollView;

    .line 1683822
    invoke-virtual {p2, v0}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/AXD;->c:Landroid/view/ViewGroup;

    .line 1683823
    iput-object p3, p0, LX/AXD;->d:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    .line 1683824
    iput-object p4, p0, LX/AXD;->e:Landroid/view/ViewGroup;

    .line 1683825
    return-void
.end method

.method public static g(LX/AXD;)F
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1683826
    iget-object v0, p0, LX/AXD;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget-object v1, p0, LX/AXD;->d:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    invoke-virtual {v1}, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/AXD;->b:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    return v0
.end method
