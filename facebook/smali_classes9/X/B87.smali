.class public final LX/B87;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/B8A;


# direct methods
.method public constructor <init>(LX/B8A;)V
    .locals 0

    .prologue
    .line 1748445
    iput-object p1, p0, LX/B87;->a:LX/B8A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1748446
    iget-object v0, p0, LX/B87;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->h:LX/B7w;

    if-eqz v0, :cond_0

    .line 1748447
    iget-object v0, p0, LX/B87;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B87;->a:LX/B8A;

    invoke-virtual {v0}, LX/B8A;->getInputValue()Ljava/lang/String;

    .line 1748448
    :cond_0
    iget-object v0, p0, LX/B87;->a:LX/B8A;

    .line 1748449
    iget-object p1, v0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getVisibility()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    :goto_0
    move v0, p1

    .line 1748450
    if-eqz v0, :cond_1

    .line 1748451
    iget-object v0, p0, LX/B87;->a:LX/B8A;

    invoke-static {v0}, LX/B8A;->f(LX/B8A;)V

    .line 1748452
    iget-object v0, p0, LX/B87;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748453
    :cond_1
    return-void

    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748454
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748455
    return-void
.end method
