.class public LX/C7Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/17W;


# direct methods
.method public constructor <init>(LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851438
    iput-object p1, p0, LX/C7Z;->a:LX/17W;

    .line 1851439
    return-void
.end method

.method public static a(LX/0QB;)LX/C7Z;
    .locals 4

    .prologue
    .line 1851440
    const-class v1, LX/C7Z;

    monitor-enter v1

    .line 1851441
    :try_start_0
    sget-object v0, LX/C7Z;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851442
    sput-object v2, LX/C7Z;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851443
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851444
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851445
    new-instance p0, LX/C7Z;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-direct {p0, v3}, LX/C7Z;-><init>(LX/17W;)V

    .line 1851446
    move-object v0, p0

    .line 1851447
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851448
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851449
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
