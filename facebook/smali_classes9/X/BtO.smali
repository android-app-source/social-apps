.class public final LX/BtO;
.super LX/556;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/556",
        "<",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[I

.field private static final c:Landroid/graphics/Rect;

.field private static final d:Landroid/graphics/RectF;

.field private static final e:Landroid/graphics/Path;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1829032
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, LX/BtO;->b:[I

    .line 1829033
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/BtO;->c:Landroid/graphics/Rect;

    .line 1829034
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/BtO;->d:Landroid/graphics/RectF;

    .line 1829035
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, LX/BtO;->e:Landroid/graphics/Path;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/fbui/widget/text/TextLayoutView;)V
    .locals 0

    .prologue
    .line 1829036
    invoke-direct {p0, p1}, LX/556;-><init>(Landroid/view/View;)V

    .line 1829037
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 7

    .prologue
    .line 1829038
    float-to-int v1, p1

    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v0

    sub-int/2addr v1, v0

    float-to-int v2, p2

    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingTop()I

    move-result v0

    sub-int v0, v2, v0

    const/4 p2, 0x0

    const/4 v3, 0x0

    .line 1829039
    iget-object v2, p0, LX/556;->a:Landroid/view/View;

    check-cast v2, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 1829040
    if-nez v4, :cond_1

    move-object v2, v3

    .line 1829041
    :goto_0
    move-object v1, v2

    .line 1829042
    if-nez v1, :cond_0

    .line 1829043
    const/4 v0, -0x1

    .line 1829044
    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1

    .line 1829045
    :cond_1
    invoke-virtual {v4}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1829046
    instance-of v5, v2, Landroid/text/Spanned;

    if-nez v5, :cond_2

    move-object v2, v3

    .line 1829047
    goto :goto_0

    .line 1829048
    :cond_2
    invoke-virtual {v4, p2}, Landroid/text/Layout;->getLineTop(I)I

    move-result v5

    if-lt v0, v5, :cond_3

    invoke-virtual {v4}, Landroid/text/Layout;->getLineCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v5

    if-lt v0, v5, :cond_4

    :cond_3
    move-object v2, v3

    .line 1829049
    goto :goto_0

    .line 1829050
    :cond_4
    invoke-virtual {v4, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v5

    .line 1829051
    int-to-float v6, v1

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineLeft(I)F

    move-result p1

    cmpg-float v6, v6, p1

    if-ltz v6, :cond_5

    int-to-float v6, v1

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineRight(I)F

    move-result p1

    cmpl-float v6, v6, p1

    if-lez v6, :cond_6

    :cond_5
    move-object v2, v3

    .line 1829052
    goto :goto_0

    .line 1829053
    :cond_6
    int-to-float v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v4

    .line 1829054
    check-cast v2, Landroid/text/Spanned;

    const-class v5, Landroid/text/style/ClickableSpan;

    invoke-interface {v2, v4, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/ClickableSpan;

    .line 1829055
    array-length v4, v2

    if-nez v4, :cond_7

    move-object v2, v3

    .line 1829056
    goto :goto_0

    .line 1829057
    :cond_7
    aget-object v2, v2, p2

    goto :goto_0
.end method

.method public final a(ILX/3sp;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1829058
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 1829059
    if-nez v5, :cond_1

    .line 1829060
    :cond_0
    :goto_0
    return-void

    .line 1829061
    :cond_1
    invoke-virtual {v5}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1829062
    instance-of v1, v0, Landroid/text/Spanned;

    if-eqz v1, :cond_0

    .line 1829063
    check-cast v0, Landroid/text/Spanned;

    .line 1829064
    const-class v1, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, p1, p1, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 1829065
    array-length v4, v1

    if-eqz v4, :cond_0

    .line 1829066
    aget-object v1, v1, v3

    .line 1829067
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 1829068
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    .line 1829069
    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    .line 1829070
    invoke-virtual {v5, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 1829071
    if-eq v7, v1, :cond_2

    move v1, v2

    .line 1829072
    :goto_1
    iget-object v8, p0, LX/556;->a:Landroid/view/View;

    invoke-virtual {p2, v8}, LX/3sp;->d(Landroid/view/View;)V

    .line 1829073
    invoke-interface {v0, v6, v4}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1829074
    invoke-virtual {p2, v2}, LX/3sp;->c(Z)V

    .line 1829075
    if-eqz v1, :cond_3

    .line 1829076
    invoke-virtual {v5, v7}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v0

    .line 1829077
    :goto_2
    sget-object v1, LX/BtO;->e:Landroid/graphics/Path;

    invoke-virtual {v5, v6, v0, v1}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 1829078
    sget-object v0, LX/BtO;->e:Landroid/graphics/Path;

    sget-object v1, LX/BtO;->d:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1829079
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    sget-object v1, LX/BtO;->b:[I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLocationOnScreen([I)V

    .line 1829080
    sget-object v0, LX/BtO;->b:[I

    aget v1, v0, v3

    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v1, v0

    .line 1829081
    sget-object v0, LX/BtO;->b:[I

    aget v3, v0, v2

    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v3

    .line 1829082
    sget-object v3, LX/BtO;->c:Landroid/graphics/Rect;

    sget-object v4, LX/BtO;->d:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    add-int/2addr v4, v1

    sget-object v5, LX/BtO;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    add-int/2addr v5, v0

    sget-object v6, LX/BtO;->d:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    add-int/2addr v1, v6

    sget-object v6, LX/BtO;->d:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    add-int/2addr v0, v6

    invoke-virtual {v3, v4, v5, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1829083
    sget-object v0, LX/BtO;->c:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 1829084
    invoke-virtual {p2, v2}, LX/3sp;->a(Z)V

    .line 1829085
    invoke-virtual {p2, v2}, LX/3sp;->f(Z)V

    .line 1829086
    invoke-virtual {p2, v2}, LX/3sp;->h(Z)V

    goto/16 :goto_0

    :cond_2
    move v1, v3

    .line 1829087
    goto :goto_1

    :cond_3
    move v0, v4

    goto :goto_2
.end method

.method public final a(LX/3sp;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1829088
    iget-object v0, p0, LX/556;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1829089
    if-nez v0, :cond_1

    .line 1829090
    :cond_0
    return-void

    .line 1829091
    :cond_1
    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1829092
    instance-of v0, v1, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1829093
    check-cast v0, Landroid/text/Spanned;

    .line 1829094
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const-class v3, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v2, v1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 1829095
    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 1829096
    iget-object v5, p0, LX/556;->a:Landroid/view/View;

    invoke-interface {v0, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v5, v4}, LX/3sp;->c(Landroid/view/View;I)V

    .line 1829097
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
