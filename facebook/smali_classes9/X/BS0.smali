.class public LX/BS0;
.super LX/AhO;
.source ""


# instance fields
.field private final a:LX/AhN;

.field public b:LX/EpP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1784633
    invoke-direct {p0, p1}, LX/AhO;-><init>(Landroid/content/Context;)V

    .line 1784634
    new-instance v0, LX/BRz;

    invoke-direct {v0, p0}, LX/BRz;-><init>(LX/BS0;)V

    iput-object v0, p0, LX/BS0;->a:LX/AhN;

    .line 1784635
    invoke-direct {p0}, LX/BS0;->a()V

    .line 1784636
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1784637
    invoke-direct {p0, p1, p2}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1784638
    new-instance v0, LX/BRz;

    invoke-direct {v0, p0}, LX/BRz;-><init>(LX/BS0;)V

    iput-object v0, p0, LX/BS0;->a:LX/AhN;

    .line 1784639
    invoke-direct {p0}, LX/BS0;->a()V

    .line 1784640
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1784641
    invoke-direct {p0, p1, p2, p3}, LX/AhO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1784642
    new-instance v0, LX/BRz;

    invoke-direct {v0, p0}, LX/BRz;-><init>(LX/BS0;)V

    iput-object v0, p0, LX/BS0;->a:LX/AhN;

    .line 1784643
    invoke-direct {p0}, LX/BS0;->a()V

    .line 1784644
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1784645
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 1784646
    iget-object v0, p0, LX/BS0;->a:LX/AhN;

    .line 1784647
    iput-object v0, p0, LX/AhO;->b:LX/AhN;

    .line 1784648
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1}, LX/AhO;->a(ZZI)V

    .line 1784649
    return-void
.end method
