.class public final LX/B86;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:LX/B8A;


# direct methods
.method public constructor <init>(LX/B8A;)V
    .locals 0

    .prologue
    .line 1748433
    iput-object p1, p0, LX/B86;->a:LX/B8A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 1748434
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 1748435
    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1748436
    iget-object v0, p0, LX/B86;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1748437
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, "()-."

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1748438
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/B86;->a:LX/B8A;

    iget-object v2, v2, LX/B8A;->m:LX/7Tl;

    iget-object v2, v2, LX/7Tl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1748439
    iget-object v1, p0, LX/B86;->a:LX/B8A;

    iget-object v1, v1, LX/B8A;->c:LX/B8r;

    invoke-virtual {v1, v0}, LX/B8r;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748440
    iget-object v0, p0, LX/B86;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->n:LX/B7F;

    iget-object v1, p0, LX/B86;->a:LX/B8A;

    iget-object v1, v1, LX/B8A;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-static {v1}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v0

    .line 1748441
    iget-object v1, p0, LX/B86;->a:LX/B8A;

    invoke-virtual {v1, v0}, LX/B8A;->a(Ljava/lang/String;)V

    .line 1748442
    iget-object v0, p0, LX/B86;->a:LX/B8A;

    invoke-virtual {v0}, LX/B8A;->b()V

    .line 1748443
    const/4 v0, 0x1

    .line 1748444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
