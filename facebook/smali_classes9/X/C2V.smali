.class public final LX/C2V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1844378
    iput-object p1, p0, LX/C2V;->b:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    iput-object p2, p0, LX/C2V;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x23a10fb0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1844379
    iget-object v1, p0, LX/C2V;->b:Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;

    iget-object v2, p0, LX/C2V;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    .line 1844380
    sget-object v4, LX/21D;->NEWSFEED:LX/21D;

    const-string v5, "linkShareActionButton"

    invoke-static {v2}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v6

    invoke-virtual {v6}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    .line 1844381
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const-string v5, "newsfeed_composer"

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    .line 1844382
    iget-object v5, v1, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->b:LX/1Kf;

    const/4 v6, 0x0

    const/16 p0, 0x6dc

    iget-object p1, v1, Lcom/facebook/feedplugins/calltoaction/LinkShareActionButtonPartDefinition;->a:Landroid/app/Activity;

    invoke-interface {v5, v6, v4, p0, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1844383
    const v1, -0x51e05dd0

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
