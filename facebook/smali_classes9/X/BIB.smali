.class public final LX/BIB;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/BIC;


# direct methods
.method public constructor <init>(LX/BIC;)V
    .locals 0

    .prologue
    .line 1770033
    iput-object p1, p0, LX/BIB;->a:LX/BIC;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 2

    .prologue
    .line 1770037
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v1, v0

    .line 1770038
    iget-object v0, p0, LX/BIB;->a:LX/BIC;

    iget-object v0, v0, LX/BIC;->e:LX/BIF;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1770039
    iget-object v0, p0, LX/BIB;->a:LX/BIC;

    iget-object v0, v0, LX/BIC;->e:LX/BIF;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1770040
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 1770034
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide v2, 0x3fef5c28f5c28f5cL    # 0.98

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1770035
    iget-object v0, p0, LX/BIB;->a:LX/BIC;

    iget-object v0, v0, LX/BIC;->c:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    iget-object v1, p0, LX/BIB;->a:LX/BIC;

    iget-object v1, v1, LX/BIC;->e:LX/BIF;

    invoke-interface {v1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    .line 1770036
    :cond_0
    return-void
.end method
