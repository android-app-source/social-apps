.class public final LX/Byr;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bys;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:LX/Bys;


# direct methods
.method public constructor <init>(LX/Bys;)V
    .locals 1

    .prologue
    .line 1837926
    iput-object p1, p0, LX/Byr;->b:LX/Bys;

    .line 1837927
    move-object v0, p1

    .line 1837928
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1837929
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1837925
    const-string v0, "VideoViewCountTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1837914
    if-ne p0, p1, :cond_1

    .line 1837915
    :cond_0
    :goto_0
    return v0

    .line 1837916
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1837917
    goto :goto_0

    .line 1837918
    :cond_3
    check-cast p1, LX/Byr;

    .line 1837919
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1837920
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1837921
    if-eq v2, v3, :cond_0

    .line 1837922
    iget-object v2, p0, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1837923
    goto :goto_0

    .line 1837924
    :cond_4
    iget-object v2, p1, LX/Byr;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
