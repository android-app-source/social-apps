.class public LX/BN6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BN6;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BMr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BMu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/BMr;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BMu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778355
    iput-object p1, p0, LX/BN6;->a:LX/0Or;

    .line 1778356
    iput-object p2, p0, LX/BN6;->b:LX/0Or;

    .line 1778357
    return-void
.end method

.method public static a(LX/0QB;)LX/BN6;
    .locals 5

    .prologue
    .line 1778358
    sget-object v0, LX/BN6;->c:LX/BN6;

    if-nez v0, :cond_1

    .line 1778359
    const-class v1, LX/BN6;

    monitor-enter v1

    .line 1778360
    :try_start_0
    sget-object v0, LX/BN6;->c:LX/BN6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778361
    if-eqz v2, :cond_0

    .line 1778362
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778363
    new-instance v3, LX/BN6;

    const/16 v4, 0x3017

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x3018

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/BN6;-><init>(LX/0Or;LX/0Or;)V

    .line 1778364
    move-object v0, v3

    .line 1778365
    sput-object v0, LX/BN6;->c:LX/BN6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778366
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778367
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778368
    :cond_1
    sget-object v0, LX/BN6;->c:LX/BN6;

    return-object v0

    .line 1778369
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778370
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
