.class public LX/CGA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1865001
    const-class v0, LX/CGA;

    sput-object v0, LX/CGA;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1864965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864966
    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/verve/model/VMDeck;I)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1864990
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864991
    iget-object v1, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    if-eqz v1, :cond_2

    .line 1864992
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1864993
    invoke-static {}, LX/31M;->values()[LX/31M;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v7, v5, v3

    .line 1864994
    invoke-static {v7}, LX/CGA;->a(LX/31M;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1864995
    :try_start_0
    invoke-static {p0, p1, v7}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;ILX/31M;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1864996
    invoke-virtual {v7}, LX/31M;->flag()I

    move-result v7

    or-int/2addr v1, v7

    .line 1864997
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1864998
    :catch_0
    move-exception v7

    .line 1864999
    sget-object v8, LX/CGA;->a:Ljava/lang/Class;

    const-string v9, "Unable to get new slide for swipe"

    invoke-static {v8, v9, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1865000
    :cond_3
    return v1
.end method

.method public static a(Lcom/facebook/greetingcards/verve/model/VMDeck;ILX/31M;)I
    .locals 4

    .prologue
    .line 1864980
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864981
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    invoke-static {p2}, LX/CGA;->a(LX/31M;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMAction;

    .line 1864982
    const-string v1, "select-slide"

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1864983
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMAction;->arg:Ljava/lang/String;

    invoke-static {p0, v0}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;Ljava/lang/String;)I

    move-result v0

    .line 1864984
    :goto_0
    return v0

    .line 1864985
    :cond_0
    const-string v1, "previous-slide"

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1864986
    add-int/lit8 v0, p1, -0x1

    goto :goto_0

    .line 1864987
    :cond_1
    const-string v1, "next-slide"

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1864988
    add-int/lit8 v0, p1, 0x1

    goto :goto_0

    .line 1864989
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized action type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Lcom/facebook/greetingcards/verve/model/VMDeck;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1864974
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1864975
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864976
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1864977
    return v1

    .line 1864978
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1864979
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No such slideName: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/31M;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1864967
    sget-object v0, LX/CG9;->a:[I

    invoke-virtual {p0}, LX/31M;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1864968
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid direction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1864969
    :pswitch_0
    const-string v0, "swipe-left"

    .line 1864970
    :goto_0
    return-object v0

    .line 1864971
    :pswitch_1
    const-string v0, "swipe-right"

    goto :goto_0

    .line 1864972
    :pswitch_2
    const-string v0, "swipe-up"

    goto :goto_0

    .line 1864973
    :pswitch_3
    const-string v0, "swipe-down"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
