.class public final LX/Aol;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Z

.field public final synthetic g:Landroid/app/Activity;

.field public final synthetic h:LX/5OG;

.field public final synthetic i:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;ZLandroid/app/Activity;LX/5OG;)V
    .locals 0

    .prologue
    .line 1714826
    iput-object p1, p0, LX/Aol;->i:LX/Aov;

    iput-object p2, p0, LX/Aol;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aol;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object p4, p0, LX/Aol;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p6, p0, LX/Aol;->e:Ljava/lang/String;

    iput-boolean p7, p0, LX/Aol;->f:Z

    iput-object p8, p0, LX/Aol;->g:Landroid/app/Activity;

    iput-object p9, p0, LX/Aol;->h:LX/5OG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14

    .prologue
    const/4 v7, 0x1

    .line 1714747
    iget-object v0, p0, LX/Aol;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1714748
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1714749
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714750
    iget-object v0, p0, LX/Aol;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_3

    .line 1714751
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aol;->c:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aol;->i:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714752
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714753
    iget-object v4, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aol;->e:Ljava/lang/String;

    .line 1714754
    iget-object v8, v0, LX/1EQ;->a:LX/0Zb;

    const-string v9, "feed_share_action"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v8

    .line 1714755
    invoke-virtual {v8}, LX/0oG;->a()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1714756
    invoke-virtual {v8, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "share_type"

    const-string v11, "quick_share_public"

    invoke-virtual {v9, v10, v11}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "story_id"

    invoke-virtual {v9, v10, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "user_id"

    invoke-virtual {v9, v10, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "shareable_id"

    invoke-virtual {v9, v10, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714757
    invoke-virtual {v8}, LX/0oG;->d()V

    .line 1714758
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714759
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1714760
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1714761
    new-instance v4, LX/5M9;

    invoke-direct {v4}, LX/5M9;-><init>()V

    .line 1714762
    iget-object v0, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-static {v0}, LX/2cA;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1714763
    iput-boolean v7, v4, LX/5M9;->Q:Z

    .line 1714764
    :cond_1
    iget-object v0, p0, LX/Aol;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1714765
    if-eqz v0, :cond_5

    .line 1714766
    :goto_1
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v1

    .line 1714767
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 1714768
    iput-object v5, v1, LX/21A;->a:LX/162;

    .line 1714769
    iget-object v5, p0, LX/Aol;->i:LX/Aov;

    iget-object v5, v5, LX/Aov;->b:LX/1EQ;

    .line 1714770
    iget-object v8, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v8

    .line 1714771
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v5, v0, v1}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 1714772
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v5

    .line 1714773
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->r:LX/0pf;

    iget-object v1, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v8

    .line 1714774
    if-eqz v8, :cond_7

    .line 1714775
    iget-object v0, v8, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 1714776
    if-eqz v0, :cond_7

    .line 1714777
    iget-object v0, v8, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 1714778
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1714779
    iget-object v0, v8, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 1714780
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1714781
    :goto_2
    iget-object v9, p0, LX/Aol;->i:LX/Aov;

    iget-object v9, v9, LX/Aov;->m:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, LX/5M9;->d(J)LX/5M9;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, LX/5M9;->a(J)LX/5M9;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/5M9;->c(J)LX/5M9;

    move-result-object v0

    iget-object v1, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 1714782
    iput-object v1, v0, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1714783
    move-object v0, v0

    .line 1714784
    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1714785
    iput-object v1, v0, LX/5M9;->m:Ljava/lang/String;

    .line 1714786
    move-object v0, v0

    .line 1714787
    iget-object v1, p0, LX/Aol;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v1

    .line 1714788
    iput-object v1, v0, LX/5M9;->h:Ljava/lang/String;

    .line 1714789
    move-object v0, v0

    .line 1714790
    iget-object v1, p0, LX/Aol;->e:Ljava/lang/String;

    .line 1714791
    iput-object v1, v0, LX/5M9;->G:Ljava/lang/String;

    .line 1714792
    move-object v1, v0

    .line 1714793
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-boolean v0, v0, LX/Aov;->o:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/Aol;->f:Z

    if-eqz v0, :cond_6

    move v0, v7

    .line 1714794
    :goto_3
    iput-boolean v0, v1, LX/5M9;->ad:Z

    .line 1714795
    move-object v0, v1

    .line 1714796
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1714797
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1714798
    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1714799
    if-eqz v8, :cond_2

    .line 1714800
    iget-object v0, v8, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 1714801
    if-eqz v0, :cond_2

    .line 1714802
    iget-object v0, v8, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v0

    .line 1714803
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 1714804
    if-eqz v0, :cond_2

    .line 1714805
    const-string v0, "extra_actor_viewer_context"

    .line 1714806
    iget-object v2, v8, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v2, v2

    .line 1714807
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1714808
    :cond_2
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1714809
    iget-object v2, p0, LX/Aol;->g:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Context;)V

    .line 1714810
    new-instance v2, LX/Aok;

    invoke-direct {v2, p0, v6}, LX/Aok;-><init>(LX/Aol;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1714811
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nT;

    iget-object v1, p0, LX/Aol;->h:LX/5OG;

    .line 1714812
    iget-object v2, v1, LX/5OG;->c:Landroid/content/Context;

    move-object v1, v2

    .line 1714813
    const v2, 0x7f081412

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1714814
    return v7

    .line 1714815
    :cond_3
    iget-object v0, p0, LX/Aol;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_0

    .line 1714816
    iget-object v0, p0, LX/Aol;->i:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aol;->c:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aol;->i:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714817
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714818
    iget-object v4, p0, LX/Aol;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aol;->e:Ljava/lang/String;

    .line 1714819
    iget-object v8, v0, LX/1EQ;->a:LX/0Zb;

    const-string v9, "feed_share_action"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v8

    .line 1714820
    invoke-virtual {v8}, LX/0oG;->a()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1714821
    invoke-virtual {v8, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "share_type"

    const-string v11, "quick_share_friends"

    invoke-virtual {v9, v10, v11}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "story_id"

    invoke-virtual {v9, v10, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "user_id"

    invoke-virtual {v9, v10, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    const-string v10, "shareable_id"

    invoke-virtual {v9, v10, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714822
    invoke-virtual {v8}, LX/0oG;->d()V

    .line 1714823
    :cond_4
    goto/16 :goto_0

    .line 1714824
    :cond_5
    iget-object v0, p0, LX/Aol;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto/16 :goto_1

    .line 1714825
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_7
    move-wide v0, v2

    goto/16 :goto_2
.end method
