.class public LX/CBy;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/24a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/button/FigButton;

.field public b:Lcom/facebook/fbui/glyph/GlyphView;

.field public c:Landroid/widget/LinearLayout;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1856914
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1856915
    const p1, 0x7f0310bf

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1856916
    const p1, 0x7f0d0bbc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, LX/CBy;->c:Landroid/widget/LinearLayout;

    .line 1856917
    const p1, 0x7f0d27d1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p1, p0, LX/CBy;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1856918
    const p1, 0x7f0d0551

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigButton;

    iput-object p1, p0, LX/CBy;->a:Lcom/facebook/fig/button/FigButton;

    .line 1856919
    const p1, 0x7f0d27ce

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/CBy;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1856920
    const p1, 0x7f0d27cf

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/CBy;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1856921
    const p1, 0x7f0d27d2

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p1, p0, LX/CBy;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1856922
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 1856912
    iget-object v0, p0, LX/CBy;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1856913
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1856926
    iget-object v0, p0, LX/CBy;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1856927
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 1856923
    iget-object v1, p0, LX/CBy;->b:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1856924
    return-void

    .line 1856925
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
