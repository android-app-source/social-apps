.class public LX/CLq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/Object;


# instance fields
.field private final b:LX/3Ed;

.field private final c:LX/CLn;

.field private d:LX/CLh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1879547
    const-class v0, LX/CLq;

    sput-object v0, LX/CLq;->a:Ljava/lang/Class;

    .line 1879548
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/CLq;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3Ed;LX/CLn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879550
    iput-object p1, p0, LX/CLq;->b:LX/3Ed;

    .line 1879551
    iput-object p2, p0, LX/CLq;->c:LX/CLn;

    .line 1879552
    return-void
.end method

.method private static a(LX/CLq;LX/CLh;LX/CLh;)LX/CLh;
    .locals 11
    .param p0    # LX/CLq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/CLh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1879553
    if-nez p2, :cond_0

    .line 1879554
    :goto_0
    return-object p1

    .line 1879555
    :cond_0
    if-nez p1, :cond_1

    move-object p1, p2

    .line 1879556
    goto :goto_0

    .line 1879557
    :cond_1
    iget-object v0, p2, LX/CLh;->a:LX/CLs;

    .line 1879558
    iget-object v2, p1, LX/CLh;->a:LX/CLs;

    iget-object v2, v2, LX/CLs;->deliveryReceipts:Ljava/util/List;

    .line 1879559
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 1879560
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CLr;

    .line 1879561
    invoke-static {v1}, LX/CLn;->a(LX/CLr;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    invoke-interface {v3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1879562
    :cond_2
    move-object v2, v3

    .line 1879563
    iget-object v0, v0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879564
    invoke-static {v0}, LX/CLn;->a(LX/CLr;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    .line 1879565
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CLr;

    .line 1879566
    if-nez v1, :cond_3

    .line 1879567
    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1879568
    :cond_3
    iget-object v5, v1, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v5, v0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-gez v5, :cond_4

    .line 1879569
    iget-object v1, v1, LX/CLr;->messageIds:Ljava/util/List;

    .line 1879570
    :goto_3
    invoke-static {v1, v0}, LX/CLn;->a(Ljava/util/List;LX/CLr;)LX/CLr;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1879571
    :cond_4
    iget-object v0, v0, LX/CLr;->messageIds:Ljava/util/List;

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 1879572
    goto :goto_3

    .line 1879573
    :cond_5
    new-instance v0, LX/CLs;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/CLq;->b:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/CLs;-><init>(Ljava/util/List;Ljava/lang/Long;)V

    .line 1879574
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1879575
    iget-object v2, p1, LX/CLh;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1879576
    iget-object v2, p2, LX/CLh;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1879577
    new-instance p1, LX/CLh;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {p1, v0, v1}, LX/CLh;-><init>(LX/CLs;LX/0Px;)V

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/CLq;
    .locals 8

    .prologue
    .line 1879578
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1879579
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1879580
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1879581
    if-nez v1, :cond_0

    .line 1879582
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1879583
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1879584
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1879585
    sget-object v1, LX/CLq;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1879586
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1879587
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1879588
    :cond_1
    if-nez v1, :cond_4

    .line 1879589
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1879590
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1879591
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1879592
    new-instance p0, LX/CLq;

    invoke-static {v0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v1

    check-cast v1, LX/3Ed;

    invoke-static {v0}, LX/CLn;->a(LX/0QB;)LX/CLn;

    move-result-object v7

    check-cast v7, LX/CLn;

    invoke-direct {p0, v1, v7}, LX/CLq;-><init>(LX/3Ed;LX/CLn;)V

    .line 1879593
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1879594
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1879595
    if-nez v1, :cond_2

    .line 1879596
    sget-object v0, LX/CLq;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLq;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1879597
    :goto_1
    if-eqz v0, :cond_3

    .line 1879598
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1879599
    :goto_3
    check-cast v0, LX/CLq;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1879600
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1879601
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1879602
    :catchall_1
    move-exception v0

    .line 1879603
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1879604
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1879605
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1879606
    :cond_2
    :try_start_8
    sget-object v0, LX/CLq;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLq;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a(LX/CLs;)LX/CLh;
    .locals 2
    .param p1    # LX/CLs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1879607
    monitor-enter p0

    .line 1879608
    if-eqz p1, :cond_0

    .line 1879609
    :try_start_0
    new-instance v0, LX/CLh;

    .line 1879610
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1879611
    invoke-direct {v0, p1, v1}, LX/CLh;-><init>(LX/CLs;LX/0Px;)V

    .line 1879612
    :cond_0
    iget-object v1, p0, LX/CLq;->d:LX/CLh;

    invoke-static {p0, v1, v0}, LX/CLq;->a(LX/CLq;LX/CLh;LX/CLh;)LX/CLh;

    move-result-object v0

    .line 1879613
    const/4 v1, 0x0

    iput-object v1, p0, LX/CLq;->d:LX/CLh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1879614
    monitor-exit p0

    return-object v0

    .line 1879615
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CLh;)V
    .locals 3

    .prologue
    .line 1879616
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1879617
    iget-object v1, p1, LX/CLh;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    iget-object v2, p1, LX/CLh;->a:LX/CLs;

    iget-object v2, v2, LX/CLs;->batchId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1879618
    new-instance v1, LX/CLh;

    iget-object v2, p1, LX/CLh;->a:LX/CLs;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/CLh;-><init>(LX/CLs;LX/0Px;)V

    .line 1879619
    iget-object v0, p0, LX/CLq;->d:LX/CLh;

    invoke-static {p0, v0, v1}, LX/CLq;->a(LX/CLq;LX/CLh;LX/CLh;)LX/CLh;

    move-result-object v0

    iput-object v0, p0, LX/CLq;->d:LX/CLh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1879620
    monitor-exit p0

    return-void

    .line 1879621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1879622
    const/4 v0, 0x0

    iput-object v0, p0, LX/CLq;->d:LX/CLh;

    .line 1879623
    return-void
.end method
