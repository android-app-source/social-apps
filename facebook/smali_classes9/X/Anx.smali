.class public LX/Anx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/39c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39c",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/21T;


# direct methods
.method public constructor <init>(LX/39c;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1713136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1713137
    iput-object p1, p0, LX/Anx;->a:LX/39c;

    .line 1713138
    invoke-virtual {p2}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a()LX/21T;

    move-result-object v0

    iput-object v0, p0, LX/Anx;->b:LX/21T;

    .line 1713139
    return-void
.end method

.method public static a(LX/0QB;)LX/Anx;
    .locals 5

    .prologue
    .line 1713140
    const-class v1, LX/Anx;

    monitor-enter v1

    .line 1713141
    :try_start_0
    sget-object v0, LX/Anx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1713142
    sput-object v2, LX/Anx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1713143
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713144
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1713145
    new-instance p0, LX/Anx;

    invoke-static {v0}, LX/39c;->a(LX/0QB;)LX/39c;

    move-result-object v3

    check-cast v3, LX/39c;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-direct {p0, v3, v4}, LX/Anx;-><init>(LX/39c;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;)V

    .line 1713146
    move-object v0, p0

    .line 1713147
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1713148
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Anx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1713149
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1713150
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
