.class public final enum LX/AUi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AUi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AUi;

.field public static final enum CROSS:LX/AUi;

.field public static final enum INNER:LX/AUi;

.field public static final enum LEFT:LX/AUi;

.field public static final enum LEFT_OUTER:LX/AUi;

.field public static final enum STANDARD:LX/AUi;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1678319
    new-instance v0, LX/AUi;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v2}, LX/AUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUi;->STANDARD:LX/AUi;

    .line 1678320
    new-instance v0, LX/AUi;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LX/AUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUi;->LEFT:LX/AUi;

    .line 1678321
    new-instance v0, LX/AUi;

    const-string v1, "LEFT_OUTER"

    invoke-direct {v0, v1, v4}, LX/AUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUi;->LEFT_OUTER:LX/AUi;

    .line 1678322
    new-instance v0, LX/AUi;

    const-string v1, "INNER"

    invoke-direct {v0, v1, v5}, LX/AUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUi;->INNER:LX/AUi;

    .line 1678323
    new-instance v0, LX/AUi;

    const-string v1, "CROSS"

    invoke-direct {v0, v1, v6}, LX/AUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUi;->CROSS:LX/AUi;

    .line 1678324
    const/4 v0, 0x5

    new-array v0, v0, [LX/AUi;

    sget-object v1, LX/AUi;->STANDARD:LX/AUi;

    aput-object v1, v0, v2

    sget-object v1, LX/AUi;->LEFT:LX/AUi;

    aput-object v1, v0, v3

    sget-object v1, LX/AUi;->LEFT_OUTER:LX/AUi;

    aput-object v1, v0, v4

    sget-object v1, LX/AUi;->INNER:LX/AUi;

    aput-object v1, v0, v5

    sget-object v1, LX/AUi;->CROSS:LX/AUi;

    aput-object v1, v0, v6

    sput-object v0, LX/AUi;->$VALUES:[LX/AUi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1678318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AUi;
    .locals 1

    .prologue
    .line 1678326
    const-class v0, LX/AUi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AUi;

    return-object v0
.end method

.method public static values()[LX/AUi;
    .locals 1

    .prologue
    .line 1678325
    sget-object v0, LX/AUi;->$VALUES:[LX/AUi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AUi;

    return-object v0
.end method
