.class public final LX/BKA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/text/SpannableStringBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772670
    iput-object p1, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1772671
    iget-object v0, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v1, 0x1

    .line 1772672
    iput-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aj:Z

    .line 1772673
    iget-object v0, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1772674
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1772675
    invoke-direct {p0}, LX/BKA;->a()V

    .line 1772676
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772677
    check-cast p1, Landroid/text/SpannableStringBuilder;

    .line 1772678
    if-nez p1, :cond_0

    .line 1772679
    invoke-direct {p0}, LX/BKA;->a()V

    .line 1772680
    :goto_0
    return-void

    .line 1772681
    :cond_0
    iget-object v0, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    invoke-virtual {v0, p1}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->a(Landroid/text/SpannableStringBuilder;)V

    .line 1772682
    iget-object v0, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v1, 0x1

    .line 1772683
    iput-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aj:Z

    .line 1772684
    iget-object v0, p0, LX/BKA;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_0
.end method
