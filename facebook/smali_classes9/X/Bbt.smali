.class public LX/Bbt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field private final c:LX/0sa;

.field public final d:LX/0rq;

.field public final e:LX/0TD;

.field private final f:LX/8uo;

.field public final g:LX/3LP;

.field public final h:LX/2RQ;

.field public final i:LX/3Oq;

.field public final j:LX/1Ck;

.field public final k:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1801261
    const-class v0, LX/Bbt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bbt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0sa;LX/0rq;LX/0TD;LX/8uo;LX/3LP;LX/2RQ;LX/3Oq;LX/1Ck;LX/03V;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1801262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801263
    iput-object p1, p0, LX/Bbt;->b:LX/0tX;

    .line 1801264
    iput-object p2, p0, LX/Bbt;->c:LX/0sa;

    .line 1801265
    iput-object p3, p0, LX/Bbt;->d:LX/0rq;

    .line 1801266
    iput-object p4, p0, LX/Bbt;->e:LX/0TD;

    .line 1801267
    iput-object p5, p0, LX/Bbt;->f:LX/8uo;

    .line 1801268
    iput-object p7, p0, LX/Bbt;->h:LX/2RQ;

    .line 1801269
    iput-object p6, p0, LX/Bbt;->g:LX/3LP;

    .line 1801270
    iput-object p8, p0, LX/Bbt;->i:LX/3Oq;

    .line 1801271
    iput-object p9, p0, LX/Bbt;->j:LX/1Ck;

    .line 1801272
    iput-object p10, p0, LX/Bbt;->k:LX/03V;

    .line 1801273
    return-void
.end method

.method public static b(LX/0QB;)LX/Bbt;
    .locals 11

    .prologue
    .line 1801274
    new-instance v0, LX/Bbt;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {p0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v5

    check-cast v5, LX/8uo;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v6

    check-cast v6, LX/3LP;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v7

    check-cast v7, LX/2RQ;

    invoke-static {p0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v8

    check-cast v8, LX/3Oq;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-direct/range {v0 .. v10}, LX/Bbt;-><init>(LX/0tX;LX/0sa;LX/0rq;LX/0TD;LX/8uo;LX/3LP;LX/2RQ;LX/3Oq;LX/1Ck;LX/03V;)V

    .line 1801275
    return-object v0
.end method
