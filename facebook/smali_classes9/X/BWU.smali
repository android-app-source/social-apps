.class public LX/BWU;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/BWU;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;)V
    .locals 9
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/webview/IsInWebViewLinkShareNativeComposer;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/webview/IsInBrowserLinkShareNativeComposer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1792111
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1792112
    iput-object p1, p0, LX/BWU;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1792113
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1792114
    :cond_0
    const-string v0, "composer/?%s=<share_link>&%s=<app_id>&%s=true&%s=<in_app>&%s=null&%s=null&%s=null&%s=null&%s=null&%s=<next>&%s=<host_url>"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "link"

    aput-object v2, v1, v4

    const-string v2, "app_id"

    aput-object v2, v1, v5

    const-string v2, "is_web_share"

    aput-object v2, v1, v6

    const-string v2, "is_in_app_web_share"

    aput-object v2, v1, v7

    const-string v2, "name"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "caption"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "picture"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "quote"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "next"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "host_url"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1792115
    const-string v1, "dialog/share_open_graph?href={share_link}&app_id={#app_id 0}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792116
    const-string v1, "sharer?u={share_link}&app_id={#app_id 0}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792117
    const-string v1, "sharer.php?u={share_link}&app_id={#app_id 0}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792118
    const-string v1, "sharer/sharer.php?u={share_link}&app_id={#app_id 0}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792119
    const-string v1, "sharer/sharer.php?p[url]={share_link}&p[app_id]={#app_id 0}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792120
    const-string v0, "composer/?%s=<share_link>&%s=<app_id>&%s=true&%s=<in_app>&%s=null&%s=null&%s=null&%s=null&%s=<quote>&%s=<next>&%s=<host_url>"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "link"

    aput-object v2, v1, v4

    const-string v2, "app_id"

    aput-object v2, v1, v5

    const-string v2, "is_web_share"

    aput-object v2, v1, v6

    const-string v2, "is_in_app_web_share"

    aput-object v2, v1, v7

    const-string v2, "name"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "caption"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "picture"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "quote"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "next"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "host_url"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1792121
    const-string v1, "dialog/share?href={share_link}&app_id={#app_id 0}&in_app={in_app false}&quote={quote null}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792122
    const-string v0, "composer/?%s=<share_link>&%s=<app_id>&%s=<name>&%s=<caption>&%s=<description>&%s=<picture>&%s=null&%s=true&%s=<in_app>&%s=<next>&%s=<host_url>"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "link"

    aput-object v2, v1, v4

    const-string v2, "app_id"

    aput-object v2, v1, v5

    const-string v2, "name"

    aput-object v2, v1, v6

    const-string v2, "caption"

    aput-object v2, v1, v7

    const-string v2, "description"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "picture"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "quote"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "is_web_share"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "is_in_app_web_share"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "next"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "host_url"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1792123
    const-string v1, "dialog/feed?link={share_link}&app_id={#app_id 0}&name={name null}&caption={caption null}&description={description null}&picture={picture null}&in_app={in_app false}&next={next null}&host_url={host_url null}"

    invoke-direct {p0, v1, v0}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792124
    :cond_1
    const-string v0, "marketplace"

    sget-object v1, LX/0ax;->hx:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792125
    const-string v0, "marketplace/?referralSurface={referralSurface}"

    sget-object v1, LX/0ax;->hx:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/BWU;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792126
    sget-object v0, LX/0ax;->hy:Ljava/lang/String;

    const-string v1, "{referralSurface}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->hx:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792127
    return-void
.end method

.method public static a(LX/0QB;)LX/BWU;
    .locals 6

    .prologue
    .line 1792098
    sget-object v0, LX/BWU;->b:LX/BWU;

    if-nez v0, :cond_1

    .line 1792099
    const-class v1, LX/BWU;

    monitor-enter v1

    .line 1792100
    :try_start_0
    sget-object v0, LX/BWU;->b:LX/BWU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1792101
    if-eqz v2, :cond_0

    .line 1792102
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1792103
    new-instance v4, LX/BWU;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x159f

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x159e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/BWU;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0Or;)V

    .line 1792104
    move-object v0, v4

    .line 1792105
    sput-object v0, LX/BWU;->b:LX/BWU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1792106
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1792107
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1792108
    :cond_1
    sget-object v0, LX/BWU;->b:LX/BWU;

    return-object v0

    .line 1792109
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1792110
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1792088
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792089
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://m.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792090
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792091
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792092
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://m.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792093
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792094
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1792095
    if-eqz v0, :cond_0

    .line 1792096
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://our.intern.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792097
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1792064
    if-eqz p2, :cond_4

    .line 1792065
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1792066
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "in_app"

    const-string v3, "true"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 1792067
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1792068
    const-string v0, "host_url"

    invoke-virtual {v3, v0, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1792069
    :cond_0
    const/4 v0, 0x0

    .line 1792070
    const-string v1, "next"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1792071
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1792072
    :try_start_0
    const-string v4, "UTF-8"

    invoke-static {v1, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1792073
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1792074
    :try_start_1
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    .line 1792075
    if-nez v1, :cond_6
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1792076
    :cond_1
    :goto_0
    move-object v0, v0

    .line 1792077
    if-nez v0, :cond_5

    .line 1792078
    const-string v0, ""

    move-object v1, v0

    .line 1792079
    :goto_1
    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1792080
    const-string v5, "next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1792081
    const-string v0, "next"

    invoke-virtual {v3, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 1792082
    :cond_2
    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 1792083
    :cond_3
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1792084
    :cond_4
    invoke-virtual {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_5
    move-object v1, v0

    goto :goto_1

    .line 1792085
    :cond_6
    :try_start_2
    const-string v4, "UTF-8"

    invoke-static {v1, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1792086
    :catch_0
    goto :goto_0

    .line 1792087
    :catch_1
    goto :goto_0
.end method
