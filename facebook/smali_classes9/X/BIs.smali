.class public final LX/BIs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 0

    .prologue
    .line 1770988
    iput-object p1, p0, LX/BIs;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1770989
    iget-object v0, p0, LX/BIs;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v0

    .line 1770990
    iget-object v1, v0, LX/9ip;->c:LX/9j0;

    .line 1770991
    iget-object v2, v1, LX/9j0;->f:LX/9ij;

    if-eqz v2, :cond_1

    .line 1770992
    invoke-virtual {v1}, LX/9j0;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    iget-object v4, v1, LX/9j0;->f:LX/9ij;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1770993
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1770994
    const/4 v7, 0x2

    new-array v7, v7, [I

    .line 1770995
    invoke-virtual {v4, v7}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1770996
    aget v8, v7, v0

    aget v9, v7, v1

    aget v10, v7, v0

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result p1

    add-int/2addr v10, p1

    aget v7, v7, v1

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result p1

    add-int/2addr v7, p1

    invoke-virtual {v6, v8, v9, v10, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1770997
    goto :goto_0

    .line 1770998
    :goto_0
    move-object v2, v6

    .line 1770999
    :goto_1
    move-object v1, v2

    .line 1771000
    move-object v0, v1

    .line 1771001
    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1771002
    iget-object v0, p0, LX/BIs;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v0

    invoke-virtual {v0}, LX/9ip;->l()V

    .line 1771003
    iget-object v0, p0, LX/BIs;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    const/4 v1, 0x1

    .line 1771004
    iput-boolean v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->r:Z

    .line 1771005
    :goto_2
    return v3

    .line 1771006
    :cond_0
    iget-object v0, p0, LX/BIs;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1771007
    iput-boolean v3, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->r:Z

    .line 1771008
    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
