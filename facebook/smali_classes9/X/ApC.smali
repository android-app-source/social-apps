.class public final LX/ApC;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/ApC;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApA;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/ApD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715499
    const/4 v0, 0x0

    sput-object v0, LX/ApC;->a:LX/ApC;

    .line 1715500
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApC;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1715501
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715502
    new-instance v0, LX/ApD;

    invoke-direct {v0}, LX/ApD;-><init>()V

    iput-object v0, p0, LX/ApC;->c:LX/ApD;

    .line 1715503
    return-void
.end method

.method public static declared-synchronized q()LX/ApC;
    .locals 2

    .prologue
    .line 1715504
    const-class v1, LX/ApC;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/ApC;->a:LX/ApC;

    if-nez v0, :cond_0

    .line 1715505
    new-instance v0, LX/ApC;

    invoke-direct {v0}, LX/ApC;-><init>()V

    sput-object v0, LX/ApC;->a:LX/ApC;

    .line 1715506
    :cond_0
    sget-object v0, LX/ApC;->a:LX/ApC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1715507
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715508
    invoke-static {}, LX/1dS;->b()V

    .line 1715509
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1715510
    check-cast p4, LX/ApB;

    .line 1715511
    iget-object v3, p4, LX/ApB;->a:Ljava/lang/CharSequence;

    iget-object v4, p4, LX/ApB;->b:Ljava/lang/CharSequence;

    iget-object v5, p4, LX/ApB;->c:Ljava/lang/CharSequence;

    move-object v0, p1

    move v1, p2

    move v2, p3

    const/4 p1, 0x0

    const/4 p0, 0x0

    const/4 v7, 0x1

    .line 1715512
    sget v6, LX/ApD;->a:I

    const/4 p2, 0x2

    invoke-static {v0, v3, v6, p2}, LX/ApD;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v6

    .line 1715513
    if-nez v6, :cond_2

    move-object p2, p1

    .line 1715514
    :goto_0
    if-eqz p2, :cond_1

    const/4 v6, 0x1

    const/4 p3, 0x0

    .line 1715515
    invoke-static {v1}, LX/1mh;->a(I)I

    move-result p4

    .line 1715516
    if-nez p4, :cond_6

    .line 1715517
    :cond_0
    :goto_1
    move v6, v6

    .line 1715518
    if-eqz v6, :cond_3

    :cond_1
    move v6, v7

    .line 1715519
    :goto_2
    if-eqz v6, :cond_4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    .line 1715520
    :goto_3
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p3

    invoke-interface {p3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p0

    const/4 p3, 0x4

    invoke-interface {p0, p3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object p0

    if-eqz v6, :cond_5

    sget v6, LX/ApD;->b:I

    invoke-static {v0, v4, v6, v7}, LX/ApD;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v6

    :goto_4
    invoke-interface {p0, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    sget p0, LX/ApD;->c:I

    invoke-static {v0, v5, p0, v7}, LX/ApD;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x7

    const p0, 0x7f0b1169

    invoke-interface {v6, v7, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1715521
    return-object v0

    .line 1715522
    :cond_2
    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    move-object p2, v6

    goto :goto_0

    :cond_3
    move v6, p0

    .line 1715523
    goto :goto_2

    :cond_4
    move v6, p0

    .line 1715524
    goto :goto_3

    :cond_5
    move-object v6, p1

    .line 1715525
    goto :goto_4

    .line 1715526
    :cond_6
    new-instance p4, LX/1no;

    invoke-direct {p4}, LX/1no;-><init>()V

    .line 1715527
    invoke-static {p3, p3}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {p2, v0, v3, v2, p4}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1715528
    iget p4, p4, LX/1no;->a:I

    invoke-static {v1}, LX/1mh;->b(I)I

    move-result v3

    if-le p4, v3, :cond_0

    move v6, p3

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1715529
    const/4 v0, 0x1

    return v0
.end method
