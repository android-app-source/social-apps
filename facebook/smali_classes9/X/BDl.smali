.class public LX/BDl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/notifications/server/GetNotificationsSettingsParams;",
        "Lcom/facebook/notifications/protocol/methods/GetNotificationsSettingsResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1763801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763802
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1763803
    check-cast p1, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;

    .line 1763804
    new-instance v0, LX/3fG;

    invoke-direct {v0}, LX/3fG;-><init>()V

    .line 1763805
    const-string v1, "notificationssettings"

    const-string v2, "SELECT setting_id, setting_value FROM notification_setting WHERE user_id=%s AND type=\'%s\'"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1763806
    iget-object v5, p1, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1763807
    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 1763808
    iget-object v5, p1, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1763809
    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/3fG;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1763810
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1763811
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763812
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "queries"

    invoke-virtual {v0}, LX/3fG;->a()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763813
    new-instance v0, LX/14N;

    const-string v1, "getNotificationsSettings"

    const-string v2, "GET"

    const-string v3, "method/fql.multiquery"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1763814
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1763815
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1763816
    new-instance v1, LX/3fM;

    invoke-direct {v1, v0}, LX/3fM;-><init>(LX/0lF;)V

    .line 1763817
    const-string v0, "notificationssettings"

    invoke-virtual {v1, v0}, LX/3fM;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1763818
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1763819
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1763820
    sget-object v3, LX/461;->g:LX/461;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 1763821
    invoke-static {v0}, Lcom/facebook/notifications/model/FacebookNotificationSetting;->a(LX/15w;)Lcom/facebook/notifications/model/FacebookNotificationSetting;

    move-result-object v0

    .line 1763822
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1763823
    :cond_0
    new-instance v0, Lcom/facebook/notifications/protocol/methods/GetNotificationsSettingsResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v2, v1, v4, v5}, Lcom/facebook/notifications/protocol/methods/GetNotificationsSettingsResult;-><init>(LX/0ta;LX/0Px;J)V

    return-object v0
.end method
