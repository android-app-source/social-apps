.class public LX/CGC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/CFt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1865065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865066
    return-void
.end method

.method private static a(DFF)F
    .locals 4

    .prologue
    .line 1865064
    sub-float v0, p3, p2

    float-to-double v0, v0

    mul-double/2addr v0, p0

    float-to-double v2, p2

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private static a(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865062
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->a()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->a()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotation(F)V

    .line 1865063
    return-void
.end method

.method private static b(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865060
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->b()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->b()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1865061
    return-void
.end method

.method private static c(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865058
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->j()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->j()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setX(F)V

    .line 1865059
    return-void
.end method

.method private static d(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865056
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->k()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->k()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setY(F)V

    .line 1865057
    return-void
.end method

.method private static e(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865003
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->l()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->l()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 1865004
    return-void
.end method

.method private static f(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865054
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->m()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->m()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    .line 1865055
    return-void
.end method

.method private static g(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865052
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->a(Landroid/view/View;F)V

    .line 1865053
    return-void
.end method

.method private static h(Landroid/view/View;LX/CFr;D)V
    .locals 2

    .prologue
    .line 1865050
    iget-object v0, p1, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v0

    iget-object v1, p1, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v1

    invoke-static {p2, p3, v0, v1}, LX/CGC;->a(DFF)F

    move-result v0

    invoke-static {p0, v0}, LX/CGS;->b(Landroid/view/View;F)V

    .line 1865051
    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 7

    .prologue
    .line 1865005
    iget-object v0, p0, LX/CGC;->b:LX/CFt;

    iget-object v3, v0, LX/CFt;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_a

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFr;

    .line 1865006
    sget-object v1, LX/CGB;->a:[I

    iget-object v5, v0, LX/CFr;->a:LX/CFs;

    invoke-virtual {v5}, LX/CFs;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_0

    .line 1865007
    iget-object v1, p0, LX/CGC;->a:LX/0YU;

    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v5}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1865008
    invoke-static {v1, v0, p1, p2}, LX/CGC;->a(Landroid/view/View;LX/CFr;D)V

    .line 1865009
    invoke-static {v1, v0, p1, p2}, LX/CGC;->b(Landroid/view/View;LX/CFr;D)V

    .line 1865010
    invoke-static {v1, v0, p1, p2}, LX/CGC;->c(Landroid/view/View;LX/CFr;D)V

    .line 1865011
    invoke-static {v1, v0, p1, p2}, LX/CGC;->d(Landroid/view/View;LX/CFr;D)V

    .line 1865012
    invoke-static {v1, v0, p1, p2}, LX/CGC;->e(Landroid/view/View;LX/CFr;D)V

    .line 1865013
    invoke-static {v1, v0, p1, p2}, LX/CGC;->f(Landroid/view/View;LX/CFr;D)V

    .line 1865014
    invoke-static {v1, v0, p1, p2}, LX/CGC;->g(Landroid/view/View;LX/CFr;D)V

    .line 1865015
    invoke-static {v1, v0, p1, p2}, LX/CGC;->h(Landroid/view/View;LX/CFr;D)V

    :cond_0
    move-object v0, v1

    .line 1865016
    :goto_1
    instance-of v1, v0, LX/CGH;

    if-eqz v1, :cond_1

    .line 1865017
    check-cast v0, LX/CGH;

    .line 1865018
    iget-object v1, v0, LX/CGH;->a:LX/CGE;

    move-object v0, v1

    .line 1865019
    iget-object v1, v0, LX/CGE;->c:LX/CGC;

    invoke-virtual {v1, p1, p2}, LX/CGC;->a(D)V

    .line 1865020
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1865021
    :pswitch_0
    iget-object v1, p0, LX/CGC;->a:LX/0YU;

    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v5}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1865022
    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    if-eqz v5, :cond_2

    .line 1865023
    invoke-static {v1, v0, p1, p2}, LX/CGC;->a(Landroid/view/View;LX/CFr;D)V

    .line 1865024
    :cond_2
    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    if-eqz v5, :cond_3

    .line 1865025
    invoke-static {v1, v0, p1, p2}, LX/CGC;->b(Landroid/view/View;LX/CFr;D)V

    .line 1865026
    :cond_3
    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    if-eqz v5, :cond_4

    .line 1865027
    invoke-static {v1, v0, p1, p2}, LX/CGC;->c(Landroid/view/View;LX/CFr;D)V

    .line 1865028
    invoke-static {v1, v0, p1, p2}, LX/CGC;->d(Landroid/view/View;LX/CFr;D)V

    .line 1865029
    :cond_4
    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    if-eqz v5, :cond_5

    .line 1865030
    invoke-static {v1, v0, p1, p2}, LX/CGC;->e(Landroid/view/View;LX/CFr;D)V

    .line 1865031
    invoke-static {v1, v0, p1, p2}, LX/CGC;->f(Landroid/view/View;LX/CFr;D)V

    .line 1865032
    :cond_5
    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    if-eqz v5, :cond_0

    .line 1865033
    invoke-static {v1, v0, p1, p2}, LX/CGC;->g(Landroid/view/View;LX/CFr;D)V

    .line 1865034
    invoke-static {v1, v0, p1, p2}, LX/CGC;->h(Landroid/view/View;LX/CFr;D)V

    move-object v0, v1

    goto :goto_1

    .line 1865035
    :pswitch_1
    iget-object v1, p0, LX/CGC;->a:LX/0YU;

    iget-object v5, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v5}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1865036
    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    if-eqz v5, :cond_6

    .line 1865037
    invoke-static {v1, v0, p1, p2}, LX/CGC;->a(Landroid/view/View;LX/CFr;D)V

    .line 1865038
    :cond_6
    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    if-eqz v5, :cond_7

    .line 1865039
    invoke-static {v1, v0, p1, p2}, LX/CGC;->b(Landroid/view/View;LX/CFr;D)V

    .line 1865040
    :cond_7
    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    if-eqz v5, :cond_8

    .line 1865041
    invoke-static {v1, v0, p1, p2}, LX/CGC;->c(Landroid/view/View;LX/CFr;D)V

    .line 1865042
    invoke-static {v1, v0, p1, p2}, LX/CGC;->d(Landroid/view/View;LX/CFr;D)V

    .line 1865043
    :cond_8
    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    if-eqz v5, :cond_9

    .line 1865044
    invoke-static {v1, v0, p1, p2}, LX/CGC;->e(Landroid/view/View;LX/CFr;D)V

    .line 1865045
    invoke-static {v1, v0, p1, p2}, LX/CGC;->f(Landroid/view/View;LX/CFr;D)V

    .line 1865046
    :cond_9
    iget-object v5, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v5, v5, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    if-eqz v5, :cond_0

    .line 1865047
    invoke-static {v1, v0, p1, p2}, LX/CGC;->g(Landroid/view/View;LX/CFr;D)V

    .line 1865048
    invoke-static {v1, v0, p1, p2}, LX/CGC;->h(Landroid/view/View;LX/CFr;D)V

    move-object v0, v1

    goto/16 :goto_1

    .line 1865049
    :cond_a
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
