.class public LX/AmR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AmR;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710831
    iput-object p1, p0, LX/AmR;->a:Landroid/content/Context;

    .line 1710832
    return-void
.end method

.method public static a(LX/0QB;)LX/AmR;
    .locals 4

    .prologue
    .line 1710817
    sget-object v0, LX/AmR;->b:LX/AmR;

    if-nez v0, :cond_1

    .line 1710818
    const-class v1, LX/AmR;

    monitor-enter v1

    .line 1710819
    :try_start_0
    sget-object v0, LX/AmR;->b:LX/AmR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1710820
    if-eqz v2, :cond_0

    .line 1710821
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1710822
    new-instance p0, LX/AmR;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/AmR;-><init>(Landroid/content/Context;)V

    .line 1710823
    move-object v0, p0

    .line 1710824
    sput-object v0, LX/AmR;->b:LX/AmR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1710825
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1710826
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1710827
    :cond_1
    sget-object v0, LX/AmR;->b:LX/AmR;

    return-object v0

    .line 1710828
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1710829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
