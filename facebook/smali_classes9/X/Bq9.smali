.class public LX/Bq9;
.super LX/1Cd;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1B5;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1BX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1824358
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 1824359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bq9;->a:Ljava/util/Map;

    .line 1824360
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1824361
    iput-object v0, p0, LX/Bq9;->c:LX/0Ot;

    .line 1824362
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1824363
    iput-object v0, p0, LX/Bq9;->d:LX/0Ot;

    .line 1824364
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1824365
    iput-object v0, p0, LX/Bq9;->e:LX/0Ot;

    .line 1824366
    return-void
.end method

.method public static a(LX/0QB;)LX/Bq9;
    .locals 4

    .prologue
    .line 1824367
    new-instance v0, LX/Bq9;

    invoke-direct {v0}, LX/Bq9;-><init>()V

    .line 1824368
    const/16 v1, 0x2e3

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x67c

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x678

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1824369
    iput-object v1, v0, LX/Bq9;->c:LX/0Ot;

    iput-object v2, v0, LX/Bq9;->d:LX/0Ot;

    iput-object v3, v0, LX/Bq9;->e:LX/0Ot;

    .line 1824370
    move-object v0, v0

    .line 1824371
    return-object v0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 1824372
    iget-object v0, p0, LX/Bq9;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 1824373
    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 1824374
    iget-object v1, p0, LX/Bq9;->a:Ljava/util/Map;

    iget-object v0, p0, LX/Bq9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1824375
    :cond_0
    :goto_0
    if-nez p2, :cond_1

    .line 1824376
    iget-object v0, p0, LX/Bq9;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1824377
    :cond_1
    return-void

    .line 1824378
    :cond_2
    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 1824379
    iget-object v0, p0, LX/Bq9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/Bq9;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 1824380
    iget-object v4, p0, LX/Bq9;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1B5;

    iget-object v6, p0, LX/Bq9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/Bq9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824381
    iget-object v7, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 1824382
    check-cast v7, Lcom/facebook/graphql/model/FeedUnit;

    const-string v10, "single_photo"

    const-string v4, "graphQLID"

    invoke-static {v4, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v11

    move-wide v8, v0

    invoke-virtual/range {v5 .. v11}, LX/1B5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/FeedUnit;JLjava/lang/String;LX/0P1;)V

    .line 1824383
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 4

    .prologue
    .line 1824384
    iget-object v0, p0, LX/Bq9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1824385
    :cond_0
    :goto_0
    return-void

    .line 1824386
    :cond_1
    const/4 v1, 0x0

    .line 1824387
    instance-of v0, p2, LX/1Rk;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 1824388
    :goto_1
    move-object v1, v0

    .line 1824389
    if-eqz v1, :cond_0

    .line 1824390
    invoke-static {p1}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v2

    .line 1824391
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/1Qr;->d()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0g8;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824392
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v3

    .line 1824393
    iget-object v0, p0, LX/Bq9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1BX;

    invoke-virtual {v0, v2, p1, v3}, LX/1BX;->a(LX/1Qr;LX/0g8;Landroid/view/View;)Z

    move-result v0

    .line 1824394
    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LX/Bq9;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1824395
    :cond_2
    check-cast p2, LX/1Rk;

    .line 1824396
    iget-object v0, p2, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 1824397
    iget-object v2, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v2

    .line 1824398
    instance-of v2, v0, LX/5kD;

    if-eqz v2, :cond_3

    .line 1824399
    check-cast v0, LX/5kD;

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1824400
    goto :goto_1
.end method

.method public final b(LX/0g8;)V
    .locals 3

    .prologue
    .line 1824401
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Bq9;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1824402
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1824403
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, LX/Bq9;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1824404
    :cond_0
    return-void
.end method
