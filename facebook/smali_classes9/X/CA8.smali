.class public LX/CA8;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/CA4;


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ImageView;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1854953
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1854954
    const v0, 0x7f030c5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1854955
    const v0, 0x7f0d1e6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CA8;->a:Landroid/widget/ImageView;

    .line 1854956
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CA8;->b:Landroid/widget/ImageView;

    .line 1854957
    const v0, 0x7f0d0b0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CA8;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1854958
    const v0, 0x7f0d1e69

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CA8;->d:Landroid/view/View;

    .line 1854959
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1854960
    iget-object v0, p0, LX/CA8;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1854961
    iget-object v0, p0, LX/CA8;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1854962
    return-void
.end method

.method public getOfflineHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1854963
    iget-object v0, p0, LX/CA8;->d:Landroid/view/View;

    return-object v0
.end method

.method public setErrorListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1854964
    iget-object v0, p0, LX/CA8;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1854965
    iget-object v0, p0, LX/CA8;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1854966
    return-void
.end method
