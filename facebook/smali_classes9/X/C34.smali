.class public final enum LX/C34;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C34;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C34;

.field public static final enum COMPACT_GROUPS_FEED:LX/C34;

.field public static final enum DENSE_SEARCH_STORIES:LX/C34;

.field public static final enum GROUP_RELATED_STORIES:LX/C34;

.field public static final enum HOT_CONVERSATION:LX/C34;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1845056
    new-instance v0, LX/C34;

    const-string v1, "HOT_CONVERSATION"

    invoke-direct {v0, v1, v2}, LX/C34;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C34;->HOT_CONVERSATION:LX/C34;

    .line 1845057
    new-instance v0, LX/C34;

    const-string v1, "DENSE_SEARCH_STORIES"

    invoke-direct {v0, v1, v3}, LX/C34;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    .line 1845058
    new-instance v0, LX/C34;

    const-string v1, "GROUP_RELATED_STORIES"

    invoke-direct {v0, v1, v4}, LX/C34;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    .line 1845059
    new-instance v0, LX/C34;

    const-string v1, "COMPACT_GROUPS_FEED"

    invoke-direct {v0, v1, v5}, LX/C34;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    .line 1845060
    const/4 v0, 0x4

    new-array v0, v0, [LX/C34;

    sget-object v1, LX/C34;->HOT_CONVERSATION:LX/C34;

    aput-object v1, v0, v2

    sget-object v1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    aput-object v1, v0, v3

    sget-object v1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    aput-object v1, v0, v4

    sget-object v1, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    aput-object v1, v0, v5

    sput-object v0, LX/C34;->$VALUES:[LX/C34;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1845061
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C34;
    .locals 1

    .prologue
    .line 1845062
    const-class v0, LX/C34;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C34;

    return-object v0
.end method

.method public static values()[LX/C34;
    .locals 1

    .prologue
    .line 1845063
    sget-object v0, LX/C34;->$VALUES:[LX/C34;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C34;

    return-object v0
.end method
