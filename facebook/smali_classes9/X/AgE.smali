.class public final LX/AgE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveReactionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AgH;


# direct methods
.method public constructor <init>(LX/AgH;)V
    .locals 0

    .prologue
    .line 1700430
    iput-object p1, p0, LX/AgE;->a:LX/AgH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1700431
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700432
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1700433
    if-eqz p1, :cond_0

    .line 1700434
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700435
    if-nez v0, :cond_1

    .line 1700436
    :cond_0
    :goto_0
    return-void

    .line 1700437
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700438
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveReactionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1700439
    iget-object v1, p0, LX/AgE;->a:LX/AgH;

    .line 1700440
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700441
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveReactionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveReactionsQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;

    move-result-object v0

    invoke-static {v1, v0}, LX/AgH;->a$redex0(LX/AgH;Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$LiveReactionsFeedbackFragmentModel;)V

    goto :goto_0

    .line 1700442
    :cond_2
    goto :goto_0
.end method
