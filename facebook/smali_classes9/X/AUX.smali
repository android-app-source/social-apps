.class public final LX/AUX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AU9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AU9",
        "<",
        "Lcom/facebook/crudolib/dbschema/direct/model/MetadataTable_Queries$QueryByTableDAO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1678223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678224
    iput-object p1, p0, LX/AUX;->a:Ljava/lang/String;

    .line 1678225
    return-void
.end method

.method public static b(Landroid/database/Cursor;)LX/AUW;
    .locals 2

    .prologue
    .line 1678228
    new-instance v0, LX/AUW;

    invoke-direct {v0, p0}, LX/AUW;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/database/Cursor;)LX/AU0;
    .locals 1

    .prologue
    .line 1678229
    invoke-static {p1}, LX/AUX;->b(Landroid/database/Cursor;)LX/AUW;

    move-result-object v0

    return-object v0
.end method

.method public final a()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1678227
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, LX/AUU;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final b()[Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1678226
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "sqliteproc_metadata "

    aput-object v1, v0, v4

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    const-string v2, "hash"

    aput-object v2, v1, v3

    aput-object v1, v0, v3

    const-string v1, "table_name = ?"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, LX/AUX;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-object v2, v0, v1

    return-object v0
.end method
