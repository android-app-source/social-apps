.class public final LX/CdQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V
    .locals 0

    .prologue
    .line 1922451
    iput-object p1, p0, LX/CdQ;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1922452
    check-cast p2, Lcom/facebook/places/suggestions/PlaceRowView;

    .line 1922453
    invoke-virtual {p2}, Lcom/facebook/places/suggestions/PlaceRowView;->a()V

    .line 1922454
    iget-object v0, p2, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    move-object v0, v0

    .line 1922455
    iget-boolean v1, v0, LX/CdS;->e:Z

    move v0, v1

    .line 1922456
    if-eqz v0, :cond_0

    .line 1922457
    iget-object v0, p0, LX/CdQ;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    const/4 v1, 0x1

    .line 1922458
    invoke-static {v0, v1}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a$redex0(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;Z)V

    .line 1922459
    :goto_0
    return-void

    .line 1922460
    :cond_0
    iget-object v0, p0, LX/CdQ;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v1, p0, LX/CdQ;->a:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-static {v1}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->d(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)Z

    move-result v1

    .line 1922461
    invoke-static {v0, v1}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a$redex0(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;Z)V

    .line 1922462
    goto :goto_0
.end method
