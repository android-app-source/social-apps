.class public LX/Bm1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Bm1;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1817771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1817772
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventArtist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1817753
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v0

    .line 1817754
    if-nez v0, :cond_0

    .line 1817755
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1817756
    :goto_0
    return-object v0

    .line 1817757
    :cond_0
    const/4 v1, 0x0

    .line 1817758
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;

    .line 1817759
    const-string v5, "Line Up"

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1817760
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->j()LX/0Px;

    move-result-object v0

    .line 1817761
    :goto_2
    if-nez v0, :cond_2

    .line 1817762
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1817763
    goto :goto_0

    .line 1817764
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1817765
    :cond_2
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1817766
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;

    .line 1817767
    invoke-static {v1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;)Lcom/facebook/events/model/EventArtist;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817768
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 1817769
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1817770
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;Ljava/lang/String;J)LX/3CE;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;",
            "Ljava/lang/String;",
            "J)",
            "LX/3CE",
            "<",
            "LX/Bm0;",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1817742
    new-instance v2, LX/4yA;

    invoke-direct {v2}, LX/4yA;-><init>()V

    .line 1817743
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;

    .line 1817744
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->l()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->a()Z

    move-result v7

    .line 1817745
    new-instance v8, LX/7vI;

    invoke-static {v5, v6}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object p0

    invoke-direct {v8, p0}, LX/7vI;-><init>(Lcom/facebook/events/model/EventUser;)V

    .line 1817746
    iput-boolean v7, v8, LX/7vI;->j:Z

    .line 1817747
    invoke-virtual {v8}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v8

    move-object v5, v8

    .line 1817748
    iget-object v6, v5, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1817749
    invoke-static {v6, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1817750
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->k()J

    move-result-wide v6

    invoke-static {v6, v7, p2, p3}, LX/Bm1;->a(JJ)LX/Bm0;

    move-result-object v0

    invoke-virtual {v2, v0, v5}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4yA;

    .line 1817751
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1817752
    :cond_1
    invoke-virtual {v2}, LX/4yA;->a()LX/3CE;

    move-result-object v0

    return-object v0
.end method

.method public static a(JJ)LX/Bm0;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1817735
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    sub-long v0, p2, v0

    .line 1817736
    const-wide/32 v2, 0x5265bff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 1817737
    sget-object v0, LX/Bm0;->TODAY:LX/Bm0;

    .line 1817738
    :goto_0
    return-object v0

    .line 1817739
    :cond_0
    const-wide/32 v2, 0xa4cb7ff

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 1817740
    sget-object v0, LX/Bm0;->YESTERDAY:LX/Bm0;

    goto :goto_0

    .line 1817741
    :cond_1
    sget-object v0, LX/Bm0;->EARLIER:LX/Bm0;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Bm1;
    .locals 3

    .prologue
    .line 1817723
    sget-object v0, LX/Bm1;->a:LX/Bm1;

    if-nez v0, :cond_1

    .line 1817724
    const-class v1, LX/Bm1;

    monitor-enter v1

    .line 1817725
    :try_start_0
    sget-object v0, LX/Bm1;->a:LX/Bm1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1817726
    if-eqz v2, :cond_0

    .line 1817727
    :try_start_1
    new-instance v0, LX/Bm1;

    invoke-direct {v0}, LX/Bm1;-><init>()V

    .line 1817728
    move-object v0, v0

    .line 1817729
    sput-object v0, LX/Bm1;->a:LX/Bm1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1817730
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1817731
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1817732
    :cond_1
    sget-object v0, LX/Bm1;->a:LX/Bm1;

    return-object v0

    .line 1817733
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1817734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/events/model/Event;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 8

    .prologue
    .line 1817680
    const-wide/16 v5, 0x3e8

    .line 1817681
    new-instance v1, LX/7p3;

    invoke-direct {v1}, LX/7p3;-><init>()V

    .line 1817682
    iget-object v2, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1817683
    iput-object v2, v1, LX/7p3;->E:Ljava/lang/String;

    .line 1817684
    move-object v1, v1

    .line 1817685
    iget-object v2, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1817686
    iput-object v2, v1, LX/7p3;->J:Ljava/lang/String;

    .line 1817687
    move-object v1, v1

    .line 1817688
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->H:Z

    move v2, v2

    .line 1817689
    iput-boolean v2, v1, LX/7p3;->T:Z

    .line 1817690
    move-object v1, v1

    .line 1817691
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1817692
    iput-object v2, v1, LX/7p3;->S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1817693
    move-object v1, v1

    .line 1817694
    iget-object v2, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v2

    .line 1817695
    iput-object v2, v1, LX/7p3;->W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1817696
    move-object v1, v1

    .line 1817697
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v3

    div-long/2addr v3, v5

    .line 1817698
    iput-wide v3, v1, LX/7p3;->O:J

    .line 1817699
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1817700
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v3

    div-long/2addr v3, v5

    .line 1817701
    iput-wide v3, v1, LX/7p3;->k:J

    .line 1817702
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v2, v2

    .line 1817703
    if-eqz v2, :cond_1

    .line 1817704
    iget-object v2, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v2, v2

    .line 1817705
    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    .line 1817706
    iput-object v2, v1, LX/7p3;->Q:Ljava/lang/String;

    .line 1817707
    :cond_1
    new-instance v2, LX/7pj;

    invoke-direct {v2}, LX/7pj;-><init>()V

    .line 1817708
    iget-object v3, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1817709
    if-eqz v3, :cond_2

    .line 1817710
    iget-object v3, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1817711
    iput-object v3, v2, LX/7pj;->g:Ljava/lang/String;

    .line 1817712
    :cond_2
    iget-object v3, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v3, v3

    .line 1817713
    if-eqz v3, :cond_3

    .line 1817714
    new-instance v3, LX/7pi;

    invoke-direct {v3}, LX/7pi;-><init>()V

    .line 1817715
    iget-object v4, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v4, v4

    .line 1817716
    iput-object v4, v3, LX/7pi;->a:Ljava/lang/String;

    .line 1817717
    invoke-virtual {v3}, LX/7pi;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v3

    .line 1817718
    iput-object v3, v2, LX/7pj;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1817719
    :cond_3
    invoke-virtual {v2}, LX/7pj;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    .line 1817720
    iput-object v2, v1, LX/7p3;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1817721
    move-object v0, v1

    .line 1817722
    invoke-virtual {v0}, LX/7p3;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;
    .locals 2

    .prologue
    .line 1817665
    invoke-static {p0}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v1

    .line 1817666
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah()LX/2rX;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1817667
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah()LX/2rX;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1817668
    iput-object v0, v1, LX/7vC;->an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1817669
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X()I

    move-result v0

    .line 1817670
    iput v0, v1, LX/7vC;->ao:I

    .line 1817671
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1817672
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817673
    iput-object v0, v1, LX/7vC;->t:Ljava/lang/String;

    .line 1817674
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1817675
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817676
    iput-object v0, v1, LX/7vC;->Y:Ljava/lang/String;

    .line 1817677
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ai()Z

    move-result v0

    .line 1817678
    iput-boolean v0, v1, LX/7vC;->aa:Z

    .line 1817679
    invoke-virtual {v1}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;)Lcom/facebook/events/model/EventArtist;
    .locals 12

    .prologue
    .line 1817635
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1817636
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageLikersModel;

    move-result-object v1

    .line 1817637
    new-instance v2, LX/7vE;

    invoke-direct {v2}, LX/7vE;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 1817638
    iput-object v3, v2, LX/7vE;->b:Ljava/lang/String;

    .line 1817639
    move-object v2, v2

    .line 1817640
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 1817641
    iput-object v3, v2, LX/7vE;->a:Ljava/lang/String;

    .line 1817642
    move-object v2, v2

    .line 1817643
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1817644
    :goto_0
    iput-object v0, v2, LX/7vE;->c:Ljava/lang/String;

    .line 1817645
    move-object v0, v2

    .line 1817646
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->j()Z

    move-result v2

    .line 1817647
    iput-boolean v2, v0, LX/7vE;->d:Z

    .line 1817648
    move-object v0, v0

    .line 1817649
    const/4 v4, 0x0

    .line 1817650
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;->m()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_4

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel;

    .line 1817651
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v3, v4

    :goto_2
    if-ge v3, v9, :cond_3

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;

    .line 1817652
    const-string v10, "Genre"

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 1817653
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1817654
    :goto_3
    move-object v2, v2

    .line 1817655
    iput-object v2, v0, LX/7vE;->f:Ljava/lang/String;

    .line 1817656
    move-object v2, v0

    .line 1817657
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageLikersModel;->a()I

    move-result v0

    .line 1817658
    :goto_4
    iput v0, v2, LX/7vE;->e:I

    .line 1817659
    move-object v0, v2

    .line 1817660
    new-instance v1, Lcom/facebook/events/model/EventArtist;

    invoke-direct {v1, v0}, Lcom/facebook/events/model/EventArtist;-><init>(LX/7vE;)V

    move-object v0, v1

    .line 1817661
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_4

    .line 1817662
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1817663
    :cond_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 1817664
    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLEventType;)Lcom/facebook/events/model/EventType;
    .locals 2

    .prologue
    .line 1817629
    if-nez p0, :cond_0

    .line 1817630
    const/4 v0, 0x0

    .line 1817631
    :goto_0
    return-object v0

    .line 1817632
    :cond_0
    sget-object v0, LX/Blz;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1817633
    sget-object v0, Lcom/facebook/events/model/EventType;->NORMAL:Lcom/facebook/events/model/EventType;

    goto :goto_0

    .line 1817634
    :pswitch_0
    sget-object v0, Lcom/facebook/events/model/EventType;->QUICK_INVITE:Lcom/facebook/events/model/EventType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/7ob;LX/7vJ;)Lcom/facebook/events/model/EventUser;
    .locals 3

    .prologue
    .line 1817613
    invoke-interface {p0}, LX/7ob;->e()LX/1Fb;

    move-result-object v0

    .line 1817614
    new-instance v1, LX/7vI;

    invoke-direct {v1}, LX/7vI;-><init>()V

    invoke-interface {p0}, LX/7ob;->c()Ljava/lang/String;

    move-result-object v2

    .line 1817615
    iput-object v2, v1, LX/7vI;->c:Ljava/lang/String;

    .line 1817616
    move-object v1, v1

    .line 1817617
    invoke-interface {p0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v2

    .line 1817618
    iput-object v2, v1, LX/7vI;->b:Ljava/lang/String;

    .line 1817619
    move-object v1, v1

    .line 1817620
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1817621
    :goto_0
    iput-object v0, v1, LX/7vI;->d:Ljava/lang/String;

    .line 1817622
    move-object v0, v1

    .line 1817623
    invoke-interface {p0}, LX/7ob;->eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 1817624
    iput-object v1, v0, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1817625
    move-object v0, v0

    .line 1817626
    iput-object p1, v0, LX/7vI;->a:LX/7vJ;

    .line 1817627
    move-object v0, v0

    .line 1817628
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;
    .locals 5

    .prologue
    .line 1817773
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 1817774
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1817775
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1817776
    new-instance v3, LX/7vI;

    invoke-direct {v3}, LX/7vI;-><init>()V

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1817777
    :cond_0
    iput-object v0, v3, LX/7vI;->b:Ljava/lang/String;

    .line 1817778
    move-object v0, v3

    .line 1817779
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 1817780
    iput-object v3, v0, LX/7vI;->c:Ljava/lang/String;

    .line 1817781
    move-object v3, v0

    .line 1817782
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1817783
    :goto_0
    iput-object v0, v3, LX/7vI;->d:Ljava/lang/String;

    .line 1817784
    move-object v0, v3

    .line 1817785
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1817786
    iput-object v1, v0, LX/7vI;->e:Ljava/lang/String;

    .line 1817787
    move-object v0, v0

    .line 1817788
    sget-object v1, LX/7vJ;->EMAIL_USER:LX/7vJ;

    .line 1817789
    iput-object v1, v0, LX/7vI;->a:LX/7vJ;

    .line 1817790
    move-object v0, v0

    .line 1817791
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    return-object v0

    .line 1817792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1817793
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1817589
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 1817590
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1817591
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1817592
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1817593
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1817594
    invoke-virtual {v4, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1817595
    :goto_0
    new-instance v4, LX/7vI;

    invoke-direct {v4}, LX/7vI;-><init>()V

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v0

    .line 1817596
    :cond_0
    iput-object v1, v4, LX/7vI;->b:Ljava/lang/String;

    .line 1817597
    move-object v1, v4

    .line 1817598
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 1817599
    iput-object v4, v1, LX/7vI;->c:Ljava/lang/String;

    .line 1817600
    move-object v4, v1

    .line 1817601
    if-nez v3, :cond_2

    const/4 v1, 0x0

    .line 1817602
    :goto_1
    iput-object v1, v4, LX/7vI;->d:Ljava/lang/String;

    .line 1817603
    move-object v1, v4

    .line 1817604
    iput-object v0, v1, LX/7vI;->f:Ljava/lang/String;

    .line 1817605
    move-object v0, v1

    .line 1817606
    sget-object v1, LX/7vJ;->SMS_USER:LX/7vJ;

    .line 1817607
    iput-object v1, v0, LX/7vI;->a:LX/7vJ;

    .line 1817608
    move-object v0, v0

    .line 1817609
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    return-object v0

    .line 1817610
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1817611
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 1817612
    :cond_2
    invoke-virtual {v2, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1817564
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 1817565
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1817566
    new-instance v4, LX/7vI;

    invoke-direct {v4}, LX/7vI;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1817567
    iput-object v5, v4, LX/7vI;->c:Ljava/lang/String;

    .line 1817568
    move-object v4, v4

    .line 1817569
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 1817570
    iput-object v5, v4, LX/7vI;->b:Ljava/lang/String;

    .line 1817571
    move-object v4, v4

    .line 1817572
    if-nez v2, :cond_1

    .line 1817573
    :goto_0
    iput v0, v4, LX/7vI;->g:I

    .line 1817574
    move-object v2, v4

    .line 1817575
    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 1817576
    :goto_1
    iput-object v0, v2, LX/7vI;->d:Ljava/lang/String;

    .line 1817577
    move-object v0, v2

    .line 1817578
    sget-object v1, LX/7vJ;->USER:LX/7vJ;

    .line 1817579
    iput-object v1, v0, LX/7vI;->a:LX/7vJ;

    .line 1817580
    move-object v0, v0

    .line 1817581
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;->eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 1817582
    iput-object v1, v0, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1817583
    move-object v0, v0

    .line 1817584
    if-eqz p1, :cond_0

    .line 1817585
    iput-object p1, v0, LX/7vI;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1817586
    :cond_0
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    return-object v0

    .line 1817587
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1817588
    :cond_1
    invoke-virtual {v3, v2, v0}, LX/15i;->j(II)I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;
    .locals 2

    .prologue
    .line 1817556
    new-instance v0, LX/7vI;

    invoke-direct {v0}, LX/7vI;-><init>()V

    const v1, 0x7f082aef

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1817557
    iput-object v1, v0, LX/7vI;->b:Ljava/lang/String;

    .line 1817558
    move-object v0, v0

    .line 1817559
    iput-object p0, v0, LX/7vI;->c:Ljava/lang/String;

    .line 1817560
    move-object v0, v0

    .line 1817561
    iput-object p1, v0, LX/7vI;->a:LX/7vJ;

    .line 1817562
    move-object v0, v0

    .line 1817563
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLEventVisibility;)Lcom/facebook/events/model/PrivacyType;
    .locals 2

    .prologue
    .line 1817546
    if-nez p0, :cond_0

    .line 1817547
    const/4 v0, 0x0

    .line 1817548
    :goto_0
    return-object v0

    .line 1817549
    :cond_0
    sget-object v0, LX/Blz;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1817550
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    .line 1817551
    :pswitch_0
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_FRIENDS:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    .line 1817552
    :pswitch_1
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    .line 1817553
    :pswitch_2
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    .line 1817554
    :pswitch_3
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    .line 1817555
    :pswitch_4
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(LX/7ob;I)Z
    .locals 1

    .prologue
    .line 1817545
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/7ob;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7ob;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1817031
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v3

    .line 1817032
    if-nez v3, :cond_1

    .line 1817033
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1817034
    :cond_0
    :goto_0
    return-object v0

    .line 1817035
    :cond_1
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1817036
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;

    .line 1817037
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    move-result-object v0

    .line 1817038
    if-eqz v0, :cond_2

    const v7, 0x25d6af

    invoke-static {v0, v7}, LX/Bm1;->a(LX/7ob;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1817039
    sget-object v7, LX/7vJ;->PAGE:LX/7vJ;

    invoke-static {v0, v7}, LX/Bm1;->a(LX/7ob;LX/7vJ;)Lcom/facebook/events/model/EventUser;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817040
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1817041
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1817042
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1817043
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1817044
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;

    .line 1817045
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    move-result-object v0

    .line 1817046
    if-eqz v0, :cond_4

    const v5, 0x285feb

    invoke-static {v0, v5}, LX/Bm1;->a(LX/7ob;I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1817047
    sget-object v5, LX/7vJ;->USER:LX/7vJ;

    invoke-static {v0, v5}, LX/Bm1;->a(LX/7ob;LX/7vJ;)Lcom/facebook/events/model/EventUser;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817048
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1817049
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/7oa;)Lcom/facebook/events/model/Event;
    .locals 1

    .prologue
    .line 1817544
    invoke-static {p0}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v0

    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/events/model/Event;
    .locals 15

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1817318
    new-instance v0, LX/7vC;

    invoke-direct {v0}, LX/7vC;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    .line 1817319
    iput-object v1, v0, LX/7vC;->a:Ljava/lang/String;

    .line 1817320
    move-object v0, v0

    .line 1817321
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    .line 1817322
    iput-object v1, v0, LX/7vC;->b:Ljava/lang/String;

    .line 1817323
    move-object v0, v0

    .line 1817324
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kW()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    .line 1817325
    iput-object v1, v0, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1817326
    move-object v0, v0

    .line 1817327
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aL()Z

    move-result v1

    .line 1817328
    iput-boolean v1, v0, LX/7vC;->B:Z

    .line 1817329
    move-object v0, v0

    .line 1817330
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kL()Z

    move-result v1

    .line 1817331
    iput-boolean v1, v0, LX/7vC;->H:Z

    .line 1817332
    move-object v9, v0

    .line 1817333
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1817334
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/7ng;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    .line 1817335
    iput-object v0, v9, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1817336
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cO()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v0

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/graphql/enums/GraphQLEventVisibility;)Lcom/facebook/events/model/PrivacyType;

    move-result-object v0

    .line 1817337
    iput-object v0, v9, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    .line 1817338
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v0, v1, :cond_3

    .line 1817339
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    .line 1817340
    iput-object v0, v9, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817341
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->q()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->q()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    .line 1817342
    :goto_1
    iput-object v0, v9, LX/7vC;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1817343
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cM()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v0

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/graphql/enums/GraphQLEventType;)Lcom/facebook/events/model/EventType;

    move-result-object v0

    .line 1817344
    iput-object v0, v9, LX/7vC;->e:Lcom/facebook/events/model/EventType;

    .line 1817345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bE()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 1817346
    if-eqz v0, :cond_1

    .line 1817347
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    .line 1817348
    iput-object v1, v9, LX/7vC;->r:Ljava/lang/String;

    .line 1817349
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v0

    .line 1817350
    iput-object v0, v9, LX/7vC;->s:Ljava/lang/String;

    .line 1817351
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gg()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 1817352
    if-eqz v0, :cond_2

    .line 1817353
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    .line 1817354
    iput-object v1, v9, LX/7vC;->t:Ljava/lang/String;

    .line 1817355
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v0

    .line 1817356
    iput-object v0, v9, LX/7vC;->u:Ljava/lang/String;

    .line 1817357
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cD()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1817358
    if-eqz v0, :cond_2f

    .line 1817359
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    .line 1817360
    iput-object v0, v9, LX/7vC;->v:Ljava/lang/String;

    .line 1817361
    move-object v1, v0

    .line 1817362
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cF()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v0

    .line 1817363
    if-eqz v0, :cond_6

    .line 1817364
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 1817365
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventHostsConnection;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v8, v7

    move-object v3, v2

    move-object v5, v2

    :goto_3
    if-ge v8, v12, :cond_5

    invoke-virtual {v11, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventHostsEdge;

    .line 1817366
    if-eqz v0, :cond_2e

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventHostsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    if-eqz v4, :cond_2e

    .line 1817367
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventHostsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1817368
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v13

    .line 1817369
    invoke-static {v13}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2e

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2e

    .line 1817370
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2d

    .line 1817371
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventHostsEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1817372
    :goto_4
    invoke-virtual {v10, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817373
    if-nez v5, :cond_2c

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aI()Z

    move-result v3

    if-eqz v3, :cond_2c

    move-object v3, v4

    .line 1817374
    :goto_5
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move-object v5, v3

    move-object v3, v0

    goto :goto_3

    .line 1817375
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817376
    iput-object v0, v9, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817377
    goto/16 :goto_0

    .line 1817378
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto/16 :goto_1

    .line 1817379
    :cond_5
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1817380
    iput-object v0, v9, LX/7vC;->q:LX/0Px;

    .line 1817381
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1817382
    iput v0, v9, LX/7vC;->p:I

    .line 1817383
    iput-object v3, v9, LX/7vC;->o:Ljava/lang/String;

    .line 1817384
    if-eqz v5, :cond_6

    .line 1817385
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    .line 1817386
    iput-object v0, v9, LX/7vC;->w:Ljava/lang/String;

    .line 1817387
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_29

    .line 1817388
    :goto_6
    iput-object v2, v9, LX/7vC;->x:Ljava/lang/String;

    .line 1817389
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->et()Z

    move-result v0

    .line 1817390
    iput-boolean v0, v9, LX/7vC;->y:Z

    .line 1817391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ex()Z

    move-result v0

    .line 1817392
    iput-boolean v0, v9, LX/7vC;->z:Z

    .line 1817393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hU()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-virtual {v9, v0, v1}, LX/7vC;->a(J)LX/7vC;

    .line 1817394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aF()Z

    move-result v0

    .line 1817395
    iput-boolean v0, v9, LX/7vC;->h:Z

    .line 1817396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aG()Z

    move-result v0

    .line 1817397
    iput-boolean v0, v9, LX/7vC;->i:Z

    .line 1817398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gK()Z

    move-result v0

    .line 1817399
    iput-boolean v0, v9, LX/7vC;->j:Z

    .line 1817400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eF()Z

    move-result v0

    .line 1817401
    iput-boolean v0, v9, LX/7vC;->l:Z

    .line 1817402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kK()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    .line 1817403
    iput-object v0, v9, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1817404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_2a

    move v0, v6

    .line 1817405
    :goto_7
    iput-boolean v0, v9, LX/7vC;->E:Z

    .line 1817406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1817407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817408
    iput-object v0, v9, LX/7vC;->F:Ljava/lang/String;

    .line 1817409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817410
    iput-object v0, v9, LX/7vC;->G:Ljava/lang/String;

    .line 1817411
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eq()Z

    move-result v0

    .line 1817412
    iput-boolean v0, v9, LX/7vC;->L:Z

    .line 1817413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->a(J)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1817414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v1

    .line 1817415
    iput-object v1, v9, LX/7vC;->I:Ljava/util/Date;

    .line 1817416
    if-eqz v0, :cond_2b

    .line 1817417
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x5265bff

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1817418
    iput-object v0, v9, LX/7vC;->J:Ljava/util/Date;

    .line 1817419
    :goto_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jI()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1817420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jI()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 1817421
    iput-object v0, v9, LX/7vC;->K:Ljava/util/TimeZone;

    .line 1817422
    :cond_8
    invoke-virtual {v9}, LX/7vC;->a()LX/7vC;

    .line 1817423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cI()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1817424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cI()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 1817425
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1817426
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->j()Ljava/lang/String;

    move-result-object v1

    .line 1817427
    iput-object v1, v9, LX/7vC;->P:Ljava/lang/String;

    .line 1817428
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 1817429
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 1817430
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-virtual {v9, v2, v3, v4, v5}, LX/7vC;->a(DD)LX/7vC;

    .line 1817431
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 1817432
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 1817433
    iput-object v1, v9, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1817434
    :cond_a
    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1817435
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v9, v2, v3}, LX/7vC;->b(J)LX/7vC;

    .line 1817436
    :cond_b
    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1817437
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v1

    .line 1817438
    iput-object v1, v9, LX/7vC;->O:Ljava/lang/String;

    .line 1817439
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_d

    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->t()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1817440
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->t()Ljava/lang/String;

    move-result-object v0

    .line 1817441
    iput-object v0, v9, LX/7vC;->S:Ljava/lang/String;

    .line 1817442
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cA()Ljava/lang/String;

    move-result-object v0

    .line 1817443
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1817444
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1817445
    iput-object v0, v9, LX/7vC;->T:Landroid/net/Uri;

    .line 1817446
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cy()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1817447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cy()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 1817448
    if-eqz v0, :cond_f

    .line 1817449
    iput-object v0, v9, LX/7vC;->X:Landroid/net/Uri;

    .line 1817450
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1817451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    .line 1817452
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1817453
    iput-object v0, v9, LX/7vC;->U:Ljava/lang/String;

    .line 1817454
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1817455
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 1817456
    if-eqz v0, :cond_11

    .line 1817457
    iput-object v0, v9, LX/7vC;->V:Landroid/net/Uri;

    .line 1817458
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1817459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 1817460
    if-eqz v0, :cond_12

    .line 1817461
    iput-object v0, v9, LX/7vC;->W:Landroid/net/Uri;

    .line 1817462
    :cond_12
    const-class v0, LX/7vK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 1817463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cN()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v1

    .line 1817464
    if-eqz v1, :cond_22

    .line 1817465
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->w()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1817466
    sget-object v2, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817467
    :cond_13
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->o()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1817468
    sget-object v2, LX/7vK;->JOIN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817469
    :cond_14
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->p()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1817470
    sget-object v2, LX/7vK;->MAYBE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817471
    :cond_15
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->j()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1817472
    sget-object v2, LX/7vK;->DECLINE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817473
    :cond_16
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->n()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1817474
    sget-object v2, LX/7vK;->INVITE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817475
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aQ()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1817476
    sget-object v2, LX/7vK;->POST:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817477
    :cond_18
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->l()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1817478
    sget-object v2, LX/7vK;->EDIT:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817479
    :cond_19
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->s()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1817480
    sget-object v2, LX/7vK;->REPORT:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817481
    :cond_1a
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->k()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1817482
    sget-object v2, LX/7vK;->DELETE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817483
    :cond_1b
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->t()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1817484
    sget-object v2, LX/7vK;->SAVE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817485
    :cond_1c
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->v()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1817486
    sget-object v2, LX/7vK;->SHARE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817487
    :cond_1d
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->u()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1817488
    sget-object v2, LX/7vK;->SEND_MESSAGE_TO_GUESTS:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817489
    :cond_1e
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->m()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1817490
    sget-object v2, LX/7vK;->EDIT_HOST:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817491
    :cond_1f
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->r()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1817492
    sget-object v2, LX/7vK;->REMOVE_SELF:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817493
    :cond_20
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->q()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1817494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cK()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v2, v3, :cond_30

    .line 1817495
    sget-object v2, LX/7vK;->PROMOTE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817496
    :cond_21
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->x()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v2, v3, :cond_31

    .line 1817497
    sget-object v2, LX/7vK;->SEEN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817498
    :goto_a
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->a()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1817499
    sget-object v1, LX/7vK;->CREATE_REPEAT_EVENT:LX/7vK;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817500
    :cond_22
    move-object v0, v0

    .line 1817501
    invoke-virtual {v9, v0}, LX/7vC;->a(Ljava/util/EnumSet;)LX/7vC;

    .line 1817502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->du()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    .line 1817503
    if-eqz v1, :cond_23

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->l()I

    move-result v0

    if-lez v0, :cond_23

    .line 1817504
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_23

    .line 1817505
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMembersEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1817506
    iput-object v0, v9, LX/7vC;->ab:Ljava/lang/String;

    .line 1817507
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->l()I

    move-result v0

    .line 1817508
    iput v0, v9, LX/7vC;->ac:I

    .line 1817509
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dt()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    .line 1817510
    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->k()I

    move-result v0

    if-lez v0, :cond_24

    .line 1817511
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_24

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1817512
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventMaybesEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1817513
    iput-object v0, v9, LX/7vC;->ad:Ljava/lang/String;

    .line 1817514
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;->k()I

    move-result v0

    .line 1817515
    iput v0, v9, LX/7vC;->ae:I

    .line 1817516
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dv()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    .line 1817517
    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->l()I

    move-result v0

    if-lez v0, :cond_25

    .line 1817518
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_25

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_25

    .line 1817519
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEventWatchersEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventWatchersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1817520
    iput-object v0, v9, LX/7vC;->af:Ljava/lang/String;

    .line 1817521
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->l()I

    move-result v0

    .line 1817522
    iput v0, v9, LX/7vC;->ag:I

    .line 1817523
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    .line 1817524
    invoke-static {p0}, LX/Bm1;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 1817525
    iput-object v0, v9, LX/7vC;->ah:Lcom/facebook/events/model/EventUser;

    .line 1817526
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cx()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 1817527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cx()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v0

    .line 1817528
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCategoryData;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 1817529
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCategoryData;->j()Ljava/lang/String;

    move-result-object v1

    .line 1817530
    iput-object v1, v9, LX/7vC;->ai:Ljava/lang/String;

    .line 1817531
    :cond_27
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCategoryData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_28

    .line 1817532
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCategoryData;->a()Ljava/lang/String;

    move-result-object v0

    .line 1817533
    iput-object v0, v9, LX/7vC;->aj:Ljava/lang/String;

    .line 1817534
    :cond_28
    invoke-virtual {v9}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    return-object v0

    .line 1817535
    :cond_29
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    :cond_2a
    move v0, v7

    .line 1817536
    goto/16 :goto_7

    .line 1817537
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cp()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v0

    .line 1817538
    iput-object v0, v9, LX/7vC;->J:Ljava/util/Date;

    .line 1817539
    goto/16 :goto_8

    :cond_2c
    move-object v3, v5

    goto/16 :goto_5

    :cond_2d
    move-object v0, v3

    goto/16 :goto_4

    :cond_2e
    move-object v0, v3

    move-object v3, v5

    goto/16 :goto_5

    :cond_2f
    move-object v1, v2

    goto/16 :goto_2

    .line 1817540
    :cond_30
    sget-object v2, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9

    .line 1817541
    :cond_31
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->x()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v2, v3, :cond_32

    .line 1817542
    sget-object v2, LX/7vK;->UNSEEN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 1817543
    :cond_32
    sget-object v2, LX/7vK;->UNKNOWN_SEEN_STATE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1817296
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1817297
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kN()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1817298
    sget-object v5, LX/7vJ;->USER:LX/7vJ;

    .line 1817299
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 1817300
    new-instance v7, LX/7vI;

    invoke-direct {v7}, LX/7vI;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    .line 1817301
    iput-object p0, v7, LX/7vI;->c:Ljava/lang/String;

    .line 1817302
    move-object v7, v7

    .line 1817303
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p0

    .line 1817304
    iput-object p0, v7, LX/7vI;->b:Ljava/lang/String;

    .line 1817305
    move-object v7, v7

    .line 1817306
    if-nez v6, :cond_1

    const/4 v6, 0x0

    .line 1817307
    :goto_1
    iput-object v6, v7, LX/7vI;->d:Ljava/lang/String;

    .line 1817308
    move-object v6, v7

    .line 1817309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 1817310
    iput-object v7, v6, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1817311
    move-object v6, v6

    .line 1817312
    iput-object v5, v6, LX/7vI;->a:LX/7vJ;

    .line 1817313
    move-object v6, v6

    .line 1817314
    invoke-virtual {v6}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v6

    move-object v0, v6

    .line 1817315
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817316
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1817317
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public static c(LX/7oa;)LX/7vC;
    .locals 14

    .prologue
    .line 1817050
    new-instance v0, LX/7vC;

    invoke-direct {v0}, LX/7vC;-><init>()V

    invoke-interface {p0}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v1

    .line 1817051
    iput-object v1, v0, LX/7vC;->a:Ljava/lang/String;

    .line 1817052
    move-object v0, v0

    .line 1817053
    invoke-interface {p0}, LX/7oa;->eR_()Ljava/lang/String;

    move-result-object v1

    .line 1817054
    iput-object v1, v0, LX/7vC;->b:Ljava/lang/String;

    .line 1817055
    move-object v6, v0

    .line 1817056
    invoke-interface {p0}, LX/7oa;->E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1817057
    invoke-interface {p0}, LX/7oa;->E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    .line 1817058
    iput-object v0, v6, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1817059
    :cond_0
    invoke-interface {p0}, LX/7oa;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    .line 1817060
    iput-object v0, v6, LX/7vC;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1817061
    invoke-interface {p0}, LX/7oa;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v0

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/graphql/enums/GraphQLEventVisibility;)Lcom/facebook/events/model/PrivacyType;

    move-result-object v0

    .line 1817062
    iput-object v0, v6, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    .line 1817063
    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v0, v1, :cond_5

    .line 1817064
    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    .line 1817065
    iput-object v0, v6, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817066
    :goto_0
    invoke-interface {p0}, LX/7oa;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p0}, LX/7oa;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    .line 1817067
    :goto_1
    iput-object v0, v6, LX/7vC;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1817068
    invoke-interface {p0}, LX/7oa;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v0

    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/graphql/enums/GraphQLEventType;)Lcom/facebook/events/model/EventType;

    move-result-object v0

    .line 1817069
    iput-object v0, v6, LX/7vC;->e:Lcom/facebook/events/model/EventType;

    .line 1817070
    invoke-interface {p0}, LX/7oa;->y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    .line 1817071
    if-eqz v0, :cond_1

    .line 1817072
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1817073
    iput-object v1, v6, LX/7vC;->r:Ljava/lang/String;

    .line 1817074
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817075
    iput-object v0, v6, LX/7vC;->s:Ljava/lang/String;

    .line 1817076
    :cond_1
    invoke-interface {p0}, LX/7oa;->T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    .line 1817077
    if-eqz v0, :cond_2

    .line 1817078
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1817079
    iput-object v1, v6, LX/7vC;->t:Ljava/lang/String;

    .line 1817080
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817081
    iput-object v0, v6, LX/7vC;->u:Ljava/lang/String;

    .line 1817082
    :cond_2
    invoke-interface {p0}, LX/7oa;->C()LX/7oW;

    move-result-object v1

    .line 1817083
    const/4 v0, 0x0

    .line 1817084
    if-eqz v1, :cond_33

    .line 1817085
    invoke-interface {v1}, LX/7oW;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817086
    iput-object v0, v6, LX/7vC;->v:Ljava/lang/String;

    .line 1817087
    move-object v1, v0

    .line 1817088
    :goto_2
    invoke-interface {p0}, LX/7oa;->F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    .line 1817089
    if-eqz v0, :cond_8

    .line 1817090
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1817091
    const/4 v4, 0x0

    .line 1817092
    const/4 v3, 0x0

    .line 1817093
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v5, v0

    :goto_3
    if-ge v5, v9, :cond_7

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;

    .line 1817094
    if-eqz v0, :cond_32

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    move-result-object v2

    if-eqz v2, :cond_32

    .line 1817095
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    move-result-object v2

    .line 1817096
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 1817097
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 1817098
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1817099
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 1817100
    :cond_3
    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_4
    move-object v0, v4

    .line 1817101
    if-nez v3, :cond_31

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->j()Z

    move-result v4

    if-eqz v4, :cond_31

    move-object v12, v2

    move-object v2, v0

    move-object v0, v12

    .line 1817102
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v2

    move-object v3, v0

    goto :goto_3

    .line 1817103
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817104
    iput-object v0, v6, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1817105
    goto/16 :goto_0

    .line 1817106
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->DEFAULT_STYLE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    goto/16 :goto_1

    .line 1817107
    :cond_7
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1817108
    iput-object v0, v6, LX/7vC;->q:LX/0Px;

    .line 1817109
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1817110
    iput v0, v6, LX/7vC;->p:I

    .line 1817111
    iput-object v4, v6, LX/7vC;->o:Ljava/lang/String;

    .line 1817112
    if-eqz v3, :cond_8

    .line 1817113
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817114
    iput-object v0, v6, LX/7vC;->w:Ljava/lang/String;

    .line 1817115
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_2d

    const/4 v0, 0x0

    .line 1817116
    :goto_5
    iput-object v0, v6, LX/7vC;->x:Ljava/lang/String;

    .line 1817117
    :cond_8
    invoke-interface {p0}, LX/7oa;->R()Z

    move-result v0

    .line 1817118
    iput-boolean v0, v6, LX/7vC;->y:Z

    .line 1817119
    invoke-interface {p0}, LX/7oa;->n()Z

    move-result v0

    .line 1817120
    iput-boolean v0, v6, LX/7vC;->z:Z

    .line 1817121
    invoke-interface {p0}, LX/7oa;->o()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 1817122
    iput-wide v0, v6, LX/7vC;->A:J

    .line 1817123
    invoke-interface {p0}, LX/7oa;->v()Z

    move-result v0

    .line 1817124
    iput-boolean v0, v6, LX/7vC;->h:Z

    .line 1817125
    invoke-interface {p0}, LX/7oa;->w()Z

    move-result v0

    .line 1817126
    iput-boolean v0, v6, LX/7vC;->i:Z

    .line 1817127
    invoke-interface {p0}, LX/7oa;->U()Z

    move-result v0

    .line 1817128
    iput-boolean v0, v6, LX/7vC;->j:Z

    .line 1817129
    invoke-interface {p0}, LX/7oa;->u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    if-nez v0, :cond_2e

    .line 1817130
    sget-object v0, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v6, v0}, LX/7vC;->a(LX/03R;)LX/7vC;

    .line 1817131
    :goto_6
    invoke-interface {p0}, LX/7oa;->S()Z

    move-result v0

    .line 1817132
    iput-boolean v0, v6, LX/7vC;->l:Z

    .line 1817133
    invoke-interface {p0}, LX/7oa;->k()Z

    move-result v0

    .line 1817134
    iput-boolean v0, v6, LX/7vC;->B:Z

    .line 1817135
    invoke-interface {p0}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    .line 1817136
    iput-object v0, v6, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1817137
    invoke-interface {p0}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    .line 1817138
    iput-object v0, v6, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1817139
    invoke-interface {p0}, LX/7oa;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_2f

    const/4 v0, 0x1

    .line 1817140
    :goto_7
    iput-boolean v0, v6, LX/7vC;->E:Z

    .line 1817141
    invoke-interface {p0}, LX/7oa;->V()LX/1oP;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1817142
    invoke-interface {p0}, LX/7oa;->V()LX/1oP;

    move-result-object v0

    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817143
    iput-object v0, v6, LX/7vC;->F:Ljava/lang/String;

    .line 1817144
    invoke-interface {p0}, LX/7oa;->V()LX/1oP;

    move-result-object v0

    invoke-interface {v0}, LX/1oP;->d()LX/1oQ;

    move-result-object v0

    invoke-interface {v0}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v0

    .line 1817145
    iput-object v0, v6, LX/7vC;->G:Ljava/lang/String;

    .line 1817146
    :cond_9
    invoke-interface {p0}, LX/7oa;->r()Z

    move-result v0

    .line 1817147
    iput-boolean v0, v6, LX/7vC;->H:Z

    .line 1817148
    invoke-interface {p0}, LX/7oa;->eQ_()Z

    move-result v0

    .line 1817149
    iput-boolean v0, v6, LX/7vC;->L:Z

    .line 1817150
    invoke-interface {p0}, LX/7oa;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->a(J)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1817151
    invoke-interface {p0}, LX/7oa;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v1

    .line 1817152
    iput-object v1, v6, LX/7vC;->I:Ljava/util/Date;

    .line 1817153
    if-eqz v0, :cond_30

    .line 1817154
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x5265bff

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1817155
    iput-object v0, v6, LX/7vC;->J:Ljava/util/Date;

    .line 1817156
    :goto_8
    invoke-interface {p0}, LX/7oa;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1817157
    invoke-interface {p0}, LX/7oa;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 1817158
    iput-object v0, v6, LX/7vC;->K:Ljava/util/TimeZone;

    .line 1817159
    :cond_a
    invoke-virtual {v6}, LX/7vC;->a()LX/7vC;

    .line 1817160
    invoke-interface {p0}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1817161
    invoke-interface {p0}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    .line 1817162
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1817163
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1817164
    iput-object v1, v6, LX/7vC;->P:Ljava/lang/String;

    .line 1817165
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eW_()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1817166
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eW_()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v1

    .line 1817167
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;->a()D

    move-result-wide v2

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;->b()D

    move-result-wide v4

    invoke-virtual {v6, v2, v3, v4, v5}, LX/7vC;->a(DD)LX/7vC;

    .line 1817168
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 1817169
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 1817170
    iput-object v1, v6, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1817171
    :cond_c
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eV_()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1817172
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eV_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1817173
    iput-wide v2, v6, LX/7vC;->N:J

    .line 1817174
    :cond_d
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1817175
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1817176
    iput-object v1, v6, LX/7vC;->O:Ljava/lang/String;

    .line 1817177
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 1817178
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 1817179
    iput v1, v6, LX/7vC;->R:I

    .line 1817180
    :cond_f
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1817181
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1817182
    iput-object v0, v6, LX/7vC;->S:Ljava/lang/String;

    .line 1817183
    :cond_10
    invoke-interface {p0}, LX/7oa;->B()Ljava/lang/String;

    move-result-object v0

    .line 1817184
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1817185
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1817186
    iput-object v0, v6, LX/7vC;->T:Landroid/net/Uri;

    .line 1817187
    :cond_11
    invoke-interface {p0}, LX/7oa;->c()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1817188
    invoke-interface {p0}, LX/7oa;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817189
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 1817190
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1817191
    iput-object v0, v6, LX/7vC;->X:Landroid/net/Uri;

    .line 1817192
    :cond_12
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1817193
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 1817194
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 1817195
    iput-object v0, v6, LX/7vC;->U:Ljava/lang/String;

    .line 1817196
    :cond_13
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1817197
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817198
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1817199
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1817200
    iput-object v0, v6, LX/7vC;->V:Landroid/net/Uri;

    .line 1817201
    :cond_14
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1817202
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1817203
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1817204
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1817205
    iput-object v0, v6, LX/7vC;->W:Landroid/net/Uri;

    .line 1817206
    :cond_15
    const-class v0, LX/7vK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 1817207
    invoke-interface {p0}, LX/7oa;->M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    .line 1817208
    if-eqz v1, :cond_25

    .line 1817209
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1817210
    sget-object v2, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817211
    :cond_16
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->fc_()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1817212
    sget-object v2, LX/7vK;->JOIN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817213
    :cond_17
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->j()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1817214
    sget-object v2, LX/7vK;->MAYBE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817215
    :cond_18
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->b()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1817216
    sget-object v2, LX/7vK;->DECLINE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817217
    :cond_19
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->fb_()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1817218
    sget-object v2, LX/7vK;->INVITE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817219
    :cond_1a
    invoke-interface {p0}, LX/7oa;->x()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1817220
    sget-object v2, LX/7vK;->POST:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817221
    :cond_1b
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->d()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1817222
    sget-object v2, LX/7vK;->EDIT:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817223
    :cond_1c
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->m()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1817224
    sget-object v2, LX/7vK;->REPORT:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817225
    :cond_1d
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->c()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1817226
    sget-object v2, LX/7vK;->DELETE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817227
    :cond_1e
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->n()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1817228
    sget-object v2, LX/7vK;->SAVE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817229
    :cond_1f
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->p()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1817230
    sget-object v2, LX/7vK;->SHARE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817231
    :cond_20
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->o()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1817232
    sget-object v2, LX/7vK;->SEND_MESSAGE_TO_GUESTS:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817233
    :cond_21
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->e()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1817234
    sget-object v2, LX/7vK;->EDIT_HOST:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817235
    :cond_22
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->l()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1817236
    sget-object v2, LX/7vK;->REMOVE_SELF:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817237
    :cond_23
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v2, v3, :cond_24

    .line 1817238
    sget-object v2, LX/7vK;->SEEN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817239
    :cond_24
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->k()Z

    move-result v2

    if-eqz v2, :cond_35

    .line 1817240
    invoke-interface {p0}, LX/7oa;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    if-ne v2, v3, :cond_34

    .line 1817241
    sget-object v2, LX/7vK;->PROMOTE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817242
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->a()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1817243
    sget-object v1, LX/7vK;->CREATE_REPEAT_EVENT:LX/7vK;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1817244
    :cond_25
    move-object v0, v0

    .line 1817245
    invoke-virtual {v6, v0}, LX/7vC;->a(Ljava/util/EnumSet;)LX/7vC;

    .line 1817246
    invoke-interface {p0}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v1

    .line 1817247
    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v0

    if-lez v0, :cond_26

    .line 1817248
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_26

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    .line 1817249
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    .line 1817250
    iput-object v0, v6, LX/7vC;->ab:Ljava/lang/String;

    .line 1817251
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v0

    .line 1817252
    iput v0, v6, LX/7vC;->ac:I

    .line 1817253
    :cond_26
    invoke-interface {p0}, LX/7oa;->O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v1

    .line 1817254
    if-eqz v1, :cond_27

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->b()I

    move-result v0

    if-lez v0, :cond_27

    .line 1817255
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_27

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_27

    .line 1817256
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    .line 1817257
    iput-object v0, v6, LX/7vC;->ad:Ljava/lang/String;

    .line 1817258
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->b()I

    move-result v0

    .line 1817259
    iput v0, v6, LX/7vC;->ae:I

    .line 1817260
    :cond_27
    invoke-interface {p0}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v1

    .line 1817261
    if-eqz v1, :cond_28

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->b()I

    move-result v0

    if-lez v0, :cond_28

    .line 1817262
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_28

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    .line 1817263
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-interface {v0}, LX/7ob;->d()Ljava/lang/String;

    move-result-object v0

    .line 1817264
    iput-object v0, v6, LX/7vC;->af:Ljava/lang/String;

    .line 1817265
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->b()I

    move-result v0

    .line 1817266
    iput v0, v6, LX/7vC;->ag:I

    .line 1817267
    :cond_28
    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 1817268
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1817269
    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_a
    if-ge v1, v4, :cond_29

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    .line 1817270
    sget-object v5, LX/7vJ;->USER:LX/7vJ;

    invoke-static {v0, v5}, LX/Bm1;->a(LX/7ob;LX/7vJ;)Lcom/facebook/events/model/EventUser;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1817271
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1817272
    :cond_29
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1817273
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 1817274
    iput-object v0, v6, LX/7vC;->ah:Lcom/facebook/events/model/EventUser;

    .line 1817275
    :cond_2a
    invoke-interface {p0}, LX/7oa;->A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 1817276
    invoke-interface {p0}, LX/7oa;->A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    .line 1817277
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2b

    .line 1817278
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1817279
    iput-object v1, v6, LX/7vC;->ai:Ljava/lang/String;

    .line 1817280
    :cond_2b
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2c

    .line 1817281
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1817282
    iput-object v0, v6, LX/7vC;->aj:Ljava/lang/String;

    .line 1817283
    :cond_2c
    invoke-interface {p0}, LX/7oa;->X()I

    move-result v0

    .line 1817284
    iput v0, v6, LX/7vC;->ao:I

    .line 1817285
    return-object v6

    .line 1817286
    :cond_2d
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1817287
    :cond_2e
    invoke-interface {p0}, LX/7oa;->u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;->a()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/7vC;->a(LX/03R;)LX/7vC;

    goto/16 :goto_6

    .line 1817288
    :cond_2f
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 1817289
    :cond_30
    invoke-interface {p0}, LX/7oa;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v0

    .line 1817290
    iput-object v0, v6, LX/7vC;->J:Ljava/util/Date;

    .line 1817291
    goto/16 :goto_8

    :cond_31
    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_4

    :cond_32
    move-object v0, v3

    move-object v2, v4

    goto/16 :goto_4

    :cond_33
    move-object v1, v0

    goto/16 :goto_2

    .line 1817292
    :cond_34
    sget-object v2, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9

    .line 1817293
    :cond_35
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v2, v3, :cond_36

    .line 1817294
    sget-object v2, LX/7vK;->UNSEEN:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9

    .line 1817295
    :cond_36
    sget-object v2, LX/7vK;->UNKNOWN_SEEN_STATE:LX/7vK;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9
.end method
