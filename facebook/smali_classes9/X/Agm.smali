.class public final LX/Agm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qb;


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 0

    .prologue
    .line 1701526
    iput-object p1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1701525
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701523
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701524
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1701507
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701508
    iget-object v0, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V

    .line 1701509
    return-void
.end method

.method public final a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 2

    .prologue
    .line 1701520
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701521
    iget-object v0, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 1701522
    return-void
.end method

.method public final a(Lcom/facebook/payments/model/PaymentsPin;)V
    .locals 2

    .prologue
    .line 1701517
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701518
    iget-object v0, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V

    .line 1701519
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 2

    .prologue
    .line 1701515
    iget-object v0, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V

    .line 1701516
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701513
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701514
    return-void
.end method

.method public final c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701510
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701511
    iget-object v0, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agm;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->c(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V

    .line 1701512
    return-void
.end method
