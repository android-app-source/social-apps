.class public LX/BRi;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field public static b:Ljava/util/Calendar;


# instance fields
.field private final c:LX/11R;

.field private final d:J

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1784377
    const/4 v0, 0x0

    sput-object v0, LX/BRi;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/11R;Ljava/lang/Long;)V
    .locals 2
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784360
    iput-object p1, p0, LX/BRi;->c:LX/11R;

    .line 1784361
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/BRi;->d:J

    .line 1784362
    return-void
.end method

.method private a(Ljava/util/Calendar;)V
    .locals 6

    .prologue
    const/16 v1, 0xc

    .line 1784363
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1784364
    rem-int/lit8 v0, v0, 0xf

    rsub-int/lit8 v0, v0, 0xf

    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 1784365
    sget-object v2, LX/BRi;->b:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    .line 1784366
    invoke-static {v2}, LX/BRi;->c(Ljava/util/Calendar;)V

    .line 1784367
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Calendar;

    .line 1784368
    invoke-static {v3}, LX/BRi;->c(Ljava/util/Calendar;)V

    .line 1784369
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long v2, v4, v2

    .line 1784370
    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v2, v2

    move v0, v2

    .line 1784371
    iput v0, p0, LX/BRi;->e:I

    .line 1784372
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1784373
    iput v0, p0, LX/BRi;->f:I

    .line 1784374
    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1784375
    iput v0, p0, LX/BRi;->g:I

    .line 1784376
    return-void
.end method

.method public static c(Ljava/util/Calendar;)V
    .locals 5

    .prologue
    const/16 v4, 0xe

    const/16 v3, 0xd

    const/16 v2, 0xc

    const/16 v1, 0xb

    .line 1784378
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 1784379
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 1784380
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 1784381
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-virtual {p0, v4, v0}, Ljava/util/Calendar;->set(II)V

    .line 1784382
    return-void
.end method

.method private k()J
    .locals 5

    .prologue
    .line 1784317
    invoke-virtual {p0}, LX/BRi;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1784318
    const-wide/16 v0, 0x0

    .line 1784319
    :goto_0
    return-wide v0

    .line 1784320
    :cond_0
    const/16 v4, 0xc

    const/16 v3, 0xb

    .line 1784321
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1784322
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->clear(I)V

    .line 1784323
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->clear(I)V

    .line 1784324
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->clear(I)V

    .line 1784325
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->clear(I)V

    .line 1784326
    const/4 v1, 0x6

    iget v2, p0, LX/BRi;->e:I

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1784327
    iget v1, p0, LX/BRi;->f:I

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 1784328
    iget v1, p0, LX/BRi;->g:I

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 1784329
    move-object v0, v0

    .line 1784330
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1784354
    sget-object v0, LX/BRi;->b:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1784355
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->add(II)V

    .line 1784356
    invoke-direct {p0, v0}, LX/BRi;->a(Ljava/util/Calendar;)V

    .line 1784357
    return-void
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 1784358
    invoke-direct {p0}, LX/BRi;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1784350
    invoke-direct {p0}, LX/BRi;->k()J

    move-result-wide v0

    .line 1784351
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1784352
    sget-object v0, LX/BRi;->a:Ljava/lang/String;

    .line 1784353
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/BRi;->c:LX/11R;

    sget-object v3, LX/1lB;->DAY_HOUR_FUTURE_STYLE:LX/1lB;

    invoke-virtual {v2, v3, v0, v1}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1784349
    iget v0, p0, LX/BRi;->e:I

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/BRi;->f:I

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/BRi;->g:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1784345
    iput v0, p0, LX/BRi;->e:I

    .line 1784346
    iput v0, p0, LX/BRi;->f:I

    .line 1784347
    iput v0, p0, LX/BRi;->g:I

    .line 1784348
    return-void
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 1784337
    iget-wide v0, p0, LX/BRi;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1784338
    invoke-virtual {p0}, LX/BRi;->g()V

    .line 1784339
    :goto_0
    return-void

    .line 1784340
    :cond_0
    invoke-static {}, LX/BRX;->a()Ljava/util/Calendar;

    move-result-object v0

    .line 1784341
    iget-wide v2, p0, LX/BRi;->d:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1784342
    sget-object v1, LX/BRi;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1784343
    invoke-virtual {p0}, LX/BRi;->g()V

    goto :goto_0

    .line 1784344
    :cond_1
    invoke-direct {p0, v0}, LX/BRi;->a(Ljava/util/Calendar;)V

    goto :goto_0
.end method

.method public final i()Z
    .locals 6

    .prologue
    .line 1784331
    iget-wide v0, p0, LX/BRi;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1784332
    const/4 v0, 0x0

    .line 1784333
    :goto_0
    return v0

    .line 1784334
    :cond_0
    invoke-static {}, LX/BRX;->a()Ljava/util/Calendar;

    move-result-object v0

    .line 1784335
    iget-wide v2, p0, LX/BRi;->d:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1784336
    sget-object v1, LX/BRi;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
