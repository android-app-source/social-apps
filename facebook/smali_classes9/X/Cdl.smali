.class public LX/Cdl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/bluetooth/BluetoothDevice;

.field public final b:[B

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;[B)V
    .locals 2

    .prologue
    .line 1923373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923374
    iput-object p1, p0, LX/Cdl;->a:Landroid/bluetooth/BluetoothDevice;

    .line 1923375
    iput-object p2, p0, LX/Cdl;->b:[B

    .line 1923376
    new-instance v0, Ljava/math/BigInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p2}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cdl;->c:Ljava/lang/String;

    .line 1923377
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1923378
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/Cdl;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, LX/Cdl;->hashCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1923379
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Cdl;->a:Landroid/bluetooth/BluetoothDevice;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/Cdl;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
