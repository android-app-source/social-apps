.class public LX/BzH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/Bz8;

.field public final b:LX/1xu;


# direct methods
.method public constructor <init>(LX/Bz8;LX/1xu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838482
    iput-object p1, p0, LX/BzH;->a:LX/Bz8;

    .line 1838483
    iput-object p2, p0, LX/BzH;->b:LX/1xu;

    .line 1838484
    return-void
.end method

.method public static a(LX/0QB;)LX/BzH;
    .locals 5

    .prologue
    .line 1838485
    const-class v1, LX/BzH;

    monitor-enter v1

    .line 1838486
    :try_start_0
    sget-object v0, LX/BzH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838487
    sput-object v2, LX/BzH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838488
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838489
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838490
    new-instance p0, LX/BzH;

    const-class v3, LX/Bz8;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Bz8;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v4

    check-cast v4, LX/1xu;

    invoke-direct {p0, v3, v4}, LX/BzH;-><init>(LX/Bz8;LX/1xu;)V

    .line 1838491
    move-object v0, p0

    .line 1838492
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838493
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838494
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
