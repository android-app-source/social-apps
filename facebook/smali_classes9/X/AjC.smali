.class public final LX/AjC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public final synthetic d:LX/1du;


# direct methods
.method public constructor <init>(LX/1du;Ljava/lang/String;LX/0TF;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)V
    .locals 0

    .prologue
    .line 1707651
    iput-object p1, p0, LX/AjC;->d:LX/1du;

    iput-object p2, p0, LX/AjC;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AjC;->b:LX/0TF;

    iput-object p4, p0, LX/AjC;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1707652
    iget-object v0, p0, LX/AjC;->d:LX/1du;

    iget-object v1, p0, LX/AjC;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1du;->d(LX/1du;Ljava/lang/String;)V

    .line 1707653
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Fetch curation flow failed: id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/AjC;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " actionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AjC;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1707654
    iget-object v1, p0, LX/AjC;->d:LX/1du;

    iget-object v1, v1, LX/1du;->b:LX/03V;

    sget-object v2, LX/1du;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1707655
    iget-object v0, p0, LX/AjC;->b:LX/0TF;

    if-eqz v0, :cond_0

    .line 1707656
    iget-object v0, p0, LX/AjC;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1707657
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1707658
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1707659
    if-eqz p1, :cond_0

    .line 1707660
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707661
    if-nez v0, :cond_2

    .line 1707662
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Curation result is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/AjC;->onFailure(Ljava/lang/Throwable;)V

    .line 1707663
    :cond_1
    :goto_0
    return-void

    .line 1707664
    :cond_2
    iget-object v0, p0, LX/AjC;->d:LX/1du;

    iget-object v1, p0, LX/AjC;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1du;->d(LX/1du;Ljava/lang/String;)V

    .line 1707665
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707666
    check-cast v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel;->a()Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    move-result-object v0

    .line 1707667
    iget-object v1, p0, LX/AjC;->d:LX/1du;

    iget-object v2, p0, LX/AjC;->a:Ljava/lang/String;

    .line 1707668
    iget-object v3, v1, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1707669
    iget-object v3, v1, LX/1du;->f:Ljava/util/Map;

    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    invoke-interface {v3, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1707670
    :cond_3
    iget-object v3, v1, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1707671
    iget-object v1, p0, LX/AjC;->b:LX/0TF;

    if-eqz v1, :cond_1

    .line 1707672
    iget-object v1, p0, LX/AjC;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
