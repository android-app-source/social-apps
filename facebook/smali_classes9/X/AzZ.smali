.class public LX/AzZ;
.super LX/Aqa;
.source ""


# instance fields
.field private final a:LX/AzS;


# direct methods
.method public constructor <init>(LX/AzS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732664
    invoke-direct {p0}, LX/Aqa;-><init>()V

    .line 1732665
    iput-object p1, p0, LX/AzZ;->a:LX/AzS;

    .line 1732666
    return-void
.end method

.method public static b(LX/0QB;)LX/AzZ;
    .locals 2

    .prologue
    .line 1732667
    new-instance v1, LX/AzZ;

    const-class v0, LX/AzS;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AzS;

    invoke-direct {v1, v0}, LX/AzZ;-><init>(LX/AzS;)V

    .line 1732668
    return-object v1
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 1

    .prologue
    .line 1732669
    new-instance v0, LX/AzY;

    invoke-direct {v0, p2, p1}, LX/AzY;-><init>(Landroid/content/Context;LX/1RN;)V

    return-object v0
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732670
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1732671
    check-cast p1, LX/2xq;

    .line 1732672
    iget-object v0, p0, LX/AzZ;->a:LX/AzS;

    invoke-virtual {v0, p2}, LX/AzS;->a(LX/1RN;)LX/AzR;

    move-result-object v0

    .line 1732673
    iget-object v1, p1, LX/2xq;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732674
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732675
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1732676
    check-cast p1, LX/2xq;

    .line 1732677
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732678
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 2

    .prologue
    .line 1732679
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1732680
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1732681
    check-cast v0, LX/1kW;

    .line 1732682
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1732683
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    .line 1732684
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
