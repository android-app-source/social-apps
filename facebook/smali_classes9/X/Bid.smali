.class public LX/Bid;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/BiW;

.field public final c:LX/1nQ;

.field private final d:LX/0hx;

.field private final e:LX/0So;

.field private final f:LX/1Ck;

.field public final g:LX/0kL;

.field private final h:LX/0tX;

.field public final i:LX/Bib;

.field private j:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1810636
    const-class v0, LX/Bid;

    sput-object v0, LX/Bid;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/BiW;LX/1nQ;LX/0hx;LX/0So;LX/1Ck;LX/0kL;LX/0tX;LX/Bib;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810655
    iput-object p1, p0, LX/Bid;->b:LX/BiW;

    .line 1810656
    iput-object p2, p0, LX/Bid;->c:LX/1nQ;

    .line 1810657
    iput-object p3, p0, LX/Bid;->d:LX/0hx;

    .line 1810658
    iput-object p4, p0, LX/Bid;->e:LX/0So;

    .line 1810659
    iput-object p5, p0, LX/Bid;->f:LX/1Ck;

    .line 1810660
    iput-object p6, p0, LX/Bid;->g:LX/0kL;

    .line 1810661
    iput-object p7, p0, LX/Bid;->h:LX/0tX;

    .line 1810662
    iput-object p8, p0, LX/Bid;->i:LX/Bib;

    .line 1810663
    return-void
.end method

.method public static a(LX/0QB;)LX/Bid;
    .locals 10

    .prologue
    .line 1810651
    new-instance v1, LX/Bid;

    invoke-static {p0}, LX/BiW;->a(LX/0QB;)LX/BiW;

    move-result-object v2

    check-cast v2, LX/BiW;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    invoke-static {p0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v4

    check-cast v4, LX/0hx;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {p0}, LX/Bib;->a(LX/0QB;)LX/Bib;

    move-result-object v9

    check-cast v9, LX/Bib;

    invoke-direct/range {v1 .. v9}, LX/Bid;-><init>(LX/BiW;LX/1nQ;LX/0hx;LX/0So;LX/1Ck;LX/0kL;LX/0tX;LX/Bib;)V

    .line 1810652
    move-object v0, v1

    .line 1810653
    return-object v0
.end method

.method private static a(LX/Bid;LX/0gc;Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 1810664
    const-string v0, "progress_dialog"

    invoke-virtual {p2, p1, v0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1810665
    iget-object v0, p0, LX/Bid;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Bid;->j:J

    .line 1810666
    iget-object v0, p0, LX/Bid;->d:LX/0hx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 1810667
    return-void
.end method

.method public static a$redex0(LX/Bid;Landroid/support/v4/app/DialogFragment;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1810645
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1810646
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1810647
    :cond_0
    iget-wide v0, p0, LX/Bid;->j:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 1810648
    iget-object v0, p0, LX/Bid;->d:LX/0hx;

    iget-object v1, p0, LX/Bid;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Bid;->j:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1810649
    iput-wide v6, p0, LX/Bid;->j:J

    .line 1810650
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/0gc;LX/4EM;LX/Bj7;Landroid/os/Handler;Landroid/net/Uri;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;LX/0oG;)V
    .locals 10

    .prologue
    .line 1810637
    const v0, 0x7f082141

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    .line 1810638
    invoke-static {p0, p1, v2}, LX/Bid;->a(LX/Bid;LX/0gc;Landroid/support/v4/app/DialogFragment;)V

    .line 1810639
    invoke-static {}, LX/Bk4;->a()LX/Bk2;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Bk2;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1810640
    iget-object v1, p0, LX/Bid;->h:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 1810641
    new-instance v0, LX/Bic;

    move-object v1, p0

    move-object/from16 v3, p8

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/Bic;-><init>(LX/Bid;Landroid/support/v4/app/DialogFragment;LX/0oG;LX/Bj7;Landroid/net/Uri;Landroid/os/Handler;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    .line 1810642
    invoke-virtual {p2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v1

    .line 1810643
    iget-object v2, p0, LX/Bid;->f:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-createEvent"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "name"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "start_time"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v9, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1810644
    return-void
.end method
