.class public LX/B4O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B4O;


# instance fields
.field public a:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741595
    return-void
.end method

.method public static a(LX/0QB;)LX/B4O;
    .locals 4

    .prologue
    .line 1741596
    sget-object v0, LX/B4O;->b:LX/B4O;

    if-nez v0, :cond_1

    .line 1741597
    const-class v1, LX/B4O;

    monitor-enter v1

    .line 1741598
    :try_start_0
    sget-object v0, LX/B4O;->b:LX/B4O;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1741599
    if-eqz v2, :cond_0

    .line 1741600
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1741601
    new-instance p0, LX/B4O;

    invoke-direct {p0}, LX/B4O;-><init>()V

    .line 1741602
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    .line 1741603
    iput-object v3, p0, LX/B4O;->a:LX/0Zb;

    .line 1741604
    move-object v0, p0

    .line 1741605
    sput-object v0, LX/B4O;->b:LX/B4O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1741606
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1741607
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1741608
    :cond_1
    sget-object v0, LX/B4O;->b:LX/B4O;

    return-object v0

    .line 1741609
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1741610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1741611
    iget-object v0, p0, LX/B4O;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1741612
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1741613
    const-string v1, "profile_picture_overlay"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1741614
    invoke-virtual {v0, p2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741615
    invoke-virtual {v0, p4, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741616
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1741617
    :cond_0
    return-void
.end method

.method private static a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1741618
    iget-object v2, p0, LX/B4O;->a:LX/0Zb;

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 1741619
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1741620
    const-string v3, "profile_picture_overlay"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1741621
    invoke-virtual {v2, p2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741622
    invoke-virtual {v2, p4, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741623
    invoke-virtual {v2, p6, p7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741624
    invoke-virtual {v2, p8, p9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741625
    invoke-virtual {v2, p10, p11}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741626
    move-object/from16 v0, p12

    move-object/from16 v1, p13

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741627
    move-object/from16 v0, p14

    move-object/from16 v1, p15

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741628
    move-object/from16 v0, p16

    move-object/from16 v1, p17

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741629
    move-object/from16 v0, p18

    move-object/from16 v1, p19

    invoke-virtual {v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741630
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1741631
    :cond_0
    return-void
.end method

.method public static b(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1741632
    const-string v2, "heisman_composer_session_id"

    const-string v4, "profile_pic_frame_id"

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/B4O;->a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741633
    return-void
.end method

.method public static g(LX/B4O;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1741634
    const-string v0, "heisman_composer_session_id"

    .line 1741635
    iget-object v1, p0, LX/B4O;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1741636
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1741637
    const-string v2, "profile_picture_overlay"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1741638
    invoke-virtual {v1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741639
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1741640
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/B4P;Ljava/lang/String;Ljava/lang/String;)V
    .locals 20

    .prologue
    .line 1741641
    const-string v1, "heisman_profile_picture_set"

    const-string v2, "heisman_composer_session_id"

    const-string v4, "profile_pic_frame_id"

    const-string v6, "num_left_swipes"

    invoke-virtual/range {p3 .. p3}, LX/B4P;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "num_right_swipes"

    invoke-virtual/range {p3 .. p3}, LX/B4P;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "num_swipe_direction_changes"

    invoke-virtual/range {p3 .. p3}, LX/B4P;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "num_swipeable_frames"

    invoke-virtual/range {p3 .. p3}, LX/B4P;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "media_type"

    const-string v15, "photo"

    const-string v16, "heisman_photo_source"

    const-string v18, "heisman_entry_point"

    move-object/from16 v0, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v17, p4

    move-object/from16 v19, p5

    invoke-static/range {v0 .. v19}, LX/B4O;->a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741642
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741643
    const-string v1, "goodwill_throwback_share_composer_open_birthday_frame"

    const-string v2, "heisman_composer_session_id"

    const-string v4, "profile_pic_frame_id"

    const-string v6, "heisman_entry_point"

    move-object v0, p0

    move-object v3, p1

    move-object v5, p2

    move-object v7, p3

    .line 1741644
    iget-object p0, v0, LX/B4O;->a:LX/0Zb;

    const/4 p1, 0x0

    invoke-interface {p0, v1, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 1741645
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1741646
    const-string p1, "profile_picture_overlay_birthday"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1741647
    invoke-virtual {p0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741648
    invoke-virtual {p0, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741649
    invoke-virtual {p0, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1741650
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 1741651
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1741652
    const-string v0, "heisman_return_to_camera"

    invoke-static {p0, v0, p1, p2}, LX/B4O;->b(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741653
    return-void
.end method
