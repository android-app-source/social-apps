.class public final enum LX/Ay0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ay0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ay0;

.field public static final enum Burst:LX/Ay0;

.field public static final enum Photo:LX/Ay0;

.field public static final enum Video:LX/Ay0;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1729301
    new-instance v0, LX/Ay0;

    const-string v1, "Photo"

    invoke-direct {v0, v1, v2}, LX/Ay0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ay0;->Photo:LX/Ay0;

    new-instance v0, LX/Ay0;

    const-string v1, "Video"

    invoke-direct {v0, v1, v3}, LX/Ay0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ay0;->Video:LX/Ay0;

    new-instance v0, LX/Ay0;

    const-string v1, "Burst"

    invoke-direct {v0, v1, v4}, LX/Ay0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ay0;->Burst:LX/Ay0;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Ay0;

    sget-object v1, LX/Ay0;->Photo:LX/Ay0;

    aput-object v1, v0, v2

    sget-object v1, LX/Ay0;->Video:LX/Ay0;

    aput-object v1, v0, v3

    sget-object v1, LX/Ay0;->Burst:LX/Ay0;

    aput-object v1, v0, v4

    sput-object v0, LX/Ay0;->$VALUES:[LX/Ay0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1729302
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ay0;
    .locals 1

    .prologue
    .line 1729303
    const-class v0, LX/Ay0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ay0;

    return-object v0
.end method

.method public static values()[LX/Ay0;
    .locals 1

    .prologue
    .line 1729304
    sget-object v0, LX/Ay0;->$VALUES:[LX/Ay0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ay0;

    return-object v0
.end method
