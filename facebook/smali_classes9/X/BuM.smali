.class public final LX/BuM;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7MG;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;)V
    .locals 0

    .prologue
    .line 1830868
    iput-object p1, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7MG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1830869
    const-class v0, LX/7MG;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1830870
    check-cast p1, LX/7MG;

    .line 1830871
    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->y:Ljava/util/ArrayList;

    iget-object v1, p1, LX/7MG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1830872
    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->y:Ljava/util/ArrayList;

    iget-object v1, p1, LX/7MG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1830873
    :cond_0
    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    iget-object v1, p1, LX/7MG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1830874
    :cond_1
    :goto_0
    return-void

    .line 1830875
    :cond_2
    iget-object v1, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830876
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1830877
    check-cast v0, LX/16n;

    invoke-virtual {v1, v0}, LX/BuJ;->a(LX/16n;)V

    .line 1830878
    iget-object v0, p0, LX/BuM;->a:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    .line 1830879
    iget-object v1, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 1830880
    :goto_1
    goto :goto_0

    .line 1830881
    :cond_3
    new-instance v1, LX/81S;

    invoke-direct {v1}, LX/81S;-><init>()V

    move-object v1, v1

    .line 1830882
    const-string v2, "video_id"

    iget-object v3, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1830883
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1830884
    iget-object v2, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    .line 1830885
    iget-object v3, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->u:LX/1Ck;

    const-string p0, "fetchVideoBroadcastPlayCount"

    iget-object p1, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->s:LX/0tX;

    invoke-virtual {p1, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance p1, LX/BuL;

    invoke-direct {p1, v0, v2}, LX/BuL;-><init>(Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;Ljava/lang/String;)V

    invoke-virtual {v3, p0, v1, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1
.end method
