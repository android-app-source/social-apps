.class public LX/Byn;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field private final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final b:Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 1837822
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1837823
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1837824
    const v0, 0x7f020a3c

    invoke-virtual {p0, v0}, LX/Byn;->setBackgroundResource(I)V

    .line 1837825
    const v0, 0x7f0d2e1a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Byn;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1837826
    iget-object v0, p0, LX/Byn;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v1, 0x7f0a045d

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    .line 1837827
    const v0, 0x7f0d2e21

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    iput-object v0, p0, LX/Byn;->b:Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    .line 1837828
    const v0, 0x7f0d2e1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Byn;->c:LX/0am;

    .line 1837829
    iget-object v0, p0, LX/Byn;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837830
    iget-object v0, p0, LX/Byn;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f081014

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1837831
    :cond_0
    const v0, 0x7f0d2e19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    .line 1837832
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1837833
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget-object v1, LX/1vY;->SUB_ATTACHMENT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1837834
    :cond_1
    sget-object v0, LX/1vY;->SUBSTORY:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1837835
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1837836
    iget-boolean v0, p0, LX/Byn;->d:Z

    return v0
.end method

.method public getFooter()Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;
    .locals 1

    .prologue
    .line 1837837
    iget-object v0, p0, LX/Byn;->b:Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4c4ec4a4    # 5.4203024E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837838
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1837839
    const/4 v1, 0x1

    .line 1837840
    iput-boolean v1, p0, LX/Byn;->d:Z

    .line 1837841
    const/16 v1, 0x2d

    const v2, 0x4c6c4ec9    # 6.194666E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3ed75b70

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837842
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1837843
    const/4 v1, 0x0

    .line 1837844
    iput-boolean v1, p0, LX/Byn;->d:Z

    .line 1837845
    const/16 v1, 0x2d

    const v2, -0x44c34f89

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setSideImageController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1837846
    iget-object v0, p0, LX/Byn;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1837847
    if-eqz p1, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v0, p0}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 1837848
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1837849
    return-void

    .line 1837850
    :cond_0
    const/16 p0, 0x8

    goto :goto_0
.end method
