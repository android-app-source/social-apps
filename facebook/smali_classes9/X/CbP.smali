.class public final LX/CbP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CbS;


# direct methods
.method public constructor <init>(LX/CbS;)V
    .locals 0

    .prologue
    .line 1919500
    iput-object p1, p0, LX/CbP;->a:LX/CbS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xf95a91a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919501
    iget-object v1, p0, LX/CbP;->a:LX/CbS;

    invoke-static {v1}, LX/CbS;->c(LX/CbS;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1919502
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    iget-object v2, p0, LX/CbP;->a:LX/CbS;

    iget-object v2, v2, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1919503
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 1919504
    move-object v1, v1

    .line 1919505
    iget-object v2, p0, LX/CbP;->a:LX/CbS;

    iget-object v2, v2, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getMentionsEntityRanges()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1919506
    iput-object v2, v1, LX/173;->e:LX/0Px;

    .line 1919507
    move-object v1, v1

    .line 1919508
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1919509
    iget-object v2, p0, LX/CbP;->a:LX/CbS;

    iget-object v2, v2, LX/CbS;->h:Lcom/google/common/util/concurrent/SettableFuture;

    const v3, -0x611307e

    invoke-static {v2, v1, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1919510
    :cond_0
    iget-object v1, p0, LX/CbP;->a:LX/CbS;

    invoke-virtual {v1}, LX/CbS;->dismiss()V

    .line 1919511
    const v1, -0x58af69a9

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
