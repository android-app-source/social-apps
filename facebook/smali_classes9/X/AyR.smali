.class public LX/AyR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private b:LX/AyQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1730020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730021
    iput-object p1, p0, LX/AyR;->a:LX/0ad;

    .line 1730022
    return-void
.end method

.method public static a(LX/AyR;Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1730023
    invoke-interface {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;->iq_()LX/Ay0;

    move-result-object v1

    sget-object v2, LX/Ay0;->Video:LX/Ay0;

    if-eq v1, v2, :cond_1

    .line 1730024
    :cond_0
    :goto_0
    return v0

    .line 1730025
    :cond_1
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->d()J

    move-result-wide v2

    .line 1730026
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    iget-object v1, p0, LX/AyR;->b:LX/AyQ;

    iget-wide v4, v1, LX/AyQ;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;J)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1730027
    iget-object v1, p0, LX/AyR;->b:LX/AyQ;

    if-nez v1, :cond_0

    .line 1730028
    new-instance v1, LX/AyQ;

    iget-object v2, p0, LX/AyR;->a:LX/0ad;

    invoke-direct {v1, v2}, LX/AyQ;-><init>(LX/0ad;)V

    iput-object v1, p0, LX/AyR;->b:LX/AyQ;

    .line 1730029
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->c()J

    move-result-wide v2

    sub-long v2, p2, v2

    iget-object v1, p0, LX/AyR;->b:LX/AyQ;

    iget-wide v4, v1, LX/AyQ;->a:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    move-object p1, v0

    .line 1730030
    :cond_1
    :goto_0
    return-object p1

    .line 1730031
    :cond_2
    const/4 v1, 0x0

    .line 1730032
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1730033
    invoke-static {p0, v1}, LX/AyR;->a(LX/AyR;Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1730034
    add-int/lit8 v1, v2, 0x1

    .line 1730035
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_1

    .line 1730036
    :cond_3
    move v1, v2

    .line 1730037
    if-eqz v1, :cond_4

    iget-object v2, p0, LX/AyR;->b:LX/AyQ;

    iget v2, v2, LX/AyQ;->b:I

    if-ge v1, v2, :cond_5

    :cond_4
    move-object p1, v0

    .line 1730038
    goto :goto_0

    .line 1730039
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eq v1, v0, :cond_1

    .line 1730040
    new-instance v0, LX/Ay6;

    invoke-direct {v0}, LX/Ay6;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v1

    .line 1730041
    iput-object v1, v0, LX/Ay6;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1730042
    move-object v2, v0

    .line 1730043
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1730044
    invoke-static {p0, v0}, LX/AyR;->a(LX/AyR;Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1730045
    invoke-virtual {v2, v0}, LX/Ay6;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)LX/Ay6;

    .line 1730046
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1730047
    :cond_7
    invoke-virtual {v2}, LX/Ay6;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    move-object p1, v0

    .line 1730048
    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_2
.end method
