.class public final LX/Auu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/Auw;


# direct methods
.method public constructor <init>(LX/Auw;)V
    .locals 0

    .prologue
    .line 1723408
    iput-object p1, p0, LX/Auu;->a:LX/Auw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 1723409
    iget-object v0, p0, LX/Auu;->a:LX/Auw;

    iget-object v0, v0, LX/Auw;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->isKeyboardOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1723410
    iget-object v0, p0, LX/Auu;->a:LX/Auw;

    invoke-virtual {v0}, LX/Auw;->a()V

    .line 1723411
    :cond_0
    iget-object v0, p0, LX/Auu;->a:LX/Auw;

    iget-object v0, v0, LX/Auw;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1723412
    return-void
.end method
