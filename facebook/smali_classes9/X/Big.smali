.class public final LX/Big;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bih;


# direct methods
.method public constructor <init>(LX/Bih;)V
    .locals 0

    .prologue
    .line 1810702
    iput-object p1, p0, LX/Big;->a:LX/Bih;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6400c4a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810703
    check-cast p1, Lcom/facebook/resources/ui/FbCheckedTextView;

    .line 1810704
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->toggle()V

    .line 1810705
    iget-object v1, p0, LX/Big;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->isChecked()Z

    move-result v2

    invoke-static {v2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/events/create/EventCompositionModel;->a(LX/03R;)Lcom/facebook/events/create/EventCompositionModel;

    .line 1810706
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1810707
    iget-object v1, p0, LX/Big;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->c:Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbCheckedTextView;->setChecked(Z)V

    .line 1810708
    iget-object v1, p0, LX/Big;->a:LX/Bih;

    iget-object v1, v1, LX/Bih;->e:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810709
    iput-boolean v4, v1, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    .line 1810710
    :cond_0
    const v1, 0x441ac9cd

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
