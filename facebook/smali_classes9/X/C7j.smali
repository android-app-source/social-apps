.class public LX/C7j;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7h;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1851731
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C7j;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851728
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851729
    iput-object p1, p0, LX/C7j;->b:LX/0Ot;

    .line 1851730
    return-void
.end method

.method public static a(LX/0QB;)LX/C7j;
    .locals 4

    .prologue
    .line 1851732
    const-class v1, LX/C7j;

    monitor-enter v1

    .line 1851733
    :try_start_0
    sget-object v0, LX/C7j;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851734
    sput-object v2, LX/C7j;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851735
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851736
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851737
    new-instance v3, LX/C7j;

    const/16 p0, 0x1f9d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7j;-><init>(LX/0Ot;)V

    .line 1851738
    move-object v0, v3

    .line 1851739
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851740
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851741
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851742
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1851711
    check-cast p2, LX/C7i;

    .line 1851712
    iget-object v0, p0, LX/C7j;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;

    iget-object v1, p2, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 p0, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 1851713
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1d56

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1d56

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020a43

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 1851714
    const v3, -0x48f5e690

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1851715
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x6

    const v5, 0x7f0b1d5e

    invoke-interface {v2, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    const/4 p2, 0x1

    .line 1851716
    iget-object v5, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->a:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v6, v5}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    new-instance v6, LX/4Ab;

    invoke-direct {v6}, LX/4Ab;-><init>()V

    .line 1851717
    iput-boolean p2, v6, LX/4Ab;->b:Z

    .line 1851718
    move-object v6, v6

    .line 1851719
    invoke-virtual {v5, v6}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v5

    const v6, 0x7f020c6c

    invoke-virtual {v5, v6}, LX/1nw;->h(I)LX/1nw;

    move-result-object v5

    sget-object v6, LX/1Up;->a:LX/1Up;

    invoke-virtual {v5, v6}, LX/1nw;->b(LX/1Up;)LX/1nw;

    move-result-object v5

    sget-object v6, LX/1Up;->h:LX/1Up;

    invoke-virtual {v5, v6}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f020c12

    invoke-interface {v5, v6}, LX/1Di;->x(I)LX/1Di;

    move-result-object v5

    const/16 v6, 0x8

    invoke-interface {v5, v6, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1d5f

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1d5f

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    move-object v2, v5

    .line 1851720
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x2

    .line 1851721
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f082a03

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00ab

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b1d54

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x7f0b1d52

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1851722
    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v4, 0x7f082a04

    new-array v5, p0, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, LX/1De;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1851723
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00a4

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b1d50

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    move-object v4, v5

    .line 1851724
    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {v1}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLGroup;)Ljava/util/List;

    move-result-object v4

    .line 1851725
    iget-object v5, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->b:LX/8yV;

    invoke-virtual {v5, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d65

    invoke-virtual {v5, v6}, LX/8yU;->l(I)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d64

    invoke-virtual {v5, v6}, LX/8yU;->h(I)LX/8yU;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, LX/8yU;->m(I)LX/8yU;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/8yU;->d(F)LX/8yU;

    move-result-object v5

    const v6, 0x7f0b1d66

    invoke-virtual {v5, v6}, LX/8yU;->k(I)LX/8yU;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x7

    const v7, 0x7f0b1d52

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v4, v5

    .line 1851726
    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1851727
    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1851700
    invoke-static {}, LX/1dS;->b()V

    .line 1851701
    iget v0, p1, LX/1dQ;->b:I

    .line 1851702
    packed-switch v0, :pswitch_data_0

    .line 1851703
    :goto_0
    return-object v2

    .line 1851704
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1851705
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1851706
    check-cast v1, LX/C7i;

    .line 1851707
    iget-object v3, p0, LX/C7j;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;

    iget-object v4, v1, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1851708
    iget-object p1, v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1g8;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object p2

    const-string p0, "gmw_unit_end_card_click"

    invoke-virtual {p1, p2, p0}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1851709
    iget-object p1, v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->c:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->C:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p2, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1851710
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x48f5e690
        :pswitch_0
    .end packed-switch
.end method
