.class public final LX/C0q;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMedia;

.field public c:Z

.field public d:LX/1f6;

.field public e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/1xC;


# direct methods
.method public constructor <init>(LX/1xC;)V
    .locals 1

    .prologue
    .line 1841529
    iput-object p1, p0, LX/C0q;->f:LX/1xC;

    .line 1841530
    move-object v0, p1

    .line 1841531
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1841532
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1841533
    const-string v0, "ObjectionableContentMediaWithWarningComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1841534
    if-ne p0, p1, :cond_1

    .line 1841535
    :cond_0
    :goto_0
    return v0

    .line 1841536
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1841537
    goto :goto_0

    .line 1841538
    :cond_3
    check-cast p1, LX/C0q;

    .line 1841539
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1841540
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1841541
    if-eq v2, v3, :cond_0

    .line 1841542
    iget-object v2, p0, LX/C0q;->a:LX/1Pb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0q;->a:LX/1Pb;

    iget-object v3, p1, LX/C0q;->a:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1841543
    goto :goto_0

    .line 1841544
    :cond_5
    iget-object v2, p1, LX/C0q;->a:LX/1Pb;

    if-nez v2, :cond_4

    .line 1841545
    :cond_6
    iget-object v2, p0, LX/C0q;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C0q;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-object v3, p1, LX/C0q;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1841546
    goto :goto_0

    .line 1841547
    :cond_8
    iget-object v2, p1, LX/C0q;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    if-nez v2, :cond_7

    .line 1841548
    :cond_9
    iget-boolean v2, p0, LX/C0q;->c:Z

    iget-boolean v3, p1, LX/C0q;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1841549
    goto :goto_0

    .line 1841550
    :cond_a
    iget-object v2, p0, LX/C0q;->d:LX/1f6;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/C0q;->d:LX/1f6;

    iget-object v3, p1, LX/C0q;->d:LX/1f6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1841551
    goto :goto_0

    .line 1841552
    :cond_c
    iget-object v2, p1, LX/C0q;->d:LX/1f6;

    if-nez v2, :cond_b

    .line 1841553
    :cond_d
    iget-object v2, p0, LX/C0q;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/C0q;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C0q;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1841554
    goto :goto_0

    .line 1841555
    :cond_e
    iget-object v2, p1, LX/C0q;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
