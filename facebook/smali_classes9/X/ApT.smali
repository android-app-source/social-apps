.class public LX/ApT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1716080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1X1;LX/1X1;LX/1dQ;Z)LX/1Dg;
    .locals 9
    .param p1    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/hscroll/annotations/FigHscrollType;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;",
            "LX/1X1",
            "<",
            "LX/Apj;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1716081
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 1716082
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 1716083
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 1716084
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 1716085
    const/4 v1, 0x0

    .line 1716086
    :goto_0
    return-object v1

    .line 1716087
    :cond_0
    if-eqz p5, :cond_1

    const/4 v1, 0x1

    move v3, v1

    .line 1716088
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 1716089
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported H-Scroll type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0zj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1716090
    :cond_1
    const/4 v1, 0x0

    move v3, v1

    goto :goto_1

    .line 1716091
    :pswitch_0
    if-eqz p6, :cond_2

    const/4 v1, 0x1

    .line 1716092
    :goto_2
    invoke-static {p0}, LX/ApW;->c(LX/1De;)LX/ApU;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/ApU;->a(Ljava/lang/CharSequence;)LX/ApU;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/ApU;->b(Ljava/lang/CharSequence;)LX/ApU;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/ApU;->c(Ljava/lang/CharSequence;)LX/ApU;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, LX/ApU;->a(Z)LX/ApU;

    move-result-object v2

    move-object v8, v2

    move v2, v1

    move-object v1, v8

    .line 1716093
    :goto_3
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020b09

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-interface {v4, v0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    const v7, 0x7f0b1169

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x5

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    :goto_4
    invoke-interface {v5, v6, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    if-nez v2, :cond_5

    const/4 v3, 0x0

    :goto_5
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v1, v5}, LX/1Di;->e(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    if-eqz v2, :cond_6

    const v1, 0x7f0b1169

    :goto_6
    invoke-interface {v5, v6, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_0

    .line 1716094
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1716095
    :pswitch_1
    if-nez v3, :cond_3

    if-eqz p6, :cond_3

    const/4 v1, 0x1

    .line 1716096
    :goto_7
    invoke-static {p0}, LX/Apd;->c(LX/1De;)LX/Apb;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/Apb;->a(Ljava/lang/CharSequence;)LX/Apb;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/Apb;->b(Ljava/lang/CharSequence;)LX/Apb;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/Apb;->c(Ljava/lang/CharSequence;)LX/Apb;

    move-result-object v2

    move-object v8, v2

    move v2, v1

    move-object v1, v8

    .line 1716097
    goto/16 :goto_3

    .line 1716098
    :cond_3
    const/4 v1, 0x0

    goto :goto_7

    .line 1716099
    :cond_4
    const v3, 0x7f0b1169

    goto :goto_4

    :cond_5
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x7

    const v7, 0x7f0b1169

    invoke-interface {v3, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
