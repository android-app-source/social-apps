.class public final LX/Cax;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JW;


# instance fields
.field public final synthetic a:LX/Caz;


# direct methods
.method public constructor <init>(LX/Caz;)V
    .locals 0

    .prologue
    .line 1918756
    iput-object p1, p0, LX/Cax;->a:LX/Caz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1918740
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1918741
    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1918742
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1918743
    goto :goto_0

    .line 1918744
    :cond_2
    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918745
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    .line 1918746
    goto :goto_1

    .line 1918747
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1918748
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1918749
    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1918750
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1918751
    goto :goto_0

    .line 1918752
    :cond_2
    iget-object v0, p0, LX/Cax;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918753
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, -0x2e4d6bd6

    if-ne v6, v7, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    .line 1918754
    goto :goto_1

    .line 1918755
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2
.end method
