.class public LX/C7v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/C7y;


# direct methods
.method public constructor <init>(LX/C7y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1852038
    iput-object p1, p0, LX/C7v;->a:LX/C7y;

    .line 1852039
    return-void
.end method

.method public static a(LX/0QB;)LX/C7v;
    .locals 4

    .prologue
    .line 1852040
    const-class v1, LX/C7v;

    monitor-enter v1

    .line 1852041
    :try_start_0
    sget-object v0, LX/C7v;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852042
    sput-object v2, LX/C7v;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852043
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852044
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852045
    new-instance p0, LX/C7v;

    invoke-static {v0}, LX/C7y;->a(LX/0QB;)LX/C7y;

    move-result-object v3

    check-cast v3, LX/C7y;

    invoke-direct {p0, v3}, LX/C7v;-><init>(LX/C7y;)V

    .line 1852046
    move-object v0, p0

    .line 1852047
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852048
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852049
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852050
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
