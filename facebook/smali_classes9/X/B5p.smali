.class public LX/B5p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5o;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/B5m;

.field public final c:I

.field public final d:LX/1po;

.field public final e:Landroid/net/Uri;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:I

.field public j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/B5m;ILX/1po;Landroid/net/Uri;LX/0am;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Landroid/net/Uri;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/B5m;",
            "I",
            "LX/1po;",
            "Landroid/net/Uri;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Landroid/net/Uri;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1745987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745988
    iput-object p1, p0, LX/B5p;->a:Ljava/lang/String;

    .line 1745989
    iput-object p2, p0, LX/B5p;->b:LX/B5m;

    .line 1745990
    iput p3, p0, LX/B5p;->c:I

    .line 1745991
    iput-object p4, p0, LX/B5p;->d:LX/1po;

    .line 1745992
    iput-object p5, p0, LX/B5p;->e:Landroid/net/Uri;

    .line 1745993
    iput-object p6, p0, LX/B5p;->j:LX/0am;

    .line 1745994
    iput-object p7, p0, LX/B5p;->f:Ljava/lang/String;

    .line 1745995
    iput-object p8, p0, LX/B5p;->g:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1745996
    iput-object p9, p0, LX/B5p;->h:Landroid/net/Uri;

    .line 1745997
    iput p10, p0, LX/B5p;->i:I

    .line 1745998
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;LX/B5m;ILX/1po;Landroid/net/Uri;LX/0am;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Landroid/net/Uri;IB)V
    .locals 0

    .prologue
    .line 1745999
    invoke-direct/range {p0 .. p10}, LX/B5p;-><init>(Ljava/lang/String;LX/B5m;ILX/1po;Landroid/net/Uri;LX/0am;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Landroid/net/Uri;I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1746000
    iget-object v0, p0, LX/B5p;->a:Ljava/lang/String;

    return-object v0
.end method
