.class public LX/BbD;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BbD;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800504
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1800505
    sget-object v0, LX/0ax;->iC:Ljava/lang/String;

    const-string v1, "{post_id}"

    const-string v2, "{entry_point}"

    const-string v3, "{funnel_logging_tags}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1800506
    return-void
.end method

.method public static a(LX/0QB;)LX/BbD;
    .locals 3

    .prologue
    .line 1800507
    sget-object v0, LX/BbD;->a:LX/BbD;

    if-nez v0, :cond_1

    .line 1800508
    const-class v1, LX/BbD;

    monitor-enter v1

    .line 1800509
    :try_start_0
    sget-object v0, LX/BbD;->a:LX/BbD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1800510
    if-eqz v2, :cond_0

    .line 1800511
    :try_start_1
    new-instance v0, LX/BbD;

    invoke-direct {v0}, LX/BbD;-><init>()V

    .line 1800512
    move-object v0, v0

    .line 1800513
    sput-object v0, LX/BbD;->a:LX/BbD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1800514
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1800515
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1800516
    :cond_1
    sget-object v0, LX/BbD;->a:LX/BbD;

    return-object v0

    .line 1800517
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1800518
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
