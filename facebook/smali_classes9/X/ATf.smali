.class public final LX/ATf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8G6;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:LX/ATg;


# direct methods
.method public constructor <init>(LX/ATg;Ljava/util/List;Ljava/util/List;Ljava/util/List;LX/0Px;)V
    .locals 0

    .prologue
    .line 1676254
    iput-object p1, p0, LX/ATf;->e:LX/ATg;

    iput-object p2, p0, LX/ATf;->a:Ljava/util/List;

    iput-object p3, p0, LX/ATf;->b:Ljava/util/List;

    iput-object p4, p0, LX/ATf;->c:Ljava/util/List;

    iput-object p5, p0, LX/ATf;->d:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 5

    .prologue
    .line 1676255
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676256
    :cond_0
    :goto_0
    return-void

    .line 1676257
    :cond_1
    iget-object v0, p0, LX/ATf;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1676258
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->D:LX/8GP;

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget-object v2, p0, LX/ATf;->e:LX/ATg;

    iget-object v2, v2, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    invoke-virtual {v0, v1, v2}, LX/8GP;->a(II)LX/8GO;

    move-result-object v3

    .line 1676259
    iget-object v0, p0, LX/ATf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    .line 1676260
    iget-object v0, p0, LX/ATf;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1676261
    iget-object v1, p0, LX/ATf;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, LX/8GO;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;Ljava/lang/String;)LX/8GO;

    goto :goto_2

    .line 1676262
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 1676263
    :cond_3
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATf;->d:LX/0Px;

    invoke-virtual {v3, v1}, LX/8GO;->a(LX/0Px;)LX/8GO;

    move-result-object v1

    invoke-virtual {v1}, LX/8GO;->b()LX/0Px;

    move-result-object v1

    .line 1676264
    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676265
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-boolean v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aM:Z

    if-nez v1, :cond_5

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->c:Ljava/lang/String;

    :goto_3
    invoke-virtual {v0, v2, v1}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1676266
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)LX/0am;

    move-result-object v1

    .line 1676267
    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676268
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->e:LX/9dY;

    if-eqz v0, :cond_0

    .line 1676269
    iget-object v0, p0, LX/ATf;->e:LX/ATg;

    iget-object v0, v0, LX/ATg;->e:LX/9dY;

    iget-object v1, p0, LX/ATf;->e:LX/ATg;

    iget-object v1, v1, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676270
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676271
    iget-object v2, v0, LX/9dY;->e:Ljava/util/Set;

    if-eqz v2, :cond_4

    iget-boolean v2, v0, LX/9dY;->f:Z

    if-eqz v2, :cond_6

    .line 1676272
    :cond_4
    :goto_4
    goto/16 :goto_0

    .line 1676273
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 1676274
    :cond_6
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_4

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1676275
    iget-object p0, v0, LX/9dY;->e:Ljava/util/Set;

    .line 1676276
    iget-object p1, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, p1

    .line 1676277
    invoke-interface {p0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1676278
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/9dY;->f:Z

    goto :goto_4

    .line 1676279
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5
.end method
