.class public final LX/BbN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800747
    iput-object p1, p0, LX/BbN;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iput-object p2, p0, LX/BbN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x348d3af4

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800748
    iget-object v1, p0, LX/BbN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1800749
    if-nez v1, :cond_0

    .line 1800750
    const v1, -0x31d6f750

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800751
    :goto_0
    return-void

    .line 1800752
    :cond_0
    new-instance v2, LX/31Y;

    iget-object v3, p0, LX/BbN;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v3, v3, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/high16 v3, 0x1040000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    iget-object v3, p0, LX/BbN;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    iget-object v4, p0, LX/BbN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800753
    iget-boolean p1, v3, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->t:Z

    if-eqz p1, :cond_1

    .line 1800754
    const p1, 0x7f0829ef

    .line 1800755
    :goto_1
    move v3, p1

    .line 1800756
    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f0829ed

    new-instance v4, LX/BbL;

    invoke-direct {v4, p0, v1}, LX/BbL;-><init>(LX/BbN;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 1800757
    const v1, -0x2a9ec745

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1800758
    :cond_1
    invoke-static {v4}, LX/BbT;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1800759
    const p1, 0x7f0829f0

    goto :goto_1

    .line 1800760
    :cond_2
    const p1, 0x7f0829ee

    goto :goto_1
.end method
