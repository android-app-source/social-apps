.class public final LX/AlF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/AlG;


# direct methods
.method public constructor <init>(LX/AlG;)V
    .locals 0

    .prologue
    .line 1709559
    iput-object p1, p0, LX/AlF;->a:LX/AlG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1709560
    iget-object v0, p0, LX/AlF;->a:LX/AlG;

    .line 1709561
    iget-object v1, v0, LX/AlG;->d:Landroid/view/View;

    iget-object v2, v0, LX/AlG;->e:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1709562
    iget-object v1, v0, LX/AlG;->e:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 1709563
    iget-object v2, v0, LX/AlG;->e:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    .line 1709564
    iget-object v3, v0, LX/AlG;->f:Landroid/graphics/Rect;

    iget-object v4, v0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object p0, v0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p0, v2

    invoke-virtual {v3, v1, v2, v4, p0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1709565
    iget-object v1, v0, LX/AlG;->f:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    move v1, v1

    .line 1709566
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1709567
    :goto_0
    const/4 v1, 0x1

    move v0, v1

    .line 1709568
    return v0

    .line 1709569
    :pswitch_0
    iget-object v2, v0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 1709570
    :pswitch_1
    if-eqz v1, :cond_0

    .line 1709571
    invoke-static {v0}, LX/AlG;->e(LX/AlG;)V

    .line 1709572
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 1709573
    :cond_0
    :pswitch_2
    iget-object v1, v0, LX/AlG;->d:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
