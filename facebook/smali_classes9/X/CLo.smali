.class public final LX/CLo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1879518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1879519
    check-cast p1, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;

    check-cast p2, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;

    .line 1879520
    iget-wide v4, p1, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    move-wide v0, v4

    .line 1879521
    iget-wide v4, p2, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    move-wide v2, v4

    .line 1879522
    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1879523
    const/4 v0, 0x1

    .line 1879524
    :goto_0
    return v0

    .line 1879525
    :cond_0
    iget-wide v4, p1, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    move-wide v0, v4

    .line 1879526
    iget-wide v4, p2, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    move-wide v2, v4

    .line 1879527
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1879528
    const/4 v0, -0x1

    goto :goto_0

    .line 1879529
    :cond_1
    sget-object v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->a:Ljava/util/Comparator;

    .line 1879530
    iget-object v1, p1, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v1, v1

    .line 1879531
    iget-object v2, p2, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v2, v2

    .line 1879532
    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
