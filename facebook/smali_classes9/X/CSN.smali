.class public LX/CSN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/CSN;


# instance fields
.field public a:LX/CSM;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1894508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894509
    sget-object v0, LX/CSM;->NONE:LX/CSM;

    iput-object v0, p0, LX/CSN;->a:LX/CSM;

    .line 1894510
    iput-object p1, p0, LX/CSN;->b:LX/0if;

    .line 1894511
    return-void
.end method

.method public static a(LX/0QB;)LX/CSN;
    .locals 4

    .prologue
    .line 1894519
    sget-object v0, LX/CSN;->c:LX/CSN;

    if-nez v0, :cond_1

    .line 1894520
    const-class v1, LX/CSN;

    monitor-enter v1

    .line 1894521
    :try_start_0
    sget-object v0, LX/CSN;->c:LX/CSN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1894522
    if-eqz v2, :cond_0

    .line 1894523
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1894524
    new-instance p0, LX/CSN;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/CSN;-><init>(LX/0if;)V

    .line 1894525
    move-object v0, p0

    .line 1894526
    sput-object v0, LX/CSN;->c:LX/CSN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1894527
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1894528
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1894529
    :cond_1
    sget-object v0, LX/CSN;->c:LX/CSN;

    return-object v0

    .line 1894530
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1894531
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/CSN;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1894517
    iget-object v0, p0, LX/CSN;->b:LX/0if;

    sget-object v1, LX/0ig;->ag:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1894518
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1894512
    if-eqz p2, :cond_1

    sget-object v0, LX/CSM;->SEEALL_WITH_TAB:LX/CSM;

    :goto_0
    iput-object v0, p0, LX/CSN;->a:LX/CSM;

    .line 1894513
    if-nez p2, :cond_0

    .line 1894514
    iget-object v0, p0, LX/CSN;->b:LX/0if;

    sget-object v1, LX/0ig;->ag:LX/0ih;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "seeAll_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1894515
    :cond_0
    return-void

    .line 1894516
    :cond_1
    sget-object v0, LX/CSM;->SEEALL_NO_TAB:LX/CSM;

    goto :goto_0
.end method
