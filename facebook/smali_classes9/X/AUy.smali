.class public final LX/AUy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field public final synthetic a:LX/AV1;


# direct methods
.method public constructor <init>(LX/AV1;)V
    .locals 0

    .prologue
    .line 1678494
    iput-object p1, p0, LX/AUy;->a:LX/AV1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 6

    .prologue
    .line 1678495
    iget-object v0, p0, LX/AUy;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->w:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AwB;

    .line 1678496
    iget-object v2, v0, LX/AwB;->c:LX/AwC;

    iget-object v2, v2, LX/AwC;->f:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ArrayBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    .line 1678497
    if-nez v2, :cond_2

    .line 1678498
    :goto_1
    goto :goto_0

    .line 1678499
    :cond_0
    iget-object v0, p0, LX/AUy;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->m:LX/BAF;

    .line 1678500
    iget-object v1, v0, LX/BAF;->a:LX/BA7;

    if-eqz v1, :cond_1

    .line 1678501
    iget-object v1, v0, LX/BAF;->a:LX/BA7;

    .line 1678502
    iget-object v0, v1, LX/BA7;->a:LX/BA8;

    invoke-virtual {v0, p1}, LX/BA8;->a([B)V

    .line 1678503
    :cond_1
    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 1678504
    return-void

    .line 1678505
    :cond_2
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1678506
    const/4 v3, 0x0

    iget v4, v0, LX/AwB;->a:I

    iget v5, v0, LX/AwB;->b:I

    mul-int/2addr v4, v5

    invoke-virtual {v2, p1, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1678507
    iget-object v3, v0, LX/AwB;->c:LX/AwC;

    iget-object v3, v3, LX/AwC;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectFormatController$3$1;

    invoke-direct {v4, v0, v2}, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectFormatController$3$1;-><init>(LX/AwB;Ljava/nio/ByteBuffer;)V

    const v2, 0x5b3db66

    invoke-static {v3, v4, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method
