.class public LX/Bln;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/events/planning/CalendarRange;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1816637
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1816638
    const-class p1, LX/Bln;

    invoke-static {p1, p0}, LX/Bln;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1816639
    const p1, 0x7f030f8b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1816640
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/Bln;->setOrientation(I)V

    .line 1816641
    const p1, 0x7f0d2589

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/Bln;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1816642
    const p1, 0x7f0d258b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/Bln;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1816643
    return-void
.end method

.method private a(Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V
    .locals 4

    .prologue
    .line 1816635
    iget-object v0, p0, LX/Bln;->a:LX/6RZ;

    const/4 v1, 0x0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1816636
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bln;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object p0

    check-cast p0, LX/6RZ;

    iput-object p0, p1, LX/Bln;->a:LX/6RZ;

    return-void
.end method


# virtual methods
.method public getEndTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 1816630
    iget-object v0, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816631
    iget-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    move-object v0, v1

    .line 1816632
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816633
    iget-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    move-object v0, v1

    .line 1816634
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public getPollPosition()I
    .locals 1

    .prologue
    .line 1816629
    iget v0, p0, LX/Bln;->f:I

    return v0
.end method

.method public getStartAndEndTimeViewActivityId()I
    .locals 1

    .prologue
    .line 1816628
    iget v0, p0, LX/Bln;->e:I

    return v0
.end method

.method public getStartTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 1816623
    iget-object v0, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816624
    iget-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    move-object v0, v1

    .line 1816625
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816626
    iget-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    move-object v0, v1

    .line 1816627
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1816616
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/os/Bundle;

    if-eq v0, v1, :cond_0

    .line 1816617
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1816618
    :goto_0
    return-void

    .line 1816619
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 1816620
    const-string v0, "EventsPlanningStartAndEndTimeView_superState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1816621
    const-string v0, "EventsPlanningStartAndEndTimeView_calendarRange"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/planning/CalendarRange;

    .line 1816622
    invoke-virtual {p0, v0}, LX/Bln;->setCalendarRange(Lcom/facebook/events/planning/CalendarRange;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1816611
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1816612
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1816613
    const-string v2, "EventsPlanningStartAndEndTimeView_superState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1816614
    const-string v0, "EventsPlanningStartAndEndTimeView_calendarRange"

    iget-object v2, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1816615
    return-object v1
.end method

.method public setCalendarRange(Lcom/facebook/events/planning/CalendarRange;)V
    .locals 2

    .prologue
    .line 1816598
    iput-object p1, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816599
    iget-object v0, p0, LX/Bln;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816600
    iget-object p1, v1, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    move-object v1, p1

    .line 1816601
    invoke-direct {p0, v0, v1}, LX/Bln;->a(Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V

    .line 1816602
    iget-object v0, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816603
    iget-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    move-object v0, v1

    .line 1816604
    if-eqz v0, :cond_0

    .line 1816605
    iget-object v0, p0, LX/Bln;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1816606
    iget-object v0, p0, LX/Bln;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Bln;->d:Lcom/facebook/events/planning/CalendarRange;

    .line 1816607
    iget-object p1, v1, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    move-object v1, p1

    .line 1816608
    invoke-direct {p0, v0, v1}, LX/Bln;->a(Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V

    .line 1816609
    :goto_0
    return-void

    .line 1816610
    :cond_0
    iget-object v0, p0, LX/Bln;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
