.class public LX/BaB;
.super Lcom/facebook/livephotos/LivePhotoView;
.source ""

# interfaces
.implements LX/24e;


# instance fields
.field public e:LX/24f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/attachments/photos/ui/PostPostBadge;

.field public g:LX/1eo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1798643
    invoke-direct {p0, p1}, Lcom/facebook/livephotos/LivePhotoView;-><init>(Landroid/content/Context;)V

    .line 1798644
    const-class v0, LX/BaB;

    invoke-static {v0, p0}, LX/BaB;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1798645
    const v0, 0x7f0d008a

    invoke-virtual {p0, v0}, LX/BaB;->setId(I)V

    .line 1798646
    invoke-virtual {p0}, LX/BaB;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v0

    const v1, 0x7f0d24e0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setId(I)V

    .line 1798647
    new-instance v0, Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {p0}, LX/BaB;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/attachments/photos/ui/PostPostBadge;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BaB;->f:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    .line 1798648
    iget-object v0, p0, LX/BaB;->f:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {p0, v0}, LX/BaB;->addView(Landroid/view/View;)V

    .line 1798649
    iget-object v0, p0, LX/BaB;->e:LX/24f;

    iget-object v1, p0, LX/BaB;->f:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    invoke-virtual {v0, p0, p0, v1}, LX/24f;->a(Landroid/view/View;Landroid/view/View;Lcom/facebook/attachments/photos/ui/PostPostBadge;)LX/1eo;

    move-result-object v0

    iput-object v0, p0, LX/BaB;->g:LX/1eo;

    .line 1798650
    invoke-virtual {p0}, LX/BaB;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1798651
    const v1, 0x7f0a045d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1798652
    const v2, 0x7f021af6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1798653
    new-instance v3, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const/16 p1, 0x3e8

    invoke-direct {v3, v2, p1}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1798654
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1798655
    iput-object v1, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1798656
    move-object v1, v2

    .line 1798657
    const v2, 0x7f020aa6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1798658
    iput-object v0, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 1798659
    move-object v0, v1

    .line 1798660
    iput-object v3, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1798661
    move-object v0, v0

    .line 1798662
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1798663
    invoke-virtual {p0}, LX/BaB;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1798664
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/BaB;

    const-class p0, LX/24f;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/24f;

    iput-object v1, p1, LX/BaB;->e:LX/24f;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1798641
    iget-object v0, p0, LX/BaB;->g:LX/1eo;

    invoke-virtual {v0, p1, p2}, LX/1eo;->a(II)V

    .line 1798642
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1798639
    iget-object v0, p0, LX/BaB;->g:LX/1eo;

    invoke-virtual {v0, p1, p2}, LX/1eo;->a(Ljava/lang/String;I)V

    .line 1798640
    return-void
.end method

.method public getPhotoAttachmentView()Landroid/view/View;
    .locals 0

    .prologue
    .line 1798638
    return-object p0
.end method

.method public getPostPostBadge()Lcom/facebook/attachments/photos/ui/PostPostBadge;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1798637
    iget-object v0, p0, LX/BaB;->f:Lcom/facebook/attachments/photos/ui/PostPostBadge;

    return-object v0
.end method

.method public getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;
    .locals 1

    .prologue
    .line 1798635
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 1798636
    return-object v0
.end method

.method public setActualImageFocusPoint(Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 1798632
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 1798633
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 1798634
    return-void
.end method

.method public setController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1798629
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 1798630
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1798631
    return-void
.end method

.method public setOnBadgeClickListener(LX/24m;)V
    .locals 1
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1798619
    iget-object v0, p0, LX/BaB;->g:LX/1eo;

    .line 1798620
    iput-object p1, v0, LX/1eo;->f:LX/24m;

    .line 1798621
    return-void
.end method

.method public setOnPhotoClickListener(LX/24m;)V
    .locals 1
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1798626
    iget-object v0, p0, LX/BaB;->g:LX/1eo;

    .line 1798627
    iput-object p1, v0, LX/1eo;->e:LX/24m;

    .line 1798628
    return-void
.end method

.method public setPairedVideoUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1798624
    invoke-virtual {p0, p1}, Lcom/facebook/livephotos/LivePhotoView;->setVideoUri(Ljava/lang/String;)V

    .line 1798625
    return-void
.end method

.method public setPhotoBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1798622
    invoke-static {p0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1798623
    return-void
.end method
