.class public final LX/C7i;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C7j;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLGroup;

.field public final synthetic b:LX/C7j;


# direct methods
.method public constructor <init>(LX/C7j;)V
    .locals 1

    .prologue
    .line 1851684
    iput-object p1, p0, LX/C7i;->b:LX/C7j;

    .line 1851685
    move-object v0, p1

    .line 1851686
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1851687
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1851688
    const-string v0, "GroupMemberWelcomeEndCard"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1851689
    if-ne p0, p1, :cond_1

    .line 1851690
    :cond_0
    :goto_0
    return v0

    .line 1851691
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1851692
    goto :goto_0

    .line 1851693
    :cond_3
    check-cast p1, LX/C7i;

    .line 1851694
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1851695
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1851696
    if-eq v2, v3, :cond_0

    .line 1851697
    iget-object v2, p0, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    iget-object v3, p1, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1851698
    goto :goto_0

    .line 1851699
    :cond_4
    iget-object v2, p1, LX/C7i;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
