.class public final LX/AUz;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/AV1;


# direct methods
.method public constructor <init>(LX/AV1;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1678569
    iput-object p1, p0, LX/AUz;->a:LX/AV1;

    .line 1678570
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1678571
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 13

    .prologue
    .line 1678508
    iget v1, p1, Landroid/os/Message;->what:I

    .line 1678509
    iget-object v0, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Camera Handler Delegate is not set."

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1678510
    iget-object v0, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b6;

    .line 1678511
    if-nez v0, :cond_1

    .line 1678512
    sget-object v0, LX/AV1;->a:Ljava/lang/String;

    const-string v1, "FacecastCamera.handleMessage: activity is null"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678513
    :goto_1
    return-void

    .line 1678514
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1678515
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 1678516
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown msg "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1678517
    :pswitch_0
    iget-object v1, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    invoke-static {v1, v0}, LX/AV1;->a$redex0(LX/AV1;Landroid/graphics/SurfaceTexture;)V

    goto :goto_1

    .line 1678518
    :pswitch_1
    iget-object v1, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1678519
    iget v2, v1, LX/AV1;->e:I

    invoke-static {v1, v2, v0}, LX/AV1;->a(LX/AV1;II)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1678520
    :cond_2
    :goto_2
    goto :goto_1

    .line 1678521
    :pswitch_2
    iget-object v0, p0, LX/AUz;->a:LX/AV1;

    .line 1678522
    invoke-static {v0}, LX/AV1;->k(LX/AV1;)V

    .line 1678523
    invoke-static {v0}, LX/AV1;->r(LX/AV1;)LX/1b6;

    move-result-object v1

    .line 1678524
    if-eqz v1, :cond_3

    .line 1678525
    invoke-interface {v1}, LX/1b6;->p()V

    .line 1678526
    :cond_3
    goto :goto_1

    .line 1678527
    :pswitch_3
    iget-object v0, p0, LX/AUz;->a:LX/AV1;

    const/4 v4, 0x1

    const v6, 0xac0007

    .line 1678528
    iget v5, v0, LX/AV1;->e:I

    .line 1678529
    if-ne v5, v4, :cond_4

    .line 1678530
    const/4 v4, 0x0

    .line 1678531
    :cond_4
    invoke-static {v0, v6}, LX/AV1;->d(LX/AV1;I)V

    .line 1678532
    invoke-static {v0}, LX/AV1;->k(LX/AV1;)V

    .line 1678533
    iget v5, v0, LX/AV1;->q:I

    invoke-static {v0, v4, v5}, LX/AV1;->a(LX/AV1;II)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1678534
    iget-object v4, v0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x3

    invoke-interface {v4, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1678535
    :cond_5
    :goto_3
    goto :goto_1

    .line 1678536
    :pswitch_4
    iget-object v1, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1678537
    iget-object v2, v1, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1678538
    iget-object v2, v1, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 1678539
    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 1678540
    iget-object v3, v1, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1678541
    goto/16 :goto_1

    .line 1678542
    :pswitch_5
    iget-object v0, p0, LX/AUz;->a:LX/AV1;

    .line 1678543
    invoke-static {v0}, LX/AV1;->k(LX/AV1;)V

    .line 1678544
    iget v1, v0, LX/AV1;->e:I

    iget v2, v0, LX/AV1;->q:I

    invoke-static {v0, v1, v2}, LX/AV1;->a(LX/AV1;II)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1678545
    :cond_6
    :goto_4
    goto/16 :goto_1

    .line 1678546
    :pswitch_6
    iget-object v1, p0, LX/AUz;->a:LX/AV1;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1678547
    iget-object v2, v1, LX/AV1;->r:Landroid/hardware/Camera;

    if-nez v2, :cond_b

    .line 1678548
    :goto_5
    goto/16 :goto_1

    .line 1678549
    :cond_7
    invoke-static {v1}, LX/AV1;->r(LX/AV1;)LX/1b6;

    move-result-object v2

    .line 1678550
    if-eqz v2, :cond_2

    .line 1678551
    invoke-interface {v2}, LX/1b6;->q()V

    goto/16 :goto_2

    .line 1678552
    :cond_8
    iget-object v4, v0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x2

    invoke-interface {v4, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1678553
    iget-object v4, v0, LX/AV1;->g:Landroid/graphics/SurfaceTexture;

    invoke-static {v0, v4}, LX/AV1;->a$redex0(LX/AV1;Landroid/graphics/SurfaceTexture;)V

    .line 1678554
    const/4 v8, 0x0

    .line 1678555
    iget v7, v0, LX/AV1;->e:I

    if-nez v7, :cond_9

    const-string v9, "backCamera"

    .line 1678556
    :goto_6
    iget-object v7, v0, LX/AV1;->c:LX/AVT;

    const-string v10, "cameraFlipped"

    move-object v11, v8

    move-object v12, v8

    invoke-virtual/range {v7 .. v12}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1678557
    invoke-static {v0}, LX/AV1;->r(LX/AV1;)LX/1b6;

    move-result-object v4

    .line 1678558
    if-eqz v4, :cond_5

    .line 1678559
    invoke-interface {v4}, LX/1b6;->r()V

    goto :goto_3

    .line 1678560
    :cond_9
    const-string v9, "frontCamera"

    goto :goto_6

    .line 1678561
    :cond_a
    iget-object v1, v0, LX/AV1;->g:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_6

    .line 1678562
    iget-object v1, v0, LX/AV1;->g:Landroid/graphics/SurfaceTexture;

    invoke-static {v0, v1}, LX/AV1;->a$redex0(LX/AV1;Landroid/graphics/SurfaceTexture;)V

    goto :goto_4

    .line 1678563
    :cond_b
    iput v0, v1, LX/AV1;->q:I

    .line 1678564
    iget-object v2, v1, LX/AV1;->r:Landroid/hardware/Camera;

    invoke-virtual {v1}, LX/AV1;->a()I

    move-result v3

    invoke-static {v1}, LX/AV1;->q(LX/AV1;)I

    move-result v4

    .line 1678565
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1678566
    invoke-static {v3, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1678567
    invoke-static {v2, v1, v4}, LX/7RT;->a(Landroid/hardware/Camera;Landroid/hardware/Camera$CameraInfo;I)V

    .line 1678568
    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
