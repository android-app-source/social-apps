.class public final LX/Bb7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V
    .locals 0

    .prologue
    .line 1800267
    iput-object p1, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1800254
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    const/4 v1, 0x0

    .line 1800255
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    .line 1800256
    if-eqz p1, :cond_0

    .line 1800257
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800258
    if-eqz v0, :cond_0

    .line 1800259
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1800260
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1800261
    :cond_0
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u:LX/03V;

    sget-object v1, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->x:Ljava/lang/String;

    const-string v2, "Error converting post"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800262
    :goto_0
    return-void

    .line 1800263
    :cond_1
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->y:LX/0Tn;

    iget-object v2, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1800264
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CONVERT_POST:LX/BbB;

    invoke-virtual {v0, v1}, LX/BbC;->a(LX/BbB;)V

    .line 1800265
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    invoke-virtual {v0}, LX/BbC;->a()V

    .line 1800266
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1800251
    iget-object v0, p0, LX/Bb7;->a:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    const/4 v1, 0x0

    .line 1800252
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    .line 1800253
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800250
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bb7;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
