.class public final LX/BEO;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1764380
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1764381
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1764382
    iput-object v0, p0, LX/BEO;->a:Ljava/util/List;

    .line 1764383
    iput-object p1, p0, LX/BEO;->b:Landroid/content/Context;

    .line 1764384
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/ipc/pages/PageInfo;
    .locals 1

    .prologue
    .line 1764377
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1764378
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/pages/PageInfo;

    .line 1764379
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1764362
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1764376
    invoke-virtual {p0, p1}, LX/BEO;->a(I)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1764373
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1764374
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/pages/PageInfo;

    iget-wide v0, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    .line 1764375
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1764364
    check-cast p2, LX/BEN;

    .line 1764365
    iget-object v0, p0, LX/BEO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index is a not list index"

    invoke-static {v0, v1}, LX/45Y;->a(ZLjava/lang/String;)V

    .line 1764366
    if-nez p2, :cond_0

    .line 1764367
    new-instance p2, LX/BEN;

    iget-object v0, p0, LX/BEO;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/BEN;-><init>(Landroid/content/Context;)V

    .line 1764368
    :cond_0
    invoke-virtual {p0, p1}, LX/BEO;->a(I)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v0

    .line 1764369
    iget-object v1, p2, LX/BEN;->a:Landroid/widget/TextView;

    iget-object p0, v0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1764370
    iget-object v1, p2, LX/BEN;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, v0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object p1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1764371
    return-object p2

    .line 1764372
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1764363
    const/4 v0, 0x1

    return v0
.end method
