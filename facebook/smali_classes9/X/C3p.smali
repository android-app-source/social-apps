.class public LX/C3p;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3p",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846288
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1846289
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3p;->b:LX/0Zi;

    .line 1846290
    iput-object p1, p0, LX/C3p;->a:LX/0Ot;

    .line 1846291
    return-void
.end method

.method public static a(LX/0QB;)LX/C3p;
    .locals 4

    .prologue
    .line 1846292
    const-class v1, LX/C3p;

    monitor-enter v1

    .line 1846293
    :try_start_0
    sget-object v0, LX/C3p;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846294
    sput-object v2, LX/C3p;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846297
    new-instance v3, LX/C3p;

    const/16 p0, 0x1ed2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3p;-><init>(LX/0Ot;)V

    .line 1846298
    move-object v0, v3

    .line 1846299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1846303
    check-cast p2, LX/C3o;

    .line 1846304
    iget-object v0, p0, LX/C3p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;

    iget-object v1, p2, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p2, LX/C3o;->b:I

    iget-boolean v3, p2, LX/C3o;->c:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;IZ)LX/1Dg;

    move-result-object v0

    .line 1846305
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1846306
    invoke-static {}, LX/1dS;->b()V

    .line 1846307
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C3n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3p",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1846308
    new-instance v1, LX/C3o;

    invoke-direct {v1, p0}, LX/C3o;-><init>(LX/C3p;)V

    .line 1846309
    iget-object v2, p0, LX/C3p;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3n;

    .line 1846310
    if-nez v2, :cond_0

    .line 1846311
    new-instance v2, LX/C3n;

    invoke-direct {v2, p0}, LX/C3n;-><init>(LX/C3p;)V

    .line 1846312
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3n;->a$redex0(LX/C3n;LX/1De;IILX/C3o;)V

    .line 1846313
    move-object v1, v2

    .line 1846314
    move-object v0, v1

    .line 1846315
    return-object v0
.end method
