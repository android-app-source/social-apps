.class public final LX/Aks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/net/Uri;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:I

.field public final synthetic g:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic h:LX/BUA;

.field public final synthetic i:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/feed/rows/core/props/FeedProps;LX/BUA;)V
    .locals 0

    .prologue
    .line 1709336
    iput-object p1, p0, LX/Aks;->i:LX/1wH;

    iput-object p2, p0, LX/Aks;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aks;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aks;->c:Landroid/net/Uri;

    iput-object p5, p0, LX/Aks;->d:Ljava/lang/String;

    iput-object p6, p0, LX/Aks;->e:Ljava/lang/String;

    iput p7, p0, LX/Aks;->f:I

    iput-object p8, p0, LX/Aks;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p9, p0, LX/Aks;->h:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 1709337
    iget-object v0, p0, LX/Aks;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1709338
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1709339
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 1709340
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    .line 1709341
    iget-object v0, p0, LX/Aks;->i:LX/1wH;

    iget-object v1, p0, LX/Aks;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Aks;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, LX/1wH;->a$redex0(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;)V

    .line 1709342
    :cond_0
    new-instance v1, LX/BUL;

    iget-object v2, p0, LX/Aks;->c:Landroid/net/Uri;

    iget-object v3, p0, LX/Aks;->d:Ljava/lang/String;

    const-string v4, ""

    iget-object v5, p0, LX/Aks;->e:Ljava/lang/String;

    iget v0, p0, LX/Aks;->f:I

    int-to-long v6, v0

    sget-object v8, LX/2ft;->NONE:LX/2ft;

    invoke-direct/range {v1 .. v8}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/2ft;)V

    .line 1709343
    iget-object v0, p0, LX/Aks;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v1}, LX/15V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/BUL;)V

    .line 1709344
    iget-object v0, p0, LX/Aks;->h:LX/BUA;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1709345
    iget-object v1, p0, LX/Aks;->h:LX/BUA;

    iget-object v2, p0, LX/Aks;->d:Ljava/lang/String;

    iget-object v0, p0, LX/Aks;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1709346
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1709347
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2, v0}, LX/BUA;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1709348
    const/4 v0, 0x1

    return v0
.end method
