.class public final LX/B0X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0d;

.field public final synthetic b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final synthetic c:LX/B0M;

.field public final synthetic d:Z

.field public final synthetic e:LX/B0a;

.field private f:LX/B0d;


# direct methods
.method public constructor <init>(LX/B0a;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Z)V
    .locals 1

    .prologue
    .line 1733969
    iput-object p1, p0, LX/B0X;->e:LX/B0a;

    iput-object p2, p0, LX/B0X;->a:LX/B0d;

    iput-object p3, p0, LX/B0X;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p4, p0, LX/B0X;->c:LX/B0M;

    iput-boolean p5, p0, LX/B0X;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733970
    iget-object v0, p0, LX/B0X;->a:LX/B0d;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0X;->f:LX/B0d;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1733983
    iget-boolean v0, p0, LX/B0X;->d:Z

    if-nez v0, :cond_0

    .line 1733984
    iget-object v0, p0, LX/B0X;->c:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1733985
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1733974
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1733975
    if-eqz p1, :cond_0

    .line 1733976
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1733977
    if-nez v0, :cond_1

    .line 1733978
    :cond_0
    const-class v0, LX/B0a;

    const-string v1, "Null response from network"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1733979
    :goto_0
    return-void

    .line 1733980
    :cond_1
    iget-object v0, p0, LX/B0X;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/B0X;->e:LX/B0a;

    iget-object v1, v1, LX/B0a;->b:LX/B0V;

    iget-object v2, p0, LX/B0X;->f:LX/B0d;

    invoke-static {v0, v1, v2, p1}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;

    move-result-object v1

    .line 1733981
    invoke-interface {v1}, LX/B0N;->d()LX/B0d;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0X;->f:LX/B0d;

    .line 1733982
    iget-object v0, p0, LX/B0X;->c:LX/B0M;

    invoke-virtual {v0, v1}, LX/B0M;->a(LX/B0N;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1733971
    iget-object v0, p0, LX/B0X;->c:LX/B0M;

    invoke-virtual {v0, p1}, LX/B0M;->a(Ljava/lang/Throwable;)V

    .line 1733972
    iget-object v0, p0, LX/B0X;->c:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1733973
    return-void
.end method
