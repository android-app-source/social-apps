.class public LX/CLr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;


# instance fields
.field public final isGroupThread:Ljava/lang/Boolean;

.field public final messageIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final messageRecipientFbid:Ljava/lang/Long;

.field public final messageSenderFbid:Ljava/lang/Long;

.field public final threadFbid:Ljava/lang/Long;

.field public final watermarkTimestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, 0xa

    .line 1879770
    new-instance v0, LX/1sv;

    const-string v1, "DeliveryReceipt"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/CLr;->b:LX/1sv;

    .line 1879771
    new-instance v0, LX/1sw;

    const-string v1, "messageSenderFbid"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->c:LX/1sw;

    .line 1879772
    new-instance v0, LX/1sw;

    const-string v1, "watermarkTimestamp"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->d:LX/1sw;

    .line 1879773
    new-instance v0, LX/1sw;

    const-string v1, "threadFbid"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->e:LX/1sw;

    .line 1879774
    new-instance v0, LX/1sw;

    const-string v1, "messageIds"

    const/16 v2, 0xf

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->f:LX/1sw;

    .line 1879775
    new-instance v0, LX/1sw;

    const-string v1, "messageRecipientFbid"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->g:LX/1sw;

    .line 1879776
    new-instance v0, LX/1sw;

    const-string v1, "isGroupThread"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLr;->h:LX/1sw;

    .line 1879777
    sput-boolean v5, LX/CLr;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1879778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879779
    iput-object p1, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    .line 1879780
    iput-object p2, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    .line 1879781
    iput-object p3, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    .line 1879782
    iput-object p4, p0, LX/CLr;->messageIds:Ljava/util/List;

    .line 1879783
    iput-object p5, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    .line 1879784
    iput-object p6, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    .line 1879785
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1879678
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1879679
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v1, v0

    .line 1879680
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1879681
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeliveryReceipt"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1879682
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879683
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879684
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879685
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879686
    const-string v4, "messageSenderFbid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879687
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879688
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879689
    iget-object v4, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    if-nez v4, :cond_7

    .line 1879690
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879691
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879692
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879693
    const-string v4, "watermarkTimestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879694
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879695
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879696
    iget-object v4, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    if-nez v4, :cond_8

    .line 1879697
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879698
    :goto_4
    iget-object v4, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 1879699
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879700
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879701
    const-string v4, "threadFbid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879702
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879703
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879704
    iget-object v4, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-nez v4, :cond_9

    .line 1879705
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879706
    :cond_0
    :goto_5
    iget-object v4, p0, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 1879707
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879708
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879709
    const-string v4, "messageIds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879710
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879711
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879712
    iget-object v4, p0, LX/CLr;->messageIds:Ljava/util/List;

    if-nez v4, :cond_a

    .line 1879713
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879714
    :cond_1
    :goto_6
    iget-object v4, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1879715
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879716
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879717
    const-string v4, "messageRecipientFbid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879718
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879719
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879720
    iget-object v4, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-nez v4, :cond_b

    .line 1879721
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879722
    :cond_2
    :goto_7
    iget-object v4, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    .line 1879723
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879724
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879725
    const-string v4, "isGroupThread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879726
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879727
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879728
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    .line 1879729
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879730
    :cond_3
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879731
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879732
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1879733
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1879734
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1879735
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1879736
    :cond_7
    iget-object v4, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1879737
    :cond_8
    iget-object v4, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1879738
    :cond_9
    iget-object v4, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1879739
    :cond_a
    iget-object v4, p0, LX/CLr;->messageIds:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1879740
    :cond_b
    iget-object v4, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1879741
    :cond_c
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1879742
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1879743
    iget-object v0, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1879744
    sget-object v0, LX/CLr;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879745
    iget-object v0, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1879746
    :cond_0
    iget-object v0, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1879747
    sget-object v0, LX/CLr;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879748
    iget-object v0, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1879749
    :cond_1
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1879750
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1879751
    sget-object v0, LX/CLr;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879752
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1879753
    :cond_2
    iget-object v0, p0, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1879754
    iget-object v0, p0, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1879755
    sget-object v0, LX/CLr;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879756
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/CLr;->messageIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1879757
    iget-object v0, p0, LX/CLr;->messageIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1879758
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1879759
    :cond_3
    iget-object v0, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1879760
    iget-object v0, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1879761
    sget-object v0, LX/CLr;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879762
    iget-object v0, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1879763
    :cond_4
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1879764
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1879765
    sget-object v0, LX/CLr;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879766
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1879767
    :cond_5
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1879768
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1879769
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1879624
    if-nez p1, :cond_1

    .line 1879625
    :cond_0
    :goto_0
    return v0

    .line 1879626
    :cond_1
    instance-of v1, p1, LX/CLr;

    if-eqz v1, :cond_0

    .line 1879627
    check-cast p1, LX/CLr;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1879628
    if-nez p1, :cond_3

    .line 1879629
    :cond_2
    :goto_1
    move v0, v2

    .line 1879630
    goto :goto_0

    .line 1879631
    :cond_3
    iget-object v0, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1879632
    :goto_2
    iget-object v3, p1, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1879633
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1879634
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879635
    iget-object v0, p0, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/CLr;->messageSenderFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879636
    :cond_5
    iget-object v0, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1879637
    :goto_4
    iget-object v3, p1, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1879638
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1879639
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879640
    iget-object v0, p0, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/CLr;->watermarkTimestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879641
    :cond_7
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1879642
    :goto_6
    iget-object v3, p1, LX/CLr;->threadFbid:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1879643
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1879644
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879645
    iget-object v0, p0, LX/CLr;->threadFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/CLr;->threadFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879646
    :cond_9
    iget-object v0, p0, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1879647
    :goto_8
    iget-object v3, p1, LX/CLr;->messageIds:Ljava/util/List;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1879648
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1879649
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879650
    iget-object v0, p0, LX/CLr;->messageIds:Ljava/util/List;

    iget-object v3, p1, LX/CLr;->messageIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879651
    :cond_b
    iget-object v0, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1879652
    :goto_a
    iget-object v3, p1, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1879653
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1879654
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879655
    iget-object v0, p0, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/CLr;->messageRecipientFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879656
    :cond_d
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1879657
    :goto_c
    iget-object v3, p1, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1879658
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1879659
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879660
    iget-object v0, p0, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    iget-object v3, p1, LX/CLr;->isGroupThread:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_f
    move v2, v1

    .line 1879661
    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 1879662
    goto/16 :goto_2

    :cond_11
    move v3, v2

    .line 1879663
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 1879664
    goto/16 :goto_4

    :cond_13
    move v3, v2

    .line 1879665
    goto/16 :goto_5

    :cond_14
    move v0, v2

    .line 1879666
    goto :goto_6

    :cond_15
    move v3, v2

    .line 1879667
    goto :goto_7

    :cond_16
    move v0, v2

    .line 1879668
    goto :goto_8

    :cond_17
    move v3, v2

    .line 1879669
    goto :goto_9

    :cond_18
    move v0, v2

    .line 1879670
    goto :goto_a

    :cond_19
    move v3, v2

    .line 1879671
    goto :goto_b

    :cond_1a
    move v0, v2

    .line 1879672
    goto :goto_c

    :cond_1b
    move v3, v2

    .line 1879673
    goto :goto_d
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1879674
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1879675
    sget-boolean v0, LX/CLr;->a:Z

    .line 1879676
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/CLr;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1879677
    return-object v0
.end method
