.class public final LX/CVx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1905855
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1905856
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905857
    :goto_0
    return v1

    .line 1905858
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905859
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1905860
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1905861
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905862
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1905863
    const-string v5, "heading"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1905864
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1905865
    :cond_2
    const-string v5, "metadata"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1905866
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1905867
    :cond_3
    const-string v5, "time_slot_groups"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1905868
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1905869
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 1905870
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1905871
    invoke-static {p0, p1}, LX/CVw;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1905872
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1905873
    :cond_4
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1905874
    goto :goto_1

    .line 1905875
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1905876
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1905877
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1905878
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1905879
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1905836
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1905837
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905838
    if-eqz v0, :cond_0

    .line 1905839
    const-string v1, "heading"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905840
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905841
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905842
    if-eqz v0, :cond_1

    .line 1905843
    const-string v1, "metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905844
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905845
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905846
    if-eqz v0, :cond_3

    .line 1905847
    const-string v1, "time_slot_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905848
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1905849
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_2

    .line 1905850
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/CVw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905851
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1905852
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1905853
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1905854
    return-void
.end method
