.class public final LX/AVJ;
.super LX/AVI;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1679177
    iput-object p1, p0, LX/AVJ;->a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;

    invoke-direct {p0, p2}, LX/AVI;-><init>(Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final a(II)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1679178
    if-nez p2, :cond_0

    .line 1679179
    :goto_0
    return v0

    .line 1679180
    :cond_0
    iget-object v1, p0, LX/AVJ;->a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;

    iget-object v1, v1, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1679181
    iget-object v2, p0, LX/AVI;->a:Landroid/graphics/Rect;

    move-object v2, v2

    .line 1679182
    iget-object v3, p0, LX/AVJ;->a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;

    invoke-virtual {v3}, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;->getBottom()I

    move-result v3

    iget v4, v2, Landroid/graphics/Rect;->right:I

    if-eq v3, v4, :cond_1

    .line 1679183
    iget-object v3, p0, LX/AVJ;->a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;

    invoke-virtual {v3}, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;->getBottom()I

    move-result v3

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v3, v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1679184
    :cond_1
    iget-object v0, p0, LX/AVJ;->a:Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;

    iget-object v0, v0, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1679185
    const/4 v0, 0x1

    goto :goto_0
.end method
