.class public final LX/Bit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bis;


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventCreationHostSelectionFragment;)V
    .locals 0

    .prologue
    .line 1810965
    iput-object p1, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1810966
    iget-object v0, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->e:LX/Bip;

    iget-object v1, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    iget-object v1, v1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->h:LX/Bir;

    .line 1810967
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1810968
    iget-object v3, v0, LX/Bip;->h:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    sget-object v4, LX/Bip;->a:Ljava/lang/Object;

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1810969
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1810970
    invoke-virtual {v2, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1810971
    :goto_0
    sget-object v3, LX/Bip;->d:Ljava/lang/Object;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v0, LX/Bip;->e:Ljava/util/List;

    .line 1810972
    iput-object v1, v0, LX/Bip;->g:LX/Bir;

    .line 1810973
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1810974
    iget-object v0, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    .line 1810975
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, v0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->f:Ljava/util/HashMap;

    .line 1810976
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1810977
    iget-object v3, v0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1810978
    :cond_0
    iget-object v1, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    iget-object v0, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->f:Ljava/util/HashMap;

    iget-object v2, p0, LX/Bit;->a:Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    iget-object v2, v2, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1810979
    iput-object v0, v1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->g:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1810980
    return-void

    .line 1810981
    :cond_1
    sget-object v3, LX/Bip;->c:Ljava/lang/Object;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method
