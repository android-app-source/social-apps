.class public LX/AsC;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AsB;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720230
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1720231
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;LX/ArT;LX/ArL;Landroid/view/ViewStub;)LX/AsB;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0iq;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
            "<TMutation;>;:",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData$SetsPrivacyData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "LX/ArT;",
            "LX/ArL;",
            "Landroid/view/ViewStub;",
            ")",
            "LX/AsB",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1720232
    new-instance v0, LX/AsB;

    move-object/from16 v2, p2

    check-cast v2, LX/0il;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v1, LX/Asf;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Asf;

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    move-result-object v8

    check-cast v8, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    invoke-static {p0}, LX/1kH;->a(LX/0QB;)LX/1kH;

    move-result-object v9

    check-cast v9, LX/1kH;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static {p0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v11

    check-cast v11, LX/0tS;

    const-class v1, LX/AuQ;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AuQ;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v13

    check-cast v13, LX/74n;

    move-object v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v13}, LX/AsB;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;LX/ArT;LX/ArL;Landroid/view/ViewStub;Landroid/content/Context;LX/Asf;Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;LX/1kH;LX/0Sh;LX/0tS;LX/AuQ;LX/74n;)V

    .line 1720233
    return-object v0
.end method
