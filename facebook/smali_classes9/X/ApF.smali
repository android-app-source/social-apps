.class public LX/ApF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApE;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715657
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715658
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715659
    iput-object p1, p0, LX/ApF;->b:LX/0Ot;

    .line 1715660
    return-void
.end method

.method public static a(LX/0QB;)LX/ApF;
    .locals 4

    .prologue
    .line 1715661
    const-class v1, LX/ApF;

    monitor-enter v1

    .line 1715662
    :try_start_0
    sget-object v0, LX/ApF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715663
    sput-object v2, LX/ApF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715664
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715665
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715666
    new-instance v3, LX/ApF;

    const/16 p0, 0x21f8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ApF;-><init>(LX/0Ot;)V

    .line 1715667
    move-object v0, v3

    .line 1715668
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715669
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715670
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715671
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 19

    .prologue
    .line 1715672
    check-cast p2, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    .line 1715673
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ApF;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget v4, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->b:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->c:F

    move-object/from16 v0, p2

    iget v6, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->d:I

    move-object/from16 v0, p2

    iget v7, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->e:I

    move-object/from16 v0, p2

    iget v8, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->f:I

    move-object/from16 v0, p2

    iget v9, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->g:I

    move-object/from16 v0, p2

    iget v10, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->h:I

    move-object/from16 v0, p2

    iget v11, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->i:I

    move-object/from16 v0, p2

    iget v12, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->j:I

    move-object/from16 v0, p2

    iget v13, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->k:I

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->p:I

    move/from16 v18, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v18}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a(LX/1De;Landroid/net/Uri;IFIIIIIIIILcom/facebook/common/callercontext/CallerContext;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)LX/1Dg;

    move-result-object v1

    .line 1715674
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715675
    invoke-static {}, LX/1dS;->b()V

    .line 1715676
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/ApE;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1715677
    new-instance v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;-><init>(LX/ApF;)V

    .line 1715678
    sget-object v2, LX/ApF;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ApE;

    .line 1715679
    if-nez v2, :cond_0

    .line 1715680
    new-instance v2, LX/ApE;

    invoke-direct {v2}, LX/ApE;-><init>()V

    .line 1715681
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/ApE;->a$redex0(LX/ApE;LX/1De;IILcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;)V

    .line 1715682
    move-object v1, v2

    .line 1715683
    move-object v0, v1

    .line 1715684
    return-object v0
.end method
