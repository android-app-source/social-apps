.class public final enum LX/BZf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZf;

.field public static final enum APP_DISCOVERY_MENU:LX/BZf;

.field public static final enum BOOKMARK:LX/BZf;

.field public static final enum END_OF_HSCROLL:LX/BZf;

.field public static final enum REACTION:LX/BZf;

.field public static final enum RETURN_FROM_APP_STORE:LX/BZf;

.field public static final enum SECONDARY_ACTIONS_MENU:LX/BZf;

.field public static final enum UFI_SEE_MORE:LX/BZf;

.field public static final enum UNKNOWN:LX/BZf;

.field public static final enum URL_SEGUE:LX/BZf;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1797937
    new-instance v0, LX/BZf;

    const-string v1, "APP_DISCOVERY_MENU"

    const-string v2, "app_discovery_menu"

    invoke-direct {v0, v1, v4, v2}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->APP_DISCOVERY_MENU:LX/BZf;

    .line 1797938
    new-instance v0, LX/BZf;

    const-string v1, "BOOKMARK"

    const-string v2, "bookmark"

    invoke-direct {v0, v1, v5, v2}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->BOOKMARK:LX/BZf;

    .line 1797939
    new-instance v0, LX/BZf;

    const-string v1, "END_OF_HSCROLL"

    const-string v2, "end_of_hscroll"

    invoke-direct {v0, v1, v6, v2}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->END_OF_HSCROLL:LX/BZf;

    .line 1797940
    new-instance v0, LX/BZf;

    const-string v1, "REACTION"

    const-string v2, "reaction"

    invoke-direct {v0, v1, v7, v2}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->REACTION:LX/BZf;

    .line 1797941
    new-instance v0, LX/BZf;

    const-string v1, "RETURN_FROM_APP_STORE"

    const-string v2, "return_from_app_store"

    invoke-direct {v0, v1, v8, v2}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->RETURN_FROM_APP_STORE:LX/BZf;

    .line 1797942
    new-instance v0, LX/BZf;

    const-string v1, "SECONDARY_ACTIONS_MENU"

    const/4 v2, 0x5

    const-string v3, "secondary_actions_menu"

    invoke-direct {v0, v1, v2, v3}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->SECONDARY_ACTIONS_MENU:LX/BZf;

    .line 1797943
    new-instance v0, LX/BZf;

    const-string v1, "UFI_SEE_MORE"

    const/4 v2, 0x6

    const-string v3, "ufi_see_more"

    invoke-direct {v0, v1, v2, v3}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->UFI_SEE_MORE:LX/BZf;

    .line 1797944
    new-instance v0, LX/BZf;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->UNKNOWN:LX/BZf;

    .line 1797945
    new-instance v0, LX/BZf;

    const-string v1, "URL_SEGUE"

    const/16 v2, 0x8

    const-string v3, "url_segue"

    invoke-direct {v0, v1, v2, v3}, LX/BZf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BZf;->URL_SEGUE:LX/BZf;

    .line 1797946
    const/16 v0, 0x9

    new-array v0, v0, [LX/BZf;

    sget-object v1, LX/BZf;->APP_DISCOVERY_MENU:LX/BZf;

    aput-object v1, v0, v4

    sget-object v1, LX/BZf;->BOOKMARK:LX/BZf;

    aput-object v1, v0, v5

    sget-object v1, LX/BZf;->END_OF_HSCROLL:LX/BZf;

    aput-object v1, v0, v6

    sget-object v1, LX/BZf;->REACTION:LX/BZf;

    aput-object v1, v0, v7

    sget-object v1, LX/BZf;->RETURN_FROM_APP_STORE:LX/BZf;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BZf;->SECONDARY_ACTIONS_MENU:LX/BZf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BZf;->UFI_SEE_MORE:LX/BZf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BZf;->UNKNOWN:LX/BZf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BZf;->URL_SEGUE:LX/BZf;

    aput-object v2, v0, v1

    sput-object v0, LX/BZf;->$VALUES:[LX/BZf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1797947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1797948
    iput-object p3, p0, LX/BZf;->mEventName:Ljava/lang/String;

    .line 1797949
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/BZf;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1797950
    invoke-static {}, LX/BZf;->values()[LX/BZf;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1797951
    invoke-virtual {v0}, LX/BZf;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1797952
    :goto_1
    return-object v0

    .line 1797953
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1797954
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZf;
    .locals 1

    .prologue
    .line 1797955
    const-class v0, LX/BZf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZf;

    return-object v0
.end method

.method public static values()[LX/BZf;
    .locals 1

    .prologue
    .line 1797956
    sget-object v0, LX/BZf;->$VALUES:[LX/BZf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZf;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797957
    iget-object v0, p0, LX/BZf;->mEventName:Ljava/lang/String;

    return-object v0
.end method
