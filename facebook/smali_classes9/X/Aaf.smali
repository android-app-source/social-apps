.class public final LX/Aaf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Aag;


# direct methods
.method public constructor <init>(LX/Aag;)V
    .locals 0

    .prologue
    .line 1688796
    iput-object p1, p0, LX/Aaf;->a:LX/Aag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1688797
    iget-object v0, p0, LX/Aaf;->a:LX/Aag;

    iget-object v0, v0, LX/Aag;->d:LX/03V;

    sget-object v1, LX/Aag;->b:Ljava/lang/String;

    const-string v2, "Can\'t fetch funnel metadata"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1688798
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688799
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1688800
    iget-object v0, p0, LX/Aaf;->a:LX/Aag;

    iget-object v0, v0, LX/Aag;->a:LX/AUr;

    if-nez v0, :cond_0

    .line 1688801
    :goto_0
    return-void

    .line 1688802
    :cond_0
    if-eqz p1, :cond_1

    .line 1688803
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688804
    if-nez v0, :cond_2

    .line 1688805
    :cond_1
    iget-object v0, p0, LX/Aaf;->a:LX/Aag;

    iget-object v0, v0, LX/Aag;->d:LX/03V;

    sget-object v1, LX/Aag;->b:Ljava/lang/String;

    const-string v2, "No funnel metadata request."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1688806
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688807
    check-cast v0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;

    .line 1688808
    if-nez v0, :cond_3

    .line 1688809
    iget-object v0, p0, LX/Aaf;->a:LX/Aag;

    iget-object v0, v0, LX/Aag;->d:LX/03V;

    sget-object v1, LX/Aag;->b:Ljava/lang/String;

    const-string v2, "No funnel query model."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1688810
    :cond_3
    iget-object v1, p0, LX/Aaf;->a:LX/Aag;

    iget-object v1, v1, LX/Aag;->a:LX/AUr;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AUr;->a(LX/0Px;)V

    goto :goto_0
.end method
