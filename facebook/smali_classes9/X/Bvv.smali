.class public final enum LX/Bvv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bvv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bvv;

.field public static final enum PLAYBACK_COMPLETE:LX/Bvv;

.field public static final enum SCHEDULED_LIVE_CANCELLED:LX/Bvv;

.field public static final enum SCHEDULED_LIVE_RESCHEDULED:LX/Bvv;

.field public static final enum SCHEDULED_LIVE_TIMED_OUT:LX/Bvv;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1832990
    new-instance v0, LX/Bvv;

    const-string v1, "PLAYBACK_COMPLETE"

    invoke-direct {v0, v1, v2}, LX/Bvv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bvv;->PLAYBACK_COMPLETE:LX/Bvv;

    .line 1832991
    new-instance v0, LX/Bvv;

    const-string v1, "SCHEDULED_LIVE_TIMED_OUT"

    invoke-direct {v0, v1, v3}, LX/Bvv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bvv;->SCHEDULED_LIVE_TIMED_OUT:LX/Bvv;

    .line 1832992
    new-instance v0, LX/Bvv;

    const-string v1, "SCHEDULED_LIVE_RESCHEDULED"

    invoke-direct {v0, v1, v4}, LX/Bvv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bvv;->SCHEDULED_LIVE_RESCHEDULED:LX/Bvv;

    .line 1832993
    new-instance v0, LX/Bvv;

    const-string v1, "SCHEDULED_LIVE_CANCELLED"

    invoke-direct {v0, v1, v5}, LX/Bvv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bvv;->SCHEDULED_LIVE_CANCELLED:LX/Bvv;

    .line 1832994
    const/4 v0, 0x4

    new-array v0, v0, [LX/Bvv;

    sget-object v1, LX/Bvv;->PLAYBACK_COMPLETE:LX/Bvv;

    aput-object v1, v0, v2

    sget-object v1, LX/Bvv;->SCHEDULED_LIVE_TIMED_OUT:LX/Bvv;

    aput-object v1, v0, v3

    sget-object v1, LX/Bvv;->SCHEDULED_LIVE_RESCHEDULED:LX/Bvv;

    aput-object v1, v0, v4

    sget-object v1, LX/Bvv;->SCHEDULED_LIVE_CANCELLED:LX/Bvv;

    aput-object v1, v0, v5

    sput-object v0, LX/Bvv;->$VALUES:[LX/Bvv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1832989
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bvv;
    .locals 1

    .prologue
    .line 1832988
    const-class v0, LX/Bvv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bvv;

    return-object v0
.end method

.method public static values()[LX/Bvv;
    .locals 1

    .prologue
    .line 1832987
    sget-object v0, LX/Bvv;->$VALUES:[LX/Bvv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bvv;

    return-object v0
.end method
