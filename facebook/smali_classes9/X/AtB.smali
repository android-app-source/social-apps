.class public final LX/AtB;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/AtM;


# direct methods
.method public constructor <init>(LX/AtM;I)V
    .locals 0

    .prologue
    .line 1721204
    iput-object p1, p0, LX/AtB;->b:LX/AtM;

    iput p2, p0, LX/AtB;->a:I

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 3

    .prologue
    .line 1721205
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1721206
    iget-object v1, p0, LX/AtB;->b:LX/AtM;

    iget-object v1, v1, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v2, p0, LX/AtB;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTranslationY(F)V

    .line 1721207
    iget-object v1, p0, LX/AtB;->b:LX/AtM;

    iget-object v1, v1, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1721208
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 6

    .prologue
    .line 1721209
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1721210
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1721211
    iget-object v0, p0, LX/AtB;->b:LX/AtM;

    iget-object v0, v0, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AtB;->b:LX/AtM;

    iget-object v1, v1, LX/AtM;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1721212
    :goto_0
    return-void

    .line 1721213
    :cond_0
    iget-object v0, p0, LX/AtB;->b:LX/AtM;

    iget-object v0, v0, LX/AtM;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
