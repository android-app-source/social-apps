.class public final LX/CRY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1891987
    const-wide/16 v8, 0x0

    .line 1891988
    const/4 v7, 0x0

    .line 1891989
    const/4 v6, 0x0

    .line 1891990
    const/4 v5, 0x0

    .line 1891991
    const/4 v4, 0x0

    .line 1891992
    const/4 v3, 0x0

    .line 1891993
    const/4 v2, 0x0

    .line 1891994
    const/4 v1, 0x0

    .line 1891995
    const/4 v0, 0x0

    .line 1891996
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1891997
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1891998
    const/4 v0, 0x0

    .line 1891999
    :goto_0
    return v0

    .line 1892000
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 1892001
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1892002
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1892003
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1892004
    const-string v4, "creation_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1892005
    const/4 v0, 0x1

    .line 1892006
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1892007
    :cond_1
    const-string v4, "creator"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1892008
    invoke-static {p0, p1}, LX/5u6;->a(LX/15w;LX/186;)I

    move-result v0

    move v12, v0

    goto :goto_1

    .line 1892009
    :cond_2
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1892010
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 1892011
    :cond_3
    const-string v4, "page_rating"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1892012
    const/4 v0, 0x1

    .line 1892013
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v10, v4

    goto :goto_1

    .line 1892014
    :cond_4
    const-string v4, "privacy_scope"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1892015
    invoke-static {p0, p1}, LX/5uE;->a(LX/15w;LX/186;)I

    move-result v0

    move v9, v0

    goto :goto_1

    .line 1892016
    :cond_5
    const-string v4, "story"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1892017
    invoke-static {p0, p1}, LX/5u7;->a(LX/15w;LX/186;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 1892018
    :cond_6
    const-string v4, "value"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1892019
    invoke-static {p0, p1}, LX/5u1;->a(LX/15w;LX/186;)I

    move-result v0

    move v7, v0

    goto/16 :goto_1

    .line 1892020
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1892021
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1892022
    if-eqz v1, :cond_9

    .line 1892023
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1892024
    :cond_9
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1892025
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1892026
    if-eqz v6, :cond_a

    .line 1892027
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v10, v1}, LX/186;->a(III)V

    .line 1892028
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1892029
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1892030
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1892031
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v5

    move v11, v6

    move v12, v7

    move v6, v0

    move v7, v2

    move v13, v3

    move-wide v2, v8

    move v9, v4

    move v8, v13

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1892032
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1892033
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1892034
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1892035
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892036
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1892037
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892038
    if-eqz v0, :cond_1

    .line 1892039
    const-string v1, "creator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892040
    invoke-static {p0, v0, p2, p3}, LX/5u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1892041
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1892042
    if-eqz v0, :cond_2

    .line 1892043
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892044
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1892045
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1892046
    if-eqz v0, :cond_3

    .line 1892047
    const-string v1, "page_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892048
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1892049
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892050
    if-eqz v0, :cond_4

    .line 1892051
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892052
    invoke-static {p0, v0, p2, p3}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1892053
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892054
    if-eqz v0, :cond_5

    .line 1892055
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892056
    invoke-static {p0, v0, p2}, LX/5u7;->a(LX/15i;ILX/0nX;)V

    .line 1892057
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1892058
    if-eqz v0, :cond_6

    .line 1892059
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1892060
    invoke-static {p0, v0, p2}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 1892061
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1892062
    return-void
.end method
