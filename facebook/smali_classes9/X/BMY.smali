.class public LX/BMY;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

.field public final b:LX/BMQ;


# direct methods
.method public constructor <init>(LX/B5j;Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;Landroid/content/Context;LX/BMQ;)V
    .locals 1
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;",
            "Landroid/content/Context;",
            "LX/BMQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777497
    invoke-direct {p0, p3, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1777498
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    iput-object v0, p0, LX/BMY;->a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    .line 1777499
    iput-object p4, p0, LX/BMY;->b:LX/BMQ;

    .line 1777500
    return-void
.end method

.method public static aM(LX/BMY;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1777489
    iget-object v0, p0, LX/BMY;->a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->c()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, LX/1kV;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, LX/1kW;

    goto :goto_0
.end method


# virtual methods
.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 1777496
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aE()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777495
    new-instance v0, LX/BMW;

    invoke-direct {v0, p0}, LX/BMW;-><init>(LX/BMY;)V

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 1777494
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777493
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777492
    new-instance v0, LX/BMV;

    invoke-direct {v0, p0}, LX/BMV;-><init>(LX/BMY;)V

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 1777491
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1

    .prologue
    .line 1777490
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
