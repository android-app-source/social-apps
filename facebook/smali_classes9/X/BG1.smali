.class public final LX/BG1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f4",
        "<[B",
        "LX/5fk;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766493
    iput-object p1, p0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1766494
    sget-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v1, "Photo capture failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766495
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1766496
    check-cast p1, [B

    check-cast p2, LX/5fk;

    .line 1766497
    iget-object v0, p0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->m:LX/0TD;

    new-instance v1, LX/BFz;

    invoke-direct {v1, p0, p1, p2}, LX/BFz;-><init>(LX/BG1;[BLX/5fk;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1766498
    new-instance v1, LX/BG3;

    invoke-direct {v1}, LX/BG3;-><init>()V

    .line 1766499
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1766500
    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 1766501
    new-instance v1, LX/BG0;

    invoke-direct {v1, p0, p2}, LX/BG0;-><init>(LX/BG1;LX/5fk;)V

    iget-object v2, p0, LX/BG1;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->n:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1766502
    return-void
.end method
