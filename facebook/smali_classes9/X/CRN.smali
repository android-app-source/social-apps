.class public final LX/CRN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1891109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1891110
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1891111
    :goto_0
    return v1

    .line 1891112
    :cond_0
    const-string v6, "has_next_page"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1891113
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1891114
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1891115
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1891116
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1891117
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1891118
    const-string v6, "end_cursor"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1891119
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1891120
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1891121
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1891122
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1891123
    if-eqz v0, :cond_4

    .line 1891124
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1891125
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1891098
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1891099
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1891100
    if-eqz v0, :cond_0

    .line 1891101
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1891102
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1891103
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1891104
    if-eqz v0, :cond_1

    .line 1891105
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1891106
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1891107
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1891108
    return-void
.end method
