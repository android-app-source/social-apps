.class public LX/BKR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

.field private b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

.field public final c:LX/0Uh;

.field private final d:LX/BKi;


# direct methods
.method public constructor <init>(LX/0Uh;LX/BKi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1773780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1773781
    iput-object p1, p0, LX/BKR;->c:LX/0Uh;

    .line 1773782
    iput-object p2, p0, LX/BKR;->d:LX/BKi;

    .line 1773783
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1773784
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1773785
    return-void
.end method

.method public static b(LX/0QB;)LX/BKR;
    .locals 3

    .prologue
    .line 1773778
    new-instance v2, LX/BKR;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/BKi;->a(LX/0QB;)LX/BKi;

    move-result-object v1

    check-cast v1, LX/BKi;

    invoke-direct {v2, v0, v1}, LX/BKR;-><init>(LX/0Uh;LX/BKi;)V

    .line 1773779
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Lcom/facebook/base/activity/FbFragmentActivity;ILandroid/content/Intent;)V
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 1773752
    iget-object v0, p0, LX/BKR;->d:LX/BKi;

    iget-object v0, v0, LX/BKi;->c:LX/BKg;

    sget-object v1, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    if-ne v0, v1, :cond_2

    .line 1773753
    if-eqz p1, :cond_0

    .line 1773754
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    iput-object v0, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1773755
    :cond_0
    iget-object v0, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    if-nez v0, :cond_1

    .line 1773756
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1773757
    invoke-virtual {p4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1773758
    new-instance v1, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-direct {v1}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;-><init>()V

    .line 1773759
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1773760
    move-object v0, v1

    .line 1773761
    iput-object v0, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1773762
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1773763
    iget-object v1, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-virtual {v0, p3, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1773764
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1773765
    :cond_1
    :goto_0
    return-void

    .line 1773766
    :cond_2
    if-eqz p1, :cond_3

    .line 1773767
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iput-object v0, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1773768
    :cond_3
    iget-object v0, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-nez v0, :cond_1

    .line 1773769
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1773770
    invoke-virtual {p4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1773771
    new-instance v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;-><init>()V

    .line 1773772
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1773773
    move-object v0, v1

    .line 1773774
    iput-object v0, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1773775
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1773776
    iget-object v1, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v0, p3, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1773777
    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1773731
    iget-object v0, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-eqz v0, :cond_1

    .line 1773732
    iget-object v0, p0, LX/BKR;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->e()V

    .line 1773733
    :cond_0
    :goto_0
    return-void

    .line 1773734
    :cond_1
    iget-object v0, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    if-eqz v0, :cond_0

    .line 1773735
    iget-object v0, p0, LX/BKR;->b:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1773736
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1773737
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/FbFragment;

    .line 1773738
    instance-of v2, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-eqz v2, :cond_3

    .line 1773739
    check-cast v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->e()V

    .line 1773740
    :cond_2
    :goto_1
    goto :goto_0

    .line 1773741
    :cond_3
    instance-of v2, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1773742
    :cond_4
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 1773743
    const/4 v3, 0x0

    const/4 p0, 0x1

    invoke-virtual {v2, v3, p0}, LX/0gc;->b(Ljava/lang/String;I)Z

    .line 1773744
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 1773745
    invoke-virtual {v2, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1773746
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1773747
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1773748
    const v3, 0x7f0d25fd

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v3, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1773749
    :cond_5
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 1773750
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    if-eqz v1, :cond_2

    .line 1773751
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->n(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    goto :goto_1
.end method
