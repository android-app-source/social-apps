.class public final enum LX/BQ5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BQ5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BQ5;

.field public static final enum GRAPHQL_MAIN_REQUEST:LX/BQ5;

.field public static final enum NO_PHOTO:LX/BQ5;

.field public static final enum PRELIMINARY_DATA:LX/BQ5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1781449
    new-instance v0, LX/BQ5;

    const-string v1, "PRELIMINARY_DATA"

    invoke-direct {v0, v1, v2}, LX/BQ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ5;->PRELIMINARY_DATA:LX/BQ5;

    .line 1781450
    new-instance v0, LX/BQ5;

    const-string v1, "GRAPHQL_MAIN_REQUEST"

    invoke-direct {v0, v1, v3}, LX/BQ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ5;->GRAPHQL_MAIN_REQUEST:LX/BQ5;

    .line 1781451
    new-instance v0, LX/BQ5;

    const-string v1, "NO_PHOTO"

    invoke-direct {v0, v1, v4}, LX/BQ5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ5;->NO_PHOTO:LX/BQ5;

    .line 1781452
    const/4 v0, 0x3

    new-array v0, v0, [LX/BQ5;

    sget-object v1, LX/BQ5;->PRELIMINARY_DATA:LX/BQ5;

    aput-object v1, v0, v2

    sget-object v1, LX/BQ5;->GRAPHQL_MAIN_REQUEST:LX/BQ5;

    aput-object v1, v0, v3

    sget-object v1, LX/BQ5;->NO_PHOTO:LX/BQ5;

    aput-object v1, v0, v4

    sput-object v0, LX/BQ5;->$VALUES:[LX/BQ5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1781453
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BQ5;
    .locals 1

    .prologue
    .line 1781454
    const-class v0, LX/BQ5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BQ5;

    return-object v0
.end method

.method public static values()[LX/BQ5;
    .locals 1

    .prologue
    .line 1781455
    sget-object v0, LX/BQ5;->$VALUES:[LX/BQ5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BQ5;

    return-object v0
.end method
