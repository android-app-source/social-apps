.class public LX/Byf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/26L",
        "<",
        "LX/ByV;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/ByV;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 7
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x3

    .line 1837674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837675
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Byf;->b:Ljava/util/Map;

    .line 1837676
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1837677
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/Byf;->c:I

    .line 1837678
    iget v0, p0, LX/Byf;->c:I

    if-nez v0, :cond_0

    .line 1837679
    iput v6, p0, LX/Byf;->d:I

    .line 1837680
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1837681
    iput-object v0, p0, LX/Byf;->a:LX/0Px;

    .line 1837682
    :goto_0
    return-void

    .line 1837683
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget v0, p0, LX/Byf;->c:I

    if-ge v1, v0, :cond_1

    .line 1837684
    new-instance v3, LX/ByV;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v3, v0}, LX/ByV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1837685
    iget-object v0, p0, LX/Byf;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1837686
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1837687
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1837688
    :cond_1
    iget v0, p0, LX/Byf;->c:I

    if-ge v0, v5, :cond_2

    .line 1837689
    iget v0, p0, LX/Byf;->c:I

    div-int v0, v6, v0

    iput v0, p0, LX/Byf;->d:I

    .line 1837690
    :goto_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Byf;->a:LX/0Px;

    goto :goto_0

    .line 1837691
    :cond_2
    const/4 v0, 0x4

    iput v0, p0, LX/Byf;->d:I

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/26N;)I
    .locals 2

    .prologue
    .line 1837692
    check-cast p1, LX/ByV;

    .line 1837693
    iget-object v0, p0, LX/Byf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, LX/Byf;->d:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837672
    iget-object v0, p0, LX/Byf;->a:LX/0Px;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1837673
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/26N;)I
    .locals 1

    .prologue
    .line 1837671
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1837670
    const/16 v0, 0xa

    return v0
.end method

.method public final c(LX/26N;)I
    .locals 2

    .prologue
    .line 1837667
    check-cast p1, LX/ByV;

    const/4 v1, 0x2

    .line 1837668
    iget-object v0, p0, LX/Byf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/Byf;->d:I

    goto :goto_0
.end method

.method public final d(LX/26N;)I
    .locals 1

    .prologue
    .line 1837669
    const/4 v0, 0x4

    return v0
.end method
