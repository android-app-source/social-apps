.class public final enum LX/BGd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BGd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BGd;

.field public static final enum CAMERA_CANCELLED:LX/BGd;

.field public static final enum CAMERA_MEDIA_CAPTURED:LX/BGd;

.field public static final enum CAMERA_OPENED:LX/BGd;

.field public static final enum CANCELLED:LX/BGd;

.field public static final enum FRAGMENT_CREATED:LX/BGd;

.field public static final enum LARGEST_GRID_ROW_REACHED:LX/BGd;

.field public static final enum LAUNCHED:LX/BGd;

.field public static final enum MEDIA_ITEM_DESELECTED:LX/BGd;

.field public static final enum MEDIA_ITEM_SELECTED_IN_GALLERY:LX/BGd;

.field public static final enum MEDIA_ITEM_SELECTED_IN_GRID:LX/BGd;

.field public static final enum MEDIA_STORE_SIZE:LX/BGd;

.field public static final enum NUM_NEW_PHOTOS:LX/BGd;

.field public static final enum PICKER_CREATIVECAM_FRAMES_DOWNLOAD_FAILED:LX/BGd;

.field public static final enum PICKER_CREATIVECAM_IMPRESSION:LX/BGd;

.field public static final enum PICKER_CREATIVECAM_START_FRAMES_DOWNLOAD:LX/BGd;

.field public static final enum PICKER_MOTION_PHOTOS_SHARE_AS_PHOTOS:LX/BGd;

.field public static final enum PICKER_MOTION_PHOTOS_SHARE_AS_VIDEOS:LX/BGd;

.field public static final enum RETURN_TO_COMPOSER:LX/BGd;

.field public static final enum USE_SELECTED_ITEMS:LX/BGd;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1767339
    new-instance v0, LX/BGd;

    const-string v1, "LAUNCHED"

    const-string v2, "photo_picker_open"

    invoke-direct {v0, v1, v4, v2}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->LAUNCHED:LX/BGd;

    .line 1767340
    new-instance v0, LX/BGd;

    const-string v1, "FRAGMENT_CREATED"

    const-string v2, "photo_picker_fragment_created"

    invoke-direct {v0, v1, v5, v2}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->FRAGMENT_CREATED:LX/BGd;

    .line 1767341
    new-instance v0, LX/BGd;

    const-string v1, "CANCELLED"

    const-string v2, "photo_picker_cancel"

    invoke-direct {v0, v1, v6, v2}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->CANCELLED:LX/BGd;

    .line 1767342
    new-instance v0, LX/BGd;

    const-string v1, "RETURN_TO_COMPOSER"

    const-string v2, "photo_picker_to_composer"

    invoke-direct {v0, v1, v7, v2}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->RETURN_TO_COMPOSER:LX/BGd;

    .line 1767343
    new-instance v0, LX/BGd;

    const-string v1, "MEDIA_ITEM_SELECTED_IN_GRID"

    const-string v2, "photo_picker_grid_select_photo"

    invoke-direct {v0, v1, v8, v2}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GRID:LX/BGd;

    .line 1767344
    new-instance v0, LX/BGd;

    const-string v1, "MEDIA_ITEM_SELECTED_IN_GALLERY"

    const/4 v2, 0x5

    const-string v3, "photo_picker_gallery_select_photo"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GALLERY:LX/BGd;

    .line 1767345
    new-instance v0, LX/BGd;

    const-string v1, "MEDIA_ITEM_DESELECTED"

    const/4 v2, 0x6

    const-string v3, "photo_picker_grid_deselect_photo"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->MEDIA_ITEM_DESELECTED:LX/BGd;

    .line 1767346
    new-instance v0, LX/BGd;

    const-string v1, "CAMERA_OPENED"

    const/4 v2, 0x7

    const-string v3, "photo_picker_start_camera"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->CAMERA_OPENED:LX/BGd;

    .line 1767347
    new-instance v0, LX/BGd;

    const-string v1, "CAMERA_CANCELLED"

    const/16 v2, 0x8

    const-string v3, "photo_picker_camera_cancelled"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->CAMERA_CANCELLED:LX/BGd;

    .line 1767348
    new-instance v0, LX/BGd;

    const-string v1, "CAMERA_MEDIA_CAPTURED"

    const/16 v2, 0x9

    const-string v3, "photo_picker_camera_media_captured"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->CAMERA_MEDIA_CAPTURED:LX/BGd;

    .line 1767349
    new-instance v0, LX/BGd;

    const-string v1, "USE_SELECTED_ITEMS"

    const/16 v2, 0xa

    const-string v3, "grid_tap_compose_button"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->USE_SELECTED_ITEMS:LX/BGd;

    .line 1767350
    new-instance v0, LX/BGd;

    const-string v1, "LARGEST_GRID_ROW_REACHED"

    const/16 v2, 0xb

    const-string v3, "simplepicker_largest_grid_row_reached"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->LARGEST_GRID_ROW_REACHED:LX/BGd;

    .line 1767351
    new-instance v0, LX/BGd;

    const-string v1, "MEDIA_STORE_SIZE"

    const/16 v2, 0xc

    const-string v3, "simplepicker_media_store_size"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->MEDIA_STORE_SIZE:LX/BGd;

    .line 1767352
    new-instance v0, LX/BGd;

    const-string v1, "NUM_NEW_PHOTOS"

    const/16 v2, 0xd

    const-string v3, "simplepicker_num_new_photos"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->NUM_NEW_PHOTOS:LX/BGd;

    .line 1767353
    new-instance v0, LX/BGd;

    const-string v1, "PICKER_CREATIVECAM_START_FRAMES_DOWNLOAD"

    const/16 v2, 0xe

    const-string v3, "picker_creativecam_start_frames_download"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->PICKER_CREATIVECAM_START_FRAMES_DOWNLOAD:LX/BGd;

    .line 1767354
    new-instance v0, LX/BGd;

    const-string v1, "PICKER_CREATIVECAM_IMPRESSION"

    const/16 v2, 0xf

    const-string v3, "picker_creativecam_impression"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->PICKER_CREATIVECAM_IMPRESSION:LX/BGd;

    .line 1767355
    new-instance v0, LX/BGd;

    const-string v1, "PICKER_CREATIVECAM_FRAMES_DOWNLOAD_FAILED"

    const/16 v2, 0x10

    const-string v3, "picker_creativecam_frames_download_failed"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->PICKER_CREATIVECAM_FRAMES_DOWNLOAD_FAILED:LX/BGd;

    .line 1767356
    new-instance v0, LX/BGd;

    const-string v1, "PICKER_MOTION_PHOTOS_SHARE_AS_PHOTOS"

    const/16 v2, 0x11

    const-string v3, "picker_motion_photos_share_as_photos"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_PHOTOS:LX/BGd;

    .line 1767357
    new-instance v0, LX/BGd;

    const-string v1, "PICKER_MOTION_PHOTOS_SHARE_AS_VIDEOS"

    const/16 v2, 0x12

    const-string v3, "picker_motion_photos_share_as_videos"

    invoke-direct {v0, v1, v2, v3}, LX/BGd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_VIDEOS:LX/BGd;

    .line 1767358
    const/16 v0, 0x13

    new-array v0, v0, [LX/BGd;

    sget-object v1, LX/BGd;->LAUNCHED:LX/BGd;

    aput-object v1, v0, v4

    sget-object v1, LX/BGd;->FRAGMENT_CREATED:LX/BGd;

    aput-object v1, v0, v5

    sget-object v1, LX/BGd;->CANCELLED:LX/BGd;

    aput-object v1, v0, v6

    sget-object v1, LX/BGd;->RETURN_TO_COMPOSER:LX/BGd;

    aput-object v1, v0, v7

    sget-object v1, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GRID:LX/BGd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BGd;->MEDIA_ITEM_SELECTED_IN_GALLERY:LX/BGd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BGd;->MEDIA_ITEM_DESELECTED:LX/BGd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BGd;->CAMERA_OPENED:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BGd;->CAMERA_CANCELLED:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BGd;->CAMERA_MEDIA_CAPTURED:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BGd;->USE_SELECTED_ITEMS:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/BGd;->LARGEST_GRID_ROW_REACHED:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/BGd;->MEDIA_STORE_SIZE:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/BGd;->NUM_NEW_PHOTOS:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/BGd;->PICKER_CREATIVECAM_START_FRAMES_DOWNLOAD:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/BGd;->PICKER_CREATIVECAM_IMPRESSION:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/BGd;->PICKER_CREATIVECAM_FRAMES_DOWNLOAD_FAILED:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_PHOTOS:LX/BGd;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_VIDEOS:LX/BGd;

    aput-object v2, v0, v1

    sput-object v0, LX/BGd;->$VALUES:[LX/BGd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1767336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1767337
    iput-object p3, p0, LX/BGd;->name:Ljava/lang/String;

    .line 1767338
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BGd;
    .locals 1

    .prologue
    .line 1767335
    const-class v0, LX/BGd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BGd;

    return-object v0
.end method

.method public static values()[LX/BGd;
    .locals 1

    .prologue
    .line 1767333
    sget-object v0, LX/BGd;->$VALUES:[LX/BGd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BGd;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1767334
    iget-object v0, p0, LX/BGd;->name:Ljava/lang/String;

    return-object v0
.end method
