.class public final LX/Ax8;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ax9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/1Ri;

.field public final synthetic c:LX/Ax9;


# direct methods
.method public constructor <init>(LX/Ax9;)V
    .locals 1

    .prologue
    .line 1727583
    iput-object p1, p0, LX/Ax8;->c:LX/Ax9;

    .line 1727584
    move-object v0, p1

    .line 1727585
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1727586
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1727587
    const-string v0, "MemePromptHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1727588
    if-ne p0, p1, :cond_1

    .line 1727589
    :cond_0
    :goto_0
    return v0

    .line 1727590
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1727591
    goto :goto_0

    .line 1727592
    :cond_3
    check-cast p1, LX/Ax8;

    .line 1727593
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1727594
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1727595
    if-eq v2, v3, :cond_0

    .line 1727596
    iget-object v2, p0, LX/Ax8;->a:LX/1Pq;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ax8;->a:LX/1Pq;

    iget-object v3, p1, LX/Ax8;->a:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1727597
    goto :goto_0

    .line 1727598
    :cond_5
    iget-object v2, p1, LX/Ax8;->a:LX/1Pq;

    if-nez v2, :cond_4

    .line 1727599
    :cond_6
    iget-object v2, p0, LX/Ax8;->b:LX/1Ri;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Ax8;->b:LX/1Ri;

    iget-object v3, p1, LX/Ax8;->b:LX/1Ri;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1727600
    goto :goto_0

    .line 1727601
    :cond_7
    iget-object v2, p1, LX/Ax8;->b:LX/1Ri;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
