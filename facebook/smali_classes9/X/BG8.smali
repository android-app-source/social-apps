.class public final LX/BG8;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;)V
    .locals 1

    .prologue
    .line 1766725
    iput-object p1, p0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    invoke-direct {p0}, LX/1cC;-><init>()V

    .line 1766726
    new-instance v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;-><init>(LX/BG8;)V

    iput-object v0, p0, LX/BG8;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766727
    check-cast p2, LX/1ln;

    .line 1766728
    invoke-super {p0, p1, p2, p3}, LX/1cC;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    .line 1766729
    if-eqz p2, :cond_1

    .line 1766730
    iget-object v0, p0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const/4 p1, 0x2

    .line 1766731
    invoke-static {v0, p1}, LX/1af;->e(LX/1af;I)LX/1ai;

    move-result-object v1

    .line 1766732
    instance-of v1, v1, LX/1ao;

    move v1, v1

    .line 1766733
    if-nez v1, :cond_2

    .line 1766734
    const/4 v1, 0x0

    .line 1766735
    :goto_0
    move-object v0, v1

    .line 1766736
    iget-object v1, p0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->b:LX/BG9;

    if-eq v0, v1, :cond_0

    .line 1766737
    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1766738
    iget-object v1, p0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1766739
    :cond_0
    iget-object v0, p0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/BG8;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->post(Ljava/lang/Runnable;)Z

    .line 1766740
    :cond_1
    return-void

    :cond_2
    invoke-static {v0, p1}, LX/1af;->f(LX/1af;I)LX/1ao;

    move-result-object v1

    .line 1766741
    iget-object p1, v1, LX/1ao;->a:LX/1Up;

    move-object v1, p1

    .line 1766742
    goto :goto_0
.end method
