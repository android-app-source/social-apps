.class public final LX/Bs0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic b:LX/1Pb;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;LX/1Pb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1826909
    iput-object p1, p0, LX/Bs0;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object p2, p0, LX/Bs0;->b:LX/1Pb;

    iput-object p3, p0, LX/Bs0;->c:Ljava/lang/String;

    iput-object p4, p0, LX/Bs0;->d:Ljava/lang/String;

    iput-object p5, p0, LX/Bs0;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2146f18a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1826910
    iget-object v1, p0, LX/Bs0;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v1, v2, :cond_0

    .line 1826911
    iget-object v1, p0, LX/Bs0;->b:LX/1Pb;

    iget-object v2, p0, LX/Bs0;->c:Ljava/lang/String;

    iget-object v3, p0, LX/Bs0;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1Pb;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1826912
    :goto_0
    const v1, 0x3bd4cec7

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1826913
    :cond_0
    iget-object v1, p0, LX/Bs0;->b:LX/1Pb;

    iget-object v2, p0, LX/Bs0;->c:Ljava/lang/String;

    iget-object v3, p0, LX/Bs0;->e:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1Pb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
