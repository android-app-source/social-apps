.class public LX/BpJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BpI;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BpK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1822841
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BpJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BpK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822842
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1822843
    iput-object p1, p0, LX/BpJ;->b:LX/0Ot;

    .line 1822844
    return-void
.end method

.method public static a(LX/0QB;)LX/BpJ;
    .locals 4

    .prologue
    .line 1822845
    const-class v1, LX/BpJ;

    monitor-enter v1

    .line 1822846
    :try_start_0
    sget-object v0, LX/BpJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822847
    sput-object v2, LX/BpJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822848
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822849
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822850
    new-instance v3, LX/BpJ;

    const/16 p0, 0x1ce3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BpJ;-><init>(LX/0Ot;)V

    .line 1822851
    move-object v0, v3

    .line 1822852
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822853
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BpJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822854
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822855
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1822856
    check-cast p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    .line 1822857
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1822858
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1822859
    iget-object v0, p0, LX/BpJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BpK;

    iget-object v2, p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    iget-object v3, p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    iget-object v4, p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/BpK;->a(LX/1De;LX/5kD;LX/Bpg;Lcom/facebook/common/callercontext/CallerContext;LX/1np;LX/1np;)LX/1Dg;

    move-result-object v1

    .line 1822860
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1822861
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->d:Ljava/lang/Integer;

    .line 1822862
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1822863
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1822864
    check-cast v0, LX/1bf;

    iput-object v0, p2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->e:LX/1bf;

    .line 1822865
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1822866
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1822867
    invoke-static {}, LX/1dS;->b()V

    .line 1822868
    iget v0, p1, LX/1dQ;->b:I

    .line 1822869
    packed-switch v0, :pswitch_data_0

    .line 1822870
    :goto_0
    return-object v1

    .line 1822871
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1822872
    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    .line 1822873
    iget-object v2, p0, LX/BpJ;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    iget-object v3, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    iget-object p1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->d:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object p2, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->e:LX/1bf;

    .line 1822874
    const/4 p0, 0x0

    invoke-virtual {v3, v2, p2, p0, p1}, LX/Bpg;->a(LX/5kD;LX/1bf;ZI)V

    .line 1822875
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x739a120
        :pswitch_0
    .end packed-switch
.end method
