.class public final LX/Aqn;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1718242
    iput-object p1, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iput-object p2, p0, LX/Aqn;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, LX/Aqn;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1718243
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v1, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Aqg;->e(Ljava/lang/String;)V

    .line 1718244
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    const-string v2, "Error on onNonCancellationFailure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718245
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718246
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1718247
    :try_start_0
    iget-object v1, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, p0, LX/Aqn;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, -0x7eac9af

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1718248
    iput-object v0, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    .line 1718249
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    .line 1718250
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v1, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    iget-object v2, p0, LX/Aqn;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 1718251
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "gif_picker_search_returned"

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "session_id"

    invoke-virtual {v4, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "search_string"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "no_of_items_returned"

    invoke-virtual {v4, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1718252
    iget-object p1, v0, LX/Aqg;->a:LX/0Zb;

    invoke-interface {p1, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1718253
    :goto_0
    return-void

    .line 1718254
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1718255
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    const-string v3, "Error on Successful Result : Search Gifs"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718256
    iget-object v0, p0, LX/Aqn;->c:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    goto :goto_0
.end method
