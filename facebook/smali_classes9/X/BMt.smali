.class public final LX/BMt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0am;

.field public final synthetic b:LX/BMu;


# direct methods
.method public constructor <init>(LX/BMu;LX/0am;)V
    .locals 0

    .prologue
    .line 1778057
    iput-object p1, p0, LX/BMt;->b:LX/BMu;

    iput-object p2, p0, LX/BMt;->a:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1778058
    :try_start_0
    iget-object v0, p0, LX/BMt;->b:LX/BMu;

    iget-object v1, v0, LX/BMu;->c:LX/18V;

    iget-object v0, p0, LX/BMt;->b:LX/BMu;

    iget-object v2, v0, LX/BMu;->d:LX/8os;

    new-instance v0, LX/8ou;

    invoke-direct {v0}, LX/8ou;-><init>()V

    iget-object v3, p0, LX/BMt;->b:LX/BMu;

    iget-wide v4, v3, LX/BMu;->e:J

    .line 1778059
    iput-wide v4, v0, LX/8ou;->a:J

    .line 1778060
    move-object v3, v0

    .line 1778061
    iget-object v0, p0, LX/BMt;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1778062
    iput-object v0, v3, LX/8ou;->b:Ljava/lang/String;

    .line 1778063
    move-object v0, v3

    .line 1778064
    new-instance v6, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;

    iget-wide v8, v0, LX/8ou;->a:J

    iget-object v7, v0, LX/8ou;->b:Ljava/lang/String;

    iget v10, v0, LX/8ou;->c:I

    invoke-direct {v6, v8, v9, v7, v10}, Lcom/facebook/tagging/protocol/FetchGroupMembersParams;-><init>(JLjava/lang/String;I)V

    move-object v0, v6

    .line 1778065
    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1778066
    sget-object v1, LX/BMu;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1778067
    return-object v0

    .line 1778068
    :catch_0
    move-exception v0

    .line 1778069
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
