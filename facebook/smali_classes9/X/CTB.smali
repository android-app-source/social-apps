.class public final enum LX/CTB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CTB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CTB;

.field public static final enum FETCHING_NEXT_SCREEN:LX/CTB;

.field public static final enum INITIALIZING:LX/CTB;

.field public static final enum PLATFORM_ERROR:LX/CTB;

.field public static final enum READY:LX/CTB;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1895382
    new-instance v0, LX/CTB;

    const-string v1, "INITIALIZING"

    invoke-direct {v0, v1, v2}, LX/CTB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CTB;->INITIALIZING:LX/CTB;

    .line 1895383
    new-instance v0, LX/CTB;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, LX/CTB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CTB;->READY:LX/CTB;

    .line 1895384
    new-instance v0, LX/CTB;

    const-string v1, "FETCHING_NEXT_SCREEN"

    invoke-direct {v0, v1, v4}, LX/CTB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    .line 1895385
    new-instance v0, LX/CTB;

    const-string v1, "PLATFORM_ERROR"

    invoke-direct {v0, v1, v5}, LX/CTB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CTB;->PLATFORM_ERROR:LX/CTB;

    .line 1895386
    const/4 v0, 0x4

    new-array v0, v0, [LX/CTB;

    sget-object v1, LX/CTB;->INITIALIZING:LX/CTB;

    aput-object v1, v0, v2

    sget-object v1, LX/CTB;->READY:LX/CTB;

    aput-object v1, v0, v3

    sget-object v1, LX/CTB;->FETCHING_NEXT_SCREEN:LX/CTB;

    aput-object v1, v0, v4

    sget-object v1, LX/CTB;->PLATFORM_ERROR:LX/CTB;

    aput-object v1, v0, v5

    sput-object v0, LX/CTB;->$VALUES:[LX/CTB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1895381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CTB;
    .locals 1

    .prologue
    .line 1895380
    const-class v0, LX/CTB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CTB;

    return-object v0
.end method

.method public static values()[LX/CTB;
    .locals 1

    .prologue
    .line 1895379
    sget-object v0, LX/CTB;->$VALUES:[LX/CTB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CTB;

    return-object v0
.end method
