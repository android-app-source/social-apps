.class public final LX/BR6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V
    .locals 0

    .prologue
    .line 1783200
    iput-object p1, p0, LX/BR6;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1783201
    iget-object v0, p0, LX/BR6;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    const-string v1, "timeline_staging_ground"

    const-string v2, "Failure saving"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783202
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1783203
    check-cast p1, Landroid/net/Uri;

    .line 1783204
    iget-object v0, p0, LX/BR6;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iput-object p1, v0, LX/BR1;->a:Landroid/net/Uri;

    .line 1783205
    iget-object v0, p0, LX/BR6;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/BR6;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1783206
    return-void
.end method
