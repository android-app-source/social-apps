.class public final LX/Akn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic d:LX/0Ot;

.field public final synthetic e:Landroid/view/View;

.field public final synthetic f:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/0Ot;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1709299
    iput-object p1, p0, LX/Akn;->f:LX/1wH;

    iput-object p2, p0, LX/Akn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Akn;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Akn;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p5, p0, LX/Akn;->d:LX/0Ot;

    iput-object p6, p0, LX/Akn;->e:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1709300
    iget-object v0, p0, LX/Akn;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v1, p0, LX/Akn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Akn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1709301
    sget-object v0, LX/0ax;->jc:Ljava/lang/String;

    iget-object v1, p0, LX/Akn;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1709302
    iget-object v0, p0, LX/Akn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/Akn;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1709303
    const-string v1, "module_name"

    const-string v2, "video_insight_in_story_menu"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1709304
    iget-object v1, p0, LX/Akn;->f:LX/1wH;

    iget-object v1, v1, LX/1wH;->a:LX/1SX;

    invoke-static {v1}, LX/1SX;->i(LX/1SX;)Lcom/facebook/content/SecureContextHelper;

    move-result-object v1

    iget-object v2, p0, LX/Akn;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1709305
    return v4
.end method
