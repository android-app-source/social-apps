.class public final LX/Aav;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1689342
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1689343
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689344
    :goto_0
    return v1

    .line 1689345
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689346
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1689347
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1689348
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1689349
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1689350
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1689351
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 1689352
    :cond_3
    const-string v4, "cover_photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1689353
    const/4 v3, 0x0

    .line 1689354
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_9

    .line 1689355
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689356
    :goto_2
    move v0, v3

    .line 1689357
    goto :goto_1

    .line 1689358
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1689359
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1689360
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1689361
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1689362
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689363
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1689364
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1689365
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1689366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_7

    if-eqz v4, :cond_7

    .line 1689367
    const-string v5, "photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1689368
    const/4 v4, 0x0

    .line 1689369
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_d

    .line 1689370
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689371
    :goto_4
    move v0, v4

    .line 1689372
    goto :goto_3

    .line 1689373
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1689374
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1689375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3

    .line 1689376
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689377
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_c

    .line 1689378
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1689379
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1689380
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_b

    if-eqz v5, :cond_b

    .line 1689381
    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1689382
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1689383
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_15

    .line 1689384
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1689385
    :goto_6
    move v0, v5

    .line 1689386
    goto :goto_5

    .line 1689387
    :cond_c
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1689388
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1689389
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v0, v4

    goto :goto_5

    .line 1689390
    :cond_e
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_12

    .line 1689391
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1689392
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1689393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_e

    if-eqz v11, :cond_e

    .line 1689394
    const-string v12, "height"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 1689395
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_7

    .line 1689396
    :cond_f
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 1689397
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_7

    .line 1689398
    :cond_10
    const-string v12, "width"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 1689399
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v8, v0

    move v0, v6

    goto :goto_7

    .line 1689400
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_7

    .line 1689401
    :cond_12
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1689402
    if-eqz v7, :cond_13

    .line 1689403
    invoke-virtual {p1, v5, v10, v5}, LX/186;->a(III)V

    .line 1689404
    :cond_13
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1689405
    if-eqz v0, :cond_14

    .line 1689406
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8, v5}, LX/186;->a(III)V

    .line 1689407
    :cond_14
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_6

    :cond_15
    move v0, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1689408
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1689409
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1689410
    if-eqz v0, :cond_0

    .line 1689411
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689412
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1689413
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1689414
    if-eqz v0, :cond_6

    .line 1689415
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689416
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1689417
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1689418
    if-eqz v1, :cond_5

    .line 1689419
    const-string p1, "photo"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689420
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1689421
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->g(II)I

    move-result p1

    .line 1689422
    if-eqz p1, :cond_4

    .line 1689423
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689424
    const/4 p3, 0x0

    .line 1689425
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1689426
    invoke-virtual {p0, p1, p3, p3}, LX/15i;->a(III)I

    move-result v0

    .line 1689427
    if-eqz v0, :cond_1

    .line 1689428
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689429
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1689430
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1689431
    if-eqz v0, :cond_2

    .line 1689432
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689433
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1689434
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, p3}, LX/15i;->a(III)I

    move-result v0

    .line 1689435
    if-eqz v0, :cond_3

    .line 1689436
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689437
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1689438
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1689439
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1689440
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1689441
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1689442
    return-void
.end method
