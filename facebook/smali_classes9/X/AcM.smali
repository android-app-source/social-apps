.class public final LX/AcM;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V
    .locals 0

    .prologue
    .line 1692451
    iput-object p1, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1692452
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1692453
    check-cast p1, LX/2ow;

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1692454
    sget-object v0, LX/AcJ;->a:[I

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1692455
    :cond_0
    :goto_0
    return-void

    .line 1692456
    :pswitch_0
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    .line 1692457
    iput-boolean v2, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->x:Z

    .line 1692458
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0, v3}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a$redex0(Lcom/facebook/facecastdisplay/LiveEventsPlugin;Z)V

    .line 1692459
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v2}, LX/Aby;->d(Z)V

    .line 1692460
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v2}, LX/Aby;->b(I)V

    .line 1692461
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692462
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1692463
    :pswitch_1
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    .line 1692464
    iput-boolean v3, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->x:Z

    .line 1692465
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0, v2}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a$redex0(Lcom/facebook/facecastdisplay/LiveEventsPlugin;Z)V

    .line 1692466
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v3}, LX/Aby;->d(Z)V

    .line 1692467
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-boolean v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->C:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692468
    :cond_1
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v4}, LX/Aby;->b(I)V

    .line 1692469
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692470
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1692471
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->s:LX/Aca;

    iget-object v0, v0, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .line 1692472
    iget-object v1, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget v1, v1, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->q:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    goto :goto_0

    .line 1692473
    :pswitch_2
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    .line 1692474
    iput-boolean v3, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->x:Z

    .line 1692475
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0, v2}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->a$redex0(Lcom/facebook/facecastdisplay/LiveEventsPlugin;Z)V

    .line 1692476
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v3}, LX/Aby;->d(Z)V

    .line 1692477
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->I()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1692478
    iget-object v0, p0, LX/AcM;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0, v2}, LX/Aby;->b(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
