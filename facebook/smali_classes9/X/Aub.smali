.class public final LX/Aub;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V
    .locals 0

    .prologue
    .line 1722830
    iput-object p1, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;B)V
    .locals 0

    .prologue
    .line 1722831
    invoke-direct {p0, p1}, LX/Aub;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1722832
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1722833
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->f:F

    .line 1722834
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->m:F

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v3, v3, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->f:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    .line 1722835
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->n:F

    .line 1722836
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->f:F

    iget-object v2, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v2, v2, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    mul-float/2addr v1, v2

    .line 1722837
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->q:F

    .line 1722838
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->invalidate()V

    .line 1722839
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1722840
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    .line 1722841
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    .line 1722842
    iget-object v0, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aub;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    .line 1722843
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->m:F

    .line 1722844
    return-void
.end method
