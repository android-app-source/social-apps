.class public final LX/Ah6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

.field public final synthetic b:LX/Ah7;


# direct methods
.method public constructor <init>(LX/Ah7;Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 0

    .prologue
    .line 1702149
    iput-object p1, p0, LX/Ah6;->b:LX/Ah7;

    iput-object p2, p0, LX/Ah6;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1702150
    iget-object v0, p0, LX/Ah6;->b:LX/Ah7;

    iget-object v0, v0, LX/Ah7;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/tipjar/LiveVideoRepetitiveTippingHelper$1$2;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/tipjar/LiveVideoRepetitiveTippingHelper$1$2;-><init>(LX/Ah6;)V

    const v2, 0x117ce6b0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1702151
    iget-object v0, p0, LX/Ah6;->b:LX/Ah7;

    iget-object v0, v0, LX/Ah7;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Ah7;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get tip jar payment setting info, like repetitive tipping"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1702152
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1702153
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1702154
    if-eqz p1, :cond_0

    .line 1702155
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1702156
    if-nez v0, :cond_1

    .line 1702157
    :cond_0
    :goto_0
    return-void

    .line 1702158
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1702159
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;

    .line 1702160
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1702161
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$FetchVideoTipJarPaymentSettingQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchVideoTipJarSettingQueryModels$LiveVideoTipJarPaymentSettingFragmentModel;->j()Z

    move-result v0

    .line 1702162
    iget-object v1, p0, LX/Ah6;->b:LX/Ah7;

    iget-object v1, v1, LX/Ah7;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecastdisplay/tipjar/LiveVideoRepetitiveTippingHelper$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveVideoRepetitiveTippingHelper$1$1;-><init>(LX/Ah6;Z)V

    const v0, 0x220b2391

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
