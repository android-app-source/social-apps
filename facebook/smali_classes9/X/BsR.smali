.class public LX/BsR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1xT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1xT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827543
    iput-object p1, p0, LX/BsR;->a:LX/1xT;

    .line 1827544
    return-void
.end method

.method public static a(LX/0QB;)LX/BsR;
    .locals 4

    .prologue
    .line 1827545
    const-class v1, LX/BsR;

    monitor-enter v1

    .line 1827546
    :try_start_0
    sget-object v0, LX/BsR;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827547
    sput-object v2, LX/BsR;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827548
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827549
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827550
    new-instance p0, LX/BsR;

    invoke-static {v0}, LX/1xT;->a(LX/0QB;)LX/1xT;

    move-result-object v3

    check-cast v3, LX/1xT;

    invoke-direct {p0, v3}, LX/BsR;-><init>(LX/1xT;)V

    .line 1827551
    move-object v0, p0

    .line 1827552
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827553
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BsR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827554
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
