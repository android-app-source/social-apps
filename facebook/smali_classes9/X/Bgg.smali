.class public LX/Bgg;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1808043
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Bgg;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1808044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)Z
    .locals 1

    .prologue
    .line 1808045
    sget-object v0, LX/Bgg;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
