.class public LX/B7z;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B7z;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2sb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public f:Landroid/widget/RadioGroup;

.field private g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field public i:LX/B7w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748352
    new-instance v0, LX/B7x;

    invoke-direct {v0}, LX/B7x;-><init>()V

    sput-object v0, LX/B7z;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1748345
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748346
    const v0, 0x7f0309c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748347
    const v0, 0x7f0d18e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    .line 1748348
    const v0, 0x7f0d18e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B7z;->g:Landroid/widget/TextView;

    .line 1748349
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B7z;->h:Landroid/widget/TextView;

    .line 1748350
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B7z;

    invoke-static {v0}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v2

    check-cast v2, LX/B6l;

    invoke-static {v0}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object p1

    check-cast p1, LX/2sb;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object v2, p0, LX/B7z;->b:LX/B6l;

    iput-object p1, p0, LX/B7z;->c:LX/2sb;

    iput-object v0, p0, LX/B7z;->d:LX/B7W;

    .line 1748351
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748342
    iget-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1748343
    iput-object v1, p0, LX/B7z;->i:LX/B7w;

    .line 1748344
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 5

    .prologue
    .line 1748327
    iput-object p1, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748328
    iget-object v0, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v3

    .line 1748329
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1748330
    :goto_0
    return-void

    .line 1748331
    :cond_0
    iget-object v0, p0, LX/B7z;->g:Landroid/widget/TextView;

    iget-object v1, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748332
    iget-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 1748333
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1748334
    invoke-virtual {p0}, LX/B7z;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1748335
    const v1, 0x7f0309c6

    iget-object v4, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 1748336
    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    check-cast v0, Landroid/widget/RadioButton;

    .line 1748337
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setId(I)V

    .line 1748338
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748339
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1748340
    :cond_1
    iget-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    new-instance v1, LX/B7y;

    invoke-direct {v1, p0}, LX/B7y;-><init>(LX/B7z;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1748341
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748325
    iget-object v0, p0, LX/B7z;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748326
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748353
    iget-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    iget-object v1, p0, LX/B7z;->h:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748354
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748323
    iget-object v0, p0, LX/B7z;->h:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748324
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748322
    iget-object v0, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1748317
    iget-object v0, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->a()LX/0Px;

    move-result-object v0

    .line 1748318
    iget-object v1, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    .line 1748319
    if-ltz v1, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1748320
    :cond_0
    const-string v0, ""

    .line 1748321
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1748314
    iget-object v0, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 1748315
    iget-object v1, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v1

    .line 1748316
    if-ltz v0, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1748310
    iget-object v0, p0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/2sb;->a(Ljava/lang/String;LX/0Px;)I

    move-result v0

    .line 1748311
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1748312
    iget-object v1, p0, LX/B7z;->f:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 1748313
    :cond_0
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748308
    iput-object p1, p0, LX/B7z;->i:LX/B7w;

    .line 1748309
    return-void
.end method
