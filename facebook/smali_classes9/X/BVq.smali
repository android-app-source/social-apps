.class public final enum LX/BVq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVq;

.field public static final enum ELLIPSIZE:LX/BVq;

.field public static final enum GRAVITY:LX/BVq;

.field public static final enum INCLUDE_FONT_PADDING:LX/BVq;

.field public static final enum MAX_LINES:LX/BVq;

.field public static final enum SCROLL_HORIZONTALLY:LX/BVq;

.field public static final enum SINGLE_LINE:LX/BVq;

.field public static final enum TEXT:LX/BVq;

.field public static final enum TEXT_COLOR:LX/BVq;

.field public static final enum TEXT_SIZE:LX/BVq;

.field public static final enum TEXT_STYLE:LX/BVq;

.field public static final enum TYPEFACE:LX/BVq;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791447
    new-instance v0, LX/BVq;

    const-string v1, "TEXT"

    const-string v2, "text"

    invoke-direct {v0, v1, v4, v2}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->TEXT:LX/BVq;

    .line 1791448
    new-instance v0, LX/BVq;

    const-string v1, "GRAVITY"

    const-string v2, "gravity"

    invoke-direct {v0, v1, v5, v2}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->GRAVITY:LX/BVq;

    .line 1791449
    new-instance v0, LX/BVq;

    const-string v1, "SINGLE_LINE"

    const-string v2, "singleLine"

    invoke-direct {v0, v1, v6, v2}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->SINGLE_LINE:LX/BVq;

    .line 1791450
    new-instance v0, LX/BVq;

    const-string v1, "MAX_LINES"

    const-string v2, "maxLines"

    invoke-direct {v0, v1, v7, v2}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->MAX_LINES:LX/BVq;

    .line 1791451
    new-instance v0, LX/BVq;

    const-string v1, "SCROLL_HORIZONTALLY"

    const-string v2, "scrollHorizontally"

    invoke-direct {v0, v1, v8, v2}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->SCROLL_HORIZONTALLY:LX/BVq;

    .line 1791452
    new-instance v0, LX/BVq;

    const-string v1, "ELLIPSIZE"

    const/4 v2, 0x5

    const-string v3, "ellipsize"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->ELLIPSIZE:LX/BVq;

    .line 1791453
    new-instance v0, LX/BVq;

    const-string v1, "TEXT_COLOR"

    const/4 v2, 0x6

    const-string v3, "textColor"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->TEXT_COLOR:LX/BVq;

    .line 1791454
    new-instance v0, LX/BVq;

    const-string v1, "TEXT_SIZE"

    const/4 v2, 0x7

    const-string v3, "textSize"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->TEXT_SIZE:LX/BVq;

    .line 1791455
    new-instance v0, LX/BVq;

    const-string v1, "INCLUDE_FONT_PADDING"

    const/16 v2, 0x8

    const-string v3, "includeFontPadding"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->INCLUDE_FONT_PADDING:LX/BVq;

    .line 1791456
    new-instance v0, LX/BVq;

    const-string v1, "TYPEFACE"

    const/16 v2, 0x9

    const-string v3, "typeface"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->TYPEFACE:LX/BVq;

    .line 1791457
    new-instance v0, LX/BVq;

    const-string v1, "TEXT_STYLE"

    const/16 v2, 0xa

    const-string v3, "textStyle"

    invoke-direct {v0, v1, v2, v3}, LX/BVq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVq;->TEXT_STYLE:LX/BVq;

    .line 1791458
    const/16 v0, 0xb

    new-array v0, v0, [LX/BVq;

    sget-object v1, LX/BVq;->TEXT:LX/BVq;

    aput-object v1, v0, v4

    sget-object v1, LX/BVq;->GRAVITY:LX/BVq;

    aput-object v1, v0, v5

    sget-object v1, LX/BVq;->SINGLE_LINE:LX/BVq;

    aput-object v1, v0, v6

    sget-object v1, LX/BVq;->MAX_LINES:LX/BVq;

    aput-object v1, v0, v7

    sget-object v1, LX/BVq;->SCROLL_HORIZONTALLY:LX/BVq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVq;->ELLIPSIZE:LX/BVq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVq;->TEXT_COLOR:LX/BVq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BVq;->TEXT_SIZE:LX/BVq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BVq;->INCLUDE_FONT_PADDING:LX/BVq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BVq;->TYPEFACE:LX/BVq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BVq;->TEXT_STYLE:LX/BVq;

    aput-object v2, v0, v1

    sput-object v0, LX/BVq;->$VALUES:[LX/BVq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791459
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791460
    iput-object p3, p0, LX/BVq;->mValue:Ljava/lang/String;

    .line 1791461
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVq;
    .locals 4

    .prologue
    .line 1791462
    invoke-static {}, LX/BVq;->values()[LX/BVq;

    move-result-object v1

    .line 1791463
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791464
    aget-object v2, v1, v0

    .line 1791465
    iget-object v3, v2, LX/BVq;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791466
    return-object v2

    .line 1791467
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791468
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown text view attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVq;
    .locals 1

    .prologue
    .line 1791469
    const-class v0, LX/BVq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVq;

    return-object v0
.end method

.method public static values()[LX/BVq;
    .locals 1

    .prologue
    .line 1791470
    sget-object v0, LX/BVq;->$VALUES:[LX/BVq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVq;

    return-object v0
.end method
