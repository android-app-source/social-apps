.class public LX/CH5;
.super LX/CH4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/CH4",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0t2;

.field public final b:LX/0Uh;

.field private final c:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/CH5",
            "<TT;>.RequestCallbackHolder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0t2;LX/0Uh;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x5

    .line 1866165
    invoke-direct {p0, p1}, LX/CH4;-><init>(LX/0tX;)V

    .line 1866166
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/CH5;->c:LX/0QI;

    .line 1866167
    iput-object p2, p0, LX/CH5;->a:LX/0t2;

    .line 1866168
    iput-object p3, p0, LX/CH5;->b:LX/0Uh;

    .line 1866169
    return-void
.end method

.method private declared-synchronized a(LX/0zO;)LX/Cj3;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<TT;>;)",
            "LX/CH5",
            "<TT;>.RequestCallbackHolder;"
        }
    .end annotation

    .prologue
    .line 1866170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CH5;->a:LX/0t2;

    invoke-virtual {p1, v0}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    .line 1866171
    iget-object v1, p0, LX/CH5;->c:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cj3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1866172
    monitor-exit p0

    return-object v0

    .line 1866173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/CH5;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1866174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CH5;->c:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1866175
    monitor-exit p0

    return-void

    .line 1866176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(LX/CGs;LX/0Ve;)V
    .locals 9
    .param p1    # LX/CGs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<TT;>;>;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1866177
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1866178
    :cond_0
    :goto_0
    return-void

    .line 1866179
    :cond_1
    invoke-interface {p1}, LX/CGs;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 1866180
    iget-object v1, p0, LX/CH5;->b:LX/0Uh;

    const/16 v2, 0x3d9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v1, v1

    .line 1866181
    if-eqz v1, :cond_4

    .line 1866182
    invoke-direct {p0, v0}, LX/CH5;->a(LX/0zO;)LX/Cj3;

    move-result-object v1

    .line 1866183
    iget-wide v2, v0, LX/0zO;->c:J

    .line 1866184
    if-eqz v1, :cond_2

    .line 1866185
    iget-wide v7, v1, LX/Cj3;->d:J

    move-wide v4, v7

    .line 1866186
    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 1866187
    sget-object v2, LX/0zS;->b:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1866188
    new-instance v0, LX/Cj2;

    invoke-direct {v0, p0, p2}, LX/Cj2;-><init>(LX/CH5;LX/0Ve;)V

    .line 1866189
    invoke-virtual {v1, v0}, LX/Cj3;->a(LX/0Ve;)V

    .line 1866190
    new-instance v1, LX/Cj1;

    invoke-direct {v1, p0, p1}, LX/Cj1;-><init>(LX/CH5;LX/CGs;)V

    invoke-super {p0, v1, v0}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0

    .line 1866191
    :cond_2
    iget-object v2, p0, LX/CH5;->a:LX/0t2;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v3

    .line 1866192
    if-nez v1, :cond_3

    .line 1866193
    new-instance v1, LX/Cj3;

    iget-wide v4, v0, LX/0zO;->c:J

    move-object v2, p0

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, LX/Cj3;-><init>(LX/CH5;Ljava/lang/String;JLX/0Ve;)V

    .line 1866194
    iget-object v0, p0, LX/CH5;->c:LX/0QI;

    invoke-interface {v0, v3, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1866195
    invoke-super {p0, p1, v1}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0

    .line 1866196
    :cond_3
    invoke-super {p0, p1, p2}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0

    .line 1866197
    :cond_4
    invoke-super {p0, p1, p2}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0
.end method
