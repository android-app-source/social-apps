.class public final LX/BjA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventCreationNikumanActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 0

    .prologue
    .line 1811125
    iput-object p1, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1811111
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1811112
    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;

    .line 1811113
    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1811114
    :goto_0
    return-void

    .line 1811115
    :cond_0
    iget-object v1, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    .line 1811116
    invoke-static {v1, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1811117
    iget-object v0, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    iget-object v1, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v1, v1, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811118
    iget-object p1, v1, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v1, p1

    .line 1811119
    iput-object v1, v0, LX/BkU;->k:Lcom/facebook/events/model/PrivacyType;

    .line 1811120
    iget-object v0, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    const/4 v1, 0x0

    .line 1811121
    invoke-static {v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811122
    iget-object v0, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v1, p0, LX/BjA;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v1, v1, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811123
    iget-object p0, v1, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v1, p0

    .line 1811124
    invoke-static {v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/events/model/PrivacyType;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1811109
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1811110
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/BjA;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
