.class public LX/BfE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BfC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BfC",
        "<",
        "LX/97f;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805768
    iput-object p1, p0, LX/BfE;->a:LX/0Or;

    .line 1805769
    return-void
.end method


# virtual methods
.method public final a()LX/BeD;
    .locals 1

    .prologue
    .line 1805770
    sget-object v0, LX/BeD;->CITY_PICKER:LX/BeD;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1805771
    check-cast p1, LX/97f;

    .line 1805772
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/BfE;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 1805773
    const-string v0, "target_fragment"

    sget-object v2, LX/0cQ;->CITY_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1805774
    invoke-static {p1}, LX/Bgb;->h(LX/97f;)Landroid/location/Location;

    move-result-object v0

    .line 1805775
    const-string v2, "extra_current_location"

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1805776
    const-string v0, "extra_logger_type"

    sget-object v2, LX/CdF;->NO_LOGGER:LX/CdF;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1805777
    return-object v1

    .line 1805778
    :cond_0
    new-instance v0, Landroid/location/Location;

    const-string v3, ""

    invoke-direct {v0, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1805779
    check-cast p2, LX/97f;

    .line 1805780
    sget-object v0, LX/Cct;->a:Ljava/lang/String;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1805781
    invoke-static {p2, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/97f;

    move-result-object v0

    return-object v0
.end method
