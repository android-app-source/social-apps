.class public final LX/Aht;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "feedAwesomizerProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "feedAwesomizerProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1704309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1704310
    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;)LX/Aht;
    .locals 4

    .prologue
    .line 1704300
    new-instance v0, LX/Aht;

    invoke-direct {v0}, LX/Aht;-><init>()V

    .line 1704301
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/Aht;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1704302
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v1

    iput-object v1, v0, LX/Aht;->b:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    .line 1704303
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/Aht;->c:LX/15i;

    iput v1, v0, LX/Aht;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1704304
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Aht;->e:Ljava/lang/String;

    .line 1704305
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Aht;->f:Ljava/lang/String;

    .line 1704306
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/Aht;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1704307
    return-object v0

    .line 1704308
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1704278
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1704279
    iget-object v1, p0, LX/Aht;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1704280
    iget-object v3, p0, LX/Aht;->b:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1704281
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, LX/Aht;->c:LX/15i;

    iget v7, p0, LX/Aht;->d:I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v5, 0x4237b83a

    invoke-static {v6, v7, v5}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1704282
    iget-object v6, p0, LX/Aht;->e:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1704283
    iget-object v7, p0, LX/Aht;->f:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1704284
    iget-object v8, p0, LX/Aht;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1704285
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1704286
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1704287
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1704288
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1704289
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1704290
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1704291
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1704292
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1704293
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1704294
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1704295
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1704296
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1704297
    new-instance v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;

    invoke-direct {v1, v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;-><init>(LX/15i;)V

    .line 1704298
    return-object v1

    .line 1704299
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
