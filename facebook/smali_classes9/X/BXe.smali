.class public final LX/BXe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BXd;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 0

    .prologue
    .line 1793324
    iput-object p1, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;B)V
    .locals 0

    .prologue
    .line 1793325
    invoke-direct {p0, p1}, LX/BXe;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1793326
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->INVISIBLE:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->c(LX/BXk;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793327
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->REORDER:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793328
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    .line 1793329
    invoke-static {v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->e$redex0(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    .line 1793330
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1793331
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->INVISIBLE:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->c(LX/BXk;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793332
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793333
    iget-object v0, p0, LX/BXe;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    .line 1793334
    invoke-static {v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->g$redex0(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    .line 1793335
    :cond_0
    return-void
.end method
