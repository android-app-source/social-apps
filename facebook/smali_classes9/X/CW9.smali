.class public final LX/CW9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1907667
    const/4 v13, 0x0

    .line 1907668
    const/4 v12, 0x0

    .line 1907669
    const/4 v11, 0x0

    .line 1907670
    const/4 v10, 0x0

    .line 1907671
    const/4 v9, 0x0

    .line 1907672
    const/4 v8, 0x0

    .line 1907673
    const/4 v7, 0x0

    .line 1907674
    const/4 v6, 0x0

    .line 1907675
    const/4 v5, 0x0

    .line 1907676
    const/4 v4, 0x0

    .line 1907677
    const/4 v3, 0x0

    .line 1907678
    const/4 v2, 0x0

    .line 1907679
    const/4 v1, 0x0

    .line 1907680
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1907681
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1907682
    const/4 v1, 0x0

    .line 1907683
    :goto_0
    return v1

    .line 1907684
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1907685
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_b

    .line 1907686
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1907687
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1907688
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1907689
    const-string v15, "additional_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1907690
    invoke-static/range {p0 .. p1}, LX/CW8;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1907691
    :cond_2
    const-string v15, "component_flow_id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1907692
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1907693
    :cond_3
    const-string v15, "need_delivery_address"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1907694
    const/4 v3, 0x1

    .line 1907695
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1907696
    :cond_4
    const-string v15, "need_phone_number"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1907697
    const/4 v2, 0x1

    .line 1907698
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1907699
    :cond_5
    const-string v15, "need_verified_email"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1907700
    const/4 v1, 0x1

    .line 1907701
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1907702
    :cond_6
    const-string v15, "order_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1907703
    invoke-static/range {p0 .. p1}, LX/CW6;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1907704
    :cond_7
    const-string v15, "provider_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1907705
    invoke-static/range {p0 .. p1}, LX/CW5;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1907706
    :cond_8
    const-string v15, "screen_id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1907707
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1907708
    :cond_9
    const-string v15, "screen_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1907709
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto/16 :goto_1

    .line 1907710
    :cond_a
    const-string v15, "title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1907711
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1907712
    :cond_b
    const/16 v14, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1907713
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1907714
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1907715
    if-eqz v3, :cond_c

    .line 1907716
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1907717
    :cond_c
    if-eqz v2, :cond_d

    .line 1907718
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 1907719
    :cond_d
    if-eqz v1, :cond_e

    .line 1907720
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->a(IZ)V

    .line 1907721
    :cond_e
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1907722
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1907723
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1907724
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1907725
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1907726
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
