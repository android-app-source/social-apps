.class public final LX/CDL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/361;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3EE;

.field public b:LX/1Pe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/3FV;

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/3FN;

.field public f:LX/3FW;

.field public g:LX/3FX;

.field public h:LX/3FY;

.field public i:LX/3FZ;

.field public j:LX/3Fa;

.field public final synthetic k:LX/361;


# direct methods
.method public constructor <init>(LX/361;)V
    .locals 1

    .prologue
    .line 1859024
    iput-object p1, p0, LX/CDL;->k:LX/361;

    .line 1859025
    move-object v0, p1

    .line 1859026
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1859027
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1859028
    const-string v0, "OlderRichVideoAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/361;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859029
    check-cast p1, LX/CDL;

    .line 1859030
    iget-object v0, p1, LX/CDL;->e:LX/3FN;

    iput-object v0, p0, LX/CDL;->e:LX/3FN;

    .line 1859031
    iget-object v0, p1, LX/CDL;->f:LX/3FW;

    iput-object v0, p0, LX/CDL;->f:LX/3FW;

    .line 1859032
    iget-object v0, p1, LX/CDL;->g:LX/3FX;

    iput-object v0, p0, LX/CDL;->g:LX/3FX;

    .line 1859033
    iget-object v0, p1, LX/CDL;->h:LX/3FY;

    iput-object v0, p0, LX/CDL;->h:LX/3FY;

    .line 1859034
    iget-object v0, p1, LX/CDL;->i:LX/3FZ;

    iput-object v0, p0, LX/CDL;->i:LX/3FZ;

    .line 1859035
    iget-object v0, p1, LX/CDL;->j:LX/3Fa;

    iput-object v0, p0, LX/CDL;->j:LX/3Fa;

    .line 1859036
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1859037
    if-ne p0, p1, :cond_1

    .line 1859038
    :cond_0
    :goto_0
    return v0

    .line 1859039
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1859040
    goto :goto_0

    .line 1859041
    :cond_3
    check-cast p1, LX/CDL;

    .line 1859042
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1859043
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1859044
    if-eq v2, v3, :cond_0

    .line 1859045
    iget-object v2, p0, LX/CDL;->a:LX/3EE;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDL;->a:LX/3EE;

    iget-object v3, p1, LX/CDL;->a:LX/3EE;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1859046
    goto :goto_0

    .line 1859047
    :cond_5
    iget-object v2, p1, LX/CDL;->a:LX/3EE;

    if-nez v2, :cond_4

    .line 1859048
    :cond_6
    iget-object v2, p0, LX/CDL;->b:LX/1Pe;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CDL;->b:LX/1Pe;

    iget-object v3, p1, LX/CDL;->b:LX/1Pe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1859049
    goto :goto_0

    .line 1859050
    :cond_8
    iget-object v2, p1, LX/CDL;->b:LX/1Pe;

    if-nez v2, :cond_7

    .line 1859051
    :cond_9
    iget-object v2, p0, LX/CDL;->c:LX/3FV;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/CDL;->c:LX/3FV;

    iget-object v3, p1, LX/CDL;->c:LX/3FV;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1859052
    goto :goto_0

    .line 1859053
    :cond_b
    iget-object v2, p1, LX/CDL;->c:LX/3FV;

    if-nez v2, :cond_a

    .line 1859054
    :cond_c
    iget-object v2, p0, LX/CDL;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/CDL;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/CDL;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1859055
    goto :goto_0

    .line 1859056
    :cond_d
    iget-object v2, p1, LX/CDL;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1859057
    const/4 v1, 0x0

    .line 1859058
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDL;

    .line 1859059
    iput-object v1, v0, LX/CDL;->e:LX/3FN;

    .line 1859060
    iput-object v1, v0, LX/CDL;->f:LX/3FW;

    .line 1859061
    iput-object v1, v0, LX/CDL;->g:LX/3FX;

    .line 1859062
    iput-object v1, v0, LX/CDL;->h:LX/3FY;

    .line 1859063
    iput-object v1, v0, LX/CDL;->i:LX/3FZ;

    .line 1859064
    iput-object v1, v0, LX/CDL;->j:LX/3Fa;

    .line 1859065
    return-object v0
.end method
