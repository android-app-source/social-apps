.class public final LX/COT;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COT;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COR;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883109
    const/4 v0, 0x0

    sput-object v0, LX/COT;->a:LX/COT;

    .line 1883110
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COT;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883132
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883133
    new-instance v0, LX/COV;

    invoke-direct {v0}, LX/COV;-><init>()V

    iput-object v0, p0, LX/COT;->c:LX/COV;

    .line 1883134
    return-void
.end method

.method public static declared-synchronized q()LX/COT;
    .locals 2

    .prologue
    .line 1883128
    const-class v1, LX/COT;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COT;->a:LX/COT;

    if-nez v0, :cond_0

    .line 1883129
    new-instance v0, LX/COT;

    invoke-direct {v0}, LX/COT;-><init>()V

    sput-object v0, LX/COT;->a:LX/COT;

    .line 1883130
    :cond_0
    sget-object v0, LX/COT;->a:LX/COT;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1883113
    check-cast p2, LX/COS;

    .line 1883114
    iget-object v0, p2, LX/COS;->a:LX/CNb;

    iget-object v1, p2, LX/COS;->b:LX/CNc;

    iget-object v2, p2, LX/COS;->c:Ljava/lang/String;

    const/high16 p2, -0x1000000

    .line 1883115
    const-string v3, "font-size"

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v3

    .line 1883116
    const-string v4, "line-height"

    invoke-virtual {v0, v4, v3}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v4

    .line 1883117
    div-float v5, v4, v3

    move v4, v5

    .line 1883118
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const-string p0, "ranges"

    invoke-virtual {v0, p0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object p0

    invoke-static {v2, p0, p1, v1}, LX/COV;->a(Ljava/lang/String;LX/0Px;LX/1De;LX/CNc;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    const-string v5, "color"

    invoke-virtual {v0, v5, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const-string v5, "horizontal-alignment"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CPv;->b(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const-string v5, "max-lines"

    const p0, 0x7fffffff

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    const-string v5, "truncation-mode"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CPv;->a(Ljava/lang/String;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->j(F)LX/1ne;

    move-result-object v3

    const-string v4, "shadow-color"

    invoke-virtual {v0, v4, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/1ne;->k(I)LX/1ne;

    move-result-object v3

    const-string v4, "shadow-offset-x"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, LX/1ne;->d(F)LX/1ne;

    move-result-object v3

    const-string v4, "shadow-offset-y"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, LX/1ne;->e(F)LX/1ne;

    move-result-object v3

    invoke-static {v0, p1}, LX/COM;->a(LX/CNb;Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, LX/1ne;->c(F)LX/1ne;

    move-result-object v3

    .line 1883119
    const-string v4, "font-family"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1883120
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    .line 1883121
    const-string v5, "font-family"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1883122
    invoke-static {v5, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    .line 1883123
    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 1883124
    :cond_0
    :goto_0
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1883125
    return-object v0

    .line 1883126
    :cond_1
    const-string v4, "font-weight"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1883127
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883111
    invoke-static {}, LX/1dS;->b()V

    .line 1883112
    const/4 v0, 0x0

    return-object v0
.end method
