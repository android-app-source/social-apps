.class public final LX/BBf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1757746
    const/4 v9, 0x0

    .line 1757747
    const/4 v8, 0x0

    .line 1757748
    const/4 v7, 0x0

    .line 1757749
    const/4 v6, 0x0

    .line 1757750
    const/4 v5, 0x0

    .line 1757751
    const/4 v4, 0x0

    .line 1757752
    const/4 v3, 0x0

    .line 1757753
    const/4 v2, 0x0

    .line 1757754
    const/4 v1, 0x0

    .line 1757755
    const/4 v0, 0x0

    .line 1757756
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1757757
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757758
    const/4 v0, 0x0

    .line 1757759
    :goto_0
    return v0

    .line 1757760
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757761
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1757762
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1757763
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1757764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1757765
    const-string v11, "cache_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1757766
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1757767
    :cond_2
    const-string v11, "cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1757768
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1757769
    :cond_3
    const-string v11, "eligible_buckets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1757770
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1757771
    :cond_4
    const-string v11, "local_is_rich_notif_collapsed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1757772
    const/4 v1, 0x1

    .line 1757773
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1757774
    :cond_5
    const-string v11, "local_num_impressions"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1757775
    const/4 v0, 0x1

    .line 1757776
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 1757777
    :cond_6
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1757778
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1757779
    :cond_7
    const-string v11, "notif_option_sets"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1757780
    invoke-static {p0, p1}, LX/BBe;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1757781
    :cond_8
    const-string v11, "rich_notification"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1757782
    invoke-static {p0, p1}, LX/BDV;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1757783
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1757784
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1757785
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1757786
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1757787
    if-eqz v1, :cond_a

    .line 1757788
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1757789
    :cond_a
    if-eqz v0, :cond_b

    .line 1757790
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 1757791
    :cond_b
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1757792
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1757793
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1757794
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1757795
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1757796
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1757797
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/BBf;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757798
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1757799
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1757800
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1757801
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1757802
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1757803
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1757804
    invoke-static {p0, p1}, LX/BBf;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1757805
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1757806
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1757807
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1757808
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757809
    if-eqz v0, :cond_0

    .line 1757810
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757811
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757812
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757813
    if-eqz v0, :cond_1

    .line 1757814
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757815
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757816
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1757817
    if-eqz v0, :cond_2

    .line 1757818
    const-string v0, "eligible_buckets"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757819
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1757820
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1757821
    if-eqz v0, :cond_3

    .line 1757822
    const-string v1, "local_is_rich_notif_collapsed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757823
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1757824
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1757825
    if-eqz v0, :cond_4

    .line 1757826
    const-string v1, "local_num_impressions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757827
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1757828
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757829
    if-eqz v0, :cond_5

    .line 1757830
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757831
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757832
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757833
    if-eqz v0, :cond_6

    .line 1757834
    const-string v1, "notif_option_sets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757835
    invoke-static {p0, v0, p2, p3}, LX/BBe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757836
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757837
    if-eqz v0, :cond_7

    .line 1757838
    const-string v1, "rich_notification"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757839
    invoke-static {p0, v0, p2, p3}, LX/BDV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757840
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757841
    return-void
.end method
