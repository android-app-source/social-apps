.class public final LX/BEx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V
    .locals 0

    .prologue
    .line 1765183
    iput-object p1, p0, LX/BEx;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1765164
    iget-object v0, p0, LX/BEx;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1765165
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AaV;

    .line 1765166
    invoke-interface {v1}, LX/AaV;->iK_()V

    goto :goto_0

    .line 1765167
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1765168
    check-cast p1, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1765169
    iget-object v0, p0, LX/BEx;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1765170
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AaV;

    .line 1765171
    invoke-interface {v1}, LX/AaV;->b()V

    goto :goto_0

    .line 1765172
    :cond_0
    iput-object p1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1765173
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1765174
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1765175
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->b:LX/0Px;

    move-object v1, v3

    .line 1765176
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1765177
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1765178
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->c:LX/0Px;

    move-object v1, v3

    .line 1765179
    invoke-virtual {v2, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1765180
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AaV;

    .line 1765181
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    invoke-interface {v1, p0}, LX/AaV;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1765182
    :cond_1
    return-void
.end method
