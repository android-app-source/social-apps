.class public LX/CIh;
.super LX/CIg;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Yh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874662
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, LX/CIg;-><init>(LX/8Yh;LX/Crz;)V

    .line 1874663
    iput-object p1, p0, LX/CIh;->a:Landroid/content/Context;

    .line 1874664
    return-void
.end method

.method public static b(LX/0QB;)LX/CIh;
    .locals 5

    .prologue
    .line 1874617
    const-class v1, LX/CIh;

    monitor-enter v1

    .line 1874618
    :try_start_0
    sget-object v0, LX/CIh;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1874619
    sput-object v2, LX/CIh;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1874620
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874621
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1874622
    new-instance p0, LX/CIh;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/8Yh;->a(LX/0QB;)LX/8Yh;

    move-result-object v4

    check-cast v4, LX/8Yh;

    invoke-direct {p0, v3, v4}, LX/CIh;-><init>(Landroid/content/Context;LX/8Yh;)V

    .line 1874623
    move-object v0, p0

    .line 1874624
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1874625
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CIh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874626
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1874627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V
    .locals 6
    .param p1    # Landroid/widget/TextView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1874628
    const/16 v5, 0x11

    const/4 v4, 0x5

    const/4 v3, 0x3

    .line 1874629
    if-nez p2, :cond_1

    .line 1874630
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 1874631
    :goto_0
    if-nez p3, :cond_0

    .line 1874632
    :goto_1
    return-void

    .line 1874633
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/CIg;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1874634
    invoke-virtual {p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/CIg;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1874635
    invoke-virtual {p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1874636
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1874637
    :goto_2
    invoke-virtual {p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->d()I

    move-result v0

    const/4 v1, 0x1

    .line 1874638
    if-gtz v0, :cond_3

    .line 1874639
    const v1, 0x7fffffff

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1874640
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1874641
    :goto_3
    invoke-virtual {p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1874642
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1874643
    :goto_4
    check-cast p1, LX/CtG;

    const/4 v0, 0x1

    .line 1874644
    iput-boolean v0, p1, LX/CtG;->h:Z

    .line 1874645
    goto :goto_1

    .line 1874646
    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1874647
    sget-object v1, LX/CIf;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1874648
    :pswitch_0
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 1874649
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 1874650
    :pswitch_1
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1874651
    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 1874652
    :pswitch_2
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 1874653
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 1874654
    :cond_2
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_2

    .line 1874655
    :cond_3
    if-ne v0, v1, :cond_4

    .line 1874656
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1874657
    :goto_5
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_3

    .line 1874658
    :cond_4
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_5

    .line 1874659
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object v1, p1

    .line 1874660
    check-cast v1, LX/CtG;

    iget-object v3, p0, LX/CIh;->a:Landroid/content/Context;

    int-to-float v2, v2

    invoke-static {v3, v2}, LX/0tP;->b(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v1, v2}, LX/CtG;->a(I)V

    .line 1874661
    invoke-virtual {p1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->requestLayout()V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
