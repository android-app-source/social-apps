.class public LX/BEB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/BEC;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1764192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764193
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1764194
    check-cast p1, LX/BEC;

    .line 1764195
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1764196
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "JSON"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1764197
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "xray_analysis"

    .line 1764198
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1764199
    move-object v1, v1

    .line 1764200
    const-string v2, "POST"

    .line 1764201
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1764202
    move-object v1, v1

    .line 1764203
    const-string v2, "accessible_photos/xray_analysis"

    .line 1764204
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1764205
    move-object v1, v1

    .line 1764206
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1764207
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1764208
    move-object v0, v1

    .line 1764209
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1764210
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1764211
    move-object v0, v0

    .line 1764212
    iget-object v1, p1, LX/BEC;->a:LX/4cq;

    if-eqz v1, :cond_0

    .line 1764213
    const/4 v1, 0x1

    new-array v1, v1, [LX/4cQ;

    const/4 v2, 0x0

    new-instance v3, LX/4cQ;

    const-string v4, "image"

    iget-object v5, p1, LX/BEC;->a:LX/4cq;

    invoke-direct {v3, v4, v5}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1764214
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1764215
    :cond_0
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1764216
    const/4 v3, 0x1

    .line 1764217
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1764218
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1764219
    const-string v1, "xray_tags"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1764220
    const-string v1, "xray_tags"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1764221
    :cond_0
    new-instance v0, LX/2Oo;

    const/4 v1, 0x1

    const-string v2, "Invalid response: Missing xray_tags key"

    invoke-static {v1, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v1

    invoke-virtual {v1}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764222
    :catch_0
    move-exception v0

    .line 1764223
    new-instance v1, LX/2Oo;

    const-string v2, "Invalid response: Parse error"

    invoke-static {v3, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1764224
    iput-object v0, v2, LX/2AV;->d:Ljava/lang/String;

    .line 1764225
    move-object v0, v2

    .line 1764226
    invoke-virtual {v0}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v1
.end method
