.class public LX/C6o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/C6g;

.field public final c:LX/03V;

.field public final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1850328
    const-class v0, LX/C6o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/C6o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/C6g;LX/03V;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1850313
    iput-object p1, p0, LX/C6o;->b:LX/C6g;

    .line 1850314
    iput-object p2, p0, LX/C6o;->c:LX/03V;

    .line 1850315
    iput-object p3, p0, LX/C6o;->d:LX/0SG;

    .line 1850316
    return-void
.end method

.method public static a(LX/0QB;)LX/C6o;
    .locals 6

    .prologue
    .line 1850317
    const-class v1, LX/C6o;

    monitor-enter v1

    .line 1850318
    :try_start_0
    sget-object v0, LX/C6o;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850319
    sput-object v2, LX/C6o;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850322
    new-instance p0, LX/C6o;

    invoke-static {v0}, LX/C6g;->b(LX/0QB;)LX/C6g;

    move-result-object v3

    check-cast v3, LX/C6g;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/C6o;-><init>(LX/C6g;LX/03V;LX/0SG;)V

    .line 1850323
    move-object v0, p0

    .line 1850324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
