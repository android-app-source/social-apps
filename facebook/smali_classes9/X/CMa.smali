.class public final LX/CMa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;)V
    .locals 0

    .prologue
    .line 1880768
    iput-object p1, p0, LX/CMa;->a:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1880769
    iget-object v0, p0, LX/CMa;->a:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v0, v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->m:LX/CMf;

    iget-object v1, p0, LX/CMa;->a:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v1, v1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->u:Ljava/lang/String;

    .line 1880770
    iget-object v2, v0, LX/CMf;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "ephemeral_cancel_change"

    invoke-direct {p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "source"

    invoke-virtual {p0, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1880771
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 1880772
    return-void
.end method
