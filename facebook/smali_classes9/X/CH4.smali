.class public abstract LX/CH4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
        "<",
        "LX/0zO",
        "<TT;>;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0

    .prologue
    .line 1866156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1866157
    iput-object p1, p0, LX/CH4;->a:LX/0tX;

    .line 1866158
    return-void
.end method


# virtual methods
.method public a(LX/CGs;LX/0Ve;)V
    .locals 2
    .param p1    # LX/CGs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<TT;>;>;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1866159
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1866160
    :cond_0
    :goto_0
    return-void

    .line 1866161
    :cond_1
    invoke-interface {p1}, LX/CGs;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 1866162
    iget-object v1, p0, LX/CH4;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1866163
    sget-object v1, LX/131;->INSTANCE:LX/131;

    move-object v1, v1

    .line 1866164
    invoke-static {v0, p2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
