.class public final LX/Bnx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bny;


# direct methods
.method public constructor <init>(LX/Bny;)V
    .locals 0

    .prologue
    .line 1821335
    iput-object p1, p0, LX/Bnx;->a:LX/Bny;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1821336
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821337
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 1821338
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1821339
    :cond_0
    :goto_0
    return-void

    .line 1821340
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1821341
    iget-object v1, p0, LX/Bnx;->a:LX/Bny;

    iget-object v1, v1, LX/Bny;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1821342
    iget-object v1, p0, LX/Bnx;->a:LX/Bny;

    iget-object v1, v1, LX/Bny;->c:LX/Bnz;

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 1821343
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, LX/Bnz;->d:Lcom/facebook/user/model/User;

    .line 1821344
    iget-object p0, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, p0

    .line 1821345
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1821346
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->T()Z

    move-result p0

    .line 1821347
    iget-object v3, v1, LX/Bnz;->b:LX/1JH;

    .line 1821348
    iget-object p1, v3, LX/1JH;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    move-object p1, p1

    .line 1821349
    if-eqz p1, :cond_3

    const/4 v3, 0x1

    .line 1821350
    :goto_1
    invoke-static {v1, v2, v4, p0, v3}, LX/Bnz;->a(LX/Bnz;Lcom/facebook/graphql/model/GraphQLComment;ZZZ)V

    .line 1821351
    if-nez p0, :cond_4

    .line 1821352
    :cond_2
    :goto_2
    goto :goto_0

    .line 1821353
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 1821354
    :cond_4
    if-eqz v3, :cond_2

    .line 1821355
    iget-object v3, v1, LX/Bnz;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;

    invoke-direct {v4, v1, p1, v0, v2}, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;-><init>(LX/Bnz;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    const p0, 0x12867249

    invoke-static {v3, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_2
.end method
