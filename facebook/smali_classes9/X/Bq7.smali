.class public LX/Bq7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/5kD;

.field private final b:Z

.field private final c:LX/Boz;

.field private final d:LX/1bf;

.field private final e:LX/D3w;

.field private f:Lcom/facebook/graphql/model/GraphQLVideo;


# direct methods
.method public constructor <init>(LX/5kD;ZLX/Boz;LX/1bf;LX/D3w;)V
    .locals 0
    .param p1    # LX/5kD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Boz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1bf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1824314
    iput-object p1, p0, LX/Bq7;->a:LX/5kD;

    .line 1824315
    iput-object p5, p0, LX/Bq7;->e:LX/D3w;

    .line 1824316
    iput-object p3, p0, LX/Bq7;->c:LX/Boz;

    .line 1824317
    iput-boolean p2, p0, LX/Bq7;->b:Z

    .line 1824318
    iput-object p4, p0, LX/Bq7;->d:LX/1bf;

    .line 1824319
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x53876aa1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1824320
    iget-boolean v1, p0, LX/Bq7;->b:Z

    if-eqz v1, :cond_0

    .line 1824321
    iget-object v1, p0, LX/Bq7;->c:LX/Boz;

    iget-object v2, p0, LX/Bq7;->a:LX/5kD;

    iget-object v3, p0, LX/Bq7;->d:LX/1bf;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-interface {v1, v2, v3, v4, v5}, LX/Boz;->a(LX/5kD;LX/1bf;ZI)V

    .line 1824322
    :goto_0
    const v1, -0x5ff059ff

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1824323
    :cond_0
    iget-object v1, p0, LX/Bq7;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    if-nez v1, :cond_1

    .line 1824324
    iget-object v1, p0, LX/Bq7;->a:LX/5kD;

    .line 1824325
    invoke-interface {v1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v2

    invoke-static {v2}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1824326
    invoke-interface {v1}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v3

    invoke-static {v3}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1824327
    invoke-static {v3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    .line 1824328
    iput-object v2, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1824329
    move-object v2, v3

    .line 1824330
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1824331
    new-instance v3, LX/2oI;

    invoke-direct {v3}, LX/2oI;-><init>()V

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    .line 1824332
    iput-object v4, v3, LX/2oI;->N:Ljava/lang/String;

    .line 1824333
    move-object v3, v3

    .line 1824334
    iput-object v2, v3, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1824335
    move-object v2, v3

    .line 1824336
    invoke-interface {v1}, LX/5kD;->ac()I

    move-result v3

    .line 1824337
    iput v3, v2, LX/2oI;->aS:I

    .line 1824338
    move-object v2, v2

    .line 1824339
    invoke-interface {v1}, LX/5kD;->S()Z

    move-result v3

    .line 1824340
    iput-boolean v3, v2, LX/2oI;->am:Z

    .line 1824341
    move-object v2, v2

    .line 1824342
    invoke-interface {v1}, LX/5kD;->ad()Ljava/lang/String;

    move-result-object v3

    .line 1824343
    iput-object v3, v2, LX/2oI;->aT:Ljava/lang/String;

    .line 1824344
    move-object v2, v2

    .line 1824345
    invoke-interface {v1}, LX/5kD;->K()Ljava/lang/String;

    move-result-object v3

    .line 1824346
    iput-object v3, v2, LX/2oI;->aU:Ljava/lang/String;

    .line 1824347
    move-object v2, v2

    .line 1824348
    invoke-interface {v1}, LX/5kD;->K()Ljava/lang/String;

    move-result-object v3

    .line 1824349
    iput-object v3, v2, LX/2oI;->aQ:Ljava/lang/String;

    .line 1824350
    move-object v2, v2

    .line 1824351
    invoke-virtual {v2}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    move-object v1, v2

    .line 1824352
    iput-object v1, p0, LX/Bq7;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1824353
    :cond_1
    iget-object v1, p0, LX/Bq7;->e:LX/D3w;

    iget-object v2, p0, LX/Bq7;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/04D;->FEED:LX/04D;

    invoke-virtual {v1, v2, v3, v4}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    goto :goto_0
.end method
