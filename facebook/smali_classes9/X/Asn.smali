.class public final LX/Asn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ass;


# direct methods
.method public constructor <init>(LX/Ass;)V
    .locals 0

    .prologue
    .line 1720868
    iput-object p1, p0, LX/Asn;->a:LX/Ass;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x73b20fda

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1720869
    sget-object v1, LX/Asr;->a:[I

    iget-object v2, p0, LX/Asn;->a:LX/Ass;

    iget-object v2, v2, LX/Ass;->A:LX/87O;

    invoke-virtual {v2}, LX/87O;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1720870
    :goto_0
    const v1, 0x35463777

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1720871
    :pswitch_0
    iget-object v1, p0, LX/Asn;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->g:LX/03V;

    sget-object v2, LX/Ass;->b:Ljava/lang/String;

    const-string v3, "Checkmark button clicked in CAPTURE mode"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 1720872
    :pswitch_1
    iget-object v1, p0, LX/Asn;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->f:LX/Ar2;

    .line 1720873
    iget-object v2, v1, LX/Ar2;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 1720874
    iget-object v3, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    .line 1720875
    sget-object v4, LX/ArH;->PREVIEW_CONFIRM:LX/ArH;

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 p1, 0x1

    invoke-static {v3, p1}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    sget-object p1, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {p1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v1

    invoke-virtual {v4, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1720876
    iget-object v3, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v4, LX/ArJ;->PREVIEW_CONFIRM:LX/ArJ;

    invoke-virtual {v3, v4}, LX/ArT;->q(LX/ArJ;)V

    .line 1720877
    iget-object v3, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    sget-object v4, LX/ArJ;->PREVIEW_CONFIRM:LX/ArJ;

    invoke-virtual {v3, v4}, LX/ArT;->e(LX/ArJ;)V

    .line 1720878
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1720879
    if-eqz v3, :cond_0

    .line 1720880
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->P(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V

    .line 1720881
    :goto_1
    iget-object v1, p0, LX/Asn;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1720882
    :cond_0
    iget-object v3, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->M:LX/03V;

    sget-object v4, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->F:Ljava/lang/String;

    const-string p1, "Next button clicked without setting composer item"

    invoke-virtual {v3, v4, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
