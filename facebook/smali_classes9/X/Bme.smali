.class public LX/Bme;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bmc;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/ui/listcomponents/EventsThemeItemSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1818750
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bme;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/ui/listcomponents/EventsThemeItemSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1818747
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1818748
    iput-object p1, p0, LX/Bme;->b:LX/0Ot;

    .line 1818749
    return-void
.end method

.method public static a(LX/0QB;)LX/Bme;
    .locals 4

    .prologue
    .line 1818736
    const-class v1, LX/Bme;

    monitor-enter v1

    .line 1818737
    :try_start_0
    sget-object v0, LX/Bme;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1818738
    sput-object v2, LX/Bme;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1818739
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1818740
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1818741
    new-instance v3, LX/Bme;

    const/16 p0, 0x1bb0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bme;-><init>(LX/0Ot;)V

    .line 1818742
    move-object v0, v3

    .line 1818743
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1818744
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1818745
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1818746
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Ljava/lang/String;Ljava/lang/String;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1818751
    const v0, -0x4b157fe5

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1818726
    check-cast p2, LX/Bmd;

    .line 1818727
    iget-object v0, p0, LX/Bme;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/listcomponents/EventsThemeItemSpec;

    iget-object v1, p2, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    iget v2, p2, LX/Bmd;->b:I

    iget v3, p2, LX/Bmd;->c:I

    const/4 v5, 0x0

    .line 1818728
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v6

    .line 1818729
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v7, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 1818730
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v8, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1818731
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 1818732
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    const p0, 0x7f0a00e8

    invoke-virtual {v4, p0}, LX/1up;->h(I)LX/1up;

    move-result-object v4

    const p0, 0x3fe3d70a    # 1.78f

    invoke-virtual {v4, p0}, LX/1up;->c(F)LX/1up;

    move-result-object v4

    const/16 p0, 0x12c

    invoke-virtual {v4, p0}, LX/1up;->k(I)LX/1up;

    move-result-object p0

    iget-object v4, v0, Lcom/facebook/events/ui/listcomponents/EventsThemeItemSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v7}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object v7, Lcom/facebook/events/ui/listcomponents/EventsThemeItemSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v7}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v7

    const p0, 0x7f0a07eb

    invoke-virtual {v9, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    const p2, 0x7f0b1e1f

    invoke-virtual {v9, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v7, p0, v9}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v7

    invoke-virtual {v4, v7}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    rem-int v4, v2, v3

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    const v4, 0x7f0b1e1d

    :goto_0
    invoke-interface {v7, v5, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x3

    const v7, 0x7f0b1e1d

    invoke-interface {v4, v5, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    .line 1818733
    const v5, -0x4b157fe5

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v7, v9

    const/4 v9, 0x1

    aput-object v8, v7, v9

    invoke-static {p1, v5, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1818734
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1818735
    return-object v0

    :cond_0
    move v4, v5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1818713
    invoke-static {}, LX/1dS;->b()V

    .line 1818714
    iget v0, p1, LX/1dQ;->b:I

    .line 1818715
    packed-switch v0, :pswitch_data_0

    .line 1818716
    :goto_0
    return-object v4

    .line 1818717
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1818718
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, Ljava/lang/String;

    .line 1818719
    iget-object p1, p0, LX/Bme;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1818720
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 1818721
    const-class p2, Landroid/app/Activity;

    invoke-static {p1, p2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    .line 1818722
    if-eqz p1, :cond_0

    .line 1818723
    const/4 p2, -0x1

    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    const/high16 v3, 0x10000

    invoke-virtual {p0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object p0

    const-string v3, "extra_selected_theme_id"

    invoke-virtual {p0, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    const-string v3, "extra_selected_theme_uri"

    invoke-virtual {p0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    invoke-virtual {p1, p2, p0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1818724
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 1818725
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4b157fe5
        :pswitch_0
    .end packed-switch
.end method
