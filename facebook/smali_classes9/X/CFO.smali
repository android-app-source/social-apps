.class public final LX/CFO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V
    .locals 0

    .prologue
    .line 1863140
    iput-object p1, p0, LX/CFO;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 14

    .prologue
    .line 1863141
    iget-object v0, p0, LX/CFO;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    if-eqz v0, :cond_2

    .line 1863142
    iget-object v0, p0, LX/CFO;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    iget-object v1, p0, LX/CFO;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v2, p0, LX/CFO;->a:Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    iget-object v2, v2, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->f:Landroid/graphics/Point;

    .line 1863143
    iget-object v3, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 1863144
    iget-object v3, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    const/4 v10, 0x0

    .line 1863145
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1863146
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1863147
    const/4 v8, 0x0

    move v9, v10

    .line 1863148
    :goto_0
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v9, v5, :cond_1

    .line 1863149
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    .line 1863150
    invoke-virtual {v5}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getPhotoRect()Landroid/graphics/Rect;

    move-result-object v11

    .line 1863151
    new-instance v12, Landroid/util/Pair;

    .line 1863152
    iget-object v13, v5, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->g:Landroid/net/Uri;

    move-object v13, v13

    .line 1863153
    invoke-direct {v12, v13, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863154
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    div-float v11, v12, v11

    .line 1863155
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863156
    if-ne v9, v3, :cond_0

    .line 1863157
    iget-object v8, v5, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->g:Landroid/net/Uri;

    move-object v8, v8

    .line 1863158
    :cond_0
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_0

    .line 1863159
    :cond_1
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    if-nez v5, :cond_3

    .line 1863160
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->c:Landroid/view/ViewStub;

    invoke-virtual {v5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iput-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    .line 1863161
    :goto_1
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->g:Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;

    iget-object v9, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    .line 1863162
    iput-object v9, v5, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    .line 1863163
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v9, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    move v9, v3

    move-object v11, v0

    move-object v12, v2

    invoke-virtual/range {v5 .. v12}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Ljava/util/List;Ljava/util/List;Landroid/net/Uri;ILandroid/view/View;LX/BXc;Landroid/graphics/Point;)V

    .line 1863164
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 1863165
    :cond_3
    iget-object v5, v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-virtual {v5, v10}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->setVisibility(I)V

    goto :goto_1
.end method
