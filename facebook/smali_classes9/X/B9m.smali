.class public final LX/B9m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1752238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752239
    iput v0, p0, LX/B9m;->a:I

    .line 1752240
    iput v0, p0, LX/B9m;->b:I

    .line 1752241
    iput v0, p0, LX/B9m;->c:I

    .line 1752242
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/B9m;->d:J

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1752235
    const-string v0, "FolderCountDebugInfo"

    .line 1752236
    new-instance v1, LX/0zA;

    invoke-direct {v1, v0}, LX/0zA;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 1752237
    const-string v1, "unread"

    iget v2, p0, LX/B9m;->a:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "unseen"

    iget v2, p0, LX/B9m;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "recent_unread"

    iget v2, p0, LX/B9m;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "updateTimestamp"

    iget-wide v2, p0, LX/B9m;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
