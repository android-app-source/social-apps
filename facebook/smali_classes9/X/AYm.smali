.class public final LX/AYm;
.super LX/3Ii;
.source ""


# instance fields
.field public final synthetic a:LX/AYo;


# direct methods
.method public constructor <init>(LX/AYo;)V
    .locals 0

    .prologue
    .line 1686082
    iput-object p1, p0, LX/AYm;->a:LX/AYo;

    invoke-direct {p0}, LX/3Ii;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    .prologue
    .line 1686083
    iget-object v0, p0, LX/AYm;->a:LX/AYo;

    const v3, 0x3f8ccccd    # 1.1f

    .line 1686084
    iget-object v2, v0, LX/AYo;->m:Landroid/animation/Animator$AnimatorListener;

    if-nez v2, :cond_0

    .line 1686085
    new-instance v2, LX/AYn;

    invoke-direct {v2, v0}, LX/AYn;-><init>(LX/AYo;)V

    iput-object v2, v0, LX/AYo;->m:Landroid/animation/Animator$AnimatorListener;

    .line 1686086
    :cond_0
    iget-object v2, v0, LX/AYo;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, LX/AYo;->m:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686087
    iget-object v0, p0, LX/AYm;->a:LX/AYo;

    iget-object v0, v0, LX/AYo;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setClickable(Z)V

    .line 1686088
    return-void
.end method
