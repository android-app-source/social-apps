.class public final LX/Bxk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bxo;

.field public final synthetic b:LX/1aZ;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;LX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1835962
    iput-object p1, p0, LX/Bxk;->d:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    iput-object p2, p0, LX/Bxk;->a:LX/Bxo;

    iput-object p3, p0, LX/Bxk;->b:LX/1aZ;

    iput-object p4, p0, LX/Bxk;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3fabafa2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1835963
    check-cast p1, Lcom/facebook/drawee/view/DraweeView;

    .line 1835964
    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/ByP;

    move-object v0, v0

    .line 1835965
    iget-object v2, v0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-interface {v2}, LX/1aZ;->f()Landroid/graphics/drawable/Animatable;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v2, v2

    .line 1835966
    sget-object v3, LX/Bxm;->a:[I

    iget-object v4, p0, LX/Bxk;->a:LX/Bxo;

    iget-object v4, v4, LX/Bxo;->g:LX/Bxn;

    invoke-virtual {v4}, LX/Bxn;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1835967
    :cond_0
    :goto_0
    const v0, 0x7e3ba37

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1835968
    :pswitch_0
    sget-object v2, LX/6Wv;->LOADING:LX/6Wv;

    invoke-virtual {v0, v2}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1835969
    iget-object v2, p0, LX/Bxk;->b:LX/1aZ;

    invoke-virtual {v0, v2}, LX/ByP;->setImageController(LX/1aZ;)V

    .line 1835970
    iget-object v0, p0, LX/Bxk;->d:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    iget-object v2, p0, LX/Bxk;->a:LX/Bxo;

    iget-object v3, p0, LX/Bxk;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v2, v3}, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->a$redex0(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1835971
    iget-object v0, p0, LX/Bxk;->a:LX/Bxo;

    sget-object v2, LX/Bxn;->PAUSE_ON_CLICK:LX/Bxn;

    iput-object v2, v0, LX/Bxo;->g:LX/Bxn;

    goto :goto_0

    .line 1835972
    :pswitch_1
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1835973
    sget-object v3, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v0, v3}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1835974
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 1835975
    iget-object v0, p0, LX/Bxk;->a:LX/Bxo;

    sget-object v2, LX/Bxn;->RESUME_ON_CLICK:LX/Bxn;

    iput-object v2, v0, LX/Bxo;->g:LX/Bxn;

    goto :goto_0

    .line 1835976
    :pswitch_2
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1835977
    sget-object v3, LX/6Wv;->HIDDEN:LX/6Wv;

    invoke-virtual {v0, v3}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1835978
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1835979
    iget-object v0, p0, LX/Bxk;->a:LX/Bxo;

    sget-object v2, LX/Bxn;->PAUSE_ON_CLICK:LX/Bxn;

    iput-object v2, v0, LX/Bxo;->g:LX/Bxn;

    .line 1835980
    iget-object v0, p0, LX/Bxk;->d:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    iget-object v2, p0, LX/Bxk;->a:LX/Bxo;

    iget-object v3, p0, LX/Bxk;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v2, v3}, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->a$redex0(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
