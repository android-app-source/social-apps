.class public LX/B6T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1746749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746750
    iput-object p1, p0, LX/B6T;->a:Landroid/widget/TextView;

    .line 1746751
    iput-object p2, p0, LX/B6T;->b:Landroid/widget/TextView;

    .line 1746752
    iput-object p3, p0, LX/B6T;->c:Landroid/widget/TextView;

    .line 1746753
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1746754
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746755
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746756
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1746757
    iget-object v0, p0, LX/B6T;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746758
    return-void
.end method

.method public final a(LX/B7F;LX/B77;LX/B7N;)V
    .locals 2

    .prologue
    .line 1746759
    iget-object v0, p3, LX/B7N;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1746760
    invoke-virtual {p2}, LX/B77;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1746761
    invoke-virtual {p1, p2}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v0

    .line 1746762
    :cond_0
    iget-object v1, p0, LX/B6T;->a:Landroid/widget/TextView;

    invoke-static {v1, v0}, LX/B6T;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 1746763
    return-void
.end method

.method public final a(LX/B7N;Landroid/text/SpannableString;)V
    .locals 3

    .prologue
    .line 1746764
    const/16 v1, 0x8

    .line 1746765
    iget-object v0, p0, LX/B6T;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746766
    iget-object v0, p0, LX/B6T;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746767
    iget-object v0, p1, LX/B7N;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1746768
    iget-object v1, p1, LX/B7N;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1746769
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 1746770
    iget-object v2, p0, LX/B6T;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746771
    iget-object v0, p0, LX/B6T;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1746772
    iget-object v0, p0, LX/B6T;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1746773
    iget-object v0, p0, LX/B6T;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746774
    iget-object v0, p0, LX/B6T;->c:Landroid/widget/TextView;

    iget-object v2, p0, LX/B6T;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 1746775
    iget-object v0, p0, LX/B6T;->c:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B6T;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 1746776
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1746777
    iget-object v0, p0, LX/B6T;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746778
    return-void
.end method
