.class public LX/CBZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CBc;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CBZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CBc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856538
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1856539
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/CBZ;->b:LX/0Zi;

    .line 1856540
    iput-object p1, p0, LX/CBZ;->a:LX/0Ot;

    .line 1856541
    return-void
.end method

.method public static a(LX/0QB;)LX/CBZ;
    .locals 4

    .prologue
    .line 1856542
    const-class v1, LX/CBZ;

    monitor-enter v1

    .line 1856543
    :try_start_0
    sget-object v0, LX/CBZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856544
    sput-object v2, LX/CBZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856547
    new-instance v3, LX/CBZ;

    const/16 p0, 0x213c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CBZ;-><init>(LX/0Ot;)V

    .line 1856548
    move-object v0, v3

    .line 1856549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1856553
    check-cast p2, LX/CBY;

    .line 1856554
    iget-object v0, p0, LX/CBZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CBc;

    iget-object v2, p2, LX/CBY;->a:LX/1Pn;

    iget v3, p2, LX/CBY;->b:I

    iget v4, p2, LX/CBY;->c:I

    iget-object v5, p2, LX/CBY;->d:Landroid/view/View$OnClickListener;

    iget-object v6, p2, LX/CBY;->e:LX/0Px;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/CBc;->a(LX/1De;LX/1Pn;IILandroid/view/View$OnClickListener;LX/0Px;)LX/1Dg;

    move-result-object v0

    .line 1856555
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1856556
    invoke-static {}, LX/1dS;->b()V

    .line 1856557
    const/4 v0, 0x0

    return-object v0
.end method
