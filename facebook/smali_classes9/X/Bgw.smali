.class public final LX/Bgw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/BgK;

.field public final synthetic b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

.field public final synthetic c:LX/Bgz;


# direct methods
.method public constructor <init>(LX/Bgz;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;)V
    .locals 0

    .prologue
    .line 1808622
    iput-object p1, p0, LX/Bgw;->c:LX/Bgz;

    iput-object p2, p0, LX/Bgw;->a:LX/BgK;

    iput-object p3, p0, LX/Bgw;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1808623
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1808624
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1808625
    iget-object v0, p0, LX/Bgw;->a:LX/BgK;

    invoke-virtual {v0}, LX/BgK;->a()V

    .line 1808626
    iget-object v0, p0, LX/Bgw;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808627
    return-void
.end method
