.class public final LX/BA9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Avi;


# instance fields
.field public final synthetic a:LX/BAC;


# direct methods
.method public constructor <init>(LX/BAC;)V
    .locals 0

    .prologue
    .line 1752731
    iput-object p1, p0, LX/BA9;->a:LX/BAC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1752732
    iget-object v1, p0, LX/BA9;->a:LX/BAC;

    iget-object v1, v1, LX/BAC;->m:LX/61H;

    if-eqz v1, :cond_2

    .line 1752733
    iget-object v1, p0, LX/BA9;->a:LX/BAC;

    iget-boolean v1, v1, LX/BAC;->j:Z

    if-ne v1, v0, :cond_0

    if-lez p1, :cond_1

    :cond_0
    iget-object v1, p0, LX/BA9;->a:LX/BAC;

    iget-boolean v1, v1, LX/BAC;->j:Z

    if-nez v1, :cond_2

    if-lez p1, :cond_2

    .line 1752734
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1752735
    const-string v2, "filter_event"

    const-string v3, "face_detected_changed"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1752736
    const-string v2, "extras"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "face_detected:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/BA9;->a:LX/BAC;

    iget-boolean v4, v4, LX/BAC;->j:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1752737
    iget-object v2, p0, LX/BA9;->a:LX/BAC;

    iget-object v2, v2, LX/BAC;->m:LX/61H;

    invoke-interface {v2, v1}, LX/61H;->a(Ljava/util/Map;)V

    .line 1752738
    :cond_2
    iget-object v1, p0, LX/BA9;->a:LX/BAC;

    if-lez p1, :cond_3

    .line 1752739
    :goto_0
    iput-boolean v0, v1, LX/BAC;->j:Z

    .line 1752740
    return-void

    .line 1752741
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
