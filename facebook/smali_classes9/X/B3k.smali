.class public final LX/B3k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0L;


# instance fields
.field public final synthetic a:LX/B3l;


# direct methods
.method public constructor <init>(LX/B3l;)V
    .locals 0

    .prologue
    .line 1740857
    iput-object p1, p0, LX/B3k;->a:LX/B3l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2nf;)V
    .locals 2
    .param p1    # LX/2nf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1740830
    iget-object v0, p0, LX/B3k;->a:LX/B3l;

    iget-object v0, v0, LX/B3l;->c:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    .line 1740831
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    if-nez v1, :cond_1

    .line 1740832
    if-eqz p1, :cond_0

    .line 1740833
    invoke-interface {p1}, LX/2nf;->close()V

    .line 1740834
    :cond_0
    :goto_0
    return-void

    .line 1740835
    :cond_1
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    .line 1740836
    iget-object p0, v1, LX/B3e;->g:LX/B3j;

    .line 1740837
    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    if-eqz v0, :cond_2

    .line 1740838
    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 1740839
    :cond_2
    iput-object p1, p0, LX/B3j;->b:LX/2nf;

    .line 1740840
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1740841
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1740855
    sget-object v0, LX/B3l;->a:Ljava/lang/Class;

    const-string v1, "network error"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1740856
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1740842
    iget-object v0, p0, LX/B3k;->a:LX/B3l;

    iget-object v0, v0, LX/B3l;->c:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    const/4 p0, 0x0

    .line 1740843
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->j:LX/0zw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez v1, :cond_1

    .line 1740844
    :cond_0
    :goto_0
    return-void

    .line 1740845
    :cond_1
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-nez v1, :cond_3

    .line 1740846
    if-eqz p1, :cond_2

    .line 1740847
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->c()V

    .line 1740848
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1740849
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0

    .line 1740850
    :cond_2
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1740851
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1740852
    :cond_3
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->c()V

    .line 1740853
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1740854
    iget-object v1, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method
