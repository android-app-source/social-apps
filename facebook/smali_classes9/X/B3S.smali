.class public LX/B3S;
.super LX/8Hb;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/0wM;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/356;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/5kD;


# direct methods
.method public constructor <init>(LX/0wM;LX/0Or;LX/356;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wM;",
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;",
            "LX/356;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1740344
    invoke-direct {p0}, LX/8Hb;-><init>()V

    .line 1740345
    iput-object p1, p0, LX/B3S;->a:LX/0wM;

    .line 1740346
    iput-object p2, p0, LX/B3S;->b:LX/0Or;

    .line 1740347
    iput-object p3, p0, LX/B3S;->c:LX/356;

    .line 1740348
    iput-object p4, p0, LX/B3S;->d:LX/0Or;

    .line 1740349
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 1740340
    iget-object v0, p0, LX/B3S;->e:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B3S;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->af()LX/5QV;

    move-result-object v0

    invoke-static {v0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->BIRTHDAY_WISHES:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 1740341
    iget-object v0, p0, LX/B3S;->e:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B3S;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1740342
    :cond_0
    const/4 v0, 0x0

    .line 1740343
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/B3S;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5kD;Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)Z
    .locals 3

    .prologue
    .line 1740330
    iput-object p1, p0, LX/B3S;->e:LX/5kD;

    .line 1740331
    invoke-direct {p0}, LX/B3S;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1740332
    iget-object v1, p0, LX/B3S;->c:LX/356;

    invoke-virtual {v1}, LX/356;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, LX/B3S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1740333
    :cond_0
    const/4 v0, 0x0

    .line 1740334
    :goto_0
    return v0

    .line 1740335
    :cond_1
    iget-object v0, p0, LX/B3S;->a:LX/0wM;

    const v1, 0x7f0207b5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1740336
    const v0, 0x7f082747

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1740337
    invoke-virtual {p2, p0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1740338
    iget-object v0, p0, LX/B3S;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1740339
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x29fe21a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1740318
    invoke-direct {p0}, LX/B3S;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1740319
    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, LX/B3S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1740320
    :cond_0
    const v0, 0x4642d1d9

    invoke-static {v3, v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1740321
    :goto_0
    return-void

    .line 1740322
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1740323
    if-eqz v0, :cond_2

    .line 1740324
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1740325
    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1740326
    new-instance v4, LX/B4K;

    iget-object v5, p0, LX/B3S;->e:LX/5kD;

    invoke-interface {v5}, LX/5kD;->af()LX/5QV;

    move-result-object v5

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "snowlift"

    invoke-direct {v4, v5, v6, v7}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v1

    check-cast v1, LX/B4K;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/B4J;->a(I)LX/B4J;

    move-result-object v1

    check-cast v1, LX/B4K;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, LX/B4J;->a(J)LX/B4J;

    move-result-object v1

    check-cast v1, LX/B4K;

    invoke-virtual {v1}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v3

    .line 1740327
    iget-object v1, p0, LX/B3S;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B35;

    .line 1740328
    const/16 v4, 0x20b3

    invoke-virtual {v1, v0, v4, v3}, LX/B35;->a(Landroid/app/Activity;ILcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    .line 1740329
    :cond_2
    const v0, 0x73fdaa02

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
