.class public final LX/Abp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:LX/Aby;


# direct methods
.method public constructor <init>(LX/Aby;)V
    .locals 0

    .prologue
    .line 1691472
    iput-object p1, p0, LX/Abp;->a:LX/Aby;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1691473
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->o:LX/3Gs;

    if-eqz v0, :cond_0

    .line 1691474
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->C:I

    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget v3, v3, LX/Aby;->z:I

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 1691475
    :goto_0
    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget v3, v3, LX/Aby;->z:I

    if-ne p1, v3, :cond_5

    move v3, v1

    .line 1691476
    :goto_1
    if-nez v0, :cond_6

    if-eqz v3, :cond_6

    .line 1691477
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->o:LX/3Gs;

    invoke-interface {v0}, LX/3Gs;->il_()V

    .line 1691478
    :cond_0
    :goto_2
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v3, v0, LX/Aby;->c:LX/AgK;

    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->z:I

    if-ne p1, v0, :cond_7

    move v0, v1

    .line 1691479
    :goto_3
    iget-object v4, v3, LX/AgK;->a:Ljava/util/Set;

    iget-object v5, v3, LX/AgK;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [LX/AgJ;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [LX/AgJ;

    .line 1691480
    array-length v6, v4

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v6, :cond_1

    aget-object v7, v4, v5

    .line 1691481
    invoke-interface {v7, v0}, LX/AgJ;->a(Z)V

    .line 1691482
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1691483
    :cond_1
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->I:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->C:I

    if-eq v0, p1, :cond_3

    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->w:Z

    if-nez v0, :cond_3

    .line 1691484
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691485
    iget-object v3, v0, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691486
    move-object v0, v3

    .line 1691487
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    if-eqz v0, :cond_2

    .line 1691488
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691489
    iget-object v3, v0, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691490
    move-object v0, v3

    .line 1691491
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v3, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->z:I

    if-ne p1, v0, :cond_8

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->setIsQuietModeSelected(Z)V

    .line 1691492
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget-object v4, p0, LX/Abp;->a:LX/Aby;

    iget v4, v4, LX/Aby;->C:I

    invoke-static {v3, v4}, LX/Aby;->d(LX/Aby;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_to_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    invoke-static {v3, p1}, LX/Aby;->d(LX/Aby;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1691493
    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget-boolean v3, v3, LX/Aby;->F:Z

    if-eqz v3, :cond_9

    .line 1691494
    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget-object v3, v3, LX/Aby;->d:LX/AVU;

    .line 1691495
    const-string v4, "facecast_view_pager_user_swipe"

    invoke-static {v3, v4, v0}, LX/AVU;->a(LX/AVU;Ljava/lang/String;Ljava/lang/String;)V

    .line 1691496
    :goto_6
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691497
    iget-object v3, v0, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691498
    move-object v0, v3

    .line 1691499
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v0, :cond_3

    .line 1691500
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691501
    iget-object v3, v0, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691502
    move-object v0, v3

    .line 1691503
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget v3, v3, LX/Aby;->B:I

    if-ne p1, v3, :cond_a

    :goto_7
    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setIsShowing(Z)V

    .line 1691504
    :cond_3
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691505
    iput-boolean v2, v0, LX/Aby;->I:Z

    .line 1691506
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691507
    iput p1, v0, LX/Aby;->C:I

    .line 1691508
    return-void

    :cond_4
    move v0, v2

    .line 1691509
    goto/16 :goto_0

    :cond_5
    move v3, v2

    .line 1691510
    goto/16 :goto_1

    .line 1691511
    :cond_6
    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    .line 1691512
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->o:LX/3Gs;

    invoke-interface {v0}, LX/3Gs;->c()V

    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 1691513
    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 1691514
    goto :goto_5

    .line 1691515
    :cond_9
    iget-object v3, p0, LX/Abp;->a:LX/Aby;

    iget-object v3, v3, LX/Aby;->d:LX/AVU;

    .line 1691516
    const-string v4, "facecast_view_pager_auto_swipe"

    invoke-static {v3, v4, v0}, LX/AVU;->a(LX/AVU;Ljava/lang/String;Ljava/lang/String;)V

    .line 1691517
    goto :goto_6

    :cond_a
    move v1, v2

    .line 1691518
    goto :goto_7
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 1691519
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->z:I

    if-ne p1, v0, :cond_1

    .line 1691520
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    const/4 v1, 0x0

    .line 1691521
    iput-boolean v1, v0, LX/Aby;->R:Z

    .line 1691522
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    invoke-static {v0, p2}, LX/Aby;->b(LX/Aby;F)V

    .line 1691523
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->c:LX/AgK;

    invoke-virtual {v0, p2}, LX/AgK;->a(F)V

    .line 1691524
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->n:LX/Add;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Add;

    iput p2, v0, LX/Add;->a:F

    .line 1691525
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->M:Z

    if-nez v0, :cond_0

    .line 1691526
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Abp;->a:LX/Aby;

    iget-object v1, v1, LX/Aby;->n:LX/Add;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1691527
    :cond_0
    :goto_0
    return-void

    .line 1691528
    :cond_1
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->A:I

    if-ne p1, v0, :cond_0

    .line 1691529
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->c:LX/AgK;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p2

    invoke-virtual {v0, v1}, LX/AgK;->a(F)V

    .line 1691530
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->q:LX/Ac1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->N:Z

    if-nez v0, :cond_0

    .line 1691531
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->q:LX/Ac1;

    invoke-virtual {v0, p2}, LX/Ac1;->a(F)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1691532
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->E:I

    if-ne v0, v1, :cond_1

    if-ne p1, v2, :cond_1

    .line 1691533
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691534
    iput-boolean v1, v0, LX/Aby;->F:Z

    .line 1691535
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    .line 1691536
    iput p1, v0, LX/Aby;->E:I

    .line 1691537
    return-void

    .line 1691538
    :cond_1
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    iget v0, v0, LX/Aby;->E:I

    if-ne v0, v2, :cond_0

    if-nez p1, :cond_0

    .line 1691539
    iget-object v0, p0, LX/Abp;->a:LX/Aby;

    const/4 v1, 0x0

    .line 1691540
    iput-boolean v1, v0, LX/Aby;->F:Z

    .line 1691541
    goto :goto_0
.end method
