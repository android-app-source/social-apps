.class public LX/BFM;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Cv;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0iA;

.field private b:Lcom/facebook/graphql/model/GraphQLStory;

.field private c:LX/3kV;


# direct methods
.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765636
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1765637
    iput-object p1, p0, LX/BFM;->a:LX/0iA;

    .line 1765638
    return-void
.end method

.method public static a(LX/0QB;)LX/BFM;
    .locals 2

    .prologue
    .line 1765639
    new-instance v1, LX/BFM;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-direct {v1, v0}, LX/BFM;-><init>(LX/0iA;)V

    .line 1765640
    move-object v0, v1

    .line 1765641
    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1765629
    invoke-static {}, LX/BFL;->values()[LX/BFL;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1765630
    sget-object v1, LX/BFK;->a:[I

    invoke-virtual {v0}, LX/BFL;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1765631
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1765632
    :pswitch_0
    iget-object v0, p0, LX/BFM;->c:LX/3kV;

    if-eqz v0, :cond_0

    .line 1765633
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1765634
    new-instance v1, LX/8SX;

    invoke-direct {v1, v0}, LX/8SX;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 1765635
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1765623
    invoke-static {}, LX/BFL;->values()[LX/BFL;

    move-result-object v0

    aget-object v0, v0, p4

    .line 1765624
    sget-object v1, LX/BFK;->a:[I

    invoke-virtual {v0}, LX/BFL;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1765625
    :cond_0
    :goto_0
    return-void

    .line 1765626
    :pswitch_0
    iget-object v0, p0, LX/BFM;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1765627
    check-cast p3, LX/8SX;

    .line 1765628
    iget-object v0, p0, LX/BFM;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v0

    iget-object v1, p0, LX/BFM;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, LX/8SX;->a(Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 1765615
    invoke-virtual {p0}, LX/BFM;->getCount()I

    move-result v1

    .line 1765616
    iput-object p1, p0, LX/BFM;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765617
    iget-object v0, p0, LX/BFM;->a:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERMALINK_STORY_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/3kV;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kV;

    iput-object v0, p0, LX/BFM;->c:LX/3kV;

    .line 1765618
    iget-object v0, p0, LX/BFM;->c:LX/3kV;

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/3kW;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1765619
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/BFM;->c:LX/3kV;

    .line 1765620
    :cond_1
    invoke-virtual {p0}, LX/BFM;->getCount()I

    move-result v0

    if-eq v1, v0, :cond_2

    .line 1765621
    const v0, -0x7ed23c63

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1765622
    :cond_2
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1765642
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/BFM;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1765614
    iget-object v0, p0, LX/BFM;->c:LX/3kV;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1765613
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1765612
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1765608
    iget-object v0, p0, LX/BFM;->c:LX/3kV;

    if-eqz v0, :cond_0

    .line 1765609
    sget-object v0, LX/BFL;->PRIVACY_EDUCATION:LX/BFL;

    invoke-virtual {v0}, LX/BFL;->ordinal()I

    move-result v0

    .line 1765610
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/BFL;->UNKNOWN:LX/BFL;

    invoke-virtual {v0}, LX/BFL;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1765611
    invoke-static {}, LX/BFL;->values()[LX/BFL;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
