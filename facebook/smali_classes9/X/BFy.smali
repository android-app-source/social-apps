.class public final LX/BFy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 0

    .prologue
    .line 1766473
    iput-object p1, p0, LX/BFy;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1766471
    sget-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v1, "failed to switch camera"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766472
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1766460
    check-cast p1, LX/5fM;

    .line 1766461
    iget-object v0, p0, LX/BFy;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    .line 1766462
    iput-object p1, v0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 1766463
    iget-object v0, p0, LX/BFy;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    .line 1766464
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->r:LX/9c7;

    iget-object v2, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->P:LX/89Z;

    invoke-virtual {p1}, LX/5fM;->name()Ljava/lang/String;

    move-result-object p0

    .line 1766465
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/9c4;->CREATIVECAM_SWITCH_CAMERA_FACING:LX/9c4;

    invoke-virtual {p1}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "composer"

    .line 1766466
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1766467
    move-object v0, v0

    .line 1766468
    sget-object p1, LX/9c5;->CAMERA_FACING:LX/9c5;

    invoke-virtual {p1}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object p1, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {p1}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1766469
    invoke-static {v1, v0}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1766470
    return-void
.end method
