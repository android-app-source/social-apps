.class public final LX/CaS;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

.field private b:I

.field private c:I

.field private final d:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 1

    .prologue
    .line 1917622
    iput-object p1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    .line 1917623
    const/4 v0, -0x1

    iput v0, p0, LX/CaS;->b:I

    .line 1917624
    const/4 v0, 0x0

    iput v0, p0, LX/CaS;->c:I

    .line 1917625
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/CaS;->d:Landroid/view/animation/Interpolator;

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v6, -0x1

    .line 1917626
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0, p1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v2

    .line 1917627
    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    .line 1917628
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917629
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    invoke-virtual {v0, v3}, LX/CcX;->c(Ljava/lang/String;)V

    .line 1917630
    :cond_0
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1917631
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    invoke-virtual {v0, v3}, LX/CcS;->c(Ljava/lang/String;)V

    .line 1917632
    :cond_1
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v0

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    invoke-virtual {v0, v1, v2, v4}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/base/fragment/FbFragment;LX/5kD;LX/21D;)V

    .line 1917633
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1917634
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ae:Z

    invoke-virtual {v0, v3, v1}, LX/CcX;->a(Ljava/lang/String;Z)V

    .line 1917635
    :cond_2
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1917636
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    invoke-virtual {v0, v3}, LX/CcS;->d(Ljava/lang/String;)V

    .line 1917637
    :cond_3
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0, v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;LX/5kD;)V

    .line 1917638
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    invoke-virtual {v0}, LX/9g2;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-le p1, v0, :cond_4

    .line 1917639
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->Q:LX/9g2;

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    invoke-virtual {v0, v1}, LX/9g2;->a(I)V

    .line 1917640
    :cond_4
    iget v0, p0, LX/CaS;->b:I

    if-eq v0, v6, :cond_c

    .line 1917641
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    .line 1917642
    iget v1, v0, Landroid/support/v4/view/ViewPager;->B:I

    move v0, v1

    .line 1917643
    if-eq v0, v5, :cond_5

    .line 1917644
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1917645
    :cond_5
    iget v0, p0, LX/CaS;->b:I

    if-ge v0, p1, :cond_b

    const-string v0, "swipe_to_next_photo"

    .line 1917646
    :goto_0
    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object v4, LX/0ig;->x:LX/0ih;

    invoke-virtual {v1, v4, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1917647
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->g:LX/23g;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    .line 1917648
    iget-object v4, v0, LX/23g;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x14000b

    invoke-static {v1}, LX/23g;->a(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v4, v5, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1917649
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "swipe"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1917650
    :goto_1
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->h:LX/745;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget v1, p0, LX/CaS;->b:I

    if-ne v1, v6, :cond_e

    sget-object v1, LX/74P;->CLICK:LX/74P;

    .line 1917651
    :goto_3
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917652
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917653
    invoke-static {v4}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v7

    .line 1917654
    const-string v8, "content_id"

    invoke-virtual {v7, v8, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917655
    const-string v8, "action"

    iget-object v9, v1, LX/74P;->value:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917656
    if-eqz v0, :cond_6

    .line 1917657
    const-string v8, "owner_id"

    invoke-virtual {v7, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917658
    :cond_6
    sget-object v8, LX/74R;->ANDROID_PHOTOS_CONSUMPTION:LX/74R;

    invoke-static {v4, v8, v7, v5}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1917659
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1917660
    if-eqz v3, :cond_7

    .line 1917661
    const-string v0, "photo_id"

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917662
    :cond_7
    invoke-interface {v2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1917663
    invoke-interface {v2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1917664
    if-eqz v0, :cond_8

    .line 1917665
    const-string v0, "author_id"

    invoke-interface {v2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917666
    :cond_8
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    iget-object v4, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v0, v4, v1}, LX/0gh;->a(LX/0f2;Ljava/util/Map;)V

    .line 1917667
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->e:LX/8Hh;

    if-eqz v0, :cond_9

    .line 1917668
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->e:LX/8Hh;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    invoke-interface {v0, p1}, LX/8Hh;->a(I)V

    .line 1917669
    :cond_9
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->u:LX/9hM;

    if-eqz v0, :cond_a

    iget v0, p0, LX/CaS;->b:I

    if-eq v0, v6, :cond_a

    .line 1917670
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->u:LX/9hM;

    invoke-interface {v0, v3}, LX/9hM;->a(Ljava/lang/String;)V

    .line 1917671
    :cond_a
    iput p1, p0, LX/CaS;->b:I

    .line 1917672
    return-void

    .line 1917673
    :cond_b
    const-string v0, "swipe_to_previous_photo"

    goto/16 :goto_0

    .line 1917674
    :cond_c
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_photo"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto/16 :goto_1

    .line 1917675
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_e
    sget-object v1, LX/74P;->SWIPE:LX/74P;

    goto/16 :goto_3
.end method

.method public final a(IFI)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v0, 0x0

    .line 1917676
    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 1917677
    if-ne p1, v1, :cond_0

    .line 1917678
    sub-float p2, v3, p2

    .line 1917679
    :cond_0
    cmpl-float v1, p2, v0

    if-lez v1, :cond_2

    cmpg-float v1, p2, v3

    if-gez v1, :cond_2

    .line 1917680
    cmpl-float v1, p2, v2

    if-lez v1, :cond_1

    iget-object v0, p0, LX/CaS;->d:Landroid/view/animation/Interpolator;

    sub-float v1, p2, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 1917681
    :cond_1
    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVideoControlAlpha(F)V

    .line 1917682
    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1917683
    iget-object v0, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v0

    .line 1917684
    iget v1, p0, LX/CaS;->c:I

    if-nez v1, :cond_2

    if-ne p1, v3, :cond_2

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1917685
    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c(LX/5kD;)Ljava/lang/String;

    .line 1917686
    iget-object v4, v1, LX/CcX;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v4}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Caf;

    .line 1917687
    if-eqz v4, :cond_0

    .line 1917688
    iget-object v1, v4, LX/Caf;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1917689
    iget-object v1, v4, LX/Caf;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1917690
    :cond_1
    goto :goto_0

    .line 1917691
    :cond_2
    iget v1, p0, LX/CaS;->c:I

    if-nez v1, :cond_5

    if-ne p1, v3, :cond_5

    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1917692
    iget-object v1, p0, LX/CaS;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c(LX/5kD;)Ljava/lang/String;

    move-result-object v0

    .line 1917693
    iget-object v2, v1, LX/CcS;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Caa;

    .line 1917694
    if-eqz v2, :cond_3

    .line 1917695
    iget-object v1, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    if-eqz v1, :cond_4

    iget-object v1, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-static {v1, v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    if-eqz v1, :cond_4

    .line 1917696
    iget-object v1, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v1}, LX/CaV;->u()V

    .line 1917697
    :cond_4
    goto :goto_1

    .line 1917698
    :cond_5
    iput p1, p0, LX/CaS;->c:I

    .line 1917699
    return-void
.end method
