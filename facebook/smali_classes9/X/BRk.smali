.class public LX/BRk;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/text/SimpleDateFormat;


# instance fields
.field public final b:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1784385
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/BRk;->a:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784387
    iput-object p1, p0, LX/BRk;->b:Landroid/content/res/Resources;

    .line 1784388
    return-void
.end method

.method public static a(Landroid/widget/NumberPicker;I[Ljava/lang/String;Landroid/widget/NumberPicker$OnValueChangeListener;I)V
    .locals 1

    .prologue
    .line 1784389
    invoke-virtual {p0, p1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 1784390
    array-length v0, p2

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 1784391
    invoke-virtual {p0, p2}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    .line 1784392
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setDescendantFocusability(I)V

    .line 1784393
    invoke-virtual {p0, p3}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 1784394
    invoke-virtual {p0, p4}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 1784395
    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1784396
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1784397
    const/4 v7, 0x5

    .line 1784398
    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1784399
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v4

    .line 1784400
    const/4 v3, 0x0

    :goto_0
    const/4 v5, 0x3

    if-ge v3, v5, :cond_0

    .line 1784401
    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 1784402
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v5

    add-int/2addr v4, v5

    .line 1784403
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1784404
    :cond_0
    move v3, v4

    .line 1784405
    new-array v4, v3, [Ljava/lang/String;

    .line 1784406
    const/4 v0, 0x0

    const-string v5, "Today"

    aput-object v5, v4, v0

    move v0, v1

    .line 1784407
    :goto_1
    if-ge v0, v3, :cond_1

    .line 1784408
    const/4 v5, 0x6

    invoke-virtual {v2, v5, v1}, Ljava/util/Calendar;->add(II)V

    .line 1784409
    sget-object v5, LX/BRk;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1784410
    aput-object v5, v4, v0

    .line 1784411
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1784412
    :cond_1
    return-object v4
.end method
