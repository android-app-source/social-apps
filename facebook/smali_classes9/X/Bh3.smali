.class public final LX/Bh3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgk;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

.field public final synthetic b:LX/97f;

.field public final synthetic c:LX/BgS;

.field public final synthetic d:LX/Bh5;


# direct methods
.method public constructor <init>(LX/Bh5;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)V
    .locals 0

    .prologue
    .line 1808702
    iput-object p1, p0, LX/Bh3;->d:LX/Bh5;

    iput-object p2, p0, LX/Bh3;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    iput-object p3, p0, LX/Bh3;->b:LX/97f;

    iput-object p4, p0, LX/Bh3;->c:LX/BgS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZI)V
    .locals 3

    .prologue
    .line 1808703
    if-nez p2, :cond_2

    .line 1808704
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bh3;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getValues()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1808705
    iget-object v0, p0, LX/Bh3;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    invoke-virtual {v0, p3}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->removeViewAt(I)V

    .line 1808706
    :cond_0
    iget-object v0, p0, LX/Bh3;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    iget-object v1, p0, LX/Bh3;->b:LX/97f;

    iget-object v2, p0, LX/Bh3;->c:LX/BgS;

    invoke-static {v0, v1, v2}, LX/Bh5;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)LX/97f;

    .line 1808707
    :cond_1
    :goto_0
    return-void

    .line 1808708
    :cond_2
    iget-object v0, p0, LX/Bh3;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    invoke-virtual {v0, p3}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1808709
    instance-of v1, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    if-eqz v1, :cond_1

    .line 1808710
    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808711
    iget-object v1, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    move-object v0, v1

    .line 1808712
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method
