.class public final enum LX/Bm0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bm0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bm0;

.field public static final enum EARLIER:LX/Bm0;

.field public static final enum TODAY:LX/Bm0;

.field public static final enum YESTERDAY:LX/Bm0;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1817024
    new-instance v0, LX/Bm0;

    const-string v1, "TODAY"

    invoke-direct {v0, v1, v2}, LX/Bm0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bm0;->TODAY:LX/Bm0;

    .line 1817025
    new-instance v0, LX/Bm0;

    const-string v1, "YESTERDAY"

    invoke-direct {v0, v1, v3}, LX/Bm0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bm0;->YESTERDAY:LX/Bm0;

    .line 1817026
    new-instance v0, LX/Bm0;

    const-string v1, "EARLIER"

    invoke-direct {v0, v1, v4}, LX/Bm0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bm0;->EARLIER:LX/Bm0;

    .line 1817027
    const/4 v0, 0x3

    new-array v0, v0, [LX/Bm0;

    sget-object v1, LX/Bm0;->TODAY:LX/Bm0;

    aput-object v1, v0, v2

    sget-object v1, LX/Bm0;->YESTERDAY:LX/Bm0;

    aput-object v1, v0, v3

    sget-object v1, LX/Bm0;->EARLIER:LX/Bm0;

    aput-object v1, v0, v4

    sput-object v0, LX/Bm0;->$VALUES:[LX/Bm0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1817028
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bm0;
    .locals 1

    .prologue
    .line 1817029
    const-class v0, LX/Bm0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bm0;

    return-object v0
.end method

.method public static values()[LX/Bm0;
    .locals 1

    .prologue
    .line 1817030
    sget-object v0, LX/Bm0;->$VALUES:[LX/Bm0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bm0;

    return-object v0
.end method
