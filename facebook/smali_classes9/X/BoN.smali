.class public final LX/BoN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/BoF;


# direct methods
.method public constructor <init>(LX/BoF;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1821695
    iput-object p1, p0, LX/BoN;->d:LX/BoF;

    iput-object p2, p0, LX/BoN;->a:Lcom/facebook/graphql/model/FeedUnit;

    iput-object p3, p0, LX/BoN;->b:Landroid/view/View;

    iput-object p4, p0, LX/BoN;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 1821696
    iget-object v0, p0, LX/BoN;->d:LX/BoF;

    iget-object v0, v0, LX/BoF;->c:LX/1dt;

    iget-object v1, p0, LX/BoN;->a:Lcom/facebook/graphql/model/FeedUnit;

    iget-object v2, p0, LX/BoN;->b:Landroid/view/View;

    .line 1821697
    invoke-virtual {v0, v1, v2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    .line 1821698
    iget-object v0, p0, LX/BoN;->d:LX/BoF;

    iget-object v1, p0, LX/BoN;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/BoN;->b:Landroid/view/View;

    .line 1821699
    iget-object v3, v0, LX/BoF;->c:LX/1dt;

    .line 1821700
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1821701
    check-cast v4, Lcom/facebook/graphql/model/HideableUnit;

    invoke-virtual {v0, v4, v2}, LX/BoF;->a(Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v8, 0x1

    move-object v4, v1

    move-object v5, v2

    .line 1821702
    invoke-virtual/range {v3 .. v8}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/enums/StoryVisibility;Z)V

    .line 1821703
    const/4 v0, 0x1

    return v0
.end method
