.class public LX/C3e;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3c;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1846021
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C3e;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846022
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1846023
    iput-object p1, p0, LX/C3e;->b:LX/0Ot;

    .line 1846024
    return-void
.end method

.method public static a(LX/0QB;)LX/C3e;
    .locals 4

    .prologue
    .line 1846025
    const-class v1, LX/C3e;

    monitor-enter v1

    .line 1846026
    :try_start_0
    sget-object v0, LX/C3e;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846027
    sput-object v2, LX/C3e;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846028
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846029
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846030
    new-instance v3, LX/C3e;

    const/16 p0, 0x1ecc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3e;-><init>(LX/0Ot;)V

    .line 1846031
    move-object v0, v3

    .line 1846032
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846033
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846034
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846035
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1846036
    check-cast p2, LX/C3d;

    .line 1846037
    iget-object v0, p0, LX/C3e;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3f;

    iget-object v1, p2, LX/C3d;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846038
    invoke-static {v1}, LX/C3b;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/util/Date;

    move-result-object v2

    .line 1846039
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x0

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x1

    invoke-interface {p0, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p0

    .line 1846040
    const/4 p2, 0x0

    const v1, 0x7f0e011d

    invoke-static {p1, p2, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p2

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p2, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p2

    iget-object v1, v0, LX/C3f;->a:LX/C3b;

    invoke-virtual {v1, v2}, LX/C3b;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p2

    move-object p2, p2

    .line 1846041
    invoke-interface {p0, p2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p0

    .line 1846042
    const/4 p2, 0x0

    const v1, 0x7f0e0130

    invoke-static {p1, p2, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p2

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p2, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p2

    iget-object v1, v0, LX/C3f;->a:LX/C3b;

    invoke-virtual {v1, v2}, LX/C3b;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p2

    move-object v2, p2

    .line 1846043
    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const p0, 0x7f0a00b2

    invoke-interface {v2, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1846044
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1846045
    invoke-static {}, LX/1dS;->b()V

    .line 1846046
    const/4 v0, 0x0

    return-object v0
.end method
