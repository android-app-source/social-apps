.class public LX/As0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1719943
    const-class v0, LX/As0;

    sput-object v0, LX/As0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1719944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Landroid/net/Uri;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1719945
    invoke-static {}, LX/As0;->b()Ljava/io/File;

    move-result-object v0

    .line 1719946
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1719947
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v6

    .line 1719948
    :goto_0
    return-object v0

    .line 1719949
    :cond_0
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1719950
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1719951
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1719952
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1719953
    const-wide/16 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 1719954
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1719955
    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1719956
    invoke-virtual {p2, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1719957
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 1719958
    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    .line 1719959
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1719960
    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    move-object v0, v2

    goto :goto_0

    .line 1719961
    :catch_0
    move-exception v0

    move-object v1, v6

    move-object v2, v6

    .line 1719962
    :goto_1
    :try_start_3
    sget-object v3, LX/As0;->a:Ljava/lang/Class;

    const-string v4, "failed to save video to gallery"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1719963
    invoke-static {v2}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1719964
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    move-object v0, v6

    .line 1719965
    goto :goto_0

    .line 1719966
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1719967
    invoke-static {v6}, LX/1pX;->a(Ljava/io/Closeable;)V

    throw v0

    .line 1719968
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v2

    move-object v6, v0

    move-object v0, v2

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v6, v1

    move-object v1, v2

    goto :goto_2

    .line 1719969
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v6

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_1
.end method

.method public static b()Ljava/io/File;
    .locals 3

    .prologue
    .line 1719970
    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "Facebook_edited"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method
