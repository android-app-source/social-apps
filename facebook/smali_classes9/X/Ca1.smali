.class public final LX/Ca1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ca4;


# direct methods
.method public constructor <init>(LX/Ca4;)V
    .locals 0

    .prologue
    .line 1916939
    iput-object p1, p0, LX/Ca1;->a:LX/Ca4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1916940
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1916941
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1916942
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1916943
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1916944
    if-eqz v0, :cond_1

    .line 1916945
    iget-object v1, p0, LX/Ca1;->a:LX/Ca4;

    .line 1916946
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1916947
    iget-object v2, v1, LX/Ca4;->f:LX/CEW;

    new-instance v3, LX/Ca6;

    invoke-direct {v3, v0}, LX/Ca6;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v2, v3}, LX/CEW;->a(LX/Ca6;)V

    .line 1916948
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    .line 1916949
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1916950
    iget-object v6, v1, LX/Ca4;->b:LX/Ca0;

    invoke-virtual {v6, v4, v5}, LX/Ca0;->a(J)LX/CZz;

    move-result-object v5

    .line 1916951
    if-eqz v5, :cond_2

    .line 1916952
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v4

    .line 1916953
    iput-boolean v4, v5, LX/CZz;->m:Z

    .line 1916954
    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v4

    .line 1916955
    iput v4, v5, LX/CZz;->i:I

    .line 1916956
    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    .line 1916957
    iput v4, v5, LX/CZz;->j:I

    .line 1916958
    new-instance v4, LX/Ca6;

    invoke-direct {v4, v5}, LX/Ca6;-><init>(LX/CZz;)V

    .line 1916959
    :goto_0
    move-object v2, v4

    .line 1916960
    if-eqz v2, :cond_1

    .line 1916961
    iget-object v3, v1, LX/Ca4;->f:LX/CEW;

    invoke-virtual {v3, v2}, LX/CEW;->a(LX/Ca6;)V

    .line 1916962
    :cond_1
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
