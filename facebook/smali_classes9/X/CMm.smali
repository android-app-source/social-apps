.class public LX/CMm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1880896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/3Sb;)LX/0Px;
    .locals 14
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUsersFromModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Sb;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1880907
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1880908
    invoke-interface {p0}, LX/3Sb;->b()LX/2sN;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1880909
    const/4 v4, 0x0

    const-class v5, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v3, v0, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    .line 1880910
    if-eqz v0, :cond_0

    .line 1880911
    const/4 v12, 0x1

    .line 1880912
    new-instance v6, LX/0XI;

    invoke-direct {v6}, LX/0XI;-><init>()V

    .line 1880913
    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 1880914
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 1880915
    const-string v7, "GQLBotConverter"

    const-string v8, "Got a user of an unsupported graphql type: %d"

    new-array v9, v12, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1880916
    const-string v7, "user"

    .line 1880917
    iput-object v7, v6, LX/0XI;->y:Ljava/lang/String;

    .line 1880918
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->l()Ljava/lang/String;

    move-result-object v7

    .line 1880919
    iput-object v7, v6, LX/0XI;->h:Ljava/lang/String;

    .line 1880920
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1880921
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1880922
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    invoke-static {v8}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1880923
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1880924
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    invoke-static {v8}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1880925
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1880926
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->m()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v8

    invoke-static {v8}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1880927
    :cond_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1880928
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1880929
    const/4 v7, 0x0

    .line 1880930
    :goto_2
    move-object v7, v7

    .line 1880931
    iput-object v7, v6, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1880932
    iput-boolean v12, v6, LX/0XI;->H:Z

    .line 1880933
    sget-object v7, LX/0SF;->a:LX/0SF;

    move-object v7, v7

    .line 1880934
    invoke-virtual {v7}, LX/0SF;->a()J

    move-result-wide v8

    .line 1880935
    iput-wide v8, v6, LX/0XI;->an:J

    .line 1880936
    invoke-virtual {v6}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v6

    move-object v0, v6

    .line 1880937
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1880938
    :cond_4
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1880939
    :sswitch_0
    const-string v7, "user"

    .line 1880940
    iput-object v7, v6, LX/0XI;->y:Ljava/lang/String;

    .line 1880941
    goto :goto_1

    .line 1880942
    :sswitch_1
    const-string v7, "page"

    .line 1880943
    iput-object v7, v6, LX/0XI;->y:Ljava/lang/String;

    .line 1880944
    goto :goto_1

    .line 1880945
    :sswitch_2
    const-string v7, "event"

    .line 1880946
    iput-object v7, v6, LX/0XI;->y:Ljava/lang/String;

    .line 1880947
    goto :goto_1

    .line 1880948
    :sswitch_3
    const-string v7, "group"

    .line 1880949
    iput-object v7, v6, LX/0XI;->y:Ljava/lang/String;

    .line 1880950
    goto :goto_1

    :cond_5
    new-instance v7, Lcom/facebook/user/model/PicSquare;

    invoke-direct {v7, v8}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_2
        0x41e065f -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;)Ljava/util/List;
    .locals 9
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadParticipantsFromModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 1880897
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1880898
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1880899
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v2, v4, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1880900
    const-class v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v2, v4, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1880901
    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v1, v5, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 1880902
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    const-class v5, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v2, v4, v6, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/BotInfoModels$BotMessagingActorInfoModel$MessagingActorModel;->l()Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1880903
    new-instance v1, LX/6fz;

    invoke-direct {v1}, LX/6fz;-><init>()V

    .line 1880904
    iput-object v0, v1, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1880905
    invoke-virtual {v1}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1880906
    :cond_1
    return-object v7
.end method
