.class public final enum LX/BSA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSA;

.field public static final enum END:LX/BSA;

.field public static final enum START:LX/BSA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1785109
    new-instance v0, LX/BSA;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/BSA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSA;->START:LX/BSA;

    new-instance v0, LX/BSA;

    const-string v1, "END"

    invoke-direct {v0, v1, v3}, LX/BSA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSA;->END:LX/BSA;

    .line 1785110
    const/4 v0, 0x2

    new-array v0, v0, [LX/BSA;

    sget-object v1, LX/BSA;->START:LX/BSA;

    aput-object v1, v0, v2

    sget-object v1, LX/BSA;->END:LX/BSA;

    aput-object v1, v0, v3

    sput-object v0, LX/BSA;->$VALUES:[LX/BSA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1785111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSA;
    .locals 1

    .prologue
    .line 1785112
    const-class v0, LX/BSA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSA;

    return-object v0
.end method

.method public static values()[LX/BSA;
    .locals 1

    .prologue
    .line 1785113
    sget-object v0, LX/BSA;->$VALUES:[LX/BSA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSA;

    return-object v0
.end method
