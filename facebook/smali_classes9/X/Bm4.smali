.class public LX/Bm4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1817825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1817826
    return-void
.end method

.method public static a(Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1817796
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    .line 1817797
    iget-object v1, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v1, v1

    .line 1817798
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->N:Z

    move v2, v2

    .line 1817799
    invoke-static {v0, v1, v2}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1817819
    sget-object v0, LX/Bm3;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1817820
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type does not support setting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1817821
    :pswitch_0
    const-string v0, "OPEN"

    .line 1817822
    :goto_0
    return-object v0

    .line 1817823
    :pswitch_1
    const-string v0, "FRIENDS"

    goto :goto_0

    .line 1817824
    :pswitch_2
    const-string v0, "SECRET"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1817818
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1817806
    if-nez p0, :cond_0

    .line 1817807
    const/4 v0, 0x0

    .line 1817808
    :goto_0
    return-object v0

    .line 1817809
    :cond_0
    if-nez p1, :cond_1

    .line 1817810
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p1

    .line 1817811
    :cond_1
    if-eqz p2, :cond_2

    .line 1817812
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1817813
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1817814
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1817815
    :cond_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm\':00\'Z"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1817816
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1817817
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1817800
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->N:Z

    move v0, v0

    .line 1817801
    if-eqz v0, :cond_0

    .line 1817802
    const/4 v0, 0x0

    .line 1817803
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v0

    .line 1817804
    iget-object v1, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v1, v1

    .line 1817805
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
