.class public LX/CfC;
.super LX/45y;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/CfC;


# instance fields
.field private final a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/ADMService;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsService;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/C2DMService;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/NNAService;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsLiteService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925605
    invoke-direct {p0}, LX/45y;-><init>()V

    .line 1925606
    iput-object p1, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 1925607
    iput-object p2, p0, LX/CfC;->b:LX/0Ot;

    .line 1925608
    iput-object p3, p0, LX/CfC;->c:LX/0Ot;

    .line 1925609
    iput-object p4, p0, LX/CfC;->d:LX/0Ot;

    .line 1925610
    iput-object p5, p0, LX/CfC;->e:LX/0Ot;

    .line 1925611
    iput-object p6, p0, LX/CfC;->f:LX/0Ot;

    .line 1925612
    return-void
.end method

.method public static a(LX/0QB;)LX/CfC;
    .locals 10

    .prologue
    .line 1925613
    sget-object v0, LX/CfC;->g:LX/CfC;

    if-nez v0, :cond_1

    .line 1925614
    const-class v1, LX/CfC;

    monitor-enter v1

    .line 1925615
    :try_start_0
    sget-object v0, LX/CfC;->g:LX/CfC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925616
    if-eqz v2, :cond_0

    .line 1925617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1925618
    new-instance v3, LX/CfC;

    invoke-static {v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v4

    check-cast v4, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    const/16 v5, 0xfe8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xff9

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xfec

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3025

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xffc

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/CfC;-><init>(Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1925619
    move-object v0, v3

    .line 1925620
    sput-object v0, LX/CfC;->g:LX/CfC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925623
    :cond_1
    sget-object v0, LX/CfC;->g:LX/CfC;

    return-object v0

    .line 1925624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1925626
    :try_start_0
    invoke-static {p1}, LX/2Ge;->valueOf(Ljava/lang/String;)LX/2Ge;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1925627
    sget-object v0, LX/CfB;->a:[I

    invoke-virtual {v1}, LX/2Ge;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1925628
    :goto_0
    return-void

    .line 1925629
    :pswitch_0
    iget-object v2, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v0, p0, LX/CfC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    invoke-interface {v0}, LX/2Gh;->c()LX/2H7;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 1925630
    :pswitch_1
    iget-object v2, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v0, p0, LX/CfC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    invoke-interface {v0}, LX/2Gh;->c()LX/2H7;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 1925631
    :pswitch_2
    iget-object v2, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v0, p0, LX/CfC;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    invoke-interface {v0}, LX/2Gh;->c()LX/2H7;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 1925632
    :pswitch_3
    iget-object v2, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v0, p0, LX/CfC;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    invoke-interface {v0}, LX/2Gh;->c()LX/2H7;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 1925633
    :pswitch_4
    iget-object v2, p0, LX/CfC;->a:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iget-object v0, p0, LX/CfC;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    invoke-interface {v0}, LX/2Gh;->c()LX/2H7;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 1925634
    :catch_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1925635
    const/4 v0, 0x0

    return v0
.end method

.method public final a(ILandroid/os/Bundle;LX/45o;)Z
    .locals 1

    .prologue
    .line 1925636
    const-string v0, "serviceType"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925637
    if-eqz v0, :cond_0

    .line 1925638
    invoke-virtual {p0, v0}, LX/CfC;->a(Ljava/lang/String;)V

    .line 1925639
    const/4 v0, 0x1

    .line 1925640
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
