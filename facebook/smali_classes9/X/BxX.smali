.class public LX/BxX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0kL;

.field private final c:LX/0Zb;

.field public final d:Landroid/content/pm/PackageManager;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/1Ay;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kL;LX/0Zb;Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;LX/1Ay;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0kL;",
            "LX/0Zb;",
            "Landroid/content/pm/PackageManager;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ay;",
            "LX/0Ot",
            "<",
            "LX/CH7;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835639
    iput-object p1, p0, LX/BxX;->a:Landroid/content/Context;

    .line 1835640
    iput-object p2, p0, LX/BxX;->b:LX/0kL;

    .line 1835641
    iput-object p3, p0, LX/BxX;->c:LX/0Zb;

    .line 1835642
    iput-object p4, p0, LX/BxX;->d:Landroid/content/pm/PackageManager;

    .line 1835643
    iput-object p5, p0, LX/BxX;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1835644
    iput-object p6, p0, LX/BxX;->f:LX/1Ay;

    .line 1835645
    iput-object p7, p0, LX/BxX;->g:LX/0Ot;

    .line 1835646
    iput-object p8, p0, LX/BxX;->h:LX/0Uh;

    .line 1835647
    return-void
.end method

.method public static a(LX/0QB;)LX/BxX;
    .locals 12

    .prologue
    .line 1835627
    const-class v1, LX/BxX;

    monitor-enter v1

    .line 1835628
    :try_start_0
    sget-object v0, LX/BxX;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835629
    sput-object v2, LX/BxX;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835630
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835631
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835632
    new-instance v3, LX/BxX;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v9

    check-cast v9, LX/1Ay;

    const/16 v10, 0x2524

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v3 .. v11}, LX/BxX;-><init>(Landroid/content/Context;LX/0kL;LX/0Zb;Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;LX/1Ay;LX/0Ot;LX/0Uh;)V

    .line 1835633
    move-object v0, v3

    .line 1835634
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835635
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835636
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835637
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 1835626
    iget-object v0, p0, LX/BxX;->h:LX/0Uh;

    const/16 v1, 0x60e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1835623
    invoke-direct {p0}, LX/BxX;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835624
    iget-object v0, p0, LX/BxX;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080d25

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1835625
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 1835607
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1835608
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1835609
    if-eqz p3, :cond_0

    .line 1835610
    const-string v1, "instant_article_id"

    invoke-virtual {p3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1835611
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1835612
    const-string v2, "extra_instant_articles_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1835613
    const-string v1, "extra_instant_articles_referrer"

    const-string v2, "feed_article_chaining"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1835614
    :cond_0
    iget-object v1, p0, LX/BxX;->d:Landroid/content/pm/PackageManager;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1835615
    if-eqz v1, :cond_1

    .line 1835616
    iget-object v1, p0, LX/BxX;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/BxX;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1835617
    :cond_1
    invoke-static {p3}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1835618
    invoke-static {p3, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1835619
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->r()LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1835620
    iget-object v0, p0, LX/BxX;->f:LX/1Ay;

    invoke-virtual {p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->r()LX/0lF;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    .line 1835621
    :cond_3
    iget-object v0, p0, LX/BxX;->c:LX/0Zb;

    invoke-interface {v0, p3}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1835622
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4

    .prologue
    .line 1835602
    iget-object v0, p0, LX/BxX;->h:LX/0Uh;

    const/16 v1, 0x3db

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1835603
    :cond_0
    :goto_0
    return-void

    .line 1835604
    :cond_1
    invoke-static {p1}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 1835605
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1835606
    iget-object v0, p0, LX/BxX;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CH7;

    iget-object v2, p0, LX/BxX;->a:Landroid/content/Context;

    sget-object v3, LX/CH2;->FEED_CHAINING:LX/CH2;

    invoke-virtual {v0, v2, v1, v3, p2}, LX/CH7;->a(Landroid/content/Context;Ljava/lang/String;LX/CH2;Lcom/facebook/common/callercontext/CallerContext;)LX/CGw;

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1835599
    invoke-direct {p0}, LX/BxX;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835600
    iget-object v0, p0, LX/BxX;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080d26

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1835601
    :cond_0
    return-void
.end method
