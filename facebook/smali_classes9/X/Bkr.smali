.class public LX/Bkr;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private b:Landroid/content/res/Resources;

.field private c:LX/0wM;

.field private d:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/BjB;

.field public h:LX/BjC;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1815078
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1815079
    const/4 v0, 0x2

    iput v0, p0, LX/Bkr;->a:I

    .line 1815080
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1815081
    iput-object v0, p0, LX/Bkr;->f:LX/0Px;

    .line 1815082
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/Bkr;->b:Landroid/content/res/Resources;

    .line 1815083
    new-instance v0, LX/0wM;

    iget-object v1, p0, LX/Bkr;->b:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/Bkr;->c:LX/0wM;

    .line 1815084
    iput-object p2, p0, LX/Bkr;->d:Landroid/graphics/drawable/Drawable;

    .line 1815085
    iput-object p3, p0, LX/Bkr;->e:Landroid/graphics/drawable/Drawable;

    .line 1815086
    return-void
.end method

.method private a(LX/Bkt;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;I)V
    .locals 6
    .param p3    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1815087
    iget-object v4, p0, LX/Bkr;->h:LX/BjC;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/Bkt;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;LX/BjC;I)V

    .line 1815088
    return-void
.end method

.method private a(Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1815053
    invoke-direct {p0, p2}, LX/Bkr;->e(I)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    move-result-object v1

    .line 1815054
    if-eqz v1, :cond_1

    .line 1815055
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1815056
    if-eqz v0, :cond_0

    move v0, v5

    :goto_0
    if-eqz v0, :cond_3

    .line 1815057
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1815058
    invoke-virtual {v2, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v5

    :goto_1
    if-eqz v0, :cond_5

    .line 1815059
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1815060
    if-eqz v0, :cond_4

    move v0, v5

    :goto_2
    if-eqz v0, :cond_7

    .line 1815061
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1815062
    invoke-virtual {v2, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v5

    :goto_3
    if-eqz v0, :cond_9

    .line 1815063
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1815064
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1815065
    invoke-virtual {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Bkr;->g:LX/BjB;

    if-nez p2, :cond_8

    :goto_4
    move-object v0, p1

    .line 1815066
    iput-object v1, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->n:Ljava/lang/String;

    .line 1815067
    iput-object v3, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->o:Ljava/lang/String;

    .line 1815068
    iget-object v6, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object p1, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->r:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1815069
    iget-object v6, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p0, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1815070
    iput-object v4, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->q:LX/BjB;

    .line 1815071
    if-eqz v5, :cond_a

    .line 1815072
    iget-object v6, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->m:Lcom/facebook/resources/ui/FbTextView;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1815073
    :goto_5
    return-void

    :cond_0
    move v0, v6

    .line 1815074
    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_2

    :cond_5
    move v0, v6

    goto :goto_2

    :cond_6
    move v0, v6

    goto :goto_3

    :cond_7
    move v0, v6

    goto :goto_3

    :cond_8
    move v5, v6

    .line 1815075
    goto :goto_4

    .line 1815076
    :cond_9
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1815077
    :cond_a
    iget-object v6, v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->m:Lcom/facebook/resources/ui/FbTextView;

    const/16 p0, 0x8

    invoke-virtual {v6, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_5
.end method

.method private e(I)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1815050
    iget-object v0, p0, LX/Bkr;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1815051
    iget-object v0, p0, LX/Bkr;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1815052
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1815089
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1815090
    if-nez p2, :cond_0

    .line 1815091
    const v1, 0x7f03142f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1815092
    new-instance v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;-><init>(Landroid/view/View;)V

    .line 1815093
    :goto_0
    return-object v0

    .line 1815094
    :cond_0
    const v1, 0x7f03039e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1815095
    new-instance v0, LX/Bkt;

    invoke-direct {v0, v1}, LX/Bkt;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1815047
    iput-object p1, p0, LX/Bkr;->f:LX/0Px;

    .line 1815048
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1815049
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 1815041
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 1815042
    packed-switch v0, :pswitch_data_0

    .line 1815043
    :goto_0
    return-void

    .line 1815044
    :pswitch_0
    check-cast p1, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;

    invoke-direct {p0, p1, p2}, LX/Bkr;->a(Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;I)V

    goto :goto_0

    :pswitch_1
    move-object v1, p1

    .line 1815045
    check-cast v1, LX/Bkt;

    iget-object v0, p0, LX/Bkr;->b:Landroid/content/res/Resources;

    const v2, 0x7f08213d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Bkr;->e:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, LX/Bkr;->c:LX/0wM;

    const v4, 0x7f02095e

    invoke-virtual {v0, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Bkr;->a(LX/Bkt;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    :pswitch_2
    move-object v1, p1

    .line 1815046
    check-cast v1, LX/Bkt;

    iget-object v0, p0, LX/Bkr;->b:Landroid/content/res/Resources;

    const v2, 0x7f08213f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Bkr;->d:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, LX/Bkr;->c:LX/0wM;

    const v4, 0x7f020962

    invoke-virtual {v0, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Bkr;->a(LX/Bkt;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1815034
    iget-object v0, p0, LX/Bkr;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1815035
    const/4 v0, 0x0

    .line 1815036
    :goto_0
    return v0

    .line 1815037
    :cond_0
    iget-object v0, p0, LX/Bkr;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 1815038
    const/4 v0, 0x1

    goto :goto_0

    .line 1815039
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1815040
    iget-object v0, p0, LX/Bkr;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
