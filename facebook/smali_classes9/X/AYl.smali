.class public final LX/AYl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AYo;


# direct methods
.method public constructor <init>(LX/AYo;)V
    .locals 0

    .prologue
    .line 1686059
    iput-object p1, p0, LX/AYl;->a:LX/AYo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0xb2a6a5f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1686060
    iget-object v1, p0, LX/AYl;->a:LX/AYo;

    const/4 v3, 0x0

    .line 1686061
    iget-object v4, v1, LX/AYo;->k:LX/AYM;

    if-nez v4, :cond_0

    .line 1686062
    :goto_0
    const v1, 0x6416bf5c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1686063
    :cond_0
    iget-object v4, v1, LX/AYo;->c:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbFrameLayout;->setClickable(Z)V

    .line 1686064
    iget-boolean v4, v1, LX/AYo;->n:Z

    if-nez v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    iput-boolean v3, v1, LX/AYo;->n:Z

    .line 1686065
    iget-object v3, v1, LX/AYo;->a:LX/AVT;

    iget-boolean v4, v1, LX/AYo;->n:Z

    .line 1686066
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1686067
    const-string v5, "facecast_event_name"

    const-string p1, "facecast_toggle_audio_live"

    invoke-interface {p0, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686068
    const-string p1, "facecast_event_extra"

    if-eqz v4, :cond_6

    const-string v5, "on"

    :goto_1
    invoke-interface {p0, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686069
    invoke-virtual {v3, p0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1686070
    iget-object v3, v1, LX/AYo;->k:LX/AYM;

    iget-boolean v4, v1, LX/AYo;->n:Z

    invoke-interface {v3, v4}, LX/AYM;->b(Z)V

    .line 1686071
    iget-boolean v3, v1, LX/AYo;->n:Z

    if-eqz v3, :cond_3

    iget-object v3, v1, LX/AYo;->g:Lcom/facebook/fbui/glyph/GlyphView;

    :goto_2
    const p0, 0x3f666666    # 0.9f

    .line 1686072
    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const v5, 0x3f4ccccd    # 0.8f

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686073
    iget-boolean v3, v1, LX/AYo;->n:Z

    if-eqz v3, :cond_4

    iget-object v3, v1, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    :goto_3
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1686074
    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686075
    iget-object v3, v1, LX/AYo;->l:Landroid/animation/Animator$AnimatorListener;

    if-nez v3, :cond_2

    .line 1686076
    new-instance v3, LX/AYm;

    invoke-direct {v3, v1}, LX/AYm;-><init>(LX/AYo;)V

    iput-object v3, v1, LX/AYo;->l:Landroid/animation/Animator$AnimatorListener;

    .line 1686077
    :cond_2
    iget-object v3, v1, LX/AYo;->f:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    iget-boolean v3, v1, LX/AYo;->n:Z

    if-eqz v3, :cond_5

    iget v3, v1, LX/AYo;->j:I

    neg-int v3, v3

    int-to-float v3, v3

    :goto_4
    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, v1, LX/AYo;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_0

    .line 1686078
    :cond_3
    iget-object v3, v1, LX/AYo;->h:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_2

    .line 1686079
    :cond_4
    iget-object v3, v1, LX/AYo;->g:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_3

    .line 1686080
    :cond_5
    iget v3, v1, LX/AYo;->j:I

    int-to-float v3, v3

    goto :goto_4

    .line 1686081
    :cond_6
    const-string v5, "off"

    goto :goto_1
.end method
