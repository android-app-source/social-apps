.class public final LX/CGv;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/CH2;

.field public final synthetic c:LX/3Bl;

.field public final synthetic d:LX/CH1;

.field public final synthetic e:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic f:Landroid/content/Context;

.field public final synthetic g:LX/CH7;


# direct methods
.method public constructor <init>(LX/CH7;Ljava/lang/String;LX/CH2;LX/3Bl;LX/CH1;Lcom/facebook/common/callercontext/CallerContext;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1865980
    iput-object p1, p0, LX/CGv;->g:LX/CH7;

    iput-object p2, p0, LX/CGv;->a:Ljava/lang/String;

    iput-object p3, p0, LX/CGv;->b:LX/CH2;

    iput-object p4, p0, LX/CGv;->c:LX/3Bl;

    iput-object p5, p0, LX/CGv;->d:LX/CH1;

    iput-object p6, p0, LX/CGv;->e:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p7, p0, LX/CGv;->f:Landroid/content/Context;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1866092
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->h:LX/0QI;

    iget-object v1, p0, LX/CGv;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 1866093
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, p0, LX/CGv;->b:LX/CH2;

    iget-object v2, p0, LX/CGv;->c:LX/3Bl;

    .line 1866094
    sget-object v3, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {v1, v3}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1866095
    iget-object v3, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6VQ;

    .line 1866096
    iget-object p0, v2, LX/3Bl;->c:Ljava/lang/String;

    move-object p0, p0

    .line 1866097
    iget-object p1, v2, LX/3Bl;->d:Ljava/lang/Integer;

    move-object p1, p1

    .line 1866098
    invoke-virtual {v3, p0, p1}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1866099
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 1865981
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1865982
    if-eqz p1, :cond_0

    .line 1865983
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1865984
    if-nez v0, :cond_2

    .line 1865985
    :cond_0
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, p0, LX/CGv;->b:LX/CH2;

    iget-object v2, p0, LX/CGv;->c:LX/3Bl;

    .line 1865986
    sget-object v3, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {v1, v3}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1865987
    iget-object v3, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6VQ;

    .line 1865988
    iget-object v4, v2, LX/3Bl;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1865989
    iget-object v5, v2, LX/3Bl;->d:Ljava/lang/Integer;

    move-object v5, v5

    .line 1865990
    invoke-virtual {v3, v4, v5}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1865991
    :cond_1
    :goto_0
    return-void

    .line 1865992
    :cond_2
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->j:LX/0QI;

    iget-object v1, p0, LX/CGv;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1865993
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->j:LX/0QI;

    iget-object v1, p0, LX/CGv;->a:Ljava/lang/String;

    iget-object v2, p0, LX/CGv;->b:LX/CH2;

    invoke-interface {v0, v1, v2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1865994
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1865995
    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;->j()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1865996
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1865997
    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel;->j()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel;

    move-result-object v2

    .line 1865998
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, p0, LX/CGv;->b:LX/CH2;

    invoke-static {v0, v1}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1865999
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    iget-object v1, p0, LX/CGv;->a:Ljava/lang/String;

    iget-object v3, p0, LX/CGv;->c:LX/3Bl;

    .line 1866000
    if-nez v1, :cond_15

    .line 1866001
    :cond_4
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->f:LX/8bK;

    invoke-virtual {v0}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    sget-object v1, LX/0p3;->POOR:LX/0p3;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->e:LX/0ad;

    sget-short v1, LX/2yD;->v:S

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1866002
    :cond_5
    invoke-interface {v2}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    .line 1866003
    iget-object v1, v0, LX/CH7;->a:LX/0Uh;

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1866004
    if-eqz v0, :cond_6

    .line 1866005
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Yg;

    invoke-interface {v2}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v1

    const/4 v5, 0x0

    .line 1866006
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->p()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1866007
    :cond_6
    :goto_1
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    invoke-static {v0}, LX/CH7;->d(LX/CH7;)Z

    move-result v8

    .line 1866008
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    .line 1866009
    invoke-static {v0}, LX/CH7;->d(LX/CH7;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1866010
    sget-object v1, LX/CH3;->NONE:LX/CH3;

    .line 1866011
    :goto_2
    move-object v5, v1

    .line 1866012
    iget-object v0, p0, LX/CGv;->b:LX/CH2;

    sget-object v1, LX/CH2;->FEED:LX/CH2;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    .line 1866013
    iget-object v1, v0, LX/CH7;->a:LX/0Uh;

    const/16 v3, 0x501

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1866014
    if-nez v0, :cond_a

    .line 1866015
    :cond_7
    invoke-interface {v2}, LX/B5Y;->iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1866016
    invoke-interface {v2}, LX/B5Y;->iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;

    move-result-object v1

    .line 1866017
    const/4 v3, 0x0

    .line 1866018
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->k()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-eq v4, v0, :cond_19

    .line 1866019
    :cond_8
    :goto_3
    move v3, v3

    .line 1866020
    move v0, v3

    .line 1866021
    if-eqz v0, :cond_d

    .line 1866022
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->c:LX/8bW;

    .line 1866023
    invoke-interface {v1}, LX/8Z2;->j()LX/8Ys;

    move-result-object v3

    invoke-interface {v3}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v3

    .line 1866024
    invoke-interface {v1}, LX/8Z2;->j()LX/8Ys;

    move-result-object v4

    invoke-interface {v4}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v4

    .line 1866025
    invoke-virtual {v0, v3, v4}, LX/8bW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1866026
    iget-object v0, p0, LX/CGv;->d:LX/CH1;

    .line 1866027
    iput-object v1, v0, LX/CH1;->b:LX/8Z2;

    .line 1866028
    :cond_9
    :goto_4
    invoke-interface {v2}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1866029
    invoke-interface {v2}, LX/B5Y;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLogoModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 1866030
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1866031
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->c:LX/8bW;

    iget-object v3, p0, LX/CGv;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v3

    .line 1866032
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v4, p0, LX/CGv;->b:LX/CH2;

    invoke-static {v0, v4}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1866033
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    iget-object v4, p0, LX/CGv;->a:Ljava/lang/String;

    iget-object v6, p0, LX/CGv;->c:LX/3Bl;

    invoke-virtual {v0, v4, v1, v6}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bl;)V

    .line 1866034
    new-instance v0, LX/CH0;

    iget-object v4, p0, LX/CGv;->g:LX/CH7;

    iget-object v4, v4, LX/CH7;->g:LX/0Ot;

    iget-object v6, p0, LX/CGv;->c:LX/3Bl;

    invoke-direct {v0, v1, v4, v6}, LX/CH0;-><init>(Ljava/lang/String;LX/0Ot;LX/3Bl;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v3, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1866035
    :cond_a
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    .line 1866036
    iget-object v1, v0, LX/CH7;->a:LX/0Uh;

    const/16 v3, 0x500

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1866037
    if-eqz v0, :cond_12

    .line 1866038
    invoke-interface {v2}, LX/B5Y;->l()LX/B5X;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1866039
    invoke-interface {v2}, LX/B5Y;->l()LX/B5X;

    move-result-object v0

    invoke-interface {v0}, LX/B5X;->a()LX/0Px;

    move-result-object v0

    .line 1866040
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_b
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B5b;

    .line 1866041
    invoke-interface {v0}, LX/B5b;->a()LX/B5a;

    move-result-object v4

    .line 1866042
    if-eqz v8, :cond_c

    .line 1866043
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, p0, LX/CGv;->b:LX/CH2;

    iget-object v2, p0, LX/CGv;->f:Landroid/content/Context;

    iget-object v3, p0, LX/CGv;->a:Ljava/lang/String;

    iget-object v6, p0, LX/CGv;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v7, p0, LX/CGv;->c:LX/3Bl;

    invoke-static/range {v0 .. v7}, LX/CH7;->a(LX/CH7;LX/CH2;Landroid/content/Context;Ljava/lang/String;LX/B5a;LX/CH3;Lcom/facebook/common/callercontext/CallerContext;LX/3Bl;)V

    .line 1866044
    :cond_c
    invoke-interface {v4}, LX/B5a;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1866045
    if-eqz v0, :cond_b

    .line 1866046
    iget-object v1, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, v1, LX/CH7;->d:LX/CjE;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1866047
    const-wide/16 v10, 0x708

    invoke-static {v0, v10, v11}, LX/CjE;->a(Ljava/lang/String;J)LX/0zO;

    move-result-object v10

    .line 1866048
    iget-object v11, v1, LX/CjE;->a:LX/0tX;

    invoke-virtual {v11, v10}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    .line 1866049
    goto :goto_5

    .line 1866050
    :cond_d
    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->k()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v0, v3, :cond_9

    .line 1866051
    invoke-virtual {v1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->em_()LX/8Yr;

    move-result-object v3

    .line 1866052
    if-eqz v3, :cond_9

    .line 1866053
    const/4 v0, 0x0

    .line 1866054
    if-eqz v8, :cond_f

    .line 1866055
    invoke-static {v1, v5}, LX/CH7;->a(LX/8Yz;LX/CH3;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1866056
    move-object v1, v0

    .line 1866057
    :goto_6
    if-eqz v1, :cond_9

    .line 1866058
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->c:LX/8bW;

    iget-object v3, p0, LX/CGv;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v3

    .line 1866059
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v4, p0, LX/CGv;->b:LX/CH2;

    invoke-static {v0, v4}, LX/CH7;->a$redex0(LX/CH7;LX/CH2;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1866060
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v0, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VQ;

    iget-object v4, p0, LX/CGv;->a:Ljava/lang/String;

    iget-object v6, p0, LX/CGv;->c:LX/3Bl;

    invoke-virtual {v0, v4, v1, v6}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/String;LX/3Bl;)V

    .line 1866061
    iget-object v0, p0, LX/CGv;->d:LX/CH1;

    iget-object v4, p0, LX/CGv;->g:LX/CH7;

    iget-object v4, v4, LX/CH7;->g:LX/0Ot;

    iget-object v6, p0, LX/CGv;->c:LX/3Bl;

    .line 1866062
    iput-object v3, v0, LX/CH1;->c:LX/1ca;

    .line 1866063
    iget-object v7, v0, LX/CH1;->c:LX/1ca;

    if-eqz v7, :cond_e

    .line 1866064
    iget-object v7, v0, LX/CH1;->c:LX/1ca;

    new-instance v9, LX/CH0;

    invoke-direct {v9, v1, v4, v6}, LX/CH0;-><init>(Ljava/lang/String;LX/0Ot;LX/3Bl;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v10

    invoke-interface {v7, v9, v10}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1866065
    :cond_e
    goto/16 :goto_4

    .line 1866066
    :cond_f
    invoke-interface {v3}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v1

    .line 1866067
    if-eqz v1, :cond_14

    .line 1866068
    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_6

    .line 1866069
    :cond_10
    iget-object v0, p0, LX/CGv;->d:LX/CH1;

    .line 1866070
    iput-object v3, v0, LX/CH1;->c:LX/1ca;

    .line 1866071
    iget-object v1, v0, LX/CH1;->c:LX/1ca;

    if-eqz v1, :cond_11

    .line 1866072
    iget-object v1, v0, LX/CH1;->c:LX/1ca;

    new-instance v4, LX/8bV;

    invoke-direct {v4}, LX/8bV;-><init>()V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v6

    invoke-interface {v1, v4, v6}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1866073
    :cond_11
    goto/16 :goto_4

    .line 1866074
    :cond_12
    iget-object v0, p0, LX/CGv;->g:LX/CH7;

    iget-object v1, p0, LX/CGv;->b:LX/CH2;

    iget-object v2, p0, LX/CGv;->c:LX/3Bl;

    .line 1866075
    sget-object v3, LX/CH2;->OFFLINE_FEED:LX/CH2;

    invoke-virtual {v1, v3}, LX/CH2;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1866076
    invoke-virtual {v2}, LX/3Bl;->d()V

    .line 1866077
    invoke-virtual {v2}, LX/3Bl;->c()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1866078
    iget-object v3, v0, LX/CH7;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6VQ;

    .line 1866079
    iget-object v4, v2, LX/3Bl;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1866080
    iget-object v5, v2, LX/3Bl;->d:Ljava/lang/Integer;

    move-object v5, v5

    .line 1866081
    invoke-virtual {v3, v4, v5}, LX/6VQ;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1866082
    :cond_13
    goto/16 :goto_0

    :cond_14
    move-object v1, v0

    goto/16 :goto_6

    .line 1866083
    :cond_15
    invoke-static {v0, v3}, LX/6VQ;->b(LX/6VQ;LX/3Bl;)V

    .line 1866084
    iget-object v4, v0, LX/6VQ;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1fI;

    .line 1866085
    const-string v0, "ATTACHMENT_TEXT"

    const/4 v3, 0x1

    invoke-static {v4, v1, v0, v3}, LX/1fI;->a(LX/1fI;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1866086
    goto :goto_7

    .line 1866087
    :cond_16
    new-instance v3, LX/8Yk;

    invoke-direct {v3, v1}, LX/8Yk;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1866088
    invoke-virtual {v3}, LX/8Yk;->a()Ljava/util/Set;

    move-result-object v3

    .line 1866089
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->p()LX/0Px;

    move-result-object v4

    invoke-static {v0, v4, v3, v5, v5}, LX/8Yg;->a(LX/8Yg;LX/0Px;Ljava/util/Set;ZZ)Ljava/util/Map;

    goto/16 :goto_1

    .line 1866090
    :cond_17
    iget-object v1, v0, LX/CH7;->e:LX/0ad;

    sget-short v3, LX/2yD;->k:S

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 1866091
    if-eqz v1, :cond_18

    sget-object v1, LX/CH3;->INTERMEDIATE_IMAGE:LX/CH3;

    goto/16 :goto_2

    :cond_18
    sget-object v1, LX/CH3;->FINAL_IMAGE:LX/CH3;

    goto/16 :goto_2

    :cond_19
    if-eqz v1, :cond_8

    invoke-interface {v1}, LX/8Z2;->j()LX/8Ys;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, LX/8Z2;->j()LX/8Ys;

    move-result-object v4

    invoke-interface {v4}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    const/4 v3, 0x1

    goto/16 :goto_3
.end method
