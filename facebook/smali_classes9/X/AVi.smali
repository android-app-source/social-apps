.class public abstract LX/AVi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1680199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1680200
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 1680201
    :goto_0
    return-void

    .line 1680202
    :cond_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    .line 1680203
    iput-object p1, p0, LX/AVi;->a:Landroid/view/View;

    .line 1680204
    if-nez v0, :cond_1

    .line 1680205
    invoke-virtual {p0, p1}, LX/AVi;->b(Landroid/view/View;)V

    goto :goto_0

    .line 1680206
    :cond_1
    invoke-virtual {p0, p1, v0}, LX/AVi;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method public abstract a(Landroid/view/View;Landroid/view/View;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TV;)V"
        }
    .end annotation
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1680207
    invoke-virtual {p0}, LX/AVi;->c()V

    .line 1680208
    const/4 v0, 0x0

    iput-object v0, p0, LX/AVi;->a:Landroid/view/View;

    .line 1680209
    return-void
.end method

.method public abstract b(Landroid/view/View;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public abstract c()V
.end method
