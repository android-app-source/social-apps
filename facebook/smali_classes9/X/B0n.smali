.class public LX/B0n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/0kL;

.field private final f:LX/189;

.field private final g:LX/0bH;

.field public final h:LX/1Sl;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0bH;LX/189;LX/0tX;Landroid/content/res/Resources;LX/0kL;LX/1Sl;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1734289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734290
    iput-object p1, p0, LX/B0n;->a:Ljava/lang/String;

    .line 1734291
    iput-object p2, p0, LX/B0n;->c:Ljava/util/concurrent/ExecutorService;

    .line 1734292
    iput-object p3, p0, LX/B0n;->g:LX/0bH;

    .line 1734293
    iput-object p4, p0, LX/B0n;->f:LX/189;

    .line 1734294
    iput-object p5, p0, LX/B0n;->b:LX/0tX;

    .line 1734295
    iput-object p6, p0, LX/B0n;->d:Landroid/content/res/Resources;

    .line 1734296
    iput-object p7, p0, LX/B0n;->e:LX/0kL;

    .line 1734297
    iput-object p8, p0, LX/B0n;->h:LX/1Sl;

    .line 1734298
    return-void
.end method

.method public static a(LX/0QB;)LX/B0n;
    .locals 10

    .prologue
    .line 1734286
    new-instance v1, LX/B0n;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v9

    check-cast v9, LX/1Sl;

    invoke-direct/range {v1 .. v9}, LX/B0n;-><init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0bH;LX/189;LX/0tX;Landroid/content/res/Resources;LX/0kL;LX/1Sl;)V

    .line 1734287
    move-object v0, v1

    .line 1734288
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1734268
    invoke-static {p0}, LX/B0n;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/B0n;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/B0n;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;)V
    .locals 6
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1734269
    if-nez p2, :cond_0

    .line 1734270
    :goto_0
    return-void

    .line 1734271
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1734272
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1734273
    iget-object v2, p0, LX/B0n;->f:LX/189;

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->j()Z

    move-result v3

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->l()Z

    move-result v4

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v0, v3, v4, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZLcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1734274
    invoke-static {p1}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v0

    .line 1734275
    if-eqz v0, :cond_2

    :goto_2
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1734276
    iget-object v1, p0, LX/B0n;->g:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 1734277
    :cond_1
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1734278
    iput-object v5, v1, LX/173;->f:Ljava/lang/String;

    .line 1734279
    move-object v1, v1

    .line 1734280
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1734281
    goto :goto_2
.end method

.method private static c(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 1734282
    invoke-static {p0}, LX/B0n;->e(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1734283
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1734284
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v0

    .line 1734285
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1734222
    invoke-static {p0}, LX/B0n;->e(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1734223
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1734224
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->I()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1734225
    :cond_0
    return v0
.end method

.method private static e(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 1

    .prologue
    .line 1734221
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    const v0, -0x41c8fa46

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1734226
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1734227
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1734228
    invoke-static {v0}, LX/B0n;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1734229
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1734230
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1734231
    new-instance v1, LX/4Em;

    invoke-direct {v1}, LX/4Em;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1734232
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734233
    move-object v1, v1

    .line 1734234
    iget-object v2, p0, LX/B0n;->a:Ljava/lang/String;

    .line 1734235
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734236
    move-object v1, v1

    .line 1734237
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1734238
    const-string v2, "feedback_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734239
    move-object v0, v1

    .line 1734240
    new-instance v1, LX/9Ll;

    invoke-direct {v1}, LX/9Ll;-><init>()V

    move-object v1, v1

    .line 1734241
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1734242
    iget-object v0, p0, LX/B0n;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1734243
    new-instance v1, LX/B0l;

    invoke-direct {v1, p0, p1}, LX/B0l;-><init>(LX/B0n;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object v2, p0, LX/B0n;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1734244
    :cond_0
    :goto_0
    return-void

    .line 1734245
    :cond_1
    invoke-static {v0}, LX/B0n;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734246
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1734247
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1734248
    new-instance v1, LX/4El;

    invoke-direct {v1}, LX/4El;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1734249
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734250
    move-object v1, v1

    .line 1734251
    iget-object v2, p0, LX/B0n;->a:Ljava/lang/String;

    .line 1734252
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734253
    move-object v1, v1

    .line 1734254
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1734255
    const-string v2, "feedback_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734256
    move-object v0, v1

    .line 1734257
    new-instance v1, LX/9Lk;

    invoke-direct {v1}, LX/9Lk;-><init>()V

    move-object v1, v1

    .line 1734258
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1734259
    iget-object v0, p0, LX/B0n;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1734260
    new-instance v1, LX/B0m;

    invoke-direct {v1, p0, p1}, LX/B0m;-><init>(LX/B0n;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object v2, p0, LX/B0n;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1734261
    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1734262
    invoke-static {p1}, LX/B0n;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734263
    iget-object v0, p0, LX/B0n;->d:Landroid/content/res/Resources;

    const v1, 0x7f081060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1734264
    :goto_0
    return-object v0

    .line 1734265
    :cond_0
    invoke-static {p1}, LX/B0n;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1734266
    iget-object v0, p0, LX/B0n;->d:Landroid/content/res/Resources;

    const v1, 0x7f081061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1734267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
