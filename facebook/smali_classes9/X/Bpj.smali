.class public final LX/Bpj;
.super LX/8qA;
.source ""


# instance fields
.field public final synthetic a:LX/Bpl;


# direct methods
.method public constructor <init>(LX/Bpl;)V
    .locals 0

    .prologue
    .line 1823498
    iput-object p1, p0, LX/Bpj;->a:LX/Bpl;

    invoke-direct {p0}, LX/8qA;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 1823499
    check-cast p1, LX/8q9;

    .line 1823500
    iget-object v0, p0, LX/Bpj;->a:LX/Bpl;

    iget-object v0, v0, LX/Bpl;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823501
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1823502
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1823503
    if-eqz v0, :cond_1

    iget-object v1, p1, LX/8q9;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1823504
    :cond_0
    :goto_0
    return-void

    .line 1823505
    :cond_1
    iget-object v0, p0, LX/Bpj;->a:LX/Bpl;

    iget-object v1, p1, LX/8q9;->d:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1823506
    iget-object v2, v0, LX/Bpl;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823507
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1823508
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1823509
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1823510
    :cond_2
    const/4 v2, 0x0

    .line 1823511
    :goto_1
    move-object v0, v2

    .line 1823512
    if-eqz v0, :cond_0

    .line 1823513
    iget-object v1, p0, LX/Bpj;->a:LX/Bpl;

    iget-object v1, v1, LX/Bpl;->a:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1823514
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    .line 1823515
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1823516
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_5

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1823517
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1823518
    invoke-static {v4}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v4

    .line 1823519
    iput-object v1, v4, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1823520
    move-object v4, v4

    .line 1823521
    invoke-virtual {v4}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 1823522
    iput-object v4, v8, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1823523
    move-object v4, v8

    .line 1823524
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 1823525
    :cond_4
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1823526
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 1823527
    :cond_5
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 1823528
    iget-object v2, v0, LX/Bpl;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823529
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v5

    .line 1823530
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    invoke-static {v3}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    .line 1823531
    iput-object v4, v3, LX/39x;->q:LX/0Px;

    .line 1823532
    move-object v3, v3

    .line 1823533
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1823534
    iput-object v3, v2, LX/23u;->k:LX/0Px;

    .line 1823535
    move-object v2, v2

    .line 1823536
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    goto/16 :goto_1
.end method
