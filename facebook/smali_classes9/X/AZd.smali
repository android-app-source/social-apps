.class public LX/AZd;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:I

.field private final c:F

.field private final d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687261
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AZd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687262
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1687263
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AZd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687264
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1687265
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687266
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/AZd;->a:Landroid/graphics/Paint;

    .line 1687267
    iget-object v0, p0, LX/AZd;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1687268
    invoke-virtual {p0}, LX/AZd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/AZd;->c:F

    .line 1687269
    iget-object v0, p0, LX/AZd;->a:Landroid/graphics/Paint;

    iget v1, p0, LX/AZd;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1687270
    invoke-virtual {p0}, LX/AZd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/AZd;->b:I

    .line 1687271
    invoke-virtual {p0}, LX/AZd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/AZd;->d:I

    .line 1687272
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1687273
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1687274
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 1687275
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    .line 1687276
    invoke-static {v0, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    .line 1687277
    int-to-float v0, v0

    div-float/2addr v0, v5

    .line 1687278
    int-to-float v1, v1

    div-float/2addr v1, v5

    .line 1687279
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1687280
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    iget v4, p0, LX/AZd;->e:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1687281
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1687282
    invoke-virtual {p0}, LX/AZd;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1687283
    iget v3, p0, LX/AZd;->c:F

    div-float/2addr v3, v5

    sub-float v3, v2, v3

    .line 1687284
    iget-object v4, p0, LX/AZd;->a:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1687285
    iget-object v4, p0, LX/AZd;->a:Landroid/graphics/Paint;

    iget v5, p0, LX/AZd;->b:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1687286
    iget-object v4, p0, LX/AZd;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1687287
    :cond_0
    invoke-virtual {p0}, LX/AZd;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1687288
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1687289
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    iget v4, p0, LX/AZd;->d:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1687290
    iget-object v3, p0, LX/AZd;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1687291
    :cond_1
    return-void
.end method

.method public setPressed(Z)V
    .locals 0

    .prologue
    .line 1687292
    invoke-super {p0, p1}, Landroid/view/View;->setPressed(Z)V

    .line 1687293
    invoke-virtual {p0}, LX/AZd;->invalidate()V

    .line 1687294
    return-void
.end method
