.class public final LX/Bfq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<TT;",
        "LX/Bfc",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bfc;

.field public final synthetic b:LX/BeU;


# direct methods
.method public constructor <init>(LX/BeU;LX/Bfc;)V
    .locals 0

    .prologue
    .line 1806644
    iput-object p1, p0, LX/Bfq;->b:LX/BeU;

    iput-object p2, p0, LX/Bfq;->a:LX/Bfc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1806645
    check-cast p1, Landroid/view/View;

    .line 1806646
    if-nez p1, :cond_0

    .line 1806647
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1806648
    :goto_0
    return-object v0

    .line 1806649
    :cond_0
    iget-object v0, p0, LX/Bfq;->b:LX/BeU;

    iget-boolean v0, v0, LX/BeU;->c:Z

    if-nez v0, :cond_1

    .line 1806650
    iget-object v0, p0, LX/Bfq;->b:LX/BeU;

    invoke-virtual {v0}, LX/BeU;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08293d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/Bg1;->a(Landroid/view/View;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1806651
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1806652
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1806653
    :cond_1
    iget-object v0, p0, LX/Bfq;->a:LX/Bfc;

    iget-object v1, p0, LX/Bfq;->b:LX/BeU;

    invoke-virtual {v1}, LX/BeU;->getWidth()I

    move-result v1

    .line 1806654
    iget-object v2, v0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1806655
    iget-object v2, v0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v2}, LX/Bfc;->removeView(Landroid/view/View;)V

    .line 1806656
    iget-object v2, v0, LX/Bfc;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, LX/Bfc;->removeView(Landroid/view/View;)V

    .line 1806657
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, LX/Bfc;->addView(Landroid/view/View;I)V

    .line 1806658
    iput-object p1, v0, LX/Bfc;->d:Landroid/view/View;

    .line 1806659
    iget v2, v0, LX/Bfc;->f:I

    if-lez v2, :cond_2

    .line 1806660
    iget-object v2, v0, LX/Bfc;->d:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    iget v5, v0, LX/Bfc;->f:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1806661
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v2}, LX/Bfc;->d(LX/Bfc;I)V

    .line 1806662
    :cond_2
    iget-object v0, p0, LX/Bfq;->a:LX/Bfc;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
