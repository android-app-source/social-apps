.class public final LX/CQ5;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/CQ5;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CQ3;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CQ6;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1885412
    const/4 v0, 0x0

    sput-object v0, LX/CQ5;->a:LX/CQ5;

    .line 1885413
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CQ5;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1885405
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 1885406
    new-instance v0, LX/CQ6;

    invoke-direct {v0}, LX/CQ6;-><init>()V

    iput-object v0, p0, LX/CQ5;->c:LX/CQ6;

    .line 1885407
    return-void
.end method

.method public static declared-synchronized a()LX/CQ5;
    .locals 2

    .prologue
    .line 1885408
    const-class v1, LX/CQ5;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CQ5;->a:LX/CQ5;

    if-nez v0, :cond_0

    .line 1885409
    new-instance v0, LX/CQ5;

    invoke-direct {v0}, LX/CQ5;-><init>()V

    sput-object v0, LX/CQ5;->a:LX/CQ5;

    .line 1885410
    :cond_0
    sget-object v0, LX/CQ5;->a:LX/CQ5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1885411
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1885387
    check-cast p2, LX/CQ4;

    .line 1885388
    iget v0, p2, LX/CQ4;->a:I

    iget v1, p2, LX/CQ4;->b:I

    iget v2, p2, LX/CQ4;->c:I

    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 1885389
    sget-object v3, LX/CQ6;->a:LX/0Zk;

    invoke-interface {v3}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/LayerDrawable;

    .line 1885390
    if-nez v3, :cond_0

    .line 1885391
    new-instance v3, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    new-instance p0, LX/CQ7;

    invoke-direct {p0}, LX/CQ7;-><init>()V

    aput-object p0, v4, p1

    new-instance p0, LX/CPy;

    invoke-direct {p0}, LX/CPy;-><init>()V

    aput-object p0, v4, p2

    invoke-direct {v3, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v4, v3

    .line 1885392
    :goto_0
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, LX/CQ7;

    .line 1885393
    iget-object p0, v3, LX/CQ7;->a:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1885394
    iget-object p0, v3, LX/CQ7;->a:Landroid/graphics/drawable/GradientDrawable;

    int-to-float p1, v1

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1885395
    invoke-virtual {v4, p2}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, LX/CPy;

    .line 1885396
    iget-object p0, v3, LX/CPy;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Landroid/graphics/Paint;->getColor()I

    move-result p0

    if-ne p0, v2, :cond_1

    .line 1885397
    :goto_1
    iget p0, v3, LX/CPy;->c:I

    if-ne p0, v1, :cond_2

    .line 1885398
    :goto_2
    move-object v0, v4

    .line 1885399
    return-object v0

    :cond_0
    move-object v4, v3

    goto :goto_0

    .line 1885400
    :cond_1
    iget-object p0, v3, LX/CPy;->a:Landroid/graphics/Paint;

    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1885401
    invoke-virtual {v3}, LX/CPy;->invalidateSelf()V

    goto :goto_1

    .line 1885402
    :cond_2
    iput v1, v3, LX/CPy;->c:I

    .line 1885403
    const/4 p0, 0x1

    iput-boolean p0, v3, LX/CPy;->d:Z

    .line 1885404
    invoke-virtual {v3}, LX/CPy;->invalidateSelf()V

    goto :goto_2
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 0

    .prologue
    .line 1885384
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1885385
    sget-object p0, LX/CQ6;->a:LX/0Zk;

    check-cast p2, Landroid/graphics/drawable/LayerDrawable;

    invoke-interface {p0, p2}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 1885386
    return-void
.end method
