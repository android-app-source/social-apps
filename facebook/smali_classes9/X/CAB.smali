.class public LX/CAB;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/CA4;


# instance fields
.field public final a:Lcom/facebook/widget/NotificationTextSwitcher;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1855023
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1855024
    const v0, 0x7f030c64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1855025
    const v0, 0x7f0d1e7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/NotificationTextSwitcher;

    iput-object v0, p0, LX/CAB;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    .line 1855026
    iget-object v0, p0, LX/CAB;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    invoke-virtual {p0}, LX/CAB;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0810a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/NotificationTextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 1855027
    const v0, 0x7f0d1e7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CAB;->d:Landroid/view/View;

    .line 1855028
    const v0, 0x7f0d06e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CAB;->c:Landroid/widget/ImageView;

    .line 1855029
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/CAB;->b:Landroid/widget/ImageView;

    .line 1855030
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1855031
    iget-object v0, p0, LX/CAB;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1855032
    iget-object v0, p0, LX/CAB;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855033
    return-void
.end method

.method public final b(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1855034
    iget-object v0, p0, LX/CAB;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1855035
    iget-object v0, p0, LX/CAB;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855036
    return-void
.end method

.method public getOfflineHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1855037
    iget-object v0, p0, LX/CAB;->d:Landroid/view/View;

    return-object v0
.end method
