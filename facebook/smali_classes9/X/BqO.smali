.class public final LX/BqO;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/BqO;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BqN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/BqP;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824644
    const/4 v0, 0x0

    sput-object v0, LX/BqO;->a:LX/BqO;

    .line 1824645
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BqO;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1824646
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1824647
    new-instance v0, LX/BqP;

    invoke-direct {v0}, LX/BqP;-><init>()V

    iput-object v0, p0, LX/BqO;->c:LX/BqP;

    .line 1824648
    return-void
.end method

.method public static declared-synchronized q()LX/BqO;
    .locals 2

    .prologue
    .line 1824649
    const-class v1, LX/BqO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BqO;->a:LX/BqO;

    if-nez v0, :cond_0

    .line 1824650
    new-instance v0, LX/BqO;

    invoke-direct {v0}, LX/BqO;-><init>()V

    sput-object v0, LX/BqO;->a:LX/BqO;

    .line 1824651
    :cond_0
    sget-object v0, LX/BqO;->a:LX/BqO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1824652
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1824653
    check-cast p2, LX/BqM;

    .line 1824654
    iget-object v0, p2, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    const/4 p2, 0x2

    const/4 p0, 0x7

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 1824655
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->k()I

    move-result v1

    .line 1824656
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->a()I

    move-result v2

    .line 1824657
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->l()I

    move-result v3

    .line 1824658
    int-to-float v1, v1

    mul-float/2addr v1, v7

    int-to-float v4, v2

    div-float/2addr v1, v4

    .line 1824659
    int-to-float v3, v3

    mul-float/2addr v3, v7

    int-to-float v2, v2

    div-float v2, v3, v2

    .line 1824660
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6, v6}, LX/1Dh;->o(II)LX/1Dh;

    move-result-object v3

    const/16 v4, 0xa

    invoke-interface {v3, v4}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020aac

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020aad

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0, v6}, LX/1Dh;->o(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020aae

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    sub-float v1, v7, v1

    sub-float/2addr v1, v2

    invoke-interface {v4, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1824661
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1824662
    invoke-static {}, LX/1dS;->b()V

    .line 1824663
    const/4 v0, 0x0

    return-object v0
.end method
