.class public final LX/BfU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

.field public final synthetic b:I

.field public final synthetic c:LX/BfX;


# direct methods
.method public constructor <init>(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V
    .locals 0

    .prologue
    .line 1806068
    iput-object p1, p0, LX/BfU;->c:LX/BfX;

    iput-object p2, p0, LX/BfU;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iput p3, p0, LX/BfU;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 1806069
    iget-object v0, p0, LX/BfU;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setChildrenVisibility(Z)V

    .line 1806070
    if-eqz p2, :cond_0

    .line 1806071
    iget-object v0, p0, LX/BfU;->c:LX/BfX;

    iget-object v1, p0, LX/BfU;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iget v2, p0, LX/BfU;->b:I

    invoke-static {v0, v1, v2}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V

    .line 1806072
    :cond_0
    iget-object v0, p0, LX/BfU;->c:LX/BfX;

    iget-object v0, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v1, p0, LX/BfU;->b:I

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    .line 1806073
    iput-boolean p2, v0, LX/Bex;->b:Z

    .line 1806074
    iget-object v0, p0, LX/BfU;->c:LX/BfX;

    iget-object v0, v0, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806075
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1806076
    const-string v2, "extra_hours_selected_option"

    iget-object p0, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806077
    iget p1, p0, LX/BfX;->f:I

    move p0, p1

    .line 1806078
    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806079
    iget-object v2, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806080
    iget p0, v2, LX/BfX;->f:I

    move v2, p0

    .line 1806081
    if-nez v2, :cond_1

    .line 1806082
    const-string v2, "extra_hours_data"

    iget-object p0, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806083
    iget-object p1, p0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object p0, p1

    .line 1806084
    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806085
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 p0, -0x1

    invoke-virtual {v2, p0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806086
    return-void
.end method
