.class public final LX/BMp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0am;

.field public final synthetic b:LX/BMr;


# direct methods
.method public constructor <init>(LX/BMr;LX/0am;)V
    .locals 0

    .prologue
    .line 1778014
    iput-object p1, p0, LX/BMp;->b:LX/BMr;

    iput-object p2, p0, LX/BMp;->a:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1778015
    iget-object v0, p0, LX/BMp;->b:LX/BMr;

    iget-object v1, v0, LX/BMr;->b:LX/3fr;

    iget-object v0, p0, LX/BMp;->b:LX/BMr;

    iget-object v0, v0, LX/BMr;->e:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v2, LX/2RU;->FACEBOOK_FRIENDS_TYPES:LX/0Px;

    .line 1778016
    iput-object v2, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 1778017
    move-object v0, v0

    .line 1778018
    sget-object v2, LX/3Oq;->FRIENDS:LX/0Px;

    .line 1778019
    iput-object v2, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1778020
    move-object v0, v0

    .line 1778021
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 1778022
    iput-object v2, v0, LX/2RR;->n:LX/2RS;

    .line 1778023
    move-object v2, v0

    .line 1778024
    iget-object v0, p0, LX/BMp;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1778025
    iput-object v0, v2, LX/2RR;->e:Ljava/lang/String;

    .line 1778026
    move-object v0, v2

    .line 1778027
    invoke-virtual {v1, v0}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 1778028
    :try_start_0
    sget-object v0, LX/BMr;->a:LX/0QK;

    invoke-static {v1, v0}, LX/0RZ;->a(Ljava/util/Iterator;LX/0QK;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1778029
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1778030
    iget-object v2, p0, LX/BMp;->b:LX/BMr;

    iget-object v2, v2, LX/BMr;->d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v2}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1778031
    :cond_0
    invoke-interface {v1}, LX/6N1;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/6N1;->close()V

    throw v0
.end method
