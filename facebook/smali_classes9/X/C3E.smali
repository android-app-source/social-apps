.class public LX/C3E;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3F;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3E",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3F;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845243
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845244
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3E;->b:LX/0Zi;

    .line 1845245
    iput-object p1, p0, LX/C3E;->a:LX/0Ot;

    .line 1845246
    return-void
.end method

.method public static a(LX/0QB;)LX/C3E;
    .locals 4

    .prologue
    .line 1845247
    const-class v1, LX/C3E;

    monitor-enter v1

    .line 1845248
    :try_start_0
    sget-object v0, LX/C3E;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845249
    sput-object v2, LX/C3E;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845250
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845251
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845252
    new-instance v3, LX/C3E;

    const/16 p0, 0x1ebe

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3E;-><init>(LX/0Ot;)V

    .line 1845253
    move-object v0, v3

    .line 1845254
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845255
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845256
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845257
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845258
    const v0, 0x5cc087ad

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845259
    const v0, 0x5cc087ad

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1845260
    check-cast p2, LX/C3D;

    .line 1845261
    iget-object v0, p0, LX/C3E;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3F;

    iget-object v2, p2, LX/C3D;->a:LX/C33;

    iget-object v3, p2, LX/C3D;->b:LX/1Pb;

    iget v4, p2, LX/C3D;->c:I

    iget v5, p2, LX/C3D;->d:I

    move-object v1, p1

    .line 1845262
    iget-object p0, v0, LX/C3F;->d:LX/C39;

    invoke-virtual {p0, v1}, LX/C39;->c(LX/1De;)LX/C37;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/C37;->a(LX/C33;)LX/C37;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/C37;->a(LX/1Pb;)LX/C37;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/C37;->h(I)LX/C37;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-static {v1}, LX/C3E;->onClick(LX/1De;)LX/1dQ;

    move-result-object p1

    invoke-interface {p0, p1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    .line 1845263
    iget-object p1, v2, LX/C33;->b:LX/C34;

    sget-object p2, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-eq p1, p2, :cond_0

    const/4 p1, 0x4

    if-ne v5, p1, :cond_0

    .line 1845264
    invoke-interface {p0, v4}, LX/1Di;->t(I)LX/1Di;

    .line 1845265
    :cond_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {p1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/C3E;->onClick(LX/1De;)LX/1dQ;

    move-result-object p2

    invoke-interface {p1, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p1

    iget-object p2, v0, LX/C3F;->c:LX/C3I;

    invoke-virtual {p2, v1}, LX/C3I;->c(LX/1De;)LX/C3G;

    move-result-object p2

    invoke-virtual {p2, v2}, LX/C3G;->a(LX/C33;)LX/C3G;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/C3G;->a(LX/1Pb;)LX/C3G;

    move-result-object p2

    invoke-interface {p1, p2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    const/4 p1, 0x3

    const p2, 0x7f0b1d6c

    invoke-interface {p0, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1845266
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1845267
    invoke-static {}, LX/1dS;->b()V

    .line 1845268
    iget v0, p1, LX/1dQ;->b:I

    .line 1845269
    packed-switch v0, :pswitch_data_0

    .line 1845270
    :goto_0
    return-object v2

    .line 1845271
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1845272
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1845273
    check-cast v1, LX/C3D;

    .line 1845274
    iget-object p1, p0, LX/C3E;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C3F;

    iget-object p2, v1, LX/C3D;->a:LX/C33;

    .line 1845275
    iget-object p0, p1, LX/C3F;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3B;

    invoke-virtual {p0, v0, p2}, LX/C3B;->onClick(Landroid/view/View;LX/C33;)V

    .line 1845276
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5cc087ad
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C3C;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3E",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845277
    new-instance v1, LX/C3D;

    invoke-direct {v1, p0}, LX/C3D;-><init>(LX/C3E;)V

    .line 1845278
    iget-object v2, p0, LX/C3E;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3C;

    .line 1845279
    if-nez v2, :cond_0

    .line 1845280
    new-instance v2, LX/C3C;

    invoke-direct {v2, p0}, LX/C3C;-><init>(LX/C3E;)V

    .line 1845281
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3C;->a$redex0(LX/C3C;LX/1De;IILX/C3D;)V

    .line 1845282
    move-object v1, v2

    .line 1845283
    move-object v0, v1

    .line 1845284
    return-object v0
.end method
