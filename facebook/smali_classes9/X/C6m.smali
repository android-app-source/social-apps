.class public final LX/C6m;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C6n;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C6a;

.field public b:LX/C6H;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/C6n;


# direct methods
.method public constructor <init>(LX/C6n;)V
    .locals 1

    .prologue
    .line 1850242
    iput-object p1, p0, LX/C6m;->e:LX/C6n;

    .line 1850243
    move-object v0, p1

    .line 1850244
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1850245
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1850246
    const-string v0, "InlineSurveyFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1850247
    if-ne p0, p1, :cond_1

    .line 1850248
    :cond_0
    :goto_0
    return v0

    .line 1850249
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1850250
    goto :goto_0

    .line 1850251
    :cond_3
    check-cast p1, LX/C6m;

    .line 1850252
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1850253
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1850254
    if-eq v2, v3, :cond_0

    .line 1850255
    iget-object v2, p0, LX/C6m;->a:LX/C6a;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C6m;->a:LX/C6a;

    iget-object v3, p1, LX/C6m;->a:LX/C6a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1850256
    goto :goto_0

    .line 1850257
    :cond_5
    iget-object v2, p1, LX/C6m;->a:LX/C6a;

    if-nez v2, :cond_4

    .line 1850258
    :cond_6
    iget-object v2, p0, LX/C6m;->b:LX/C6H;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C6m;->b:LX/C6H;

    iget-object v3, p1, LX/C6m;->b:LX/C6H;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1850259
    goto :goto_0

    .line 1850260
    :cond_8
    iget-object v2, p1, LX/C6m;->b:LX/C6H;

    if-nez v2, :cond_7

    .line 1850261
    :cond_9
    iget-object v2, p0, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1850262
    goto :goto_0

    .line 1850263
    :cond_b
    iget-object v2, p1, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_a

    .line 1850264
    :cond_c
    iget-object v2, p0, LX/C6m;->d:LX/1Pq;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/C6m;->d:LX/1Pq;

    iget-object v3, p1, LX/C6m;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1850265
    goto :goto_0

    .line 1850266
    :cond_d
    iget-object v2, p1, LX/C6m;->d:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
