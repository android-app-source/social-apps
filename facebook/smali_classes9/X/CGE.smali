.class public LX/CGE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/FrameLayout;

.field public final b:LX/CFp;

.field public final c:LX/CGC;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/greetingcards/verve/model/VMView;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;LX/CFp;)V
    .locals 2

    .prologue
    .line 1865097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865098
    iput-object p1, p0, LX/CGE;->a:Landroid/widget/FrameLayout;

    .line 1865099
    iput-object p2, p0, LX/CGE;->b:LX/CFp;

    .line 1865100
    new-instance v0, LX/CGC;

    invoke-direct {v0}, LX/CGC;-><init>()V

    iput-object v0, p0, LX/CGE;->c:LX/CGC;

    .line 1865101
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/CGE;->d:Ljava/util/Set;

    .line 1865102
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CGE;->e:LX/0YU;

    .line 1865103
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CGE;->f:LX/0YU;

    .line 1865104
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CGE;->g:LX/0YU;

    .line 1865105
    iget-object v0, p0, LX/CGE;->c:LX/CGC;

    iget-object v1, p0, LX/CGE;->g:LX/0YU;

    .line 1865106
    iput-object v1, v0, LX/CGC;->a:LX/0YU;

    .line 1865107
    return-void
.end method

.method public static a(LX/CGE;LX/0Px;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1865090
    iget-object v0, p0, LX/CGE;->e:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1865091
    :goto_0
    return-void

    .line 1865092
    :cond_0
    iget-object v0, p0, LX/CGE;->e:LX/0YU;

    invoke-virtual {v0, p2, p1}, LX/0YU;->b(ILjava/lang/Object;)V

    .line 1865093
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865094
    iget-object v3, p0, LX/CGE;->f:LX/0YU;

    iget v4, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v3, v4, v0}, LX/0YU;->b(ILjava/lang/Object;)V

    .line 1865095
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1865096
    :cond_1
    iget-object v0, p0, LX/CGE;->b:LX/CFp;

    iget-object v1, p0, LX/CGE;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/CGE;->g:LX/0YU;

    invoke-static {p1, p2, v0, v1, v2}, LX/CGN;->a(LX/0Px;ILX/CFp;Landroid/content/Context;LX/0YU;)V

    goto :goto_0
.end method

.method public static b(LX/CGE;I)V
    .locals 2

    .prologue
    .line 1865087
    iget-object v0, p0, LX/CGE;->d:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1865088
    iget-object v1, p0, LX/CGE;->a:Landroid/widget/FrameLayout;

    iget-object v0, p0, LX/CGE;->g:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1865089
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1865068
    iget-object v0, p0, LX/CGE;->h:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v2, v0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865069
    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {p0, v0}, LX/CGE;->b(LX/CGE;I)V

    .line 1865070
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1865071
    :cond_0
    return-void
.end method

.method public final a(IILcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1865072
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    if-eqz v1, :cond_0

    .line 1865073
    iget-object v1, p3, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-static {p0, v1, p1}, LX/CGE;->a(LX/CGE;LX/0Px;I)V

    .line 1865074
    :cond_0
    if-eqz p4, :cond_1

    iget-object v1, p4, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    if-eqz v1, :cond_1

    .line 1865075
    iget-object v1, p4, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-static {p0, v1, p2}, LX/CGE;->a(LX/CGE;LX/0Px;I)V

    .line 1865076
    :cond_1
    if-nez p3, :cond_2

    move-object v1, v0

    :goto_0
    if-nez p4, :cond_3

    :goto_1
    const/4 v2, 0x1

    invoke-static {p1, p2, v1, v0, v2}, LX/CFu;->a(IILX/0Px;LX/0Px;Z)LX/CFt;

    move-result-object v3

    .line 1865077
    iget-object v4, v3, LX/CFt;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFr;

    .line 1865078
    sget-object v1, LX/CGD;->a:[I

    iget-object v6, v0, LX/CFr;->a:LX/CFs;

    invoke-virtual {v6}, LX/CFs;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    .line 1865079
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1865080
    :cond_2
    iget-object v1, p3, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    goto :goto_0

    :cond_3
    iget-object v0, p4, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    goto :goto_1

    .line 1865081
    :pswitch_0
    iget-object v1, p0, LX/CGE;->g:LX/0YU;

    iget-object v6, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v6, v6, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v6, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-static {v1, v6}, LX/CGS;->b(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865082
    iget-object v0, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {p0, v0}, LX/CGE;->b(LX/CGE;I)V

    goto :goto_3

    .line 1865083
    :pswitch_1
    iget-object v1, p0, LX/CGE;->a:Landroid/widget/FrameLayout;

    iget-object v6, p0, LX/CGE;->g:LX/0YU;

    iget-object v0, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v6, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    goto :goto_3

    .line 1865084
    :cond_4
    iget-object v0, p0, LX/CGE;->c:LX/CGC;

    .line 1865085
    iput-object v3, v0, LX/CGC;->b:LX/CFt;

    .line 1865086
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
