.class public final LX/AzU;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

.field private final m:Lcom/facebook/drawee/view/DraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1732548
    iput-object p1, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    .line 1732549
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1732550
    const v0, 0x7f0d0b88

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    .line 1732551
    return-void
.end method

.method public static a$redex0(LX/AzU;Landroid/net/Uri;I)V
    .locals 6
    .param p0    # LX/AzU;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732552
    new-instance v0, LX/1Uo;

    iget-object v1, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1732553
    iget-object v1, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->d:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    .line 1732554
    if-eqz p1, :cond_0

    .line 1732555
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    const/4 v3, 0x1

    .line 1732556
    iput-boolean v3, v2, LX/1bX;->g:Z

    .line 1732557
    move-object v2, v2

    .line 1732558
    new-instance v3, LX/1o9;

    iget-object v4, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget v4, v4, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    iget-object v5, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget v5, v5, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    invoke-direct {v3, v4, v5}, LX/1o9;-><init>(II)V

    .line 1732559
    iput-object v3, v2, LX/1bX;->c:LX/1o9;

    .line 1732560
    move-object v2, v2

    .line 1732561
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1732562
    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 1732563
    iget-object v2, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1732564
    :goto_0
    iget-object v2, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v3, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget v3, v3, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1732565
    iget-object v2, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v3, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget v3, v3, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1732566
    iget-object v2, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1732567
    iget-object v0, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1732568
    iget-object v0, p0, LX/AzU;->m:Lcom/facebook/drawee/view/DraweeView;

    new-instance v1, LX/AzT;

    invoke-direct {v1, p0, p1, p2}, LX/AzT;-><init>(LX/AzU;Landroid/net/Uri;I)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732569
    return-void

    .line 1732570
    :cond_0
    iget-object v2, p0, LX/AzU;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->i:Landroid/graphics/drawable/Drawable;

    sget-object v3, LX/1Up;->e:LX/1Up;

    invoke-virtual {v0, v2, v3}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    goto :goto_0
.end method
