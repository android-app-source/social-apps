.class public final LX/Bpn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)V
    .locals 0

    .prologue
    .line 1823556
    iput-object p1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iput p2, p0, LX/Bpn;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1823557
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->W:LX/BpG;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1823558
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->W:LX/BpG;

    invoke-virtual {v0}, LX/BpG;->b()LX/0Px;

    move-result-object v0

    .line 1823559
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1}, LX/0g8;->B()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1823560
    :cond_0
    :goto_0
    return-void

    .line 1823561
    :cond_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1823562
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->s:LX/03V;

    const-string v2, "PhotosFeedFragment_emptyMediaMetadataList"

    const-string v3, "GraphQLStory Id is %s"

    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823563
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1823564
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823565
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1823566
    :cond_2
    const-string v0, "unknown"

    goto :goto_1

    .line 1823567
    :cond_3
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget v1, p0, LX/Bpn;->a:I

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)LX/5kD;

    move-result-object v0

    .line 1823568
    if-nez v0, :cond_4

    .line 1823569
    iget-object v0, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1823570
    :cond_4
    invoke-static {v0}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1823571
    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {v1, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v0

    .line 1823572
    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1823573
    iget-object v1, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v2, p0, LX/Bpn;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget v3, p0, LX/Bpn;->a:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0g8;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
