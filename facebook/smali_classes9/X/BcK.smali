.class public LX/BcK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/BcO;

.field public c:LX/BcO;

.field public d:LX/BcJ;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1801776
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/BcK;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1801777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801778
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BcK;->e:Ljava/util/List;

    .line 1801779
    return-void
.end method

.method public static a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)LX/BcJ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcO;",
            "LX/BcO;",
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;)",
            "LX/BcJ;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1801780
    if-nez p1, :cond_0

    move v2, v1

    .line 1801781
    :goto_0
    if-nez p2, :cond_1

    .line 1801782
    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 1801783
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Both currentRoot and newRoot are null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    .line 1801784
    goto :goto_0

    :cond_1
    move v1, v0

    .line 1801785
    goto :goto_1

    .line 1801786
    :cond_2
    if-eqz v1, :cond_4

    .line 1801787
    iget v1, p1, LX/BcO;->g:I

    move v3, v1

    .line 1801788
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1801789
    iget v1, p1, LX/BcO;->g:I

    move v1, v1

    .line 1801790
    invoke-static {v1}, LX/BcJ;->c(I)LX/BcJ;

    move-result-object v1

    move v2, v0

    .line 1801791
    :goto_2
    if-ge v2, v3, :cond_3

    .line 1801792
    invoke-static {v0}, LX/BcI;->a(I)LX/BcI;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/BcJ;->a(LX/BcI;)V

    .line 1801793
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 1801794
    :goto_3
    return-object v0

    .line 1801795
    :cond_4
    iget-object v1, p2, LX/BcO;->i:LX/BcS;

    move-object v1, v1

    .line 1801796
    invoke-static {p1, p2}, LX/BcS;->c(LX/BcO;LX/BcO;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1801797
    iget v0, p1, LX/BcO;->g:I

    move v0, v0

    .line 1801798
    invoke-static {v0}, LX/BcJ;->c(I)LX/BcJ;

    move-result-object v0

    .line 1801799
    iget v1, v0, LX/BcJ;->b:I

    move v1, v1

    .line 1801800
    iput v1, p2, LX/BcO;->g:I

    .line 1801801
    goto :goto_3

    .line 1801802
    :cond_5
    invoke-virtual {v1}, LX/BcS;->b()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1801803
    if-eqz v2, :cond_6

    :goto_4
    invoke-static {v0}, LX/BcJ;->c(I)LX/BcJ;

    move-result-object v0

    .line 1801804
    iget-object v2, p2, LX/BcO;->d:LX/BcP;

    move-object v2, v2

    .line 1801805
    invoke-virtual {v1, v2, v0, p1, p2}, LX/BcS;->a(LX/BcP;LX/BcJ;LX/BcO;LX/BcO;)V

    .line 1801806
    iget v1, v0, LX/BcJ;->b:I

    move v1, v1

    .line 1801807
    iput v1, p2, LX/BcO;->g:I

    .line 1801808
    goto :goto_3

    .line 1801809
    :cond_6
    iget v0, p1, LX/BcO;->g:I

    move v0, v0

    .line 1801810
    goto :goto_4

    .line 1801811
    :cond_7
    invoke-static {p0, p1, p2, p3}, LX/BcK;->b(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v3

    .line 1801812
    invoke-static {}, LX/BcJ;->c()LX/BcJ;

    move-result-object v1

    .line 1801813
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v0

    :goto_5
    if-ge v2, v4, :cond_9

    .line 1801814
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcJ;

    .line 1801815
    invoke-static {v1, v0}, LX/BcJ;->a(LX/BcJ;LX/BcJ;)LX/BcJ;

    move-result-object v1

    .line 1801816
    if-eqz v0, :cond_8

    .line 1801817
    invoke-virtual {v0}, LX/BcJ;->d()V

    .line 1801818
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1801819
    :cond_9
    iget v0, v1, LX/BcJ;->b:I

    move v0, v0

    .line 1801820
    iput v0, p2, LX/BcO;->g:I

    .line 1801821
    move-object v0, v1

    .line 1801822
    goto :goto_3
.end method

.method private static b(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)Landroid/util/SparseArray;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcO;",
            "LX/BcO;",
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;)",
            "Landroid/util/SparseArray",
            "<",
            "LX/BcJ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1801823
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    move-object v6, v0

    .line 1801824
    invoke-static {p1}, LX/BcO;->b(LX/BcO;)Ljava/util/Map;

    move-result-object v7

    .line 1801825
    invoke-static {p2}, LX/BcO;->b(LX/BcO;)Ljava/util/Map;

    move-result-object v4

    .line 1801826
    if-nez p1, :cond_1

    sget-object v0, LX/BcK;->a:Ljava/util/List;

    move-object v3, v0

    .line 1801827
    :goto_0
    iget-object v0, p2, LX/BcO;->h:Ljava/util/List;

    move-object v8, v0

    .line 1801828
    move v1, v2

    .line 1801829
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1801830
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801831
    iget-object v5, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v5, v5

    .line 1801832
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801833
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    .line 1801834
    invoke-static {p0, v0, v9, p3}, LX/BcK;->a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)LX/BcJ;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1801835
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1801836
    :cond_1
    iget-object v0, p1, LX/BcO;->h:Ljava/util/List;

    move-object v0, v0

    .line 1801837
    move-object v3, v0

    goto :goto_0

    :cond_2
    move v4, v2

    .line 1801838
    :goto_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 1801839
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801840
    iget-object v1, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1801841
    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcO;

    .line 1801842
    if-nez v1, :cond_4

    const/4 v5, -0x1

    .line 1801843
    :goto_3
    if-gez v5, :cond_5

    .line 1801844
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcJ;

    .line 1801845
    invoke-static {p0, v9, v0, p3}, LX/BcK;->a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)LX/BcJ;

    move-result-object v0

    .line 1801846
    invoke-static {v1, v0}, LX/BcJ;->a(LX/BcJ;LX/BcJ;)LX/BcJ;

    move-result-object v5

    invoke-virtual {v6, v2, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1801847
    if-eqz v1, :cond_3

    .line 1801848
    invoke-virtual {v1}, LX/BcJ;->d()V

    .line 1801849
    :cond_3
    invoke-virtual {v0}, LX/BcJ;->d()V

    move v0, v2

    .line 1801850
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_2

    .line 1801851
    :cond_4
    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    goto :goto_3

    .line 1801852
    :cond_5
    if-lt v2, v5, :cond_6

    if-eqz v2, :cond_6

    .line 1801853
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Re-ordering of Sections is not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1801854
    :cond_6
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcJ;

    .line 1801855
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BcO;

    invoke-static {p0, v2, v0, p3}, LX/BcK;->a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)LX/BcJ;

    move-result-object v0

    .line 1801856
    invoke-static {v1, v0}, LX/BcJ;->a(LX/BcJ;LX/BcJ;)LX/BcJ;

    move-result-object v2

    invoke-virtual {v6, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1801857
    if-eqz v1, :cond_7

    .line 1801858
    invoke-virtual {v1}, LX/BcJ;->d()V

    .line 1801859
    :cond_7
    invoke-virtual {v0}, LX/BcJ;->d()V

    move v0, v5

    goto :goto_4

    .line 1801860
    :cond_8
    return-object v6
.end method
