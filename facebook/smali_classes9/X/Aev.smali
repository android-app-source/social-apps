.class public LX/Aev;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static k:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0wM;

.field public final d:LX/Af2;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AfD;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0hs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/AeS;

.field public h:Z

.field private i:LX/AcX;

.field public j:LX/AfJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1698457
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "broadcast_comment_pinning_tool_tip_has_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Aev;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0wM;LX/Af2;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0wM;",
            "LX/Af2;",
            "LX/0Ot",
            "<",
            "LX/AfD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1698404
    iput-object p1, p0, LX/Aev;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1698405
    iput-object p2, p0, LX/Aev;->c:LX/0wM;

    .line 1698406
    iput-object p3, p0, LX/Aev;->d:LX/Af2;

    .line 1698407
    iput-object p4, p0, LX/Aev;->e:LX/0Ot;

    .line 1698408
    return-void
.end method

.method public static a(LX/0QB;)LX/Aev;
    .locals 7

    .prologue
    .line 1698443
    const-class v1, LX/Aev;

    monitor-enter v1

    .line 1698444
    :try_start_0
    sget-object v0, LX/Aev;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1698445
    sput-object v2, LX/Aev;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1698446
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698447
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1698448
    new-instance v6, LX/Aev;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    .line 1698449
    new-instance p0, LX/Af2;

    invoke-static {v0}, LX/3id;->b(LX/0QB;)LX/3id;

    move-result-object v5

    check-cast v5, LX/3id;

    invoke-direct {p0, v5}, LX/Af2;-><init>(LX/3id;)V

    .line 1698450
    move-object v5, p0

    .line 1698451
    check-cast v5, LX/Af2;

    const/16 p0, 0x1c0c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/Aev;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0wM;LX/Af2;LX/0Ot;)V

    .line 1698452
    move-object v0, v6

    .line 1698453
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1698454
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Aev;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1698455
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1698456
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/AcX;)V
    .locals 0

    .prologue
    .line 1698441
    iput-object p1, p0, LX/Aev;->i:LX/AcX;

    .line 1698442
    return-void
.end method

.method public final a(LX/AeS;)V
    .locals 0

    .prologue
    .line 1698439
    iput-object p1, p0, LX/Aev;->g:LX/AeS;

    .line 1698440
    return-void
.end method

.method public final a(LX/Aeu;)V
    .locals 5

    .prologue
    .line 1698427
    iget-object v0, p0, LX/Aev;->d:LX/Af2;

    .line 1698428
    iget-object v1, p1, LX/Aeu;->d:Ljava/lang/String;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1698429
    iget-object v1, p1, LX/Aeu;->e:Ljava/lang/String;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1698430
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    iget-object v2, p1, LX/Aeu;->d:Ljava/lang/String;

    .line 1698431
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1698432
    move-object v1, v1

    .line 1698433
    iget-object v2, p1, LX/Aeu;->e:Ljava/lang/String;

    .line 1698434
    iput-object v2, v1, LX/3dM;->D:Ljava/lang/String;

    .line 1698435
    move-object v1, v1

    .line 1698436
    iget-boolean v2, p1, LX/Aeu;->g:Z

    invoke-virtual {v1, v2}, LX/3dM;->j(Z)LX/3dM;

    move-result-object v1

    iget-boolean v2, p1, LX/Aeu;->h:Z

    invoke-virtual {v1, v2}, LX/3dM;->g(Z)LX/3dM;

    move-result-object v1

    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1698437
    iget-object v2, v0, LX/Af2;->b:LX/3id;

    iget-object v3, v0, LX/Af2;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    const/4 v4, 0x0

    new-instance p0, LX/Af1;

    invoke-direct {p0, v0, p1}, LX/Af1;-><init>(LX/Af2;LX/Aeu;)V

    invoke-virtual {v2, v1, v3, v4, p0}, LX/3id;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    .line 1698438
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 1698415
    iget-object v3, p0, LX/Aev;->g:LX/AeS;

    .line 1698416
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1698417
    iget-object v0, v3, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeO;

    move-object v1, v0

    .line 1698418
    invoke-interface {v1}, LX/AeO;->a()LX/AeN;

    move-result-object v0

    sget-object v4, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne v0, v4, :cond_0

    move-object v0, v1

    check-cast v0, LX/Aeu;

    iget-object v0, v0, LX/AeP;->a:LX/AcC;

    iget-object v0, v0, LX/AcC;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698419
    check-cast v1, LX/Aeu;

    iput-boolean p2, v1, LX/Aeu;->o:Z

    .line 1698420
    invoke-virtual {v3, v2}, LX/1OM;->i_(I)V

    .line 1698421
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1698422
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, LX/Aev;->i:LX/AcX;

    if-eqz v0, :cond_2

    .line 1698423
    iget-object v0, p0, LX/Aev;->i:LX/AcX;

    .line 1698424
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    if-eqz v1, :cond_2

    .line 1698425
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    invoke-interface {v1, p1}, LX/AeJ;->d(Ljava/lang/String;)V

    .line 1698426
    :cond_2
    return-void
.end method

.method public final b(LX/Aeu;)V
    .locals 2

    .prologue
    .line 1698409
    iget-object v0, p0, LX/Aev;->g:LX/AeS;

    .line 1698410
    iget-object v1, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1698411
    if-ltz v1, :cond_0

    .line 1698412
    iget-object p0, v0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1698413
    invoke-virtual {v0, v1}, LX/1OM;->d(I)V

    .line 1698414
    :cond_0
    return-void
.end method
