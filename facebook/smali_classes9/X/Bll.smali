.class public final LX/Bll;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;)V
    .locals 0

    .prologue
    .line 1816525
    iput-object p1, p0, LX/Bll;->a:Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x39a13afc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1816526
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1816527
    const-string v2, "extra_poll_times"

    iget-object v3, p0, LX/Bll;->a:Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    iget-object v3, v3, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1816528
    iget-object v2, p0, LX/Bll;->a:Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 1816529
    iget-object v1, p0, LX/Bll;->a:Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    invoke-virtual {v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->finish()V

    .line 1816530
    const v1, 0x63e1fe2c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
