.class public final LX/CNQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNM;


# instance fields
.field public final synthetic a:LX/CNb;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/CNS;


# direct methods
.method public constructor <init>(LX/CNS;LX/CNb;LX/0Px;)V
    .locals 0

    .prologue
    .line 1881608
    iput-object p1, p0, LX/CNQ;->c:LX/CNS;

    iput-object p2, p0, LX/CNQ;->a:LX/CNb;

    iput-object p3, p0, LX/CNQ;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1881609
    iget-object v0, p0, LX/CNQ;->a:LX/CNb;

    invoke-virtual {p1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1881610
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1881611
    if-lez v0, :cond_0

    .line 1881612
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881613
    :cond_0
    iget-object v2, p0, LX/CNQ;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881614
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 1881615
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881616
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
