.class public final LX/Apz;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Apz;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Apx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Aq5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717146
    const/4 v0, 0x0

    sput-object v0, LX/Apz;->a:LX/Apz;

    .line 1717147
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apz;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1717143
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717144
    new-instance v0, LX/Aq5;

    invoke-direct {v0}, LX/Aq5;-><init>()V

    iput-object v0, p0, LX/Apz;->c:LX/Aq5;

    .line 1717145
    return-void
.end method

.method public static declared-synchronized q()LX/Apz;
    .locals 2

    .prologue
    .line 1717139
    const-class v1, LX/Apz;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Apz;->a:LX/Apz;

    if-nez v0, :cond_0

    .line 1717140
    new-instance v0, LX/Apz;

    invoke-direct {v0}, LX/Apz;-><init>()V

    sput-object v0, LX/Apz;->a:LX/Apz;

    .line 1717141
    :cond_0
    sget-object v0, LX/Apz;->a:LX/Apz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1717142
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717137
    invoke-static {}, LX/1dS;->b()V

    .line 1717138
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3a113fcd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1717128
    sget-object v1, LX/Aq5;->a:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    sget-object v1, LX/Aq5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1717129
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eq v2, p1, :cond_1

    .line 1717130
    :cond_0
    new-instance v1, Landroid/widget/CheckBox;

    invoke-direct {v1, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 1717131
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, LX/Aq5;->a:Ljava/lang/ref/WeakReference;

    .line 1717132
    :cond_1
    invoke-static {p3}, LX/1oC;->a(I)I

    move-result v2

    invoke-static {p4}, LX/1oC;->a(I)I

    move-result p0

    invoke-virtual {v1, v2, p0}, Landroid/widget/CheckBox;->measure(II)V

    .line 1717133
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v2

    iput v2, p5, LX/1no;->a:I

    .line 1717134
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 1717135
    const/16 v1, 0x1f

    const v2, -0x29945587

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1717136
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717126
    new-instance v0, LX/Aq4;

    invoke-direct {v0, p1}, LX/Aq4;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1717127
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1717148
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1717114
    check-cast p3, LX/Apy;

    .line 1717115
    check-cast p2, LX/Aq4;

    iget-boolean v0, p3, LX/Apy;->a:Z

    iget-boolean v1, p3, LX/Apy;->b:Z

    .line 1717116
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717117
    if-nez p0, :cond_0

    .line 1717118
    const/4 p0, 0x0

    .line 1717119
    :goto_0
    move-object p0, p0

    .line 1717120
    iput-object p0, p2, LX/Aq4;->a:LX/1dQ;

    .line 1717121
    invoke-virtual {p2, v0}, LX/Aq4;->setChecked(Z)V

    .line 1717122
    invoke-virtual {p2, v1}, LX/Aq4;->setEnabled(Z)V

    .line 1717123
    return-void

    .line 1717124
    :cond_0
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717125
    check-cast p0, LX/Apy;

    iget-object p0, p0, LX/Apy;->c:LX/1dQ;

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1717113
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717099
    check-cast p2, LX/Aq4;

    const/4 p1, 0x0

    .line 1717100
    const/4 p0, 0x0

    .line 1717101
    iput-object p0, p2, LX/Aq4;->a:LX/1dQ;

    .line 1717102
    invoke-virtual {p2, p1}, LX/Aq4;->setChecked(Z)V

    .line 1717103
    invoke-virtual {p2, p1}, LX/Aq4;->setEnabled(Z)V

    .line 1717104
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717110
    check-cast p2, LX/Aq4;

    .line 1717111
    invoke-virtual {p2, p2}, LX/Aq4;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717112
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717107
    check-cast p2, LX/Aq4;

    .line 1717108
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/Aq4;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717109
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1717106
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1717105
    const/16 v0, 0xf

    return v0
.end method
