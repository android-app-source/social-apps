.class public LX/AqM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AqK;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AqO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717563
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AqM;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AqO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1717560
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717561
    iput-object p1, p0, LX/AqM;->b:LX/0Ot;

    .line 1717562
    return-void
.end method

.method public static a(LX/0QB;)LX/AqM;
    .locals 4

    .prologue
    .line 1717549
    const-class v1, LX/AqM;

    monitor-enter v1

    .line 1717550
    :try_start_0
    sget-object v0, LX/AqM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1717551
    sput-object v2, LX/AqM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1717552
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1717553
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1717554
    new-instance v3, LX/AqM;

    const/16 p0, 0x221b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/AqM;-><init>(LX/0Ot;)V

    .line 1717555
    move-object v0, v3

    .line 1717556
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1717557
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AqM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1717558
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1717559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1717537
    check-cast p2, LX/AqL;

    .line 1717538
    iget-object v0, p0, LX/AqM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AqO;

    iget v2, p2, LX/AqL;->a:I

    iget v3, p2, LX/AqL;->b:I

    iget v4, p2, LX/AqL;->c:I

    iget-boolean v5, p2, LX/AqL;->d:Z

    move-object v1, p1

    .line 1717539
    new-instance v6, LX/AqN;

    invoke-direct {v6, v1, v2}, LX/AqN;-><init>(LX/1De;I)V

    .line 1717540
    iget-object v7, v0, LX/AqO;->a:LX/Apv;

    invoke-virtual {v7, v1}, LX/Apv;->c(LX/1De;)LX/Aps;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/Aps;->h(I)LX/Aps;

    move-result-object v7

    if-nez v4, :cond_0

    :goto_0
    invoke-virtual {v7, v3}, LX/Aps;->i(I)LX/Aps;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/Aps;->a(Z)LX/Aps;

    move-result-object v7

    .line 1717541
    iget-object p0, v1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717542
    if-nez p0, :cond_1

    .line 1717543
    const/4 p0, 0x0

    .line 1717544
    :goto_1
    move-object p0, p0

    .line 1717545
    invoke-virtual {v7, p0}, LX/Aps;->a(LX/1dQ;)LX/Aps;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/16 p0, 0x8

    iget p1, v6, LX/AqN;->a:I

    iget p2, v6, LX/AqN;->b:I

    sub-int/2addr p1, p2

    shr-int/lit8 p1, p1, 0x1

    invoke-interface {v7, p0, p1}, LX/1Di;->e(II)LX/1Di;

    move-result-object v7

    iget v6, v6, LX/AqN;->c:I

    invoke-interface {v7, v6}, LX/1Di;->x(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1717546
    return-object v0

    :cond_0
    move v3, v4

    goto :goto_0

    .line 1717547
    :cond_1
    iget-object p0, v1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717548
    check-cast p0, LX/AqL;

    iget-object p0, p0, LX/AqL;->e:LX/1dQ;

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717535
    invoke-static {}, LX/1dS;->b()V

    .line 1717536
    const/4 v0, 0x0

    return-object v0
.end method
