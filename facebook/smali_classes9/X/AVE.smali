.class public final enum LX/AVE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AVE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AVE;

.field public static final enum ABOUT_TO_FINISH:LX/AVE;

.field public static final enum COPYRIGHT_VIOLATION:LX/AVE;

.field public static final enum FAILED:LX/AVE;

.field public static final enum FINISHED:LX/AVE;

.field public static final enum IMPORT_FROM_GALLERY:LX/AVE;

.field public static final enum INTERRUPTED:LX/AVE;

.field public static final enum NETWORK_FAILURE:LX/AVE;

.field public static final enum OFFLINE:LX/AVE;

.field public static final enum RECORDING:LX/AVE;

.field public static final enum SEAL_BROADCAST_REQUEST:LX/AVE;

.field public static final enum SHOW_END_SCREEN:LX/AVE;

.field public static final enum STARTING:LX/AVE;

.field public static final enum UNINITIALIZED:LX/AVE;


# instance fields
.field public final loggerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1679062
    new-instance v0, LX/AVE;

    const-string v1, "OFFLINE"

    const-string v2, "offline"

    invoke-direct {v0, v1, v4, v2}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->OFFLINE:LX/AVE;

    .line 1679063
    new-instance v0, LX/AVE;

    const-string v1, "IMPORT_FROM_GALLERY"

    const-string v2, "import_from_gallery"

    invoke-direct {v0, v1, v5, v2}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->IMPORT_FROM_GALLERY:LX/AVE;

    .line 1679064
    new-instance v0, LX/AVE;

    const-string v1, "UNINITIALIZED"

    const-string v2, "uninitialized"

    invoke-direct {v0, v1, v6, v2}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->UNINITIALIZED:LX/AVE;

    .line 1679065
    new-instance v0, LX/AVE;

    const-string v1, "STARTING"

    const-string v2, "starting"

    invoke-direct {v0, v1, v7, v2}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->STARTING:LX/AVE;

    .line 1679066
    new-instance v0, LX/AVE;

    const-string v1, "FAILED"

    const-string v2, "failed"

    invoke-direct {v0, v1, v8, v2}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->FAILED:LX/AVE;

    .line 1679067
    new-instance v0, LX/AVE;

    const-string v1, "RECORDING"

    const/4 v2, 0x5

    const-string v3, "recording"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->RECORDING:LX/AVE;

    .line 1679068
    new-instance v0, LX/AVE;

    const-string v1, "ABOUT_TO_FINISH"

    const/4 v2, 0x6

    const-string v3, "about_to_finish"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->ABOUT_TO_FINISH:LX/AVE;

    .line 1679069
    new-instance v0, LX/AVE;

    const-string v1, "FINISHED"

    const/4 v2, 0x7

    const-string v3, "finished"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->FINISHED:LX/AVE;

    .line 1679070
    new-instance v0, LX/AVE;

    const-string v1, "SHOW_END_SCREEN"

    const/16 v2, 0x8

    const-string v3, "show_end_screen"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->SHOW_END_SCREEN:LX/AVE;

    .line 1679071
    new-instance v0, LX/AVE;

    const-string v1, "INTERRUPTED"

    const/16 v2, 0x9

    const-string v3, "interrupted"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->INTERRUPTED:LX/AVE;

    .line 1679072
    new-instance v0, LX/AVE;

    const-string v1, "COPYRIGHT_VIOLATION"

    const/16 v2, 0xa

    const-string v3, "copyright_violation"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->COPYRIGHT_VIOLATION:LX/AVE;

    .line 1679073
    new-instance v0, LX/AVE;

    const-string v1, "SEAL_BROADCAST_REQUEST"

    const/16 v2, 0xb

    const-string v3, "seal_broadcast_request"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->SEAL_BROADCAST_REQUEST:LX/AVE;

    .line 1679074
    new-instance v0, LX/AVE;

    const-string v1, "NETWORK_FAILURE"

    const/16 v2, 0xc

    const-string v3, "network_failure"

    invoke-direct {v0, v1, v2, v3}, LX/AVE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    .line 1679075
    const/16 v0, 0xd

    new-array v0, v0, [LX/AVE;

    sget-object v1, LX/AVE;->OFFLINE:LX/AVE;

    aput-object v1, v0, v4

    sget-object v1, LX/AVE;->IMPORT_FROM_GALLERY:LX/AVE;

    aput-object v1, v0, v5

    sget-object v1, LX/AVE;->UNINITIALIZED:LX/AVE;

    aput-object v1, v0, v6

    sget-object v1, LX/AVE;->STARTING:LX/AVE;

    aput-object v1, v0, v7

    sget-object v1, LX/AVE;->FAILED:LX/AVE;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/AVE;->RECORDING:LX/AVE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AVE;->ABOUT_TO_FINISH:LX/AVE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AVE;->FINISHED:LX/AVE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/AVE;->SHOW_END_SCREEN:LX/AVE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/AVE;->INTERRUPTED:LX/AVE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/AVE;->COPYRIGHT_VIOLATION:LX/AVE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/AVE;->SEAL_BROADCAST_REQUEST:LX/AVE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/AVE;->NETWORK_FAILURE:LX/AVE;

    aput-object v2, v0, v1

    sput-object v0, LX/AVE;->$VALUES:[LX/AVE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1679076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1679077
    iput-object p3, p0, LX/AVE;->loggerName:Ljava/lang/String;

    .line 1679078
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AVE;
    .locals 1

    .prologue
    .line 1679079
    const-class v0, LX/AVE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AVE;

    return-object v0
.end method

.method public static values()[LX/AVE;
    .locals 1

    .prologue
    .line 1679080
    sget-object v0, LX/AVE;->$VALUES:[LX/AVE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AVE;

    return-object v0
.end method
