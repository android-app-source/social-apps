.class public LX/C6D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/C6C;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Rg;

.field private final b:I


# direct methods
.method public constructor <init>(LX/1Rg;I)V
    .locals 0

    .prologue
    .line 1849802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849803
    iput-object p1, p0, LX/C6D;->a:LX/1Rg;

    .line 1849804
    iput p2, p0, LX/C6D;->b:I

    .line 1849805
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1849793
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 8

    .prologue
    .line 1849794
    check-cast p3, LX/C6C;

    .line 1849795
    iget-object v0, p3, LX/C6C;->a:LX/C6H;

    .line 1849796
    iget-boolean v1, v0, LX/C6H;->b:Z

    move v0, v1

    .line 1849797
    if-nez v0, :cond_0

    .line 1849798
    iget-object v4, p0, LX/C6D;->a:LX/1Rg;

    const-wide/16 v6, 0x1770

    const/4 v5, 0x0

    invoke-virtual {v4, v6, v7, v5}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 1849799
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849800
    const/16 v0, 0x12c

    iget v1, p0, LX/C6D;->b:I

    iget-object v2, p0, LX/C6D;->a:LX/1Rg;

    iget-object v3, p3, LX/C6C;->b:LX/3Ii;

    invoke-static {p4, v0, v1, v2, v3}, LX/C6J;->a(Landroid/view/View;IILX/1Rg;LX/3Ii;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849801
    :cond_0
    return-void
.end method
