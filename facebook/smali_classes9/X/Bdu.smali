.class public LX/Bdu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# static fields
.field public static final a:LX/1Of;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1804363
    new-instance v0, LX/1Oe;

    invoke-direct {v0}, LX/1Oe;-><init>()V

    sput-object v0, LX/Bdu;->a:LX/1Of;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1804361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804362
    return-void
.end method

.method public static a(LX/1De;LX/BcO;LX/Bdb;LX/1X1;LX/1X1;LX/1X1;LX/1OX;LX/BcG;ZILX/3x6;LX/5K7;LX/1Of;ZLX/BcL;ZLX/Bdv;LX/BcY;ZLX/25S;)LX/1Dg;
    .locals 8
    .param p1    # LX/BcO;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/Bdb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1OX;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/BcG;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/3x6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/5K7;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # LX/1Of;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/BcO",
            "<*>;",
            "LX/Bdb;",
            "LX/1X1",
            "<*>;",
            "LX/1X1",
            "<*>;",
            "LX/1X1",
            "<*>;",
            "LX/1OX;",
            "LX/BcG;",
            "ZI",
            "LX/3x6;",
            "LX/5K7;",
            "LX/1Of;",
            "Z",
            "LX/BcL;",
            "Z",
            "LX/Bdv;",
            "LX/BcY;",
            "Z",
            "LX/25S;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1804328
    move-object/from16 v0, p16

    invoke-virtual {v0, p2, p0}, LX/Bdv;->a(LX/Bdb;LX/1De;)V

    .line 1804329
    new-instance v1, LX/Bds;

    invoke-direct {v1, p7, p0}, LX/Bds;-><init>(LX/BcG;LX/1De;)V

    move-object/from16 v0, p17

    invoke-virtual {v0, v1}, LX/BcY;->a(LX/BcG;)V

    .line 1804330
    move-object/from16 v0, p17

    invoke-virtual {v0, p1}, LX/BcY;->a(LX/BcO;)V

    .line 1804331
    invoke-static/range {p14 .. p15}, LX/Bdu;->a(LX/BcL;Z)Z

    move-result v3

    .line 1804332
    invoke-static/range {p14 .. p15}, LX/Bdu;->b(LX/BcL;Z)Z

    move-result v4

    .line 1804333
    invoke-static/range {p14 .. p15}, LX/Bdu;->c(LX/BcL;Z)Z

    move-result v5

    .line 1804334
    if-eqz v5, :cond_1

    if-nez p5, :cond_1

    const/4 v1, 0x1

    move v2, v1

    .line 1804335
    :goto_0
    if-eqz v4, :cond_2

    if-nez p4, :cond_2

    const/4 v1, 0x1

    .line 1804336
    :goto_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 1804337
    :goto_2
    if-eqz v1, :cond_4

    .line 1804338
    const/4 v1, 0x0

    .line 1804339
    :goto_3
    return-object v1

    .line 1804340
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    .line 1804341
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1804342
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 1804343
    :cond_4
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    .line 1804344
    if-nez p19, :cond_8

    .line 1804345
    invoke-static {p0}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, LX/5Jz;->b(Z)LX/5Jz;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, LX/5Jz;->h(I)LX/5Jz;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/5Jz;->a(LX/5K7;)LX/5Jz;

    move-result-object v1

    move/from16 v0, p18

    invoke-virtual {v1, v0}, LX/5Jz;->a(Z)LX/5Jz;

    move-result-object v6

    if-eqz p13, :cond_7

    const/4 v1, 0x0

    :goto_4
    invoke-virtual {v6, v1}, LX/5Jz;->a(LX/1dQ;)LX/5Jz;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/5Jz;->a(LX/3x6;)LX/5Jz;

    move-result-object v1

    new-instance v6, LX/Bdt;

    move-object/from16 v0, p17

    invoke-direct {v6, p6, v0}, LX/Bdt;-><init>(LX/1OX;LX/BcY;)V

    invoke-virtual {v1, v6}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object v1

    invoke-virtual/range {p16 .. p16}, LX/Bdv;->a()LX/5Je;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object v1

    .line 1804346
    sget-object v6, LX/Bdu;->a:LX/1Of;

    move-object/from16 v0, p12

    if-eq v0, v6, :cond_5

    .line 1804347
    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/5Jz;->a(LX/1Of;)LX/5Jz;

    .line 1804348
    :cond_5
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v6, 0x1

    invoke-interface {v1, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-interface {v1, v6, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1804349
    :goto_5
    if-eqz v3, :cond_a

    if-eqz p3, :cond_a

    .line 1804350
    invoke-static {p0, p3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1804351
    :cond_6
    :goto_6
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_3

    .line 1804352
    :cond_7
    move-object/from16 v0, p17

    invoke-static {p0, v0}, LX/Bdq;->a(LX/1De;LX/BcY;)LX/1dQ;

    move-result-object v1

    goto :goto_4

    .line 1804353
    :cond_8
    invoke-static {p0}, LX/5KE;->c(LX/1De;)LX/5KC;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, LX/5KC;->a(Z)LX/5KC;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, LX/5KC;->h(I)LX/5KC;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/5KC;->a(LX/5K7;)LX/5KC;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/5KC;->a(LX/3x6;)LX/5KC;

    move-result-object v1

    move-object/from16 v0, p19

    invoke-virtual {v1, v0}, LX/5KC;->a(LX/25S;)LX/5KC;

    move-result-object v1

    new-instance v6, LX/Bdt;

    move-object/from16 v0, p17

    invoke-direct {v6, p6, v0}, LX/Bdt;-><init>(LX/1OX;LX/BcY;)V

    invoke-virtual {v1, v6}, LX/5KC;->a(LX/1OX;)LX/5KC;

    move-result-object v1

    invoke-virtual/range {p16 .. p16}, LX/Bdv;->a()LX/5Je;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/5KC;->a(LX/5Je;)LX/5KC;

    move-result-object v1

    .line 1804354
    sget-object v6, LX/Bdu;->a:LX/1Of;

    move-object/from16 v0, p12

    if-eq v0, v6, :cond_9

    .line 1804355
    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/5KC;->a(LX/1Of;)LX/5KC;

    .line 1804356
    :cond_9
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v6, 0x1

    invoke-interface {v1, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-interface {v1, v6, v7}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_5

    .line 1804357
    :cond_a
    if-eqz v4, :cond_b

    .line 1804358
    invoke-static {p0, p4}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_6

    .line 1804359
    :cond_b
    if-eqz v5, :cond_6

    .line 1804360
    invoke-static {p0, p5}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto/16 :goto_6
.end method

.method private static a(LX/BcL;Z)Z
    .locals 1

    .prologue
    .line 1804364
    if-eqz p1, :cond_1

    sget-object v0, LX/BcL;->INITIAL_LOAD:LX/BcL;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/BcL;->LOADING:LX/BcL;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/BcL;Z)Z
    .locals 1

    .prologue
    .line 1804327
    if-eqz p1, :cond_0

    sget-object v0, LX/BcL;->SUCCEEDED:LX/BcL;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/BcL;Z)Z
    .locals 1

    .prologue
    .line 1804326
    if-eqz p1, :cond_0

    sget-object v0, LX/BcL;->FAILED:LX/BcL;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
