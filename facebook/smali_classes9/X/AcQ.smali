.class public final LX/AcQ;
.super LX/1P1;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1692516
    iput-object p1, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    .line 1692517
    invoke-direct {p0, p2}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 1692518
    invoke-virtual {p0, v0}, LX/1P1;->a(Z)V

    .line 1692519
    invoke-virtual {p0, v0}, LX/1P1;->b(I)V

    .line 1692520
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 1692548
    invoke-direct {p0, p1, p2}, LX/AcQ;-><init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1692523
    iget-object v0, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-boolean v0, v0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    if-nez v0, :cond_0

    .line 1692524
    invoke-super {p0, p1, p2, p3, p4}, LX/1P1;->a(LX/1Od;LX/1Ok;II)V

    .line 1692525
    :goto_0
    return-void

    .line 1692526
    :cond_0
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1692527
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1692528
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1692529
    invoke-virtual {p0, v2, v3}, LX/1OR;->e(II)V

    goto :goto_0

    .line 1692530
    :cond_1
    iget-object v1, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget v1, v1, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->m:I

    .line 1692531
    :try_start_0
    invoke-virtual {p1, v0}, LX/1Od;->b(I)I

    move-result v0

    invoke-virtual {p1, v0}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v3

    .line 1692532
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 1692533
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    .line 1692534
    iget-object v4, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {p0, v3, v4}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1692535
    iget-object v4, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->j:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v5, v5, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    .line 1692536
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v5

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v4, v5, v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1692537
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 1692538
    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 1692539
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    add-int/2addr v0, v1

    .line 1692540
    :try_start_1
    invoke-virtual {p1, v3}, LX/1Od;->a(Landroid/view/View;)V

    .line 1692541
    iget-object v1, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    .line 1692542
    iput v0, v1, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->m:I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1692543
    :goto_1
    invoke-static {v0, p4}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    .line 1692544
    invoke-virtual {p0, v2, v0}, LX/1OR;->e(II)V

    goto :goto_0

    .line 1692545
    :catch_0
    move v0, v1

    :goto_2
    iget-object v1, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/1OR;->b(Ljava/lang/Runnable;)Z

    .line 1692546
    iget-object v1, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v3, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-object v3, v3, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->i:Ljava/lang/Runnable;

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v3, v4, v5}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 1692547
    :catch_1
    goto :goto_2
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1692522
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1692521
    iget-object v0, p0, LX/AcQ;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iget-boolean v0, v0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
