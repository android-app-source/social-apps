.class public final LX/C81;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C82;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/C82;


# direct methods
.method public constructor <init>(LX/C82;)V
    .locals 1

    .prologue
    .line 1852230
    iput-object p1, p0, LX/C81;->d:LX/C82;

    .line 1852231
    move-object v0, p1

    .line 1852232
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1852233
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1852234
    const-string v0, "GroupMemberWelcomePinnedPostCard"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1852213
    if-ne p0, p1, :cond_1

    .line 1852214
    :cond_0
    :goto_0
    return v0

    .line 1852215
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1852216
    goto :goto_0

    .line 1852217
    :cond_3
    check-cast p1, LX/C81;

    .line 1852218
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1852219
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1852220
    if-eq v2, v3, :cond_0

    .line 1852221
    iget-object v2, p0, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1852222
    goto :goto_0

    .line 1852223
    :cond_5
    iget-object v2, p1, LX/C81;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1852224
    :cond_6
    iget-object v2, p0, LX/C81;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C81;->b:Ljava/lang/String;

    iget-object v3, p1, LX/C81;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1852225
    goto :goto_0

    .line 1852226
    :cond_8
    iget-object v2, p1, LX/C81;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1852227
    :cond_9
    iget-object v2, p0, LX/C81;->c:LX/1Pn;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/C81;->c:LX/1Pn;

    iget-object v3, p1, LX/C81;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1852228
    goto :goto_0

    .line 1852229
    :cond_a
    iget-object v2, p1, LX/C81;->c:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
