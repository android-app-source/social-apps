.class public final LX/BnJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;)V
    .locals 0

    .prologue
    .line 1820029
    iput-object p1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1820030
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-boolean v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->B:Z

    if-eqz v0, :cond_0

    .line 1820031
    :goto_0
    return-void

    .line 1820032
    :cond_0
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1820033
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->C:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0

    .line 1820034
    :cond_1
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->q:LX/Bn3;

    iget-object v2, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->y:Ljava/lang/String;

    iget-object v3, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v3, v3, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->z:Ljava/lang/String;

    iget-object v4, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget v4, v4, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1820035
    new-instance v5, LX/Bn2;

    const-class v6, LX/Bn9;

    invoke-interface {v1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/Bn9;

    move-object v6, v2

    move-object v7, v3

    move-object v8, v4

    move-object v9, p1

    invoke-direct/range {v5 .. v10}, LX/Bn2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0Px;LX/Bn9;)V

    .line 1820036
    move-object v1, v5

    .line 1820037
    iput-object v1, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->r:LX/Bn2;

    .line 1820038
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1820039
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->w:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1820040
    iput-object v1, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->x:Landroid/view/View;

    .line 1820041
    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->x:Landroid/view/View;

    const v2, 0x7f0d0f5a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 1820042
    iput-object v0, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->v:Landroid/support/v4/view/ViewPager;

    .line 1820043
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->v:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->r:LX/Bn2;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1820044
    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->x:Landroid/view/View;

    const v2, 0x7f0d0f59

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1820045
    iput-object v0, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1820046
    iget-object v0, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, LX/BnJ;->a:Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->v:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    goto/16 :goto_0
.end method
