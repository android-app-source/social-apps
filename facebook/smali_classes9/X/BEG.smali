.class public LX/BEG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AEn;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/BEG;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1764278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764279
    iput-object p1, p0, LX/BEG;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1764280
    return-void
.end method

.method public static a(LX/0QB;)LX/BEG;
    .locals 4

    .prologue
    .line 1764265
    sget-object v0, LX/BEG;->b:LX/BEG;

    if-nez v0, :cond_1

    .line 1764266
    const-class v1, LX/BEG;

    monitor-enter v1

    .line 1764267
    :try_start_0
    sget-object v0, LX/BEG;->b:LX/BEG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1764268
    if-eqz v2, :cond_0

    .line 1764269
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1764270
    new-instance p0, LX/BEG;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3}, LX/BEG;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 1764271
    move-object v0, p0

    .line 1764272
    sput-object v0, LX/BEG;->b:LX/BEG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1764273
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1764274
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1764275
    :cond_1
    sget-object v0, LX/BEG;->b:LX/BEG;

    return-object v0

    .line 1764276
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1764277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1764264
    const v0, 0x7f0824ef

    return v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1764261
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1764262
    iget-object v1, p0, LX/BEG;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1764263
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1764260
    const v0, 0x7f021811

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1764259
    return-void
.end method
