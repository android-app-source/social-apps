.class public LX/AzL;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732437
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1732438
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;
    .locals 13

    .prologue
    .line 1732435
    new-instance v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/9iU;->a(LX/0QB;)LX/9iU;

    move-result-object v5

    check-cast v5, LX/9iU;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v8

    check-cast v8, LX/74n;

    const-class v1, LX/BP8;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BP8;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-static {p0}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v12

    check-cast v12, LX/AzQ;

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v12}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1HI;LX/9iU;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/74n;LX/BP8;Ljava/lang/String;LX/0kL;LX/AzQ;)V

    .line 1732436
    return-object v0
.end method
