.class public LX/BFR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/BFB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765845
    return-void
.end method

.method public static a(LX/0QB;)LX/BFR;
    .locals 1

    .prologue
    .line 1765846
    invoke-static {p0}, LX/BFR;->b(LX/0QB;)LX/BFR;

    move-result-object v0

    return-object v0
.end method

.method public static final a(LX/1ZF;Lcom/facebook/graphql/model/FeedUnit;LX/0pf;)V
    .locals 3

    .prologue
    .line 1765847
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1765848
    :cond_0
    :goto_0
    return-void

    .line 1765849
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 1765850
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765851
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1765852
    const v0, 0x7f0810e7

    invoke-interface {p0, v0}, LX/1ZF;->x_(I)V

    .line 1765853
    :goto_1
    goto :goto_0

    .line 1765854
    :cond_2
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v0, :cond_0

    .line 1765855
    const v0, 0x7f0810ea

    invoke-interface {p0, v0}, LX/1ZF;->x_(I)V

    goto :goto_0

    .line 1765856
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1765857
    const v0, 0x7f0810e8

    invoke-interface {p0, v0}, LX/1ZF;->x_(I)V

    goto :goto_1

    .line 1765858
    :cond_4
    invoke-virtual {p2, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 1765859
    iget-boolean v1, v0, LX/1g0;->o:Z

    move v0, v1

    .line 1765860
    if-eqz v0, :cond_5

    .line 1765861
    const v0, 0x7f0810e9

    invoke-interface {p0, v0}, LX/1ZF;->x_(I)V

    goto :goto_1

    .line 1765862
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1765863
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1765864
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_1

    .line 1765865
    :cond_7
    const v0, 0x7f0810e7

    invoke-interface {p0, v0}, LX/1ZF;->x_(I)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/BFR;
    .locals 2

    .prologue
    .line 1765866
    new-instance v1, LX/BFR;

    invoke-direct {v1}, LX/BFR;-><init>()V

    .line 1765867
    new-instance v0, LX/BFB;

    invoke-direct {v0}, LX/BFB;-><init>()V

    .line 1765868
    move-object v0, v0

    .line 1765869
    move-object v0, v0

    .line 1765870
    check-cast v0, LX/BFB;

    .line 1765871
    iput-object v0, v1, LX/BFR;->a:LX/BFB;

    .line 1765872
    return-object v1
.end method
