.class public final LX/Bqf;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/Bqg;

.field private final b:Landroid/text/style/ClickableSpan;


# direct methods
.method public constructor <init>(LX/Bqg;Landroid/text/style/ClickableSpan;)V
    .locals 0

    .prologue
    .line 1824920
    iput-object p1, p0, LX/Bqf;->a:LX/Bqg;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1824921
    iput-object p2, p0, LX/Bqf;->b:Landroid/text/style/ClickableSpan;

    .line 1824922
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1824923
    iget-object v0, p0, LX/Bqf;->b:Landroid/text/style/ClickableSpan;

    invoke-virtual {v0, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 1824924
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1824925
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1824926
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1824927
    iget-object v0, p0, LX/Bqf;->a:LX/Bqg;

    iget-object v0, v0, LX/Bqg;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1824928
    return-void
.end method
