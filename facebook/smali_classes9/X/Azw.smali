.class public final LX/Azw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Azx;


# direct methods
.method public constructor <init>(LX/Azx;)V
    .locals 0

    .prologue
    .line 1733083
    iput-object p1, p0, LX/Azw;->a:LX/Azx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1733084
    sget-object v0, LX/Azy;->a:Ljava/lang/String;

    const-string v1, "Failed to fetch drawable for cover pic mini preview"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1733085
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1733086
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 1733087
    iget-object v0, p0, LX/Azw;->a:LX/Azx;

    iget-object v0, v0, LX/Azx;->a:LX/Azy;

    iget-object v0, v0, LX/Azy;->l:LX/Azp;

    invoke-virtual {v0, p1}, LX/Azp;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1733088
    iget-object v0, p0, LX/Azw;->a:LX/Azx;

    iget-object v0, v0, LX/Azx;->a:LX/Azy;

    iget-object v0, v0, LX/Azy;->l:LX/Azp;

    invoke-virtual {v0}, LX/Azp;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1733089
    return-void
.end method
