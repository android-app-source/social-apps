.class public final LX/BMe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BMf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Ri;

.field public b:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/BMf;


# direct methods
.method public constructor <init>(LX/BMf;)V
    .locals 1

    .prologue
    .line 1777673
    iput-object p1, p0, LX/BMe;->c:LX/BMf;

    .line 1777674
    move-object v0, p1

    .line 1777675
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1777676
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1777677
    const-string v0, "LowConfidencePromptComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1777678
    if-ne p0, p1, :cond_1

    .line 1777679
    :cond_0
    :goto_0
    return v0

    .line 1777680
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1777681
    goto :goto_0

    .line 1777682
    :cond_3
    check-cast p1, LX/BMe;

    .line 1777683
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1777684
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1777685
    if-eq v2, v3, :cond_0

    .line 1777686
    iget-object v2, p0, LX/BMe;->a:LX/1Ri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BMe;->a:LX/1Ri;

    iget-object v3, p1, LX/BMe;->a:LX/1Ri;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1777687
    goto :goto_0

    .line 1777688
    :cond_5
    iget-object v2, p1, LX/BMe;->a:LX/1Ri;

    if-nez v2, :cond_4

    .line 1777689
    :cond_6
    iget-object v2, p0, LX/BMe;->b:LX/1Pp;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/BMe;->b:LX/1Pp;

    iget-object v3, p1, LX/BMe;->b:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1777690
    goto :goto_0

    .line 1777691
    :cond_7
    iget-object v2, p1, LX/BMe;->b:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
