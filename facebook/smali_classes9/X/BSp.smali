.class public final LX/BSp;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/BSr;


# direct methods
.method public constructor <init>(LX/BSr;)V
    .locals 0

    .prologue
    .line 1785917
    iput-object p1, p0, LX/BSp;->a:LX/BSr;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 9

    .prologue
    .line 1785892
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    invoke-static {v0}, LX/BSr;->q(LX/BSr;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    .line 1785893
    iget-object v1, p0, LX/BSp;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785894
    iput-object v0, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1785895
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    .line 1785896
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BSr;->F:Z

    .line 1785897
    invoke-static {v0}, LX/BSr;->q(LX/BSr;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object p1

    .line 1785898
    iget-object v1, v0, LX/BSr;->B:LX/BTG;

    iget-object v2, v0, LX/BSr;->j:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1785899
    iget-object v5, v4, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    move-object v4, v5

    .line 1785900
    iget-object v5, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v5}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getVideoDurationMs()I

    move-result v5

    iget-object v6, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v6}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v6

    .line 1785901
    iget-boolean v7, v6, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    move v6, v7

    .line 1785902
    invoke-static {p1}, LX/63w;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)I

    move-result v7

    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v8

    invoke-virtual {p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result p1

    .line 1785903
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "video_editing_exit"

    invoke-direct {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "video_editing_module"

    .line 1785904
    iput-object v0, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1785905
    move-object p2, p2

    .line 1785906
    sget-object v0, LX/BTF;->VIDEO_ITEM_IDENTIFIER:LX/BTF;

    invoke-virtual {v0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTF;->SESSION_ID:LX/BTF;

    invoke-virtual {v0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTF;->ENTRY_POINT:LX/BTF;

    invoke-virtual {v0}, LX/BTF;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTE;->ORIGINAL_LENGTH:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTE;->TRIMMED_LENGTH:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTE;->IS_MUTED:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTE;->ROTATION:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    sget-object v0, LX/BTE;->HD_UPLOAD_STATE:LX/BTE;

    invoke-virtual {v0}, LX/BTE;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    .line 1785907
    iget-object v0, v1, LX/BTG;->a:LX/0Zb;

    invoke-interface {v0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1785908
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    invoke-static {v0}, LX/BSr;->p(LX/BSr;)V

    .line 1785909
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    invoke-static {v0}, LX/BSr;->l(LX/BSr;)V

    .line 1785910
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->E:LX/ATX;

    iget-object v1, p0, LX/BSp;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785911
    iget-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v2

    .line 1785912
    iget-object v2, p0, LX/BSp;->a:LX/BSr;

    iget-object v2, v2, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getCurrentPositionMs()I

    move-result v2

    iget-object v3, p0, LX/BSp;->a:LX/BSr;

    iget-object v3, v3, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785913
    iget-object v4, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1785914
    invoke-interface {v0, v1, v2, v3}, LX/ATX;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;ILjava/lang/String;)V

    .line 1785915
    iget-object v0, p0, LX/BSp;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1785916
    return-void
.end method
