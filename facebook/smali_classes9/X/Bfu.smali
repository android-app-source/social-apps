.class public final LX/Bfu;
.super Landroid/graphics/drawable/StateListDrawable;
.source ""


# direct methods
.method public constructor <init>(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1806692
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1806693
    new-instance v0, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    .line 1806694
    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1, p2}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    .line 1806695
    const/16 v2, 0x8

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    .line 1806696
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadii([F)V

    .line 1806697
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadii([F)V

    .line 1806698
    new-array v2, v5, [I

    const v3, -0x10100a7

    aput v3, v2, v4

    invoke-virtual {p0, v2, v0}, LX/Bfu;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1806699
    new-array v0, v5, [I

    const v2, 0x10100a7

    aput v2, v0, v4

    invoke-virtual {p0, v0, v1}, LX/Bfu;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1806700
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method
