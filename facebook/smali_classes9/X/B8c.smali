.class public final LX/B8c;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1750014
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1750015
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1750016
    :goto_0
    return v1

    .line 1750017
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1750018
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1750019
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1750020
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1750021
    const-string v7, "has_shared_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1750022
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1750023
    :cond_1
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1750024
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1750025
    :cond_2
    const-string v7, "signed_request"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1750026
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1750027
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1750028
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1750029
    if-eqz v0, :cond_5

    .line 1750030
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1750031
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1750032
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1750033
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1749999
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1750000
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750001
    if-eqz v0, :cond_0

    .line 1750002
    const-string v1, "has_shared_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750003
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750004
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750005
    if-eqz v0, :cond_1

    .line 1750006
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750007
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750008
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750009
    if-eqz v0, :cond_2

    .line 1750010
    const-string v1, "signed_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750011
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750012
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1750013
    return-void
.end method
