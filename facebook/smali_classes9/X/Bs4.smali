.class public final LX/Bs4;
.super LX/2Ip;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/2Ip;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "Ljava/lang/ref/WeakReference",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1826937
    invoke-direct {p0}, LX/2Ip;-><init>()V

    .line 1826938
    iput-object p1, p0, LX/Bs4;->a:Ljava/lang/ref/WeakReference;

    .line 1826939
    iput-object p2, p0, LX/Bs4;->b:Ljava/lang/ref/WeakReference;

    .line 1826940
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1826941
    check-cast p1, LX/2f2;

    .line 1826942
    iget-object v0, p0, LX/Bs4;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826943
    iget-object v1, p0, LX/Bs4;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pq;

    .line 1826944
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 1826945
    :cond_0
    :goto_0
    return-void

    .line 1826946
    :cond_1
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1826947
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1826948
    invoke-static {v2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 1826949
    iget-wide v4, p1, LX/2f2;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1826950
    new-instance v4, LX/33P;

    invoke-direct {v4, v3}, LX/33P;-><init>(Lcom/facebook/graphql/model/GraphQLProfile;)V

    move-object v3, v1

    .line 1826951
    check-cast v3, LX/1Pr;

    invoke-interface {v3, v4, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1826952
    iget-object v3, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v3, v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v2, v1

    .line 1826953
    check-cast v2, LX/1Pr;

    iget-object v3, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v2, v4, v3}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1826954
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
