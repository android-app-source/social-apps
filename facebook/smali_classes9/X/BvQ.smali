.class public LX/BvQ;
.super LX/7N3;
.source ""


# instance fields
.field public o:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/view/View;

.field public q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1832280
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BvQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832281
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BvQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832279
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832235
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832236
    const-class v0, LX/BvQ;

    invoke-static {v0, p0}, LX/BvQ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832237
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvP;

    invoke-direct {v1, p0}, LX/BvP;-><init>(LX/BvQ;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832238
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvN;

    invoke-direct {v1, p0}, LX/BvN;-><init>(LX/BvQ;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832239
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvO;

    invoke-direct {v1, p0}, LX/BvO;-><init>(LX/BvQ;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832240
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvM;

    invoke-direct {v1, p0}, LX/BvM;-><init>(LX/BvQ;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832241
    const v0, 0x7f0d0fc9

    invoke-virtual {p0, v0}, LX/BvQ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BvQ;->p:Landroid/view/View;

    .line 1832242
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/BvQ;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object p0, p1, LX/BvQ;->o:LX/1b4;

    return-void
.end method

.method public static e(LX/BvQ;I)V
    .locals 2

    .prologue
    .line 1832274
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1832275
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    .line 1832276
    :goto_0
    return-void

    .line 1832277
    :cond_0
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    .line 1832254
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1832255
    if-eqz p2, :cond_1

    .line 1832256
    invoke-virtual {p0}, LX/BvQ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/BvQ;->e(LX/BvQ;I)V

    .line 1832257
    invoke-virtual {p1}, LX/2pa;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/2pa;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BvQ;->o:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->H()LX/6Ri;

    move-result-object v0

    invoke-virtual {v0}, LX/6Ri;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1832258
    :goto_0
    const/16 p2, 0xc

    const/16 p1, 0x8

    const/4 v4, 0x0

    .line 1832259
    invoke-virtual {p0}, LX/BvQ;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1832260
    if-eqz v0, :cond_3

    move v3, v4

    .line 1832261
    :goto_1
    iget-object v2, p0, LX/BvQ;->p:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1832262
    const/4 v5, 0x6

    invoke-virtual {v2, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1832263
    if-nez v0, :cond_4

    .line 1832264
    invoke-virtual {v2, p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1832265
    invoke-virtual {v2, p2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1832266
    :goto_2
    const/4 v3, 0x2

    const v4, 0x7f0d1974

    invoke-virtual {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1832267
    invoke-virtual {p0, v1}, LX/BvQ;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1832268
    iget-object v1, p0, LX/BvQ;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1832269
    :cond_1
    return-void

    .line 1832270
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1832271
    :cond_3
    const v2, 0x7f0d0950

    move v3, v2

    goto :goto_1

    .line 1832272
    :cond_4
    invoke-virtual {v2, p1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1832273
    invoke-virtual {v2, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1832282
    invoke-super {p0, p1}, LX/7N3;->a(Landroid/view/ViewGroup;)V

    .line 1832283
    const v0, 0x7f0d0fc9

    invoke-virtual {p0, v0}, LX/2oX;->setInnerResource(I)V

    .line 1832284
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1832249
    iget-boolean v0, p0, LX/BvQ;->q:Z

    if-eqz v0, :cond_1

    .line 1832250
    :cond_0
    :goto_0
    return-void

    .line 1832251
    :cond_1
    invoke-super {p0, p1}, LX/7N3;->c(I)V

    .line 1832252
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1832253
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_OUT:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 1832244
    iget-boolean v0, p0, LX/BvQ;->q:Z

    if-eqz v0, :cond_1

    .line 1832245
    :cond_0
    :goto_0
    return-void

    .line 1832246
    :cond_1
    invoke-super {p0, p1}, LX/7N3;->d(I)V

    .line 1832247
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1832248
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_IN:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1832243
    const v0, 0x7f03100a

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3b21af29

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1832234
    const/4 v1, 0x0

    const v2, 0x18af175b

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1832233
    const/4 v0, 0x1

    return v0
.end method
