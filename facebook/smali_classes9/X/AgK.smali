.class public LX/AgK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AgK;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/AgJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1700630
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/AgK;->a:Ljava/util/Set;

    .line 1700631
    return-void
.end method

.method public static a(LX/0QB;)LX/AgK;
    .locals 3

    .prologue
    .line 1700613
    sget-object v0, LX/AgK;->b:LX/AgK;

    if-nez v0, :cond_1

    .line 1700614
    const-class v1, LX/AgK;

    monitor-enter v1

    .line 1700615
    :try_start_0
    sget-object v0, LX/AgK;->b:LX/AgK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1700616
    if-eqz v2, :cond_0

    .line 1700617
    :try_start_1
    new-instance v0, LX/AgK;

    invoke-direct {v0}, LX/AgK;-><init>()V

    .line 1700618
    move-object v0, v0

    .line 1700619
    sput-object v0, LX/AgK;->b:LX/AgK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1700620
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1700621
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1700622
    :cond_1
    sget-object v0, LX/AgK;->b:LX/AgK;

    return-object v0

    .line 1700623
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1700624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(F)V
    .locals 4

    .prologue
    .line 1700632
    iget-object v0, p0, LX/AgK;->a:Ljava/util/Set;

    iget-object v1, p0, LX/AgK;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [LX/AgJ;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AgJ;

    .line 1700633
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 1700634
    invoke-interface {v3, p1}, LX/AgJ;->a(F)V

    .line 1700635
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1700636
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(LX/AgJ;)V
    .locals 1

    .prologue
    .line 1700625
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/AgK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1700626
    iget-object v0, p0, LX/AgK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700627
    :cond_0
    monitor-exit p0

    return-void

    .line 1700628
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/AgJ;)V
    .locals 1

    .prologue
    .line 1700610
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AgK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700611
    monitor-exit p0

    return-void

    .line 1700612
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
