.class public final LX/BsP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BsQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public final synthetic b:LX/BsQ;


# direct methods
.method public constructor <init>(LX/BsQ;)V
    .locals 1

    .prologue
    .line 1827497
    iput-object p1, p0, LX/BsP;->b:LX/BsQ;

    .line 1827498
    move-object v0, p1

    .line 1827499
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1827500
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1827501
    const-string v0, "CSHeaderActor"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1827502
    if-ne p0, p1, :cond_1

    .line 1827503
    :cond_0
    :goto_0
    return v0

    .line 1827504
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1827505
    goto :goto_0

    .line 1827506
    :cond_3
    check-cast p1, LX/BsP;

    .line 1827507
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1827508
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1827509
    if-eq v2, v3, :cond_0

    .line 1827510
    iget-object v2, p0, LX/BsP;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/BsP;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/BsP;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1827511
    goto :goto_0

    .line 1827512
    :cond_4
    iget-object v2, p1, LX/BsP;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
