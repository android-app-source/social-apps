.class public LX/CIg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/8Yh;

.field private final b:LX/Crz;


# direct methods
.method public constructor <init>(LX/8Yh;LX/Crz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874611
    iput-object p1, p0, LX/CIg;->a:LX/8Yh;

    .line 1874612
    iput-object p2, p0, LX/CIg;->b:LX/Crz;

    .line 1874613
    return-void
.end method

.method public static a(LX/ClU;)I
    .locals 3
    .param p0    # LX/ClU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1874592
    if-nez p0, :cond_0

    .line 1874593
    :goto_0
    return v0

    .line 1874594
    :cond_0
    iget-object v1, p0, LX/ClU;->c:LX/ClS;

    move-object v1, v1

    .line 1874595
    sget-object v2, LX/ClS;->MINI_LABEL:LX/ClS;

    if-ne v1, v2, :cond_1

    .line 1874596
    const v0, 0x7f0e0793

    goto :goto_0

    .line 1874597
    :cond_1
    sget-object v2, LX/ClS;->REGULAR:LX/ClS;

    if-ne v1, v2, :cond_2

    .line 1874598
    const v0, 0x7f0e0795

    goto :goto_0

    .line 1874599
    :cond_2
    sget-object v2, LX/ClS;->MEDIUM:LX/ClS;

    if-ne v1, v2, :cond_3

    .line 1874600
    const v0, 0x7f0e0796

    goto :goto_0

    .line 1874601
    :cond_3
    sget-object v2, LX/ClS;->LARGE:LX/ClS;

    if-ne v1, v2, :cond_4

    .line 1874602
    const v0, 0x7f0e0797

    goto :goto_0

    .line 1874603
    :cond_4
    sget-object v2, LX/ClS;->EXTRA_LARGE:LX/ClS;

    if-ne v1, v2, :cond_5

    .line 1874604
    const v0, 0x7f0e0798

    goto :goto_0

    .line 1874605
    :cond_5
    sget-object v1, LX/CtC;->b:[I

    .line 1874606
    iget-object v2, p0, LX/ClU;->a:LX/ClT;

    move-object v2, v2

    .line 1874607
    invoke-virtual {v2}, LX/ClT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1874608
    :pswitch_0
    const v0, 0x7f0e0797

    goto :goto_0

    .line 1874609
    :pswitch_1
    const v0, 0x7f0e0795

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1874489
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1874490
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1874491
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1874492
    :cond_0
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 1874493
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/CIg;
    .locals 5

    .prologue
    .line 1874581
    const-class v1, LX/CIg;

    monitor-enter v1

    .line 1874582
    :try_start_0
    sget-object v0, LX/CIg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1874583
    sput-object v2, LX/CIg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1874584
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874585
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1874586
    new-instance p0, LX/CIg;

    invoke-static {v0}, LX/8Yh;->a(LX/0QB;)LX/8Yh;

    move-result-object v3

    check-cast v3, LX/8Yh;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v4

    check-cast v4, LX/Crz;

    invoke-direct {p0, v3, v4}, LX/CIg;-><init>(LX/8Yh;LX/Crz;)V

    .line 1874587
    move-object v0, p0

    .line 1874588
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1874589
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CIg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874590
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1874591
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/graphics/Typeface;"
        }
    .end annotation

    .prologue
    .line 1874578
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1874579
    :cond_0
    const/4 v0, 0x0

    .line 1874580
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method private static a(Landroid/widget/TextView;D)V
    .locals 3

    .prologue
    .line 1874575
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    .line 1874576
    check-cast p0, LX/CtE;

    invoke-interface {p0, p1, p2}, LX/CtE;->a(D)V

    .line 1874577
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/TextView;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V
    .locals 3

    .prologue
    .line 1874566
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/CIg;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1874567
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->b()Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    move-result-object v0

    .line 1874568
    if-eqz v0, :cond_0

    .line 1874569
    new-instance v1, LX/CtD;

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CtD;-><init>(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1874570
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/CIg;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1874571
    instance-of v0, p1, LX/CtE;

    if-eqz v0, :cond_1

    .line 1874572
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->m()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, LX/CIg;->a(Landroid/widget/TextView;D)V

    .line 1874573
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->ek_()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, LX/CIg;->b(Landroid/widget/TextView;D)V

    .line 1874574
    :cond_1
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1874614
    :try_start_0
    invoke-static {p1}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v0

    .line 1874615
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1874616
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private a(Lcom/facebook/richdocument/view/widget/RichTextView;)V
    .locals 1

    .prologue
    .line 1874559
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874560
    iget-object v0, p0, LX/CIg;->b:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1874561
    const/4 v0, 0x5

    invoke-static {p1, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V

    .line 1874562
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutDirection(I)V

    .line 1874563
    :cond_0
    :goto_0
    return-void

    .line 1874564
    :cond_1
    const/4 v0, 0x3

    invoke-static {p1, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V

    .line 1874565
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutDirection(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V
    .locals 2

    .prologue
    .line 1874549
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v0

    .line 1874550
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1874551
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1874552
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1874553
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1874554
    instance-of v1, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_1

    .line 1874555
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1874556
    :cond_0
    :goto_0
    return-void

    .line 1874557
    :cond_1
    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v1, :cond_0

    .line 1874558
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private static b(Landroid/widget/TextView;D)V
    .locals 3

    .prologue
    .line 1874546
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_0

    .line 1874547
    check-cast p0, LX/CtE;

    invoke-interface {p0, p1, p2}, LX/CtE;->b(D)V

    .line 1874548
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;LX/Clf;)V
    .locals 7
    .param p1    # Landroid/widget/TextView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1874513
    if-nez p1, :cond_1

    .line 1874514
    :cond_0
    return-void

    .line 1874515
    :cond_1
    iget-object v0, p2, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 1874516
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1874517
    sget-object v0, LX/Csn;->b:LX/Csn;

    if-nez v0, :cond_2

    .line 1874518
    new-instance v0, LX/Csn;

    invoke-direct {v0}, LX/Csn;-><init>()V

    sput-object v0, LX/Csn;->b:LX/Csn;

    .line 1874519
    :cond_2
    sget-object v0, LX/Csn;->b:LX/Csn;

    move-object v0, v0

    .line 1874520
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1874521
    iget-object v0, p2, LX/Clf;->d:LX/0Px;

    if-eqz v0, :cond_9

    iget-object v0, p2, LX/Clf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1874522
    if-eqz v0, :cond_4

    .line 1874523
    iget-object v0, p2, LX/Clf;->d:LX/0Px;

    move-object v3, v0

    .line 1874524
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1874525
    if-eqz v0, :cond_3

    .line 1874526
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1874527
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1874528
    :cond_4
    invoke-virtual {p2}, LX/Clf;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1874529
    iget-object v0, p2, LX/Clf;->c:LX/0Px;

    move-object v3, v0

    .line 1874530
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    .line 1874531
    invoke-direct {p0, p1, v0}, LX/CIg;->a(Landroid/widget/TextView;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 1874532
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1874533
    :cond_5
    iget-object v0, p0, LX/CIg;->a:LX/8Yh;

    .line 1874534
    iget-object v1, v0, LX/8Yh;->a:Ljava/util/Map;

    move-object v0, v1

    .line 1874535
    iget-object v1, p0, LX/CIg;->a:LX/8Yh;

    .line 1874536
    iget-object v3, v1, LX/8Yh;->b:Ljava/util/Map;

    move-object v3, v3

    .line 1874537
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    move v1, v0

    .line 1874538
    :goto_3
    iget-object v0, p2, LX/Clf;->b:LX/0Px;

    if-eqz v0, :cond_a

    iget-object v0, p2, LX/Clf;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1874539
    if-eqz v0, :cond_0

    .line 1874540
    iget-object v0, p2, LX/Clf;->b:LX/0Px;

    move-object v4, v0

    .line 1874541
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_5
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    .line 1874542
    invoke-virtual {p2}, LX/Clf;->c()Z

    move-result v6

    if-eqz v6, :cond_6

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, LX/CIg;->a(Ljava/util/Map;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 1874543
    :cond_6
    invoke-direct {p0, p1, v0}, LX/CIg;->a(Landroid/widget/TextView;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 1874544
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    move v1, v2

    .line 1874545
    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmr;)V
    .locals 2

    .prologue
    .line 1874505
    if-nez p2, :cond_0

    .line 1874506
    invoke-direct {p0, p1}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;)V

    .line 1874507
    :goto_0
    return-void

    .line 1874508
    :cond_0
    sget-object v0, LX/CtC;->a:[I

    invoke-virtual {p2}, LX/Cmr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1874509
    invoke-direct {p0, p1}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;)V

    goto :goto_0

    .line 1874510
    :pswitch_0
    const/4 v0, 0x3

    invoke-static {p1, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V

    goto :goto_0

    .line 1874511
    :pswitch_1
    const/4 v0, 0x5

    invoke-static {p1, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V

    goto :goto_0

    .line 1874512
    :pswitch_2
    const/16 v0, 0x11

    invoke-static {p1, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1874494
    iget-object v0, p0, LX/CIg;->a:LX/8Yh;

    .line 1874495
    iget-object v1, v0, LX/8Yh;->a:Ljava/util/Map;

    move-object v0, v1

    .line 1874496
    invoke-static {v0, p2}, LX/CIg;->a(Ljava/util/Map;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1874497
    if-nez v0, :cond_0

    .line 1874498
    iget-object v0, p0, LX/CIg;->a:LX/8Yh;

    .line 1874499
    iget-object v1, v0, LX/8Yh;->b:Ljava/util/Map;

    move-object v0, v1

    .line 1874500
    invoke-static {v0, p2}, LX/CIg;->a(Ljava/util/Map;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1874501
    :cond_0
    if-nez v0, :cond_1

    .line 1874502
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1874503
    :goto_0
    return-void

    .line 1874504
    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method
