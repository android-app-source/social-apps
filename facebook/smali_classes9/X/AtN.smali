.class public LX/AtN;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AtM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1721429
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1721430
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/ArL;LX/ArT;LX/Ata;)LX/AtM;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "LX/0is;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$ProvidesInspirationTextParams;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState;",
            ":",
            "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/ArL;",
            "LX/ArT;",
            "LX/Ata;",
            ")",
            "LX/AtM",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1721427
    new-instance v1, LX/AtM;

    move-object/from16 v2, p1

    check-cast v2, LX/0il;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v3, LX/AtA;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AtA;

    const-class v3, LX/Asi;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Asi;

    const-class v3, LX/At5;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/At5;

    const-class v3, LX/Au1;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/Au1;

    const-class v3, LX/Asz;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Asz;

    const-class v3, LX/Atq;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/Atq;

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v13

    check-cast v13, LX/0fO;

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v14

    check-cast v14, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v15

    check-cast v15, LX/0wW;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v16

    check-cast v16, LX/0W3;

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v16}, LX/AtM;-><init>(LX/0il;LX/ArL;LX/ArT;LX/Ata;Landroid/content/Context;LX/AtA;LX/Asi;LX/At5;LX/Au1;LX/Asz;LX/Atq;LX/0fO;LX/0hB;LX/0wW;LX/0W3;)V

    .line 1721428
    return-object v1
.end method
