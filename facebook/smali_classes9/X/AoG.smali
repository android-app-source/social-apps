.class public final LX/AoG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1713841
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1713842
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1713843
    :goto_0
    return v1

    .line 1713844
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1713845
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1713846
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1713847
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1713848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1713849
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1713850
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 1713851
    :cond_3
    const-string v4, "link_target_store_data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1713852
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1713853
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 1713854
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1713855
    :goto_2
    move v0, v3

    .line 1713856
    goto :goto_1

    .line 1713857
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1713858
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1713859
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1713860
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1713861
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 1713862
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1713863
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1713864
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 1713865
    const-string v7, "show_beeper"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1713866
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_3

    .line 1713867
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1713868
    :cond_8
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1713869
    if-eqz v0, :cond_9

    .line 1713870
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 1713871
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v0, v3

    move v5, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1713872
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1713873
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1713874
    if-eqz v0, :cond_0

    .line 1713875
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713876
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1713877
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1713878
    if-eqz v0, :cond_2

    .line 1713879
    const-string v1, "link_target_store_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713880
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1713881
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1713882
    if-eqz v1, :cond_1

    .line 1713883
    const-string p1, "show_beeper"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1713884
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1713885
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1713886
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1713887
    return-void
.end method
