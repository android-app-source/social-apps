.class public final LX/BL6;
.super Landroid/content/AsyncQueryHandler;
.source ""


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1775188
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 1775189
    iput-object p2, p0, LX/BL6;->a:Ljava/lang/ref/WeakReference;

    .line 1775190
    return-void
.end method


# virtual methods
.method public final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 1775191
    iget-object v0, p0, LX/BL6;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    .line 1775192
    if-eqz v0, :cond_0

    .line 1775193
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1775194
    if-eqz v1, :cond_2

    .line 1775195
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1775196
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1775197
    :cond_0
    :goto_0
    return-void

    .line 1775198
    :cond_1
    invoke-virtual {v1, p3}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 1775199
    :cond_2
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    .line 1775200
    iput-object p3, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->d:Landroid/database/Cursor;

    .line 1775201
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1775202
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->c:Landroid/widget/Filter;

    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1775203
    :cond_3
    const/4 v3, 0x1

    .line 1775204
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    .line 1775205
    iget-object v2, v1, LX/9UA;->d:Landroid/database/Cursor;

    move-object v1, v2

    .line 1775206
    if-nez v1, :cond_4

    .line 1775207
    invoke-static {v0, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;Z)V

    .line 1775208
    :goto_1
    goto :goto_0

    .line 1775209
    :cond_4
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;Z)V

    .line 1775210
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    iget-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    .line 1775211
    iget-object p0, v2, LX/9UA;->d:Landroid/database/Cursor;

    move-object v2, p0

    .line 1775212
    invoke-virtual {v1, v2}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1775213
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {v1}, LX/3Tf;->getCount()I

    move-result v1

    if-nez v1, :cond_5

    .line 1775214
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->c:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v1}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1775215
    invoke-static {v0, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;Z)V

    .line 1775216
    :cond_5
    iget-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    goto :goto_1
.end method
