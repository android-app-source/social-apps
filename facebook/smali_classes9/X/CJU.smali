.class public LX/CJU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/CJU;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/0SG;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Ljava/security/SecureRandom;

.field private volatile f:Ljavax/crypto/SecretKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile g:Ljavax/crypto/spec/IvParameterSpec;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1875453
    const-class v0, LX/CJU;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CJU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/security/SecureRandom;)V
    .locals 0
    .param p4    # Ljava/security/SecureRandom;
        .annotation runtime Lcom/facebook/common/random/FixedSecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875471
    iput-object p1, p0, LX/CJU;->b:LX/0Sh;

    .line 1875472
    iput-object p2, p0, LX/CJU;->c:LX/0SG;

    .line 1875473
    iput-object p3, p0, LX/CJU;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1875474
    iput-object p4, p0, LX/CJU;->e:Ljava/security/SecureRandom;

    .line 1875475
    return-void
.end method

.method public static a(LX/0QB;)LX/CJU;
    .locals 7

    .prologue
    .line 1875457
    sget-object v0, LX/CJU;->h:LX/CJU;

    if-nez v0, :cond_1

    .line 1875458
    const-class v1, LX/CJU;

    monitor-enter v1

    .line 1875459
    :try_start_0
    sget-object v0, LX/CJU;->h:LX/CJU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1875460
    if-eqz v2, :cond_0

    .line 1875461
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1875462
    new-instance p0, LX/CJU;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2KD;->b(LX/0QB;)Ljava/security/SecureRandom;

    move-result-object v6

    check-cast v6, Ljava/security/SecureRandom;

    invoke-direct {p0, v3, v4, v5, v6}, LX/CJU;-><init>(LX/0Sh;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/security/SecureRandom;)V

    .line 1875463
    move-object v0, p0

    .line 1875464
    sput-object v0, LX/CJU;->h:LX/CJU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1875465
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1875466
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1875467
    :cond_1
    sget-object v0, LX/CJU;->h:LX/CJU;

    return-object v0

    .line 1875468
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1875469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1875454
    iput-object v0, p0, LX/CJU;->f:Ljavax/crypto/SecretKey;

    .line 1875455
    iput-object v0, p0, LX/CJU;->g:Ljavax/crypto/spec/IvParameterSpec;

    .line 1875456
    return-void
.end method
