.class public final LX/Bj2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventCreationNikumanActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 0

    .prologue
    .line 1811052
    iput-object p1, p0, LX/Bj2;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1811053
    iget-object v0, p0, LX/Bj2;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811054
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v4, v1

    .line 1811055
    invoke-virtual {v4}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 1811056
    :cond_0
    :goto_0
    return-void

    .line 1811057
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1811058
    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;

    .line 1811059
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v1, v2

    :goto_1
    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    if-nez v1, :cond_0

    .line 1811060
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1811061
    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1811062
    iput-object v1, v4, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1811063
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1811064
    iput-object v0, v4, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    .line 1811065
    iget-object v0, p0, LX/Bj2;->a:Lcom/facebook/events/create/EventCreationNikumanActivity;

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, v2}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    goto :goto_0

    .line 1811066
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 1811067
    if-nez v1, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_1

    .line 1811068
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1811069
    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_2
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1811070
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1811071
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bj2;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
