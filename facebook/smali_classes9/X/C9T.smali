.class public final LX/C9T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/C9Y;


# direct methods
.method public constructor <init>(LX/C9Y;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1854125
    iput-object p1, p0, LX/C9T;->c:LX/C9Y;

    iput-object p2, p0, LX/C9T;->a:Ljava/lang/String;

    iput-object p3, p0, LX/C9T;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x4efa5183

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1854126
    iget-object v0, p0, LX/C9T;->c:LX/C9Y;

    iget-object v0, v0, LX/C9Y;->d:LX/7ly;

    iget-object v2, p0, LX/C9T;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/7ly;->a(Ljava/lang/String;)LX/7lx;

    move-result-object v2

    .line 1854127
    iget-object v0, p0, LX/C9T;->c:LX/C9Y;

    iget-object v0, v0, LX/C9Y;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Mh;

    iget-object v3, p0, LX/C9T;->a:Ljava/lang/String;

    const/4 v5, 0x0

    .line 1854128
    if-nez v3, :cond_2

    .line 1854129
    :cond_0
    :goto_0
    move v0, v5

    .line 1854130
    if-eqz v0, :cond_1

    .line 1854131
    new-instance v0, LX/C9S;

    invoke-direct {v0, p0}, LX/C9S;-><init>(LX/C9T;)V

    .line 1854132
    iput-object v0, v2, LX/7lx;->j:LX/C9S;

    .line 1854133
    :cond_1
    invoke-virtual {v2}, LX/7lx;->a()V

    .line 1854134
    const v0, -0x125d71ac

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1854135
    :cond_2
    iget-object v6, v0, LX/8Mh;->a:LX/7ma;

    invoke-virtual {v6, v3}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v6

    .line 1854136
    if-eqz v6, :cond_0

    .line 1854137
    iget-object v7, v6, LX/7ml;->b:LX/7mm;

    move-object v7, v7

    .line 1854138
    sget-object p1, LX/7mm;->POST:LX/7mm;

    if-ne v7, p1, :cond_0

    .line 1854139
    iget-object v7, v6, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v7, v7

    .line 1854140
    invoke-virtual {v7}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1854141
    iget-object v7, v6, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v6, v7

    .line 1854142
    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v6

    iget-boolean v6, v6, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0
.end method
