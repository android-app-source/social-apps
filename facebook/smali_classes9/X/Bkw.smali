.class public final LX/Bkw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0U1;

.field public static final B:LX/0U1;

.field public static final C:LX/0U1;

.field public static final D:LX/0U1;

.field public static final E:LX/0U1;

.field public static final F:LX/0U1;

.field public static final G:LX/0U1;

.field public static final H:LX/0U1;

.field public static final I:LX/0U1;

.field public static final J:LX/0U1;

.field public static final K:LX/0U1;

.field public static final L:LX/0U1;

.field public static final M:LX/0U1;

.field public static final N:LX/0U1;

.field public static final O:LX/0U1;

.field public static final P:LX/0U1;

.field public static final Q:LX/0U1;

.field public static final R:LX/0U1;

.field public static final S:LX/0U1;

.field public static final T:LX/0U1;

.field public static final U:LX/0U1;

.field public static final V:LX/0U1;

.field public static final W:LX/0U1;

.field public static final X:LX/0U1;

.field public static final Y:LX/0U1;

.field public static final Z:LX/0U1;

.field public static final a:LX/0U1;

.field public static final aa:LX/0U1;

.field public static final ab:LX/0U1;

.field public static final ac:LX/0U1;

.field public static final ad:LX/0U1;

.field public static final ae:LX/0U1;

.field public static final af:LX/0U1;

.field public static final ag:LX/0U1;

.field public static final ah:LX/0U1;

.field public static final ai:LX/0U1;

.field public static final aj:LX/0U1;

.field public static final ak:LX/0U1;

.field public static final al:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;

.field public static final p:LX/0U1;

.field public static final q:LX/0U1;

.field public static final r:LX/0U1;

.field public static final s:LX/0U1;

.field public static final t:LX/0U1;

.field public static final u:LX/0U1;

.field public static final v:LX/0U1;

.field public static final w:LX/0U1;

.field public static final x:LX/0U1;

.field public static final y:LX/0U1;

.field public static final z:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1815174
    new-instance v0, LX/0U1;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->a:LX/0U1;

    .line 1815175
    new-instance v0, LX/0U1;

    const-string v1, "facebook_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->b:LX/0U1;

    .line 1815176
    new-instance v0, LX/0U1;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->c:LX/0U1;

    .line 1815177
    new-instance v0, LX/0U1;

    const-string v1, "description_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->d:LX/0U1;

    .line 1815178
    new-instance v0, LX/0U1;

    const-string v1, "description_entities"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->e:LX/0U1;

    .line 1815179
    new-instance v0, LX/0U1;

    const-string v1, "event_kind"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->f:LX/0U1;

    .line 1815180
    new-instance v0, LX/0U1;

    const-string v1, "event_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->g:LX/0U1;

    .line 1815181
    new-instance v0, LX/0U1;

    const-string v1, "privacy_kind"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->h:LX/0U1;

    .line 1815182
    new-instance v0, LX/0U1;

    const-string v1, "privacy_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->i:LX/0U1;

    .line 1815183
    new-instance v0, LX/0U1;

    const-string v1, "connection_style"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->j:LX/0U1;

    .line 1815184
    new-instance v0, LX/0U1;

    const-string v1, "event_action_style"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->k:LX/0U1;

    .line 1815185
    new-instance v0, LX/0U1;

    const-string v1, "can_guests_invite_friends"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->l:LX/0U1;

    .line 1815186
    new-instance v0, LX/0U1;

    const-string v1, "can_post_be_moderated"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->m:LX/0U1;

    .line 1815187
    new-instance v0, LX/0U1;

    const-string v1, "admin_post_approval_required"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->n:LX/0U1;

    .line 1815188
    new-instance v0, LX/0U1;

    const-string v1, "admin_post_only"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->o:LX/0U1;

    .line 1815189
    new-instance v0, LX/0U1;

    const-string v1, "is_privacy_locked"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->p:LX/0U1;

    .line 1815190
    new-instance v0, LX/0U1;

    const-string v1, "created_for_group_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->q:LX/0U1;

    .line 1815191
    new-instance v0, LX/0U1;

    const-string v1, "created_for_group_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->r:LX/0U1;

    .line 1815192
    new-instance v0, LX/0U1;

    const-string v1, "parent_group_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->s:LX/0U1;

    .line 1815193
    new-instance v0, LX/0U1;

    const-string v1, "parent_group_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->t:LX/0U1;

    .line 1815194
    new-instance v0, LX/0U1;

    const-string v1, "creator_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->u:LX/0U1;

    .line 1815195
    new-instance v0, LX/0U1;

    const-string v1, "page_actor_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->v:LX/0U1;

    .line 1815196
    new-instance v0, LX/0U1;

    const-string v1, "page_actor_profile_picture_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->w:LX/0U1;

    .line 1815197
    new-instance v0, LX/0U1;

    const-string v1, "is_cancelled"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->x:LX/0U1;

    .line 1815198
    new-instance v0, LX/0U1;

    const-string v1, "is_draft"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->y:LX/0U1;

    .line 1815199
    new-instance v0, LX/0U1;

    const-string v1, "publish_time_millis"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->z:LX/0U1;

    .line 1815200
    new-instance v0, LX/0U1;

    const-string v1, "can_change_rsvp_status"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->A:LX/0U1;

    .line 1815201
    new-instance v0, LX/0U1;

    const-string v1, "viewer_guest_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->B:LX/0U1;

    .line 1815202
    new-instance v0, LX/0U1;

    const-string v1, "viewer_watch_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->C:LX/0U1;

    .line 1815203
    new-instance v0, LX/0U1;

    const-string v1, "saved"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->D:LX/0U1;

    .line 1815204
    new-instance v0, LX/0U1;

    const-string v1, "saved_collection_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->E:LX/0U1;

    .line 1815205
    new-instance v0, LX/0U1;

    const-string v1, "saved_privacy"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->F:LX/0U1;

    .line 1815206
    new-instance v0, LX/0U1;

    const-string v1, "has_pending_invite"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->G:LX/0U1;

    .line 1815207
    new-instance v0, LX/0U1;

    const-string v1, "is_host"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->H:LX/0U1;

    .line 1815208
    new-instance v0, LX/0U1;

    const-string v1, "start_time_millis"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->I:LX/0U1;

    .line 1815209
    new-instance v0, LX/0U1;

    const-string v1, "end_time_millis"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->J:LX/0U1;

    .line 1815210
    new-instance v0, LX/0U1;

    const-string v1, "time_zone"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->K:LX/0U1;

    .line 1815211
    new-instance v0, LX/0U1;

    const-string v1, "is_all_day"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->L:LX/0U1;

    .line 1815212
    new-instance v0, LX/0U1;

    const-string v1, "location_full_address"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->M:LX/0U1;

    .line 1815213
    new-instance v0, LX/0U1;

    const-string v1, "location_latitude"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->N:LX/0U1;

    .line 1815214
    new-instance v0, LX/0U1;

    const-string v1, "location_longitude"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->O:LX/0U1;

    .line 1815215
    new-instance v0, LX/0U1;

    const-string v1, "location_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->P:LX/0U1;

    .line 1815216
    new-instance v0, LX/0U1;

    const-string v1, "location_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->Q:LX/0U1;

    .line 1815217
    new-instance v0, LX/0U1;

    const-string v1, "location_time_zone"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->R:LX/0U1;

    .line 1815218
    new-instance v0, LX/0U1;

    const-string v1, "location_object_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->S:LX/0U1;

    .line 1815219
    new-instance v0, LX/0U1;

    const-string v1, "uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->T:LX/0U1;

    .line 1815220
    new-instance v0, LX/0U1;

    const-string v1, "cover_photo_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->U:LX/0U1;

    .line 1815221
    new-instance v0, LX/0U1;

    const-string v1, "cover_photo_portrait_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->V:LX/0U1;

    .line 1815222
    new-instance v0, LX/0U1;

    const-string v1, "cover_photo_landscape_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->W:LX/0U1;

    .line 1815223
    new-instance v0, LX/0U1;

    const-string v1, "profile_photo_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->X:LX/0U1;

    .line 1815224
    new-instance v0, LX/0U1;

    const-string v1, "capabilities"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->Y:LX/0U1;

    .line 1815225
    new-instance v0, LX/0U1;

    const-string v1, "cohost_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->Z:LX/0U1;

    .line 1815226
    new-instance v0, LX/0U1;

    const-string v1, "cohost_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->aa:LX/0U1;

    .line 1815227
    new-instance v0, LX/0U1;

    const-string v1, "cohost_ids"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ab:LX/0U1;

    .line 1815228
    new-instance v0, LX/0U1;

    const-string v1, "friend_going_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ac:LX/0U1;

    .line 1815229
    new-instance v0, LX/0U1;

    const-string v1, "friend_going_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ad:LX/0U1;

    .line 1815230
    new-instance v0, LX/0U1;

    const-string v1, "friend_maybe_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ae:LX/0U1;

    .line 1815231
    new-instance v0, LX/0U1;

    const-string v1, "friend_maybe_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->af:LX/0U1;

    .line 1815232
    new-instance v0, LX/0U1;

    const-string v1, "friend_watcher_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ag:LX/0U1;

    .line 1815233
    new-instance v0, LX/0U1;

    const-string v1, "friend_watcher_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ah:LX/0U1;

    .line 1815234
    new-instance v0, LX/0U1;

    const-string v1, "inviter_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ai:LX/0U1;

    .line 1815235
    new-instance v0, LX/0U1;

    const-string v1, "inviter_friendship_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->aj:LX/0U1;

    .line 1815236
    new-instance v0, LX/0U1;

    const-string v1, "inviter_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->ak:LX/0U1;

    .line 1815237
    new-instance v0, LX/0U1;

    const-string v1, "total_purchased_tickets"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/Bkw;->al:LX/0U1;

    return-void
.end method
