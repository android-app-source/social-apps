.class public LX/Ag4;
.super LX/AeQ;
.source ""


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/0tX;

.field public final i:LX/03V;

.field private j:J

.field public volatile k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700164
    const-class v0, LX/Ag4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ag4;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/0SG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700182
    invoke-direct {p0, p4}, LX/AeQ;-><init>(LX/0SG;)V

    .line 1700183
    iput-object p1, p0, LX/Ag4;->g:Ljava/util/concurrent/ExecutorService;

    .line 1700184
    iput-object p2, p0, LX/Ag4;->h:LX/0tX;

    .line 1700185
    iput-object p3, p0, LX/Ag4;->i:LX/03V;

    .line 1700186
    return-void
.end method


# virtual methods
.method public final declared-synchronized c()V
    .locals 6

    .prologue
    .line 1700167
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/AeQ;->c()V

    .line 1700168
    iget-object v0, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1700169
    iget-object v0, p0, LX/Ag4;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Ag4;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_startFetching"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tried to fetch without a story id."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700170
    :goto_0
    monitor-exit p0

    return-void

    .line 1700171
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/AeQ;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1700172
    iget-boolean v1, p0, LX/AeQ;->e:Z

    if-nez v1, :cond_1

    .line 1700173
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x384

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/Ag4;->j:J

    .line 1700174
    :cond_1
    invoke-static {}, LX/6SN;->d()LX/6SL;

    move-result-object v1

    .line 1700175
    const-string v2, "targetID"

    iget-object v3, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1700176
    const-string v2, "after_timestamp"

    iget-wide v4, p0, LX/Ag4;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1700177
    const-string v2, "before_timestamp"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1700178
    iget-object v2, p0, LX/Ag4;->h:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    iput-object v1, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1700179
    iget-object v1, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Ag3;

    invoke-direct {v2, p0}, LX/Ag3;-><init>(LX/Ag4;)V

    iget-object v3, p0, LX/Ag4;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1700180
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/Ag4;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1700181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 1700187
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1700188
    iget-object v0, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700189
    :cond_0
    monitor-exit p0

    return-void

    .line 1700190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 1700166
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()LX/AeN;
    .locals 1

    .prologue
    .line 1700165
    sget-object v0, LX/AeN;->LIVE_WATCH_EVENT:LX/AeN;

    return-object v0
.end method
