.class public final LX/BJP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BJR;


# direct methods
.method public constructor <init>(LX/BJR;)V
    .locals 0

    .prologue
    .line 1771874
    iput-object p1, p0, LX/BJP;->a:LX/BJR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1771875
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 1771876
    if-eqz p1, :cond_2

    .line 1771877
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/LinksPreview;

    .line 1771878
    :goto_0
    iget-object v2, p0, LX/BJP;->a:LX/BJR;

    iget-object v2, v2, LX/BJR;->g:LX/BJx;

    invoke-virtual {v2}, LX/BJx;->b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/BJP;->a:LX/BJR;

    iget-object v1, v1, LX/BJR;->g:LX/BJx;

    invoke-virtual {v1}, LX/BJx;->b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    :cond_0
    invoke-static {v0, v1}, Lcom/facebook/ipc/composer/intent/SharePreview;->a(Lcom/facebook/share/model/LinksPreview;Lcom/facebook/ipc/composer/intent/SharePreview;)Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v1

    .line 1771879
    iget-object v0, p0, LX/BJP;->a:LX/BJR;

    iget-object v0, v0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, LX/2ro;

    iget-object v2, p0, LX/BJP;->a:LX/BJR;

    iget-object v2, v2, LX/BJR;->g:LX/BJx;

    invoke-virtual {v2}, LX/BJx;->b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v2

    invoke-direct {v0, v2}, LX/2ro;-><init>(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)V

    .line 1771880
    :goto_1
    iget-object v2, p0, LX/BJP;->a:LX/BJR;

    iget-object v2, v2, LX/BJR;->h:LX/BK8;

    .line 1771881
    iput-object v1, v0, LX/2ro;->j:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 1771882
    move-object v0, v0

    .line 1771883
    invoke-virtual {v0}, LX/2ro;->a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    .line 1771884
    iget-object v3, v2, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v2, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v4, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v4}, LX/BKm;->a()LX/BKl;

    move-result-object v4

    iget-object p1, v2, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object p1, p1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p1, p1, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPlatformConfiguration(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p1

    .line 1771885
    iput-object p1, v4, LX/BKl;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1771886
    move-object v4, v4

    .line 1771887
    invoke-virtual {v4}, LX/BKl;->a()LX/BKm;

    move-result-object v4

    .line 1771888
    iput-object v4, v3, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1771889
    iget-object v0, p0, LX/BJP;->a:LX/BJR;

    invoke-static {v0, v1}, LX/BJR;->a$redex0(LX/BJR;Lcom/facebook/ipc/composer/intent/SharePreview;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0

    .line 1771890
    :cond_1
    new-instance v0, LX/2ro;

    invoke-direct {v0}, LX/2ro;-><init>()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
