.class public final LX/BGY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alv;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private final a:LX/5iG;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1aX;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(LX/5iG;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 1767168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767169
    iput-object p1, p0, LX/BGY;->a:LX/5iG;

    .line 1767170
    iput-object p2, p0, LX/BGY;->b:Landroid/content/res/Resources;

    .line 1767171
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1767172
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1767173
    if-eqz v0, :cond_0

    .line 1767174
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1767175
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1767176
    :cond_0
    iget-object v0, p1, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    move-object v0, v0

    .line 1767177
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1767178
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1767179
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/BGY;->c:LX/0Px;

    .line 1767180
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1aX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1767181
    iget-object v0, p0, LX/BGY;->c:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/graphics/Canvas;II)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1767182
    iget-object v1, p0, LX/BGY;->a:LX/5iG;

    if-nez v1, :cond_1

    .line 1767183
    :cond_0
    return-void

    .line 1767184
    :cond_1
    iget-object v1, p0, LX/BGY;->d:Landroid/graphics/RectF;

    if-nez v1, :cond_2

    .line 1767185
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, LX/BGY;->d:Landroid/graphics/RectF;

    .line 1767186
    :cond_2
    iget-object v1, p0, LX/BGY;->b:Landroid/content/res/Resources;

    const v2, 0x7f020b54

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1767187
    invoke-virtual {v1, v0, v0, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1767188
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1767189
    iget-object v1, p0, LX/BGY;->a:LX/5iG;

    invoke-virtual {v1}, LX/5iG;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1767190
    iget-object v4, p0, LX/BGY;->a:LX/5iG;

    invoke-virtual {v4, v0}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1767191
    iget-object v4, p0, LX/BGY;->a:LX/5iG;

    invoke-virtual {v4, v0}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v4

    invoke-virtual {v4}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1767192
    iget-object v5, p0, LX/BGY;->d:Landroid/graphics/RectF;

    invoke-static {p1, v4, v0, v5}, LX/8GJ;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;Landroid/graphics/RectF;)V

    .line 1767193
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
