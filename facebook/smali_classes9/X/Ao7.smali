.class public LX/Ao7;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private final e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1713234
    new-instance v0, LX/Ao5;

    invoke-direct {v0}, LX/Ao5;-><init>()V

    sput-object v0, LX/Ao7;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1713226
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1713227
    const v0, 0x7f031527

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1713228
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Ao7;->setOrientation(I)V

    .line 1713229
    const v0, 0x7f0d2fbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/Ao7;->b:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1713230
    const v0, 0x7f0d2fbf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ao7;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1713231
    const v0, 0x7f0d2fbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/Ao7;->d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1713232
    const v0, 0x7f0d2fc0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ao7;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1713233
    return-void
.end method

.method private a(LX/2mQ;)Lcom/facebook/resources/ui/FbTextView;
    .locals 2

    .prologue
    .line 1713222
    sget-object v0, LX/Ao6;->a:[I

    invoke-virtual {p1}, LX/2mQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1713223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid button location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1713224
    :pswitch_0
    iget-object v0, p0, LX/Ao7;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1713225
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/Ao7;->e:Lcom/facebook/resources/ui/FbTextView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2mQ;I)V
    .locals 2

    .prologue
    .line 1713235
    invoke-direct {p0, p1}, LX/Ao7;->a(LX/2mQ;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    invoke-virtual {p0}, LX/Ao7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1713236
    return-void
.end method

.method public final a(LX/2mQ;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1713220
    invoke-direct {p0, p1}, LX/Ao7;->a(LX/2mQ;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    invoke-virtual {v0, v1, v1, p2, v1}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1713221
    return-void
.end method

.method public final a(LX/2mQ;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1713218
    invoke-direct {p0, p1}, LX/Ao7;->a(LX/2mQ;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1713219
    return-void
.end method

.method public final b(LX/2mQ;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1713216
    invoke-direct {p0, p1}, LX/Ao7;->a(LX/2mQ;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    invoke-virtual {v0, p2, v1, v1, v1}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1713217
    return-void
.end method
