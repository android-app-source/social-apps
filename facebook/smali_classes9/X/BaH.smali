.class public final LX/BaH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/model/UploadShot;

.field public final synthetic b:Lcom/facebook/audience/direct/upload/BackstageUploadService;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/upload/BackstageUploadService;Lcom/facebook/audience/model/UploadShot;)V
    .locals 0

    .prologue
    .line 1798826
    iput-object p1, p0, LX/BaH;->b:Lcom/facebook/audience/direct/upload/BackstageUploadService;

    iput-object p2, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1798827
    sget-object v0, LX/BaI;->a:[I

    iget-object v1, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v1}, LX/7h8;->b(Lcom/facebook/audience/model/UploadShot;)LX/7h7;

    move-result-object v1

    invoke-virtual {v1}, LX/7h7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1798828
    :goto_0
    return-void

    .line 1798829
    :pswitch_0
    iget-object v0, p0, LX/BaH;->b:Lcom/facebook/audience/direct/upload/BackstageUploadService;

    iget-object v0, v0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->b:LX/1El;

    iget-object v1, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/1El;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1798830
    sget-object v0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->d:Ljava/lang/String;

    const-string v1, "Failed to save the shot"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1798831
    sget-object v0, LX/BaI;->a:[I

    iget-object v1, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v1}, LX/7h8;->b(Lcom/facebook/audience/model/UploadShot;)LX/7h7;

    move-result-object v1

    invoke-virtual {v1}, LX/7h7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1798832
    :goto_0
    return-void

    .line 1798833
    :pswitch_0
    iget-object v0, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    .line 1798834
    iget-object v0, p0, LX/BaH;->b:Lcom/facebook/audience/direct/upload/BackstageUploadService;

    iget-object v0, v0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->b:LX/1El;

    iget-object v1, p0, LX/BaH;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v0, v1}, LX/1El;->a(Lcom/facebook/audience/model/UploadShot;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
