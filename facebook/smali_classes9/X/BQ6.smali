.class public final enum LX/BQ6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BQ6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BQ6;

.field public static final enum FULL:LX/BQ6;

.field public static final enum NONE:LX/BQ6;

.field public static final enum SCALED_UP:LX/BQ6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1781456
    new-instance v0, LX/BQ6;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/BQ6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ6;->FULL:LX/BQ6;

    .line 1781457
    new-instance v0, LX/BQ6;

    const-string v1, "SCALED_UP"

    invoke-direct {v0, v1, v3}, LX/BQ6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ6;->SCALED_UP:LX/BQ6;

    .line 1781458
    new-instance v0, LX/BQ6;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/BQ6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQ6;->NONE:LX/BQ6;

    .line 1781459
    const/4 v0, 0x3

    new-array v0, v0, [LX/BQ6;

    sget-object v1, LX/BQ6;->FULL:LX/BQ6;

    aput-object v1, v0, v2

    sget-object v1, LX/BQ6;->SCALED_UP:LX/BQ6;

    aput-object v1, v0, v3

    sget-object v1, LX/BQ6;->NONE:LX/BQ6;

    aput-object v1, v0, v4

    sput-object v0, LX/BQ6;->$VALUES:[LX/BQ6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1781460
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BQ6;
    .locals 1

    .prologue
    .line 1781461
    const-class v0, LX/BQ6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BQ6;

    return-object v0
.end method

.method public static values()[LX/BQ6;
    .locals 1

    .prologue
    .line 1781462
    sget-object v0, LX/BQ6;->$VALUES:[LX/BQ6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BQ6;

    return-object v0
.end method
