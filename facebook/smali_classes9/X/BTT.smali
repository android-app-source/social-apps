.class public LX/BTT;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BTS;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787530
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1787531
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;ZLX/7Sv;)LX/BTS;
    .locals 12

    .prologue
    .line 1787532
    new-instance v0, LX/BTS;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v8

    check-cast v8, LX/1FZ;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v9

    check-cast v9, LX/1Ad;

    const-class v2, LX/BV0;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/BV0;

    invoke-static {p0}, LX/7SE;->b(LX/0QB;)LX/7SE;

    move-result-object v11

    check-cast v11, LX/7SE;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v11}, LX/BTS;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;ZLX/7Sv;LX/0TD;Ljava/util/concurrent/Executor;LX/1FZ;LX/1Ad;LX/BV0;LX/7SE;)V

    .line 1787533
    return-object v0
.end method
