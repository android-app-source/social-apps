.class public final enum LX/Am4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Am4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Am4;

.field public static final enum LOADING_TEXT:LX/Am4;

.field public static final enum SPINNER:LX/Am4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1710442
    new-instance v0, LX/Am4;

    const-string v1, "SPINNER"

    invoke-direct {v0, v1, v2}, LX/Am4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am4;->SPINNER:LX/Am4;

    .line 1710443
    new-instance v0, LX/Am4;

    const-string v1, "LOADING_TEXT"

    invoke-direct {v0, v1, v3}, LX/Am4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am4;->LOADING_TEXT:LX/Am4;

    .line 1710444
    const/4 v0, 0x2

    new-array v0, v0, [LX/Am4;

    sget-object v1, LX/Am4;->SPINNER:LX/Am4;

    aput-object v1, v0, v2

    sget-object v1, LX/Am4;->LOADING_TEXT:LX/Am4;

    aput-object v1, v0, v3

    sput-object v0, LX/Am4;->$VALUES:[LX/Am4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1710445
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Am4;
    .locals 1

    .prologue
    .line 1710446
    const-class v0, LX/Am4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Am4;

    return-object v0
.end method

.method public static values()[LX/Am4;
    .locals 1

    .prologue
    .line 1710447
    sget-object v0, LX/Am4;->$VALUES:[LX/Am4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Am4;

    return-object v0
.end method
