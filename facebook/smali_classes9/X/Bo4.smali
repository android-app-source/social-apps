.class public final LX/Bo4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic d:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 1821526
    iput-object p1, p0, LX/Bo4;->d:LX/1dt;

    iput-object p2, p0, LX/Bo4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Bo4;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Bo4;->c:Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1821527
    iget-object v0, p0, LX/Bo4;->d:LX/1dt;

    iget-object v1, p0, LX/Bo4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Bo4;->b:Ljava/lang/String;

    .line 1821528
    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1821529
    iget-object v0, p0, LX/Bo4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 1821530
    iget-object v2, p0, LX/Bo4;->d:LX/1dt;

    iget-object v0, p0, LX/Bo4;->c:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    .line 1821531
    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v3

    .line 1821532
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1821533
    iget-boolean p0, v3, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    iput-boolean p0, v3, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    .line 1821534
    iget-boolean p0, v3, Lcom/facebook/graphql/model/SponsoredImpression;->w:Z

    move v3, p0

    .line 1821535
    if-eqz v3, :cond_1

    const-string p0, "ad_useful"

    .line 1821536
    :goto_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v0, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "native_newsfeed"

    .line 1821537
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1821538
    move-object p0, p0

    .line 1821539
    move-object v3, p0

    .line 1821540
    iget-object p0, v2, LX/1dt;->E:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1821541
    return v4

    .line 1821542
    :cond_0
    const/4 p0, 0x0

    goto :goto_0

    .line 1821543
    :cond_1
    const-string p0, "ad_neutral"

    goto :goto_1
.end method
