.class public final enum LX/BVj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVj;

.field public static final enum BASELINE_ALIGNED:LX/BVj;

.field public static final enum BASELINE_ALIGNED_CHILD_INDEX:LX/BVj;

.field public static final enum DIVIDER:LX/BVj;

.field public static final enum GRAVITY:LX/BVj;

.field public static final enum MEASURE_WITH_LARGEST_CHILD:LX/BVj;

.field public static final enum ORIENTATION:LX/BVj;

.field public static final enum WEIGHT_SUM:LX/BVj;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791381
    new-instance v0, LX/BVj;

    const-string v1, "BASELINE_ALIGNED"

    const-string v2, "baselineAligned"

    invoke-direct {v0, v1, v4, v2}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->BASELINE_ALIGNED:LX/BVj;

    .line 1791382
    new-instance v0, LX/BVj;

    const-string v1, "BASELINE_ALIGNED_CHILD_INDEX"

    const-string v2, "baselineAlignedChildIndex"

    invoke-direct {v0, v1, v5, v2}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->BASELINE_ALIGNED_CHILD_INDEX:LX/BVj;

    .line 1791383
    new-instance v0, LX/BVj;

    const-string v1, "DIVIDER"

    const-string v2, "divider"

    invoke-direct {v0, v1, v6, v2}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->DIVIDER:LX/BVj;

    .line 1791384
    new-instance v0, LX/BVj;

    const-string v1, "GRAVITY"

    const-string v2, "gravity"

    invoke-direct {v0, v1, v7, v2}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->GRAVITY:LX/BVj;

    .line 1791385
    new-instance v0, LX/BVj;

    const-string v1, "MEASURE_WITH_LARGEST_CHILD"

    const-string v2, "measureWithLargestChild"

    invoke-direct {v0, v1, v8, v2}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->MEASURE_WITH_LARGEST_CHILD:LX/BVj;

    .line 1791386
    new-instance v0, LX/BVj;

    const-string v1, "ORIENTATION"

    const/4 v2, 0x5

    const-string v3, "orientation"

    invoke-direct {v0, v1, v2, v3}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->ORIENTATION:LX/BVj;

    .line 1791387
    new-instance v0, LX/BVj;

    const-string v1, "WEIGHT_SUM"

    const/4 v2, 0x6

    const-string v3, "weightSum"

    invoke-direct {v0, v1, v2, v3}, LX/BVj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVj;->WEIGHT_SUM:LX/BVj;

    .line 1791388
    const/4 v0, 0x7

    new-array v0, v0, [LX/BVj;

    sget-object v1, LX/BVj;->BASELINE_ALIGNED:LX/BVj;

    aput-object v1, v0, v4

    sget-object v1, LX/BVj;->BASELINE_ALIGNED_CHILD_INDEX:LX/BVj;

    aput-object v1, v0, v5

    sget-object v1, LX/BVj;->DIVIDER:LX/BVj;

    aput-object v1, v0, v6

    sget-object v1, LX/BVj;->GRAVITY:LX/BVj;

    aput-object v1, v0, v7

    sget-object v1, LX/BVj;->MEASURE_WITH_LARGEST_CHILD:LX/BVj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVj;->ORIENTATION:LX/BVj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVj;->WEIGHT_SUM:LX/BVj;

    aput-object v2, v0, v1

    sput-object v0, LX/BVj;->$VALUES:[LX/BVj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791390
    iput-object p3, p0, LX/BVj;->mValue:Ljava/lang/String;

    .line 1791391
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVj;
    .locals 4

    .prologue
    .line 1791392
    invoke-static {}, LX/BVj;->values()[LX/BVj;

    move-result-object v1

    .line 1791393
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791394
    aget-object v2, v1, v0

    .line 1791395
    iget-object v3, v2, LX/BVj;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791396
    return-object v2

    .line 1791397
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791398
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown linear layout attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVj;
    .locals 1

    .prologue
    .line 1791399
    const-class v0, LX/BVj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVj;

    return-object v0
.end method

.method public static values()[LX/BVj;
    .locals 1

    .prologue
    .line 1791400
    sget-object v0, LX/BVj;->$VALUES:[LX/BVj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVj;

    return-object v0
.end method
