.class public final LX/B4i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "profileOverlayCategory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "profileOverlayCategory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1742512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1742513
    return-void
.end method


# virtual methods
.method public final a(LX/15i;I)LX/B4i;
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "profileOverlayCategory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1742514
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/B4i;->b:LX/15i;

    iput p2, p0, LX/B4i;->c:I

    monitor-exit v1

    .line 1742515
    return-object p0

    .line 1742516
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1742517
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1742518
    iget-object v1, p0, LX/B4i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1742519
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v5, p0, LX/B4i;->b:LX/15i;

    iget v6, p0, LX/B4i;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, -0x378f21cf

    invoke-static {v5, v6, v3}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1742520
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1742521
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1742522
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1742523
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1742524
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1742525
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1742526
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1742527
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1742528
    new-instance v1, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;-><init>(LX/15i;)V

    .line 1742529
    return-object v1

    .line 1742530
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
