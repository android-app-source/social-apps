.class public abstract LX/AVj;
.super LX/AVi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        "M::",
        "LX/AW2;",
        ">",
        "LX/AVi",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TM;>;"
        }
    .end annotation
.end field

.field public b:LX/AW2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TM;"
        }
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1680210
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1680211
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TM;>;)V"
        }
    .end annotation

    .prologue
    .line 1680219
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1680220
    :goto_0
    return-void

    .line 1680221
    :cond_0
    invoke-static {p1}, LX/0R9;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    .line 1680222
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    new-instance v1, LX/AVm;

    invoke-direct {v1, p0}, LX/AVm;-><init>(LX/AVj;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TV;)V"
        }
    .end annotation

    .prologue
    .line 1680218
    return-void
.end method

.method public abstract a(LX/AW2;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation
.end method

.method public b(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1680217
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1680216
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 1680213
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1680214
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1680215
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 1680212
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    invoke-virtual {p0, v0}, LX/AVj;->a(LX/AW2;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    invoke-virtual {p0, v0}, LX/AVj;->a(LX/AW2;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract g()V
.end method
