.class public final LX/C0f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2yi;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;LX/2yi;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1841196
    iput-object p1, p0, LX/C0f;->e:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    iput-object p2, p0, LX/C0f;->a:LX/2yi;

    iput-object p3, p0, LX/C0f;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/C0f;->c:LX/1Pq;

    iput-object p5, p0, LX/C0f;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, 0x588368f5

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1841197
    iget-object v0, p0, LX/C0f;->a:LX/2yi;

    .line 1841198
    iget-boolean v4, v0, LX/2yi;->b:Z

    move v0, v4

    .line 1841199
    if-eqz v0, :cond_0

    .line 1841200
    iget-object v0, p0, LX/C0f;->e:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->b:LX/1Sj;

    iget-object v4, p0, LX/C0f;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v5, "offline_toggle_button"

    const-string v6, "native_story"

    invoke-virtual {v0, v4, v5, v6}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1841201
    :goto_0
    iget-object v4, p0, LX/C0f;->a:LX/2yi;

    iget-object v0, p0, LX/C0f;->a:LX/2yi;

    .line 1841202
    iget-boolean v5, v0, LX/2yi;->b:Z

    move v0, v5

    .line 1841203
    if-nez v0, :cond_2

    move v0, v1

    .line 1841204
    :goto_1
    iput-boolean v0, v4, LX/2yi;->b:Z

    .line 1841205
    iget-object v0, p0, LX/C0f;->c:LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/C0f;->d:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 1841206
    const v0, 0x18ae35b3

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1841207
    :cond_0
    iget-object v0, p0, LX/C0f;->e:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    .line 1841208
    iget-object v7, v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->c:LX/0W3;

    sget-wide v9, LX/0X5;->hj:J

    const/4 v8, 0x0

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/1vD;->convertString(Ljava/lang/String;)LX/1vD;

    move-result-object v7

    .line 1841209
    sget-object v8, LX/1vD;->ELIGIBLE:LX/1vD;

    invoke-virtual {v8, v7}, LX/1vD;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1841210
    iget-object v7, v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/1vE;->e:LX/0Tn;

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 1841211
    :cond_1
    iget-object v0, p0, LX/C0f;->e:Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/offlinesave/OfflineAttachmentSaveFooterPartDefinition;->b:LX/1Sj;

    iget-object v4, p0, LX/C0f;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v5, "offline_toggle_button"

    const-string v6, "native_story"

    invoke-virtual {v0, v4, v5, v6}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1841212
    goto :goto_1
.end method
