.class public final enum LX/B1T;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B1T;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B1T;

.field public static final enum AvailableForSalePosts:LX/B1T;

.field public static final enum CommunityTrendingStoriesPosts:LX/B1T;

.field public static final enum CommununityForSalePosts:LX/B1T;

.field public static final enum CrossGroupForSalePosts:LX/B1T;

.field public static final enum GroupDiscussionTopicsPosts:LX/B1T;

.field public static final enum GroupInboxPostByGroup:LX/B1T;

.field public static final enum GroupLearningUnitPosts:LX/B1T;

.field public static final enum GroupMemberPosts:LX/B1T;

.field public static final enum GroupStoryDiveinPosts:LX/B1T;

.field public static final enum GroupsFeed:LX/B1T;

.field public static final enum GroupsStories:LX/B1T;

.field public static final enum PendingPosts:LX/B1T;

.field public static final enum PinnedPosts:LX/B1T;

.field public static final enum ReportedPosts:LX/B1T;

.field public static final enum YourAvailableForSalePosts:LX/B1T;

.field public static final enum YourExpiredForSalePosts:LX/B1T;

.field public static final enum YourSoldForSalePosts:LX/B1T;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1736001
    new-instance v0, LX/B1T;

    const-string v1, "PinnedPosts"

    invoke-direct {v0, v1, v3}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->PinnedPosts:LX/B1T;

    .line 1736002
    new-instance v0, LX/B1T;

    const-string v1, "PendingPosts"

    invoke-direct {v0, v1, v4}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->PendingPosts:LX/B1T;

    .line 1736003
    new-instance v0, LX/B1T;

    const-string v1, "ReportedPosts"

    invoke-direct {v0, v1, v5}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->ReportedPosts:LX/B1T;

    .line 1736004
    new-instance v0, LX/B1T;

    const-string v1, "GroupsFeed"

    invoke-direct {v0, v1, v6}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupsFeed:LX/B1T;

    .line 1736005
    new-instance v0, LX/B1T;

    const-string v1, "GroupsStories"

    invoke-direct {v0, v1, v7}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupsStories:LX/B1T;

    .line 1736006
    new-instance v0, LX/B1T;

    const-string v1, "YourAvailableForSalePosts"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->YourAvailableForSalePosts:LX/B1T;

    .line 1736007
    new-instance v0, LX/B1T;

    const-string v1, "YourSoldForSalePosts"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->YourSoldForSalePosts:LX/B1T;

    .line 1736008
    new-instance v0, LX/B1T;

    const-string v1, "YourExpiredForSalePosts"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->YourExpiredForSalePosts:LX/B1T;

    .line 1736009
    new-instance v0, LX/B1T;

    const-string v1, "AvailableForSalePosts"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->AvailableForSalePosts:LX/B1T;

    .line 1736010
    new-instance v0, LX/B1T;

    const-string v1, "CrossGroupForSalePosts"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->CrossGroupForSalePosts:LX/B1T;

    .line 1736011
    new-instance v0, LX/B1T;

    const-string v1, "GroupDiscussionTopicsPosts"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupDiscussionTopicsPosts:LX/B1T;

    .line 1736012
    new-instance v0, LX/B1T;

    const-string v1, "GroupMemberPosts"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupMemberPosts:LX/B1T;

    .line 1736013
    new-instance v0, LX/B1T;

    const-string v1, "GroupLearningUnitPosts"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupLearningUnitPosts:LX/B1T;

    .line 1736014
    new-instance v0, LX/B1T;

    const-string v1, "GroupStoryDiveinPosts"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupStoryDiveinPosts:LX/B1T;

    .line 1736015
    new-instance v0, LX/B1T;

    const-string v1, "CommununityForSalePosts"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->CommununityForSalePosts:LX/B1T;

    .line 1736016
    new-instance v0, LX/B1T;

    const-string v1, "CommunityTrendingStoriesPosts"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->CommunityTrendingStoriesPosts:LX/B1T;

    .line 1736017
    new-instance v0, LX/B1T;

    const-string v1, "GroupInboxPostByGroup"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/B1T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1T;->GroupInboxPostByGroup:LX/B1T;

    .line 1736018
    const/16 v0, 0x11

    new-array v0, v0, [LX/B1T;

    sget-object v1, LX/B1T;->PinnedPosts:LX/B1T;

    aput-object v1, v0, v3

    sget-object v1, LX/B1T;->PendingPosts:LX/B1T;

    aput-object v1, v0, v4

    sget-object v1, LX/B1T;->ReportedPosts:LX/B1T;

    aput-object v1, v0, v5

    sget-object v1, LX/B1T;->GroupsFeed:LX/B1T;

    aput-object v1, v0, v6

    sget-object v1, LX/B1T;->GroupsStories:LX/B1T;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/B1T;->YourAvailableForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/B1T;->YourSoldForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/B1T;->YourExpiredForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/B1T;->AvailableForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/B1T;->CrossGroupForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/B1T;->GroupDiscussionTopicsPosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/B1T;->GroupMemberPosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/B1T;->GroupLearningUnitPosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/B1T;->GroupStoryDiveinPosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/B1T;->CommununityForSalePosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/B1T;->CommunityTrendingStoriesPosts:LX/B1T;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/B1T;->GroupInboxPostByGroup:LX/B1T;

    aput-object v2, v0, v1

    sput-object v0, LX/B1T;->$VALUES:[LX/B1T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1735998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B1T;
    .locals 1

    .prologue
    .line 1736000
    const-class v0, LX/B1T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B1T;

    return-object v0
.end method

.method public static values()[LX/B1T;
    .locals 1

    .prologue
    .line 1735999
    sget-object v0, LX/B1T;->$VALUES:[LX/B1T;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B1T;

    return-object v0
.end method
