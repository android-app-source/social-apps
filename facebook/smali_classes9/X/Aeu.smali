.class public LX/Aeu;
.super LX/AeP;
.source ""

# interfaces
.implements LX/Aea;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:I

.field public n:I

.field public o:Z

.field public p:Z

.field public q:Z


# direct methods
.method private constructor <init>(LX/AcC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;ZZZII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1698384
    invoke-direct {p0, p1}, LX/AeP;-><init>(LX/AcC;)V

    .line 1698385
    iput-object p2, p0, LX/Aeu;->b:Ljava/lang/String;

    .line 1698386
    invoke-static {p2}, LX/Aeu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Aeu;->c:Ljava/lang/String;

    .line 1698387
    iput-object p3, p0, LX/Aeu;->d:Ljava/lang/String;

    .line 1698388
    iput-object p4, p0, LX/Aeu;->e:Ljava/lang/String;

    .line 1698389
    iput-object p5, p0, LX/Aeu;->f:Ljava/lang/String;

    .line 1698390
    iput-object p6, p0, LX/Aeu;->j:Ljava/lang/String;

    .line 1698391
    invoke-static {p6}, LX/Aeu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Aeu;->k:Ljava/lang/String;

    .line 1698392
    iput-object p7, p0, LX/Aeu;->i:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 1698393
    iput-boolean p8, p0, LX/Aeu;->g:Z

    .line 1698394
    iput-boolean p9, p0, LX/Aeu;->h:Z

    .line 1698395
    iput-boolean p10, p0, LX/Aeu;->l:Z

    .line 1698396
    iput p11, p0, LX/Aeu;->n:I

    .line 1698397
    iput p12, p0, LX/Aeu;->m:I

    .line 1698398
    iget-object v0, p0, LX/Aeu;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/Aeu;->p:Z

    .line 1698399
    if-eqz p6, :cond_0

    iget-object v0, p0, LX/Aeu;->k:Ljava/lang/String;

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, LX/Aeu;->q:Z

    .line 1698400
    iput-boolean v1, p0, LX/Aeu;->o:Z

    .line 1698401
    return-void

    :cond_0
    move v0, v1

    .line 1698402
    goto :goto_0
.end method

.method public static a(LX/AcC;Ljava/lang/String;I)LX/Aeu;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1698302
    new-instance v0, LX/Aeu;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    const/4 v11, -0x1

    move-object v1, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move v9, v8

    move v10, v8

    move v12, p2

    invoke-direct/range {v0 .. v12}, LX/Aeu;-><init>(LX/AcC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;ZZZII)V

    return-object v0
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)LX/Aeu;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 1698303
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    .line 1698304
    if-nez v0, :cond_0

    move-object v0, v8

    .line 1698305
    :goto_0
    return-object v0

    .line 1698306
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1698307
    if-nez v2, :cond_1

    move-object v0, v8

    .line 1698308
    goto :goto_0

    .line 1698309
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v0

    invoke-static {v0}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v1

    .line 1698310
    if-nez v1, :cond_2

    move-object v0, v8

    .line 1698311
    goto :goto_0

    .line 1698312
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 1698313
    if-nez v5, :cond_3

    move-object v0, v8

    .line 1698314
    goto :goto_0

    .line 1698315
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 1698316
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->p()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1698317
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698318
    if-eqz v4, :cond_a

    .line 1698319
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v6, v4, v3, v0, v7}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 1698320
    invoke-virtual {v6, v4, v10}, LX/15i;->g(II)I

    move-result v7

    if-eqz v7, :cond_a

    .line 1698321
    invoke-virtual {v6, v4, v10}, LX/15i;->g(II)I

    move-result v4

    invoke-virtual {v6, v4, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    move-object v7, v0

    .line 1698322
    :goto_1
    if-nez v6, :cond_6

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_4

    .line 1698323
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1698324
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;

    move-result-object v9

    .line 1698325
    if-nez v9, :cond_7

    move-object v0, v8

    .line 1698326
    goto :goto_0

    .line 1698327
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v10

    .line 1698328
    goto :goto_2

    :cond_6
    move v0, v10

    goto :goto_2

    .line 1698329
    :cond_7
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 1698330
    if-nez v3, :cond_8

    move-object v0, v8

    .line 1698331
    goto :goto_0

    .line 1698332
    :cond_8
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 1698333
    if-nez v4, :cond_9

    move-object v0, v8

    .line 1698334
    goto :goto_0

    .line 1698335
    :cond_9
    new-instance v0, LX/Aeu;

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->k()Z

    move-result v8

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->j()Z

    move-result v9

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;->n()I

    move-result v11

    move v12, v10

    invoke-direct/range {v0 .. v12}, LX/Aeu;-><init>(LX/AcC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;ZZZII)V

    goto/16 :goto_0

    :cond_a
    move-object v7, v0

    move-object v6, v8

    goto :goto_1
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel;)LX/Aeu;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1698336
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;

    move-result-object v12

    .line 1698337
    if-nez v12, :cond_0

    move-object v0, v8

    .line 1698338
    :goto_0
    return-object v0

    .line 1698339
    :cond_0
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;

    move-result-object v0

    .line 1698340
    if-nez v0, :cond_1

    move-object v0, v8

    .line 1698341
    goto :goto_0

    .line 1698342
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithAggregatedRangesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1698343
    if-nez v2, :cond_2

    move-object v0, v8

    .line 1698344
    goto :goto_0

    .line 1698345
    :cond_2
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v0

    invoke-static {v0}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v1

    .line 1698346
    if-nez v1, :cond_3

    move-object v0, v8

    .line 1698347
    goto :goto_0

    .line 1698348
    :cond_3
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 1698349
    if-nez v5, :cond_4

    move-object v0, v8

    .line 1698350
    goto :goto_0

    .line 1698351
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->NO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 1698352
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->p()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1698353
    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698354
    if-eqz v6, :cond_b

    .line 1698355
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v7, v6, v3, v0, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 1698356
    invoke-virtual {v7, v6, v4}, LX/15i;->g(II)I

    move-result v9

    if-eqz v9, :cond_b

    .line 1698357
    invoke-virtual {v7, v6, v4}, LX/15i;->g(II)I

    move-result v6

    invoke-virtual {v7, v6, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    move-object v7, v0

    .line 1698358
    :goto_1
    if-nez v6, :cond_7

    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1698359
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1698360
    :cond_5
    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->l()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;

    move-result-object v9

    .line 1698361
    if-nez v9, :cond_8

    move-object v0, v8

    .line 1698362
    goto :goto_0

    .line 1698363
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move v0, v4

    .line 1698364
    goto :goto_2

    :cond_7
    move v0, v4

    goto :goto_2

    .line 1698365
    :cond_8
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 1698366
    if-nez v3, :cond_9

    move-object v0, v8

    .line 1698367
    goto :goto_0

    .line 1698368
    :cond_9
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 1698369
    if-nez v4, :cond_a

    move-object v0, v8

    .line 1698370
    goto/16 :goto_0

    .line 1698371
    :cond_a
    new-instance v0, LX/Aeu;

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->k()Z

    move-result v8

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel$FeedbackModel;->j()Z

    move-result v9

    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->r()Z

    move-result v10

    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->n()I

    move-result v11

    invoke-virtual {v12}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoVODVideoTimestampedCommentsModel$EdgesModel$NodeModel;->o()I

    move-result v12

    invoke-direct/range {v0 .. v12}, LX/Aeu;-><init>(LX/AcC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;ZZZII)V

    goto/16 :goto_0

    :cond_b
    move-object v7, v0

    move-object v6, v8

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v2, 0xa

    .line 1698372
    if-nez p0, :cond_1

    .line 1698373
    const/4 v0, 0x0

    .line 1698374
    :cond_0
    :goto_0
    return-object v0

    .line 1698375
    :cond_1
    const/16 v0, 0xc8

    invoke-static {p0, v0}, LX/0YN;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1698376
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1698377
    if-ltz v1, :cond_0

    .line 1698378
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 1698379
    if-ltz v1, :cond_0

    .line 1698380
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/AeN;
    .locals 1

    .prologue
    .line 1698381
    sget-object v0, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1698382
    iget v0, p0, LX/Aeu;->m:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1698383
    iget-boolean v0, p0, LX/Aeu;->l:Z

    return v0
.end method
