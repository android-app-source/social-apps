.class public LX/Amf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pe;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0rq;

.field private final f:LX/0sa;

.field private g:I


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0rq;",
            "LX/0sa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1711253
    const/4 v0, 0x0

    iput v0, p0, LX/Amf;->g:I

    .line 1711254
    iput-object p1, p0, LX/Amf;->a:LX/0Sh;

    .line 1711255
    iput-object p2, p0, LX/Amf;->b:Ljava/util/concurrent/ExecutorService;

    .line 1711256
    iput-object p3, p0, LX/Amf;->c:LX/0Ot;

    .line 1711257
    iput-object p4, p0, LX/Amf;->d:LX/0Ot;

    .line 1711258
    iput-object p5, p0, LX/Amf;->e:LX/0rq;

    .line 1711259
    iput-object p6, p0, LX/Amf;->f:LX/0sa;

    .line 1711260
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1711316
    new-instance v0, LX/ADo;

    invoke-direct {v0}, LX/ADo;-><init>()V

    move-object v0, v0

    .line 1711317
    const-string v1, "poll_option_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_pic_media_type"

    iget-object v2, p0, LX/Amf;->e:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->b()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "after_poll_option_voters"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "max_poll_option_voters"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/ADo;

    .line 1711318
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Amf;
    .locals 10

    .prologue
    .line 1711305
    const-class v1, LX/Amf;

    monitor-enter v1

    .line 1711306
    :try_start_0
    sget-object v0, LX/Amf;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1711307
    sput-object v2, LX/Amf;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1711308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1711310
    new-instance v3, LX/Amf;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0xafd

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xafc

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v9

    check-cast v9, LX/0sa;

    invoke-direct/range {v3 .. v9}, LX/Amf;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V

    .line 1711311
    move-object v0, v3

    .line 1711312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1711313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Amf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1711315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1711304
    new-instance v0, LX/Ame;

    invoke-direct {v0, p0}, LX/Ame;-><init>(LX/Amf;)V

    iget-object v1, p0, LX/Amf;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Px;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1711280
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1711281
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;

    .line 1711282
    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2, v2}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1711283
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1711284
    iput-object v7, v6, LX/3dL;->E:Ljava/lang/String;

    .line 1711285
    move-object v6, v6

    .line 1711286
    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 1711287
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 1711288
    move-object v6, v6

    .line 1711289
    iput-object v5, v6, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1711290
    move-object v5, v6

    .line 1711291
    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->e()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    .line 1711292
    iput-object v6, v5, LX/3dL;->aK:Ljava/lang/String;

    .line 1711293
    move-object v5, v5

    .line 1711294
    new-instance v6, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v7, 0x285feb

    invoke-direct {v6, v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1711295
    iput-object v6, v5, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1711296
    move-object v5, v5

    .line 1711297
    invoke-virtual {v0}, Lcom/facebook/api/graphql/poll/FetchPollVotersGraphQLModels$FetchPollVotersModel$VotersModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1711298
    iput-object v0, v5, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1711299
    move-object v0, v5

    .line 1711300
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1711301
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1711302
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1711303
    :cond_0
    return-object v3
.end method


# virtual methods
.method public final a()LX/89l;
    .locals 1

    .prologue
    .line 1711279
    sget-object v0, LX/89l;->VOTERS_FOR_POLL_OPTION_ID:LX/89l;

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1711269
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1711270
    invoke-direct {p0, v0, p5}, LX/Amf;->a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1711271
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1711272
    move-object v1, v0

    .line 1711273
    iget-object v0, p0, LX/Amf;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, LX/Amd;

    invoke-direct {v2, p0, p3}, LX/Amd;-><init>(LX/Amf;LX/0TF;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1711274
    iget-object v4, v1, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 1711275
    iget-object v5, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, v5

    .line 1711276
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, LX/Amf;->g:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/Amf;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1711277
    iget-object v1, p0, LX/Amf;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/Amf;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1711278
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1711264
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1711265
    invoke-direct {p0, v0, p4}, LX/Amf;->a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1711266
    iget-object v0, p0, LX/Amf;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1711267
    iget-object v1, p0, LX/Amf;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/Amf;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1711268
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1711263
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1711261
    iget-object v0, p0, LX/Amf;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1711262
    return-void
.end method
