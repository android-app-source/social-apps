.class public LX/Bn1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

.field private b:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

.field private c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/events/model/PrivacyType;

.field public f:Z

.field private g:Z

.field private h:Z

.field private final i:Landroid/content/Context;

.field public j:Z

.field public final k:LX/0iA;

.field public final l:LX/3kp;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0iA;LX/3kp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1819741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1819742
    iput-object p1, p0, LX/Bn1;->i:Landroid/content/Context;

    .line 1819743
    iput-object p2, p0, LX/Bn1;->k:LX/0iA;

    .line 1819744
    iput-object p3, p0, LX/Bn1;->l:LX/3kp;

    .line 1819745
    iget-object v0, p0, LX/Bn1;->l:LX/3kp;

    sget-object v1, LX/7vb;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1819746
    iget-object v0, p0, LX/Bn1;->l:LX/3kp;

    const/4 v1, 0x5

    .line 1819747
    iput v1, v0, LX/3kp;->b:I

    .line 1819748
    return-void
.end method

.method public static a(LX/0QB;)LX/Bn1;
    .locals 1

    .prologue
    .line 1819740
    invoke-static {p0}, LX/Bn1;->b(LX/0QB;)LX/Bn1;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/Bn1;
    .locals 4

    .prologue
    .line 1819738
    new-instance v3, LX/Bn1;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v2

    check-cast v2, LX/3kp;

    invoke-direct {v3, v0, v1, v2}, LX/Bn1;-><init>(Landroid/content/Context;LX/0iA;LX/3kp;)V

    .line 1819739
    return-object v3
.end method

.method private b(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1819729
    sget-object v0, LX/Bn0;->a:[I

    invoke-virtual {p1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819730
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819731
    :pswitch_0
    iget-boolean v0, p0, LX/Bn1;->h:Z

    if-eqz v0, :cond_0

    .line 1819732
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b10

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1819733
    :goto_0
    return-object v0

    .line 1819734
    :cond_0
    :pswitch_1
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082af5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819735
    :pswitch_2
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082af4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819736
    :pswitch_3
    invoke-direct {p0}, LX/Bn1;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819737
    :pswitch_4
    invoke-direct {p0}, LX/Bn1;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static c(Lcom/facebook/events/model/PrivacyType;)I
    .locals 3

    .prologue
    .line 1819723
    sget-object v0, LX/Bn0;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819724
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819725
    :pswitch_1
    const v0, 0x7f082af3

    .line 1819726
    :goto_0
    return v0

    .line 1819727
    :pswitch_2
    const v0, 0x7f082af9

    goto :goto_0

    .line 1819728
    :pswitch_3
    const v0, 0x7f082aff

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 1819721
    iget-object v0, p0, LX/Bn1;->k:LX/0iA;

    sget-object v1, LX/3ki;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3ki;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ki;

    .line 1819722
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3ki;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3764"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1819718
    iget-object v0, p0, LX/Bn1;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1819719
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b04

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1819720
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b02

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Bn1;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1819713
    iget-object v0, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    .line 1819714
    iget-object v1, v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->p:Ljava/lang/String;

    move-object v0, v1

    .line 1819715
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1819716
    const-string v0, "Community Event"

    .line 1819717
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082b02

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1819710
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Bn1;->a(Z)V

    .line 1819711
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p0, v0}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;)V

    .line 1819712
    return-void
.end method

.method public final a(Lcom/facebook/events/model/PrivacyType;)V
    .locals 2

    .prologue
    .line 1819749
    if-nez p1, :cond_0

    iget-object p1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    :cond_0
    iget-boolean v0, p0, LX/Bn1;->f:Z

    iget-boolean v1, p0, LX/Bn1;->g:Z

    invoke-virtual {p0, p1, v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;ZZ)V

    .line 1819750
    return-void
.end method

.method public final a(Lcom/facebook/events/model/PrivacyType;Z)V
    .locals 1

    .prologue
    .line 1819708
    if-nez p1, :cond_0

    iget-object p1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;ZZ)V

    .line 1819709
    return-void
.end method

.method public final a(Lcom/facebook/events/model/PrivacyType;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1819683
    iput-object p1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    .line 1819684
    iput-boolean p2, p0, LX/Bn1;->f:Z

    .line 1819685
    iput-boolean p3, p0, LX/Bn1;->g:Z

    .line 1819686
    iget-object v0, p0, LX/Bn1;->b:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    if-eqz v0, :cond_0

    .line 1819687
    iget-object v0, p0, LX/Bn1;->b:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819688
    :cond_0
    iget-object v0, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    if-eqz v0, :cond_1

    .line 1819689
    iget-object v0, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    iget-boolean v1, p0, LX/Bn1;->g:Z

    invoke-virtual {p0}, LX/Bn1;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Lcom/facebook/events/model/PrivacyType;ZZLjava/lang/String;)V

    .line 1819690
    iget-object v1, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_6

    const/4 v0, 0x1

    .line 1819691
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    .line 1819692
    :cond_1
    iget-object v0, p0, LX/Bn1;->c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;

    if-eqz v0, :cond_3

    .line 1819693
    iget-object v0, p0, LX/Bn1;->c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Lcom/facebook/events/model/PrivacyType;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1819694
    if-eqz v0, :cond_2

    iget-boolean v1, p0, LX/Bn1;->j:Z

    if-eqz v1, :cond_7

    .line 1819695
    :cond_2
    iget-object v1, p0, LX/Bn1;->c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Ljava/lang/CharSequence;)V

    .line 1819696
    :cond_3
    :goto_1
    iget-object v0, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    if-eqz v0, :cond_5

    .line 1819697
    iget-object v0, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    invoke-virtual {v0, p1}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1819698
    if-eqz v0, :cond_4

    iget-boolean v1, p0, LX/Bn1;->j:Z

    if-eqz v1, :cond_9

    .line 1819699
    :cond_4
    iget-object v1, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a(Ljava/lang/CharSequence;)V

    .line 1819700
    :cond_5
    :goto_2
    return-void

    .line 1819701
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 1819702
    :cond_7
    iget-object v1, p0, LX/Bn1;->l:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, LX/Bn1;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1819703
    iget-object v1, p0, LX/Bn1;->c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1819704
    :cond_8
    iget-object v0, p0, LX/Bn1;->c:Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;

    invoke-virtual {v0, v3}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1819705
    :cond_9
    iget-object v1, p0, LX/Bn1;->l:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0}, LX/Bn1;->f()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1819706
    iget-object v1, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1819707
    :cond_a
    iget-object v0, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    invoke-virtual {v0, v3}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;)V
    .locals 2
    .param p1    # Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1819679
    iput-object p1, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    .line 1819680
    iget-object v0, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    if-eqz v0, :cond_0

    .line 1819681
    iget-object v0, p0, LX/Bn1;->d:Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    new-instance v1, LX/Bmz;

    invoke-direct {v1, p0}, LX/Bmz;-><init>(LX/Bn1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->setOnDrawListenerTo(LX/0fu;)V

    .line 1819682
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;)V
    .locals 1

    .prologue
    .line 1819675
    iput-object p1, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    .line 1819676
    iput-object p2, p0, LX/Bn1;->b:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    .line 1819677
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Bn1;->j:Z

    .line 1819678
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1819665
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne p2, v0, :cond_1

    .line 1819666
    :cond_0
    :goto_0
    return-void

    .line 1819667
    :cond_1
    invoke-virtual {p0, v1}, LX/Bn1;->a(Z)V

    .line 1819668
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne p2, v0, :cond_2

    .line 1819669
    iput-object p3, p0, LX/Bn1;->m:Ljava/lang/String;

    .line 1819670
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p0, v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    goto :goto_0

    .line 1819671
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq p2, v0, :cond_3

    .line 1819672
    iput-object p1, p0, LX/Bn1;->m:Ljava/lang/String;

    .line 1819673
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p0, v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    goto :goto_0

    .line 1819674
    :cond_3
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p0, v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1819661
    iput-boolean p1, p0, LX/Bn1;->h:Z

    .line 1819662
    iget-object v0, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    if-eqz v0, :cond_0

    .line 1819663
    iget-object v0, p0, LX/Bn1;->a:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    invoke-virtual {v0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setPrivacyLocked(Z)V

    .line 1819664
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1819658
    iget-boolean v0, p0, LX/Bn1;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/Bn1;->h:Z

    if-eqz v0, :cond_1

    .line 1819659
    :cond_0
    iget-object v0, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    invoke-direct {p0, v0}, LX/Bn1;->b(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;

    move-result-object v0

    .line 1819660
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Bn1;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v1}, LX/Bn1;->c(Lcom/facebook/events/model/PrivacyType;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1819650
    if-eqz p1, :cond_0

    .line 1819651
    const-string v0, "copy_event"

    .line 1819652
    :goto_0
    return-object v0

    .line 1819653
    :cond_0
    sget-object v0, LX/Bn0;->a:[I

    iget-object v1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819654
    :pswitch_0
    const-string v0, "user_create_dialog"

    goto :goto_0

    .line 1819655
    :pswitch_1
    const-string v0, "user_public_create_dialog"

    goto :goto_0

    .line 1819656
    :pswitch_2
    const-string v0, "page_create_dialog"

    goto :goto_0

    .line 1819657
    :pswitch_3
    const-string v0, "group_create_dialog"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/EventPrivacyEnum;
    .end annotation

    .prologue
    .line 1819642
    sget-object v0, LX/Bn0;->a:[I

    iget-object v1, p0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819643
    const-string v0, "GROUP"

    :goto_0
    return-object v0

    .line 1819644
    :pswitch_0
    const-string v0, "FRIENDS_OF_FRIENDS"

    goto :goto_0

    .line 1819645
    :pswitch_1
    const-string v0, "FRIENDS_OF_GUESTS"

    goto :goto_0

    .line 1819646
    :pswitch_2
    const-string v0, "INVITE_ONLY"

    goto :goto_0

    .line 1819647
    :pswitch_3
    const-string v0, "PAGE"

    goto :goto_0

    .line 1819648
    :pswitch_4
    const-string v0, "USER_PUBLIC"

    goto :goto_0

    .line 1819649
    :pswitch_5
    const-string v0, "COMMUNITY"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
