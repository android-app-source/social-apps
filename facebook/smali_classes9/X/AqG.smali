.class public final LX/AqG;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AqH;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/1dQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1717419
    invoke-static {}, LX/AqH;->q()LX/AqH;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1717420
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1717418
    const-string v0, "FigSwitch"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1717408
    if-ne p0, p1, :cond_1

    .line 1717409
    :cond_0
    :goto_0
    return v0

    .line 1717410
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1717411
    goto :goto_0

    .line 1717412
    :cond_3
    check-cast p1, LX/AqG;

    .line 1717413
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1717414
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1717415
    if-eq v2, v3, :cond_0

    .line 1717416
    iget-boolean v2, p0, LX/AqG;->a:Z

    iget-boolean v3, p1, LX/AqG;->a:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1717417
    goto :goto_0
.end method
