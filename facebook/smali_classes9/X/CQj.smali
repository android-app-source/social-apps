.class public final LX/CQj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Z

.field public z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1887192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;
    .locals 31

    .prologue
    .line 1887193
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1887194
    move-object/from16 v0, p0

    iget-object v2, v0, LX/CQj;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1887195
    move-object/from16 v0, p0

    iget-object v3, v0, LX/CQj;->d:LX/0Px;

    invoke-virtual {v1, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1887196
    move-object/from16 v0, p0

    iget-object v4, v0, LX/CQj;->e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-virtual {v1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1887197
    move-object/from16 v0, p0

    iget-object v5, v0, LX/CQj;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1887198
    move-object/from16 v0, p0

    iget-object v6, v0, LX/CQj;->i:LX/0Px;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1887199
    move-object/from16 v0, p0

    iget-object v7, v0, LX/CQj;->j:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1887200
    move-object/from16 v0, p0

    iget-object v8, v0, LX/CQj;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1887201
    move-object/from16 v0, p0

    iget-object v9, v0, LX/CQj;->m:Ljava/lang/String;

    invoke-virtual {v1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1887202
    move-object/from16 v0, p0

    iget-object v10, v0, LX/CQj;->n:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1887203
    move-object/from16 v0, p0

    iget-object v11, v0, LX/CQj;->o:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1887204
    move-object/from16 v0, p0

    iget-object v12, v0, LX/CQj;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1887205
    move-object/from16 v0, p0

    iget-object v13, v0, LX/CQj;->q:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-virtual {v1, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1887206
    move-object/from16 v0, p0

    iget-object v14, v0, LX/CQj;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1887207
    move-object/from16 v0, p0

    iget-object v15, v0, LX/CQj;->s:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-virtual {v1, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1887208
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->t:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1887209
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->u:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1887210
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1887211
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1887212
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->x:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1887213
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1887214
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1887215
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->B:LX/0Px;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v23

    .line 1887216
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->C:LX/0Px;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v24

    .line 1887217
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->D:LX/0Px;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v25

    .line 1887218
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->F:LX/0Px;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v26

    .line 1887219
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->G:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1887220
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->H:LX/0Px;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v28

    .line 1887221
    move-object/from16 v0, p0

    iget-object v0, v0, LX/CQj;->I:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    .line 1887222
    const/16 v30, 0x23

    move/from16 v0, v30

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1887223
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1887224
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/CQj;->b:Z

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 1887225
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/CQj;->c:Z

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 1887226
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1887227
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1887228
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/CQj;->f:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1887229
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/CQj;->g:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1887230
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1887231
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1887232
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1887233
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/CQj;->k:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1887234
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1887235
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1887236
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1887237
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1887238
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1887239
    const/16 v2, 0x10

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1887240
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1887241
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1887242
    const/16 v2, 0x13

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887243
    const/16 v2, 0x14

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887244
    const/16 v2, 0x15

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887245
    const/16 v2, 0x16

    move/from16 v0, v19

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887246
    const/16 v2, 0x17

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887247
    const/16 v2, 0x18

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/CQj;->y:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1887248
    const/16 v2, 0x19

    move/from16 v0, v21

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887249
    const/16 v2, 0x1a

    move/from16 v0, v22

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887250
    const/16 v2, 0x1b

    move/from16 v0, v23

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887251
    const/16 v2, 0x1c

    move/from16 v0, v24

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887252
    const/16 v2, 0x1d

    move/from16 v0, v25

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887253
    const/16 v2, 0x1e

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/CQj;->E:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1887254
    const/16 v2, 0x1f

    move/from16 v0, v26

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887255
    const/16 v2, 0x20

    move/from16 v0, v27

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887256
    const/16 v2, 0x21

    move/from16 v0, v28

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887257
    const/16 v2, 0x22

    move/from16 v0, v29

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1887258
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1887259
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1887260
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1887261
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1887262
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1887263
    new-instance v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    invoke-direct {v2, v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;-><init>(LX/15i;)V

    .line 1887264
    return-object v2
.end method
