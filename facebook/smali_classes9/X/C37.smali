.class public final LX/C37;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C39;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C38;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C39",
            "<TE;>.CondensedStoryBodyComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C39;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C39;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1845100
    iput-object p1, p0, LX/C37;->b:LX/C39;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1845101
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C37;->c:[Ljava/lang/String;

    .line 1845102
    iput v3, p0, LX/C37;->d:I

    .line 1845103
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C37;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C37;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C37;LX/1De;IILX/C38;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C39",
            "<TE;>.CondensedStoryBodyComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1845074
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1845075
    iput-object p4, p0, LX/C37;->a:LX/C38;

    .line 1845076
    iget-object v0, p0, LX/C37;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1845077
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/C37;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C39",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845097
    iget-object v0, p0, LX/C37;->a:LX/C38;

    iput-object p1, v0, LX/C38;->b:LX/1Pb;

    .line 1845098
    iget-object v0, p0, LX/C37;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845099
    return-object p0
.end method

.method public final a(LX/C33;)LX/C37;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C33;",
            ")",
            "LX/C39",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845094
    iget-object v0, p0, LX/C37;->a:LX/C38;

    iput-object p1, v0, LX/C38;->a:LX/C33;

    .line 1845095
    iget-object v0, p0, LX/C37;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1845096
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1845090
    invoke-super {p0}, LX/1X5;->a()V

    .line 1845091
    const/4 v0, 0x0

    iput-object v0, p0, LX/C37;->a:LX/C38;

    .line 1845092
    iget-object v0, p0, LX/C37;->b:LX/C39;

    iget-object v0, v0, LX/C39;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1845093
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C39;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845080
    iget-object v1, p0, LX/C37;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C37;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C37;->d:I

    if-ge v1, v2, :cond_2

    .line 1845081
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1845082
    :goto_0
    iget v2, p0, LX/C37;->d:I

    if-ge v0, v2, :cond_1

    .line 1845083
    iget-object v2, p0, LX/C37;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1845084
    iget-object v2, p0, LX/C37;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1845085
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1845086
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1845087
    :cond_2
    iget-object v0, p0, LX/C37;->a:LX/C38;

    .line 1845088
    invoke-virtual {p0}, LX/C37;->a()V

    .line 1845089
    return-object v0
.end method

.method public final h(I)LX/C37;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/C39",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1845078
    iget-object v0, p0, LX/C37;->a:LX/C38;

    iput p1, v0, LX/C38;->c:I

    .line 1845079
    return-object p0
.end method
