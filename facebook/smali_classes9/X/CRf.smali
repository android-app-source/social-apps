.class public final LX/CRf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 1892339
    const/4 v15, 0x0

    .line 1892340
    const/4 v14, 0x0

    .line 1892341
    const-wide/16 v12, 0x0

    .line 1892342
    const/4 v11, 0x0

    .line 1892343
    const/4 v10, 0x0

    .line 1892344
    const/4 v9, 0x0

    .line 1892345
    const/4 v8, 0x0

    .line 1892346
    const-wide/16 v6, 0x0

    .line 1892347
    const/4 v5, 0x0

    .line 1892348
    const/4 v4, 0x0

    .line 1892349
    const/4 v3, 0x0

    .line 1892350
    const/4 v2, 0x0

    .line 1892351
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 1892352
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1892353
    const/4 v2, 0x0

    .line 1892354
    :goto_0
    return v2

    .line 1892355
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_9

    .line 1892356
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1892357
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1892358
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1892359
    const-string v7, "album"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1892360
    invoke-static/range {p0 .. p1}, LX/CRe;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 1892361
    :cond_1
    const-string v7, "can_viewer_add_tags"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1892362
    const/4 v2, 0x1

    .line 1892363
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v16, v6

    move v6, v2

    goto :goto_1

    .line 1892364
    :cond_2
    const-string v7, "created_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1892365
    const/4 v2, 0x1

    .line 1892366
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1892367
    :cond_3
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1892368
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1892369
    :cond_4
    const-string v7, "image"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1892370
    invoke-static/range {p0 .. p1}, LX/CR5;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1892371
    :cond_5
    const-string v7, "is_disturbing"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1892372
    const/4 v2, 0x1

    .line 1892373
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v9, v2

    move v13, v7

    goto :goto_1

    .line 1892374
    :cond_6
    const-string v7, "message"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1892375
    invoke-static/range {p0 .. p1}, LX/8ZT;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1892376
    :cond_7
    const-string v7, "modified_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1892377
    const/4 v2, 0x1

    .line 1892378
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v10

    move v8, v2

    goto/16 :goto_1

    .line 1892379
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1892380
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1892381
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892382
    if-eqz v6, :cond_a

    .line 1892383
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1892384
    :cond_a
    if-eqz v3, :cond_b

    .line 1892385
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1892386
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1892387
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1892388
    if-eqz v9, :cond_c

    .line 1892389
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 1892390
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1892391
    if-eqz v8, :cond_d

    .line 1892392
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1892393
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move/from16 v16, v14

    move/from16 v17, v15

    move v14, v10

    move v15, v11

    move-wide v10, v6

    move v6, v5

    move/from16 v19, v9

    move v9, v3

    move v3, v4

    move-wide v4, v12

    move v12, v8

    move/from16 v13, v19

    move v8, v2

    goto/16 :goto_1
.end method
