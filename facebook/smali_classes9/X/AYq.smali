.class public LX/AYq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/AYq;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1bG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/AYp;

.field public c:LX/AYp;

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1686151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AYq;->a:Ljava/util/List;

    .line 1686153
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    iput-object v0, p0, LX/AYq;->b:LX/AYp;

    .line 1686154
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    iput-object v0, p0, LX/AYq;->c:LX/AYp;

    .line 1686155
    return-void
.end method

.method public static a(LX/0QB;)LX/AYq;
    .locals 3

    .prologue
    .line 1686156
    sget-object v0, LX/AYq;->f:LX/AYq;

    if-nez v0, :cond_1

    .line 1686157
    const-class v1, LX/AYq;

    monitor-enter v1

    .line 1686158
    :try_start_0
    sget-object v0, LX/AYq;->f:LX/AYq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1686159
    if-eqz v2, :cond_0

    .line 1686160
    :try_start_1
    new-instance v0, LX/AYq;

    invoke-direct {v0}, LX/AYq;-><init>()V

    .line 1686161
    move-object v0, v0

    .line 1686162
    sput-object v0, LX/AYq;->f:LX/AYq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1686163
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1686164
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1686165
    :cond_1
    sget-object v0, LX/AYq;->f:LX/AYq;

    return-object v0

    .line 1686166
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1686167
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1bG;)V
    .locals 1
    .param p1    # LX/1bG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686148
    if-eqz p1, :cond_0

    .line 1686149
    iget-object v0, p0, LX/AYq;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1686150
    :cond_0
    return-void
.end method

.method public final a(LX/AYp;)V
    .locals 4

    .prologue
    .line 1686136
    iget-boolean v0, p0, LX/AYq;->d:Z

    if-nez v0, :cond_0

    .line 1686137
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/AYq;->d:Z

    .line 1686138
    :cond_0
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    if-ne p1, v0, :cond_1

    .line 1686139
    iget v0, p0, LX/AYq;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/AYq;->e:I

    .line 1686140
    :cond_1
    iget-object v0, p0, LX/AYq;->b:LX/AYp;

    iput-object v0, p0, LX/AYq;->c:LX/AYp;

    .line 1686141
    iput-object p1, p0, LX/AYq;->b:LX/AYp;

    .line 1686142
    iget-object v0, p0, LX/AYq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bG;

    .line 1686143
    iget-object v2, p0, LX/AYq;->b:LX/AYp;

    iget-object v3, p0, LX/AYq;->c:LX/AYp;

    invoke-interface {v0, v2, v3}, LX/1bG;->a(LX/AYp;LX/AYp;)V

    goto :goto_1

    .line 1686144
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1686145
    :cond_3
    return-void
.end method

.method public final b(LX/1bG;)V
    .locals 1

    .prologue
    .line 1686146
    iget-object v0, p0, LX/AYq;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1686147
    return-void
.end method
