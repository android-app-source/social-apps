.class public LX/AcH;
.super LX/9Bh;
.source ""


# instance fields
.field private final a:LX/AaZ;

.field private final b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/facecastdisplay/LiveEventsPill;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AaZ;LX/0zw;LX/0wW;LX/4mV;)V
    .locals 0
    .param p2    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AaZ;",
            "LX/0zw",
            "<",
            "Lcom/facebook/facecastdisplay/LiveEventsPill;",
            ">;",
            "LX/0wW;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1692409
    invoke-direct {p0, p3, p4}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 1692410
    iput-object p1, p0, LX/AcH;->a:LX/AaZ;

    .line 1692411
    iput-object p2, p0, LX/AcH;->b:LX/0zw;

    .line 1692412
    return-void
.end method


# virtual methods
.method public final a(LX/AeO;)V
    .locals 2

    .prologue
    .line 1692413
    invoke-interface {p1}, LX/AeO;->a()LX/AeN;

    move-result-object v0

    sget-object v1, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne v0, v1, :cond_0

    .line 1692414
    check-cast p1, LX/Aeu;

    .line 1692415
    iget-object v0, p0, LX/AcH;->a:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/Aeu;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1692416
    iget-object v0, p0, LX/AcH;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsPill;

    iget-object v1, p1, LX/Aeu;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsPill;->setPillText(Ljava/lang/String;)V

    .line 1692417
    :goto_0
    invoke-virtual {p0}, LX/9Bh;->a()Z

    .line 1692418
    :cond_0
    return-void

    .line 1692419
    :cond_1
    iget-object v0, p0, LX/AcH;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsPill;

    iget-object v1, p1, LX/Aeu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveEventsPill;->setPillText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/facecastdisplay/LiveEventsPill;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1692420
    iget-object v0, p0, LX/AcH;->b:LX/0zw;

    return-object v0
.end method
