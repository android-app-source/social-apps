.class public final LX/BK6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASe;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772598
    iput-object p1, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1772599
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "attachment_add_photo"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772600
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->x(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1772601
    return-void
.end method

.method public final a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1772602
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1772603
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_1

    if-ltz p1, :cond_1

    .line 1772604
    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1772605
    :goto_0
    iget-object v2, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    if-nez p3, :cond_0

    if-eqz p4, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v2, v3, v0, v1, p4}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/0Px;ZZZ)V

    .line 1772606
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1772607
    return-void

    .line 1772608
    :cond_1
    iget-object v2, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v3, LX/0ig;->k:LX/0ih;

    const-string v4, "platform_composer_onAttachmentChanged_IndexOutOfBounds"

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1772609
    iget-object v2, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s:LX/03V;

    const-string v3, "platform_composer_onAttachmentChanged_IndexOutOfBounds"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "index: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1772610
    goto :goto_1
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1772611
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1772612
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1772613
    if-nez v2, :cond_0

    .line 1772614
    :goto_0
    return-void

    .line 1772615
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1772616
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    .line 1772617
    if-eqz v4, :cond_1

    .line 1772618
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/media/MediaIdKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1772619
    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1772620
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "photo_removed"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772621
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v6, v6}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/0Px;ZZZ)V

    .line 1772622
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;J)V
    .locals 0

    .prologue
    .line 1772623
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/photos/base/tagging/FaceBox;)V
    .locals 4
    .param p2    # Lcom/facebook/photos/base/tagging/FaceBox;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772624
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "photo_view_clicked"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772625
    iget-object v0, p0, LX/BK6;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772626
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1772627
    if-nez v2, :cond_1

    .line 1772628
    :cond_0
    :goto_0
    return-void

    .line 1772629
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1772630
    sget-object v3, LX/BKD;->c:[I

    .line 1772631
    iget-object p0, v1, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object p0, p0

    .line 1772632
    invoke-virtual {p0}, LX/4gQ;->ordinal()I

    move-result p0

    aget v3, v3, p0

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v1, v2

    .line 1772633
    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1772634
    iget-boolean v3, v1, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v1, v3

    .line 1772635
    if-nez v1, :cond_0

    .line 1772636
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;

    invoke-direct {v1, v3, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "extra_taggable_gallery_photo_list"

    new-instance p0, Ljava/util/ArrayList;

    iget-object p1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p1, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object p1

    invoke-static {p1}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extras_taggable_gallery_creative_editing_data_list"

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p0, p0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object p0

    invoke-static {p0}, LX/7kq;->c(LX/0Px;)Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extra_taggable_gallery_photo_item_id"

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extra_session_id"

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p0, p0, LX/BKm;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1772637
    if-eqz p2, :cond_2

    .line 1772638
    const-string v3, "extra_taggable_gallery_goto_facebox"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1772639
    :cond_2
    iget-object v3, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object p0, LX/0ig;->k:LX/0ih;

    const-string p1, "photo_tagging"

    invoke-virtual {v3, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772640
    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1772641
    goto :goto_0

    .line 1772642
    :pswitch_1
    sget-object v2, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    .line 1772643
    iget-object v3, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, v3

    .line 1772644
    invoke-virtual {v2, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1772645
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_session_id"

    iget-object v3, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_source"

    sget-object v3, LX/BIf;->COMPOSER:LX/BIf;

    invoke-virtual {v3}, LX/BIf;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_video_uri"

    iget-object v3, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->h(LX/0Px;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 1772646
    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v3, LX/0ig;->k:LX/0ih;

    const-string p0, "video_play"

    invoke-virtual {v2, v3, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772647
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 1772648
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 0

    .prologue
    .line 1772649
    return-void
.end method
