.class public LX/AVh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0ih;


# instance fields
.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1680194
    sget-object v0, LX/0ig;->aZ:LX/0ih;

    sput-object v0, LX/AVh;->a:LX/0ih;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1680193
    return-void
.end method

.method public static a(LX/0QB;)LX/AVh;
    .locals 1

    .prologue
    .line 1680191
    invoke-static {p0}, LX/AVh;->b(LX/0QB;)LX/AVh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/AVh;Ljava/lang/String;LX/1rQ;)V
    .locals 3

    .prologue
    .line 1680189
    iget-object v0, p0, LX/AVh;->b:LX/0if;

    sget-object v1, LX/AVh;->a:LX/0ih;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1680190
    return-void
.end method

.method public static b(LX/0QB;)LX/AVh;
    .locals 2

    .prologue
    .line 1680195
    new-instance v1, LX/AVh;

    invoke-direct {v1}, LX/AVh;-><init>()V

    .line 1680196
    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    .line 1680197
    iput-object v0, v1, LX/AVh;->b:LX/0if;

    .line 1680198
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1680187
    iget-object v0, p0, LX/AVh;->b:LX/0if;

    sget-object v1, LX/AVh;->a:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1680188
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1680185
    iget-object v0, p0, LX/AVh;->b:LX/0if;

    sget-object v1, LX/AVh;->a:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1680186
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1680182
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "video_position_ms"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    .line 1680183
    invoke-static {p0, p1, v0}, LX/AVh;->a(LX/AVh;Ljava/lang/String;LX/1rQ;)V

    .line 1680184
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1680177
    iget-object v0, p0, LX/AVh;->b:LX/0if;

    sget-object v1, LX/AVh;->a:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1680178
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1680179
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1680180
    const-string v1, "go_live"

    invoke-static {p0, v1, v0}, LX/AVh;->a(LX/AVh;Ljava/lang/String;LX/1rQ;)V

    .line 1680181
    return-void
.end method
