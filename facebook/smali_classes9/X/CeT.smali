.class public LX/CeT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/CeT;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cy;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2cy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1924655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1924656
    iput-object p1, p0, LX/CeT;->a:LX/0Ot;

    .line 1924657
    iput-object p2, p0, LX/CeT;->b:LX/0Ot;

    .line 1924658
    iput-object p3, p0, LX/CeT;->c:LX/0Ot;

    .line 1924659
    iput-object p4, p0, LX/CeT;->d:LX/0Ot;

    .line 1924660
    return-void
.end method

.method public static a(LX/0QB;)LX/CeT;
    .locals 7

    .prologue
    .line 1924661
    sget-object v0, LX/CeT;->e:LX/CeT;

    if-nez v0, :cond_1

    .line 1924662
    const-class v1, LX/CeT;

    monitor-enter v1

    .line 1924663
    :try_start_0
    sget-object v0, LX/CeT;->e:LX/CeT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1924664
    if-eqz v2, :cond_0

    .line 1924665
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1924666
    new-instance v3, LX/CeT;

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xafd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xf65

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x542

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/CeT;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1924667
    move-object v0, v3

    .line 1924668
    sput-object v0, LX/CeT;->e:LX/CeT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1924669
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1924670
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1924671
    :cond_1
    sget-object v0, LX/CeT;->e:LX/CeT;

    return-object v0

    .line 1924672
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1924673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EnabledOrDisabled;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EnabledOrDisabled;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1924674
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1924675
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "both parameters are null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1924676
    :goto_0
    return-object v0

    .line 1924677
    :cond_0
    new-instance v0, LX/4FY;

    invoke-direct {v0}, LX/4FY;-><init>()V

    .line 1924678
    if-eqz p2, :cond_1

    .line 1924679
    const-string v1, "notifications_enabled"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924680
    :cond_1
    if-eqz p1, :cond_2

    .line 1924681
    const-string v1, "feature_enabled"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924682
    :cond_2
    new-instance v1, LX/Cea;

    invoke-direct {v1}, LX/Cea;-><init>()V

    move-object v1, v1

    .line 1924683
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Cea;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1924684
    iget-object v0, p0, LX/CeT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/CeR;

    invoke-direct {v1, p0}, LX/CeR;-><init>(LX/CeT;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1924685
    new-instance v1, LX/CeS;

    invoke-direct {v1, p0}, LX/CeS;-><init>(LX/CeT;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924686
    iget-object v0, p0, LX/CeT;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cy;

    invoke-virtual {v0}, LX/2cy;->b()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    .line 1924687
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1924688
    invoke-virtual {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1924689
    :goto_0
    return-object v0

    .line 1924690
    :cond_0
    iget-object v0, p0, LX/CeT;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "place_tips_settings_helper"

    const-string v2, "Failed to get place tips learn more link"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924691
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/placetips/settings/PlaceTipsLocationData;Ljava/lang/String;J)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GravityNegativeFeedbackOptions;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/placetips/settings/PlaceTipsLocationData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1924692
    if-nez p2, :cond_0

    .line 1924693
    new-instance p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    invoke-direct {p2}, Lcom/facebook/placetips/settings/PlaceTipsLocationData;-><init>()V

    .line 1924694
    :cond_0
    new-instance v4, LX/4FW;

    invoke-direct {v4}, LX/4FW;-><init>()V

    iget v5, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->c(Ljava/lang/Integer;)LX/4FW;

    move-result-object v4

    iget v5, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->b(Ljava/lang/Integer;)LX/4FW;

    move-result-object v4

    iget-wide v6, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->c(Ljava/lang/Double;)LX/4FW;

    move-result-object v4

    iget-wide v6, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->a(Ljava/lang/Double;)LX/4FW;

    move-result-object v4

    iget-wide v6, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->e:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->b(Ljava/lang/Double;)LX/4FW;

    move-result-object v4

    iget v5, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->a(Ljava/lang/Integer;)LX/4FW;

    move-result-object v4

    iget-wide v6, p2, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->g:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4FW;->d(Ljava/lang/Double;)LX/4FW;

    move-result-object v4

    move-object v0, v4

    .line 1924695
    invoke-static {p4, p5}, LX/1lQ;->m(J)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FW;->c(Ljava/lang/Integer;)LX/4FW;

    .line 1924696
    new-instance v1, LX/4FX;

    invoke-direct {v1}, LX/4FX;-><init>()V

    .line 1924697
    const-string v2, "feedback_type"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924698
    move-object v1, v1

    .line 1924699
    const-string v2, "location_data"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1924700
    move-object v0, v1

    .line 1924701
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1924702
    move-object v0, v0

    .line 1924703
    new-instance v1, LX/CeZ;

    invoke-direct {v1}, LX/CeZ;-><init>()V

    move-object v1, v1

    .line 1924704
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/CeZ;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1924705
    iget-object v0, p0, LX/CeT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1924706
    return-void
.end method
