.class public final LX/BcD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BcE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/5Jq;

.field public b:LX/2dx;

.field public c:I

.field public final synthetic d:LX/BcE;


# direct methods
.method public constructor <init>(LX/BcE;)V
    .locals 1

    .prologue
    .line 1801614
    iput-object p1, p0, LX/BcD;->d:LX/BcE;

    .line 1801615
    move-object v0, p1

    .line 1801616
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1801617
    sget-object v0, LX/BcF;->a:LX/2dx;

    iput-object v0, p0, LX/BcD;->b:LX/2dx;

    .line 1801618
    const/4 v0, 0x0

    iput v0, p0, LX/BcD;->c:I

    .line 1801619
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1801620
    const-string v0, "FeedPager"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1801621
    if-ne p0, p1, :cond_1

    .line 1801622
    :cond_0
    :goto_0
    return v0

    .line 1801623
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1801624
    goto :goto_0

    .line 1801625
    :cond_3
    check-cast p1, LX/BcD;

    .line 1801626
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1801627
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1801628
    if-eq v2, v3, :cond_0

    .line 1801629
    iget-object v2, p0, LX/BcD;->a:LX/5Jq;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BcD;->a:LX/5Jq;

    iget-object v3, p1, LX/BcD;->a:LX/5Jq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1801630
    goto :goto_0

    .line 1801631
    :cond_5
    iget-object v2, p1, LX/BcD;->a:LX/5Jq;

    if-nez v2, :cond_4

    .line 1801632
    :cond_6
    iget-object v2, p0, LX/BcD;->b:LX/2dx;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BcD;->b:LX/2dx;

    iget-object v3, p1, LX/BcD;->b:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1801633
    goto :goto_0

    .line 1801634
    :cond_8
    iget-object v2, p1, LX/BcD;->b:LX/2dx;

    if-nez v2, :cond_7

    .line 1801635
    :cond_9
    iget v2, p0, LX/BcD;->c:I

    iget v3, p1, LX/BcD;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1801636
    goto :goto_0
.end method
