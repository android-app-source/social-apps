.class public LX/B9y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile k:LX/B9y;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/14x;

.field private final d:LX/03V;

.field public final e:LX/0W3;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xv;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/23i;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1752549
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/B9y;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/14x;LX/03V;LX/0W3;LX/0Ot;LX/23i;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/14x;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/1xv;",
            ">;",
            "LX/23i;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752539
    iput-object p1, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1752540
    iput-object p2, p0, LX/B9y;->c:LX/14x;

    .line 1752541
    iput-object p3, p0, LX/B9y;->d:LX/03V;

    .line 1752542
    iput-object p4, p0, LX/B9y;->e:LX/0W3;

    .line 1752543
    iput-object p5, p0, LX/B9y;->f:LX/0Ot;

    .line 1752544
    iput-object p6, p0, LX/B9y;->g:LX/23i;

    .line 1752545
    iput-object p7, p0, LX/B9y;->h:LX/0Ot;

    .line 1752546
    iput-object p8, p0, LX/B9y;->i:LX/0Ot;

    .line 1752547
    iput-object p9, p0, LX/B9y;->j:Ljava/util/concurrent/ExecutorService;

    .line 1752548
    return-void
.end method

.method public static a(LX/0QB;)LX/B9y;
    .locals 13

    .prologue
    .line 1752525
    sget-object v0, LX/B9y;->k:LX/B9y;

    if-nez v0, :cond_1

    .line 1752526
    const-class v1, LX/B9y;

    monitor-enter v1

    .line 1752527
    :try_start_0
    sget-object v0, LX/B9y;->k:LX/B9y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1752528
    if-eqz v2, :cond_0

    .line 1752529
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1752530
    new-instance v3, LX/B9y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v5

    check-cast v5, LX/14x;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    const/16 v8, 0x903

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v9

    check-cast v9, LX/23i;

    const/16 v10, 0xafd

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x6b4

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v12}, LX/B9y;-><init>(Lcom/facebook/content/SecureContextHelper;LX/14x;LX/03V;LX/0W3;LX/0Ot;LX/23i;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;)V

    .line 1752531
    move-object v0, v3

    .line 1752532
    sput-object v0, LX/B9y;->k:LX/B9y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1752533
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1752534
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1752535
    :cond_1
    sget-object v0, LX/B9y;->k:LX/B9y;

    return-object v0

    .line 1752536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1752537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x32

    const/4 v2, 0x1

    .line 1752504
    invoke-virtual {p0}, LX/B9y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1752505
    iget-object v0, p0, LX/B9y;->d:LX/03V;

    const-string v1, "Messenger"

    const-string v2, "Trying to pass extra share id in older versions of messenger or user should be dioded"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752506
    const/4 v0, 0x0

    .line 1752507
    :goto_0
    return-object v0

    .line 1752508
    :cond_0
    invoke-static {p1, v2, v2}, LX/B9y;->b(Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v1

    .line 1752509
    const-string v0, "share_fbid"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752510
    const-string v0, "share_return_to_fb4a"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1752511
    const-string v0, "share_title"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752512
    const-string v0, "share_media_url"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752513
    const-string v0, "share_caption"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752514
    const-string v0, "share_description"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752515
    if-eqz p8, :cond_1

    .line 1752516
    const-string v0, "send_as_message_share_source"

    invoke-virtual {v1, v0, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752517
    :cond_1
    if-eqz p6, :cond_2

    .line 1752518
    invoke-interface {p6}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p6, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1752519
    const-string v2, "preselected_recipients"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1752520
    :cond_2
    if-eqz p7, :cond_3

    .line 1752521
    invoke-interface {p7}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p7, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1752522
    const-string v2, "suggested_recipients"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1752523
    :cond_3
    invoke-static {v1, p9}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;)V

    move-object v0, v1

    .line 1752524
    goto :goto_0
.end method

.method public static a(ZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1752411
    new-instance v1, Landroid/content/Intent;

    sget-object v0, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1752412
    if-eqz p1, :cond_0

    sget-object v0, LX/3RH;->s:Ljava/lang/String;

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1752413
    const-string v0, "share_return_to_fb4a"

    invoke-virtual {v1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1752414
    return-object v1

    .line 1752415
    :cond_0
    sget-object v0, LX/3RH;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(LX/B9y;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1752500
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, LX/B9y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1752501
    if-nez v0, :cond_0

    .line 1752502
    :goto_0
    return-void

    .line 1752503
    :cond_0
    iget-object v1, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static a(LX/B9y;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8
    .param p2    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1752464
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1752465
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1752466
    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1752467
    :goto_0
    return-void

    .line 1752468
    :cond_0
    invoke-static {p1}, LX/14w;->r(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1752469
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1752470
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1752471
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-static {v1, p6, v4}, LX/B9y;->b(Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v4

    .line 1752472
    const-string v1, "share_story_url"

    invoke-static {v3}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752473
    if-eqz p3, :cond_1

    .line 1752474
    const-string v1, "page_name"

    invoke-virtual {v4, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752475
    :cond_1
    if-eqz p4, :cond_2

    .line 1752476
    const-string v1, "page_target"

    invoke-virtual {v4, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1752477
    :cond_2
    if-eqz p5, :cond_3

    .line 1752478
    const-string v1, "page_post_id"

    invoke-virtual {v4, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752479
    :cond_3
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1752480
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1752481
    iget-object v1, p0, LX/B9y;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1xv;

    invoke-virtual {v1, v3}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->a()LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->c()LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v1

    .line 1752482
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1752483
    :goto_1
    const-string v3, "share_title"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v3, v6, v1}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1752484
    const-string v3, "share_caption"

    invoke-static {v5}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v3, v6, v7}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1752485
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1752486
    const-string v3, "share_media_url"

    invoke-static {v5}, LX/1VO;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/1xl;->b(Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    :cond_4
    invoke-static {v4, v3, v6, v2}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1752487
    const-string v0, "share_attachment_url"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752488
    invoke-static {v5}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1752489
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v5}, LX/1VO;->x(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1752490
    const-string v0, "share_description"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752491
    :cond_5
    if-eqz v1, :cond_6

    .line 1752492
    const-string v0, "share_robotext"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752493
    :cond_6
    :goto_2
    invoke-static {v4, p7}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1752494
    iget-object v0, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v4, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_7
    move-object v1, v2

    .line 1752495
    goto :goto_1

    .line 1752496
    :cond_8
    const-string v1, "share_description"

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752497
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1752498
    const-string v1, "share_title"

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752499
    const-string v1, "share_media_url"

    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1752461
    const-string v0, "send_as_message_entry_point"

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752462
    return-void

    .line 1752463
    :cond_0
    const-string p1, "unspecified"

    goto :goto_0
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752457
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1752458
    :goto_0
    invoke-virtual {p0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752459
    return-void

    :cond_0
    move-object p3, p2

    .line 1752460
    goto :goto_0
.end method

.method public static b(LX/B9y;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1752449
    iget-object v0, p0, LX/B9y;->c:LX/14x;

    invoke-virtual {v0}, LX/14x;->e()Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1752450
    if-nez v1, :cond_0

    .line 1752451
    const/4 v0, 0x0

    .line 1752452
    :goto_0
    return-object v0

    .line 1752453
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1752454
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752455
    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1752456
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1752550
    invoke-static {p1, p2}, LX/B9y;->a(ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 1752551
    const-string v1, "share_fbid"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752552
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1752433
    invoke-virtual {p0}, LX/B9y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1752434
    iget-object v0, p0, LX/B9y;->d:LX/03V;

    const-string v1, "Messenger"

    const-string v2, "Trying to pass extra share id in older versions of messenger or user should be dioded"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752435
    const/4 v0, 0x0

    .line 1752436
    :goto_0
    return-object v0

    .line 1752437
    :cond_0
    invoke-static {p1, v2, v2}, LX/B9y;->b(Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v1

    .line 1752438
    const-string v0, "share_fbid"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752439
    const-string v0, "share_return_to_fb4a"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1752440
    const-string v0, "share_title"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752441
    const-string v0, "share_media_url"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752442
    const-string v0, "share_caption"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752443
    const-string v0, "share_description"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752444
    invoke-interface {p6}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v2, 0x32

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p6, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1752445
    const-string v2, "preselected_recipients"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1752446
    const-string v2, "suggested_recipients"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1752447
    invoke-static {v1, p7}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;)V

    move-object v0, v1

    .line 1752448
    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1752429
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v1 .. v8}, LX/B9y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1752430
    if-nez v1, :cond_0

    .line 1752431
    :goto_0
    return-void

    .line 1752432
    :cond_0
    iget-object v2, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    move/from16 v0, p9

    invoke-interface {v2, v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1752425
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v1 .. v10}, LX/B9y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1752426
    if-nez v1, :cond_0

    .line 1752427
    :goto_0
    return-void

    .line 1752428
    :cond_0
    iget-object v2, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    move/from16 v0, p11

    invoke-interface {v2, v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1752422
    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    .line 1752423
    invoke-static/range {v0 .. v8}, LX/B9y;->a(LX/B9y;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V

    .line 1752424
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752418
    invoke-virtual/range {p0 .. p5}, LX/B9y;->b(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1752419
    if-eqz v0, :cond_0

    .line 1752420
    iget-object v1, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1752421
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1752416
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p6

    move-object v7, p7

    invoke-static/range {v0 .. v7}, LX/B9y;->a(LX/B9y;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1752417
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;ZLjava/lang/String;)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1752397
    iget-object v0, p0, LX/B9y;->g:LX/23i;

    invoke-virtual {v0}, LX/23i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B9y;->g:LX/23i;

    invoke-virtual {v0}, LX/23i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1752398
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, LX/0ax;->aj:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1752399
    iget-object v1, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1752400
    :goto_0
    return-void

    .line 1752401
    :cond_0
    iget-object v0, p0, LX/B9y;->c:LX/14x;

    const-string v1, "21.0"

    invoke-virtual {v0, v1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move v6, p3

    move-object v7, p4

    .line 1752402
    invoke-static/range {v0 .. v7}, LX/B9y;->a(LX/B9y;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 1752403
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1752404
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1752405
    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1752406
    :cond_2
    :goto_1
    goto :goto_0

    .line 1752407
    :cond_3
    invoke-static {p1}, LX/14w;->r(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1752408
    invoke-static {v0}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/B9y;->b(LX/B9y;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1752409
    if-eqz v0, :cond_2

    .line 1752410
    iget-object v1, p0, LX/B9y;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;ZLX/FYC;Ljava/lang/String;)V
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752392
    const/4 v4, 0x0

    .line 1752393
    new-instance v2, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v1, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    const/16 v3, 0x19

    invoke-direct {v2, p1, v0, v1, v3}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1752394
    iget-object v0, p0, LX/B9y;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    iget-object v1, p0, LX/B9y;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3HM;

    invoke-virtual {v1, v2, v4, v4}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    move-object v6, v0

    .line 1752395
    new-instance v0, LX/B9x;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/B9x;-><init>(LX/B9y;LX/FYC;Landroid/content/Context;ZLjava/lang/String;)V

    iget-object v1, p0, LX/B9y;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1752396
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1752391
    iget-object v0, p0, LX/B9y;->g:LX/23i;

    invoke-virtual {v0}, LX/23i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B9y;->g:LX/23i;

    invoke-virtual {v0}, LX/23i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/B9y;->c:LX/14x;

    const-string v1, "17.0"

    invoke-virtual {v0, v1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1752376
    sget-wide v0, LX/0X5;->em:J

    .line 1752377
    iget-object v2, p0, LX/B9y;->e:LX/0W3;

    sget-object v3, LX/B9y;->a:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1752378
    iget-object v3, p0, LX/B9y;->c:LX/14x;

    invoke-virtual {v3, v2}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1752379
    if-nez v0, :cond_0

    .line 1752380
    invoke-static {p3, p4}, LX/B9y;->a(ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 1752381
    const-string v1, "share_link_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1752382
    move-object v0, v0

    .line 1752383
    :goto_1
    if-nez v0, :cond_1

    .line 1752384
    const/4 v0, 0x0

    .line 1752385
    :goto_2
    return-object v0

    .line 1752386
    :cond_0
    invoke-static {p0, p2}, LX/B9y;->b(LX/B9y;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 1752387
    :cond_1
    const-class v1, Landroid/app/Service;

    invoke-static {p1, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1752388
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1752389
    :cond_2
    invoke-static {v0, p5}, LX/B9y;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1752390
    const-string v1, "trigger"

    const-string v2, "send_as_message"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
