.class public LX/Bgf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Bgf;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808023
    iput-object p1, p0, LX/Bgf;->a:Landroid/content/Context;

    .line 1808024
    iput-object p2, p0, LX/Bgf;->b:LX/17Y;

    .line 1808025
    return-void
.end method

.method public static a(LX/0QB;)LX/Bgf;
    .locals 5

    .prologue
    .line 1808026
    sget-object v0, LX/Bgf;->c:LX/Bgf;

    if-nez v0, :cond_1

    .line 1808027
    const-class v1, LX/Bgf;

    monitor-enter v1

    .line 1808028
    :try_start_0
    sget-object v0, LX/Bgf;->c:LX/Bgf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1808029
    if-eqz v2, :cond_0

    .line 1808030
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1808031
    new-instance p0, LX/Bgf;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-direct {p0, v3, v4}, LX/Bgf;-><init>(Landroid/content/Context;LX/17Y;)V

    .line 1808032
    move-object v0, p0

    .line 1808033
    sput-object v0, LX/Bgf;->c:LX/Bgf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1808034
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1808035
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1808036
    :cond_1
    sget-object v0, LX/Bgf;->c:LX/Bgf;

    return-object v0

    .line 1808037
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1808038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;Ljava/lang/String;LX/CdT;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p5    # LX/CdT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1808039
    iget-object v0, p0, LX/Bgf;->b:LX/17Y;

    iget-object v1, p0, LX/Bgf;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->aL:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p6, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1808040
    if-nez v0, :cond_0

    .line 1808041
    const/4 v0, 0x0

    .line 1808042
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "place_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "place_pic_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "entry_point"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
