.class public LX/C3Q;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3R;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3Q",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845632
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845633
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3Q;->b:LX/0Zi;

    .line 1845634
    iput-object p1, p0, LX/C3Q;->a:LX/0Ot;

    .line 1845635
    return-void
.end method

.method public static a(LX/0QB;)LX/C3Q;
    .locals 4

    .prologue
    .line 1845636
    const-class v1, LX/C3Q;

    monitor-enter v1

    .line 1845637
    :try_start_0
    sget-object v0, LX/C3Q;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845638
    sput-object v2, LX/C3Q;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845639
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845640
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845641
    new-instance v3, LX/C3Q;

    const/16 p0, 0x1ec4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3Q;-><init>(LX/0Ot;)V

    .line 1845642
    move-object v0, v3

    .line 1845643
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845644
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845645
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845647
    const v0, -0x5752da85

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1845648
    const v0, -0x5752da85

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1845649
    check-cast p2, LX/C3P;

    .line 1845650
    iget-object v0, p0, LX/C3Q;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3R;

    iget-object v1, p2, LX/C3P;->a:LX/C33;

    iget-object v2, p2, LX/C3P;->b:LX/1Pb;

    iget v3, p2, LX/C3P;->c:I

    .line 1845651
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/C3R;->c:LX/C3I;

    invoke-virtual {v5, p1}, LX/C3I;->c(LX/1De;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/C3G;->a(LX/C33;)LX/C3G;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/C3G;->a(LX/1Pb;)LX/C3G;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 1845652
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x6

    const/4 v7, 0x1

    const/4 v12, 0x0

    .line 1845653
    invoke-virtual {p1}, LX/1De;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 p2, 0x0

    .line 1845654
    iget-object v8, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845655
    iget-object v9, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v9

    .line 1845656
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    .line 1845657
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    .line 1845658
    :cond_0
    const/4 v8, 0x0

    .line 1845659
    :goto_0
    move-object v8, v8

    .line 1845660
    invoke-static {v1}, LX/C3R;->b(LX/C33;)Ljava/lang/String;

    move-result-object v9

    .line 1845661
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v12}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    iget-object v6, v0, LX/C3R;->d:LX/C39;

    invoke-virtual {v6, p1}, LX/C39;->c(LX/1De;)LX/C37;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/C37;->a(LX/C33;)LX/C37;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/C37;->a(LX/1Pb;)LX/C37;

    move-result-object v11

    add-int/lit8 v6, v3, -0x2

    if-gtz v6, :cond_3

    move v6, v7

    :goto_1
    invoke-virtual {v11, v6}, LX/C37;->h(I)LX/C37;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-static {p1}, LX/C3Q;->onClick(LX/1De;)LX/1dQ;

    move-result-object v11

    invoke-interface {v6, v11}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v10, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 1845662
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 1845663
    sget-object v10, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object v11, v1, LX/C33;->b:LX/C34;

    if-ne v10, v11, :cond_5

    const v10, 0x7f0e0a4b

    :goto_2
    move v10, v10

    .line 1845664
    invoke-static {p1, v12, v10}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v10, 0x7f0b1d7c

    invoke-interface {v8, v7, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b1d6d

    invoke-interface {v7, p0, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1845665
    :cond_1
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1845666
    const v7, 0x7f0e0a4c

    invoke-static {p1, v12, v7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b1d6d

    invoke-interface {v7, p0, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1845667
    :cond_2
    move-object v6, v6

    .line 1845668
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 1845669
    iget-object v6, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845670
    iget-object v7, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v7

    .line 1845671
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    iget-object v7, v0, LX/C3R;->a:LX/1VL;

    invoke-static {v6, v7}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1VL;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1845672
    :goto_3
    move-object v5, v5

    .line 1845673
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/C3Q;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b1d6c

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1845674
    return-object v0

    .line 1845675
    :cond_3
    add-int/lit8 v6, v3, -0x2

    goto/16 :goto_1

    .line 1845676
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v8

    .line 1845677
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f082a08

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v8, v11, p2

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :cond_5
    const v10, 0x7f0e0a4a

    goto/16 :goto_2

    .line 1845678
    :cond_6
    iget-object v6, v0, LX/C3R;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/C3p;

    invoke-virtual {v6, p1}, LX/C3p;->c(LX/1De;)LX/C3n;

    move-result-object v7

    iget-object v6, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845679
    iget-object v8, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v8

    .line 1845680
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/C3n;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3n;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/C3n;->a(Z)LX/C3n;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x5

    const v8, 0x7f0b1d6d

    invoke-interface {v6, v7, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1845681
    invoke-static {}, LX/1dS;->b()V

    .line 1845682
    iget v0, p1, LX/1dQ;->b:I

    .line 1845683
    packed-switch v0, :pswitch_data_0

    .line 1845684
    :goto_0
    return-object v2

    .line 1845685
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1845686
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1845687
    check-cast v1, LX/C3P;

    .line 1845688
    iget-object p1, p0, LX/C3Q;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C3R;

    iget-object p2, v1, LX/C3P;->a:LX/C33;

    .line 1845689
    iget-object p0, p1, LX/C3R;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C3B;

    invoke-virtual {p0, v0, p2}, LX/C3B;->onClick(Landroid/view/View;LX/C33;)V

    .line 1845690
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5752da85
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C3O;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3Q",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845691
    new-instance v1, LX/C3P;

    invoke-direct {v1, p0}, LX/C3P;-><init>(LX/C3Q;)V

    .line 1845692
    iget-object v2, p0, LX/C3Q;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3O;

    .line 1845693
    if-nez v2, :cond_0

    .line 1845694
    new-instance v2, LX/C3O;

    invoke-direct {v2, p0}, LX/C3O;-><init>(LX/C3Q;)V

    .line 1845695
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3O;->a$redex0(LX/C3O;LX/1De;IILX/C3P;)V

    .line 1845696
    move-object v1, v2

    .line 1845697
    move-object v0, v1

    .line 1845698
    return-object v0
.end method
