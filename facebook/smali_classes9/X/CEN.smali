.class public final LX/CEN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V
    .locals 0

    .prologue
    .line 1861089
    iput-object p1, p0, LX/CEN;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1861090
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    iget-object v2, p0, LX/CEN;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->m:LX/CEg;

    .line 1861091
    iget-object v3, v2, LX/CEg;->c:LX/0TD;

    new-instance v4, LX/CEf;

    invoke-direct {v4, v2}, LX/CEf;-><init>(LX/CEg;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 1861092
    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/CEN;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->o:LX/AyD;

    iget-object v3, p0, LX/CEN;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->K:Ljava/lang/String;

    .line 1861093
    new-instance v4, LX/Ayd;

    invoke-direct {v4}, LX/Ayd;-><init>()V

    move-object v5, v4

    .line 1861094
    iget-object v4, v2, LX/AyD;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-virtual {v4, v5}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1861095
    const-string v4, "souvenir_id"

    invoke-virtual {v5, v4, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1861096
    const-string v4, "profile_picture_size"

    const/16 p0, 0x190

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1861097
    iget-object v4, v2, LX/AyD;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object p0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v5, p0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v5

    sget-object p0, LX/0zS;->d:LX/0zS;

    invoke-virtual {v5, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1861098
    new-instance v5, LX/AyC;

    invoke-direct {v5, v2}, LX/AyC;-><init>(LX/AyD;)V

    invoke-static {v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 1861099
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
