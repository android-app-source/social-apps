.class public final enum LX/CF0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CF0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CF0;

.field public static final enum THROWBACK_FEED_EMPTY_STATE:LX/CF0;

.field public static final enum THROWBACK_FEED_MENU:LX/CF0;

.field public static final enum THROWBACK_FEED_NUX_MEGAPHONE:LX/CF0;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1862333
    new-instance v0, LX/CF0;

    const-string v1, "THROWBACK_FEED_MENU"

    const-string v2, "subscribe_menu"

    invoke-direct {v0, v1, v3, v2}, LX/CF0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CF0;->THROWBACK_FEED_MENU:LX/CF0;

    .line 1862334
    new-instance v0, LX/CF0;

    const-string v1, "THROWBACK_FEED_NUX_MEGAPHONE"

    const-string v2, "nux_megaphone"

    invoke-direct {v0, v1, v4, v2}, LX/CF0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CF0;->THROWBACK_FEED_NUX_MEGAPHONE:LX/CF0;

    .line 1862335
    new-instance v0, LX/CF0;

    const-string v1, "THROWBACK_FEED_EMPTY_STATE"

    const-string v2, "nux_empty_state"

    invoke-direct {v0, v1, v5, v2}, LX/CF0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CF0;->THROWBACK_FEED_EMPTY_STATE:LX/CF0;

    .line 1862336
    const/4 v0, 0x3

    new-array v0, v0, [LX/CF0;

    sget-object v1, LX/CF0;->THROWBACK_FEED_MENU:LX/CF0;

    aput-object v1, v0, v3

    sget-object v1, LX/CF0;->THROWBACK_FEED_NUX_MEGAPHONE:LX/CF0;

    aput-object v1, v0, v4

    sget-object v1, LX/CF0;->THROWBACK_FEED_EMPTY_STATE:LX/CF0;

    aput-object v1, v0, v5

    sput-object v0, LX/CF0;->$VALUES:[LX/CF0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1862337
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1862338
    iput-object p3, p0, LX/CF0;->name:Ljava/lang/String;

    .line 1862339
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CF0;
    .locals 1

    .prologue
    .line 1862340
    const-class v0, LX/CF0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CF0;

    return-object v0
.end method

.method public static values()[LX/CF0;
    .locals 1

    .prologue
    .line 1862341
    sget-object v0, LX/CF0;->$VALUES:[LX/CF0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CF0;

    return-object v0
.end method
