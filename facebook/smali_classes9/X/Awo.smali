.class public LX/Awo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field public a:Z

.field public b:F

.field private c:Landroid/animation/TimeInterpolator;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/animation/TimeInterpolator;I)V
    .locals 0

    .prologue
    .line 1727165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727166
    iput-object p1, p0, LX/Awo;->c:Landroid/animation/TimeInterpolator;

    .line 1727167
    iput p2, p0, LX/Awo;->d:I

    .line 1727168
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;
    .locals 1

    .prologue
    .line 1727169
    new-instance v0, LX/Awm;

    invoke-direct {v0, p0, p1}, LX/Awm;-><init>(LX/Awo;Landroid/view/View;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1727170
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Awo;->a:Z

    .line 1727171
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1727172
    iget v0, p0, LX/Awo;->d:I

    int-to-float v1, v0

    .line 1727173
    iget-boolean v0, p0, LX/Awo;->a:Z

    move v0, v0

    .line 1727174
    if-eqz v0, :cond_0

    .line 1727175
    iget v0, p0, LX/Awo;->b:F

    move v0, v0

    .line 1727176
    :goto_0
    mul-float/2addr v0, v1

    float-to-long v0, v0

    return-wide v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;
    .locals 1

    .prologue
    .line 1727177
    new-instance v0, LX/Awn;

    invoke-direct {v0, p0, p1}, LX/Awn;-><init>(LX/Awo;Landroid/view/View;)V

    return-object v0
.end method

.method public final getInterpolation(F)F
    .locals 1

    .prologue
    .line 1727178
    iput p1, p0, LX/Awo;->b:F

    .line 1727179
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Awo;->a:Z

    .line 1727180
    iget-object v0, p0, LX/Awo;->c:Landroid/animation/TimeInterpolator;

    invoke-interface {v0, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
