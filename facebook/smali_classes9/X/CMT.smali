.class public final LX/CMT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/emoji/RecentEmojiView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/emoji/RecentEmojiView;)V
    .locals 0

    .prologue
    .line 1880650
    iput-object p1, p0, LX/CMT;->a:Lcom/facebook/messaging/emoji/RecentEmojiView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1880651
    sget-object v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->a:Ljava/lang/Class;

    const-string v1, "Failed to load recent emoji"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1880652
    iget-object v0, p0, LX/CMT;->a:Lcom/facebook/messaging/emoji/RecentEmojiView;

    const/4 v1, 0x0

    .line 1880653
    iput-object v1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    .line 1880654
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1880655
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1880656
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/emoji/service/FetchRecentEmojiResult;

    .line 1880657
    iget-object v1, p0, LX/CMT;->a:Lcom/facebook/messaging/emoji/RecentEmojiView;

    .line 1880658
    iget-object p1, v0, Lcom/facebook/messaging/emoji/service/FetchRecentEmojiResult;->a:LX/0Px;

    move-object v0, p1

    .line 1880659
    invoke-static {v1, v0}, Lcom/facebook/messaging/emoji/RecentEmojiView;->a$redex0(Lcom/facebook/messaging/emoji/RecentEmojiView;LX/0Px;)V

    .line 1880660
    iget-object v0, p0, LX/CMT;->a:Lcom/facebook/messaging/emoji/RecentEmojiView;

    const/4 v1, 0x0

    .line 1880661
    iput-object v1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    .line 1880662
    return-void
.end method
