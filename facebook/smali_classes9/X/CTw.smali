.class public final LX/CTw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field private b:Z

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1896110
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v1}, LX/15i;->h(II)Z

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p1, p2, v2}, LX/15i;->j(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, LX/CTw;-><init>(Ljava/lang/String;ZLjava/lang/Integer;Ljava/lang/Integer;)V

    .line 1896111
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1896112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896113
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896114
    iput-object p1, p0, LX/CTw;->a:Ljava/lang/String;

    .line 1896115
    iput-boolean p2, p0, LX/CTw;->b:Z

    .line 1896116
    iput-object p3, p0, LX/CTw;->c:Ljava/lang/Integer;

    .line 1896117
    iput-object p4, p0, LX/CTw;->d:Ljava/lang/Integer;

    .line 1896118
    return-void
.end method
