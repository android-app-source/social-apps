.class public LX/Bda;
.super LX/5Jf;
.source ""

# interfaces
.implements LX/BcX;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1X1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IIZ)V
    .locals 3

    .prologue
    .line 1803901
    new-instance v0, LX/3wu;

    invoke-direct {v0, p1, p2, p3, p4}, LX/3wu;-><init>(Landroid/content/Context;IIZ)V

    invoke-direct {p0, p1, v0}, LX/5Jf;-><init>(Landroid/content/Context;LX/3wu;)V

    .line 1803902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Bda;->a:Ljava/util/List;

    .line 1803903
    new-instance v0, LX/BdZ;

    iget-object v1, p0, LX/Bda;->a:Ljava/util/List;

    invoke-direct {v0, v1}, LX/BdZ;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, LX/3mY;->a(LX/3me;)V

    .line 1803904
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1803914
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    return-object v0
.end method

.method public final a(ILX/1X1;Z)V
    .locals 1

    .prologue
    .line 1803911
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1803912
    invoke-virtual {p0, p1}, LX/3mY;->u_(I)V

    .line 1803913
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1803915
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1803916
    invoke-virtual {p0, p1}, LX/3mY;->c(I)V

    .line 1803917
    return-void
.end method

.method public final b(ILX/1X1;Z)V
    .locals 1

    .prologue
    .line 1803907
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1803908
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1803909
    invoke-virtual {p0, p1}, LX/3mY;->b_(I)V

    .line 1803910
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1803906
    iget-object v0, p0, LX/Bda;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1803905
    const/4 v0, 0x1

    return v0
.end method
