.class public LX/BHZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:F

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:I

.field public f:I

.field public final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0ad;

.field private final j:LX/4mV;

.field public final k:LX/BHt;


# direct methods
.method public constructor <init>(LX/0Px;LX/0ad;LX/4mV;LX/BHt;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            "LX/BHt;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1769400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769401
    const/16 v0, 0x12c

    iput v0, p0, LX/BHZ;->a:I

    .line 1769402
    const/4 v0, 0x0

    iput v0, p0, LX/BHZ;->b:F

    .line 1769403
    iput-boolean v1, p0, LX/BHZ;->d:Z

    .line 1769404
    iput v1, p0, LX/BHZ;->e:I

    .line 1769405
    iput v1, p0, LX/BHZ;->f:I

    .line 1769406
    iput-object p1, p0, LX/BHZ;->c:LX/0Px;

    .line 1769407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    .line 1769408
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BHZ;->h:Ljava/util/ArrayList;

    .line 1769409
    iput-object p2, p0, LX/BHZ;->i:LX/0ad;

    .line 1769410
    iput-object p3, p0, LX/BHZ;->j:LX/4mV;

    .line 1769411
    iput-object p4, p0, LX/BHZ;->k:LX/BHt;

    .line 1769412
    return-void
.end method

.method public static a(LX/BHZ;LX/BIF;)LX/8Hs;
    .locals 8

    .prologue
    .line 1769399
    new-instance v0, LX/8Hs;

    check-cast p1, LX/BIE;

    invoke-interface {p1}, LX/BIE;->getHighlightLayerView()Landroid/view/View;

    move-result-object v1

    const-wide/16 v2, 0x12c

    const/4 v4, 0x0

    iget-object v5, p0, LX/BHZ;->j:LX/4mV;

    const/4 v6, 0x0

    const/high16 v7, 0x3f400000    # 0.75f

    invoke-direct/range {v0 .. v7}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;FF)V

    return-object v0
.end method

.method public static c(LX/BHZ;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1769396
    iput v0, p0, LX/BHZ;->e:I

    .line 1769397
    iput v0, p0, LX/BHZ;->f:I

    .line 1769398
    return-void
.end method

.method public static d(LX/BHZ;)V
    .locals 2

    .prologue
    .line 1769393
    iget-object v0, p0, LX/BHZ;->k:LX/BHt;

    .line 1769394
    const-string v1, "picker_highlights_started_after_one_item"

    const/4 p0, 0x0

    invoke-static {v0, v1, p0}, LX/BHt;->a(LX/BHt;Ljava/lang/String;Ljava/util/Map;)V

    .line 1769395
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1769390
    iget-object v0, p0, LX/BHZ;->k:LX/BHt;

    .line 1769391
    const-string v1, "picker_highlights_stopped_one_item"

    const/4 p0, 0x0

    invoke-static {v0, v1, p0}, LX/BHt;->a(LX/BHt;Ljava/lang/String;Ljava/util/Map;)V

    .line 1769392
    return-void
.end method


# virtual methods
.method public final a(LX/BHi;)V
    .locals 4

    .prologue
    .line 1769350
    invoke-virtual {p1}, LX/BHi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIF;

    .line 1769351
    instance-of v1, v0, LX/BIE;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, LX/BIE;

    invoke-interface {v1}, LX/BIE;->getHighlightLayerView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-interface {v0}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1769352
    invoke-static {p0, v0}, LX/BHZ;->a(LX/BHZ;LX/BIF;)LX/8Hs;

    move-result-object v0

    invoke-virtual {v0}, LX/8Hs;->c()V

    goto :goto_0

    .line 1769353
    :cond_1
    invoke-direct {p0}, LX/BHZ;->e()V

    .line 1769354
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BHZ;->d:Z

    .line 1769355
    iget-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1769356
    invoke-static {p0}, LX/BHZ;->c(LX/BHZ;)V

    .line 1769357
    iget-object v0, p0, LX/BHZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1769358
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1769360
    iget-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1769361
    invoke-static {p0}, LX/BHZ;->c(LX/BHZ;)V

    .line 1769362
    iget-object v0, p0, LX/BHZ;->c:LX/0Px;

    if-nez v0, :cond_1

    .line 1769363
    :cond_0
    return-void

    .line 1769364
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 1769365
    iget-object v0, p0, LX/BHZ;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    move v6, v4

    :goto_0
    if-ge v6, v8, :cond_0

    iget-object v0, p0, LX/BHZ;->c:LX/0Px;

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1769366
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v4

    move v3, v4

    :goto_1
    if-ge v5, v10, :cond_6

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1769367
    instance-of v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    if-eqz v1, :cond_2

    .line 1769368
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v1, v3

    move v3, v4

    :goto_2
    if-ge v3, v12, :cond_4

    invoke-virtual {v11, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    .line 1769369
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1769370
    iget-object v13, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v13

    .line 1769371
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1769372
    iget-object v13, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1769373
    iget v13, p0, LX/BHZ;->e:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, LX/BHZ;->e:I

    .line 1769374
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 1769375
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    :cond_2
    move-object v1, v0

    .line 1769376
    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1769377
    iget-object v11, v1, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v1, v11

    .line 1769378
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1769379
    iget-object v11, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1769380
    instance-of v11, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    if-eqz v11, :cond_5

    .line 1769381
    iget v0, p0, LX/BHZ;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BHZ;->e:I

    .line 1769382
    :cond_3
    :goto_4
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v1, v2

    .line 1769383
    :cond_4
    :goto_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v3, v1

    goto :goto_1

    .line 1769384
    :cond_5
    instance-of v0, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    if-eqz v0, :cond_3

    .line 1769385
    iget v0, p0, LX/BHZ;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BHZ;->f:I

    goto :goto_4

    .line 1769386
    :cond_6
    if-nez v3, :cond_0

    .line 1769387
    iget-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1769388
    invoke-static {p0}, LX/BHZ;->c(LX/BHZ;)V

    .line 1769389
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public final b(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 2

    .prologue
    .line 1769359
    iget-boolean v0, p0, LX/BHZ;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BHZ;->g:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
