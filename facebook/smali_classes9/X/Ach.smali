.class public final LX/Ach;
.super Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;)V
    .locals 0

    .prologue
    .line 1693038
    iput-object p1, p0, LX/Ach;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public final resize(II)Landroid/graphics/Shader;
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1693039
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v4, p2

    new-array v5, v2, [I

    fill-array-data v5, :array_0

    new-array v6, v2, [F

    fill-array-data v6, :array_1

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1693040
    return-object v0

    nop

    :array_0
    .array-data 4
        0x7f000000
        0x0
        0x0
        0x7f000000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3ecccccd    # 0.4f
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data
.end method
