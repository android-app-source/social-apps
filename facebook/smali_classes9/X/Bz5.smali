.class public LX/Bz5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedClass"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9hF;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/9i4;

.field private final d:LX/1eq;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/9i4;LX/1eq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9hF;",
            ">;",
            "LX/9i4;",
            "LX/1eq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838100
    iput-object p1, p0, LX/Bz5;->a:LX/0Ot;

    .line 1838101
    iput-object p2, p0, LX/Bz5;->b:LX/0Ot;

    .line 1838102
    iput-object p3, p0, LX/Bz5;->c:LX/9i4;

    .line 1838103
    iput-object p4, p0, LX/Bz5;->d:LX/1eq;

    .line 1838104
    return-void
.end method

.method private a(ZLcom/facebook/graphql/model/GraphQLStoryAttachment;I)LX/9hD;
    .locals 4

    .prologue
    .line 1838081
    if-eqz p1, :cond_3

    .line 1838082
    iget-object v0, p0, LX/Bz5;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    .line 1838083
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1838084
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1838085
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 1838086
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838087
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1838088
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1838089
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1838090
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1838091
    :cond_1
    new-instance v2, LX/9hE;

    const-class v1, LX/23W;

    new-instance p1, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {p1, v3}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, p1}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v2, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1838092
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1838093
    if-ltz p3, :cond_2

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge p3, v1, :cond_2

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1838094
    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    .line 1838095
    :cond_2
    move-object v0, v2

    .line 1838096
    :goto_1
    return-object v0

    .line 1838097
    :cond_3
    iget-object v0, p0, LX/Bz5;->c:LX/9i4;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9i4;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1838098
    iget-object v1, p0, LX/Bz5;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0, p3}, LX/9hF;->a(LX/0Px;I)LX/9hE;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;)LX/9hN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/26M;",
            ">;",
            "LX/1bf;",
            ")",
            "LX/9hN;"
        }
    .end annotation

    .prologue
    .line 1838069
    new-instance v0, LX/Bz4;

    invoke-direct {v0, p0, p1}, LX/Bz4;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/9hN;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/74S;ZZ)V
    .locals 3
    .param p2    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9hN;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1bf;",
            "LX/74S;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 1838070
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838071
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838072
    invoke-static {p4}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1838073
    invoke-direct {p0, p8, v0, p3}, LX/Bz5;->a(ZLcom/facebook/graphql/model/GraphQLStoryAttachment;I)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    .line 1838074
    iput-boolean p7, v0, LX/9hD;->o:Z

    .line 1838075
    move-object v0, v0

    .line 1838076
    invoke-virtual {v0, v1}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 1838077
    if-eqz p7, :cond_0

    .line 1838078
    iget-object v0, p0, LX/Bz5;->d:LX/1eq;

    invoke-static {p4}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1838079
    :cond_0
    iget-object v0, p0, LX/Bz5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-interface {v0, p1, v1, p2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 1838080
    return-void
.end method
