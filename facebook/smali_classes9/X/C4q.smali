.class public LX/C4q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/CDQ;


# direct methods
.method public constructor <init>(LX/CDQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847994
    iput-object p1, p0, LX/C4q;->a:LX/CDQ;

    .line 1847995
    return-void
.end method

.method public static a(LX/0QB;)LX/C4q;
    .locals 4

    .prologue
    .line 1847982
    const-class v1, LX/C4q;

    monitor-enter v1

    .line 1847983
    :try_start_0
    sget-object v0, LX/C4q;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847984
    sput-object v2, LX/C4q;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847985
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847986
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847987
    new-instance p0, LX/C4q;

    invoke-static {v0}, LX/CDQ;->a(LX/0QB;)LX/CDQ;

    move-result-object v3

    check-cast v3, LX/CDQ;

    invoke-direct {p0, v3}, LX/C4q;-><init>(LX/CDQ;)V

    .line 1847988
    move-object v0, p0

    .line 1847989
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847990
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847991
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847992
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
