.class public LX/AgD;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:I

.field public b:LX/9Ug;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1700405
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AgD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1700406
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1700407
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AgD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700408
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1700409
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700410
    const/4 v0, 0x0

    iput v0, p0, LX/AgD;->a:I

    .line 1700411
    return-void
.end method


# virtual methods
.method public setPressed(Z)V
    .locals 5

    .prologue
    .line 1700412
    invoke-super {p0, p1}, Landroid/view/View;->setPressed(Z)V

    .line 1700413
    invoke-virtual {p0}, LX/AgD;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1700414
    if-eqz p1, :cond_1

    .line 1700415
    const v2, 0x3f4ba2e9

    .line 1700416
    invoke-virtual {p0}, LX/AgD;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1700417
    invoke-virtual {p0}, LX/AgD;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1700418
    :cond_0
    :goto_0
    return-void

    .line 1700419
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1700420
    invoke-virtual {p0}, LX/AgD;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1700421
    invoke-virtual {p0}, LX/AgD;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1700422
    goto :goto_0
.end method

.method public setReaction(LX/1zt;)V
    .locals 1

    .prologue
    .line 1700423
    iget v0, p1, LX/1zt;->e:I

    move v0, v0

    .line 1700424
    iput v0, p0, LX/AgD;->a:I

    .line 1700425
    invoke-virtual {p1}, LX/1zt;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1700426
    invoke-virtual {p0, v0}, LX/AgD;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1700427
    instance-of p1, v0, LX/9Ug;

    if-eqz p1, :cond_0

    .line 1700428
    check-cast v0, LX/9Ug;

    iput-object v0, p0, LX/AgD;->b:LX/9Ug;

    .line 1700429
    :cond_0
    return-void
.end method
