.class public LX/Bl2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Bl2;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/Bky;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/Bky;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1815665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1815666
    iput-object p1, p0, LX/Bl2;->a:Landroid/content/ContentResolver;

    .line 1815667
    iput-object p2, p0, LX/Bl2;->b:LX/Bky;

    .line 1815668
    return-void
.end method

.method public static a(LX/0QB;)LX/Bl2;
    .locals 5

    .prologue
    .line 1815669
    sget-object v0, LX/Bl2;->c:LX/Bl2;

    if-nez v0, :cond_1

    .line 1815670
    const-class v1, LX/Bl2;

    monitor-enter v1

    .line 1815671
    :try_start_0
    sget-object v0, LX/Bl2;->c:LX/Bl2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1815672
    if-eqz v2, :cond_0

    .line 1815673
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1815674
    new-instance p0, LX/Bl2;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v4

    check-cast v4, LX/Bky;

    invoke-direct {p0, v3, v4}, LX/Bl2;-><init>(Landroid/content/ContentResolver;LX/Bky;)V

    .line 1815675
    move-object v0, p0

    .line 1815676
    sput-object v0, LX/Bl2;->c:LX/Bl2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1815677
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1815678
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1815679
    :cond_1
    sget-object v0, LX/Bl2;->c:LX/Bl2;

    return-object v0

    .line 1815680
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1815681
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1815682
    iget-object v0, p0, LX/Bl2;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/Bl2;->b:LX/Bky;

    iget-object v1, v1, LX/Bky;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1815683
    return-void
.end method
