.class public LX/Bvl;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

.field public final d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

.field public final e:LX/Bvk;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field private p:Z

.field public q:Z

.field private r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832821
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bvl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832822
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832819
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bvl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832820
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832807
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832808
    const-class v0, LX/Bvl;

    invoke-static {v0, p0}, LX/Bvl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832809
    const v0, 0x7f031401

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1832810
    const v0, 0x7f0d1050

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    iput-object v0, p0, LX/Bvl;->c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    .line 1832811
    const v0, 0x7f0d104f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    iput-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1832812
    new-instance v0, LX/Bvk;

    invoke-direct {v0, p0}, LX/Bvk;-><init>(LX/Bvl;)V

    iput-object v0, p0, LX/Bvl;->e:LX/Bvk;

    .line 1832813
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvg;

    invoke-direct {v1, p0}, LX/Bvg;-><init>(LX/Bvl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832814
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvh;

    invoke-direct {v1, p0}, LX/Bvh;-><init>(LX/Bvl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832815
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvf;

    invoke-direct {v1, p0}, LX/Bvf;-><init>(LX/Bvl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832816
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvj;

    invoke-direct {v1, p0}, LX/Bvj;-><init>(LX/Bvl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832817
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvi;

    invoke-direct {v1, p0}, LX/Bvi;-><init>(LX/Bvl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832818
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bvl;

    invoke-static {p0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v1

    check-cast v1, LX/Ac6;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object v1, p1, LX/Bvl;->a:LX/Ac6;

    iput-object p0, p1, LX/Bvl;->b:LX/1b4;

    return-void
.end method

.method public static a$redex0(LX/Bvl;Z)V
    .locals 4

    .prologue
    .line 1832799
    iput-boolean p1, p0, LX/Bvl;->q:Z

    .line 1832800
    iget-object v0, p0, LX/Bvl;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1832801
    invoke-static {p0}, LX/Bvl;->h(LX/Bvl;)V

    .line 1832802
    :cond_0
    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_1

    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1832803
    iget-boolean v2, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->J:Z

    move v0, v2

    .line 1832804
    if-nez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1832805
    return-void

    .line 1832806
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, LX/Bvd;

    invoke-direct {v0, p0}, LX/Bvd;-><init>(LX/Bvl;)V

    goto :goto_1
.end method

.method public static c(LX/Bvl;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x2

    const/4 v4, 0x0

    .line 1832769
    iget-object v0, p0, LX/Bvl;->c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 1832770
    if-ne p1, v2, :cond_1

    .line 1832771
    iput-boolean v5, p0, LX/Bvl;->p:Z

    .line 1832772
    iget-object v1, p0, LX/Bvl;->a:LX/Ac6;

    invoke-virtual {v1}, LX/Ac6;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1832773
    const v1, 0x7f0d1974

    invoke-virtual {v0, v2, v1}, LX/2p4;->addRule(II)V

    .line 1832774
    invoke-virtual {v0, v3, v4}, LX/2p4;->addRule(II)V

    .line 1832775
    iput v4, v0, LX/2p4;->bottomMargin:I

    .line 1832776
    iget-object v1, p0, LX/Bvl;->c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setPercent(F)V

    .line 1832777
    iget-object v1, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1832778
    iput-boolean v5, v1, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->D:Z

    .line 1832779
    :goto_0
    iget-object v1, p0, LX/Bvl;->c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1832780
    return-void

    .line 1832781
    :cond_0
    iget-object v1, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1, v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setVisibility(I)V

    .line 1832782
    iget-object v1, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e()V

    goto :goto_0

    .line 1832783
    :cond_1
    iput-boolean v4, p0, LX/Bvl;->p:Z

    .line 1832784
    iget-boolean v1, p0, LX/Bvl;->r:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bvl;->b:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->H()LX/6Ri;

    move-result-object v1

    .line 1832785
    iget-boolean v5, v1, LX/6Ri;->e:Z

    move v1, v5

    .line 1832786
    if-eqz v1, :cond_2

    .line 1832787
    const v1, 0x7f0d1974

    invoke-virtual {v0, v2, v1}, LX/2p4;->addRule(II)V

    .line 1832788
    invoke-virtual {v0, v3, v4}, LX/2p4;->addRule(II)V

    .line 1832789
    invoke-virtual {p0}, LX/Bvl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b04c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, LX/2p4;->bottomMargin:I

    .line 1832790
    :goto_1
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p0}, LX/Bvl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v1, p0, LX/Bvl;->s:Z

    if-eqz v1, :cond_3

    const v1, 0x7f0b0472

    :goto_2
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v1, v2

    iput v1, v0, LX/2p4;->bottomMargin:I

    .line 1832791
    iget-object v1, p0, LX/Bvl;->c:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setPercent(F)V

    .line 1832792
    iget-object v1, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1832793
    iput-boolean v4, v1, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->D:Z

    .line 1832794
    invoke-static {p0}, LX/Bvl;->h(LX/Bvl;)V

    goto :goto_0

    .line 1832795
    :cond_2
    invoke-virtual {v0, v2, v4}, LX/2p4;->addRule(II)V

    .line 1832796
    const v1, 0x7f0d0fc9

    invoke-virtual {v0, v3, v1}, LX/2p4;->addRule(II)V

    .line 1832797
    iput v4, v0, LX/2p4;->bottomMargin:I

    goto :goto_1

    .line 1832798
    :cond_3
    const v1, 0x7f0b0471

    goto :goto_2
.end method

.method public static g$redex0(LX/Bvl;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1832762
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1832763
    iget-boolean v0, p0, LX/Bvl;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->i()I

    move-result v0

    int-to-long v0, v0

    .line 1832764
    :goto_0
    iget-object v2, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    long-to-int v0, v0

    invoke-virtual {v2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setVideoTime(I)V

    .line 1832765
    :cond_0
    iget-object v0, p0, LX/Bvl;->e:LX/Bvk;

    invoke-virtual {v0, v6}, LX/Bvk;->removeMessages(I)V

    .line 1832766
    iget-object v0, p0, LX/Bvl;->e:LX/Bvk;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v6, v2, v3}, LX/Bvk;->sendEmptyMessageDelayed(IJ)Z

    .line 1832767
    return-void

    .line 1832768
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static h(LX/Bvl;)V
    .locals 2

    .prologue
    .line 1832758
    iget-boolean v0, p0, LX/Bvl;->o:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/Bvl;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bvl;->a:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1832759
    :cond_0
    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setVisibility(I)V

    .line 1832760
    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->f()V

    .line 1832761
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1832733
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1832734
    :cond_0
    :goto_0
    return-void

    .line 1832735
    :cond_1
    if-eqz p2, :cond_0

    .line 1832736
    invoke-virtual {p1}, LX/2pa;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Bvl;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->H()LX/6Ri;

    move-result-object v0

    invoke-virtual {v0}, LX/6Ri;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/Bvl;->r:Z

    .line 1832737
    iget-boolean v0, p0, LX/Bvl;->r:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bvl;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->H()LX/6Ri;

    move-result-object v0

    .line 1832738
    iget-boolean v1, v0, LX/6Ri;->d:Z

    move v0, v1

    .line 1832739
    if-eqz v0, :cond_0

    .line 1832740
    :cond_2
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832741
    if-eqz v0, :cond_0

    .line 1832742
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1832743
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1832744
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1832745
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1832746
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1832747
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1832748
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bvl;->f:Ljava/lang/String;

    .line 1832749
    iget-object v0, p0, LX/Bvl;->b:LX/1b4;

    invoke-virtual {v0, v1}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    iput-boolean v0, p0, LX/Bvl;->s:Z

    .line 1832750
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    iput-boolean v0, p0, LX/Bvl;->n:Z

    .line 1832751
    const/4 v0, 0x0

    .line 1832752
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1832753
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1832754
    :cond_3
    iget-object v1, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    iget-object v2, p0, LX/Bvl;->f:Ljava/lang/String;

    iget-boolean v3, p0, LX/Bvl;->n:Z

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1832755
    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b()V

    .line 1832756
    invoke-virtual {p0}, LX/Bvl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/Bvl;->c(LX/Bvl;I)V

    goto/16 :goto_0

    .line 1832757
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1832726
    iget-object v0, p0, LX/Bvl;->e:LX/Bvk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Bvk;->removeMessages(I)V

    .line 1832727
    iget-object v0, p0, LX/Bvl;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->g()V

    .line 1832728
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bvl;->f:Ljava/lang/String;

    .line 1832729
    iput-boolean v2, p0, LX/Bvl;->o:Z

    .line 1832730
    iput-boolean v2, p0, LX/Bvl;->p:Z

    .line 1832731
    iput-boolean v2, p0, LX/Bvl;->q:Z

    .line 1832732
    return-void
.end method
