.class public LX/B8K;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B8K;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B8r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

.field public f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public g:LX/B7w;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Landroid/widget/TextView;

.field public j:LX/B7F;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748726
    new-instance v0, LX/B8G;

    invoke-direct {v0}, LX/B8G;-><init>()V

    sput-object v0, LX/B8K;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1748736
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748737
    const v0, 0x7f0309c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748738
    const v0, 0x7f0d18e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iput-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    .line 1748739
    const v0, 0x7f0d18e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/B8K;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1748740
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B8K;->i:Landroid/widget/TextView;

    .line 1748741
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B8K;

    invoke-static {v0}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v2

    check-cast v2, LX/B6l;

    invoke-static {v0}, LX/B8r;->a(LX/0QB;)LX/B8r;

    move-result-object p1

    check-cast p1, LX/B8r;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object v2, p0, LX/B8K;->b:LX/B6l;

    iput-object p1, p0, LX/B8K;->c:LX/B8r;

    iput-object v0, p0, LX/B8K;->d:LX/B7W;

    .line 1748742
    return-void
.end method

.method public static g(LX/B8K;)V
    .locals 1

    .prologue
    .line 1748734
    const v0, 0x7f020deb

    invoke-static {p0, v0}, LX/B8K;->setIconDrawable(LX/B8K;I)V

    .line 1748735
    return-void
.end method

.method public static setIconDrawable(LX/B8K;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1748731
    invoke-virtual {p0}, LX/B8K;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1748732
    iget-object v1, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1748733
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748727
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748728
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748729
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748730
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 4

    .prologue
    .line 1748695
    iput-object p2, p0, LX/B8K;->j:LX/B7F;

    .line 1748696
    iput-object p1, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748697
    iget-object v0, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1748698
    iget-object v0, p0, LX/B8K;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1748699
    iget-object v0, p0, LX/B8K;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748700
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {p0}, LX/B8K;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a074f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHintTextColor(I)V

    .line 1748701
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1748702
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748703
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1748704
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-ne v1, v2, :cond_1

    .line 1748705
    sget-object v1, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1748706
    const/4 v1, 0x0

    .line 1748707
    :goto_1
    move v1, v1

    .line 1748708
    if-nez v1, :cond_1

    .line 1748709
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748710
    :goto_2
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, LX/B8K;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090011

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1748711
    iget-object v1, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748712
    :cond_0
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    .line 1748713
    sget-object v1, LX/B75;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1748714
    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 1748715
    or-int/lit16 v1, v1, 0x90

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setInputType(I)V

    .line 1748716
    invoke-static {p0}, LX/B8K;->g(LX/B8K;)V

    .line 1748717
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    new-instance v1, LX/B8H;

    invoke-direct {v1, p0}, LX/B8H;-><init>(LX/B8K;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748718
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    new-instance v1, LX/B8I;

    invoke-direct {v1, p0}, LX/B8I;-><init>(LX/B8K;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748719
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    new-instance v1, LX/B8J;

    invoke-direct {v1, p0}, LX/B8J;-><init>(LX/B8K;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748720
    return-void

    .line 1748721
    :cond_1
    iget-object v1, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1748722
    :cond_2
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1748723
    iget-object v0, p0, LX/B8K;->h:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    .line 1748724
    :pswitch_0
    const/16 v1, 0x21

    goto :goto_3

    .line 1748725
    :pswitch_1
    const/4 v1, 0x3

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748692
    const v0, 0x7f020dec

    invoke-static {p0, v0}, LX/B8K;->setIconDrawable(LX/B8K;I)V

    .line 1748693
    iget-object v0, p0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748694
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748743
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748744
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748689
    invoke-static {p0}, LX/B8K;->g(LX/B8K;)V

    .line 1748690
    iget-object v0, p0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748691
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1748687
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setImeOptions(I)V

    .line 1748688
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748679
    iget-object v0, p0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748686
    invoke-virtual {p0}, LX/B8K;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748685
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748682
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748683
    iget-object v0, p0, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->clearFocus()V

    .line 1748684
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748680
    iput-object p1, p0, LX/B8K;->g:LX/B7w;

    .line 1748681
    return-void
.end method
