.class public final LX/BK3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772567
    iput-object p1, p0, LX/BK3;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1772568
    iget-object v0, p0, LX/BK3;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772569
    iget-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->M:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ae:Z

    if-nez v1, :cond_0

    .line 1772570
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1772571
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 1772572
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1772573
    move-object v1, v1

    .line 1772574
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->S:I

    .line 1772575
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    iget v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->S:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 1772576
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->N:LX/0wd;

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 1772577
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->M:Z

    .line 1772578
    :cond_0
    return-void
.end method
