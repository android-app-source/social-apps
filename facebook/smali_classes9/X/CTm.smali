.class public final LX/CTm;
.super LX/CTK;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTi;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/CTl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTj;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 1895973
    sget-object v4, LX/CSS;->SHOPPING_CART_SIMPLE:LX/CSS;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v4, v2}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    .line 1895974
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->W()LX/2uF;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895975
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1895976
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->O()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_1

    .line 1895977
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->O()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v6, v4, LX/1vs;->b:I

    .line 1895978
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->O()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 1895979
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const/4 v9, 0x1

    invoke-virtual {v5, v6, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v7, v8, v6}, LX/15i;->j(II)I

    move-result v6

    int-to-long v6, v6

    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    :goto_0
    move-object/from16 v0, p0

    iput-object v4, v0, LX/CTm;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1895980
    move-object/from16 v0, p0

    iget-object v4, v0, LX/CTm;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v4, :cond_0

    .line 1895981
    move-object/from16 v0, p0

    iget-object v4, v0, LX/CTm;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v4}, Lcom/facebook/payments/currency/CurrencyAmount;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895982
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/CTm;->b:Ljava/util/ArrayList;

    .line 1895983
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->C()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, v8, :cond_3

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    .line 1895984
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v10, 0x26f30b65

    invoke-static {v9, v5, v10}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1895985
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 1895986
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1895987
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v10

    iget-object v12, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 1895988
    new-instance v13, LX/CTi;

    new-instance v14, Lcom/facebook/payments/currency/CurrencyAmount;

    const/4 v15, 0x1

    invoke-virtual {v9, v5, v15}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-virtual {v12, v10, v9}, LX/15i;->j(II)I

    move-result v9

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v14, v5, v0, v1}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v13, v14, v5}, LX/CTi;-><init>(Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V

    .line 1895989
    move-object/from16 v0, p0

    iget-object v5, v0, LX/CTm;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895990
    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v9, 0x1

    invoke-virtual {v5, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895991
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 1895992
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1895993
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 1895994
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/CTm;->k:Ljava/util/ArrayList;

    .line 1895995
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, LX/CTm;->l:Ljava/util/ArrayList;

    .line 1895996
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->W()LX/2uF;

    move-result-object v4

    invoke-virtual {v4}, LX/3Sa;->e()LX/3Sh;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v12}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v10, v4, LX/1vs;->a:LX/15i;

    iget v13, v4, LX/1vs;->b:I

    .line 1895997
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895998
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895999
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->jS_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1896000
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_4

    .line 1896001
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896002
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v10, v13, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1896003
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_6

    .line 1896004
    const/4 v4, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v6, v4, LX/1vs;->b:I

    .line 1896005
    const/4 v4, 0x5

    const-class v7, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 1896006
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const/4 v9, 0x1

    invoke-virtual {v5, v6, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v7, v8, v6}, LX/15i;->j(II)I

    move-result v6

    int-to-long v6, v6

    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    move-object v5, v4

    .line 1896007
    :goto_5
    move-object/from16 v0, p0

    iget-object v6, v0, LX/CTm;->l:Ljava/util/ArrayList;

    new-instance v7, LX/CTk;

    new-instance v8, LX/CU0;

    const/4 v4, 0x5

    const-class v9, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v9}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, LX/CUx;

    invoke-direct {v8, v4}, LX/CU0;-><init>(LX/CUx;)V

    const/4 v4, 0x2

    invoke-virtual {v10, v13, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v8, v5, v4}, LX/CTk;-><init>(LX/CU0;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1896008
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1896009
    :cond_6
    const/4 v4, 0x0

    move-object v5, v4

    goto :goto_5

    .line 1896010
    :cond_7
    const/4 v4, 0x3

    invoke-virtual {v10, v13, v4}, LX/15i;->g(II)I

    move-result v4

    if-eqz v4, :cond_8

    .line 1896011
    const/4 v4, 0x3

    invoke-virtual {v10, v13, v4}, LX/15i;->g(II)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v10, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896012
    :cond_8
    const/4 v4, 0x3

    invoke-virtual {v10, v13, v4}, LX/15i;->g(II)I

    move-result v4

    if-eqz v4, :cond_9

    .line 1896013
    const/4 v4, 0x3

    invoke-virtual {v10, v13, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1896014
    const/4 v5, 0x3

    invoke-virtual {v10, v13, v5}, LX/15i;->g(II)I

    move-result v6

    .line 1896015
    new-instance v5, Lcom/facebook/payments/currency/CurrencyAmount;

    const/4 v7, 0x1

    invoke-virtual {v10, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v10, v6, v7}, LX/15i;->j(II)I

    move-result v6

    int-to-long v6, v6

    invoke-direct {v5, v4, v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 1896016
    :goto_6
    const/4 v4, 0x5

    const-class v6, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_a

    .line 1896017
    const/4 v4, 0x5

    const-class v6, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 1896018
    const/4 v4, 0x5

    const-class v6, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v4, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v9, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1896019
    new-instance v6, Lcom/facebook/payments/currency/CurrencyAmount;

    const/4 v14, 0x1

    invoke-virtual {v7, v8, v14}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v9, v4, v8}, LX/15i;->j(II)I

    move-result v4

    int-to-long v8, v4

    invoke-direct {v6, v7, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 1896020
    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, LX/CTm;->k:Ljava/util/ArrayList;

    new-instance v4, LX/CTj;

    const/4 v7, 0x4

    invoke-virtual {v10, v13, v7}, LX/15i;->j(II)I

    move-result v7

    const/4 v8, 0x1

    invoke-virtual {v10, v13, v8}, LX/15i;->j(II)I

    move-result v8

    const/4 v9, 0x5

    const-class v15, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10, v13, v9, v15}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v9

    check-cast v9, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v9}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->jS_()Ljava/lang/String;

    move-result-object v9

    const/4 v15, 0x5

    const-class v16, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-object/from16 v0, v16

    invoke-virtual {v10, v13, v15, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v10

    check-cast v10, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {v10}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;->c()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, LX/CTj;-><init>(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;IILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1896021
    :cond_9
    const/4 v5, 0x0

    goto :goto_6

    .line 1896022
    :cond_a
    const/4 v6, 0x0

    goto :goto_7

    .line 1896023
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ac()LX/1vs;

    move-result-object v4

    iget-object v9, v4, LX/1vs;->a:LX/15i;

    iget v10, v4, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1896024
    if-nez v10, :cond_c

    const/4 v4, 0x0

    :goto_8
    move-object/from16 v0, p0

    iput-object v4, v0, LX/CTm;->d:LX/CTl;

    .line 1896025
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->y()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/CTm;->a:Z

    .line 1896026
    const/4 v4, 0x1

    move v5, v4

    :goto_9
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v5, v4, :cond_e

    .line 1896027
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1896028
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_9

    .line 1896029
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 1896030
    :cond_c
    const/4 v4, 0x4

    invoke-virtual {v9, v10, v4}, LX/15i;->n(II)LX/22e;

    move-result-object v7

    .line 1896031
    new-instance v4, LX/CTl;

    const/4 v5, 0x1

    invoke-virtual {v9, v10, v5}, LX/15i;->j(II)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v9, v10, v6}, LX/15i;->h(II)Z

    move-result v6

    if-eqz v7, :cond_d

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    :goto_a
    const/4 v8, 0x3

    invoke-virtual {v9, v10, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v12}, LX/15i;->h(II)Z

    move-result v9

    invoke-direct/range {v4 .. v9}, LX/CTl;-><init>(IZLX/0Px;Ljava/lang/String;Z)V

    goto :goto_8

    :cond_d
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v7

    goto :goto_a

    .line 1896032
    :cond_e
    return-void
.end method
