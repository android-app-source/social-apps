.class public LX/AY2;
.super LX/AWm;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684744
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AY2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684745
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684746
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AY2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684747
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684748
    invoke-direct {p0, p1, p2, p3}, LX/AWm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684749
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1684750
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 1684751
    const v2, 0x7f010470

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1684752
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 1684753
    invoke-virtual {p0}, LX/AY2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setDescription(Ljava/lang/CharSequence;)V

    .line 1684754
    invoke-virtual {p0}, LX/AY2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setTitle(Ljava/lang/CharSequence;)V

    .line 1684755
    invoke-virtual {p0}, LX/AY2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c84

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AWm;->setActionFinishText(Ljava/lang/CharSequence;)V

    .line 1684756
    invoke-virtual {p0}, LX/AY2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0206cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1684757
    iget-object v1, p0, LX/AWm;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1684758
    iget-object v0, p0, LX/AWm;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1684759
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1684760
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->ABOUT_TO_FINISH:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1684761
    return-void
.end method
