.class public LX/BOi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/5JV;

.field public final c:LX/BMm;

.field public final d:LX/BMP;

.field public final e:LX/B5l;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/5JV;LX/BMm;LX/BMP;LX/B5l;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1780020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780021
    iput-object p1, p0, LX/BOi;->a:Landroid/content/res/Resources;

    .line 1780022
    iput-object p2, p0, LX/BOi;->b:LX/5JV;

    .line 1780023
    iput-object p3, p0, LX/BOi;->c:LX/BMm;

    .line 1780024
    iput-object p4, p0, LX/BOi;->d:LX/BMP;

    .line 1780025
    iput-object p5, p0, LX/BOi;->e:LX/B5l;

    .line 1780026
    return-void
.end method

.method public static a(LX/0QB;)LX/BOi;
    .locals 9

    .prologue
    .line 1780009
    const-class v1, LX/BOi;

    monitor-enter v1

    .line 1780010
    :try_start_0
    sget-object v0, LX/BOi;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1780011
    sput-object v2, LX/BOi;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1780012
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780013
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1780014
    new-instance v3, LX/BOi;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/5JV;->a(LX/0QB;)LX/5JV;

    move-result-object v5

    check-cast v5, LX/5JV;

    invoke-static {v0}, LX/BMm;->a(LX/0QB;)LX/BMm;

    move-result-object v6

    check-cast v6, LX/BMm;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v7

    check-cast v7, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v8

    check-cast v8, LX/B5l;

    invoke-direct/range {v3 .. v8}, LX/BOi;-><init>(Landroid/content/res/Resources;LX/5JV;LX/BMm;LX/BMP;LX/B5l;)V

    .line 1780015
    move-object v0, v3

    .line 1780016
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1780017
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BOi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780018
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1780019
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
