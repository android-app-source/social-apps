.class public final LX/BXG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1792986
    iput-object p1, p0, LX/BXG;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1792987
    iget-object v0, p0, LX/BXG;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v1, p0, LX/BXG;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v1, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    invoke-virtual {v1}, LX/BXD;->a()LX/0Rf;

    move-result-object v1

    .line 1792988
    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1792989
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792990
    iget-object p1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    const/4 p2, 0x0

    invoke-virtual {p1, v2, p2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    goto :goto_0

    .line 1792991
    :cond_0
    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const p0, 0xf4fc4d0

    invoke-static {v2, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1792992
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D()V

    .line 1792993
    return-void
.end method
