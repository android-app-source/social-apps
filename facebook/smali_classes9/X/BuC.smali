.class public final LX/BuC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BuJ;


# direct methods
.method public constructor <init>(LX/BuJ;)V
    .locals 0

    .prologue
    .line 1830574
    iput-object p1, p0, LX/BuC;->a:LX/BuJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x53d8b9b8

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1830575
    iget-object v0, p0, LX/BuC;->a:LX/BuJ;

    iget-object v2, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830576
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1830577
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830578
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1830579
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1830580
    iget-object v3, p0, LX/BuC;->a:LX/BuJ;

    iget-object v3, v3, LX/BuJ;->p:LX/3iG;

    sget-object v4, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v3, v4}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    const-string v6, "newsfeed_ufi"

    iget-object v7, p0, LX/BuC;->a:LX/BuJ;

    .line 1830581
    iget-object v9, v7, LX/2oy;->j:LX/2pb;

    .line 1830582
    iget-object p1, v9, LX/2pb;->D:LX/04G;

    move-object v9, p1

    .line 1830583
    sget-object p1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v9, p1, :cond_1

    .line 1830584
    const-string v9, "video_fullscreen_player"

    .line 1830585
    :goto_0
    move-object v7, v9

    .line 1830586
    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v4}, LX/3iK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1830587
    iget-object v0, p0, LX/BuC;->a:LX/BuJ;

    iget-object v0, v0, LX/BuJ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnQ;

    const-string v3, "video"

    invoke-virtual {v0, v2, v3}, LX/AnQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1830588
    :cond_0
    const v0, -0x72a42f5e

    invoke-static {v8, v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const-string v9, "video"

    goto :goto_0
.end method
