.class public LX/ATw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ATv;)V
    .locals 2

    .prologue
    .line 1677355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677356
    iget-object v0, p1, LX/ATv;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, LX/ATw;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1677357
    iget-object v0, p1, LX/ATv;->b:LX/0Px;

    iput-object v0, p0, LX/ATw;->b:LX/0Px;

    .line 1677358
    iget-object v0, p0, LX/ATw;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATw;->b:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attachments are mutually exclusive."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1677359
    return-void

    .line 1677360
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
