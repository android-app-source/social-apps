.class public final LX/CPB;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPB;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CP9;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884122
    const/4 v0, 0x0

    sput-object v0, LX/CPB;->a:LX/CPB;

    .line 1884123
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPB;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884124
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884125
    new-instance v0, LX/CPC;

    invoke-direct {v0}, LX/CPC;-><init>()V

    iput-object v0, p0, LX/CPB;->c:LX/CPC;

    .line 1884126
    return-void
.end method

.method public static declared-synchronized q()LX/CPB;
    .locals 2

    .prologue
    .line 1884118
    const-class v1, LX/CPB;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPB;->a:LX/CPB;

    if-nez v0, :cond_0

    .line 1884119
    new-instance v0, LX/CPB;

    invoke-direct {v0}, LX/CPB;-><init>()V

    sput-object v0, LX/CPB;->a:LX/CPB;

    .line 1884120
    :cond_0
    sget-object v0, LX/CPB;->a:LX/CPB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1884090
    check-cast p2, LX/CPA;

    .line 1884091
    iget-object v0, p2, LX/CPA;->a:LX/CNb;

    iget-object v1, p2, LX/CPA;->b:LX/CNc;

    iget-object v2, p2, LX/CPA;->c:Ljava/util/List;

    .line 1884092
    const-string v3, "children"

    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1884093
    if-nez v3, :cond_0

    .line 1884094
    const/4 v3, 0x0

    .line 1884095
    :goto_0
    move-object v0, v3

    .line 1884096
    return-object v0

    .line 1884097
    :cond_0
    invoke-static {v3, v1, p1}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v3

    .line 1884098
    invoke-static {p1, v3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v3

    const-string v4, "align-self"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1884099
    const/4 p0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch p0, :pswitch_data_0

    .line 1884100
    :goto_2
    :pswitch_0
    move v4, v5

    .line 1884101
    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const-string v4, "grow"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v4

    invoke-interface {v3, v4}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    const-string v4, "shrink"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v4

    invoke-interface {v3, v4}, LX/1Di;->c(F)LX/1Di;

    move-result-object v3

    .line 1884102
    const-string v4, "basis"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1884103
    const-string v4, "basis"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    invoke-interface {v3, v4}, LX/1Di;->d(I)LX/1Di;

    .line 1884104
    :cond_2
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884105
    if-eqz v4, :cond_3

    .line 1884106
    const v5, -0x12ba54e9

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884107
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884108
    :cond_3
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884109
    if-eqz v4, :cond_4

    .line 1884110
    const v5, -0x2db2ca4c

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884111
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884112
    :cond_4
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 1884113
    :sswitch_0
    const-string p2, "AUTO"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    move p0, v5

    goto :goto_1

    :sswitch_1
    const-string p2, "START"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    move p0, v6

    goto :goto_1

    :sswitch_2
    const-string p2, "CENTER"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    move p0, v7

    goto/16 :goto_1

    :sswitch_3
    const-string p2, "END"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    move p0, v8

    goto/16 :goto_1

    :sswitch_4
    const-string p2, "STRETCH"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    move p0, v9

    goto/16 :goto_1

    :pswitch_1
    move v5, v6

    .line 1884114
    goto/16 :goto_2

    :pswitch_2
    move v5, v7

    .line 1884115
    goto/16 :goto_2

    :pswitch_3
    move v5, v8

    .line 1884116
    goto/16 :goto_2

    :pswitch_4
    move v5, v9

    .line 1884117
    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x455f8d1b -> :sswitch_4
        0x10cbb -> :sswitch_3
        0x1ed5af -> :sswitch_0
        0x4b8cc42 -> :sswitch_1
        0x7645c055 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884077
    invoke-static {}, LX/1dS;->b()V

    .line 1884078
    iget v0, p1, LX/1dQ;->b:I

    .line 1884079
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884080
    :goto_0
    return-object v0

    .line 1884081
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884082
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884083
    move-object v0, v1

    .line 1884084
    goto :goto_0

    .line 1884085
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884086
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884087
    const/4 v2, 0x1

    move v2, v2

    .line 1884088
    move v0, v2

    .line 1884089
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2db2ca4c -> :sswitch_1
        -0x12ba54e9 -> :sswitch_0
    .end sparse-switch
.end method
