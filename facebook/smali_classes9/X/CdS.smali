.class public LX/CdS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/CdR;)V
    .locals 1

    .prologue
    .line 1922616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922617
    iget-object v0, p1, LX/CdR;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922618
    iget-boolean v0, p1, LX/CdR;->b:Z

    iput-boolean v0, p0, LX/CdS;->b:Z

    .line 1922619
    iget-boolean v0, p1, LX/CdR;->c:Z

    iput-boolean v0, p0, LX/CdS;->c:Z

    .line 1922620
    iget-boolean v0, p1, LX/CdR;->d:Z

    iput-boolean v0, p0, LX/CdS;->d:Z

    .line 1922621
    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/CdR;
    .locals 2

    .prologue
    .line 1922622
    new-instance v0, LX/CdR;

    invoke-direct {v0, p0}, LX/CdR;-><init>(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 1922623
    iget-boolean v0, p0, LX/CdS;->b:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1922624
    iput-boolean p1, p0, LX/CdS;->e:Z

    .line 1922625
    return-void
.end method
