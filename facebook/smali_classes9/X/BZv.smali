.class public final LX/BZv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

.field public b:LX/BZx;


# direct methods
.method public constructor <init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZx;)V
    .locals 1

    .prologue
    .line 1798228
    iput-object p1, p0, LX/BZv;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798229
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZx;

    iput-object v0, p0, LX/BZv;->b:LX/BZx;

    .line 1798230
    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 1798233
    iget-object v0, p0, LX/BZv;->b:LX/BZx;

    invoke-virtual {v0}, LX/BZx;->a()Landroid/view/View;

    move-result-object v0

    .line 1798234
    if-eqz v0, :cond_0

    .line 1798235
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1798236
    :cond_0
    iget-object v0, p0, LX/BZv;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->p:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$FadeOutAnimationListener$1;

    invoke-direct {v1, p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$FadeOutAnimationListener$1;-><init>(LX/BZv;)V

    const v2, 0x12e2a63

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1798237
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1798232
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1798231
    return-void
.end method
