.class public LX/BfD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BfC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BfC",
        "<",
        "LX/97f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805757
    iput-object p1, p0, LX/BfD;->a:LX/0Or;

    .line 1805758
    return-void
.end method


# virtual methods
.method public final a()LX/BeD;
    .locals 1

    .prologue
    .line 1805759
    sget-object v0, LX/BeD;->CATEGORY_PICKER:LX/BeD;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1805760
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/BfD;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1805761
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->CATEGORY_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1805762
    const-string v1, "extra_logger_type"

    sget-object v2, LX/9kb;->NO_LOGGER:LX/9kb;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1805763
    return-object v0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1805764
    check-cast p2, LX/97f;

    .line 1805765
    const-string v0, "extra_picked_category"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 1805766
    invoke-static {p2, v0}, LX/BgV;->a(LX/97f;Lcom/facebook/ipc/model/PageTopic;)LX/97f;

    move-result-object v0

    return-object v0
.end method
