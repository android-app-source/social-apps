.class public final LX/BiC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BiF;

.field public final synthetic b:LX/BiD;


# direct methods
.method public constructor <init>(LX/BiD;LX/BiF;)V
    .locals 0

    .prologue
    .line 1809982
    iput-object p1, p0, LX/BiC;->b:LX/BiD;

    iput-object p2, p0, LX/BiC;->a:LX/BiF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809983
    iget-object v0, p0, LX/BiC;->b:LX/BiD;

    iget-object v0, v0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v0, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1809984
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1809985
    if-eqz v0, :cond_0

    .line 1809986
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1809987
    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1809988
    :cond_0
    :goto_0
    return-void

    .line 1809989
    :cond_1
    iget-object v0, p0, LX/BiC;->b:LX/BiD;

    iget-object v0, v0, LX/BiD;->a:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iget-object v1, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1809990
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1809991
    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsQueryModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    .line 1809992
    if-nez v0, :cond_2

    .line 1809993
    :goto_1
    goto :goto_0

    .line 1809994
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    iget-object p1, v1, LX/Bi8;->e:LX/0Px;

    invoke-virtual {p0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    iput-object p0, v1, LX/Bi8;->e:LX/0Px;

    .line 1809995
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 1809996
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;->b()Z

    move-result p0

    iput-boolean p0, v1, LX/Bi8;->h:Z

    .line 1809997
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;->b()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, LX/Bi8;->i:Ljava/lang/String;

    .line 1809998
    :cond_3
    const p0, 0x7f9da900

    invoke-static {v1, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1809999
    iget-object v0, p0, LX/BiC;->a:LX/BiF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BiF;->a(Z)V

    .line 1810000
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1810001
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/BiC;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
