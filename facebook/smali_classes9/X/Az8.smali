.class public LX/Az8;
.super LX/0Rc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rc",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1732161
    invoke-direct {p0}, LX/0Rc;-><init>()V

    .line 1732162
    iput-object p1, p0, LX/Az8;->a:LX/0Rc;

    .line 1732163
    invoke-direct {p0}, LX/Az8;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    move-result-object v0

    iput-object v0, p0, LX/Az8;->c:Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1732164
    return-void
.end method

.method private a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1732171
    iget-object v0, p0, LX/Az8;->b:LX/0Rc;

    if-eqz v0, :cond_1

    .line 1732172
    iget-object v0, p0, LX/Az8;->b:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1732173
    iget-object v2, p0, LX/Az8;->b:LX/0Rc;

    invoke-virtual {v2}, LX/0Rc;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1732174
    iput-object v1, p0, LX/Az8;->b:LX/0Rc;

    .line 1732175
    :cond_0
    :goto_0
    return-object v0

    .line 1732176
    :cond_1
    iget-object v0, p0, LX/Az8;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 1732177
    goto :goto_0

    .line 1732178
    :cond_2
    iget-object v0, p0, LX/Az8;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1732179
    instance-of v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    if-eqz v1, :cond_3

    .line 1732180
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    iput-object v0, p0, LX/Az8;->b:LX/0Rc;

    .line 1732181
    iget-object v0, p0, LX/Az8;->b:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    .line 1732182
    iget-object v1, p0, LX/Az8;->b:LX/0Rc;

    invoke-virtual {v1}, LX/0Rc;->hasNext()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    .line 1732183
    :cond_3
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    goto :goto_0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1732170
    iget-object v0, p0, LX/Az8;->c:Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1732165
    iget-object v0, p0, LX/Az8;->c:Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1732166
    if-nez v0, :cond_0

    .line 1732167
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1732168
    :cond_0
    invoke-direct {p0}, LX/Az8;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    move-result-object v1

    iput-object v1, p0, LX/Az8;->c:Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1732169
    return-object v0
.end method
