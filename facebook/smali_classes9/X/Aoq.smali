.class public final LX/Aoq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1714914
    iput-object p1, p0, LX/Aoq;->e:LX/Aov;

    iput-object p2, p0, LX/Aoq;->a:Landroid/view/View;

    iput-object p3, p0, LX/Aoq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/Aoq;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aoq;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 1714915
    iget-object v0, p0, LX/Aoq;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Activity;

    .line 1714916
    iget-object v0, p0, LX/Aoq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1714917
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1714918
    move-object v4, v0

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714919
    iget-object v0, p0, LX/Aoq;->e:LX/Aov;

    iget-object v0, v0, LX/Aov;->v:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/Aoq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v2, LX/21D;->NEWSFEED:LX/21D;

    const-string v3, "shareToYourPageButton"

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    .line 1714920
    iget-object v0, p0, LX/Aoq;->e:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aoq;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Aoq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1714921
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1714922
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aoq;->e:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714923
    iget-object v5, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1714924
    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aoq;->d:Ljava/lang/String;

    .line 1714925
    iget-object v9, v0, LX/1EQ;->a:LX/0Zb;

    const-string v10, "feed_share_action"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v9

    .line 1714926
    invoke-virtual {v9}, LX/0oG;->a()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1714927
    invoke-virtual {v9, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v10

    const-string v11, "share_type"

    const-string p1, "share_to_your_page"

    invoke-virtual {v10, v11, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v10

    const-string v11, "composer_session_id"

    invoke-virtual {v10, v11, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v10

    const-string v11, "story_id"

    invoke-virtual {v10, v11, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v10

    const-string v11, "user_id"

    invoke-virtual {v10, v11, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v10

    const-string v11, "shareable_id"

    invoke-virtual {v10, v11, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714928
    invoke-virtual {v9}, LX/0oG;->d()V

    .line 1714929
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;

    invoke-direct {v0, v6, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_composer_configuration"

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1714930
    iget-object v1, p0, LX/Aoq;->e:LX/Aov;

    iget-object v1, v1, LX/Aov;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x6dc

    invoke-interface {v1, v0, v2, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1714931
    return v8
.end method
