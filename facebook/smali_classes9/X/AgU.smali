.class public LX/AgU;
.super LX/AVi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1zf;

.field public final b:LX/AgV;

.field public final c:LX/0So;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AgD;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLStory;

.field public f:J

.field public g:F

.field public h:LX/Adu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1zf;LX/AgV;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700932
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1700933
    const/4 v0, 0x0

    iput v0, p0, LX/AgU;->g:F

    .line 1700934
    iput-object p1, p0, LX/AgU;->a:LX/1zf;

    .line 1700935
    iput-object p2, p0, LX/AgU;->b:LX/AgV;

    .line 1700936
    iput-object p3, p0, LX/AgU;->c:LX/0So;

    .line 1700937
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AgU;->d:Ljava/util/List;

    .line 1700938
    return-void
.end method

.method public static a(LX/0QB;)LX/AgU;
    .locals 6

    .prologue
    .line 1700947
    new-instance v3, LX/AgU;

    invoke-static {p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v0

    check-cast v0, LX/1zf;

    .line 1700948
    new-instance v5, LX/AgV;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v1, v2, v4}, LX/AgV;-><init>(LX/0tX;Ljava/lang/String;LX/0SG;)V

    .line 1700949
    move-object v1, v5

    .line 1700950
    check-cast v1, LX/AgV;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-direct {v3, v0, v1, v2}, LX/AgU;-><init>(LX/1zf;LX/AgV;LX/0So;)V

    .line 1700951
    move-object v0, v3

    .line 1700952
    return-object v0
.end method

.method public static a(LX/AgU;Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;)V
    .locals 2

    .prologue
    .line 1700944
    iget-object v0, p0, LX/AgU;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgD;

    .line 1700945
    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 1700946
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1700953
    check-cast p1, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    check-cast p2, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    .line 1700954
    invoke-static {p0, p2}, LX/AgU;->a(LX/AgU;Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;)V

    .line 1700955
    iget-object v0, p0, LX/AgU;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgD;

    .line 1700956
    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 1700957
    :cond_0
    iget-object v0, p2, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    move-object v0, v0

    .line 1700958
    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setSelectedReaction(Landroid/view/View;)V

    .line 1700959
    return-void
.end method

.method public final bridge synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1700943
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1700939
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1700940
    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-static {p0, v0}, LX/AgU;->a(LX/AgU;Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;)V

    .line 1700941
    iget-object v0, p0, LX/AgU;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1700942
    return-void
.end method
