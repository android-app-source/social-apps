.class public final LX/BQd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9fc;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic d:Z

.field public final synthetic e:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V
    .locals 0

    .prologue
    .line 1782469
    iput-object p1, p0, LX/BQd;->e:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iput-object p2, p0, LX/BQd;->a:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    iput-object p3, p0, LX/BQd;->b:Ljava/lang/String;

    iput-object p4, p0, LX/BQd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-boolean p5, p0, LX/BQd;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 6

    .prologue
    .line 1782474
    iget-object v0, p0, LX/BQd;->e:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v1, p0, LX/BQd;->a:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, LX/BQd;->b:Ljava/lang/String;

    iget-object v4, p0, LX/BQd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-boolean v5, p0, LX/BQd;->d:Z

    .line 1782475
    invoke-static/range {v0 .. v5}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->a$redex0(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 1782476
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 6

    .prologue
    .line 1782470
    const-class v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    const-string v1, "Failed to crop cover photo for optimistic preview: %s"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1782471
    iget-object v0, p0, LX/BQd;->e:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v1, p0, LX/BQd;->a:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    const/4 v2, 0x0

    iget-object v3, p0, LX/BQd;->b:Ljava/lang/String;

    iget-object v4, p0, LX/BQd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    const/4 v5, 0x0

    .line 1782472
    invoke-static/range {v0 .. v5}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->a$redex0(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 1782473
    return-void
.end method
