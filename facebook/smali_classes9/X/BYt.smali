.class public LX/BYt;
.super LX/BYp;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796635
    invoke-direct {p0}, LX/BYp;-><init>()V

    return-void
.end method


# virtual methods
.method public getDiffuseTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796634
    iget-object v0, p0, LX/BYt;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNormalTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796633
    iget-object v0, p0, LX/BYt;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getSpecularTextureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796636
    iget-object v0, p0, LX/BYt;->c:Ljava/lang/String;

    return-object v0
.end method

.method public setDiffuseTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796631
    iput-object p1, p0, LX/BYt;->a:Ljava/lang/String;

    .line 1796632
    return-void
.end method

.method public setNormalTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796629
    iput-object p1, p0, LX/BYt;->b:Ljava/lang/String;

    .line 1796630
    return-void
.end method

.method public setSpecularTextureName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796627
    iput-object p1, p0, LX/BYt;->c:Ljava/lang/String;

    .line 1796628
    return-void
.end method
