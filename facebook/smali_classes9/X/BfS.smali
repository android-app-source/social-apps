.class public final LX/BfS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

.field public final synthetic d:LX/BfX;


# direct methods
.method public constructor <init>(LX/BfX;IILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V
    .locals 0

    .prologue
    .line 1806034
    iput-object p1, p0, LX/BfS;->d:LX/BfX;

    iput p2, p0, LX/BfS;->a:I

    iput p3, p0, LX/BfS;->b:I

    iput-object p4, p0, LX/BfS;->c:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 6

    .prologue
    .line 1806035
    iget-object v0, p0, LX/BfS;->d:LX/BfX;

    iget-object v0, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v1, p0, LX/BfS;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v0

    iget v1, p0, LX/BfS;->b:I

    iget-object v2, p0, LX/BfS;->d:LX/BfX;

    iget-object v2, v2, LX/BfX;->b:LX/Bez;

    iget v3, p0, LX/BfS;->a:I

    invoke-virtual {v2, v3, p2, p3}, LX/Bez;->a(III)J

    move-result-wide v2

    iget-object v4, p0, LX/BfS;->d:LX/BfX;

    iget-object v4, v4, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v5, p0, LX/BfS;->a:I

    invoke-virtual {v4, v5}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    iget-object v4, v4, LX/Bex;->a:LX/0Px;

    iget v5, p0, LX/BfS;->b:I

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    iget-wide v4, v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-static/range {v0 .. v5}, LX/BfK;->a(LX/Bex;IJJ)LX/Bex;

    move-result-object v0

    .line 1806036
    iget-object v1, p0, LX/BfS;->d:LX/BfX;

    iget-object v1, v1, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v2, p0, LX/BfS;->a:I

    invoke-virtual {v1, v2, v0}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(ILX/Bex;)V

    .line 1806037
    iget-object v1, p0, LX/BfS;->d:LX/BfX;

    iget-object v2, p0, LX/BfS;->c:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iget v3, p0, LX/BfS;->b:I

    invoke-static {v1, v2, v0, v3}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/Bex;I)V

    .line 1806038
    iget-object v0, p0, LX/BfS;->d:LX/BfX;

    iget-object v0, v0, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806039
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1806040
    const-string v2, "extra_hours_selected_option"

    iget-object v3, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806041
    iget v4, v3, LX/BfX;->f:I

    move v3, v4

    .line 1806042
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806043
    iget-object v2, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806044
    iget v3, v2, LX/BfX;->f:I

    move v2, v3

    .line 1806045
    if-nez v2, :cond_0

    .line 1806046
    const-string v2, "extra_hours_data"

    iget-object v3, v0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806047
    iget-object v4, v3, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object v3, v4

    .line 1806048
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806049
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806050
    return-void
.end method
