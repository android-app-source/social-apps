.class public final enum LX/BvC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BvC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BvC;

.field public static final enum MODE_EXIT:LX/BvC;

.field public static final enum MODE_INVITE:LX/BvC;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1832007
    new-instance v0, LX/BvC;

    const-string v1, "MODE_INVITE"

    invoke-direct {v0, v1, v2}, LX/BvC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BvC;->MODE_INVITE:LX/BvC;

    .line 1832008
    new-instance v0, LX/BvC;

    const-string v1, "MODE_EXIT"

    invoke-direct {v0, v1, v3}, LX/BvC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BvC;->MODE_EXIT:LX/BvC;

    .line 1832009
    const/4 v0, 0x2

    new-array v0, v0, [LX/BvC;

    sget-object v1, LX/BvC;->MODE_INVITE:LX/BvC;

    aput-object v1, v0, v2

    sget-object v1, LX/BvC;->MODE_EXIT:LX/BvC;

    aput-object v1, v0, v3

    sput-object v0, LX/BvC;->$VALUES:[LX/BvC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1832010
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BvC;
    .locals 1

    .prologue
    .line 1832011
    const-class v0, LX/BvC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BvC;

    return-object v0
.end method

.method public static values()[LX/BvC;
    .locals 1

    .prologue
    .line 1832012
    sget-object v0, LX/BvC;->$VALUES:[LX/BvC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BvC;

    return-object v0
.end method
