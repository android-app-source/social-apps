.class public LX/AvB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/AvB;


# instance fields
.field private final a:LX/1kR;

.field private final b:LX/0SG;

.field private final c:LX/0tS;

.field private final d:LX/0tX;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0fO;

.field private final g:Ljava/util/concurrent/Executor;

.field private final h:LX/AvF;

.field public final i:LX/ArS;

.field private final j:LX/0tR;


# direct methods
.method public constructor <init>(LX/1kR;LX/0SG;LX/0tS;LX/0fO;LX/0tX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/Executor;LX/AvF;LX/ArS;LX/0tR;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1723855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723856
    iput-object p1, p0, LX/AvB;->a:LX/1kR;

    .line 1723857
    iput-object p2, p0, LX/AvB;->b:LX/0SG;

    .line 1723858
    iput-object p3, p0, LX/AvB;->c:LX/0tS;

    .line 1723859
    iput-object p4, p0, LX/AvB;->f:LX/0fO;

    .line 1723860
    iput-object p5, p0, LX/AvB;->d:LX/0tX;

    .line 1723861
    iput-object p6, p0, LX/AvB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1723862
    iput-object p7, p0, LX/AvB;->g:Ljava/util/concurrent/Executor;

    .line 1723863
    iput-object p8, p0, LX/AvB;->h:LX/AvF;

    .line 1723864
    iput-object p9, p0, LX/AvB;->i:LX/ArS;

    .line 1723865
    iput-object p10, p0, LX/AvB;->j:LX/0tR;

    .line 1723866
    return-void
.end method

.method private a(JLX/3Aj;)LX/0zO;
    .locals 3
    .param p3    # LX/3Aj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/3Aj;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1723822
    iget-object v0, p0, LX/AvB;->a:LX/1kR;

    invoke-virtual {v0, p1, p2, p3}, LX/1kR;->a(JLX/3Aj;)LX/0gW;

    move-result-object v0

    .line 1723823
    const-string v1, "count"

    iget-object v2, p0, LX/AvB;->f:LX/0fO;

    .line 1723824
    invoke-static {v2}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object p1

    invoke-virtual {p1}, LX/0i8;->e()I

    move-result p1

    move v2, p1

    .line 1723825
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1723826
    iget-object v1, p0, LX/AvB;->j:LX/0tR;

    invoke-virtual {v1, v0}, LX/0tR;->a(LX/0gW;)V

    .line 1723827
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/AvB;
    .locals 14

    .prologue
    .line 1723842
    sget-object v0, LX/AvB;->k:LX/AvB;

    if-nez v0, :cond_1

    .line 1723843
    const-class v1, LX/AvB;

    monitor-enter v1

    .line 1723844
    :try_start_0
    sget-object v0, LX/AvB;->k:LX/AvB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1723845
    if-eqz v2, :cond_0

    .line 1723846
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1723847
    new-instance v3, LX/AvB;

    invoke-static {v0}, LX/1kR;->b(LX/0QB;)LX/1kR;

    move-result-object v4

    check-cast v4, LX/1kR;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v6

    check-cast v6, LX/0tS;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v7

    check-cast v7, LX/0fO;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/AvF;->a(LX/0QB;)LX/AvF;

    move-result-object v11

    check-cast v11, LX/AvF;

    invoke-static {v0}, LX/ArS;->b(LX/0QB;)LX/ArS;

    move-result-object v12

    check-cast v12, LX/ArS;

    invoke-static {v0}, LX/0tR;->b(LX/0QB;)LX/0tR;

    move-result-object v13

    check-cast v13, LX/0tR;

    invoke-direct/range {v3 .. v13}, LX/AvB;-><init>(LX/1kR;LX/0SG;LX/0tS;LX/0fO;LX/0tX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/Executor;LX/AvF;LX/ArS;LX/0tR;)V

    .line 1723848
    move-object v0, v3

    .line 1723849
    sput-object v0, LX/AvB;->k:LX/AvB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1723850
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1723851
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1723852
    :cond_1
    sget-object v0, LX/AvB;->k:LX/AvB;

    return-object v0

    .line 1723853
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1723854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)J
    .locals 3

    .prologue
    .line 1723841
    iget-object v0, p0, LX/AvB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/AwE;->h:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/AvA;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AvA;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1723828
    invoke-virtual {p1}, LX/AvA;->getMaxCacheAge()J

    move-result-wide v6

    .line 1723829
    invoke-virtual {p1}, LX/AvA;->getKey()I

    move-result v5

    .line 1723830
    iget-object v0, p0, LX/AvB;->i:LX/ArS;

    .line 1723831
    const v1, 0xb60001

    invoke-static {v0, v1, v5}, LX/ArS;->a(LX/ArS;II)V

    .line 1723832
    const v1, 0xb60004

    invoke-static {v0, v1, v5}, LX/ArS;->a(LX/ArS;II)V

    .line 1723833
    iget-object v0, p0, LX/AvB;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1723834
    invoke-virtual {p0, v0, v1}, LX/AvB;->a(J)J

    move-result-wide v2

    .line 1723835
    sub-long v8, v0, v2

    cmp-long v4, v8, v6

    if-lez v4, :cond_0

    const/4 v4, 0x1

    .line 1723836
    :goto_0
    if-eqz v4, :cond_1

    .line 1723837
    :goto_1
    iget-object v2, p0, LX/AvB;->h:LX/AvF;

    invoke-virtual {v2, v4}, LX/AvF;->a(Z)LX/3Aj;

    move-result-object v2

    .line 1723838
    iget-object v3, p0, LX/AvB;->d:LX/0tX;

    invoke-direct {p0, v0, v1, v2}, LX/AvB;->a(JLX/3Aj;)LX/0zO;

    move-result-object v2

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/Av9;

    invoke-direct {v3, p0, v5, v0, v1}, LX/Av9;-><init>(LX/AvB;IJ)V

    iget-object v0, p0, LX/AvB;->g:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1723839
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 1723840
    goto :goto_1
.end method
