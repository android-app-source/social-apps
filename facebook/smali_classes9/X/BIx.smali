.class public final LX/BIx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yZ;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 0

    .prologue
    .line 1771119
    iput-object p1, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7ye;)V
    .locals 10

    .prologue
    .line 1771077
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v0

    .line 1771078
    iget-wide v8, v0, LX/74w;->a:J

    move-wide v0, v8

    .line 1771079
    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    iget-object v1, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/75F;->b(LX/74x;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1771080
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771081
    :cond_0
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getPhotoViewsInPager()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    .line 1771082
    iget-object v1, v0, LX/9iQ;->b:LX/74w;

    move-object v1, v1

    .line 1771083
    check-cast v1, LX/74x;

    .line 1771084
    iget-wide v8, v1, LX/74w;->a:J

    move-wide v4, v8

    .line 1771085
    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    move-object v1, v0

    .line 1771086
    check-cast v1, LX/9ip;

    invoke-virtual {v1}, LX/9ip;->n()V

    .line 1771087
    check-cast v0, LX/9ip;

    .line 1771088
    const/4 v1, 0x0

    move v1, v1

    .line 1771089
    if-nez v1, :cond_2

    iget-object v1, v0, LX/9ip;->w:LX/4n9;

    if-nez v1, :cond_4

    .line 1771090
    :cond_2
    :goto_1
    goto :goto_0

    .line 1771091
    :cond_3
    return-void

    .line 1771092
    :cond_4
    iget-object v1, v0, LX/9ip;->w:LX/4n9;

    invoke-virtual {v0, v1}, LX/9ip;->setFetchParams(LX/4n9;)V

    goto :goto_1
.end method

.method public final b(LX/7ye;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1771093
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->N:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v0

    .line 1771094
    iget-wide v8, v0, LX/74w;->a:J

    move-wide v0, v8

    .line 1771095
    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1771096
    invoke-virtual {p1}, LX/7ye;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1771097
    iget-object v2, v0, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1771098
    iget-object v3, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v3, v3, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->N:Ljava/lang/String;

    if-ne v2, v3, :cond_0

    .line 1771099
    iget-object v1, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    .line 1771100
    iget-object v2, v1, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v2, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setTagSuggestions(Ljava/util/List;)V

    .line 1771101
    :cond_1
    return-void

    .line 1771102
    :cond_2
    iget-object v1, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1771103
    iget-object v0, p1, LX/7ye;->d:Ljava/util/List;

    move-object v0, v0

    .line 1771104
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    if-eqz v0, :cond_6

    :cond_3
    const/4 v0, 0x1

    .line 1771105
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    .line 1771106
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a:LX/9iZ;

    iget-object v1, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v3}, LX/9iZ;->a(Landroid/content/Context;LX/7ye;LX/0Px;)V

    .line 1771107
    iget-object v0, p0, LX/BIx;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getPhotoViewsInPager()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    .line 1771108
    iget-object v1, v0, LX/9iQ;->b:LX/74w;

    move-object v1, v1

    .line 1771109
    check-cast v1, LX/74x;

    .line 1771110
    iget-wide v8, v1, LX/74w;->a:J

    move-wide v4, v8

    .line 1771111
    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_4

    move-object v1, v0

    .line 1771112
    check-cast v1, LX/9ip;

    .line 1771113
    iput-boolean v2, v1, LX/9ip;->o:Z

    .line 1771114
    iget-object v1, p1, LX/7ye;->d:Ljava/util/List;

    move-object v1, v1

    .line 1771115
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    move-object v1, v0

    .line 1771116
    check-cast v1, LX/9ip;

    invoke-virtual {v1}, LX/9ip;->h()V

    .line 1771117
    :cond_5
    check-cast v0, LX/9ip;

    invoke-virtual {v0}, LX/9ip;->m()V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 1771118
    goto :goto_0
.end method
