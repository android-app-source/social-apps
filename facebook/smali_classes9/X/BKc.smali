.class public final enum LX/BKc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BKc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BKc;

.field public static final enum ALBUM:LX/BKc;

.field public static final enum BIG_POST:LX/BKc;

.field public static final enum MINUTIAE:LX/BKc;

.field public static final enum PEOPLE_TAGGING:LX/BKc;

.field public static final enum PHOTO:LX/BKc;

.field public static final enum PLACE_TAGGING:LX/BKc;

.field public static final enum POST:LX/BKc;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1773970
    new-instance v0, LX/BKc;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->PHOTO:LX/BKc;

    .line 1773971
    new-instance v0, LX/BKc;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v4}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->ALBUM:LX/BKc;

    .line 1773972
    new-instance v0, LX/BKc;

    const-string v1, "MINUTIAE"

    invoke-direct {v0, v1, v5}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->MINUTIAE:LX/BKc;

    .line 1773973
    new-instance v0, LX/BKc;

    const-string v1, "PEOPLE_TAGGING"

    invoke-direct {v0, v1, v6}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->PEOPLE_TAGGING:LX/BKc;

    .line 1773974
    new-instance v0, LX/BKc;

    const-string v1, "PLACE_TAGGING"

    invoke-direct {v0, v1, v7}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->PLACE_TAGGING:LX/BKc;

    .line 1773975
    new-instance v0, LX/BKc;

    const-string v1, "POST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->POST:LX/BKc;

    .line 1773976
    new-instance v0, LX/BKc;

    const-string v1, "BIG_POST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKc;->BIG_POST:LX/BKc;

    .line 1773977
    const/4 v0, 0x7

    new-array v0, v0, [LX/BKc;

    sget-object v1, LX/BKc;->PHOTO:LX/BKc;

    aput-object v1, v0, v3

    sget-object v1, LX/BKc;->ALBUM:LX/BKc;

    aput-object v1, v0, v4

    sget-object v1, LX/BKc;->MINUTIAE:LX/BKc;

    aput-object v1, v0, v5

    sget-object v1, LX/BKc;->PEOPLE_TAGGING:LX/BKc;

    aput-object v1, v0, v6

    sget-object v1, LX/BKc;->PLACE_TAGGING:LX/BKc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BKc;->POST:LX/BKc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BKc;->BIG_POST:LX/BKc;

    aput-object v2, v0, v1

    sput-object v0, LX/BKc;->$VALUES:[LX/BKc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1773978
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BKc;
    .locals 1

    .prologue
    .line 1773979
    const-class v0, LX/BKc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BKc;

    return-object v0
.end method

.method public static values()[LX/BKc;
    .locals 1

    .prologue
    .line 1773980
    sget-object v0, LX/BKc;->$VALUES:[LX/BKc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BKc;

    return-object v0
.end method
