.class public final LX/Axq;
.super LX/1CO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1CO",
        "<",
        "LX/0bI;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Axq;


# instance fields
.field public a:Z

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728918
    invoke-direct {p0}, LX/1CO;-><init>()V

    .line 1728919
    iput-object p1, p0, LX/Axq;->b:LX/0Ot;

    .line 1728920
    return-void
.end method

.method public static a(LX/0QB;)LX/Axq;
    .locals 4

    .prologue
    .line 1728905
    sget-object v0, LX/Axq;->c:LX/Axq;

    if-nez v0, :cond_1

    .line 1728906
    const-class v1, LX/Axq;

    monitor-enter v1

    .line 1728907
    :try_start_0
    sget-object v0, LX/Axq;->c:LX/Axq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1728908
    if-eqz v2, :cond_0

    .line 1728909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1728910
    new-instance v3, LX/Axq;

    const/16 p0, 0x22c0

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Axq;-><init>(LX/0Ot;)V

    .line 1728911
    move-object v0, v3

    .line 1728912
    sput-object v0, LX/Axq;->c:LX/Axq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1728914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1728915
    :cond_1
    sget-object v0, LX/Axq;->c:LX/Axq;

    return-object v0

    .line 1728916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1728917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1728904
    const-class v0, LX/0bI;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 1728878
    const/4 v2, 0x0

    .line 1728879
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    iget-object v0, v0, LX/Axt;->d:LX/0ad;

    sget-short v1, LX/1kO;->p:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1728880
    iget-boolean v0, p0, LX/Axq;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 1728881
    iget-object v3, v0, LX/Axt;->d:LX/0ad;

    sget v4, LX/1kO;->O:F

    const/high16 v5, 0x41700000    # 15.0f

    invoke-interface {v3, v4, v5}, LX/0ad;->a(FF)F

    move-result v3

    .line 1728882
    iget-object v4, v0, LX/Axt;->q:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, v0, LX/Axt;->v:J

    sub-long/2addr v5, v7

    long-to-float v4, v5

    const v5, 0x476a6000    # 60000.0f

    mul-float/2addr v3, v5

    cmpl-float v3, v4, v3

    if-lez v3, :cond_5

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 1728883
    if-eqz v0, :cond_1

    .line 1728884
    :cond_0
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    invoke-static {v0}, LX/Axt;->k(LX/Axt;)V

    .line 1728885
    iput-boolean v2, p0, LX/Axq;->a:Z

    .line 1728886
    :cond_1
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 1728887
    iget-object v3, v0, LX/Axt;->d:LX/0ad;

    sget v4, LX/1kO;->v:F

    const/high16 v5, 0x42700000    # 60.0f

    invoke-interface {v3, v4, v5}, LX/0ad;->a(FF)F

    move-result v3

    .line 1728888
    iget-object v4, v0, LX/Axt;->q:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, v0, LX/Axt;->w:J

    sub-long/2addr v5, v7

    long-to-float v4, v5

    const v5, 0x476a6000    # 60000.0f

    mul-float/2addr v3, v5

    cmpl-float v3, v4, v3

    if-lez v3, :cond_6

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 1728889
    if-eqz v0, :cond_2

    .line 1728890
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 1728891
    iget-object v1, v0, LX/Axt;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1728892
    iget-object v1, v0, LX/Axt;->k:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, v0, LX/Axt;->t:Ljava/lang/Runnable;

    const v3, -0x55d1f30b

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1728893
    :cond_2
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 1728894
    iget-object v3, v0, LX/Axt;->d:LX/0ad;

    sget v4, LX/1kO;->w:F

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-interface {v3, v4, v5}, LX/0ad;->a(FF)F

    move-result v3

    .line 1728895
    iget-object v4, v0, LX/Axt;->q:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, v0, LX/Axt;->x:J

    sub-long/2addr v5, v7

    long-to-float v4, v5

    const v5, 0x476a6000    # 60000.0f

    mul-float/2addr v3, v5

    cmpl-float v3, v4, v3

    if-lez v3, :cond_7

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 1728896
    if-eqz v0, :cond_3

    .line 1728897
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    .line 1728898
    iget-object v1, v0, LX/Axt;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1728899
    iget-object v1, v0, LX/Axt;->k:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, v0, LX/Axt;->u:Ljava/lang/Runnable;

    const v3, 0x412f3067

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1728900
    :cond_3
    :goto_3
    return-void

    .line 1728901
    :cond_4
    iget-boolean v0, p0, LX/Axq;->a:Z

    if-eqz v0, :cond_3

    .line 1728902
    iget-object v0, p0, LX/Axq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    invoke-static {v0}, LX/Axt;->k(LX/Axt;)V

    .line 1728903
    iput-boolean v2, p0, LX/Axq;->a:Z

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_2
.end method
