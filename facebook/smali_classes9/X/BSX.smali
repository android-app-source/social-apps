.class public final enum LX/BSX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSX;

.field public static final enum FULLSCREEN_TO_INLINE:LX/BSX;

.field public static final enum HIDE_AD:LX/BSX;

.field public static final enum INLINE_SCROLLED_AWAY:LX/BSX;

.field public static final enum INLINE_SCROLLED_INTO:LX/BSX;

.field public static final enum INLINE_TO_FULLSCREEN:LX/BSX;

.field public static final enum PAUSE_VIDEO_AD:LX/BSX;

.field public static final enum PLAY_VIDEO_AD:LX/BSX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1785543
    new-instance v0, LX/BSX;

    const-string v1, "HIDE_AD"

    invoke-direct {v0, v1, v3}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->HIDE_AD:LX/BSX;

    .line 1785544
    new-instance v0, LX/BSX;

    const-string v1, "INLINE_SCROLLED_INTO"

    invoke-direct {v0, v1, v4}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->INLINE_SCROLLED_INTO:LX/BSX;

    .line 1785545
    new-instance v0, LX/BSX;

    const-string v1, "INLINE_SCROLLED_AWAY"

    invoke-direct {v0, v1, v5}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->INLINE_SCROLLED_AWAY:LX/BSX;

    .line 1785546
    new-instance v0, LX/BSX;

    const-string v1, "INLINE_TO_FULLSCREEN"

    invoke-direct {v0, v1, v6}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->INLINE_TO_FULLSCREEN:LX/BSX;

    .line 1785547
    new-instance v0, LX/BSX;

    const-string v1, "FULLSCREEN_TO_INLINE"

    invoke-direct {v0, v1, v7}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->FULLSCREEN_TO_INLINE:LX/BSX;

    .line 1785548
    new-instance v0, LX/BSX;

    const-string v1, "PAUSE_VIDEO_AD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->PAUSE_VIDEO_AD:LX/BSX;

    .line 1785549
    new-instance v0, LX/BSX;

    const-string v1, "PLAY_VIDEO_AD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BSX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSX;->PLAY_VIDEO_AD:LX/BSX;

    .line 1785550
    const/4 v0, 0x7

    new-array v0, v0, [LX/BSX;

    sget-object v1, LX/BSX;->HIDE_AD:LX/BSX;

    aput-object v1, v0, v3

    sget-object v1, LX/BSX;->INLINE_SCROLLED_INTO:LX/BSX;

    aput-object v1, v0, v4

    sget-object v1, LX/BSX;->INLINE_SCROLLED_AWAY:LX/BSX;

    aput-object v1, v0, v5

    sget-object v1, LX/BSX;->INLINE_TO_FULLSCREEN:LX/BSX;

    aput-object v1, v0, v6

    sget-object v1, LX/BSX;->FULLSCREEN_TO_INLINE:LX/BSX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BSX;->PAUSE_VIDEO_AD:LX/BSX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BSX;->PLAY_VIDEO_AD:LX/BSX;

    aput-object v2, v0, v1

    sput-object v0, LX/BSX;->$VALUES:[LX/BSX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1785551
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSX;
    .locals 1

    .prologue
    .line 1785542
    const-class v0, LX/BSX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSX;

    return-object v0
.end method

.method public static values()[LX/BSX;
    .locals 1

    .prologue
    .line 1785541
    sget-object v0, LX/BSX;->$VALUES:[LX/BSX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSX;

    return-object v0
.end method
