.class public LX/BwY;
.super LX/3Gn;
.source ""


# instance fields
.field public o:LX/19w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/16U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/D7Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/15V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/15X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/BUA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final v:LX/BwX;

.field public final w:Landroid/os/Handler;

.field public x:LX/2fs;

.field public y:LX/2pa;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1833922
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BwY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1833923
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833946
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BwY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833947
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 1833941
    invoke-direct {p0, p1, p2, p3}, LX/3Gn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833942
    new-instance v0, LX/BwX;

    invoke-direct {v0, p0}, LX/BwX;-><init>(LX/BwY;)V

    iput-object v0, p0, LX/BwY;->v:LX/BwX;

    .line 1833943
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/BwY;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v3

    check-cast v3, LX/19w;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v4

    check-cast v4, LX/16U;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    const-class p1, LX/D7Y;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/D7Y;

    invoke-static {v0}, LX/15V;->a(LX/0QB;)LX/15V;

    move-result-object p2

    check-cast p2, LX/15V;

    invoke-static {v0}, LX/15X;->a(LX/0QB;)LX/15X;

    move-result-object p3

    check-cast p3, LX/15X;

    invoke-static {v0}, LX/BUA;->a(LX/0QB;)LX/BUA;

    move-result-object v0

    check-cast v0, LX/BUA;

    iput-object v3, v2, LX/BwY;->o:LX/19w;

    iput-object v4, v2, LX/BwY;->p:LX/16U;

    iput-object v5, v2, LX/BwY;->q:LX/0wM;

    iput-object p1, v2, LX/BwY;->r:LX/D7Y;

    iput-object p2, v2, LX/BwY;->s:LX/15V;

    iput-object p3, v2, LX/BwY;->t:LX/15X;

    iput-object v0, v2, LX/BwY;->u:LX/BUA;

    .line 1833944
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/BwY;->w:Landroid/os/Handler;

    .line 1833945
    return-void
.end method

.method public static k(LX/BwY;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 1833924
    invoke-virtual {p0}, LX/3Gn;->j()V

    .line 1833925
    const/4 v0, 0x0

    iput-object v0, p0, LX/BwY;->e:Ljava/lang/Boolean;

    .line 1833926
    sget-object v0, LX/BwW;->a:[I

    iget-object v1, p0, LX/BwY;->x:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1833927
    :goto_0
    return-void

    .line 1833928
    :pswitch_0
    invoke-virtual {p0, v3}, LX/3Gn;->a(Z)V

    .line 1833929
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080dac

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0

    .line 1833930
    :pswitch_1
    invoke-virtual {p0, v5}, LX/3Gn;->a(Z)V

    .line 1833931
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/BwY;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1833932
    iget-object v2, p0, LX/BwY;->s:LX/15V;

    invoke-virtual {v2}, LX/15V;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1833933
    iget-object v2, p0, LX/BwY;->t:LX/15X;

    invoke-virtual {v2, v1}, LX/15X;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1833934
    :goto_1
    move-object v1, v2

    .line 1833935
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1833936
    :pswitch_2
    iget-object v0, p0, LX/BwY;->x:LX/2fs;

    iget-object v0, v0, LX/2fs;->d:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    if-ne v0, v1, :cond_0

    .line 1833937
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/BwY;->s:LX/15V;

    iget-object v2, p0, LX/BwY;->x:LX/2fs;

    invoke-virtual {v1, v2}, LX/15V;->b(LX/2fs;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833938
    invoke-virtual {p0, v3}, LX/3Gn;->a(Z)V

    goto :goto_0

    .line 1833939
    :cond_0
    :pswitch_3
    invoke-virtual {p0, v3, v5}, LX/3Gn;->a(ZZ)V

    .line 1833940
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/BwY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080dd1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/BwY;->x:LX/2fs;

    invoke-static {v4}, LX/15V;->c(LX/2fs;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const v2, 0x7f080db0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setButtonImage(I)V
    .locals 3

    .prologue
    .line 1833920
    iget-object v0, p0, LX/3Gn;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/BwY;->q:LX/0wM;

    const/4 v2, -0x1

    invoke-virtual {v1, p1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1833921
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1833948
    invoke-static {p1}, LX/393;->o(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BwY;->o:LX/19w;

    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/393;->n(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1833949
    :cond_0
    iput-boolean v4, p0, LX/BwY;->f:Z

    .line 1833950
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1833951
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1833952
    if-eqz v0, :cond_1

    .line 1833953
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1833954
    :cond_1
    :goto_0
    return-void

    .line 1833955
    :cond_2
    iput-boolean v5, p0, LX/BwY;->f:Z

    .line 1833956
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1833957
    iput-object p1, p0, LX/BwY;->y:LX/2pa;

    .line 1833958
    iget-object v0, p0, LX/BwY;->o:LX/19w;

    invoke-static {p1}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    iput-object v0, p0, LX/BwY;->x:LX/2fs;

    .line 1833959
    iget-object v1, p0, LX/BwY;->r:LX/D7Y;

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833960
    iget-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v2

    .line 1833961
    :goto_1
    iget-object v2, p0, LX/BwY;->y:LX/2pa;

    invoke-static {v2}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BwY;->y:LX/2pa;

    invoke-static {v3}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3, v5}, LX/D7Y;->a(LX/04D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Z)LX/D7X;

    move-result-object v0

    .line 1833962
    iget-object v1, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833963
    invoke-virtual {v0}, LX/D7X;->a()V

    .line 1833964
    if-eqz p2, :cond_3

    .line 1833965
    iget-object v0, p0, LX/3Gn;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1833966
    iget-object v0, p0, LX/3Gn;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1833967
    iget-object v0, p0, LX/BwY;->p:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/BwY;->v:LX/BwX;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 1833968
    :cond_3
    invoke-static {p0}, LX/BwY;->k(LX/BwY;)V

    goto :goto_0

    .line 1833969
    :cond_4
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_1
.end method

.method public final b(Z)I
    .locals 1

    .prologue
    .line 1833919
    const/4 v0, -0x1

    return v0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1833903
    sget-object v0, LX/BwW;->a:[I

    iget-object v1, p0, LX/BwY;->x:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1833904
    :goto_0
    return-void

    .line 1833905
    :pswitch_0
    const v0, 0x7f0207d6

    invoke-direct {p0, v0}, LX/BwY;->setButtonImage(I)V

    goto :goto_0

    .line 1833906
    :pswitch_1
    const v0, 0x7f020841

    invoke-direct {p0, v0}, LX/BwY;->setButtonImage(I)V

    goto :goto_0

    .line 1833907
    :pswitch_2
    iget-object v0, p0, LX/BwY;->x:LX/2fs;

    iget-object v0, v0, LX/2fs;->d:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    if-ne v0, v1, :cond_0

    .line 1833908
    const v0, 0x7f020841

    invoke-direct {p0, v0}, LX/BwY;->setButtonImage(I)V

    goto :goto_0

    .line 1833909
    :cond_0
    :pswitch_3
    const v0, 0x7f020818

    invoke-direct {p0, v0}, LX/BwY;->setButtonImage(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1833917
    iget-object v0, p0, LX/BwY;->p:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/BwY;->v:LX/BwX;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 1833918
    return-void
.end method

.method public final dI_()V
    .locals 3

    .prologue
    .line 1833912
    iget-object v1, p0, LX/BwY;->u:LX/BUA;

    iget-object v0, p0, LX/BwY;->y:LX/2pa;

    invoke-static {v0}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v0, p0, LX/3Gn;->f:Z

    if-eqz v0, :cond_0

    .line 1833913
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1833914
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/BUA;->a(Ljava/lang/String;Z)V

    .line 1833915
    return-void

    .line 1833916
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 1

    .prologue
    .line 1833910
    const v0, 0x7f020841

    invoke-direct {p0, v0}, LX/BwY;->setButtonImage(I)V

    .line 1833911
    return-void
.end method
