.class public final LX/ATe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676248
    iput-object p1, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7d9a3f4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1676249
    iget-object v0, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v2, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0, v2}, LX/ATL;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1676250
    iget-object v0, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1676251
    iget-object v2, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1676252
    iget-object v0, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c3;

    iget-object v2, p0, LX/ATe;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    sget-object v3, LX/9c2;->ATTACHMENT_REMOVED:LX/9c2;

    invoke-virtual {v0, v2, v3}, LX/9c3;->a(Ljava/lang/String;LX/9c2;)V

    .line 1676253
    const v0, -0x1d5726de

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
