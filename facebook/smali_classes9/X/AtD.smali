.class public final LX/AtD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ar8;


# instance fields
.field public final synthetic a:LX/AtM;


# direct methods
.method public constructor <init>(LX/AtM;)V
    .locals 0

    .prologue
    .line 1721221
    iput-object p1, p0, LX/AtD;->a:LX/AtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1721215
    iget-object v0, p0, LX/AtD;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->e:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_GALLERY_BUTTON:LX/ArJ;

    .line 1721216
    iget-object v3, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setGallerySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    iget-object v4, v0, LX/ArT;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setGallerySessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v0, v3}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1721217
    iget-object v3, v0, LX/ArT;->b:LX/ArL;

    .line 1721218
    sget-object v4, LX/ArH;->START_GALLERY_SESSION:LX/ArH;

    invoke-static {v3, v4, v1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v5}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721219
    iget-object v0, p0, LX/AtD;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/AtM;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/AtD;->a:LX/AtM;

    iget-object v1, v1, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setShouldRefreshCameraRoll(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721220
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1721222
    iget-object v0, p0, LX/AtD;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    sget-object v1, LX/AtM;->a:LX/0jK;

    sget-object v2, LX/86o;->CAMERA_ROLL:LX/86o;

    invoke-static {v0, v1, v2}, LX/87N;->b(LX/0il;LX/0jK;LX/86o;)V

    .line 1721223
    return-void
.end method
