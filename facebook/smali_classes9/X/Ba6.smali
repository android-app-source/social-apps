.class public final LX/Ba6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ba8;


# direct methods
.method public constructor <init>(LX/Ba8;)V
    .locals 0

    .prologue
    .line 1798482
    iput-object p1, p0, LX/Ba6;->a:LX/Ba8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1798483
    iget-object v0, p0, LX/Ba6;->a:LX/Ba8;

    iget-object v0, v0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 1798484
    iget v1, v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v0, v1

    .line 1798485
    if-gtz v0, :cond_0

    .line 1798486
    iget-object v0, p0, LX/Ba6;->a:LX/Ba8;

    iget-object v0, v0, LX/Ba8;->c:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Ba8;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onPositiveButtonClicked"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Submit should only be clickable after a rating has been selected."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1798487
    :goto_0
    return-void

    .line 1798488
    :cond_0
    iget-object v1, p0, LX/Ba6;->a:LX/Ba8;

    .line 1798489
    iget-object v2, v1, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v2, v2

    .line 1798490
    iget-object p0, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p0, p0

    .line 1798491
    const-string p1, "rating"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1798492
    iget-object v2, v1, LX/Ba8;->b:LX/29r;

    invoke-virtual {v2}, LX/29r;->d()Lcom/facebook/appirater/api/FetchISRConfigResult;

    move-result-object v2

    .line 1798493
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result p0

    if-nez p0, :cond_2

    .line 1798494
    :cond_1
    iget-object v2, v1, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v2, v2

    .line 1798495
    sget-object p0, LX/BZw;->THANKS_FOR_FEEDBACK:LX/BZw;

    invoke-virtual {v2, p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->a(LX/BZw;)V

    .line 1798496
    :goto_1
    goto :goto_0

    .line 1798497
    :cond_2
    iget p0, v2, Lcom/facebook/appirater/api/FetchISRConfigResult;->maxStarsForFeedback:I

    if-gt v0, p0, :cond_3

    .line 1798498
    iget-object v2, v1, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v2, v2

    .line 1798499
    sget-object p0, LX/BZw;->PROVIDE_FEEDBACK:LX/BZw;

    invoke-virtual {v2, p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->a(LX/BZw;)V

    goto :goto_1

    .line 1798500
    :cond_3
    iget v2, v2, Lcom/facebook/appirater/api/FetchISRConfigResult;->minStarsForStore:I

    if-lt v0, v2, :cond_4

    .line 1798501
    iget-object v2, v1, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v2, v2

    .line 1798502
    sget-object p0, LX/BZw;->RATE_ON_PLAY_STORE:LX/BZw;

    invoke-virtual {v2, p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->a(LX/BZw;)V

    goto :goto_1

    .line 1798503
    :cond_4
    iget-object v2, v1, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v2, v2

    .line 1798504
    sget-object p0, LX/BZw;->THANKS_FOR_FEEDBACK:LX/BZw;

    invoke-virtual {v2, p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->a(LX/BZw;)V

    goto :goto_1
.end method
