.class public final LX/AZn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AMN;


# instance fields
.field public final synthetic a:LX/AZl;

.field public final synthetic b:I

.field public final synthetic c:LX/AZt;


# direct methods
.method public constructor <init>(LX/AZt;LX/AZl;I)V
    .locals 0

    .prologue
    .line 1687426
    iput-object p1, p0, LX/AZn;->c:LX/AZt;

    iput-object p2, p0, LX/AZn;->a:LX/AZl;

    iput p3, p0, LX/AZn;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/AN2;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;",
            "LX/AN2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1687427
    iget-object v0, p0, LX/AZn;->a:LX/AZl;

    .line 1687428
    iget-object v1, v0, LX/AZl;->g:LX/AZk;

    move-object v0, v1

    .line 1687429
    sget-object v1, LX/AZk;->DOWNLOADING:LX/AZk;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/AZn;->a:LX/AZl;

    .line 1687430
    iget-object v1, v0, LX/AZl;->k:LX/AN1;

    move-object v0, v1

    .line 1687431
    if-eqz v0, :cond_0

    .line 1687432
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget-object v0, v0, LX/AZt;->b:LX/AVT;

    iget-object v1, p0, LX/AZn;->c:LX/AZt;

    iget-object v1, v1, LX/AZt;->m:LX/AZm;

    invoke-virtual {v1}, LX/AZm;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "maskDownload"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AZn;->a:LX/AZl;

    .line 1687433
    iget-object v4, v3, LX/AZl;->k:LX/AN1;

    move-object v3, v4

    .line 1687434
    iget-object v4, v3, LX/AN1;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1687435
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/AZn;->a:LX/AZl;

    iget-object v3, v3, LX/AZl;->b:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/Aa6;->a(LX/AVT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687436
    :cond_0
    iget-object v1, p0, LX/AZn;->a:LX/AZl;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AN1;

    .line 1687437
    iput-object v0, v1, LX/AZl;->k:LX/AN1;

    .line 1687438
    iget-object v0, p0, LX/AZn;->a:LX/AZl;

    .line 1687439
    iput-object p2, v0, LX/AZl;->l:LX/AN2;

    .line 1687440
    iget-object v0, p0, LX/AZn;->a:LX/AZl;

    sget-object v1, LX/AZk;->DOWNLOADED:LX/AZk;

    .line 1687441
    iput-object v1, v0, LX/AZl;->g:LX/AZk;

    .line 1687442
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget-object v0, v0, LX/AZt;->j:LX/AZr;

    iget v1, p0, LX/AZn;->b:I

    invoke-virtual {v0, v1}, LX/1OM;->i_(I)V

    .line 1687443
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget v0, v0, LX/AZt;->n:I

    iget v1, p0, LX/AZn;->b:I

    if-ne v0, v1, :cond_1

    .line 1687444
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget-object v1, p0, LX/AZn;->a:LX/AZl;

    invoke-static {v0, v1}, LX/AZt;->a$redex0(LX/AZt;LX/AZl;)V

    .line 1687445
    :cond_1
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget v1, p0, LX/AZn;->b:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/AZt;->a$redex0(LX/AZt;IZ)V

    .line 1687446
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1687447
    iget-object v0, p0, LX/AZn;->c:LX/AZt;

    iget v1, p0, LX/AZn;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/AZt;->a$redex0(LX/AZt;IZ)V

    .line 1687448
    return-void
.end method
