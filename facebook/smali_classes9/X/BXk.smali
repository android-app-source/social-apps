.class public final enum LX/BXk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BXk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BXk;

.field public static final enum EXPAND:LX/BXk;

.field public static final enum INVISIBLE:LX/BXk;

.field public static final enum REORDER:LX/BXk;

.field public static final enum SHRINK:LX/BXk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1793665
    new-instance v0, LX/BXk;

    const-string v1, "INVISIBLE"

    invoke-direct {v0, v1, v2}, LX/BXk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BXk;->INVISIBLE:LX/BXk;

    .line 1793666
    new-instance v0, LX/BXk;

    const-string v1, "SHRINK"

    invoke-direct {v0, v1, v3}, LX/BXk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BXk;->SHRINK:LX/BXk;

    .line 1793667
    new-instance v0, LX/BXk;

    const-string v1, "REORDER"

    invoke-direct {v0, v1, v4}, LX/BXk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BXk;->REORDER:LX/BXk;

    .line 1793668
    new-instance v0, LX/BXk;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v5}, LX/BXk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BXk;->EXPAND:LX/BXk;

    .line 1793669
    const/4 v0, 0x4

    new-array v0, v0, [LX/BXk;

    sget-object v1, LX/BXk;->INVISIBLE:LX/BXk;

    aput-object v1, v0, v2

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    aput-object v1, v0, v3

    sget-object v1, LX/BXk;->REORDER:LX/BXk;

    aput-object v1, v0, v4

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    aput-object v1, v0, v5

    sput-object v0, LX/BXk;->$VALUES:[LX/BXk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1793670
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BXk;
    .locals 1

    .prologue
    .line 1793671
    const-class v0, LX/BXk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BXk;

    return-object v0
.end method

.method public static values()[LX/BXk;
    .locals 1

    .prologue
    .line 1793672
    sget-object v0, LX/BXk;->$VALUES:[LX/BXk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BXk;

    return-object v0
.end method
