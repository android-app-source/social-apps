.class public final LX/CaL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917426
    iput-object p1, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1917440
    iget-object v0, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->A:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->F:Ljava/lang/String;

    const-string v2, "Failed on GraphQLSubscription callback"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1917441
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1917427
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1917428
    if-eqz p1, :cond_0

    .line 1917429
    iget-object v1, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 1917430
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1917431
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1917432
    iput-object v0, v2, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1917433
    move-object v0, v2

    .line 1917434
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1917435
    iput-object v0, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1917436
    iget-object v0, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->o()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    move-result-object v0

    .line 1917437
    if-eqz v0, :cond_0

    .line 1917438
    iget-object v1, p0, LX/CaL;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->af:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1917439
    :cond_0
    return-void
.end method
