.class public LX/B79;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B79;


# instance fields
.field public final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1747739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747740
    new-instance v0, LX/0aq;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/B79;->a:LX/0aq;

    .line 1747741
    return-void
.end method

.method public static a(LX/0QB;)LX/B79;
    .locals 3

    .prologue
    .line 1747727
    sget-object v0, LX/B79;->b:LX/B79;

    if-nez v0, :cond_1

    .line 1747728
    const-class v1, LX/B79;

    monitor-enter v1

    .line 1747729
    :try_start_0
    sget-object v0, LX/B79;->b:LX/B79;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1747730
    if-eqz v2, :cond_0

    .line 1747731
    :try_start_1
    new-instance v0, LX/B79;

    invoke-direct {v0}, LX/B79;-><init>()V

    .line 1747732
    move-object v0, v0

    .line 1747733
    sput-object v0, LX/B79;->b:LX/B79;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1747734
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1747735
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1747736
    :cond_1
    sget-object v0, LX/B79;->b:LX/B79;

    return-object v0

    .line 1747737
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1747738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
