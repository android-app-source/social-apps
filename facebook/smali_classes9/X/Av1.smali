.class public LX/Av1;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

.field public b:Landroid/widget/ImageView;

.field public c:LX/Auq;

.field public d:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1723585
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1723586
    invoke-virtual {p0}, LX/Av1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1723587
    const v1, 0x7f030940

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1723588
    const v0, 0x7f0d17bd

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    iput-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    .line 1723589
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/Av1;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/Auz;

    invoke-direct {v3, p0}, LX/Auz;-><init>(LX/Av1;)V

    invoke-direct {v0, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/Av1;->d:Landroid/view/GestureDetector;

    .line 1723590
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    new-instance v2, LX/Av0;

    invoke-direct {v2, p0}, LX/Av0;-><init>(LX/Av1;)V

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1723591
    const v0, 0x7f0d17bc

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Av1;->b:Landroid/widget/ImageView;

    .line 1723592
    iget-object v0, p0, LX/Av1;->b:Landroid/widget/ImageView;

    new-instance v1, LX/Auy;

    invoke-direct {v1, p0}, LX/Auy;-><init>(LX/Av1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1723593
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1723594
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1723595
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setVisibility(I)V

    .line 1723596
    invoke-virtual {p0, v2}, LX/Av1;->setVisibility(I)V

    .line 1723597
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1723598
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a(Z)V

    .line 1723599
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1723600
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    if-nez v0, :cond_0

    .line 1723601
    const/4 v0, 0x0

    .line 1723602
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 1723603
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 1723604
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getTextSize()F

    move-result v0

    return v0
.end method

.method public setTextInputEnabled(Z)V
    .locals 1

    .prologue
    .line 1723605
    iget-object v0, p0, LX/Av1;->a:Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setEnabled(Z)V

    .line 1723606
    return-void
.end method
