.class public final synthetic LX/BUC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1788662
    invoke-static {}, LX/2qY;->values()[LX/2qY;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BUC;->b:[I

    :try_start_0
    sget-object v0, LX/BUC;->b:[I

    sget-object v1, LX/2qY;->SAVE_OFFLINE:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, LX/BUC;->b:[I

    sget-object v1, LX/2qY;->DOWNLOAD:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, LX/BUC;->b:[I

    sget-object v1, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    .line 1788663
    :goto_2
    invoke-static {}, LX/BTo;->values()[LX/BTo;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BUC;->a:[I

    :try_start_3
    sget-object v0, LX/BUC;->a:[I

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL:LX/BTo;

    invoke-virtual {v1}, LX/BTo;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, LX/BUC;->a:[I

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD:LX/BTo;

    invoke-virtual {v1}, LX/BTo;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, LX/BUC;->a:[I

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_DEVICE:LX/BTo;

    invoke-virtual {v1}, LX/BTo;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    goto :goto_5

    :catch_1
    goto :goto_4

    :catch_2
    goto :goto_3

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_0
.end method
