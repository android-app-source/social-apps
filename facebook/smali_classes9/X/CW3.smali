.class public final LX/CW3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "amount"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "amount"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1907253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1907254
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1907255
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1907256
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, LX/CW3;->a:LX/15i;

    iget v5, p0, LX/CW3;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x26f30b65

    invoke-static {v3, v5, v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1907257
    iget-object v3, p0, LX/CW3;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1907258
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1907259
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1907260
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1907261
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1907262
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1907263
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1907264
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1907265
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1907266
    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;-><init>(LX/15i;)V

    .line 1907267
    return-object v1

    .line 1907268
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
