.class public final LX/AZ6;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/AZ9;


# direct methods
.method public constructor <init>(LX/AZ9;)V
    .locals 4

    .prologue
    .line 1686506
    iput-object p1, p0, LX/AZ6;->a:LX/AZ9;

    .line 1686507
    const-wide/16 v0, 0x3a98

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1686508
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 2

    .prologue
    .line 1686501
    iget-object v0, p0, LX/AZ6;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686502
    iget-object v0, p0, LX/AZ6;->a:LX/AZ9;

    sget-object v1, LX/AZ8;->END_TRANSITION:LX/AZ8;

    .line 1686503
    iput-object v1, v0, LX/AZ9;->c:LX/AZ8;

    .line 1686504
    iget-object v0, p0, LX/AZ6;->a:LX/AZ9;

    invoke-static {v0}, LX/AZ9;->c$redex0(LX/AZ9;)V

    .line 1686505
    return-void
.end method

.method public final onTick(J)V
    .locals 5

    .prologue
    .line 1686496
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1686497
    iget-object v1, p0, LX/AZ6;->a:LX/AZ9;

    iget-object v1, v1, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1686498
    iget-object v1, p0, LX/AZ6;->a:LX/AZ9;

    iget-object v1, v1, LX/AZ9;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686499
    iget-object v0, p0, LX/AZ6;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->b:LX/3RX;

    const v1, 0x7f07002c

    iget-object v2, p0, LX/AZ6;->a:LX/AZ9;

    iget-object v2, v2, LX/AZ9;->a:LX/3RZ;

    invoke-virtual {v2}, LX/3RZ;->a()I

    move-result v2

    iget-object v3, p0, LX/AZ6;->a:LX/AZ9;

    iget v3, v3, LX/AZ9;->f:F

    invoke-virtual {v0, v1, v2, v3}, LX/3RX;->a(IIF)LX/7Cb;

    .line 1686500
    :cond_0
    return-void
.end method
