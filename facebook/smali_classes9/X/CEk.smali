.class public final enum LX/CEk;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/CEj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CEk;",
        ">;",
        "LX/CEj;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CEk;

.field public static final enum BURST_VIEW:LX/CEk;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1861752
    new-instance v0, LX/CEk;

    const-string v1, "BURST_VIEW"

    invoke-direct {v0, v1, v2}, LX/CEk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CEk;->BURST_VIEW:LX/CEk;

    .line 1861753
    const/4 v0, 0x1

    new-array v0, v0, [LX/CEk;

    sget-object v1, LX/CEk;->BURST_VIEW:LX/CEk;

    aput-object v1, v0, v2

    sput-object v0, LX/CEk;->$VALUES:[LX/CEk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1861754
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CEk;
    .locals 1

    .prologue
    .line 1861755
    const-class v0, LX/CEk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CEk;

    return-object v0
.end method

.method public static values()[LX/CEk;
    .locals 1

    .prologue
    .line 1861756
    sget-object v0, LX/CEk;->$VALUES:[LX/CEk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CEk;

    return-object v0
.end method
