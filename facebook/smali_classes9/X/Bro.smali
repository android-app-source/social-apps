.class public final LX/Bro;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Brp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Object;

.field public final synthetic b:LX/Brp;


# direct methods
.method public constructor <init>(LX/Brp;)V
    .locals 1

    .prologue
    .line 1826666
    iput-object p1, p0, LX/Bro;->b:LX/Brp;

    .line 1826667
    move-object v0, p1

    .line 1826668
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1826669
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1826670
    const-string v0, "UnknownFeedUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1826671
    if-ne p0, p1, :cond_1

    .line 1826672
    :cond_0
    :goto_0
    return v0

    .line 1826673
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1826674
    goto :goto_0

    .line 1826675
    :cond_3
    check-cast p1, LX/Bro;

    .line 1826676
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1826677
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1826678
    if-eq v2, v3, :cond_0

    .line 1826679
    iget-object v2, p0, LX/Bro;->a:Ljava/lang/Object;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Bro;->a:Ljava/lang/Object;

    iget-object v3, p1, LX/Bro;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1826680
    goto :goto_0

    .line 1826681
    :cond_4
    iget-object v2, p1, LX/Bro;->a:Ljava/lang/Object;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
