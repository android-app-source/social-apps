.class public final LX/BcJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcI;",
            ">;"
        }
    .end annotation
.end field

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1801728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801729
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    .line 1801730
    const/4 v0, 0x0

    iput v0, p0, LX/BcJ;->b:I

    .line 1801731
    return-void
.end method

.method public static a(LX/BcJ;LX/BcJ;)LX/BcJ;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1801755
    invoke-static {}, LX/BcJ;->c()LX/BcJ;

    move-result-object v3

    .line 1801756
    if-eqz p0, :cond_0

    iget v1, p0, LX/BcJ;->b:I

    move v2, v1

    .line 1801757
    :goto_0
    if-eqz p1, :cond_1

    iget v0, p1, LX/BcJ;->b:I

    move v1, v0

    .line 1801758
    :goto_1
    iget-object v4, v3, LX/BcJ;->a:Ljava/util/List;

    .line 1801759
    if-eqz p0, :cond_2

    .line 1801760
    iget-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcI;

    .line 1801761
    iget v6, v0, LX/BcI;->a:I

    iget v7, v0, LX/BcI;->b:I

    iget-object v8, v0, LX/BcI;->c:LX/1X1;

    iget-boolean p0, v0, LX/BcI;->d:Z

    invoke-static {v6, v7, v8, p0}, LX/BcI;->a(IILX/1X1;Z)LX/BcI;

    move-result-object v6

    move-object v0, v6

    .line 1801762
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_0
    move v2, v0

    .line 1801763
    goto :goto_0

    :cond_1
    move v1, v0

    .line 1801764
    goto :goto_1

    .line 1801765
    :cond_2
    if-eqz p1, :cond_3

    .line 1801766
    iget-object v0, p1, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcI;

    .line 1801767
    iget v6, v0, LX/BcI;->a:I

    iget v7, v0, LX/BcI;->b:I

    add-int/2addr v7, v2

    iget-object v8, v0, LX/BcI;->c:LX/1X1;

    iget-boolean p0, v0, LX/BcI;->d:Z

    invoke-static {v6, v7, v8, p0}, LX/BcI;->a(IILX/1X1;Z)LX/BcI;

    move-result-object v6

    move-object v0, v6

    .line 1801768
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1801769
    :cond_3
    add-int v0, v2, v1

    iput v0, v3, LX/BcJ;->b:I

    .line 1801770
    return-object v3
.end method

.method public static c()LX/BcJ;
    .locals 1

    .prologue
    .line 1801771
    const/4 v0, 0x0

    invoke-static {v0}, LX/BcJ;->c(I)LX/BcJ;

    move-result-object v0

    return-object v0
.end method

.method public static c(I)LX/BcJ;
    .locals 1

    .prologue
    .line 1801773
    new-instance v0, LX/BcJ;

    invoke-direct {v0}, LX/BcJ;-><init>()V

    move-object v0, v0

    .line 1801774
    iput p0, v0, LX/BcJ;->b:I

    .line 1801775
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1801772
    iget-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(ILX/1X1;)V
    .locals 1

    .prologue
    .line 1801750
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/BcJ;->a(ILX/1X1;Z)V

    .line 1801751
    return-void
.end method

.method public final a(ILX/1X1;Z)V
    .locals 1

    .prologue
    .line 1801752
    const/4 v0, 0x1

    invoke-static {v0, p1, p2, p3}, LX/BcI;->a(IILX/1X1;Z)LX/BcI;

    move-result-object v0

    move-object v0, v0

    .line 1801753
    invoke-virtual {p0, v0}, LX/BcJ;->a(LX/BcI;)V

    .line 1801754
    return-void
.end method

.method public final a(LX/BcI;)V
    .locals 2

    .prologue
    .line 1801745
    iget v0, p0, LX/BcJ;->b:I

    .line 1801746
    iget v1, p1, LX/BcI;->a:I

    move v1, v1

    .line 1801747
    add-int/2addr v0, v1

    iput v0, p0, LX/BcJ;->b:I

    .line 1801748
    iget-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1801749
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1801743
    invoke-static {p1}, LX/BcI;->a(I)LX/BcI;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BcJ;->a(LX/BcI;)V

    .line 1801744
    return-void
.end method

.method public final b(ILX/1X1;)V
    .locals 1

    .prologue
    .line 1801741
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/BcJ;->b(ILX/1X1;Z)V

    .line 1801742
    return-void
.end method

.method public final b(ILX/1X1;Z)V
    .locals 1

    .prologue
    .line 1801738
    const/4 v0, 0x0

    invoke-static {v0, p1, p2, p3}, LX/BcI;->a(IILX/1X1;Z)LX/BcI;

    move-result-object v0

    move-object v0, v0

    .line 1801739
    invoke-virtual {p0, v0}, LX/BcJ;->a(LX/BcI;)V

    .line 1801740
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1801732
    iget-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcI;

    .line 1801733
    const/4 v2, 0x0

    iput-object v2, v0, LX/BcI;->c:LX/1X1;

    .line 1801734
    goto :goto_0

    .line 1801735
    :cond_0
    iget-object v0, p0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1801736
    const/4 v0, 0x0

    iput v0, p0, LX/BcJ;->b:I

    .line 1801737
    return-void
.end method
