.class public LX/C3b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Locale;

.field private final b:Ljava/text/SimpleDateFormat;

.field private final c:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845979
    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/C3b;->a:Ljava/util/Locale;

    .line 1845980
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM"

    iget-object v2, p0, LX/C3b;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/C3b;->b:Ljava/text/SimpleDateFormat;

    .line 1845981
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    iget-object v2, p0, LX/C3b;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/C3b;->c:Ljava/text/SimpleDateFormat;

    .line 1845982
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/util/Date;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/util/Date;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1845961
    invoke-static {p0}, LX/C3b;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1845962
    const/4 v0, 0x0

    .line 1845963
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/Date;

    .line 1845964
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1845965
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/C3b;
    .locals 2

    .prologue
    .line 1845966
    new-instance v1, LX/C3b;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-direct {v1, v0}, LX/C3b;-><init>(LX/0W9;)V

    .line 1845967
    return-object v1
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1845968
    if-eqz p0, :cond_0

    .line 1845969
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1845970
    if-eqz v0, :cond_0

    .line 1845971
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1845972
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1845973
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1845974
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845975
    iget-object v0, p0, LX/C3b;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1845976
    iget-object v0, p0, LX/C3b;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1845977
    iget-object v1, p0, LX/C3b;->a:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
