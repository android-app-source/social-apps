.class public LX/C3N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/C3I;

.field public final d:LX/C39;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3l;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/C3I;LX/C39;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qb;",
            ">;",
            "LX/C3I;",
            "LX/C39;",
            "LX/0Ot",
            "<",
            "LX/C3l;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/C3i;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845557
    iput-object p1, p0, LX/C3N;->a:LX/0Ot;

    .line 1845558
    iput-object p2, p0, LX/C3N;->b:LX/0Ot;

    .line 1845559
    iput-object p3, p0, LX/C3N;->c:LX/C3I;

    .line 1845560
    iput-object p4, p0, LX/C3N;->d:LX/C39;

    .line 1845561
    iput-object p5, p0, LX/C3N;->e:LX/0Ot;

    .line 1845562
    iput-object p6, p0, LX/C3N;->f:LX/0Ot;

    .line 1845563
    return-void
.end method

.method public static a(LX/0QB;)LX/C3N;
    .locals 10

    .prologue
    .line 1845564
    const-class v1, LX/C3N;

    monitor-enter v1

    .line 1845565
    :try_start_0
    sget-object v0, LX/C3N;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845566
    sput-object v2, LX/C3N;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845567
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845568
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845569
    new-instance v3, LX/C3N;

    const/16 v4, 0x1ebc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x781

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/C3I;->a(LX/0QB;)LX/C3I;

    move-result-object v6

    check-cast v6, LX/C3I;

    invoke-static {v0}, LX/C39;->a(LX/0QB;)LX/C39;

    move-result-object v7

    check-cast v7, LX/C39;

    const/16 v8, 0x1ecf

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1ecd

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/C3N;-><init>(LX/0Ot;LX/0Ot;LX/C3I;LX/C39;LX/0Ot;LX/0Ot;)V

    .line 1845570
    move-object v0, v3

    .line 1845571
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845572
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845573
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/C33;)Z
    .locals 2

    .prologue
    .line 1845575
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845576
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1845577
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1845578
    if-eqz v0, :cond_0

    invoke-static {p0}, LX/C3N;->b(LX/C33;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/C33;)Z
    .locals 1

    .prologue
    .line 1845579
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845580
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1845581
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
