.class public final LX/Cbx;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1920537
    iput-object p1, p0, LX/Cbx;->b:LX/CcO;

    iput-object p2, p0, LX/Cbx;->a:Landroid/content/Context;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1920541
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1920542
    :goto_0
    return-void

    .line 1920543
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1920544
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-nez v1, :cond_1

    .line 1920545
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1920546
    :cond_1
    new-instance v1, LX/Cbw;

    invoke-direct {v1, p0}, LX/Cbw;-><init>(LX/Cbx;)V

    .line 1920547
    iget-object v2, p0, LX/Cbx;->b:LX/CcO;

    iget-object v2, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->w:Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    new-array v3, v3, [LX/1FJ;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, LX/3nE;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1920538
    iget-object v0, p0, LX/Cbx;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not share file (w/ Fresco + non-jpeg)"

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1920539
    iget-object v0, p0, LX/Cbx;->b:LX/CcO;

    invoke-static {v0}, LX/CcO;->n(LX/CcO;)V

    .line 1920540
    return-void
.end method
