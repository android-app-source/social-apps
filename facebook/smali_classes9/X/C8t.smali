.class public LX/C8t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition",
            "<*",
            "LX/1Ps;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition",
            "<*",
            "LX/1Ps;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;

.field private final e:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<*",
            "LX/1Ps;",
            "Lcom/facebook/widget/ShimmerFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<*",
            "LX/1Ps;",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<*",
            "LX/1Ps;",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<",
            "Ljava/lang/Float;",
            "LX/1PW;",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition;Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853386
    iput-object p1, p0, LX/C8t;->a:Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition;

    .line 1853387
    iput-object p2, p0, LX/C8t;->b:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;

    .line 1853388
    iput-object p3, p0, LX/C8t;->c:Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;

    .line 1853389
    iput-object p4, p0, LX/C8t;->d:Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;

    .line 1853390
    new-instance v0, LX/5eI;

    iget-object v1, p0, LX/C8t;->a:Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition;

    new-instance v2, LX/1QG;

    invoke-direct {v2}, LX/1QG;-><init>()V

    invoke-direct {v0, v1, v2}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/C8t;->e:LX/5eI;

    .line 1853391
    new-instance v0, LX/5eI;

    iget-object v1, p0, LX/C8t;->b:Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;

    new-instance v2, LX/1QG;

    invoke-direct {v2}, LX/1QG;-><init>()V

    invoke-direct {v0, v1, v2}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/C8t;->f:LX/5eI;

    .line 1853392
    new-instance v0, LX/5eI;

    iget-object v1, p0, LX/C8t;->c:Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;

    new-instance v2, LX/1QG;

    invoke-direct {v2}, LX/1QG;-><init>()V

    invoke-direct {v0, v1, v2}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/C8t;->g:LX/5eI;

    .line 1853393
    new-instance v0, LX/5eI;

    iget-object v1, p0, LX/C8t;->d:Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;

    sget-object v2, LX/1Qm;->a:LX/1PW;

    invoke-direct {v0, v1, v2}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/C8t;->h:LX/5eI;

    .line 1853394
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 1853395
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1853396
    const v1, 0x7f0b042c

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1853397
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 1853398
    const v0, 0x7f030a39

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public final a(LX/C8v;Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 1853399
    invoke-static {p3, p4}, LX/C8t;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 1853400
    invoke-static {p2}, LX/C8t;->a(Landroid/content/res/Resources;)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, LX/C8t;->a(LX/C8v;Landroid/view/ViewGroup;I)V

    .line 1853401
    return-object v0
.end method

.method public final a(LX/C8v;Landroid/view/ViewGroup;I)V
    .locals 5

    .prologue
    .line 1853402
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1853403
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 1853404
    sget-object v2, LX/C8s;->a:[I

    invoke-virtual {p1}, LX/C8v;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1853405
    invoke-static {v1, p2}, Lcom/facebook/feedplugins/loadingindicator/ShimmeringStoryPartDefinition;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/widget/ShimmerFrameLayout;

    move-result-object v2

    .line 1853406
    iget-object v3, p0, LX/C8t;->e:LX/5eI;

    invoke-virtual {v3, v2}, LX/5eI;->a(Landroid/view/View;)V

    .line 1853407
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1853408
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1853409
    :pswitch_0
    invoke-static {v1, p2}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    move-result-object v2

    .line 1853410
    iget-object v3, p0, LX/C8t;->f:LX/5eI;

    invoke-virtual {v3, v2}, LX/5eI;->a(Landroid/view/View;)V

    .line 1853411
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1853412
    :pswitch_1
    invoke-static {v1, p2}, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    move-result-object v2

    .line 1853413
    iget-object v3, p0, LX/C8t;->g:LX/5eI;

    invoke-virtual {v3, v2}, LX/5eI;->a(Landroid/view/View;)V

    .line 1853414
    iget-object v3, p0, LX/C8t;->g:LX/5eI;

    .line 1853415
    iget-object v4, v3, LX/5eI;->d:LX/5eH;

    invoke-static {v3, v4}, LX/5eI;->a(LX/5eI;LX/5eH;)V

    .line 1853416
    const/4 v4, 0x0

    iput-object v4, v3, LX/5eI;->e:Landroid/view/View;

    .line 1853417
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1853418
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
