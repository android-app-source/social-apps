.class public final LX/B3a;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic l:LX/B3e;

.field public final m:Lcom/facebook/fig/listitem/FigListItem;

.field private final n:LX/B3p;

.field public o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

.field public p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;


# direct methods
.method public constructor <init>(LX/B3e;Lcom/facebook/fig/listitem/FigListItem;LX/B3p;)V
    .locals 1

    .prologue
    .line 1740527
    iput-object p1, p0, LX/B3a;->l:LX/B3e;

    .line 1740528
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1740529
    iput-object p2, p0, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    .line 1740530
    iput-object p3, p0, LX/B3a;->n:LX/B3p;

    .line 1740531
    iget-object v0, p0, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, p0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1740532
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V
    .locals 1

    .prologue
    .line 1740524
    iput-object p1, p0, LX/B3a;->o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740525
    const/4 v0, 0x0

    iput-object v0, p0, LX/B3a;->p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1740526
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x40560925

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1740515
    iget-object v0, p0, LX/B3a;->m:Lcom/facebook/fig/listitem/FigListItem;

    if-ne p1, v0, :cond_1

    .line 1740516
    iget-object v0, p0, LX/B3a;->o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/B3a;->p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/B3u;->a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1740517
    iget-object v0, p0, LX/B3a;->l:LX/B3e;

    iget-object v0, v0, LX/B3e;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "category_browser_invalid_overlays"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing ImageOverlay in fragment model: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/B3a;->p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    invoke-virtual {v4}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1740518
    const v0, 0x798ac1cd

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1740519
    :goto_0
    return-void

    .line 1740520
    :cond_0
    iget-object v2, p0, LX/B3a;->n:LX/B3p;

    iget-object v0, p0, LX/B3a;->o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B3a;->o:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740521
    :goto_1
    iget-object v3, v2, LX/B3p;->a:LX/B3Z;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, LX/B3Z;->a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;I)V

    .line 1740522
    :cond_1
    const v0, -0x472f11bf

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1740523
    :cond_2
    iget-object v0, p0, LX/B3a;->p:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/B3w;->a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v0

    goto :goto_1
.end method
