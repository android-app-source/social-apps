.class public LX/CXF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CU4;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CTn;)V
    .locals 1

    .prologue
    .line 1909664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1909665
    iget-object v0, p1, LX/CTn;->b:Ljava/util/ArrayList;

    iput-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    .line 1909666
    iget-object v0, p1, LX/CTn;->d:Ljava/util/ArrayList;

    iput-object v0, p0, LX/CXF;->b:Ljava/util/ArrayList;

    .line 1909667
    return-void
.end method

.method public static a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909668
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909669
    invoke-virtual {p0, p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909670
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909671
    :goto_0
    return-void

    .line 1909672
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1909673
    iget-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1909674
    iget-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU4;

    iget-object v0, v0, LX/CU4;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;
    .locals 4
    .param p2    # LX/CSY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/CSY;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1909675
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1909676
    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 1909677
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1909678
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1909679
    iget-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU4;

    iget-object v0, v0, LX/CU4;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1909680
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1909681
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1909682
    :cond_1
    iget-object v0, p0, LX/CXF;->b:Ljava/util/ArrayList;

    move-object v1, v0

    goto :goto_0

    .line 1909683
    :cond_2
    return-object v3
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1909684
    iget-object v0, p0, LX/CXF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU4;

    iget-object v0, v0, LX/CU4;->b:Ljava/lang/String;

    return-object v0
.end method
