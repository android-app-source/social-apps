.class public final LX/Aon;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1714842
    iput-object p1, p0, LX/Aon;->e:LX/Aov;

    iput-object p2, p0, LX/Aon;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Aon;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/Aon;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aon;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    .line 1714843
    iget-object v0, p0, LX/Aon;->e:LX/Aov;

    iget-object v1, p0, LX/Aon;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Aon;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Aon;->c:Ljava/lang/String;

    iget-object v4, p0, LX/Aon;->d:Landroid/view/View;

    .line 1714844
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1714845
    move-object v6, v5

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714846
    iget-object v5, v0, LX/Aov;->b:LX/1EQ;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 1714847
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v8

    .line 1714848
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, LX/Aov;->c:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714849
    iget-object v9, v8, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v9

    .line 1714850
    invoke-static {v6}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v9

    move-object v6, v1

    move-object v10, v3

    .line 1714851
    iget-object p0, v5, LX/1EQ;->a:LX/0Zb;

    const-string p1, "feed_share_action"

    const/4 v1, 0x1

    invoke-interface {p0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 1714852
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1714853
    invoke-virtual {p0, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v1, "share_type"

    const-string v3, "send_in_message"

    invoke-virtual {p1, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v1, "story_id"

    invoke-virtual {p1, v1, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v1, "user_id"

    invoke-virtual {p1, v1, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v1, "shareable_id"

    invoke-virtual {p1, v1, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    invoke-virtual {p1, v10}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714854
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 1714855
    :cond_0
    iget-object v5, v0, LX/Aov;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/B9y;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    const-string v8, "footer_share_popover_menu"

    invoke-virtual {v5, v2, v6, v7, v8}, LX/B9y;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;ZLjava/lang/String;)V

    .line 1714856
    const/4 v0, 0x1

    return v0
.end method
