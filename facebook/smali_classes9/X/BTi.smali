.class public LX/BTi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field public final c:I

.field public final d:I

.field public final e:Landroid/content/Context;

.field public final f:LX/BV0;

.field public final g:LX/1FZ;

.field public h:Landroid/net/Uri;

.field public i:J

.field public j:F

.field public k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1787895
    const-class v0, LX/BTi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BTi;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BV0;LX/1FZ;LX/0ad;Landroid/net/Uri;JF)V
    .locals 4
    .param p5    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787897
    iput-object p1, p0, LX/BTi;->e:Landroid/content/Context;

    .line 1787898
    iput-object p2, p0, LX/BTi;->f:LX/BV0;

    .line 1787899
    iput-object p3, p0, LX/BTi;->g:LX/1FZ;

    .line 1787900
    iput-object p5, p0, LX/BTi;->h:Landroid/net/Uri;

    .line 1787901
    iput-wide p6, p0, LX/BTi;->i:J

    .line 1787902
    iput p8, p0, LX/BTi;->j:F

    .line 1787903
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BTi;->k:Ljava/lang/String;

    .line 1787904
    sget v0, LX/8tG;->c:I

    const/16 v1, 0x1e

    invoke-interface {p4, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/BTi;->b:I

    .line 1787905
    const/4 v0, 0x1

    sget v1, LX/8tG;->d:I

    const/16 v2, 0x96

    invoke-interface {p4, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/BTi;->c:I

    .line 1787906
    sget v0, LX/8tG;->e:I

    const/16 v1, 0x64

    invoke-interface {p4, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/BTi;->d:I

    .line 1787907
    return-void
.end method

.method public static a(LX/BTi;LX/BUz;I)V
    .locals 10

    .prologue
    .line 1787908
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "video_editing_frame_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BTi;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1787909
    iget-object v1, p0, LX/BTi;->e:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1787910
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1787911
    :cond_0
    :goto_0
    return-void

    .line 1787912
    :cond_1
    const/4 v5, 0x0

    .line 1787913
    :try_start_0
    iget v2, p0, LX/BTi;->j:F

    invoke-virtual {p1, p2, v2}, LX/BUz;->a(IF)LX/1FJ;

    move-result-object v6

    .line 1787914
    if-eqz v6, :cond_2

    invoke-virtual {v6}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move-object v2, v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1787915
    :goto_1
    move-object v2, v2

    .line 1787916
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1787917
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".tmp"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1787918
    iget-object v0, p0, LX/BTi;->e:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 1787919
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iget v6, p0, LX/BTi;->b:I

    invoke-virtual {v0, v5, v6, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1787920
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1787921
    iget-object v0, p0, LX/BTi;->e:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1787922
    invoke-virtual {v2}, LX/1FJ;->close()V

    goto :goto_0

    .line 1787923
    :catch_0
    move-exception v0

    .line 1787924
    :try_start_2
    sget-object v1, LX/BTi;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t save bitmap file"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1787925
    invoke-virtual {v2}, LX/1FJ;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1FJ;->close()V

    throw v0

    .line 1787926
    :cond_3
    :try_start_3
    invoke-virtual {v6}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1787927
    iget v3, p0, LX/BTi;->c:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1787928
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 1787929
    invoke-virtual {v7, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1787930
    iget-object v3, p0, LX/BTi;->g:LX/1FZ;

    iget v4, p0, LX/BTi;->c:I

    iget v8, p0, LX/BTi;->c:I

    int-to-float v8, v8

    iget v9, p0, LX/BTi;->j:F

    div-float/2addr v8, v9

    float-to-int v8, v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4, v8, v9}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v4

    .line 1787931
    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 1787932
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1787933
    const/4 v3, 0x0

    invoke-virtual {v8, v2, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1787934
    invoke-virtual {v6}, LX/1FJ;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v4

    .line 1787935
    goto/16 :goto_1

    .line 1787936
    :catch_1
    move-exception v2

    .line 1787937
    sget-object v3, LX/BTi;->a:Ljava/lang/String;

    const-string v4, "Unable to extract frame"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v5

    .line 1787938
    goto/16 :goto_1
.end method
