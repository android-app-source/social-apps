.class public final LX/AYV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AYY;


# direct methods
.method public constructor <init>(LX/AYY;)V
    .locals 0

    .prologue
    .line 1685576
    iput-object p1, p0, LX/AYV;->a:LX/AYY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2d8b7490

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1685577
    iget-object v1, p0, LX/AYV;->a:LX/AYY;

    iget-object v1, v1, LX/AYY;->n:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->b()V

    .line 1685578
    iget-object v1, p0, LX/AYV;->a:LX/AYY;

    iget-boolean v1, v1, LX/AYY;->I:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/AYV;->a:LX/AYY;

    iget-boolean v1, v1, LX/AYY;->J:Z

    if-eqz v1, :cond_0

    .line 1685579
    iget-object v1, p0, LX/AYV;->a:LX/AYY;

    sget-object v2, LX/AXr;->FUNDRAISER_ALREADY_TURNED_ON:LX/AXr;

    invoke-static {v1, v2}, LX/AYY;->a$redex0(LX/AYY;LX/AXr;)V

    .line 1685580
    const v1, 0x6961b4a9

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1685581
    :goto_0
    return-void

    .line 1685582
    :cond_0
    iget-object v1, p0, LX/AYV;->a:LX/AYY;

    .line 1685583
    iget-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    if-nez v2, :cond_1

    .line 1685584
    new-instance v2, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-direct {v2}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;-><init>()V

    iput-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    .line 1685585
    iget-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    .line 1685586
    iput-object v1, v2, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    .line 1685587
    :cond_1
    iget-object v2, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-boolean v3, v1, LX/AYY;->I:Z

    .line 1685588
    iput-boolean v3, v2, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    .line 1685589
    iget-object v2, v1, LX/AYY;->p:LX/AaY;

    iget-boolean v3, v1, LX/AYY;->I:Z

    .line 1685590
    iput-boolean v3, v2, LX/AaY;->a:Z

    .line 1685591
    iget-object v2, v1, LX/AYY;->p:LX/AaY;

    .line 1685592
    iget-object v3, v2, LX/AaY;->c:LX/0if;

    sget-object v4, LX/0ig;->G:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 1685593
    iget-object v3, v2, LX/AaY;->c:LX/0if;

    sget-object v4, LX/0ig;->G:LX/0ih;

    const-string v5, "start_session"

    const/4 p0, 0x0

    invoke-static {v2}, LX/AaY;->g(LX/AaY;)LX/1rQ;

    move-result-object p1

    invoke-virtual {v3, v4, v5, p0, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1685594
    invoke-virtual {v1}, LX/AYY;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    .line 1685595
    if-eqz v2, :cond_2

    .line 1685596
    iget-object v3, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    iget-object v4, v1, LX/AYY;->p:LX/AaY;

    .line 1685597
    iput-object v4, v3, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->y:LX/AaY;

    .line 1685598
    iget-object v3, v1, LX/AYY;->F:Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string v4, "FACECAST_TIP_JAR_SETTING_FRAGMENT_TAG"

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1685599
    :cond_2
    const v1, 0xbef285a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
