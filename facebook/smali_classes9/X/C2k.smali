.class public LX/C2k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/154;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/154;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1844568
    iput-object p1, p0, LX/C2k;->a:Landroid/content/res/Resources;

    .line 1844569
    iput-object p2, p0, LX/C2k;->b:LX/154;

    .line 1844570
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1844571
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v0

    .line 1844572
    iget-object v1, p0, LX/C2k;->a:Landroid/content/res/Resources;

    const v2, 0x7f082a09

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
