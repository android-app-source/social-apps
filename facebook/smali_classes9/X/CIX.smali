.class public LX/CIX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1874345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1874337
    invoke-static {p0, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1874338
    if-eqz v0, :cond_0

    .line 1874339
    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 1874340
    if-eqz p1, :cond_0

    .line 1874341
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1874342
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1874343
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1874344
    :cond_0
    return-void
.end method
