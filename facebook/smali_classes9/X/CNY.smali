.class public final LX/CNY;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/CNY;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CNW;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CNZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1881887
    const/4 v0, 0x0

    sput-object v0, LX/CNY;->a:LX/CNY;

    .line 1881888
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CNY;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1881889
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 1881890
    new-instance v0, LX/CNZ;

    invoke-direct {v0}, LX/CNZ;-><init>()V

    iput-object v0, p0, LX/CNY;->c:LX/CNZ;

    .line 1881891
    return-void
.end method

.method public static a(LX/1De;)LX/CNW;
    .locals 2

    .prologue
    .line 1881892
    new-instance v0, LX/CNX;

    invoke-direct {v0}, LX/CNX;-><init>()V

    .line 1881893
    sget-object v1, LX/CNY;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CNW;

    .line 1881894
    if-nez v1, :cond_0

    .line 1881895
    new-instance v1, LX/CNW;

    invoke-direct {v1}, LX/CNW;-><init>()V

    .line 1881896
    :cond_0
    invoke-static {v1, p0, v0}, LX/CNW;->a$redex0(LX/CNW;LX/1De;LX/CNX;)V

    .line 1881897
    move-object v0, v1

    .line 1881898
    return-object v0
.end method

.method public static declared-synchronized a()LX/CNY;
    .locals 2

    .prologue
    .line 1881899
    const-class v1, LX/CNY;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CNY;->a:LX/CNY;

    if-nez v0, :cond_0

    .line 1881900
    new-instance v0, LX/CNY;

    invoke-direct {v0}, LX/CNY;-><init>()V

    sput-object v0, LX/CNY;->a:LX/CNY;

    .line 1881901
    :cond_0
    sget-object v0, LX/CNY;->a:LX/CNY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1881902
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1881903
    check-cast p2, LX/CNX;

    .line 1881904
    iget-object v0, p2, LX/CNX;->a:LX/CO8;

    iget-object v1, p2, LX/CNX;->b:LX/CNb;

    .line 1881905
    invoke-virtual {v1}, LX/CNb;->a()Ljava/lang/String;

    move-result-object p0

    .line 1881906
    sget-object v2, LX/CNZ;->a:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1881907
    new-instance v2, LX/0Zi;

    const/16 p2, 0xa

    invoke-direct {v2, p2}, LX/0Zi;-><init>(I)V

    .line 1881908
    sget-object p2, LX/CNZ;->a:Ljava/util/Map;

    invoke-interface {p2, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1881909
    :goto_0
    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    .line 1881910
    if-nez v2, :cond_0

    .line 1881911
    invoke-static {p0}, LX/3ji;->a(Ljava/lang/String;)LX/3jJ;

    move-result-object v2

    .line 1881912
    invoke-virtual {v2, p1}, LX/3jJ;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1881913
    :cond_0
    const-string p0, ""

    invoke-virtual {v0, v2, p0}, LX/CO8;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 1881914
    move-object v0, v2

    .line 1881915
    return-object v0

    .line 1881916
    :cond_1
    sget-object v2, LX/CNZ;->a:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zi;

    goto :goto_0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 2

    .prologue
    .line 1881917
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1881918
    check-cast p3, LX/CNX;

    .line 1881919
    iget-object v0, p3, LX/CNX;->a:LX/CO8;

    iget-object v1, p3, LX/CNX;->b:LX/CNb;

    .line 1881920
    invoke-virtual {v1}, LX/CNb;->a()Ljava/lang/String;

    move-result-object p0

    .line 1881921
    sget-object p1, LX/CNZ;->a:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Zi;

    .line 1881922
    invoke-virtual {p0, p2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1881923
    invoke-virtual {v0, p2}, LX/CO8;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1881924
    return-void
.end method
