.class public LX/Bzl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35l;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bzl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/35l;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839167
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839168
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bzl;->b:LX/0Zi;

    .line 1839169
    iput-object p1, p0, LX/Bzl;->a:LX/0Ot;

    .line 1839170
    return-void
.end method

.method public static a(LX/0QB;)LX/Bzl;
    .locals 4

    .prologue
    .line 1839171
    const-class v1, LX/Bzl;

    monitor-enter v1

    .line 1839172
    :try_start_0
    sget-object v0, LX/Bzl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839173
    sput-object v2, LX/Bzl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839174
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839175
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839176
    new-instance v3, LX/Bzl;

    const/16 p0, 0x80a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bzl;-><init>(LX/0Ot;)V

    .line 1839177
    move-object v0, v3

    .line 1839178
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839179
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bzl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839180
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1839182
    check-cast p2, LX/Bzk;

    .line 1839183
    iget-object v0, p0, LX/Bzl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35l;

    iget-object v1, p2, LX/Bzk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bzk;->b:LX/1Pn;

    .line 1839184
    iget-object v3, v0, LX/35l;->b:LX/35i;

    invoke-virtual {v3, v1}, LX/35i;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v3

    .line 1839185
    const/4 p0, 0x1

    int-to-float v3, v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    invoke-static {p0, v3, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result p0

    .line 1839186
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1839187
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3, p0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1839188
    if-nez v3, :cond_0

    .line 1839189
    const/4 v3, 0x0

    .line 1839190
    :goto_0
    move-object v0, v3

    .line 1839191
    return-object v0

    .line 1839192
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1839193
    iget-object p0, v0, LX/35l;->a:LX/Ap5;

    invoke-virtual {p0, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object p0

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v3

    check-cast v2, LX/1Po;

    invoke-virtual {v3, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object v3

    .line 1839194
    iget-object p2, v0, LX/35l;->c:LX/1qb;

    .line 1839195
    iget-object p0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1839196
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p2, p0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/1qb;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object p0

    move-object p0, p0

    .line 1839197
    invoke-virtual {v3, p0}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    .line 1839198
    iget-object p0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1839199
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object p0

    move-object p0, p0

    .line 1839200
    invoke-virtual {v3, p0}, LX/Ap4;->b(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    .line 1839201
    iget-object p0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1839202
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object p0

    move-object p0, p0

    .line 1839203
    invoke-virtual {v3, p0}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1839204
    invoke-static {}, LX/1dS;->b()V

    .line 1839205
    iget v0, p1, LX/1dQ;->b:I

    .line 1839206
    packed-switch v0, :pswitch_data_0

    .line 1839207
    :goto_0
    return-object v2

    .line 1839208
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1839209
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1839210
    check-cast v1, LX/Bzk;

    .line 1839211
    iget-object v3, p0, LX/Bzl;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/35l;

    iget-object p1, v1, LX/Bzk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/Bzk;->b:LX/1Pn;

    .line 1839212
    iget-object p0, v3, LX/35l;->d:LX/C0D;

    check-cast p2, LX/1Pq;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1839213
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x15634986
        :pswitch_0
    .end packed-switch
.end method
