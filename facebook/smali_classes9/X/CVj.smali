.class public final LX/CVj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1904571
    const/4 v12, 0x0

    .line 1904572
    const/4 v11, 0x0

    .line 1904573
    const/4 v10, 0x0

    .line 1904574
    const/4 v9, 0x0

    .line 1904575
    const/4 v8, 0x0

    .line 1904576
    const/4 v7, 0x0

    .line 1904577
    const/4 v6, 0x0

    .line 1904578
    const/4 v5, 0x0

    .line 1904579
    const/4 v4, 0x0

    .line 1904580
    const/4 v3, 0x0

    .line 1904581
    const/4 v2, 0x0

    .line 1904582
    const/4 v1, 0x0

    .line 1904583
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1904584
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1904585
    const/4 v1, 0x0

    .line 1904586
    :goto_0
    return v1

    .line 1904587
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1904588
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_d

    .line 1904589
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1904590
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1904591
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1904592
    const-string v14, "anchor_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1904593
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1904594
    :cond_2
    const-string v14, "body"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1904595
    invoke-static/range {p0 .. p1}, LX/CVr;->b(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1904596
    :cond_3
    const-string v14, "component_flow_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1904597
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1904598
    :cond_4
    const-string v14, "disclaimer"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1904599
    invoke-static/range {p0 .. p1}, LX/CVl;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1904600
    :cond_5
    const-string v14, "event_listeners"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1904601
    invoke-static/range {p0 .. p1}, LX/CVU;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1904602
    :cond_6
    const-string v14, "footer"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1904603
    invoke-static/range {p0 .. p1}, LX/CVs;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1904604
    :cond_7
    const-string v14, "header"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1904605
    invoke-static/range {p0 .. p1}, LX/CVr;->b(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1904606
    :cond_8
    const-string v14, "navbar_action"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1904607
    invoke-static/range {p0 .. p1}, LX/CVi;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1904608
    :cond_9
    const-string v14, "next_button"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1904609
    invoke-static/range {p0 .. p1}, LX/CVg;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1904610
    :cond_a
    const-string v14, "screen_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1904611
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1904612
    :cond_b
    const-string v14, "screen_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 1904613
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 1904614
    :cond_c
    const-string v14, "title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1904615
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1904616
    :cond_d
    const/16 v13, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1904617
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1904618
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1904619
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1904620
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1904621
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1904622
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1904623
    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1904624
    const/4 v6, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1904625
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1904626
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1904627
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1904628
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1904629
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
