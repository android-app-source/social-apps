.class public final LX/Cci;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Ccj;


# direct methods
.method public constructor <init>(LX/Ccj;Landroid/content/Context;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1921569
    iput-object p1, p0, LX/Cci;->d:LX/Ccj;

    iput-object p2, p0, LX/Cci;->a:Landroid/content/Context;

    iput-boolean p3, p0, LX/Cci;->b:Z

    iput-object p4, p0, LX/Cci;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1921570
    iget-object v0, p0, LX/Cci;->d:LX/Ccj;

    iget-object v0, v0, LX/Ccj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/Cci;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->dn:Ljava/lang/String;

    const-string v3, "/feed/panel/advanced_prep/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1921571
    iget-object v0, p0, LX/Cci;->d:LX/Ccj;

    iget-object v0, v0, LX/Ccj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Cci;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1921572
    iget-object v0, p0, LX/Cci;->d:LX/Ccj;

    iget-object v0, v0, LX/Ccj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WN;

    const-string v1, "warning_screen_content_filter_prefs_tapped"

    iget-boolean v2, p0, LX/Cci;->b:Z

    iget-object v3, p0, LX/Cci;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 1921573
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1921574
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1921575
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1921576
    iget-object v0, p0, LX/Cci;->a:Landroid/content/Context;

    const v1, 0x7f0a00d5

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1921577
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1921578
    return-void
.end method
