.class public final LX/ApE;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/ApF;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1715593
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1715594
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "photoUri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "titleStyle"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "aspectRatio"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "photoWidth"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "titleMarginTop"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/ApE;->b:[Ljava/lang/String;

    .line 1715595
    iput v3, p0, LX/ApE;->c:I

    .line 1715596
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/ApE;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/ApE;LX/1De;IILcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;)V
    .locals 1

    .prologue
    .line 1715589
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1715590
    iput-object p4, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    .line 1715591
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1715592
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/ApE;
    .locals 2

    .prologue
    .line 1715586
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    .line 1715587
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715588
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/ApE;
    .locals 1

    .prologue
    .line 1715584
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 1715585
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/ApE;
    .locals 1

    .prologue
    .line 1715582
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    .line 1715583
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1715578
    invoke-super {p0}, LX/1X5;->a()V

    .line 1715579
    const/4 v0, 0x0

    iput-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    .line 1715580
    sget-object v0, LX/ApF;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1715581
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/ApE;
    .locals 1

    .prologue
    .line 1715576
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    .line 1715577
    return-object p0
.end method

.method public final c(F)LX/ApE;
    .locals 2

    .prologue
    .line 1715573
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->c:F

    .line 1715574
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715575
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/ApE;
    .locals 1

    .prologue
    .line 1715571
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    .line 1715572
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/ApF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1715561
    iget-object v1, p0, LX/ApE;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ApE;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/ApE;->c:I

    if-ge v1, v2, :cond_2

    .line 1715562
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1715563
    :goto_0
    iget v2, p0, LX/ApE;->c:I

    if-ge v0, v2, :cond_1

    .line 1715564
    iget-object v2, p0, LX/ApE;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1715565
    iget-object v2, p0, LX/ApE;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1715566
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1715567
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715568
    :cond_2
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    .line 1715569
    invoke-virtual {p0}, LX/ApE;->a()V

    .line 1715570
    return-object v0
.end method

.method public final d(F)LX/ApE;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1715597
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->d:I

    .line 1715598
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715599
    return-object p0
.end method

.method public final e(F)LX/ApE;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1715535
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->e:I

    .line 1715536
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715537
    return-object p0
.end method

.method public final f(F)LX/ApE;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1715540
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->h:I

    .line 1715541
    return-object p0
.end method

.method public final g(F)LX/ApE;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1715538
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->i:I

    .line 1715539
    return-object p0
.end method

.method public final h(F)LX/ApE;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1715542
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->k:I

    .line 1715543
    return-object p0
.end method

.method public final h(I)LX/ApE;
    .locals 2

    .prologue
    .line 1715544
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->b:I

    .line 1715545
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715546
    return-object p0
.end method

.method public final i(I)LX/ApE;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1715547
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->d:I

    .line 1715548
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715549
    return-object p0
.end method

.method public final j(I)LX/ApE;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1715550
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->e:I

    .line 1715551
    iget-object v0, p0, LX/ApE;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715552
    return-object p0
.end method

.method public final k(I)LX/ApE;
    .locals 1

    .prologue
    .line 1715553
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->f:I

    .line 1715554
    return-object p0
.end method

.method public final l(I)LX/ApE;
    .locals 1

    .prologue
    .line 1715555
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->g:I

    .line 1715556
    return-object p0
.end method

.method public final m(I)LX/ApE;
    .locals 1

    .prologue
    .line 1715557
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->j:I

    .line 1715558
    return-object p0
.end method

.method public final n(I)LX/ApE;
    .locals 1
    .param p1    # I
        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigCoverPhotoIconType;
        .end annotation
    .end param

    .prologue
    .line 1715559
    iget-object v0, p0, LX/ApE;->a:Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->p:I

    .line 1715560
    return-object p0
.end method
