.class public final enum LX/CBk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBk;

.field public static final enum BLUE:LX/CBk;

.field public static final enum UNKNOWN:LX/CBk;

.field public static final enum WHITE:LX/CBk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856785
    new-instance v0, LX/CBk;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v2}, LX/CBk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBk;->BLUE:LX/CBk;

    .line 1856786
    new-instance v0, LX/CBk;

    const-string v1, "WHITE"

    invoke-direct {v0, v1, v3}, LX/CBk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBk;->WHITE:LX/CBk;

    .line 1856787
    new-instance v0, LX/CBk;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/CBk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBk;->UNKNOWN:LX/CBk;

    .line 1856788
    const/4 v0, 0x3

    new-array v0, v0, [LX/CBk;

    sget-object v1, LX/CBk;->BLUE:LX/CBk;

    aput-object v1, v0, v2

    sget-object v1, LX/CBk;->WHITE:LX/CBk;

    aput-object v1, v0, v3

    sget-object v1, LX/CBk;->UNKNOWN:LX/CBk;

    aput-object v1, v0, v4

    sput-object v0, LX/CBk;->$VALUES:[LX/CBk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856784
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBk;
    .locals 1

    .prologue
    .line 1856789
    if-nez p0, :cond_0

    .line 1856790
    :try_start_0
    sget-object v0, LX/CBk;->UNKNOWN:LX/CBk;

    .line 1856791
    :goto_0
    return-object v0

    .line 1856792
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBk;->valueOf(Ljava/lang/String;)LX/CBk;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856793
    :catch_0
    sget-object v0, LX/CBk;->UNKNOWN:LX/CBk;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBk;
    .locals 1

    .prologue
    .line 1856782
    const-class v0, LX/CBk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBk;

    return-object v0
.end method

.method public static values()[LX/CBk;
    .locals 1

    .prologue
    .line 1856783
    sget-object v0, LX/CBk;->$VALUES:[LX/CBk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBk;

    return-object v0
.end method
