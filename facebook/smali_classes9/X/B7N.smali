.class public final LX/B7N;
.super LX/B7B;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:LX/B7K;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/B7O;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenPage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/B7K;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1747967
    invoke-direct {p0}, LX/B7B;-><init>()V

    .line 1747968
    iput-object p2, p0, LX/B7N;->b:Ljava/lang/String;

    .line 1747969
    iput-object p3, p0, LX/B7N;->f:Ljava/lang/String;

    .line 1747970
    iput-object p4, p0, LX/B7N;->e:Ljava/lang/String;

    .line 1747971
    iput-object p5, p0, LX/B7N;->g:LX/B7K;

    .line 1747972
    iput-object v0, p0, LX/B7N;->a:LX/0Px;

    .line 1747973
    iput-object v0, p0, LX/B7N;->c:Ljava/lang/String;

    .line 1747974
    iput-object v0, p0, LX/B7N;->d:Ljava/lang/String;

    .line 1747975
    if-eqz p1, :cond_1

    .line 1747976
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B7N;->a:LX/0Px;

    .line 1747977
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/B7N;->h:Ljava/util/List;

    .line 1747978
    iget-object v0, p0, LX/B7N;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/B7N;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1747979
    iget-object v4, p0, LX/B7N;->h:Ljava/util/List;

    new-instance v5, LX/B7O;

    invoke-direct {v5, v0}, LX/B7O;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1747980
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1747981
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenPage;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;

    .line 1747982
    if-eqz v0, :cond_1

    .line 1747983
    sget-object v4, LX/B7M;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1747984
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1747985
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7N;->c:Ljava/lang/String;

    goto :goto_2

    .line 1747986
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenPrivacyNode;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7N;->d:Ljava/lang/String;

    goto :goto_2

    .line 1747987
    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
