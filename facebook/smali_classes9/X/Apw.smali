.class public LX/Apw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field private static h:LX/0Xm;


# instance fields
.field public final g:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1717052
    const v0, 0x7f0a008a

    sput v0, LX/Apw;->a:I

    .line 1717053
    const v0, 0x7f0a00cf

    sput v0, LX/Apw;->b:I

    .line 1717054
    const v0, 0x7f0a00a3

    sput v0, LX/Apw;->c:I

    .line 1717055
    const v0, 0x7f0a00a1

    sput v0, LX/Apw;->d:I

    .line 1717056
    const v0, 0x7f020b15

    sput v0, LX/Apw;->e:I

    .line 1717057
    const v0, 0x7f020b14

    sput v0, LX/Apw;->f:I

    return-void
.end method

.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1717058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1717059
    iput-object p1, p0, LX/Apw;->g:LX/1vg;

    .line 1717060
    return-void
.end method

.method public static a(LX/0QB;)LX/Apw;
    .locals 4

    .prologue
    .line 1717061
    const-class v1, LX/Apw;

    monitor-enter v1

    .line 1717062
    :try_start_0
    sget-object v0, LX/Apw;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1717063
    sput-object v2, LX/Apw;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1717064
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1717065
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1717066
    new-instance p0, LX/Apw;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/Apw;-><init>(LX/1vg;)V

    .line 1717067
    move-object v0, p0

    .line 1717068
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1717069
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1717070
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1717071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
