.class public final LX/CQD;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1885468
    const-class v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultsSearchQueryV2Model;

    const v0, 0x2b3e225e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "NearbyPlacesHugeResultsSearchQueryV2"

    const-string v6, "dd42729f7306cdf85853f7234637faf6"

    const-string v7, "nearby_search"

    const-string v8, "10155156451931729"

    const/4 v9, 0x0

    .line 1885469
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1885470
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1885471
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1885472
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1885473
    sparse-switch v0, :sswitch_data_0

    .line 1885474
    :goto_0
    return-object p1

    .line 1885475
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1885476
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1885477
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1885478
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1885479
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1885480
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1885481
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1885482
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1885483
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1885484
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1885485
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1885486
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1885487
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1885488
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1885489
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1885490
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1885491
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1885492
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1885493
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1885494
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1885495
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1885496
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1885497
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1885498
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1885499
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x66801974 -> :sswitch_e
        -0x5864c243 -> :sswitch_2
        -0x55d45394 -> :sswitch_6
        -0x54eebaed -> :sswitch_4
        -0x54792830 -> :sswitch_0
        -0x52d0f9a0 -> :sswitch_16
        -0x47f307db -> :sswitch_d
        -0x295d97e2 -> :sswitch_3
        -0x1e0f349f -> :sswitch_c
        0x2f63fd -> :sswitch_1
        0x37a1af -> :sswitch_18
        0x6234bbb -> :sswitch_7
        0x6424905 -> :sswitch_b
        0x683094a -> :sswitch_13
        0x688c9cd -> :sswitch_15
        0x83009af -> :sswitch_9
        0x13bf1dd7 -> :sswitch_10
        0x13f7647d -> :sswitch_12
        0x1b7d0371 -> :sswitch_14
        0x20ebb9e5 -> :sswitch_8
        0x32fb70cf -> :sswitch_5
        0x3cbbc973 -> :sswitch_11
        0x5d8fb49a -> :sswitch_f
        0x6597ee3b -> :sswitch_17
        0x73a026b5 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1885500
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1885501
    :goto_1
    return v0

    .line 1885502
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1885503
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
