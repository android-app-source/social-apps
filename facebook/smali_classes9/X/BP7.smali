.class public LX/BP7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/0Or;)V
    .locals 2
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780736
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/BP7;->a:J

    .line 1780737
    iput-object p2, p0, LX/BP7;->b:LX/0Or;

    .line 1780738
    return-void
.end method

.method private a(Ljava/lang/String;JZLandroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/String;)V
    .locals 4
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1780721
    if-nez p5, :cond_0

    .line 1780722
    :goto_0
    return-void

    .line 1780723
    :cond_0
    if-nez p1, :cond_1

    .line 1780724
    const v0, 0x7f08273f

    invoke-virtual {p5, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p5, v0}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1780725
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    invoke-direct {v1, p5, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1780726
    const-string v0, "cover_photo_uri"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780727
    const-string v0, "cover_photo_fbid"

    invoke-virtual {v1, v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1780728
    const-string v0, "cover_photo_refresh_header"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1780729
    const-string v0, "target_fragment"

    sget-object v2, LX/0cQ;->TIMELINE_COVERPHOTO_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1780730
    const-string v0, "profile_id"

    iget-wide v2, p0, LX/BP7;->a:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1780731
    const-string v0, "session_id"

    invoke-virtual {v1, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780732
    const-string v0, "prompt_entry_point_analytics_extra"

    invoke-virtual {v1, v0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1780733
    const-string v0, "prompt_object_class_name_extra"

    invoke-virtual {v1, v0, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780734
    iget-object v0, p0, LX/BP7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc37

    invoke-interface {v0, v1, v2, p5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/content/Intent;ZLjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 9

    .prologue
    .line 1780739
    const-string v0, "extra_media_items"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1780740
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1780741
    :cond_0
    :goto_0
    return-void

    .line 1780742
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-class v0, LX/1kW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move v4, p3

    move-object v5, p1

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, LX/BP7;->a(Ljava/lang/String;JZLandroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;Z)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1780717
    const-string v0, "extra_media_items"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1780718
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1780719
    :cond_0
    :goto_0
    return-void

    .line 1780720
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object v0, p0

    move v4, p3

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v0 .. v8}, LX/BP7;->a(Ljava/lang/String;JZLandroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/String;)V

    goto :goto_0
.end method
