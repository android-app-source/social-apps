.class public LX/AuN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8GH;


# instance fields
.field private final a:LX/Atw;

.field public final b:LX/Atx;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/friendsharing/inspiration/InspirationViewController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Atw;LX/Atx;)V
    .locals 1
    .param p1    # LX/Atw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Atx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722555
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1722556
    iput-object v0, p0, LX/AuN;->c:LX/0Px;

    .line 1722557
    iput-object p1, p0, LX/AuN;->a:LX/Atw;

    .line 1722558
    iput-object p2, p0, LX/AuN;->b:LX/Atx;

    .line 1722559
    return-void
.end method

.method private a(I)LX/89u;
    .locals 2

    .prologue
    .line 1722548
    iget-object v0, p0, LX/AuN;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1722549
    const/4 v0, 0x0

    .line 1722550
    :goto_0
    iget-object v1, p0, LX/AuN;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AsM;

    .line 1722551
    iget-object v1, v0, LX/AsM;->m:LX/AtP;

    move-object v0, v1

    .line 1722552
    return-object v0

    .line 1722553
    :cond_0
    iget-object v0, p0, LX/AuN;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v0, p1

    iget-object v1, p0, LX/AuN;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1722541
    if-nez p1, :cond_0

    .line 1722542
    :goto_0
    return-void

    .line 1722543
    :cond_0
    iget-object v0, p0, LX/AuN;->a:LX/Atw;

    invoke-virtual {v0}, LX/Atw;->a()I

    move-result v0

    .line 1722544
    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1}, LX/AuN;->a(I)LX/89u;

    move-result-object v1

    invoke-interface {v1}, LX/89u;->b()V

    .line 1722545
    invoke-direct {p0, v0}, LX/AuN;->a(I)LX/89u;

    move-result-object v1

    invoke-interface {v1}, LX/89u;->b()V

    .line 1722546
    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/AuN;->a(I)LX/89u;

    move-result-object v0

    invoke-interface {v0}, LX/89u;->b()V

    .line 1722547
    iget-object v0, p0, LX/AuN;->b:LX/Atx;

    invoke-virtual {v0}, LX/Atx;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V
    .locals 1
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1722560
    if-nez p1, :cond_0

    .line 1722561
    :goto_0
    return-void

    .line 1722562
    :cond_0
    iget-object v0, p0, LX/AuN;->a:LX/Atw;

    invoke-virtual {v0}, LX/Atw;->a()I

    move-result v0

    .line 1722563
    if-nez p2, :cond_1

    .line 1722564
    invoke-direct {p0, v0}, LX/AuN;->a(I)LX/89u;

    move-result-object v0

    invoke-interface {v0}, LX/89u;->c()V

    .line 1722565
    :cond_1
    iget-object v0, p0, LX/AuN;->b:LX/Atx;

    invoke-virtual {v0}, LX/Atx;->a()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1722540
    return-void
.end method

.method public final b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1722539
    return-void
.end method

.method public final c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1722537
    invoke-virtual {p0, p1, v0, v0}, LX/AuN;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V

    .line 1722538
    return-void
.end method
