.class public final LX/CcH;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:LX/CcI;


# direct methods
.method public constructor <init>(LX/CcI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1920772
    iput-object p1, p0, LX/CcH;->h:LX/CcI;

    iput-object p2, p0, LX/CcH;->a:Ljava/lang/String;

    iput-object p3, p0, LX/CcH;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CcH;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CcH;->d:Ljava/lang/String;

    iput-object p6, p0, LX/CcH;->e:Ljava/lang/String;

    iput-object p7, p0, LX/CcH;->f:Ljava/lang/String;

    iput-object p8, p0, LX/CcH;->g:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 1920773
    iget-object v0, p0, LX/CcH;->h:LX/CcI;

    iget-object v0, v0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    iget-object v1, p0, LX/CcH;->h:LX/CcI;

    iget-object v1, v1, LX/CcI;->a:LX/CcJ;

    iget-object v1, v1, LX/CcJ;->a:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CcH;->a:Ljava/lang/String;

    iget-object v3, p0, LX/CcH;->b:Ljava/lang/String;

    iget-object v4, p0, LX/CcH;->c:Ljava/lang/String;

    iget-object v5, p0, LX/CcH;->d:Ljava/lang/String;

    iget-object v6, p0, LX/CcH;->e:Ljava/lang/String;

    iget-object v7, p0, LX/CcH;->f:Ljava/lang/String;

    iget-object v8, p0, LX/CcH;->g:Ljava/lang/String;

    move-object v10, v9

    invoke-virtual/range {v0 .. v10}, LX/745;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1920774
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1920775
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v10, 0x0

    .line 1920776
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    .line 1920777
    if-eqz v0, :cond_0

    .line 1920778
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1920779
    if-eqz v1, :cond_0

    .line 1920780
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1920781
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1920782
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v0, v1

    .line 1920783
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1920784
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aO()Ljava/lang/String;

    move-result-object v9

    .line 1920785
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aP()Ljava/lang/String;

    move-result-object v10

    .line 1920786
    :goto_0
    iget-object v0, p0, LX/CcH;->h:LX/CcI;

    iget-object v0, v0, LX/CcI;->a:LX/CcJ;

    iget-object v0, v0, LX/CcJ;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    iget-object v1, p0, LX/CcH;->h:LX/CcI;

    iget-object v1, v1, LX/CcI;->a:LX/CcJ;

    iget-object v1, v1, LX/CcJ;->a:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CcH;->a:Ljava/lang/String;

    iget-object v3, p0, LX/CcH;->b:Ljava/lang/String;

    iget-object v4, p0, LX/CcH;->c:Ljava/lang/String;

    iget-object v5, p0, LX/CcH;->d:Ljava/lang/String;

    iget-object v6, p0, LX/CcH;->e:Ljava/lang/String;

    iget-object v7, p0, LX/CcH;->f:Ljava/lang/String;

    iget-object v8, p0, LX/CcH;->g:Ljava/lang/String;

    invoke-virtual/range {v0 .. v10}, LX/745;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1920787
    return-void

    :cond_0
    move-object v9, v10

    goto :goto_0
.end method
