.class public final LX/BVB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Z

.field private h:Landroid/graphics/PointF;

.field private i:Landroid/graphics/PointF;

.field private j:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1790224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790225
    const/16 v0, 0x1e0

    iput v0, p0, LX/BVB;->a:I

    .line 1790226
    const/16 v0, 0x280

    iput v0, p0, LX/BVB;->b:I

    .line 1790227
    iput v3, p0, LX/BVB;->c:I

    .line 1790228
    iput v3, p0, LX/BVB;->d:I

    .line 1790229
    iput v2, p0, LX/BVB;->e:I

    .line 1790230
    iput v2, p0, LX/BVB;->f:I

    .line 1790231
    iput-boolean v2, p0, LX/BVB;->g:Z

    .line 1790232
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, LX/BVB;->h:Landroid/graphics/PointF;

    .line 1790233
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, LX/BVB;->i:Landroid/graphics/PointF;

    .line 1790234
    const/high16 v0, 0x3e800000    # 0.25f

    iput v0, p0, LX/BVB;->j:F

    return-void
.end method


# virtual methods
.method public final a(FF)LX/BVB;
    .locals 1

    .prologue
    .line 1790235
    iget-object v0, p0, LX/BVB;->h:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 1790236
    iget-object v0, p0, LX/BVB;->h:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 1790237
    return-object p0
.end method

.method public final a(II)LX/BVB;
    .locals 0

    .prologue
    .line 1790238
    iput p1, p0, LX/BVB;->a:I

    .line 1790239
    iput p2, p0, LX/BVB;->b:I

    .line 1790240
    return-object p0
.end method

.method public final a()[B
    .locals 7

    .prologue
    .line 1790241
    new-instance v0, LX/0eX;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1790242
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/0eX;->b(I)V

    .line 1790243
    iget v1, p0, LX/BVB;->a:I

    const/4 v2, 0x0

    .line 1790244
    invoke-virtual {v0, v2, v1, v2}, LX/0eX;->b(III)V

    .line 1790245
    iget v1, p0, LX/BVB;->b:I

    .line 1790246
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790247
    iget v1, p0, LX/BVB;->c:I

    .line 1790248
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790249
    iget v1, p0, LX/BVB;->d:I

    .line 1790250
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790251
    iget v1, p0, LX/BVB;->e:I

    .line 1790252
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790253
    iget v1, p0, LX/BVB;->f:I

    .line 1790254
    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->b(III)V

    .line 1790255
    iget-boolean v1, p0, LX/BVB;->g:Z

    .line 1790256
    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->a(IZZ)V

    .line 1790257
    iget-object v1, p0, LX/BVB;->h:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/BVB;->h:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790258
    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790259
    iget-object v1, p0, LX/BVB;->i:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/BVB;->i:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v0, v1, v2}, LX/BEV;->a(LX/0eX;FF)I

    move-result v1

    .line 1790260
    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/0eX;->d(III)V

    .line 1790261
    iget v1, p0, LX/BVB;->j:F

    .line 1790262
    const/16 v3, 0x9

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v3, v1, v5, v6}, LX/0eX;->a(IFD)V

    .line 1790263
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v1

    .line 1790264
    move v1, v1

    .line 1790265
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1790266
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v0

    return-object v0
.end method

.method public final b(FF)LX/BVB;
    .locals 1

    .prologue
    .line 1790267
    iget-object v0, p0, LX/BVB;->i:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 1790268
    iget-object v0, p0, LX/BVB;->i:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 1790269
    return-object p0
.end method
