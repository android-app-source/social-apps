.class public LX/BRn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BRn;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11R;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/11R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0wM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784463
    iput-object p1, p0, LX/BRn;->a:LX/0Or;

    .line 1784464
    iput-object p2, p0, LX/BRn;->b:LX/0Or;

    .line 1784465
    return-void
.end method

.method public static a(LX/0QB;)LX/BRn;
    .locals 5

    .prologue
    .line 1784449
    sget-object v0, LX/BRn;->c:LX/BRn;

    if-nez v0, :cond_1

    .line 1784450
    const-class v1, LX/BRn;

    monitor-enter v1

    .line 1784451
    :try_start_0
    sget-object v0, LX/BRn;->c:LX/BRn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1784452
    if-eqz v2, :cond_0

    .line 1784453
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1784454
    new-instance v3, LX/BRn;

    const/16 v4, 0x2e4

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x5c8

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/BRn;-><init>(LX/0Or;LX/0Or;)V

    .line 1784455
    move-object v0, v3

    .line 1784456
    sput-object v0, LX/BRn;->c:LX/BRn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1784457
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1784458
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1784459
    :cond_1
    sget-object v0, LX/BRn;->c:LX/BRn;

    return-object v0

    .line 1784460
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1784461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
