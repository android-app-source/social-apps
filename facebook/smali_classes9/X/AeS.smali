.class public LX/AeS;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AeK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Aeq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Afl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Af0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AfY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Afx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AfU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AeO;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/AeU;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1697376
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1697377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AeS;->g:Ljava/util/List;

    .line 1697378
    return-void
.end method

.method public static b(LX/0QB;)LX/AeS;
    .locals 7

    .prologue
    .line 1697379
    new-instance v0, LX/AeS;

    invoke-direct {v0}, LX/AeS;-><init>()V

    .line 1697380
    const-class v1, LX/Aeq;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Aeq;

    const-class v2, LX/Afl;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Afl;

    const-class v3, LX/Af0;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Af0;

    const-class v4, LX/AfY;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AfY;

    const-class v5, LX/Afx;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Afx;

    const-class v6, LX/AfU;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AfU;

    .line 1697381
    iput-object v1, v0, LX/AeS;->a:LX/Aeq;

    iput-object v2, v0, LX/AeS;->b:LX/Afl;

    iput-object v3, v0, LX/AeS;->c:LX/Af0;

    iput-object v4, v0, LX/AeS;->d:LX/AfY;

    iput-object v5, v0, LX/AeS;->e:LX/Afx;

    iput-object v6, v0, LX/AeS;->f:LX/AfU;

    .line 1697382
    return-object v0
.end method

.method public static f(LX/AeS;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1697383
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x4b

    .line 1697384
    if-lez v0, :cond_0

    .line 1697385
    iget-object v1, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1697386
    invoke-virtual {p0, v2, v0}, LX/1OM;->d(II)V

    .line 1697387
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 12

    .prologue
    .line 1697388
    const/4 v3, 0x0

    .line 1697389
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1697390
    invoke-static {}, LX/AeN;->values()[LX/AeN;

    move-result-object v1

    aget-object v1, v1, p2

    .line 1697391
    sget-object v2, LX/AeR;->a:[I

    invoke-virtual {v1}, LX/AeN;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1697392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1697393
    :pswitch_0
    const v1, 0x7f0309fc

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697394
    iget-object v1, p0, LX/AeS;->c:LX/Af0;

    .line 1697395
    new-instance v4, LX/Aez;

    invoke-static {v1}, LX/215;->b(LX/0QB;)LX/215;

    move-result-object v6

    check-cast v6, LX/215;

    invoke-static {v1}, LX/Aev;->a(LX/0QB;)LX/Aev;

    move-result-object v7

    check-cast v7, LX/Aev;

    invoke-static {v1}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    invoke-static {v1}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v9

    check-cast v9, LX/AaZ;

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, LX/Aez;-><init>(Landroid/view/View;LX/215;LX/Aev;LX/1b4;LX/AaZ;)V

    .line 1697396
    move-object v0, v4

    .line 1697397
    :goto_0
    return-object v0

    .line 1697398
    :pswitch_1
    const v1, 0x7f030a2e

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697399
    iget-object v1, p0, LX/AeS;->b:LX/Afl;

    .line 1697400
    new-instance v4, LX/Afk;

    invoke-static {v1}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v6

    check-cast v6, LX/3HT;

    invoke-static {v1}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v7

    check-cast v7, LX/Ac6;

    invoke-static {v1}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    invoke-static {v1}, LX/215;->b(LX/0QB;)LX/215;

    move-result-object v9

    check-cast v9, LX/215;

    .line 1697401
    new-instance v11, LX/Acf;

    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v1}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v10

    check-cast v10, LX/AVU;

    invoke-direct {v11, v5, v10}, LX/Acf;-><init>(LX/0aG;LX/AVU;)V

    .line 1697402
    move-object v10, v11

    .line 1697403
    check-cast v10, LX/Acf;

    move-object v5, v0

    invoke-direct/range {v4 .. v10}, LX/Afk;-><init>(Landroid/view/View;LX/3HT;LX/Ac6;LX/1b4;LX/215;LX/Acf;)V

    .line 1697404
    move-object v0, v4

    .line 1697405
    goto :goto_0

    .line 1697406
    :pswitch_2
    const v1, 0x7f030a16

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1697407
    new-instance v0, LX/Afe;

    invoke-direct {v0, v1}, LX/Afe;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1697408
    :pswitch_3
    const v1, 0x7f030a14

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1697409
    new-instance v0, LX/Afc;

    invoke-direct {v0, v1}, LX/Afc;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1697410
    :pswitch_4
    const v1, 0x7f0309f8

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697411
    iget-object v1, p0, LX/AeS;->a:LX/Aeq;

    .line 1697412
    new-instance v5, LX/Aep;

    invoke-static {v1}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v2

    check-cast v2, LX/AaZ;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v1}, LX/Aem;->a(LX/0QB;)LX/Aem;

    move-result-object v4

    check-cast v4, LX/Aem;

    invoke-direct {v5, v0, v2, v3, v4}, LX/Aep;-><init>(Landroid/view/View;LX/AaZ;LX/0wM;LX/Aem;)V

    .line 1697413
    move-object v0, v5

    .line 1697414
    goto :goto_0

    .line 1697415
    :pswitch_5
    const v1, 0x7f030a2f

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1697416
    new-instance v0, LX/Afn;

    invoke-direct {v0, v1}, LX/Afn;-><init>(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1697417
    :pswitch_6
    const v1, 0x7f030a05

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697418
    iget-object v1, p0, LX/AeS;->d:LX/AfY;

    .line 1697419
    new-instance v2, Lcom/facebook/facecastdisplay/liveevent/donation/LiveDonationEventViewHolder;

    const/16 v3, 0x12cb

    invoke-static {v1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/facebook/facecastdisplay/liveevent/donation/LiveDonationEventViewHolder;-><init>(LX/0Or;Landroid/view/View;)V

    .line 1697420
    move-object v0, v2

    .line 1697421
    goto/16 :goto_0

    .line 1697422
    :pswitch_7
    const v1, 0x7f030a1f

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697423
    iget-object v1, p0, LX/AeS;->e:LX/Afx;

    .line 1697424
    new-instance v3, LX/Afw;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v2}, LX/Afw;-><init>(Landroid/view/View;LX/03V;)V

    .line 1697425
    move-object v0, v3

    .line 1697426
    goto/16 :goto_0

    .line 1697427
    :pswitch_8
    const v1, 0x7f030a00

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1697428
    iget-object v1, p0, LX/AeS;->f:LX/AfU;

    .line 1697429
    new-instance v3, LX/AfT;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v2

    check-cast v2, LX/0wM;

    invoke-direct {v3, v0, v2}, LX/AfT;-><init>(Landroid/view/View;LX/0wM;)V

    .line 1697430
    move-object v0, v3

    .line 1697431
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1697432
    check-cast p1, LX/AeK;

    .line 1697433
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeO;

    .line 1697434
    sget-object v1, LX/AeR;->a:[I

    invoke-interface {v0}, LX/AeO;->a()LX/AeN;

    move-result-object v2

    invoke-virtual {v2}, LX/AeN;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1697435
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1697436
    :pswitch_0
    iget-object v1, p0, LX/AeS;->h:LX/AeU;

    invoke-virtual {p1, v0, v1}, LX/AeK;->a(LX/AeO;LX/AeU;)V

    .line 1697437
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/AeO;)V
    .locals 1

    .prologue
    .line 1697438
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697439
    invoke-static {p0}, LX/AeS;->f(LX/AeS;)V

    .line 1697440
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/1OM;->j_(I)V

    .line 1697441
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1697442
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1697443
    iget-object v1, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1697444
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, LX/1OM;->d(II)V

    .line 1697445
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1697446
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeO;

    invoke-interface {v0}, LX/AeO;->a()LX/AeN;

    move-result-object v0

    invoke-virtual {v0}, LX/AeN;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1697447
    iget-object v0, p0, LX/AeS;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
