.class public final LX/AqR;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/AqR;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AqP;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/AqS;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717930
    const/4 v0, 0x0

    sput-object v0, LX/AqR;->a:LX/AqR;

    .line 1717931
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AqR;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1717912
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717913
    new-instance v0, LX/AqS;

    invoke-direct {v0}, LX/AqS;-><init>()V

    iput-object v0, p0, LX/AqR;->c:LX/AqS;

    .line 1717914
    return-void
.end method

.method private static a(LX/1De;II)LX/AqP;
    .locals 2

    .prologue
    .line 1717929
    new-instance v0, LX/AqQ;

    invoke-direct {v0}, LX/AqQ;-><init>()V

    invoke-static {p0, p1, p2, v0}, LX/AqR;->a(LX/1De;IILX/AqQ;)LX/AqP;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1De;IILX/AqQ;)LX/AqP;
    .locals 1

    .prologue
    .line 1717924
    sget-object v0, LX/AqR;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AqP;

    .line 1717925
    if-nez v0, :cond_0

    .line 1717926
    new-instance v0, LX/AqP;

    invoke-direct {v0}, LX/AqP;-><init>()V

    .line 1717927
    :cond_0
    invoke-static {v0, p0, p1, p2, p3}, LX/AqP;->a$redex0(LX/AqP;LX/1De;IILX/AqQ;)V

    .line 1717928
    return-object v0
.end method

.method public static c(LX/1De;)LX/AqP;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1717932
    invoke-static {p0, v0, v0}, LX/AqR;->a(LX/1De;II)LX/AqP;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/AqR;
    .locals 2

    .prologue
    .line 1717920
    const-class v1, LX/AqR;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/AqR;->a:LX/AqR;

    if-nez v0, :cond_0

    .line 1717921
    new-instance v0, LX/AqR;

    invoke-direct {v0}, LX/AqR;-><init>()V

    sput-object v0, LX/AqR;->a:LX/AqR;

    .line 1717922
    :cond_0
    sget-object v0, LX/AqR;->a:LX/AqR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1717923
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1717917
    check-cast p2, LX/AqQ;

    .line 1717918
    iget-object v0, p2, LX/AqQ;->a:LX/1X1;

    iget-object v1, p2, LX/AqQ;->b:LX/1X1;

    iget-object v2, p2, LX/AqQ;->c:LX/1X1;

    invoke-static {p1, v0, v1, v2}, LX/AqS;->a(LX/1De;LX/1X1;LX/1X1;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1717919
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717915
    invoke-static {}, LX/1dS;->b()V

    .line 1717916
    const/4 v0, 0x0

    return-object v0
.end method
