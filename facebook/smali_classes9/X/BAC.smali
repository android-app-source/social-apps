.class public LX/BAC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;
.implements LX/61G;
.implements LX/6Jv;
.implements LX/BAB;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/BYk;

.field private final c:LX/BZR;

.field public final d:LX/BA8;

.field private final e:LX/BAG;

.field private final f:LX/BZS;

.field private final g:LX/03V;

.field private h:Z

.field private i:Z

.field public j:Z

.field private k:LX/BA6;

.field private l:Ljava/lang/String;

.field public m:LX/61H;

.field private n:LX/6Js;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:I

.field public q:Z

.field private final r:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1752743
    const-class v0, LX/BAC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BAC;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/Context;LX/03V;LX/7ex;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p4    # LX/7ex;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 1752897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752898
    new-instance v0, LX/BAG;

    invoke-direct {v0}, LX/BAG;-><init>()V

    iput-object v0, p0, LX/BAC;->e:LX/BAG;

    .line 1752899
    iput-boolean v2, p0, LX/BAC;->h:Z

    .line 1752900
    iput-boolean v2, p0, LX/BAC;->i:Z

    .line 1752901
    iput-boolean v2, p0, LX/BAC;->j:Z

    .line 1752902
    iput-object v3, p0, LX/BAC;->k:LX/BA6;

    .line 1752903
    iput-object v3, p0, LX/BAC;->l:Ljava/lang/String;

    .line 1752904
    iput v1, p0, LX/BAC;->o:I

    .line 1752905
    iput v1, p0, LX/BAC;->p:I

    .line 1752906
    iput-boolean v2, p0, LX/BAC;->q:Z

    .line 1752907
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/BAC;->r:[F

    .line 1752908
    new-instance v0, LX/BZS;

    invoke-direct {v0}, LX/BZS;-><init>()V

    iput-object v0, p0, LX/BAC;->f:LX/BZS;

    .line 1752909
    new-instance v0, LX/BYk;

    iget-object v1, p0, LX/BAC;->f:LX/BZS;

    invoke-direct {v0, p4, v1}, LX/BYk;-><init>(LX/7ex;LX/BZS;)V

    iput-object v0, p0, LX/BAC;->b:LX/BYk;

    .line 1752910
    new-instance v0, LX/BZR;

    iget-object v1, p0, LX/BAC;->f:LX/BZS;

    invoke-direct {v0, v1, p1, p2}, LX/BZR;-><init>(LX/BZS;Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    iput-object v0, p0, LX/BAC;->c:LX/BZR;

    .line 1752911
    new-instance v0, LX/BA8;

    iget-object v1, p0, LX/BAC;->f:LX/BZS;

    invoke-direct {v0, v1}, LX/BA8;-><init>(LX/BZS;)V

    iput-object v0, p0, LX/BAC;->d:LX/BA8;

    .line 1752912
    iput-object p3, p0, LX/BAC;->g:LX/03V;

    .line 1752913
    iget-object v0, p0, LX/BAC;->r:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1752914
    new-instance v0, LX/BA9;

    invoke-direct {v0, p0}, LX/BA9;-><init>(LX/BAC;)V

    invoke-virtual {p0, v0}, LX/BAC;->a(LX/Avi;)V

    .line 1752915
    return-void
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    .line 1752916
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/BAC;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/BAC;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1752917
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1752918
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/BAC;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1752919
    :try_start_2
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    invoke-virtual {v0}, LX/BYk;->a()V
    :try_end_2
    .catch Lme/msqrd/sdk/nativecalls/effectsframework/EffectsFrameworkException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1752920
    :goto_1
    :try_start_3
    iget v0, p0, LX/BAC;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1752921
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    iget v1, p0, LX/BAC;->o:I

    iget v2, p0, LX/BAC;->p:I

    invoke-virtual {v0, v1, v2}, LX/BYk;->a(II)V

    .line 1752922
    :cond_2
    iget-object v0, p0, LX/BAC;->k:LX/BA6;

    invoke-virtual {p0, v0}, LX/BAC;->a(LX/BA6;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1752923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1752924
    :catch_0
    move-exception v0

    .line 1752925
    :try_start_4
    iget-object v1, p0, LX/BAC;->g:LX/03V;

    sget-object v2, LX/BAC;->a:Ljava/lang/String;

    const-string v3, "maybePrepareRenderer prepareGl failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private a(LX/BYj;)V
    .locals 2

    .prologue
    .line 1752875
    const-string v0, "FbMsqrdRenderer:setEffect"

    const v1, -0x313120e4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1752876
    :try_start_0
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    .line 1752877
    iget-object v1, v0, LX/BYk;->c:LX/BYj;

    if-ne p1, v1, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1752878
    :goto_0
    const v0, 0x71a412c6

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1752879
    return-void

    .line 1752880
    :catchall_0
    move-exception v0

    const v1, -0x600b2575

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1752881
    :cond_0
    if-eqz p1, :cond_1

    .line 1752882
    iget-object v1, p1, LX/BYj;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1752883
    if-eqz v1, :cond_1

    .line 1752884
    iget-object v1, p1, LX/BYj;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1752885
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1752886
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/BYk;->c:LX/BYj;

    goto :goto_0

    .line 1752887
    :cond_2
    iput-object p1, v0, LX/BYk;->c:LX/BYj;

    .line 1752888
    iget-object v1, v0, LX/BYk;->h:LX/BZS;

    .line 1752889
    iget-object p0, p1, LX/BYj;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1752890
    invoke-virtual {v1, p0}, LX/BZS;->a(Ljava/lang/String;)V

    .line 1752891
    invoke-static {v0}, LX/BYk;->c(LX/BYk;)V

    .line 1752892
    invoke-static {v0}, LX/BYk;->f(LX/BYk;)V

    goto :goto_0
.end method

.method private b(LX/6Js;)V
    .locals 1
    .param p1    # LX/6Js;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752926
    if-nez p1, :cond_0

    .line 1752927
    :goto_0
    return-void

    .line 1752928
    :cond_0
    sget-object v0, LX/7Sc;->MSQRD_EFFECT:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    goto :goto_0
.end method

.method private c(LX/6Js;)V
    .locals 1
    .param p1    # LX/6Js;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752929
    if-nez p1, :cond_0

    .line 1752930
    :goto_0
    return-void

    .line 1752931
    :cond_0
    sget-object v0, LX/7Sc;->MSQRD_EFFECT:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    goto :goto_0
.end method

.method private d(LX/6Js;)V
    .locals 1
    .param p1    # LX/6Js;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752932
    if-nez p1, :cond_0

    .line 1752933
    :goto_0
    return-void

    .line 1752934
    :cond_0
    sget-object v0, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1752935
    sget-object v0, LX/7Sc;->INPUT_FACING:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1752936
    sget-object v0, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1752937
    sget-object v0, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1752938
    iput-object v0, p0, LX/BAC;->k:LX/BA6;

    .line 1752939
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BAC;->h:Z

    .line 1752940
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->e(LX/6Js;)V

    .line 1752941
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/BAC;->a(LX/BYj;)V
    :try_end_0
    .catch Lme/msqrd/sdk/nativecalls/effectsframework/EffectsFrameworkException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1752942
    :goto_0
    return-void

    .line 1752943
    :catch_0
    move-exception v0

    .line 1752944
    iget-object v1, p0, LX/BAC;->g:LX/03V;

    sget-object v2, LX/BAC;->a:Ljava/lang/String;

    const-string v3, "setEffect(null) failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private e(LX/6Js;)V
    .locals 1
    .param p1    # LX/6Js;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752945
    if-nez p1, :cond_0

    .line 1752946
    :goto_0
    return-void

    .line 1752947
    :cond_0
    sget-object v0, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1752948
    sget-object v0, LX/7Sc;->INPUT_FACING:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1752949
    sget-object v0, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1752950
    sget-object v0, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    goto :goto_0
.end method

.method private declared-synchronized f()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1752951
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/BAC;->h:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/BAC;->n:LX/6Js;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/BAC;->e:LX/BAG;

    .line 1752952
    iget-boolean v3, v2, LX/BAG;->d:Z

    move v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1752953
    if-nez v2, :cond_1

    .line 1752954
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1752955
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/BAC;->e:LX/BAG;

    .line 1752956
    iget-object v3, v2, LX/BAG;->c:LX/7Sj;

    move-object v3, v3

    .line 1752957
    iget-object v2, p0, LX/BAC;->e:LX/BAG;

    .line 1752958
    iget-object v4, v2, LX/BAG;->b:LX/7Sl;

    move-object v4, v4

    .line 1752959
    iget-object v2, p0, LX/BAC;->e:LX/BAG;

    .line 1752960
    iget-object v5, v2, LX/BAG;->a:LX/7Sm;

    move-object v5, v5

    .line 1752961
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 1752962
    iget-object v2, p0, LX/BAC;->e:LX/BAG;

    const/4 v6, 0x0

    .line 1752963
    iput-boolean v6, v2, LX/BAG;->d:Z

    .line 1752964
    iget-object v2, v3, LX/7Sj;->a:LX/7Si;

    move-object v2, v2

    .line 1752965
    sget-object v6, LX/7Si;->FRONT:LX/7Si;

    if-ne v2, v6, :cond_2

    move v2, v0

    :goto_1
    invoke-virtual {v5}, LX/7Sm;->d()I

    move-result v6

    .line 1752966
    iget v7, v5, LX/7Sm;->b:I

    move v5, v7

    .line 1752967
    invoke-static {v2, v6, v5}, LX/7ev;->a(ZII)I

    move-result v2

    .line 1752968
    iget-object v5, p0, LX/BAC;->d:LX/BA8;

    .line 1752969
    iget v6, v4, LX/7Sl;->a:I

    move v6, v6

    .line 1752970
    iget v7, v4, LX/7Sl;->b:I

    move v4, v7

    .line 1752971
    iget-object v7, v3, LX/7Sj;->a:LX/7Si;

    move-object v3, v7

    .line 1752972
    sget-object v7, LX/7Si;->FRONT:LX/7Si;

    if-ne v3, v7, :cond_3

    :goto_2
    invoke-virtual {v5, v6, v4, v2, v0}, LX/BA8;->a(IIIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1752973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v2, v1

    .line 1752974
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1752975
    goto :goto_2
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1752976
    iget-boolean v0, p0, LX/BAC;->i:Z

    if-eqz v0, :cond_0

    .line 1752977
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    invoke-virtual {v0, p1, p2}, LX/BYk;->a(II)V

    .line 1752978
    iput v1, p0, LX/BAC;->o:I

    .line 1752979
    iput v1, p0, LX/BAC;->p:I

    .line 1752980
    :goto_0
    return-void

    .line 1752981
    :cond_0
    iput p1, p0, LX/BAC;->o:I

    .line 1752982
    iput p2, p0, LX/BAC;->p:I

    goto :goto_0
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1752893
    iget-object v0, p0, LX/BAC;->e:LX/BAG;

    const/4 v1, 0x1

    .line 1752894
    iput-boolean v1, v0, LX/BAG;->d:Z

    .line 1752895
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->b(LX/6Js;)V

    .line 1752896
    return-void
.end method

.method public final a(LX/61H;)V
    .locals 0

    .prologue
    .line 1752873
    iput-object p1, p0, LX/BAC;->m:LX/61H;

    .line 1752874
    return-void
.end method

.method public final a(LX/6Js;)V
    .locals 1
    .param p1    # LX/6Js;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752865
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    if-ne p1, v0, :cond_0

    .line 1752866
    :goto_0
    return-void

    .line 1752867
    :cond_0
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->c(LX/6Js;)V

    .line 1752868
    invoke-direct {p0, p1}, LX/BAC;->b(LX/6Js;)V

    .line 1752869
    iget-boolean v0, p0, LX/BAC;->h:Z

    if-eqz v0, :cond_1

    .line 1752870
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->e(LX/6Js;)V

    .line 1752871
    invoke-direct {p0, p1}, LX/BAC;->d(LX/6Js;)V

    .line 1752872
    :cond_1
    iput-object p1, p0, LX/BAC;->n:LX/6Js;

    goto :goto_0
.end method

.method public final a(LX/7Sb;)V
    .locals 9

    .prologue
    .line 1752822
    sget-object v0, LX/BAA;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1752823
    :goto_0
    return-void

    .line 1752824
    :pswitch_0
    check-cast p1, LX/BAE;

    .line 1752825
    iget-object v0, p1, LX/BAE;->a:LX/BA6;

    move-object v0, v0

    .line 1752826
    invoke-virtual {p0, v0}, LX/BAC;->a(LX/BA6;)V

    .line 1752827
    goto :goto_0

    .line 1752828
    :pswitch_1
    check-cast p1, LX/7Sk;

    .line 1752829
    iget-object v2, p1, LX/7Sk;->a:[B

    move-object v2, v2

    .line 1752830
    if-eqz v2, :cond_4

    .line 1752831
    iget-object v2, p0, LX/BAC;->d:LX/BA8;

    .line 1752832
    iget-object v3, p1, LX/7Sk;->a:[B

    move-object v3, v3

    .line 1752833
    invoke-virtual {v2, v3}, LX/BA8;->a([B)V

    .line 1752834
    :cond_0
    :goto_1
    goto :goto_0

    .line 1752835
    :pswitch_2
    iget-object v0, p0, LX/BAC;->e:LX/BAG;

    check-cast p1, LX/7Sl;

    .line 1752836
    if-eqz p1, :cond_1

    iget-object v1, v0, LX/BAG;->b:LX/7Sl;

    invoke-virtual {p1, v1}, LX/7Sl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1752837
    :cond_1
    :goto_2
    invoke-direct {p0}, LX/BAC;->f()V

    goto :goto_0

    .line 1752838
    :pswitch_3
    iget-object v0, p0, LX/BAC;->e:LX/BAG;

    check-cast p1, LX/7Sj;

    .line 1752839
    if-eqz p1, :cond_2

    iget-object v1, v0, LX/BAG;->c:LX/7Sj;

    invoke-virtual {p1, v1}, LX/7Sj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1752840
    :cond_2
    :goto_3
    invoke-direct {p0}, LX/BAC;->f()V

    goto :goto_0

    .line 1752841
    :pswitch_4
    iget-object v0, p0, LX/BAC;->e:LX/BAG;

    check-cast p1, LX/7Sm;

    .line 1752842
    if-eqz p1, :cond_3

    iget-object v1, v0, LX/BAG;->a:LX/7Sm;

    invoke-virtual {p1, v1}, LX/7Sm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1752843
    :cond_3
    :goto_4
    invoke-direct {p0}, LX/BAC;->f()V

    goto :goto_0

    .line 1752844
    :cond_4
    iget-object v2, p1, LX/7Sk;->b:[LX/6JH;

    move-object v2, v2

    .line 1752845
    if-eqz v2, :cond_0

    .line 1752846
    iget-object v2, p1, LX/7Sk;->b:[LX/6JH;

    move-object v2, v2

    .line 1752847
    array-length v2, v2

    if-lez v2, :cond_0

    .line 1752848
    iget-object v2, p1, LX/7Sk;->b:[LX/6JH;

    move-object v2, v2

    .line 1752849
    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 1752850
    iget-object v3, p0, LX/BAC;->d:LX/BA8;

    invoke-interface {v2}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-interface {v2}, LX/6JH;->c()I

    move-result v2

    .line 1752851
    if-eqz v4, :cond_5

    iget-boolean v5, v3, LX/BA8;->b:Z

    if-nez v5, :cond_6

    .line 1752852
    :cond_5
    :goto_5
    goto :goto_1

    .line 1752853
    :cond_6
    if-lez v2, :cond_7

    iget v5, v3, LX/BA8;->f:I

    if-eq v2, v5, :cond_7

    .line 1752854
    iput v2, v3, LX/BA8;->f:I

    .line 1752855
    invoke-static {v3}, LX/BA8;->b(LX/BA8;)V

    .line 1752856
    :cond_7
    iget-object v5, v3, LX/BA8;->a:LX/BZS;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    .line 1752857
    invoke-static {v5}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v7

    invoke-static {v7, v8, v4, v6}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->writeImageByteBuffer(JLjava/nio/ByteBuffer;I)V

    .line 1752858
    goto :goto_5

    .line 1752859
    :cond_8
    iput-object p1, v0, LX/BAG;->b:LX/7Sl;

    .line 1752860
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BAG;->d:Z

    goto :goto_2

    .line 1752861
    :cond_9
    iput-object p1, v0, LX/BAG;->c:LX/7Sj;

    .line 1752862
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BAG;->d:Z

    goto :goto_3

    .line 1752863
    :cond_a
    iput-object p1, v0, LX/BAG;->a:LX/7Sm;

    .line 1752864
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BAG;->d:Z

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/Avi;)V
    .locals 1

    .prologue
    .line 1752820
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    invoke-interface {v0, p1}, LX/BAB;->a(LX/Avi;)V

    .line 1752821
    return-void
.end method

.method public final a(LX/BA6;)V
    .locals 6

    .prologue
    .line 1752768
    iput-object p1, p0, LX/BAC;->k:LX/BA6;

    .line 1752769
    if-eqz p1, :cond_3

    .line 1752770
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BAC;->h:Z

    .line 1752771
    iget-object v0, p0, LX/BAC;->k:LX/BA6;

    .line 1752772
    iget-object v1, v0, LX/BA6;->m:Ljava/lang/String;

    move-object v0, v1

    .line 1752773
    iput-object v0, p0, LX/BAC;->l:Ljava/lang/String;

    .line 1752774
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->d(LX/6Js;)V

    .line 1752775
    iget-boolean v0, p0, LX/BAC;->i:Z

    if-eqz v0, :cond_2

    .line 1752776
    invoke-direct {p0}, LX/BAC;->f()V

    .line 1752777
    iget-object v0, p0, LX/BAC;->c:LX/BZR;

    .line 1752778
    iget-object v1, p1, LX/BA6;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1752779
    iget-object v2, p1, LX/BA6;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1752780
    iget-object v3, p1, LX/BA6;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1752781
    iget-boolean v4, p1, LX/BA6;->e:Z

    move v4, v4

    .line 1752782
    iput-object v1, v0, LX/BZR;->c:Ljava/lang/String;

    .line 1752783
    iput-object v2, v0, LX/BZR;->d:Ljava/lang/String;

    .line 1752784
    iput-object v3, v0, LX/BZR;->e:Ljava/lang/String;

    .line 1752785
    iput-boolean v4, v0, LX/BZR;->f:Z

    .line 1752786
    iget-boolean v0, p1, LX/BA6;->k:Z

    move v0, v0

    .line 1752787
    if-eqz v0, :cond_0

    .line 1752788
    iget-object v0, p0, LX/BAC;->d:LX/BA8;

    .line 1752789
    iget v1, p1, LX/BA6;->g:I

    move v1, v1

    .line 1752790
    iget v2, p1, LX/BA6;->h:I

    move v2, v2

    .line 1752791
    iget v3, p1, LX/BA6;->i:I

    move v3, v3

    .line 1752792
    iget-boolean v4, p1, LX/BA6;->j:Z

    move v4, v4

    .line 1752793
    invoke-virtual {v0, v1, v2, v3, v4}, LX/BA8;->a(IIIZ)V

    .line 1752794
    :cond_0
    iget-object v0, p1, LX/BA6;->l:LX/BAF;

    move-object v0, v0

    .line 1752795
    if-eqz v0, :cond_1

    .line 1752796
    iget-object v0, p1, LX/BA6;->l:LX/BAF;

    move-object v0, v0

    .line 1752797
    iget-object v1, p0, LX/BAC;->d:LX/BA8;

    .line 1752798
    iget-object v2, v1, LX/BA8;->c:LX/BA7;

    move-object v1, v2

    .line 1752799
    iput-object v1, v0, LX/BAF;->a:LX/BA7;

    .line 1752800
    :cond_1
    :try_start_0
    iget-object v0, p1, LX/BA6;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1752801
    new-instance v1, LX/BYj;

    invoke-direct {v1}, LX/BYj;-><init>()V

    .line 1752802
    iput-object v0, v1, LX/BYj;->a:Ljava/lang/String;

    .line 1752803
    move-object v0, v1

    .line 1752804
    invoke-direct {p0, v0}, LX/BAC;->a(LX/BYj;)V
    :try_end_0
    .catch Lme/msqrd/sdk/nativecalls/effectsframework/EffectsFrameworkException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1752805
    :goto_0
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    iget-object v1, p0, LX/BAC;->c:LX/BZR;

    .line 1752806
    iget-object v2, v0, LX/BYk;->d:LX/BZR;

    if-ne v2, v1, :cond_4

    .line 1752807
    :cond_2
    :goto_1
    return-void

    .line 1752808
    :catch_0
    move-exception v0

    .line 1752809
    iget-object v1, p0, LX/BAC;->g:LX/03V;

    sget-object v2, LX/BAC;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setEffect failed, file exist:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1752810
    iget-object v4, p1, LX/BA6;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1752811
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    move v4, v5

    .line 1752812
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1752813
    invoke-direct {p0}, LX/BAC;->e()V

    goto :goto_0

    .line 1752814
    :cond_3
    invoke-direct {p0}, LX/BAC;->e()V

    goto :goto_1

    .line 1752815
    :cond_4
    iget-object v2, v0, LX/BYk;->d:LX/BZR;

    if-eqz v2, :cond_5

    .line 1752816
    iget-object v2, v0, LX/BYk;->d:LX/BZR;

    invoke-virtual {v2}, LX/BZR;->b()V

    .line 1752817
    :cond_5
    iput-object v1, v0, LX/BYk;->d:LX/BZR;

    .line 1752818
    invoke-static {v0}, LX/BYk;->c(LX/BYk;)V

    .line 1752819
    invoke-static {v0}, LX/BYk;->f(LX/BYk;)V

    goto :goto_1
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1752763
    invoke-direct {p0}, LX/BAC;->a()V

    .line 1752764
    iget-boolean v0, p0, LX/BAC;->i:Z

    if-eqz v0, :cond_0

    .line 1752765
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    iget-boolean v1, p0, LX/BAC;->q:Z

    if-eqz v1, :cond_1

    iget-object v2, p0, LX/BAC;->r:[F

    :goto_0
    move-object v1, p1

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/BYk;->a([F[F[FJ)V

    .line 1752766
    :cond_0
    return-void

    :cond_1
    move-object v2, p2

    .line 1752767
    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1752752
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->e(LX/6Js;)V

    .line 1752753
    iget-object v0, p0, LX/BAC;->n:LX/6Js;

    invoke-direct {p0, v0}, LX/BAC;->c(LX/6Js;)V

    .line 1752754
    iget-boolean v0, p0, LX/BAC;->i:Z

    if-eqz v0, :cond_1

    .line 1752755
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    .line 1752756
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/BYk;->e:Z

    .line 1752757
    iget-object v1, v0, LX/BYk;->d:LX/BZR;

    if-eqz v1, :cond_0

    .line 1752758
    iget-object v1, v0, LX/BYk;->d:LX/BZR;

    invoke-virtual {v1}, LX/BZR;->b()V

    .line 1752759
    :cond_0
    iget-object v1, v0, LX/BYk;->h:LX/BZS;

    .line 1752760
    invoke-static {v1}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->releaseGl(J)V

    .line 1752761
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BAC;->i:Z

    .line 1752762
    return-void
.end method

.method public final b(LX/Avi;)V
    .locals 1

    .prologue
    .line 1752750
    iget-object v0, p0, LX/BAC;->b:LX/BYk;

    invoke-interface {v0, p1}, LX/BAB;->b(LX/Avi;)V

    .line 1752751
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1752749
    iget-boolean v0, p0, LX/BAC;->h:Z

    return v0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1752744
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1752745
    const-string v1, "filter_type"

    const-string v2, "msqrd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1752746
    iget-object v1, p0, LX/BAC;->l:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1752747
    const-string v1, "filter_id"

    iget-object v2, p0, LX/BAC;->l:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1752748
    :cond_0
    return-object v0
.end method
