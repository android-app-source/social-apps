.class public LX/CfW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/reaction/ReactionUtil;

.field private final b:LX/2iz;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ReactionUtil;LX/2iz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1926352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926353
    iput-object p1, p0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 1926354
    iput-object p2, p0, LX/CfW;->b:LX/2iz;

    .line 1926355
    return-void
.end method

.method public static a(LX/CfW;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Z)LX/2jY;
    .locals 6
    .param p0    # LX/CfW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1926332
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1926333
    :goto_0
    iget-object v0, p0, LX/CfW;->b:LX/2iz;

    invoke-virtual {v0, v4, p2}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v2

    .line 1926334
    if-nez p4, :cond_2

    .line 1926335
    new-instance v0, Lcom/facebook/reaction/ReactionSessionHelper$1;

    move-object v1, p0

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/ReactionSessionHelper$1;-><init>(LX/CfW;LX/2jY;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 1926336
    iput-object v0, v2, LX/2jY;->A:Ljava/lang/Runnable;

    .line 1926337
    :goto_1
    iget-object v0, p3, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1926338
    if-eqz v0, :cond_0

    .line 1926339
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1926340
    const-string v1, "place_name"

    .line 1926341
    iget-object v3, p3, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    move-object v3, v3

    .line 1926342
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926343
    iput-object v0, v2, LX/2jY;->x:Landroid/os/Bundle;

    .line 1926344
    :cond_0
    iput-object p3, v2, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    .line 1926345
    iget-object v0, p0, LX/CfW;->b:LX/2iz;

    invoke-virtual {v0, v4}, LX/2iz;->c(Ljava/lang/String;)V

    .line 1926346
    iget-object v0, p0, LX/CfW;->b:LX/2iz;

    invoke-virtual {v0, v4}, LX/2iz;->d(Ljava/lang/String;)V

    .line 1926347
    return-object v2

    :cond_1
    move-object v4, p1

    .line 1926348
    goto :goto_0

    .line 1926349
    :cond_2
    new-instance v0, Lcom/facebook/reaction/ReactionSessionHelper$2;

    invoke-direct {v0, p0}, Lcom/facebook/reaction/ReactionSessionHelper$2;-><init>(LX/CfW;)V

    .line 1926350
    iput-object v0, v2, LX/2jY;->A:Ljava/lang/Runnable;

    .line 1926351
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/CfW;
    .locals 1

    .prologue
    .line 1926331
    invoke-static {p0}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/CfW;
    .locals 3

    .prologue
    .line 1926287
    new-instance v2, LX/CfW;

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ReactionUtil;

    invoke-static {p0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v1

    check-cast v1, LX/2iz;

    invoke-direct {v2, v0, v1}, LX/CfW;-><init>(Lcom/facebook/reaction/ReactionUtil;LX/2iz;)V

    .line 1926288
    return-object v2
.end method

.method private static d(LX/CfW;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;
    .locals 2
    .param p0    # LX/CfW;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1926330
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, p2, v1}, LX/CfW;->a(LX/CfW;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Z)LX/2jY;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0v6;ILjava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;LX/0Ve;Ljava/util/concurrent/ExecutorService;Z)LX/2jY;
    .locals 18
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p5    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1926327
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, LX/CfW;->d(LX/CfW;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v17

    .line 1926328
    move-object/from16 v0, p0

    iget-object v5, v0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual/range {v17 .. v17}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v9

    const-wide/32 v14, 0x93a80

    new-instance v16, LX/CfP;

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/reaction/ReactionQueryParams;->p()Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, LX/CfP;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v8, p4

    move-object/from16 v10, p3

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move/from16 v13, p7

    invoke-virtual/range {v5 .. v16}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0v6;ILcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0Ve;Ljava/util/concurrent/ExecutorService;ZJLX/0zT;)V

    .line 1926329
    return-object v17
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1926322
    invoke-static {p0, p1, p2}, LX/CfW;->d(LX/CfW;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    .line 1926323
    iget-object v1, p0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 1926324
    iget-object v2, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1926325
    invoke-virtual {v1, p2, v2, p1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 1926326
    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/Long;)LX/2jY;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1926301
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, p2, v1}, LX/CfW;->a(LX/CfW;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Z)LX/2jY;

    move-result-object v0

    move-object v7, v0

    .line 1926302
    iget-object v0, p0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 1926303
    iget-object v1, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v1

    .line 1926304
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_0
    new-instance v6, LX/CfP;

    .line 1926305
    iget-object v1, p2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    move-object v1, v1

    .line 1926306
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, p1, v1}, LX/CfP;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p2

    move-object v3, p1

    .line 1926307
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/reaction/ReactionUtil;->d(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1926308
    :goto_1
    return-object v7

    .line 1926309
    :cond_0
    const-wide/32 v4, 0x93a80

    goto :goto_0

    .line 1926310
    :cond_1
    iget-object v8, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v9, 0x1e000f

    invoke-virtual {v8, v9, v2, v3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1926311
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/reaction/ReactionUtil;->c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v8

    .line 1926312
    sget-object v9, LX/0zS;->b:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    const/4 p0, 0x1

    .line 1926313
    iput-boolean p0, v9, LX/0zO;->p:Z

    .line 1926314
    move-object v9, v9

    .line 1926315
    invoke-virtual {v9, v4, v5}, LX/0zO;->a(J)LX/0zO;

    .line 1926316
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 1926317
    iput-object v3, v8, LX/0zO;->z:Ljava/lang/String;

    .line 1926318
    :cond_2
    if-eqz v6, :cond_3

    .line 1926319
    invoke-virtual {v8, v6}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 1926320
    :cond_3
    iget-object v9, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    iget-object p0, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p0, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    new-instance p0, LX/Cfa;

    invoke-direct {p0, v0, v2}, LX/Cfa;-><init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V

    invoke-virtual {v9, v2, v8, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1926321
    iget-object v8, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v9, LX/2jX;

    invoke-direct {v9, v2, v1}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v8, v9}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1926296
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/CfW;->a(LX/CfW;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Z)LX/2jY;

    move-result-object v0

    move-object v0, v0

    .line 1926297
    iget-object v1, p0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 1926298
    iget-object v2, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1926299
    invoke-virtual {v1, p3, v2, p2}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 1926300
    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1926289
    invoke-static {p0, p1, p2}, LX/CfW;->d(LX/CfW;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v9

    .line 1926290
    iget-object v1, p0, LX/CfW;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 1926291
    iget-object v0, v9, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v0

    .line 1926292
    sget-object v5, LX/0zS;->d:LX/0zS;

    const-wide/32 v6, 0x93a80

    new-instance v8, LX/CfP;

    .line 1926293
    iget-object v0, p2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    move-object v0, v0

    .line 1926294
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, p1, v0}, LX/CfP;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p2

    move-object v4, p1

    invoke-virtual/range {v1 .. v8}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0zS;JLX/0zT;)V

    .line 1926295
    return-object v9
.end method
