.class public LX/AWK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/AWL;

.field public final b:LX/5Pc;

.field public final c:[F

.field public d:LX/5Pf;

.field public e:LX/5PO;

.field public f:LX/7S6;

.field public g:LX/7SB;

.field public h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

.field public i:LX/7Se;

.field public j:LX/BAC;

.field private k:LX/8Fw;

.field public l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;


# direct methods
.method public constructor <init>(LX/5Pc;LX/1b4;LX/7SH;LX/7SC;Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;LX/BAD;LX/BA4;LX/8Fx;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1682703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682704
    iput-object p1, p0, LX/AWK;->b:LX/5Pc;

    .line 1682705
    invoke-virtual {p3, v3}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v0

    iput-object v0, p0, LX/AWK;->h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1682706
    new-instance v0, LX/7S6;

    invoke-direct {v0}, LX/7S6;-><init>()V

    iput-object v0, p0, LX/AWK;->f:LX/7S6;

    .line 1682707
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/AWK;->c:[F

    .line 1682708
    iget-object v0, p0, LX/AWK;->c:[F

    invoke-static {v0, v4}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1682709
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1682710
    iget-object v1, p0, LX/AWK;->f:LX/7S6;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682711
    iget-object v1, p0, LX/AWK;->h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682712
    invoke-virtual {p7}, LX/BA4;->a()LX/7ex;

    move-result-object v1

    invoke-virtual {p6, v1}, LX/BAD;->a(LX/7ex;)LX/BAC;

    move-result-object v1

    iput-object v1, p0, LX/AWK;->j:LX/BAC;

    .line 1682713
    iget-object v1, p0, LX/AWK;->j:LX/BAC;

    const/4 v2, 0x1

    .line 1682714
    iput-boolean v2, v1, LX/BAC;->q:Z

    .line 1682715
    iget-object v1, p0, LX/AWK;->j:LX/BAC;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682716
    iget-object v1, p2, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x34f

    const/4 p3, 0x0

    invoke-virtual {v1, v2, p3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v1, v1

    .line 1682717
    new-instance p2, LX/7SB;

    invoke-static {p4}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-direct {p2, v2, v1}, LX/7SB;-><init>(LX/0So;Z)V

    .line 1682718
    move-object v1, p2

    .line 1682719
    iput-object v1, p0, LX/AWK;->g:LX/7SB;

    .line 1682720
    iget-object v1, p0, LX/AWK;->g:LX/7SB;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682721
    new-instance v1, LX/7Se;

    invoke-direct {v1}, LX/7Se;-><init>()V

    iput-object v1, p0, LX/AWK;->i:LX/7Se;

    .line 1682722
    iget-object v1, p0, LX/AWK;->i:LX/7Se;

    invoke-virtual {v1, v4, v3}, LX/7Se;->a(ZLX/5Pc;)V

    .line 1682723
    invoke-virtual {p8, v3}, LX/8Fx;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)LX/8Fw;

    move-result-object v1

    iput-object v1, p0, LX/AWK;->k:LX/8Fw;

    .line 1682724
    iget-object v1, p0, LX/AWK;->k:LX/8Fw;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682725
    iput-object p5, p0, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    .line 1682726
    iget-object v1, p0, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682727
    new-instance v1, LX/AWL;

    invoke-direct {v1, v0, p1}, LX/AWL;-><init>(Ljava/util/List;LX/5Pc;)V

    iput-object v1, p0, LX/AWK;->a:LX/AWL;

    .line 1682728
    return-void
.end method


# virtual methods
.method public final a(IIIIIF)V
    .locals 2

    .prologue
    .line 1682694
    iget-object v0, p0, LX/AWK;->a:LX/AWL;

    invoke-virtual {v0, p1, p2, p3, p6}, LX/AWL;->a(IIIF)V

    .line 1682695
    iget-object v0, p0, LX/AWK;->j:LX/BAC;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWK;->d:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1682696
    new-instance v0, LX/5PO;

    invoke-direct {v0, p4, p5}, LX/5PO;-><init>(II)V

    iput-object v0, p0, LX/AWK;->e:LX/5PO;

    .line 1682697
    iget-object v0, p0, LX/AWK;->e:LX/5PO;

    invoke-virtual {v0}, LX/5PO;->a()V

    .line 1682698
    iget-object v0, p0, LX/AWK;->i:LX/7Se;

    iget-object v1, p0, LX/AWK;->e:LX/5PO;

    iget-object v1, v1, LX/5PO;->d:LX/5Pf;

    .line 1682699
    iput-object v1, v0, LX/7Se;->e:LX/5Pf;

    .line 1682700
    :cond_0
    iget-object v0, p0, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    if-eqz v0, :cond_1

    .line 1682701
    iget-object v0, p0, LX/AWK;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-virtual {v0, p4, p5}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a(II)V

    .line 1682702
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1682689
    iget-object v0, p0, LX/AWK;->h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1682690
    invoke-static {v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d(Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;)V

    .line 1682691
    iget-object p0, v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1682692
    iput-object p1, p0, Lcom/facebook/videocodec/effects/model/OverlayData;->b:Landroid/graphics/Bitmap;

    .line 1682693
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1682729
    iget-object v0, p0, LX/AWK;->h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v0, p1}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(Landroid/net/Uri;)V

    .line 1682730
    return-void
.end method

.method public final b([F)V
    .locals 4

    .prologue
    .line 1682674
    iget-object v0, p0, LX/AWK;->a:LX/AWL;

    iget-object v1, p0, LX/AWK;->d:LX/5Pf;

    .line 1682675
    iget-object v2, v0, LX/AWL;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/61B;

    .line 1682676
    iget-boolean p0, v0, LX/AWL;->f:Z

    if-eqz p0, :cond_1

    instance-of p0, v2, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    if-eqz p0, :cond_0

    .line 1682677
    :cond_1
    const/4 p0, 0x0

    invoke-virtual {v0, p1, v1, v2, p0}, LX/AWL;->a([FLX/5Pf;LX/61B;Z)V

    goto :goto_0

    .line 1682678
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1682679
    iget-object v0, p0, LX/AWK;->a:LX/AWL;

    .line 1682680
    iget-object v1, v0, LX/AWL;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/61B;

    .line 1682681
    invoke-interface {v1}, LX/61B;->b()V

    goto :goto_0

    .line 1682682
    :cond_0
    iget-object v0, p0, LX/AWK;->d:LX/5Pf;

    if-eqz v0, :cond_1

    .line 1682683
    iget-object v0, p0, LX/AWK;->d:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1682684
    const/4 v0, 0x0

    iput-object v0, p0, LX/AWK;->d:LX/5Pf;

    .line 1682685
    :cond_1
    iget-object v0, p0, LX/AWK;->i:LX/7Se;

    invoke-virtual {v0}, LX/7Se;->b()V

    .line 1682686
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1682687
    iget-object v0, p0, LX/AWK;->i:LX/7Se;

    const-wide/16 v4, 0x0

    move-object v2, v1

    move-object v3, v1

    invoke-virtual/range {v0 .. v5}, LX/7Se;->a([F[F[FJ)V

    .line 1682688
    return-void
.end method
