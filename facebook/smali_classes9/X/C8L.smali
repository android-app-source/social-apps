.class public final LX/C8L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/C8M;

.field public final synthetic c:LX/C8O;

.field public final synthetic d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;LX/C8O;)V
    .locals 0

    .prologue
    .line 1852635
    iput-object p1, p0, LX/C8L;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C8L;->b:LX/C8M;

    iput-object p4, p0, LX/C8L;->c:LX/C8O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1852636
    iget-object v0, p0, LX/C8L;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8L;->c:LX/C8O;

    invoke-virtual {v1}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/content/Context;)V

    .line 1852637
    iget-object v0, p0, LX/C8L;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8L;->c:LX/C8O;

    iget-object v2, p0, LX/C8L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8L;->b:LX/C8M;

    .line 1852638
    invoke-virtual {v1}, LX/C8O;->e()V

    .line 1852639
    invoke-static {v0, v1, v3, v2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1852640
    const/4 p0, 0x0

    .line 1852641
    iget-object v0, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852642
    iget-object v0, v1, LX/C8O;->g:LX/C8D;

    iget-object v2, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    .line 1852643
    invoke-static {p0}, LX/C8D;->a(Landroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    .line 1852644
    iget-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1852645
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1852646
    check-cast p1, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1852647
    if-nez p1, :cond_0

    .line 1852648
    iget-object v1, p0, LX/C8L;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v0, p0, LX/C8L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852649
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1852650
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    iget-object v3, p0, LX/C8L;->b:LX/C8M;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/graphql/enums/StoryVisibility;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C8M;)V

    .line 1852651
    :goto_0
    return-void

    .line 1852652
    :cond_0
    iget-object v0, p0, LX/C8L;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8L;->c:LX/C8O;

    iget-object v2, p0, LX/C8L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8L;->b:LX/C8M;

    .line 1852653
    invoke-virtual {v1}, LX/C8O;->e()V

    .line 1852654
    invoke-static {v0, v1, v3, v2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1852655
    const/4 p0, 0x0

    .line 1852656
    iget-object v0, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852657
    iget-object v0, v1, LX/C8O;->g:LX/C8D;

    iget-object v2, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    .line 1852658
    invoke-static {p0}, LX/C8D;->a(Landroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    .line 1852659
    iget-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1852660
    goto :goto_0
.end method
