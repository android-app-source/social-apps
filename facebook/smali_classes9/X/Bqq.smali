.class public final LX/Bqq;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/Br2;

.field public final synthetic c:Z

.field public final synthetic d:LX/1Pq;

.field public final synthetic e:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic f:LX/Bqw;


# direct methods
.method public constructor <init>(LX/Bqw;Lcom/facebook/graphql/model/GraphQLStory;LX/Br2;ZLX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1825433
    iput-object p1, p0, LX/Bqq;->f:LX/Bqw;

    iput-object p2, p0, LX/Bqq;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/Bqq;->b:LX/Br2;

    iput-boolean p4, p0, LX/Bqq;->c:Z

    iput-object p5, p0, LX/Bqq;->d:LX/1Pq;

    iput-object p6, p0, LX/Bqq;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1825434
    iget-object v0, p0, LX/Bqq;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bqq;->f:LX/Bqw;

    iget-object v0, v0, LX/Bqw;->i:LX/2Pc;

    iget-object v1, p0, LX/Bqq;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Pc;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1825435
    iget-object v0, p0, LX/Bqq;->f:LX/Bqw;

    iget-object v0, v0, LX/Bqw;->i:LX/2Pc;

    iget-object v1, p0, LX/Bqq;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Pc;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1825436
    iget-object v1, p0, LX/Bqq;->f:LX/Bqw;

    iget-object v1, v1, LX/Bqw;->i:LX/2Pc;

    invoke-virtual {v1, v0}, LX/2Pc;->a(Ljava/lang/String;)V

    .line 1825437
    iget-object v1, p0, LX/Bqq;->f:LX/Bqw;

    iget-object v1, v1, LX/Bqw;->i:LX/2Pc;

    invoke-virtual {v1, v0}, LX/2Pc;->b(Ljava/lang/String;)V

    .line 1825438
    :cond_0
    iget-object v0, p0, LX/Bqq;->b:LX/Br2;

    iget-object v1, p0, LX/Bqq;->f:LX/Bqw;

    iget-object v2, p0, LX/Bqq;->a:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iget-boolean v4, p0, LX/Bqq;->c:Z

    invoke-virtual {v1, v2, v3, v4}, LX/Bqw;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/Br2;->a(I)V

    .line 1825439
    iget-object v0, p0, LX/Bqq;->d:LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Bqq;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1825440
    return-void
.end method
