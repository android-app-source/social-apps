.class public LX/AjX;
.super LX/0rB;
.source ""


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/81K;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708133
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->g:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {p0, v0, p1}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;)V

    .line 1708134
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 4

    .prologue
    .line 1708131
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1708132
    new-instance v2, Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->g:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v2, v0, v1}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    return-object v2
.end method

.method public final a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1708130
    const-string v0, "fragment_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
