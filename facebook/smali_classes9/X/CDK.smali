.class public final LX/CDK;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/361;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CDL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/361",
            "<TE;>.OlderRichVideoAttachmentComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/361;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/361;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1859020
    iput-object p1, p0, LX/CDK;->b:LX/361;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1859021
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CDK;->c:[Ljava/lang/String;

    .line 1859022
    iput v3, p0, LX/CDK;->d:I

    .line 1859023
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CDK;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CDK;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CDK;LX/1De;IILX/CDL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/361",
            "<TE;>.OlderRichVideoAttachmentComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1858994
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1858995
    iput-object p4, p0, LX/CDK;->a:LX/CDL;

    .line 1858996
    iget-object v0, p0, LX/CDK;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1858997
    return-void
.end method


# virtual methods
.method public final a(LX/1Pe;)LX/CDK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/361",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1858998
    iget-object v0, p0, LX/CDK;->a:LX/CDL;

    iput-object p1, v0, LX/CDL;->b:LX/1Pe;

    .line 1858999
    iget-object v0, p0, LX/CDK;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859000
    return-object p0
.end method

.method public final a(LX/3EE;)LX/CDK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            ")",
            "LX/361",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1859001
    iget-object v0, p0, LX/CDK;->a:LX/CDL;

    iput-object p1, v0, LX/CDL;->a:LX/3EE;

    .line 1859002
    iget-object v0, p0, LX/CDK;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1859003
    return-object p0
.end method

.method public final a(Landroid/view/View$OnClickListener;)LX/CDK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/361",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1859004
    iget-object v0, p0, LX/CDK;->a:LX/CDL;

    iput-object p1, v0, LX/CDL;->d:Landroid/view/View$OnClickListener;

    .line 1859005
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1859006
    invoke-super {p0}, LX/1X5;->a()V

    .line 1859007
    const/4 v0, 0x0

    iput-object v0, p0, LX/CDK;->a:LX/CDL;

    .line 1859008
    iget-object v0, p0, LX/CDK;->b:LX/361;

    iget-object v0, v0, LX/361;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1859009
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/361;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1859010
    iget-object v1, p0, LX/CDK;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CDK;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CDK;->d:I

    if-ge v1, v2, :cond_2

    .line 1859011
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1859012
    :goto_0
    iget v2, p0, LX/CDK;->d:I

    if-ge v0, v2, :cond_1

    .line 1859013
    iget-object v2, p0, LX/CDK;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1859014
    iget-object v2, p0, LX/CDK;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1859015
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1859016
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1859017
    :cond_2
    iget-object v0, p0, LX/CDK;->a:LX/CDL;

    .line 1859018
    invoke-virtual {p0}, LX/CDK;->a()V

    .line 1859019
    return-object v0
.end method
