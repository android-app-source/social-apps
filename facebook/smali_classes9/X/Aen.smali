.class public LX/Aen;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AeO;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1698139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1698140
    iput-object p1, p0, LX/Aen;->a:Ljava/lang/String;

    .line 1698141
    iput p2, p0, LX/Aen;->b:I

    .line 1698142
    iput-object p3, p0, LX/Aen;->c:Ljava/lang/String;

    .line 1698143
    iput-object p4, p0, LX/Aen;->d:Ljava/lang/String;

    .line 1698144
    iput-object p5, p0, LX/Aen;->e:Ljava/lang/String;

    .line 1698145
    iput-object p6, p0, LX/Aen;->f:Ljava/lang/String;

    .line 1698146
    iput-object p7, p0, LX/Aen;->g:Ljava/lang/String;

    .line 1698147
    iput-object p8, p0, LX/Aen;->h:Ljava/lang/String;

    .line 1698148
    iput-object p9, p0, LX/Aen;->i:Ljava/lang/String;

    .line 1698149
    iput-boolean p10, p0, LX/Aen;->j:Z

    .line 1698150
    return-void
.end method

.method public static a(LX/15i;I)LX/Aen;
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFromFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v1, 0x9

    const/16 v5, 0x8

    const/4 v3, 0x6

    const/4 v4, 0x0

    .line 1698134
    invoke-virtual {p0, p1, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1698135
    :cond_0
    const/4 v0, 0x0

    .line 1698136
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/Aen;

    invoke-virtual {p0, p1, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {p0, p1, v2}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {p0, p1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, p1, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {p0, p1, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {p0, p1, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {p0, p1, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {p0, p1, v10}, LX/15i;->h(II)Z

    move-result v10

    invoke-direct/range {v0 .. v10}, LX/Aen;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Aen;
    .locals 11

    .prologue
    .line 1698138
    new-instance v0, LX/Aen;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, LX/Aen;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/AeN;
    .locals 1

    .prologue
    .line 1698137
    sget-object v0, LX/AeN;->LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

    return-object v0
.end method
