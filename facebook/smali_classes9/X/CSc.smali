.class public LX/CSc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/CSY;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:LX/CSb;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1894839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894840
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CSc;->a:Ljava/util/HashMap;

    .line 1894841
    return-void
.end method

.method public static b(LX/CSY;)Lorg/json/JSONObject;
    .locals 9

    .prologue
    .line 1894842
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1894843
    :try_start_0
    const-string v1, "form_field_id"

    iget-object v2, p0, LX/CSY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894844
    const-string v1, "disable_autofill"

    iget-boolean v2, p0, LX/CSY;->b:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1894845
    const-string v1, "value"

    .line 1894846
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1894847
    iget-object v2, p0, LX/CSY;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1894848
    iget-object v3, p0, LX/CSY;->c:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CSZ;

    .line 1894849
    sget-object v6, LX/CSX;->a:[I

    iget-object v7, v3, LX/CSZ;->a:LX/CSa;

    invoke-virtual {v7}, LX/CSa;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1894850
    const/4 v2, 0x0

    const-string v3, "Unhandled case"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    .line 1894851
    :pswitch_0
    new-instance v6, Lorg/json/JSONArray;

    invoke-virtual {v3}, LX/CSZ;->b()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v6, v3}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 1894852
    :pswitch_1
    invoke-virtual {v3}, LX/CSZ;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 1894853
    :pswitch_2
    invoke-virtual {v3}, LX/CSZ;->c()Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v3

    .line 1894854
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 1894855
    const-string v7, "card_association"

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894856
    const-string v7, "credential_id"

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894857
    const-string v7, "expiration_month"

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894858
    const-string v7, "expiration_year"

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894859
    const-string v7, "last_four_digits"

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v7, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1894860
    invoke-virtual {v4, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 1894861
    :pswitch_3
    invoke-virtual {v3}, LX/CSZ;->d()Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_0

    .line 1894862
    :pswitch_4
    invoke-virtual {v3}, LX/CSZ;->e()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_0

    .line 1894863
    :cond_0
    move-object v2, v4

    .line 1894864
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1894865
    :goto_1
    return-object v0

    :catch_0
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/CSb;)LX/CSb;
    .locals 2

    .prologue
    .line 1894866
    iput-object p1, p0, LX/CSc;->c:Ljava/lang/String;

    .line 1894867
    iget-object v0, p0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1894868
    if-nez p2, :cond_0

    .line 1894869
    new-instance v1, LX/CSb;

    invoke-direct {v1, p0}, LX/CSb;-><init>(LX/CSc;)V

    .line 1894870
    :goto_0
    instance-of v0, v1, LX/CSb;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, v1

    .line 1894871
    check-cast v0, LX/CSb;

    iput-object v0, p0, LX/CSc;->b:LX/CSb;

    .line 1894872
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1894873
    iget-object v0, p0, LX/CSc;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894874
    iget-object v0, p0, LX/CSc;->b:LX/CSb;

    .line 1894875
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, v0, LX/CSb;->b:LX/CSc;

    iget-object v2, v2, LX/CSc;->a:Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, LX/CSb;->a:Ljava/util/HashMap;

    .line 1894876
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1894877
    :try_start_0
    iget-object v0, p0, LX/CSc;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1894878
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 1894879
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CSY;

    .line 1894880
    invoke-static {v1}, LX/CSc;->b(LX/CSY;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1894881
    :catch_0
    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1894882
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
