.class public final LX/Ccl;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Ccm;

.field public final synthetic c:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;Landroid/content/Context;LX/Ccm;)V
    .locals 0

    .prologue
    .line 1921608
    iput-object p1, p0, LX/Ccl;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    iput-object p2, p0, LX/Ccl;->a:Landroid/content/Context;

    iput-object p3, p0, LX/Ccl;->b:LX/Ccm;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1921609
    iget-object v0, p0, LX/Ccl;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    iget-object v0, v0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->g:LX/17Y;

    iget-object v1, p0, LX/Ccl;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->dn:Ljava/lang/String;

    const-string v3, "/feed/panel/advanced_prep/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1921610
    iget-object v1, p0, LX/Ccl;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    iget-object v1, v1, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Ccl;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1921611
    iget-object v0, p0, LX/Ccl;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    iget-object v0, v0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->a:LX/1WN;

    const-string v1, "warning_screen_content_filter_prefs_tapped"

    iget-object v2, p0, LX/Ccl;->b:LX/Ccm;

    iget-boolean v2, v2, LX/Ccm;->c:Z

    iget-object v3, p0, LX/Ccl;->b:LX/Ccm;

    iget-object v3, v3, LX/Ccm;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 1921612
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1921613
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1921614
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1921615
    iget-object v0, p0, LX/Ccl;->a:Landroid/content/Context;

    const v1, 0x7f0a00d5

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1921616
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1921617
    return-void
.end method
