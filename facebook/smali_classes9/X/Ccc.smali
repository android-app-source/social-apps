.class public final LX/Ccc;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)V
    .locals 0

    .prologue
    .line 1921441
    iput-object p1, p0, LX/Ccc;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;

    iput-object p2, p0, LX/Ccc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Ccc;->b:LX/1Pn;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1921442
    iget-object v0, p0, LX/Ccc;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->f:LX/1WM;

    iget-object v1, p0, LX/Ccc;->c:Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;

    iget-object v2, p0, LX/Ccc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v2}, Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;->c(Lcom/facebook/photos/warning/rows/ObjectionableContentMessageComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/1WM;->b(Ljava/lang/String;Z)V

    .line 1921443
    iget-object v0, p0, LX/Ccc;->b:LX/1Pn;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Ccc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1921444
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1921445
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1921446
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1921447
    iget-object v0, p0, LX/Ccc;->b:LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00d9

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1921448
    return-void
.end method
