.class public LX/Baj;
.super Landroid/graphics/drawable/ShapeDrawable;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1799486
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1799487
    invoke-direct {p0, p1}, LX/Baj;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Baj;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1799488
    return-void
.end method

.method private a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v1, 0x0

    .line 1799489
    new-array v0, v3, [I

    aput v1, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0a0417

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0a0418

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0a0419

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f0a041a

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x7f0a041b

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 1799490
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    .line 1799491
    new-instance v2, LX/Bai;

    invoke-direct {v2, p0, v0, v1}, LX/Bai;-><init>(LX/Baj;[I[F)V

    return-object v2

    nop

    :array_0
    .array-data 4
        0x0
        0x3df5c28f    # 0.12f
        0x3f28f5c3    # 0.66f
        0x3f3ae148    # 0.73f
        0x3f51eb85    # 0.82f
        0x3f800000    # 1.0f
    .end array-data
.end method
