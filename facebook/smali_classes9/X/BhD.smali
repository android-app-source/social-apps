.class public LX/BhD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;",
        "LX/BgW;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/17Y;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/BgQ;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/BgQ;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/crowdsourcing/suggestedits/gk/IsCameraGlyphEnabled;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/crowdsourcing/suggestedits/gk/IsPendingEditsViewEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/BgQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808926
    iput-object p1, p0, LX/BhD;->a:LX/0Or;

    .line 1808927
    iput-object p2, p0, LX/BhD;->b:LX/0Or;

    .line 1808928
    iput-object p3, p0, LX/BhD;->c:LX/17Y;

    .line 1808929
    iput-object p4, p0, LX/BhD;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1808930
    iput-object p5, p0, LX/BhD;->e:LX/BgQ;

    .line 1808931
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;LX/BgW;LX/BgS;)LX/BgW;
    .locals 4

    .prologue
    .line 1808905
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getPageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v1

    invoke-static {v1}, LX/Bgb;->c(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1808906
    :goto_0
    return-object p1

    .line 1808907
    :cond_0
    invoke-virtual {p1}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getPageName()Ljava/lang/String;

    move-result-object v1

    .line 1808908
    invoke-static {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v3

    .line 1808909
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1808910
    :cond_1
    :goto_1
    move-object v0, v0

    .line 1808911
    invoke-interface {p2, v0}, LX/BgS;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1808912
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v2

    invoke-static {v2}, LX/Bgb;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808913
    if-nez v2, :cond_3

    .line 1808914
    new-instance v2, LX/97m;

    invoke-direct {v2}, LX/97m;-><init>()V

    invoke-virtual {v2}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808915
    :cond_3
    invoke-static {v2, v1}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808916
    new-instance p0, LX/97l;

    invoke-direct {p0}, LX/97l;-><init>()V

    .line 1808917
    iput-object v2, p0, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1808918
    move-object v2, p0

    .line 1808919
    invoke-virtual {v2}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1808920
    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object p0

    invoke-static {p0, v2}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;LX/0Px;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v2

    .line 1808921
    new-instance p0, LX/97z;

    invoke-direct {p0}, LX/97z;-><init>()V

    invoke-static {v3}, LX/97z;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)LX/97z;

    move-result-object v3

    .line 1808922
    iput-object v2, v3, LX/97z;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1808923
    move-object v2, v3

    .line 1808924
    invoke-virtual {v2}, LX/97z;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808932
    sget-object v0, LX/BeE;->PAGE_HEADER:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808902
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808903
    const v1, 0x7f03141a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    .line 1808904
    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808901
    check-cast p1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    check-cast p2, LX/BgW;

    invoke-static {p1, p2, p3}, LX/BhD;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;LX/BgW;LX/BgS;)LX/BgW;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808868
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    move-object v2, p2

    check-cast v2, LX/BgW;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    move-object v6, p8

    .line 1808869
    iget-object p0, v0, LX/BhD;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1808870
    const p0, 0x7f0207b3

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setAddPhotoImageResource(I)V

    .line 1808871
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->a()I

    move-result p1

    .line 1808872
    if-nez p1, :cond_4

    .line 1808873
    const/4 p2, 0x0

    .line 1808874
    :goto_0
    move-object p0, p2

    .line 1808875
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    new-instance p1, LX/BhA;

    invoke-direct {p1, v0, v1, v6}, LX/BhA;-><init>(LX/BhD;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a(Landroid/text/Spanned;Landroid/view/View$OnClickListener;)V

    .line 1808876
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object p0

    invoke-static {p0}, LX/Bgb;->c(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setPageName(Ljava/lang/String;)V

    .line 1808877
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->b()V

    .line 1808878
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object p0

    .line 1808879
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a()Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x1

    :goto_1
    move p0, p1

    .line 1808880
    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setPageNameEditable(Z)V

    .line 1808881
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object p0

    invoke-static {p0}, LX/Bgb;->d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setPhoto(Ljava/lang/String;)V

    .line 1808882
    iget-object p0, v2, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    move-object p0, p0

    .line 1808883
    iget-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    invoke-static {p1}, LX/Bgb;->d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    const/4 p1, 0x1

    :goto_2
    move p0, p1

    .line 1808884
    if-eqz p0, :cond_3

    const/4 p0, 0x0

    :goto_3
    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setPhotoGradientVisibility(I)V

    .line 1808885
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object p0

    .line 1808886
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a()Z

    move-result p1

    if-eqz p1, :cond_7

    const/4 p1, 0x1

    :goto_4
    move p0, p1

    .line 1808887
    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setPhotoEditable(Z)V

    .line 1808888
    if-eqz p0, :cond_1

    .line 1808889
    iget-object p0, v0, LX/BhD;->e:LX/BgQ;

    .line 1808890
    new-instance p1, LX/BgL;

    invoke-direct {p1, p0, v5, v2, v3}, LX/BgL;-><init>(LX/BgQ;Landroid/support/v4/app/Fragment;LX/BgW;LX/BgS;)V

    move-object p0, p1

    .line 1808891
    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->setCameraButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808892
    :cond_1
    new-instance p0, LX/BhB;

    invoke-direct {p0, v0, v1, v2, v3}, LX/BhB;-><init>(LX/BhD;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;LX/BgW;LX/BgS;)V

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808893
    iget-object p0, v2, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    move-object p0, p0

    .line 1808894
    iget-boolean p1, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    move p0, p1

    .line 1808895
    if-eqz p0, :cond_2

    .line 1808896
    new-instance p0, LX/BhC;

    invoke-direct {p0, v0, v4, v1}, LX/BhC;-><init>(LX/BhD;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    invoke-virtual {v1, p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a(Landroid/text/TextWatcher;)V

    .line 1808897
    :cond_2
    return-void

    .line 1808898
    :cond_3
    const/16 p0, 0x8

    goto :goto_3

    .line 1808899
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0f0123

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    const/4 p5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p6

    aput-object p6, p4, p5

    invoke-virtual {p2, p3, p1, p4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 1808900
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p2

    goto/16 :goto_0

    :cond_5
    const/4 p1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 p1, 0x0

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    goto :goto_4
.end method
