.class public final LX/C9d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/C9e;


# direct methods
.method public constructor <init>(LX/C9e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1854257
    iput-object p1, p0, LX/C9d;->b:LX/C9e;

    iput-object p2, p0, LX/C9d;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7b673745

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1854258
    iget-object v0, p0, LX/C9d;->b:LX/C9e;

    iget-object v0, v0, LX/C9e;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Mj;

    iget-object v2, p0, LX/C9d;->a:Ljava/lang/String;

    sget-object v3, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    .line 1854259
    iget-object v5, v0, LX/8Mj;->f:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 1854260
    iget-wide v7, v0, LX/8Mj;->j:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_0

    iget-wide v7, v0, LX/8Mj;->j:J

    sub-long v7, v5, v7

    iget-wide v9, v0, LX/8Mj;->i:J

    cmp-long v7, v7, v9

    if-gez v7, :cond_0

    .line 1854261
    invoke-static {}, LX/0Vg;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1854262
    :goto_0
    const v0, 0x121bd759

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1854263
    :cond_0
    iput-wide v5, v0, LX/8Mj;->j:J

    .line 1854264
    iget-object v5, v0, LX/8Mj;->a:LX/7ma;

    invoke-virtual {v5, v2}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v5

    .line 1854265
    if-nez v5, :cond_1

    .line 1854266
    new-instance v5, Ljava/lang/Throwable;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "couldn\'t find story, sessionId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1854267
    :cond_1
    invoke-static {v5}, LX/8Mj;->a(LX/7ml;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1854268
    iget-object v6, v0, LX/8Mj;->c:LX/1EZ;

    invoke-virtual {v6}, LX/1EZ;->l()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    const/4 v7, 0x2

    if-ge v6, v7, :cond_5

    const/4 v6, 0x1

    :goto_1
    move v6, v6

    .line 1854269
    if-nez v6, :cond_2

    .line 1854270
    new-instance v5, Ljava/lang/Throwable;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "upload manager isn\'t in the correct state for us to use, sessionId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 1854271
    :cond_2
    iget-object v6, v0, LX/8Mj;->h:LX/1RW;

    invoke-virtual {v3}, LX/5Ro;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1854272
    const/4 v6, 0x0

    .line 1854273
    iget-object v7, v0, LX/8Mj;->c:LX/1EZ;

    invoke-virtual {v7}, LX/1EZ;->l()Ljava/util/Set;

    move-result-object v7

    .line 1854274
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1854275
    :goto_2
    move-object v6, v6

    .line 1854276
    if-nez v6, :cond_3

    .line 1854277
    iget-object v6, v0, LX/8Mj;->c:LX/1EZ;

    invoke-virtual {v5}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3}, LX/8Mj;->a(LX/5Ro;)LX/8Kx;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/1EZ;->a(Ljava/lang/String;LX/8Kx;)Z

    .line 1854278
    invoke-static {v0, v5, v3}, LX/8Mj;->b(LX/8Mj;LX/7ml;LX/5Ro;)V

    .line 1854279
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v6}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1854280
    goto/16 :goto_0

    .line 1854281
    :cond_3
    iget-object v5, v0, LX/8Mj;->a:LX/7ma;

    invoke-virtual {v5, v6}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v5

    .line 1854282
    if-nez v5, :cond_8

    .line 1854283
    new-instance v5, Ljava/lang/Throwable;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "couldn\'t find first upload story, sessionId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1854284
    :goto_3
    goto/16 :goto_0

    .line 1854285
    :cond_4
    new-instance v5, Ljava/lang/Throwable;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "can\'t retry non-media stories (yet!), sessionId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1854286
    :cond_6
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_7

    .line 1854287
    iget-object v7, v0, LX/8Mj;->g:LX/03V;

    const-string v8, "compost_retry_not_supported_concurrent_uploads"

    const-string v9, "this retry controller does not support concurrent uploads"

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1854288
    :cond_7
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1854289
    iget-object v7, v6, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v6, v7

    .line 1854290
    goto :goto_2

    .line 1854291
    :cond_8
    iget-object v7, v0, LX/8Mj;->c:LX/1EZ;

    invoke-static {v3}, LX/8Mj;->a(LX/5Ro;)LX/8Kx;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, LX/1EZ;->b(Ljava/lang/String;LX/8Kx;)Z

    .line 1854292
    invoke-static {v0, v5, v3}, LX/8Mj;->b(LX/8Mj;LX/7ml;LX/5Ro;)V

    .line 1854293
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_3
.end method
