.class public LX/Bie;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810690
    iput-object p1, p0, LX/Bie;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1810691
    iput-object p2, p0, LX/Bie;->b:LX/0Or;

    .line 1810692
    return-void
.end method

.method public static a(LX/0QB;)LX/Bie;
    .locals 1

    .prologue
    .line 1810688
    invoke-static {p0}, LX/Bie;->b(LX/0QB;)LX/Bie;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Bie;Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)V
    .locals 7

    .prologue
    .line 1810681
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1810682
    if-nez v1, :cond_0

    .line 1810683
    invoke-direct/range {p0 .. p6}, LX/Bie;->b(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1810684
    iget-object v1, p0, LX/Bie;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1810685
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    .line 1810686
    invoke-direct/range {v0 .. v6}, LX/Bie;->b(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1810687
    iget-object v2, p0, LX/Bie;->a:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x6a

    invoke-interface {v2, v0, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Bie;
    .locals 3

    .prologue
    .line 1810679
    new-instance v1, LX/Bie;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x19e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Bie;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 1810680
    return-object v1
.end method

.method private b(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1810672
    iget-object v0, p0, LX/Bie;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1810673
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1810674
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1810675
    iget-object v4, p4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v0

    .line 1810676
    if-eqz p6, :cond_0

    .line 1810677
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1810678
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1810668
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, LX/Bie;->a(LX/Bie;Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)V

    .line 1810669
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 7

    .prologue
    .line 1810670
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, LX/Bie;->a(LX/Bie;Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;Z)V

    .line 1810671
    return-void
.end method
