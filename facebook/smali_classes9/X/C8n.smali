.class public final LX/C8n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/2yF;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;LX/2yF;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "LX/2yF;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1853245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853246
    iput-object p1, p0, LX/C8n;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1853247
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853248
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/C8n;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1853249
    iput-object p2, p0, LX/C8n;->c:Ljava/util/Map;

    .line 1853250
    iput-object p3, p0, LX/C8n;->d:LX/2yF;

    .line 1853251
    iput-object p4, p0, LX/C8n;->e:Ljava/lang/String;

    .line 1853252
    return-void
.end method
