.class public final enum LX/CTG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CTG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CTG;

.field public static final enum FETCH_TYPEAHEAD_RESULTS:LX/CTG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1895650
    new-instance v0, LX/CTG;

    const-string v1, "FETCH_TYPEAHEAD_RESULTS"

    invoke-direct {v0, v1, v2}, LX/CTG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CTG;->FETCH_TYPEAHEAD_RESULTS:LX/CTG;

    .line 1895651
    const/4 v0, 0x1

    new-array v0, v0, [LX/CTG;

    sget-object v1, LX/CTG;->FETCH_TYPEAHEAD_RESULTS:LX/CTG;

    aput-object v1, v0, v2

    sput-object v0, LX/CTG;->$VALUES:[LX/CTG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1895652
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CTG;
    .locals 1

    .prologue
    .line 1895653
    const-class v0, LX/CTG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CTG;

    return-object v0
.end method

.method public static values()[LX/CTG;
    .locals 1

    .prologue
    .line 1895654
    sget-object v0, LX/CTG;->$VALUES:[LX/CTG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CTG;

    return-object v0
.end method
