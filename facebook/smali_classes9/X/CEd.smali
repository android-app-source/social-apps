.class public LX/CEd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/CEd;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/03V;

.field public final c:LX/0W9;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/03V;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1861575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1861576
    iput-object p1, p0, LX/CEd;->a:Landroid/content/res/Resources;

    .line 1861577
    iput-object p2, p0, LX/CEd;->b:LX/03V;

    .line 1861578
    iput-object p3, p0, LX/CEd;->c:LX/0W9;

    .line 1861579
    return-void
.end method

.method public static a(LX/CEd;Ljava/util/HashMap;Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            ">;",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1861580
    const/4 v6, 0x0

    .line 1861581
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 1861582
    iget-object v10, p2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->b:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v11, :cond_8

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    .line 1861583
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1861584
    iget-object v1, p0, LX/CEd;->b:LX/03V;

    const-string v2, "SouvenirTemplateGen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing slide key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 1861585
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v6, v0

    goto :goto_0

    .line 1861586
    :cond_0
    const/4 v3, 0x0

    .line 1861587
    const/4 v2, 0x0

    .line 1861588
    const/4 v1, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    .line 1861589
    :goto_2
    iget v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    if-ge v4, v1, :cond_5

    .line 1861590
    add-int v1, v6, v4

    iget-object v5, p2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v1, v5, :cond_5

    .line 1861591
    iget-object v1, p2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object v1

    add-int v5, v6, v4

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v12

    .line 1861592
    invoke-virtual {v12}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v1

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v1, v5, :cond_1

    invoke-virtual {v12}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v1

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v1, v5, :cond_3

    .line 1861593
    :cond_1
    invoke-virtual {v12}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1861594
    const/4 v1, 0x0

    move v2, v3

    .line 1861595
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    .line 1861596
    goto :goto_2

    .line 1861597
    :cond_2
    add-int/lit8 v3, v3, 0x1

    move v1, v2

    move v2, v3

    goto :goto_3

    .line 1861598
    :cond_3
    const/4 v7, 0x0

    .line 1861599
    const/4 v1, 0x0

    move v5, v1

    :goto_4
    invoke-virtual {v12}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_b

    .line 1861600
    invoke-virtual {v12}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v1

    .line 1861601
    if-eqz v1, :cond_4

    .line 1861602
    const/4 v5, 0x1

    .line 1861603
    :goto_5
    if-eqz v2, :cond_a

    move v1, v5

    .line 1861604
    :goto_6
    if-eqz v5, :cond_9

    .line 1861605
    add-int/lit8 v2, v3, 0x1

    .line 1861606
    :goto_7
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    .line 1861607
    goto/16 :goto_2

    .line 1861608
    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 1861609
    :cond_5
    iget v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    if-ne v4, v1, :cond_6

    if-eqz v2, :cond_6

    .line 1861610
    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861611
    iget v0, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    add-int/2addr v0, v6

    .line 1861612
    goto/16 :goto_1

    .line 1861613
    :cond_6
    if-nez v3, :cond_7

    iget v1, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 1861614
    add-int/lit8 v0, v6, 0x1

    .line 1861615
    goto/16 :goto_1

    .line 1861616
    :cond_7
    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    iget-object v2, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;-><init>(Ljava/lang/String;I)V

    .line 1861617
    iget v0, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    add-int/2addr v0, v6

    .line 1861618
    invoke-virtual {v9, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 1861619
    :cond_8
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_9
    move v2, v3

    goto :goto_7

    :cond_a
    move v1, v2

    goto :goto_6

    :cond_b
    move v5, v7

    goto :goto_5
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;LX/0Rc;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;",
            "LX/0Rc",
            "<+",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsDetailsFields$MediaElements$Edges;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 1861620
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    move v1, v2

    .line 1861621
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    if-ge v1, v0, :cond_6

    .line 1861622
    invoke-virtual {p1}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v6

    .line 1861623
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v0, v3, :cond_0

    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v0, v3, :cond_0

    :cond_1
    move v3, v2

    .line 1861624
    :goto_1
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 1861625
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1861626
    if-eqz v0, :cond_2

    .line 1861627
    const/4 v0, 0x1

    .line 1861628
    :goto_2
    if-eqz v0, :cond_0

    .line 1861629
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v3, :cond_3

    .line 1861630
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "media_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861631
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1861632
    goto/16 :goto_0

    .line 1861633
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1861634
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1861635
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    .line 1861636
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->c()LX/1f8;

    move-result-object v3

    .line 1861637
    if-eqz v3, :cond_4

    new-instance v0, Landroid/graphics/PointF;

    invoke-interface {v3}, LX/1f8;->a()D

    move-result-wide v8

    double-to-float v7, v8

    invoke-interface {v3}, LX/1f8;->b()D

    move-result-wide v8

    double-to-float v3, v8

    invoke-direct {v0, v7, v3}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v3, v0

    .line 1861638
    :goto_3
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v7, :cond_7

    .line 1861639
    invoke-virtual {v6}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1861640
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->q()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1861641
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->q()Ljava/lang/String;

    move-result-object v0

    .line 1861642
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "media_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0, v3}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1861644
    goto/16 :goto_0

    .line 1861645
    :cond_4
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v10, v10}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v3, v0

    goto :goto_3

    .line 1861646
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 1861647
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_7
    move-object v0, v4

    goto :goto_4

    :cond_8
    move v0, v2

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)LX/CEd;
    .locals 6

    .prologue
    .line 1861648
    sget-object v0, LX/CEd;->d:LX/CEd;

    if-nez v0, :cond_1

    .line 1861649
    const-class v1, LX/CEd;

    monitor-enter v1

    .line 1861650
    :try_start_0
    sget-object v0, LX/CEd;->d:LX/CEd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1861651
    if-eqz v2, :cond_0

    .line 1861652
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1861653
    new-instance p0, LX/CEd;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-direct {p0, v3, v4, v5}, LX/CEd;-><init>(Landroid/content/res/Resources;LX/03V;LX/0W9;)V

    .line 1861654
    move-object v0, p0

    .line 1861655
    sput-object v0, LX/CEd;->d:LX/CEd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1861656
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1861657
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1861658
    :cond_1
    sget-object v0, LX/CEd;->d:LX/CEd;

    return-object v0

    .line 1861659
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1861660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
