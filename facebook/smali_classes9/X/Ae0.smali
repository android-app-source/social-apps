.class public final LX/Ae0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:LX/Ae2;


# direct methods
.method public constructor <init>(LX/Ae2;)V
    .locals 0

    .prologue
    .line 1695763
    iput-object p1, p0, LX/Ae0;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1695764
    iget-object v0, p0, LX/Ae0;->a:LX/Ae2;

    const-string v1, "share_menu_dismissed"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695765
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1695766
    const-string v0, "share_button_tapped"

    iget-object v1, p0, LX/Ae0;->a:LX/Ae2;

    iget-object v1, v1, LX/Ae2;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1695767
    iget-object v0, p0, LX/Ae0;->a:LX/Ae2;

    const-string v1, "share_menu_dismissed"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695768
    :cond_0
    return-void
.end method
