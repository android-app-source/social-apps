.class public final LX/BkM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1813979
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1813980
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1813981
    :goto_0
    return v1

    .line 1813982
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1813983
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1813984
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1813985
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1813986
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1813987
    const-string v7, "cover_photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1813988
    const/4 v6, 0x0

    .line 1813989
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_c

    .line 1813990
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1813991
    :goto_2
    move v5, v6

    .line 1813992
    goto :goto_1

    .line 1813993
    :cond_2
    const-string v7, "event_category_groups"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1813994
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1813995
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_11

    .line 1813996
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1813997
    :goto_3
    move v4, v6

    .line 1813998
    goto :goto_1

    .line 1813999
    :cond_3
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1814000
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1814001
    :cond_4
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1814002
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1814003
    :cond_5
    const-string v7, "pageProfilePicture"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1814004
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1814005
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1814006
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1814007
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1814008
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1814009
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1814010
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1814011
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1814012
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1814013
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1814014
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1814015
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1814016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_9

    if-eqz v8, :cond_9

    .line 1814017
    const-string v9, "focus"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1814018
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_4

    .line 1814019
    :cond_a
    const-string v9, "photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1814020
    invoke-static {p0, p1}, LX/BkL;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_4

    .line 1814021
    :cond_b
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1814022
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 1814023
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1814024
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_c
    move v5, v6

    move v7, v6

    goto :goto_4

    .line 1814025
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 1814026
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1814027
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1814028
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_d

    if-eqz v9, :cond_d

    .line 1814029
    const-string v10, "count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1814030
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v7

    goto :goto_5

    .line 1814031
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1814032
    :cond_f
    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1814033
    if-eqz v4, :cond_10

    .line 1814034
    invoke-virtual {p1, v6, v8, v6}, LX/186;->a(III)V

    .line 1814035
    :cond_10
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_11
    move v4, v6

    move v8, v6

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1814036
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1814037
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1814038
    if-eqz v0, :cond_2

    .line 1814039
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814040
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1814041
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1814042
    if-eqz v1, :cond_0

    .line 1814043
    const-string v2, "focus"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814044
    invoke-static {p0, v1, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1814045
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1814046
    if-eqz v1, :cond_1

    .line 1814047
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814048
    invoke-static {p0, v1, p2, p3}, LX/BkL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1814049
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1814050
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1814051
    if-eqz v0, :cond_4

    .line 1814052
    const-string v1, "event_category_groups"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814053
    const/4 v1, 0x0

    .line 1814054
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1814055
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1814056
    if-eqz v1, :cond_3

    .line 1814057
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814058
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1814059
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1814060
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1814061
    if-eqz v0, :cond_5

    .line 1814062
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814063
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1814064
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1814065
    if-eqz v0, :cond_6

    .line 1814066
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814067
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1814068
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1814069
    if-eqz v0, :cond_7

    .line 1814070
    const-string v1, "pageProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1814071
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1814072
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1814073
    return-void
.end method
