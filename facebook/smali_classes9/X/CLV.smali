.class public LX/CLV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field private static final b:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final c:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final d:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final e:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final f:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final g:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final h:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final i:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final j:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final k:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final l:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final m:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final n:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final o:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

.field public static final p:Lcom/facebook/messaging/customthreads/CustomThreadTheme;


# instance fields
.field private final q:LX/0W3;

.field private final r:LX/0lB;

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/customthreads/CustomThreadTheme;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const v6, -0xbb4139

    const v5, -0xdf310b

    const v4, -0xec30ed

    const/4 v3, -0x1

    const v2, -0x151516

    .line 1878930
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878931
    iput v2, v0, LX/CLU;->d:I

    .line 1878932
    move-object v0, v0

    .line 1878933
    const v1, -0xff7b01

    .line 1878934
    iput v1, v0, LX/CLU;->e:I

    .line 1878935
    move-object v0, v0

    .line 1878936
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->a:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878937
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    const v1, -0x62ff4c

    .line 1878938
    iput v1, v0, LX/CLU;->b:I

    .line 1878939
    move-object v0, v0

    .line 1878940
    const v1, -0x62ff4c

    .line 1878941
    iput v1, v0, LX/CLU;->d:I

    .line 1878942
    move-object v0, v0

    .line 1878943
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->b:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878944
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878945
    iput v3, v0, LX/CLU;->a:I

    .line 1878946
    move-object v0, v0

    .line 1878947
    const v1, -0x996634

    .line 1878948
    iput v1, v0, LX/CLU;->b:I

    .line 1878949
    move-object v0, v0

    .line 1878950
    iput v2, v0, LX/CLU;->c:I

    .line 1878951
    move-object v0, v0

    .line 1878952
    const v1, -0x996634

    .line 1878953
    iput v1, v0, LX/CLU;->d:I

    .line 1878954
    move-object v0, v0

    .line 1878955
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->c:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878956
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878957
    iput v3, v0, LX/CLU;->a:I

    .line 1878958
    move-object v0, v0

    .line 1878959
    iput v5, v0, LX/CLU;->b:I

    .line 1878960
    move-object v0, v0

    .line 1878961
    iput v2, v0, LX/CLU;->c:I

    .line 1878962
    move-object v0, v0

    .line 1878963
    iput v5, v0, LX/CLU;->d:I

    .line 1878964
    move-object v0, v0

    .line 1878965
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->d:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878966
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878967
    iput v3, v0, LX/CLU;->a:I

    .line 1878968
    move-object v0, v0

    .line 1878969
    iput v6, v0, LX/CLU;->b:I

    .line 1878970
    move-object v0, v0

    .line 1878971
    iput v2, v0, LX/CLU;->c:I

    .line 1878972
    move-object v0, v0

    .line 1878973
    iput v6, v0, LX/CLU;->d:I

    .line 1878974
    move-object v0, v0

    .line 1878975
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->e:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878976
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878977
    iput v3, v0, LX/CLU;->a:I

    .line 1878978
    move-object v0, v0

    .line 1878979
    iput v4, v0, LX/CLU;->b:I

    .line 1878980
    move-object v0, v0

    .line 1878981
    iput v2, v0, LX/CLU;->c:I

    .line 1878982
    move-object v0, v0

    .line 1878983
    iput v4, v0, LX/CLU;->d:I

    .line 1878984
    move-object v0, v0

    .line 1878985
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->f:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878986
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878987
    iput v3, v0, LX/CLU;->a:I

    .line 1878988
    move-object v0, v0

    .line 1878989
    const v1, -0x984798

    .line 1878990
    iput v1, v0, LX/CLU;->b:I

    .line 1878991
    move-object v0, v0

    .line 1878992
    iput v2, v0, LX/CLU;->c:I

    .line 1878993
    move-object v0, v0

    .line 1878994
    const v1, -0x984798

    .line 1878995
    iput v1, v0, LX/CLU;->d:I

    .line 1878996
    move-object v0, v0

    .line 1878997
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->g:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1878998
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1878999
    iput v3, v0, LX/CLU;->a:I

    .line 1879000
    move-object v0, v0

    .line 1879001
    const/16 v1, -0x3d00

    .line 1879002
    iput v1, v0, LX/CLU;->b:I

    .line 1879003
    move-object v0, v0

    .line 1879004
    iput v2, v0, LX/CLU;->c:I

    .line 1879005
    move-object v0, v0

    .line 1879006
    const/16 v1, -0x3d00

    .line 1879007
    iput v1, v0, LX/CLU;->d:I

    .line 1879008
    move-object v0, v0

    .line 1879009
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->h:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879010
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879011
    iput v3, v0, LX/CLU;->a:I

    .line 1879012
    move-object v0, v0

    .line 1879013
    const v1, -0x81d7

    .line 1879014
    iput v1, v0, LX/CLU;->b:I

    .line 1879015
    move-object v0, v0

    .line 1879016
    iput v2, v0, LX/CLU;->c:I

    .line 1879017
    move-object v0, v0

    .line 1879018
    const v1, -0x81d7

    .line 1879019
    iput v1, v0, LX/CLU;->d:I

    .line 1879020
    move-object v0, v0

    .line 1879021
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->i:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879022
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879023
    iput v3, v0, LX/CLU;->a:I

    .line 1879024
    move-object v0, v0

    .line 1879025
    const v1, -0x2b5774

    .line 1879026
    iput v1, v0, LX/CLU;->b:I

    .line 1879027
    move-object v0, v0

    .line 1879028
    iput v2, v0, LX/CLU;->c:I

    .line 1879029
    move-object v0, v0

    .line 1879030
    const v1, -0x2b5774

    .line 1879031
    iput v1, v0, LX/CLU;->d:I

    .line 1879032
    move-object v0, v0

    .line 1879033
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->j:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879034
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879035
    iput v3, v0, LX/CLU;->a:I

    .line 1879036
    move-object v0, v0

    .line 1879037
    const v1, -0x5c3b4

    .line 1879038
    iput v1, v0, LX/CLU;->b:I

    .line 1879039
    move-object v0, v0

    .line 1879040
    iput v2, v0, LX/CLU;->c:I

    .line 1879041
    move-object v0, v0

    .line 1879042
    const v1, -0x5c3b4

    .line 1879043
    iput v1, v0, LX/CLU;->d:I

    .line 1879044
    move-object v0, v0

    .line 1879045
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->k:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879046
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879047
    iput v3, v0, LX/CLU;->a:I

    .line 1879048
    move-object v0, v0

    .line 1879049
    const v1, -0x197a7b

    .line 1879050
    iput v1, v0, LX/CLU;->b:I

    .line 1879051
    move-object v0, v0

    .line 1879052
    iput v2, v0, LX/CLU;->c:I

    .line 1879053
    move-object v0, v0

    .line 1879054
    const v1, -0x197a7b

    .line 1879055
    iput v1, v0, LX/CLU;->d:I

    .line 1879056
    move-object v0, v0

    .line 1879057
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->l:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879058
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879059
    iput v3, v0, LX/CLU;->a:I

    .line 1879060
    move-object v0, v0

    .line 1879061
    const v1, -0xa35f

    .line 1879062
    iput v1, v0, LX/CLU;->b:I

    .line 1879063
    move-object v0, v0

    .line 1879064
    iput v2, v0, LX/CLU;->c:I

    .line 1879065
    move-object v0, v0

    .line 1879066
    const v1, -0xa35f

    .line 1879067
    iput v1, v0, LX/CLU;->d:I

    .line 1879068
    move-object v0, v0

    .line 1879069
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->m:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879070
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879071
    iput v3, v0, LX/CLU;->a:I

    .line 1879072
    move-object v0, v0

    .line 1879073
    const v1, -0x296945

    .line 1879074
    iput v1, v0, LX/CLU;->b:I

    .line 1879075
    move-object v0, v0

    .line 1879076
    iput v2, v0, LX/CLU;->c:I

    .line 1879077
    move-object v0, v0

    .line 1879078
    const v1, -0x296945

    .line 1879079
    iput v1, v0, LX/CLU;->d:I

    .line 1879080
    move-object v0, v0

    .line 1879081
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->n:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879082
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879083
    iput v3, v0, LX/CLU;->a:I

    .line 1879084
    move-object v0, v0

    .line 1879085
    const v1, -0x89b901

    .line 1879086
    iput v1, v0, LX/CLU;->b:I

    .line 1879087
    move-object v0, v0

    .line 1879088
    iput v2, v0, LX/CLU;->c:I

    .line 1879089
    move-object v0, v0

    .line 1879090
    const v1, -0x89b901

    .line 1879091
    iput v1, v0, LX/CLU;->d:I

    .line 1879092
    move-object v0, v0

    .line 1879093
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->o:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879094
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object v0

    .line 1879095
    iput v3, v0, LX/CLU;->a:I

    .line 1879096
    move-object v0, v0

    .line 1879097
    const v1, -0x596a39

    .line 1879098
    iput v1, v0, LX/CLU;->b:I

    .line 1879099
    move-object v0, v0

    .line 1879100
    iput v2, v0, LX/CLU;->c:I

    .line 1879101
    move-object v0, v0

    .line 1879102
    const v1, -0x596a39

    .line 1879103
    iput v1, v0, LX/CLU;->d:I

    .line 1879104
    move-object v0, v0

    .line 1879105
    invoke-virtual {v0}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v0

    sput-object v0, LX/CLV;->p:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879107
    iput-object p1, p0, LX/CLV;->q:LX/0W3;

    .line 1879108
    iput-object p2, p0, LX/CLV;->r:LX/0lB;

    .line 1879109
    invoke-static {p0}, LX/CLV;->b(LX/CLV;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, LX/CLV;->s:Ljava/util/List;

    .line 1879110
    iget-object p1, p0, LX/CLV;->s:Ljava/util/List;

    if-nez p1, :cond_0

    .line 1879111
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1879112
    sget-object p2, LX/CLV;->a:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879113
    sget-object p2, LX/CLV;->c:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879114
    sget-object p2, LX/CLV;->d:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879115
    sget-object p2, LX/CLV;->e:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879116
    sget-object p2, LX/CLV;->f:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879117
    sget-object p2, LX/CLV;->g:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879118
    sget-object p2, LX/CLV;->h:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879119
    sget-object p2, LX/CLV;->i:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879120
    sget-object p2, LX/CLV;->j:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879121
    sget-object p2, LX/CLV;->k:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879122
    sget-object p2, LX/CLV;->l:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879123
    sget-object p2, LX/CLV;->m:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879124
    sget-object p2, LX/CLV;->n:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879125
    sget-object p2, LX/CLV;->o:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879126
    sget-object p2, LX/CLV;->p:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879127
    move-object p1, p1

    .line 1879128
    move-object p1, p1

    .line 1879129
    iput-object p1, p0, LX/CLV;->s:Ljava/util/List;

    .line 1879130
    :cond_0
    return-void
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1879131
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1879132
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0lF;->o()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1879133
    :cond_0
    :goto_0
    return-object v0

    .line 1879134
    :cond_1
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 1879135
    const/16 v2, 0x10

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1879136
    :catch_0
    goto :goto_0
.end method

.method public static b(LX/CLV;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/customthreads/CustomThreadTheme;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1879137
    iget-object v0, p0, LX/CLV;->q:LX/0W3;

    sget-wide v2, LX/0X5;->gi:J

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1879138
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1879139
    :goto_0
    return-object v0

    .line 1879140
    :cond_0
    :try_start_0
    iget-object v2, p0, LX/CLV;->r:LX/0lB;

    invoke-virtual {v2, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1879141
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1879142
    sget-object v3, LX/CLV;->a:Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1879143
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1879144
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1879145
    const-string v4, "wallpaper_color"

    invoke-static {v0, v4}, LX/CLV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1879146
    const-string v5, "me_color"

    invoke-static {v0, v5}, LX/CLV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 1879147
    const-string v6, "other_color"

    invoke-static {v0, v6}, LX/CLV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 1879148
    const-string v7, "bar_color"

    invoke-static {v0, v7}, LX/CLV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    .line 1879149
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    if-eqz v6, :cond_2

    if-nez v7, :cond_4

    .line 1879150
    :cond_2
    const/4 v4, 0x0

    .line 1879151
    :goto_2
    move-object v0, v4

    .line 1879152
    if-eqz v0, :cond_1

    .line 1879153
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1879154
    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 1879155
    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->newBuilder()LX/CLU;

    move-result-object p0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1879156
    iput v4, p0, LX/CLU;->a:I

    .line 1879157
    move-object v4, p0

    .line 1879158
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1879159
    iput v5, v4, LX/CLU;->b:I

    .line 1879160
    move-object v4, v4

    .line 1879161
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1879162
    iput v5, v4, LX/CLU;->c:I

    .line 1879163
    move-object v4, v4

    .line 1879164
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1879165
    iput v5, v4, LX/CLU;->d:I

    .line 1879166
    move-object v4, v4

    .line 1879167
    invoke-virtual {v4}, LX/CLU;->f()Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    move-result-object v4

    goto :goto_2
.end method
