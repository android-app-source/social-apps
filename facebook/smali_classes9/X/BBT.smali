.class public final LX/BBT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1757281
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1757282
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757283
    :goto_0
    return v1

    .line 1757284
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757285
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1757286
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1757287
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1757288
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1757289
    const-string v6, "action_links"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1757290
    invoke-static {p0, p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1757291
    :cond_2
    const-string v6, "fallback_url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1757292
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1757293
    :cond_3
    const-string v6, "target"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1757294
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1757295
    :cond_4
    const-string v6, "targetId"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1757296
    invoke-static {p0, p1}, LX/BBS;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1757297
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1757298
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1757299
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1757300
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1757301
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1757302
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1757303
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1757304
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757305
    if-eqz v0, :cond_0

    .line 1757306
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757307
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757308
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757309
    if-eqz v0, :cond_1

    .line 1757310
    const-string v1, "fallback_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757311
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757312
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757313
    if-eqz v0, :cond_2

    .line 1757314
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757315
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757316
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757317
    if-eqz v0, :cond_3

    .line 1757318
    const-string v1, "targetId"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757319
    invoke-static {p0, v0, p2}, LX/BBS;->a(LX/15i;ILX/0nX;)V

    .line 1757320
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757321
    return-void
.end method
