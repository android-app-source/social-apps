.class public LX/C9q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/C9u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C9u",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/C9u;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854604
    iput-object p1, p0, LX/C9q;->a:LX/C9u;

    .line 1854605
    return-void
.end method

.method public static a(LX/0QB;)LX/C9q;
    .locals 4

    .prologue
    .line 1854606
    const-class v1, LX/C9q;

    monitor-enter v1

    .line 1854607
    :try_start_0
    sget-object v0, LX/C9q;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854608
    sput-object v2, LX/C9q;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854609
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854610
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854611
    new-instance p0, LX/C9q;

    invoke-static {v0}, LX/C9u;->a(LX/0QB;)LX/C9u;

    move-result-object v3

    check-cast v3, LX/C9u;

    invoke-direct {p0, v3}, LX/C9q;-><init>(LX/C9u;)V

    .line 1854612
    move-object v0, p0

    .line 1854613
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854614
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854615
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
