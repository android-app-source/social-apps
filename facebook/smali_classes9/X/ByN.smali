.class public LX/ByN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/38w;

.field public final c:LX/2yS;

.field public final d:LX/1nP;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/38w;LX/2yS;LX/1nP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837158
    iput-object p1, p0, LX/ByN;->a:Landroid/content/Context;

    .line 1837159
    iput-object p2, p0, LX/ByN;->b:LX/38w;

    .line 1837160
    iput-object p3, p0, LX/ByN;->c:LX/2yS;

    .line 1837161
    iput-object p4, p0, LX/ByN;->d:LX/1nP;

    .line 1837162
    return-void
.end method

.method public static a(LX/0QB;)LX/ByN;
    .locals 7

    .prologue
    .line 1837163
    const-class v1, LX/ByN;

    monitor-enter v1

    .line 1837164
    :try_start_0
    sget-object v0, LX/ByN;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837165
    sput-object v2, LX/ByN;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837166
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837167
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837168
    new-instance p0, LX/ByN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/38w;->b(LX/0QB;)LX/38w;

    move-result-object v4

    check-cast v4, LX/38w;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v5

    check-cast v5, LX/2yS;

    invoke-static {v0}, LX/1nP;->a(LX/0QB;)LX/1nP;

    move-result-object v6

    check-cast v6, LX/1nP;

    invoke-direct {p0, v3, v4, v5, v6}, LX/ByN;-><init>(Landroid/content/Context;LX/38w;LX/2yS;LX/1nP;)V

    .line 1837169
    move-object v0, p0

    .line 1837170
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837171
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ByN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837172
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
