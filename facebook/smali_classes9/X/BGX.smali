.class public LX/BGX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8G1;


# instance fields
.field public final a:LX/1kR;

.field private b:LX/0tX;

.field public c:LX/0fO;


# direct methods
.method public constructor <init>(LX/1kR;LX/0tX;LX/0fO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1767160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767161
    iput-object p1, p0, LX/BGX;->a:LX/1kR;

    .line 1767162
    iput-object p2, p0, LX/BGX;->b:LX/0tX;

    .line 1767163
    iput-object p3, p0, LX/BGX;->c:LX/0fO;

    .line 1767164
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1767165
    iget-object v0, p0, LX/BGX;->a:LX/1kR;

    invoke-virtual {v0}, LX/1kR;->a()LX/0zO;

    move-result-object v0

    .line 1767166
    iget-object v1, p0, LX/BGX;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1767167
    new-instance v1, LX/BGW;

    invoke-direct {v1, p0}, LX/BGW;-><init>(LX/BGX;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
