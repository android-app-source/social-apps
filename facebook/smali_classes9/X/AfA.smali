.class public final LX/AfA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Aeu;

.field public final synthetic b:LX/AfD;


# direct methods
.method public constructor <init>(LX/AfD;LX/Aeu;)V
    .locals 0

    .prologue
    .line 1698705
    iput-object p1, p0, LX/AfA;->b:LX/AfD;

    iput-object p2, p0, LX/AfA;->a:LX/Aeu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1698706
    iget-object v0, p0, LX/AfA;->b:LX/AfD;

    iget-object v0, v0, LX/AfD;->j:LX/Aev;

    iget-object v1, p0, LX/AfA;->a:LX/Aeu;

    invoke-virtual {v0, v1}, LX/Aev;->b(LX/Aeu;)V

    .line 1698707
    iget-object v0, p0, LX/AfA;->b:LX/AfD;

    iget-object v1, p0, LX/AfA;->a:LX/Aeu;

    iget-object v1, v1, LX/Aeu;->e:Ljava/lang/String;

    .line 1698708
    new-instance v2, LX/5Gu;

    invoke-direct {v2}, LX/5Gu;-><init>()V

    move-object v2, v2

    .line 1698709
    iput-object v1, v2, LX/5Gu;->b:Ljava/lang/String;

    .line 1698710
    move-object v2, v2

    .line 1698711
    const-string v3, ""

    .line 1698712
    iput-object v3, v2, LX/5Gu;->c:Ljava/lang/String;

    .line 1698713
    move-object v2, v2

    .line 1698714
    invoke-virtual {v2}, LX/5Gu;->a()Lcom/facebook/api/ufiservices/common/DeleteCommentParams;

    move-result-object v2

    .line 1698715
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1698716
    const-string p0, "deleteCommentParams"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1698717
    iget-object v2, v0, LX/AfD;->b:LX/0aG;

    const-string p0, "feed_delete_comment"

    const p1, 0x67362aee

    invoke-static {v2, p0, v3, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    .line 1698718
    const/4 v0, 0x0

    return v0
.end method
