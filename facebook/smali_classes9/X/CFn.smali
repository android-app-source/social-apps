.class public final LX/CFn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:Ljava/lang/String;

.field private B:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;"
        }
    .end annotation
.end field

.field public C:Ljava/lang/Float;

.field public D:Landroid/graphics/PointF;

.field public E:Lcom/facebook/greetingcards/verve/model/VMColor;

.field private F:Ljava/lang/String;

.field private a:F

.field private b:Ljava/lang/String;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field private g:Ljava/lang/Float;

.field public h:Ljava/lang/Float;

.field public i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field public l:Lcom/facebook/greetingcards/verve/model/VMColor;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Float;

.field private q:Ljava/lang/String;

.field private r:Lcom/facebook/greetingcards/verve/model/VMColor;

.field private s:Ljava/lang/Integer;

.field private t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1864166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864167
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/CFn;->a:F

    .line 1864168
    return-void
.end method


# virtual methods
.method public final a(F)LX/CFn;
    .locals 5

    .prologue
    .line 1864169
    iput p1, p0, LX/CFn;->a:F

    .line 1864170
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 1864171
    iget-object v0, p0, LX/CFn;->n:LX/0P1;

    if-eqz v0, :cond_0

    .line 1864172
    iget-object v0, p0, LX/CFn;->n:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1864173
    invoke-static {}, Lcom/facebook/greetingcards/verve/model/VMView;->p()LX/CFn;

    move-result-object v4

    iget-object v1, p0, LX/CFn;->n:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v4, v1}, LX/CFn;->a(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/CFn;->a(F)LX/CFn;

    move-result-object v1

    invoke-virtual {v1}, LX/CFn;->a()Lcom/facebook/greetingcards/verve/model/VMView;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1864174
    :cond_0
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 1864175
    iput-object v0, p0, LX/CFn;->n:LX/0P1;

    .line 1864176
    return-object p0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;
    .locals 1

    .prologue
    .line 1864177
    iget v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->i:F

    iput v0, p0, LX/CFn;->a:F

    .line 1864178
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->b:Ljava/lang/String;

    .line 1864179
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    iput-object v0, p0, LX/CFn;->c:LX/0Px;

    .line 1864180
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    iput-object v0, p0, LX/CFn;->d:LX/0Px;

    .line 1864181
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    iput-object v0, p0, LX/CFn;->e:LX/0Px;

    .line 1864182
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->mType:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->f:Ljava/lang/String;

    .line 1864183
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    iput-object v0, p0, LX/CFn;->g:Ljava/lang/Float;

    .line 1864184
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    iput-object v0, p0, LX/CFn;->h:Ljava/lang/Float;

    .line 1864185
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->i:Ljava/lang/String;

    .line 1864186
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderSrc:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->j:Ljava/lang/String;

    .line 1864187
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->resize:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->k:Ljava/lang/String;

    .line 1864188
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object v0, p0, LX/CFn;->l:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864189
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    iput-object v0, p0, LX/CFn;->m:LX/0Px;

    .line 1864190
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    iput-object v0, p0, LX/CFn;->n:LX/0P1;

    .line 1864191
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->text:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->o:Ljava/lang/String;

    .line 1864192
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    iput-object v0, p0, LX/CFn;->p:Ljava/lang/Float;

    .line 1864193
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->q:Ljava/lang/String;

    .line 1864194
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object v0, p0, LX/CFn;->r:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864195
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->shadowRadius:Ljava/lang/Integer;

    iput-object v0, p0, LX/CFn;->s:Ljava/lang/Integer;

    .line 1864196
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->shadowOffset:LX/0Px;

    iput-object v0, p0, LX/CFn;->t:LX/0Px;

    .line 1864197
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->u:Ljava/lang/String;

    .line 1864198
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->v:Ljava/lang/String;

    .line 1864199
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->w:Ljava/lang/String;

    .line 1864200
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->styleName:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->x:Ljava/lang/String;

    .line 1864201
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->title:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->y:Ljava/lang/String;

    .line 1864202
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->z:Ljava/lang/String;

    .line 1864203
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->buttonSize:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->A:Ljava/lang/String;

    .line 1864204
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    iput-object v0, p0, LX/CFn;->B:LX/0P1;

    .line 1864205
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->duration:Ljava/lang/Float;

    iput-object v0, p0, LX/CFn;->C:Ljava/lang/Float;

    .line 1864206
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    iput-object v0, p0, LX/CFn;->D:Landroid/graphics/PointF;

    .line 1864207
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object v0, p0, LX/CFn;->E:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864208
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->lineBreak:Ljava/lang/String;

    iput-object v0, p0, LX/CFn;->F:Ljava/lang/String;

    .line 1864209
    return-object p0
.end method

.method public final a()Lcom/facebook/greetingcards/verve/model/VMView;
    .locals 35

    .prologue
    .line 1864210
    new-instance v1, Lcom/facebook/greetingcards/verve/model/VMView;

    move-object/from16 v0, p0

    iget v2, v0, LX/CFn;->a:F

    move-object/from16 v0, p0

    iget-object v3, v0, LX/CFn;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/CFn;->c:LX/0Px;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/CFn;->d:LX/0Px;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/CFn;->e:LX/0Px;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/CFn;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/CFn;->g:Ljava/lang/Float;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/CFn;->h:Ljava/lang/Float;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/CFn;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/CFn;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/CFn;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/CFn;->l:Lcom/facebook/greetingcards/verve/model/VMColor;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/CFn;->m:LX/0Px;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/CFn;->n:LX/0P1;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->p:Ljava/lang/Float;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->r:Lcom/facebook/greetingcards/verve/model/VMColor;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->s:Ljava/lang/Integer;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->t:LX/0Px;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->u:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->w:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->x:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->y:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->z:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->A:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->B:LX/0P1;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->C:Ljava/lang/Float;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->D:Landroid/graphics/PointF;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->E:Lcom/facebook/greetingcards/verve/model/VMColor;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/CFn;->F:Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    invoke-direct/range {v1 .. v34}, Lcom/facebook/greetingcards/verve/model/VMView;-><init>(FLjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;LX/0Px;LX/0P1;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/Integer;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;Ljava/lang/Float;Landroid/graphics/PointF;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/String;B)V

    return-object v1
.end method

.method public final b(Lcom/facebook/greetingcards/verve/model/VMView;)LX/CFn;
    .locals 1

    .prologue
    .line 1864211
    iget-object v0, p0, LX/CFn;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CFn;->i:Ljava/lang/String;

    .line 1864212
    :goto_0
    iput-object v0, p0, LX/CFn;->i:Ljava/lang/String;

    .line 1864213
    iget-object v0, p0, LX/CFn;->l:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CFn;->l:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864214
    :goto_1
    iput-object v0, p0, LX/CFn;->l:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864215
    iget-object v0, p0, LX/CFn;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CFn;->o:Ljava/lang/String;

    .line 1864216
    :goto_2
    iput-object v0, p0, LX/CFn;->o:Ljava/lang/String;

    .line 1864217
    iget-object v0, p0, LX/CFn;->p:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/CFn;->p:Ljava/lang/Float;

    .line 1864218
    :goto_3
    iput-object v0, p0, LX/CFn;->p:Ljava/lang/Float;

    .line 1864219
    iget-object v0, p0, LX/CFn;->u:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CFn;->u:Ljava/lang/String;

    .line 1864220
    :goto_4
    iput-object v0, p0, LX/CFn;->u:Ljava/lang/String;

    .line 1864221
    iget-object v0, p0, LX/CFn;->v:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/CFn;->v:Ljava/lang/String;

    .line 1864222
    :goto_5
    iput-object v0, p0, LX/CFn;->v:Ljava/lang/String;

    .line 1864223
    iget-object v0, p0, LX/CFn;->E:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/CFn;->E:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864224
    :goto_6
    iput-object v0, p0, LX/CFn;->E:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864225
    return-object p0

    .line 1864226
    :cond_0
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    goto :goto_0

    .line 1864227
    :cond_1
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    goto :goto_1

    .line 1864228
    :cond_2
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->text:Ljava/lang/String;

    goto :goto_2

    .line 1864229
    :cond_3
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    goto :goto_3

    .line 1864230
    :cond_4
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    goto :goto_4

    .line 1864231
    :cond_5
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    goto :goto_5

    .line 1864232
    :cond_6
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    goto :goto_6
.end method
