.class public LX/Bsz;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/1wK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/1wK;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1828558
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1828559
    return-void
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1828557
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1828555
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0}, LX/1wK;->a()V

    .line 1828556
    return-void
.end method

.method public getBaseView()Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1828552
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Bsz;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1828553
    instance-of v1, v0, LX/1wK;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1828554
    return-object v0
.end method

.method public setAlpha(F)V
    .locals 1

    .prologue
    .line 1828550
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1828551
    return-void
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 1828548
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setBottomDividerStyle(LX/1Wl;)V

    .line 1828549
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1828546
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1828547
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 1

    .prologue
    .line 1828544
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setButtonContainerHeight(I)V

    .line 1828545
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 1

    .prologue
    .line 1828560
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setButtonWeights([F)V

    .line 1828561
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1828542
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setButtons(Ljava/util/Set;)V

    .line 1828543
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 1

    .prologue
    .line 1828540
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setDownstateType(LX/1Wk;)V

    .line 1828541
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1828538
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1828539
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 1

    .prologue
    .line 1828536
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setHasCachedComments(Z)V

    .line 1828537
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 1

    .prologue
    .line 1828534
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setIsLiked(Z)V

    .line 1828535
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 1828532
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setOnButtonClickedListener(LX/20Z;)V

    .line 1828533
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 1

    .prologue
    .line 1828530
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setShowIcons(Z)V

    .line 1828531
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1828528
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setSprings(Ljava/util/EnumMap;)V

    .line 1828529
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 1

    .prologue
    .line 1828526
    invoke-virtual {p0}, LX/Bsz;->getBaseView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1wK;

    invoke-interface {v0, p1}, LX/1wK;->setTopDividerStyle(LX/1Wl;)V

    .line 1828527
    return-void
.end method
