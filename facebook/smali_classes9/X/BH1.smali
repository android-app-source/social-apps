.class public final LX/BH1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BH0;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767582
    iput-object p1, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1767583
    if-eqz p1, :cond_0

    .line 1767584
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ar:LX/3l1;

    .line 1767585
    sget-object v1, LX/BOR;->SLIDESHOW_SELECTED:LX/BOR;

    invoke-static {v1}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767586
    :goto_0
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-boolean p1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    .line 1767587
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    iget-object v1, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BHj;->a(LX/4gI;)V

    .line 1767588
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1767589
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->F:LX/BHq;

    iget-object v1, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v1}, LX/BHJ;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/BHq;->a(ZLX/0Px;)V

    .line 1767590
    return-void

    .line 1767591
    :cond_0
    iget-object v0, p0, LX/BH1;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ar:LX/3l1;

    .line 1767592
    sget-object v1, LX/BOR;->SLIDESHOW_DESELECTED:LX/BOR;

    invoke-static {v1}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767593
    goto :goto_0
.end method
