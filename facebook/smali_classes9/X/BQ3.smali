.class public LX/BQ3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1781399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)F
    .locals 2

    .prologue
    .line 1781400
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1781401
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x40800000    # 4.0f

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFocusedPhoto;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;
    .locals 14
    .param p0    # Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1781402
    if-nez p0, :cond_0

    .line 1781403
    const/4 v0, 0x0

    .line 1781404
    :goto_0
    return-object v0

    .line 1781405
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1781406
    :cond_1
    const/4 v0, 0x0

    .line 1781407
    :goto_1
    move-object v0, v0

    .line 1781408
    new-instance v1, LX/5wZ;

    invoke-direct {v1}, LX/5wZ;-><init>()V

    .line 1781409
    iput-object v0, v1, LX/5wZ;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    .line 1781410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    .line 1781411
    if-eqz v0, :cond_3

    .line 1781412
    const/4 v4, 0x0

    .line 1781413
    if-nez v0, :cond_5

    .line 1781414
    :cond_2
    :goto_2
    move-object v0, v4

    .line 1781415
    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a(LX/1f8;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    .line 1781416
    iput-object v0, v1, LX/5wZ;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1781417
    :cond_3
    invoke-virtual {v1}, LX/5wZ;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, LX/5wY;

    invoke-direct {v0}, LX/5wY;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/9JZ;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 1781418
    iput-object v1, v0, LX/5wY;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1781419
    move-object v0, v0

    .line 1781420
    invoke-virtual {v0}, LX/5wY;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    goto :goto_1

    .line 1781421
    :cond_5
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1781422
    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    .line 1781423
    if-nez v0, :cond_7

    .line 1781424
    :goto_3
    move v3, v9

    .line 1781425
    if-eqz v3, :cond_2

    .line 1781426
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1781427
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1781428
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1781429
    new-instance v2, LX/15i;

    const/4 v6, 0x1

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1781430
    instance-of v3, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v3, :cond_6

    .line 1781431
    const-string v3, "DefaultGraphQLConversionHelper.getDefaultVect2Fields"

    invoke-virtual {v2, v3, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1781432
    :cond_6
    new-instance v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-direct {v4, v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;-><init>(LX/15i;)V

    goto :goto_2

    .line 1781433
    :cond_7
    const/4 v8, 0x2

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1781434
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v10

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, LX/186;->a(IDD)V

    .line 1781435
    const/4 v9, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v10

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, LX/186;->a(IDD)V

    .line 1781436
    invoke-virtual {v2}, LX/186;->d()I

    move-result v9

    .line 1781437
    invoke-virtual {v2, v9}, LX/186;->d(I)V

    goto :goto_3
.end method

.method public static a(LX/5SB;Z)Z
    .locals 2

    .prologue
    .line 1781438
    sget-object v0, LX/5SA;->USER:LX/5SA;

    .line 1781439
    iget-object v1, p0, LX/5SB;->e:LX/5SA;

    move-object v1, v1

    .line 1781440
    invoke-virtual {v0, v1}, LX/5SA;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z
    .locals 3
    .param p0    # Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1781441
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v1

    .line 1781442
    :goto_0
    if-nez v2, :cond_0

    invoke-static {p0}, LX/BQ3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    .line 1781443
    goto :goto_0
.end method

.method public static b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;)Z
    .locals 1

    .prologue
    .line 1781444
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->ch_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
