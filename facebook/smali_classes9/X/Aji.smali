.class public LX/Aji;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

.field public final b:LX/Ajc;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/B5j;Landroid/content/Context;LX/Ajc;)V
    .locals 0
    .param p1    # Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            "LX/Ajc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708209
    invoke-direct {p0, p3, p2}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1708210
    iput-object p1, p0, LX/Aji;->a:Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    .line 1708211
    iput-object p4, p0, LX/Aji;->b:LX/Ajc;

    .line 1708212
    return-void
.end method


# virtual methods
.method public final X()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708213
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aB()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708214
    new-instance v0, LX/Ajd;

    invoke-direct {v0, p0}, LX/Ajd;-><init>(LX/Aji;)V

    return-object v0
.end method

.method public final aJ()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708215
    new-instance v0, LX/Ajg;

    invoke-direct {v0, p0}, LX/Ajg;-><init>(LX/Aji;)V

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708216
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final ap()LX/Aje;
    .locals 1

    .prologue
    .line 1708217
    new-instance v0, LX/Ajf;

    invoke-direct {v0, p0}, LX/Ajf;-><init>(LX/Aji;)V

    return-object v0
.end method
