.class public final LX/CVs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1905623
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1905624
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905625
    :goto_0
    return v1

    .line 1905626
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1905627
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1905628
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1905629
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1905630
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1905631
    const-string v5, "logo_image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1905632
    invoke-static {p0, p1}, LX/CVV;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1905633
    :cond_2
    const-string v5, "logo_url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1905634
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1905635
    :cond_3
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1905636
    invoke-static {p0, p1}, LX/CVa;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1905637
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1905638
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1905639
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1905640
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1905641
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1905642
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1905643
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905644
    if-eqz v0, :cond_0

    .line 1905645
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905646
    invoke-static {p0, v0, p2}, LX/CVV;->a(LX/15i;ILX/0nX;)V

    .line 1905647
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1905648
    if-eqz v0, :cond_1

    .line 1905649
    const-string v1, "logo_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905650
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1905651
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1905652
    if-eqz v0, :cond_2

    .line 1905653
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1905654
    invoke-static {p0, v0, p2, p3}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1905655
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1905656
    return-void
.end method
