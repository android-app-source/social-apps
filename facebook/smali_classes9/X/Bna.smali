.class public final LX/Bna;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bne;

.field public final b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;


# direct methods
.method public constructor <init>(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 0

    .prologue
    .line 1820934
    iput-object p1, p0, LX/Bna;->a:LX/Bne;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1820935
    iput-object p2, p0, LX/Bna;->b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1820936
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x46d10032

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1820916
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1820917
    new-instance v4, LX/6WS;

    invoke-direct {v4, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1820918
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 1820919
    sget-object v0, LX/Bne;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1820920
    iget-object v1, p0, LX/Bna;->b:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_0

    move v1, v2

    .line 1820921
    :goto_1
    sget-object v7, LX/BnX;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1820922
    const/4 v7, 0x0

    :goto_2
    move v7, v7

    .line 1820923
    invoke-virtual {v5, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 1820924
    iget-object v8, p0, LX/Bna;->a:LX/Bne;

    invoke-static {v8, v0, v1}, LX/Bne;->a$redex0(LX/Bne;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1820925
    invoke-virtual {v7, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1820926
    invoke-virtual {v7, v1}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1820927
    new-instance v1, LX/BnZ;

    invoke-direct {v1, p0, v0}, LX/BnZ;-><init>(LX/Bna;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    invoke-virtual {v7, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1820928
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1820929
    :cond_1
    invoke-virtual {v4, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1820930
    const v0, -0x874156

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1820931
    :pswitch_0
    const v7, 0x7f08129b

    goto :goto_2

    .line 1820932
    :pswitch_1
    const v7, 0x7f08129c

    goto :goto_2

    .line 1820933
    :pswitch_2
    const v7, 0x7f08129d

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
