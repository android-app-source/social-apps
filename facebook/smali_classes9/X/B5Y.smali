.class public interface abstract LX/B5Y;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract b()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()I
.end method

.method public abstract d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract iQ_()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()LX/B5W;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/B5X;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()LX/8Yp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()LX/8Z4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()J
.end method

.method public abstract t()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
