.class public final LX/BJE;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/BJF;


# direct methods
.method public constructor <init>(LX/BJF;)V
    .locals 0

    .prologue
    .line 1771687
    iput-object p1, p0, LX/BJE;->a:LX/BJF;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 1771688
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1771689
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1771690
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 1771691
    if-nez v0, :cond_0

    .line 1771692
    :goto_0
    return-void

    .line 1771693
    :cond_0
    iget-object v1, p0, LX/BJE;->a:LX/BJF;

    sget-object v2, LX/9b0;->DEFAULT:LX/9b0;

    invoke-static {v2}, LX/9b2;->a(LX/9b0;)LX/9b1;

    move-result-object v2

    .line 1771694
    iput-object v2, v1, LX/BJF;->h:LX/9b1;

    .line 1771695
    iget-object v1, p0, LX/BJE;->a:LX/BJF;

    iget-object v1, v1, LX/BJF;->a:Landroid/widget/ListView;

    iget-object v2, p0, LX/BJE;->a:LX/BJF;

    iget-object v2, v2, LX/BJF;->h:LX/9b1;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1771696
    iget-object v1, p0, LX/BJE;->a:LX/BJF;

    iget-object v1, v1, LX/BJF;->h:LX/9b1;

    invoke-virtual {v1, v0}, LX/9b1;->a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V

    .line 1771697
    iget-object v0, p0, LX/BJE;->a:LX/BJF;

    iget-object v0, v0, LX/BJF;->f:LX/BJB;

    .line 1771698
    iget-object v1, v0, LX/BJB;->b:LX/BJG;

    iget-object v1, v1, LX/BJG;->j:LX/BK5;

    invoke-virtual {v1}, LX/BK5;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1771699
    iget-object v1, v0, LX/BJB;->b:LX/BJG;

    iget-object v1, v1, LX/BJG;->i:LX/BJF;

    iget-object v2, v0, LX/BJB;->b:LX/BJG;

    iget-object v2, v2, LX/BJG;->j:LX/BK5;

    invoke-virtual {v2}, LX/BK5;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    .line 1771700
    if-nez v2, :cond_2

    .line 1771701
    :cond_1
    :goto_1
    iget-object v1, v0, LX/BJB;->a:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1771702
    goto :goto_0

    .line 1771703
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p0

    .line 1771704
    iget-object v2, v1, LX/BJF;->h:LX/9b1;

    if-eqz v2, :cond_3

    .line 1771705
    iget-object v2, v1, LX/BJF;->h:LX/9b1;

    invoke-virtual {v2, p0}, LX/9b1;->a(Ljava/lang/String;)V

    .line 1771706
    :cond_3
    goto :goto_1
.end method
