.class public LX/CZk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1916520
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 11

    .prologue
    .line 1916521
    const-string v0, "comment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1916522
    const-string v0, "feedback_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1916523
    const-string v0, "story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1916524
    const-string v0, "story_cache_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1916525
    const-string v0, "group_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1916526
    const-string v0, "relevant_comment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1916527
    const-string v0, "comment"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916528
    const-string v1, "include_comments_disabled_fields"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 1916529
    const-string v1, "relevant_comment"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916530
    const-string v2, "feedback_logging_params"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1916531
    new-instance v10, LX/6BM;

    invoke-direct {v10}, LX/6BM;-><init>()V

    .line 1916532
    iput-object v0, v10, LX/6BM;->h:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916533
    move-object v0, v10

    .line 1916534
    iput-object v1, v0, LX/6BM;->i:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916535
    move-object v0, v0

    .line 1916536
    iput-object v3, v0, LX/6BM;->a:Ljava/lang/String;

    .line 1916537
    move-object v0, v0

    .line 1916538
    iput-object v4, v0, LX/6BM;->b:Ljava/lang/String;

    .line 1916539
    move-object v0, v0

    .line 1916540
    iput-object v5, v0, LX/6BM;->d:Ljava/lang/String;

    .line 1916541
    move-object v0, v0

    .line 1916542
    iput-object v6, v0, LX/6BM;->e:Ljava/lang/String;

    .line 1916543
    move-object v0, v0

    .line 1916544
    iput-object v8, v0, LX/6BM;->g:Ljava/lang/String;

    .line 1916545
    move-object v0, v0

    .line 1916546
    iput-object v7, v0, LX/6BM;->j:Ljava/lang/String;

    .line 1916547
    move-object v0, v0

    .line 1916548
    iput-boolean v9, v0, LX/6BM;->k:Z

    .line 1916549
    move-object v0, v0

    .line 1916550
    invoke-virtual {v0}, LX/6BM;->a()Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    move-result-object v0

    .line 1916551
    const-string v1, "Comment params are required"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916552
    new-instance v1, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    invoke-direct {v1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;-><init>()V

    .line 1916553
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1916554
    const-string v4, "commentParams"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1916555
    const-string v4, "feedbackLoggingParams"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1916556
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1916557
    move-object v0, v1

    .line 1916558
    return-object v0
.end method
